package es.uji.apps.par.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import es.uji.apps.par.model.Abonado;

@RunWith(SpringJUnit4ClassRunner.class)
public class AbonadosServiceTest extends BaseServiceTest {
    @Autowired
    AbonadosService abonadosService;

    @Test
    public void getAbonados() {
        List<Abonado> abonados = abonadosService.getAbonadosByAbonoId(abonoJuvilado.getId(), SORT, START, LIMIT);

        Assert.assertNotNull(abonados);
        Assert.assertEquals(abonados.size(), 1);
        Assert.assertEquals(abonados.get(0).getEmail(), EMAIL_ABONADO);

        int total = abonadosService.getTotalAbonadosByAbonoId(abonoJuvilado.getId());
        Assert.assertEquals(total, 1);
    }

    @Test
    public void removeAbonado() {
        Abonado abonado =
            abonadosService.getAbonadosByAbonoId(abonoJuvilado.getId(), null, 0, Integer.MAX_VALUE).get(0);
        abonadosService.removeAbonado(abonado.getId(), usuario.getUsuario());

        List<Abonado> abonados = abonadosService.getAbonadosByAbonoId(abonoJuvilado.getId(), SORT, START, LIMIT);

        Assert.assertNotNull(abonados);
        Assert.assertEquals(abonados.size(), 0);

        int total = abonadosService.getTotalAbonadosByAbonoId(abonoJuvilado.getId());
        Assert.assertEquals(total, 0);
    }
}