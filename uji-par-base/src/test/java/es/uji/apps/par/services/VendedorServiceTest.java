package es.uji.apps.par.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.par.auth.Role;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.UsuarioYaExisteException;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Usuario;

@RunWith(SpringJUnit4ClassRunner.class)
public class VendedorServiceTest extends BaseServiceTest {
    @Autowired
    VendedoresService vendedoresService;

    @Autowired
    UsersService usersService;

    @Test
    public void subdominioDisponibleTest() {
        boolean available = vendedoresService.consultaSubdominioDisponible("test.com");
        Assert.assertFalse(available);

        available = vendedoresService.consultaSubdominioDisponible("test.es");
        Assert.assertTrue(available);
    }

    @Test(expected = CampoRequeridoException.class)
    public void addVendedorSinCamposRequeridosTest() {
        Usuario usuario = new Usuario();
        usuario.setNombre("Sergio");

        Sala sala = new Sala();
        sala.setNombre("Sala 1");

        Cine cine = new Cine();
        cine.setNombre("Cine 1");

        vendedoresService.addVendedor(usuario, cine, sala);
    }

    @Test(expected = UsuarioYaExisteException.class)
    public void addVendedorYaExistenteTest() {
        Usuario usuario = new Usuario();
        usuario.setNombre("Sergio");
        usuario.setMail("user1@test.com");
        usuario.setUsuario("user1");

        Sala sala = new Sala();
        sala.setNombre("Sala 1");

        Cine cine = new Cine();
        cine.setNombre("Cine 1");

        vendedoresService.addVendedor(usuario, cine, sala);
    }

    @Test
    public void addVendedorTest() {
        Usuario usuario = new Usuario();
        usuario.setNombre("Sergio");
        usuario.setMail("sergio@test.com");
        usuario.setUsuario("sergio");

        Sala sala = new Sala();
        sala.setNombre("Sala 1");

        Cine cine = new Cine();
        cine.setCodigo("cine1");
        cine.setNombre("Cine 1");

        Usuario vendedor = vendedoresService.addVendedor(usuario, cine, sala);
        Assert.assertNotNull(vendedor);
        Assert.assertNotNull(vendedor.getTokenValidacion());
        Assert.assertEquals(vendedor.getRole(), Role.ADMIN);
    }
}
