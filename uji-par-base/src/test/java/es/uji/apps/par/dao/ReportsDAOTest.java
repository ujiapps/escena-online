package es.uji.apps.par.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.ReportDTO;
import es.uji.apps.par.model.Report;
import es.uji.apps.par.report.EntradaReportFactory;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager")
@ContextConfiguration(locations = { "/applicationContext-db-test.xml" })
@Transactional
public class ReportsDAOTest
{
    @Autowired
    ReportsDAO reportsDAO;

    @PersistenceContext
    protected EntityManager entityManager;

    CineDTO cine;

    @Before
    public void setUp()
    {
        cine = new CineBuilder("Cine 1")
                .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void addReports()
    {
        List<Report> reports = Arrays.asList(
            new Report(cine.getId(), "es.uji.apps.par.report.InformeEfectivoReport", EntradaReportFactory.TIPO_INFORME_PDF_EFECTIVO),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeEventosReport", EntradaReportFactory.TIPO_INFORME_PDF_EVENTOS),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeIncidenciasReport", EntradaReportFactory.TIPO_INFORME_PDF_INCIDENCIAS),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeSesionNoAnuladasReport", EntradaReportFactory.TIPO_INFORME_PDF_SESIONES_NO_ANULADAS),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeSesionReport", EntradaReportFactory.TIPO_INFORME_PDF_SESIONES),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeTaquillaReport", EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeTaquillaTpvSubtotalesReport", EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA_TPV_SUBTOTALES));

        reportsDAO.addReports(reports);

        List<ReportDTO> reportsDTO = reportsDAO.getReportsByCine(cine.getId());
        Assert.assertEquals(reportsDTO.size(), reports.size());
    }
}
