package es.uji.apps.par.builders;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.model.Abono;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.utils.DateUtils;

public class AbonoBuilder {
    private Abono abono;
    private List<String> abonados;

    public AbonoBuilder(String nombre) {
        abono = new Abono();
        abono.setNombre(nombre);

        abonados = new ArrayList<>();
    }

    public AbonoBuilder withMaxEventos(int maxEventos) {
        abono.setMaxEventos(Long.valueOf(maxEventos));

        return this;
    }

    public AbonoBuilder withAbonado(String nombre) {
        abonados.add(nombre);

        return this;
    }

    public AbonoBuilder withPrecio(BigDecimal precio, LocalizacionDTO localizacion) {
        Date ahora = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, +10);
        Timestamp diezMinutosDespues = new Timestamp(calendar.getTimeInMillis());

        abono.setPrecio(precio);
        abono.setCanalInternet(Boolean.toString(true));
        abono.setFechaInicioVentaOnlineWithDate(ahora);
        abono.setHoraInicioVentaOnline(DateUtils.getHourAndMinutesWithLeadingZeros(ahora));
        abono.setFechaFinVentaOnlineWithDate(diezMinutosDespues);
        abono.setHoraFinVentaOnline(DateUtils.getHourAndMinutesWithLeadingZeros(diezMinutosDespues));
        abono.setParLocalizacion(Localizacion.localizacionDTOtoLocalizacion(localizacion));

        return this;
    }

    public Abono get() {
        return abono;
    }

    public TarifaDTO build(
        CineDTO cine,
        SalaDTO sala,
        TipoEventoDTO tipoEvento,
        TarifaDTO tarifa,
        EntityManager entityManager
    ) {
        TarifaDTO tarifaAbono = Abono.toTarifaDTO(abono, Cine.cineDTOToCine(cine, false));
        entityManager.persist(tarifaAbono);

        EventoDTO eventoAbono =
            new EventoBuilder(abono.getNombre(), abono.getNombre(), cine, tipoEvento).isAbono().build(entityManager);
        new SesionBuilder(abono.getNombre(), sala, eventoAbono, "10:00")
            .withPrecio(abono.getPrecio(), Localizacion.localizacionToLocalizacionDTO(abono.getParLocalizacion()), tarifa).withVentaOnline()
            .withCompras(abonados, BigDecimal.ONE, 1).build(entityManager);

        return tarifaAbono;
    }
}