package es.uji.apps.par.services;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.AbonoBuilder;
import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.MailBlacklistBuilder;
import es.uji.apps.par.builders.EventoBuilder;
import es.uji.apps.par.builders.LocalizacionBuilder;
import es.uji.apps.par.builders.PlantillaBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.SesionBuilder;
import es.uji.apps.par.builders.TarifaBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.builders.TpvBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.dao.PlantillasDAO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.PlantillaDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.db.TpvsDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.model.Butaca;

@ContextConfiguration(locations = {"/applicationContext-db-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class BaseServiceTest {
    protected static final String SORT = "[{\"property\":\"id\", \"direction\":\"ASC\"}]";
    protected static final int START = 0;
    protected static final int LIMIT = Integer.MAX_VALUE;

    final String NOMBRE_ABONO_JUVILADO = "Abono Juvilado";
    final String LOCALIZACION_CODE = "PL";
    final String NOMBRE_ABONADO = "alguien";
    final String EMAIL_ABONADO = String.format("%s@test.com", NOMBRE_ABONADO.toLowerCase());
    final String EMAIL_BLACKLIST = "blacklist@blacklist.com";

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    PlantillasDAO plantillasDAO;

    UsuarioDTO usuario;
    UsuarioDTO usuario2;
    UsuarioDTO usuarioSinCine;
    UsuarioDTO usuarioSinPermisoAnular;
    CineDTO cine;
    CineDTO cine2;
    SalaDTO sala;
    EventoDTO evento;
    EventoDTO eventoNoNumerado;
    EventoDTO eventoConLimiteEntradasPorEmail;
    SesionDTO sesionConVentaAnticipadaActiva;
    SesionDTO sesionConVentaAnticipadaCaducada;
    SesionDTO sesionSinVentaAnticipada;
    SesionDTO sesionConPlantillaConVentaAnticipadaActiva;
    SesionDTO sesionConPlantillaConVentaAnticipadaCaducada;
    SesionDTO sesionConPlantillaSinVentaAnticipada;
    SesionDTO sesionConPlantillaConCompras;
    SesionDTO sesionConCompras;
    SesionDTO sesion2;
    SesionDTO sesion3;
    SesionDTO sesion4;
    SesionDTO sesion1EventoLimiteEntradasPorEmail;
    SesionDTO sesion2EventoLimiteEntradasPorEmail;
    SesionDTO sesionSinPrecios;
    SesionDTO sesionConReservas;
    SesionDTO sesionEventoNoNumerado;
    LocalizacionDTO localizacion;
    LocalizacionDTO localizacion2;
    TarifaDTO abonoJuvilado;
    TarifaDTO abono2Usos;
    TarifaDTO tarifaTaquilla;
    TarifaDTO tarifaGeneral;
    TarifaDTO tarifaGeneralCine2;
    TipoEventoDTO tipoEvento;
    TpvsDTO tpv;

    protected List<Butaca> getButacasSeleccionadas(
        TarifaDTO tarifaDTO,
        String localizacionCode,
        int start
    ) {
        return Butaca.parseaJSON(String.format(
            "[{fila: \"1\", numero: \"%3$s\", tipo: \"%1$s\", localizacion: \"%2$s\"},{fila: \"1\", numero: \"%4$s\", tipo: \"%1$s\", localizacion: \"%2$s\"}]",
            tarifaDTO.getId(), localizacionCode, start, start + 1));
    }

    protected List<Butaca> getNButacasSeleccionadas(
        TarifaDTO tarifaDTO,
        String localizacionCode,
        int start,
        int n
    ) {
        List<String> butacas = new ArrayList<>();
        for (int i = start; i < start + n; i++) {
            butacas.add(String
                .format("{fila: \"1\", numero: \"%3$s\", tipo: \"%1$s\", localizacion: \"%2$s\"}", tarifaDTO.getId(),
                    localizacionCode, i));
        }
        return Butaca.parseaJSON("[" + StringUtils.join(butacas.iterator(), ",") + "]");
    }

    protected List<Butaca> getNButacasNoNumeradasSeleccionadas(
        TarifaDTO tarifaDTO,
        String localizacionCode,
        int n
    ) {
        List<String> butacas = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            butacas.add(String
                .format("{tipo: \"%1$s\", localizacion: \"%2$s\"}", tarifaDTO.getId(),
                    localizacionCode));
        }
        return Butaca.parseaJSON("[" + StringUtils.join(butacas.iterator(), ",") + "]");
    }

    @Before
    public void setUp() {
        new MailBlacklistBuilder(EMAIL_BLACKLIST).build(entityManager);

        cine = new CineBuilder("Cine 1").withAnularCompraDesdeMail(true).withComision(new BigDecimal(0.031)).withLimiteEntradas(10).build(entityManager);

        sala = new SalaBuilder("Sala 1", cine).build(entityManager);

        new TpvBuilder("Tpv de test").withSala(sala).build(entityManager);

        localizacion = new LocalizacionBuilder("platea", LOCALIZACION_CODE, BigDecimal.valueOf(200)).withSala(sala)
            .build(entityManager);

        tpv = new TpvBuilder("tpv test").withSala(sala).build(entityManager);

        tarifaTaquilla = new TarifaBuilder("Taquilla").build(cine, entityManager);
        tarifaGeneral = new TarifaBuilder("General").withPublic().build(cine, entityManager);

        tipoEvento = new TipoEventoBuilder("tipo", "tipo", false, cine).build(entityManager);

        abonoJuvilado =
            new AbonoBuilder(NOMBRE_ABONO_JUVILADO).withAbonado(NOMBRE_ABONADO).withPrecio(BigDecimal.ONE, localizacion)
                .build(cine, sala, tipoEvento, tarifaGeneral, entityManager);

        evento = new EventoBuilder("test", "test", cine, tipoEvento).build(entityManager);
        eventoConLimiteEntradasPorEmail =
            new EventoBuilder("testLimite", "testLimite", cine, tipoEvento).withLimiteEntradasPorEmail(15).build(entityManager);
        eventoNoNumerado = new EventoBuilder("test no numerado", "test no numerat", cine, tipoEvento).withAsientosNoNumerados().build(entityManager);

        PlantillaDTO plantilla2Precios = new PlantillaBuilder("Plantilla 2 precios", sala)
            .withPrecio(BigDecimal.valueOf(1), localizacion, tarifaTaquilla)
            .withPrecioAnticipado(BigDecimal.valueOf(20), BigDecimal.valueOf(10), localizacion, tarifaGeneral)
            .build(entityManager);

        PlantillaDTO plantilla3Precios = new PlantillaBuilder("Plantilla 3 precios", sala)
            .withPrecio(BigDecimal.valueOf(1), localizacion, abonoJuvilado)
            .withPrecio(BigDecimal.valueOf(2), localizacion, tarifaTaquilla)
            .withPrecioAnticipado(BigDecimal.valueOf(20), BigDecimal.valueOf(10), localizacion, tarifaGeneral)
            .build(entityManager);

        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.MINUTE, +10);
        Timestamp diezMinutosDespues = new Timestamp(calendar.getTimeInMillis());

        calendar.add(Calendar.MINUTE, -20);
        Timestamp diezMinutosAntes = new Timestamp(calendar.getTimeInMillis());

        sesionSinPrecios = new SesionBuilder("sesion sin precios", sala,
            eventoConLimiteEntradasPorEmail, "10:00").build(entityManager);

        sesion1EventoLimiteEntradasPorEmail = new SesionBuilder("test 1 límite entradas por email", sala,
            eventoConLimiteEntradasPorEmail, "10:00")
            .withPrecioAnticipado(BigDecimal.valueOf(20), BigDecimal.valueOf(10), localizacion, tarifaGeneral)
            .withVentaOnline().build(entityManager);

        sesion2EventoLimiteEntradasPorEmail = new SesionBuilder("test 2 límite entradas por email", sala,
            eventoConLimiteEntradasPorEmail, "12:00")
            .withPrecioAnticipado(BigDecimal.valueOf(20), BigDecimal.valueOf(10), localizacion, tarifaGeneral)
            .withVentaOnline().build(entityManager);

        sesionConVentaAnticipadaCaducada =
            new SesionBuilder("test diezMinutosAntes", sala, evento, "11:00").withFechaFinAnticipada(diezMinutosAntes)
                .withPrecio(BigDecimal.valueOf(1), localizacion, tarifaTaquilla)
                .withPrecioAnticipado(BigDecimal.valueOf(20), BigDecimal.valueOf(10), localizacion, tarifaGeneral)
                .withVentaOnline().build(entityManager);

        sesionConVentaAnticipadaActiva = new SesionBuilder("test diezMinutosDespues", sala, evento, "10:00")
            .withFechaFinAnticipada(diezMinutosDespues).withPrecio(BigDecimal.valueOf(1), localizacion, abonoJuvilado)
            .withPrecio(BigDecimal.valueOf(2), localizacion, tarifaTaquilla)
            .withPrecioAnticipado(BigDecimal.valueOf(20), BigDecimal.valueOf(10), localizacion, tarifaGeneral)
            .withVentaOnline().build(entityManager);

        sesionConVentaAnticipadaCaducada =
            new SesionBuilder("test diezMinutosAntes", sala, evento, "11:00").withFechaFinAnticipada(diezMinutosAntes)
                .withPrecio(BigDecimal.valueOf(1), localizacion, tarifaTaquilla)
                .withPrecioAnticipado(BigDecimal.valueOf(20), BigDecimal.valueOf(10), localizacion, tarifaGeneral)
                .withVentaOnline().build(entityManager);

        sesionSinVentaAnticipada = new SesionBuilder("test sin venta anticipada", sala, evento, "12:00")
            .withPrecio(BigDecimal.valueOf(1), localizacion, tarifaTaquilla)
            .withPrecioAnticipado(BigDecimal.valueOf(20), BigDecimal.valueOf(10), localizacion, tarifaGeneral)
            .withVentaOnline().build(entityManager);

        sesionConPlantillaConVentaAnticipadaActiva =
            new SesionBuilder("test diezMinutosDespues conplantilla", sala, evento, "13:00")
                .withFechaFinAnticipada(diezMinutosDespues).withPlantilla(plantilla3Precios).withVentaOnline()
                .build(entityManager);

        sesionConPlantillaConVentaAnticipadaCaducada =
            new SesionBuilder("test diezMinutosAntes conplantilla", sala, evento, "14:00")
                .withFechaFinAnticipada(diezMinutosAntes).withPlantilla(plantilla2Precios).withVentaOnline()
                .build(entityManager);

        sesionConPlantillaSinVentaAnticipada =
            new SesionBuilder("test sin venta anticipada conplantilla", sala, evento, "15:00")
                .withPlantilla(plantilla2Precios).withVentaOnline().build(entityManager);

        sesionConPlantillaConCompras = new SesionBuilder("test con compras", sala, evento, "18:00")
            .withPlantilla(plantilla2Precios)
            .withVentaOnline()
            .withCompra("Sergio", BigDecimal.valueOf(2), 2, tarifaGeneral, localizacion, "test-uuid-concompra1")
            .build(entityManager);

        sesionConCompras = new SesionBuilder("test con compras", sala, evento, "18:00")
            .withPrecio(BigDecimal.valueOf(1), localizacion, tarifaTaquilla)
            .withPrecioAnticipado(BigDecimal.valueOf(20), BigDecimal.valueOf(10), localizacion, tarifaGeneral)
            .withVentaOnline()
            .withCompra("Sergio", BigDecimal.valueOf(10), 2, tarifaGeneral, localizacion, "test-uuid-1")
            .withCompra("Luis", BigDecimal.valueOf(200), 10, tarifaGeneral, localizacion, "test-uuid-2")
            .withReserva(BigDecimal.valueOf(10), 2, tarifaGeneral, localizacion, "test-reserva-1", null)
            .withCompra("Gratis", BigDecimal.valueOf(0), 3, tarifaGeneral, localizacion, "test-uuid-gratis")
            .build(entityManager);

        sesionConReservas = new SesionBuilder("test con reservas", sala, evento, "18:00")
            .withPlantilla(plantilla2Precios)
            .withVentaOnline()
            .withReservaAnulada(BigDecimal.valueOf(10), 2, tarifaGeneral, localizacion, "test-reserva-anulada-1", null)
            .withReserva(BigDecimal.valueOf(10), 2, tarifaGeneral, localizacion, "test-reserva-25", null)
            .build(entityManager);

        sesionEventoNoNumerado = new SesionBuilder("test 1 no numerado", sala, eventoNoNumerado, "10:00")
            .withPrecioAnticipado(BigDecimal.valueOf(20), BigDecimal.valueOf(10), localizacion, tarifaGeneral)
            .withVentaOnline().build(entityManager);

        cine2 =
            new CineBuilder("Cine 2").withComision(new BigDecimal(0.04235)).build(entityManager);

        TipoEventoDTO tipoEvento2 = new TipoEventoBuilder("tipo 2", "tipo 2", false, cine2).build(entityManager);

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine2).build(entityManager);

        localizacion2 = new LocalizacionBuilder("platea", LOCALIZACION_CODE, BigDecimal.valueOf(200)).withSala(sala2)
            .build(entityManager);

        new TpvBuilder("Tpv de test").withSala(sala2).build(entityManager);

        tarifaGeneralCine2 = new TarifaBuilder("General").withPublic().build(cine2, entityManager);

        abono2Usos = new AbonoBuilder("Abono 2 Usos").withMaxEventos(2).withAbonado(NOMBRE_ABONADO)
            .withPrecio(BigDecimal.ONE, localizacion)
            .build(cine2, sala2, tipoEvento2, tarifaGeneralCine2, entityManager);

        EventoDTO evento2 = new EventoBuilder("Evento 2", "Esdeveniment 2", cine2, tipoEvento2).build(entityManager);

        sesion2 = new SesionBuilder("Sesión 2", sala2, evento2)
            .withPrecio(new BigDecimal(20), localizacion, tarifaGeneralCine2)
            .withPrecio(BigDecimal.valueOf(2), localizacion, abono2Usos).withVentaOnline().build(entityManager);

        EventoDTO evento3 = new EventoBuilder("Evento 3", "Esdeveniment 3", cine2, tipoEvento2).build(entityManager);

        sesion3 =
            new SesionBuilder("Sesión 3", sala2, evento3).withPrecio(BigDecimal.valueOf(4), localizacion, abono2Usos)
                .withVentaOnline().build(entityManager);

        EventoDTO evento4 = new EventoBuilder("Evento 4", "Esdeveniment 4", cine2, tipoEvento2).build(entityManager);

        sesion4 =
            new SesionBuilder("Sesión 4", sala2, evento4).withPrecio(BigDecimal.valueOf(6), localizacion, abono2Usos)
                .withVentaOnline().build(entityManager);

        usuario =
            new UsuarioBuilder("User 1", "user1@test.com", "user1").withURL("test.com").withSala(sala).withSala(sala2)
                .build(entityManager);

        usuarioSinPermisoAnular =
            new UsuarioBuilder("User 1", "user1sinpermiso@test.com", "user1sinpermisoanular").withSala(sala)
                .withSala(sala2).withPuedeAnular(false).build(entityManager);

        usuario2 = new UsuarioBuilder("User 2", "user2@test.com", "user2").withURL("test2.com").withSala(sala2)
            .build(entityManager);

        usuarioSinCine = new UsuarioBuilder("User SC", "user_sc@test.com", "user_sc").build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }
}
