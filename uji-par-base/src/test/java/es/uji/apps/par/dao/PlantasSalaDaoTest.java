package es.uji.apps.par.dao;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.model.PlantaSala;
import es.uji.apps.par.model.Sala;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager")
@ContextConfiguration(locations = { "/applicationContext-db-test.xml" })
public class PlantasSalaDaoTest extends BaseDAOTest
{
    @Autowired
    SalasDAO salasDao;

    @Autowired
    CinesDAO cinesDao;

    private SalaDTO sala;

    @Before
    public void before()
    {
        CineDTO cine = new CineBuilder("Cine 1")
            .build(entityManager);

        sala = new SalaBuilder("Sala 1", cine)
            .build(entityManager);
    }

    @Test
    @Transactional
    public void insertaUna()
    {
        salasDao.addPlanta(new PlantaSala("planta 1", Sala.salaDTOtoSala(sala)));

        List<PlantaSala> plantas = salasDao.getPlantas(sala.getId());

        Assert.assertEquals(1, plantas.size());
        Assert.assertEquals("planta 1", plantas.get(0).getNombre());
    }

}
