package es.uji.apps.par.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import es.uji.apps.par.model.PreciosSesion;

@RunWith(SpringJUnit4ClassRunner.class)
public class PreciosServiceTest extends BaseServiceTest {
    @Autowired
    PreciosService preciosService;

    @Test
    public void getPreciosSesionInternosDeSesionConVentaAnticipadaActiva() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesion(sesionConVentaAnticipadaActiva.getId(), usuario.getUsuario());
        Assert.assertEquals(3, preciosSesion.size());
        for (PreciosSesion precioSesion : preciosSesion) {
            Assert.assertEquals(precioSesion.getLocalizacion().getCodigo(), LOCALIZACION_CODE);
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecio());
            }
            else if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("on")) {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("2.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionInternosDeSesionConPlantillaConVentaAnticipadaActiva() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesion(sesionConPlantillaConVentaAnticipadaActiva.getId(), usuario.getUsuario());
        Assert.assertEquals(3, preciosSesion.size());
        for (PreciosSesion precioSesion : preciosSesion) {
            Assert.assertEquals(precioSesion.getLocalizacion().getCodigo(), LOCALIZACION_CODE);
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecio());
            }
            else if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("on")) {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("2.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionInternosDeSesionConVentaAnticipadaCaducada() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesion(sesionConVentaAnticipadaCaducada.getId(), usuario.getUsuario());
        Assert.assertEquals(2, preciosSesion.size());
        for (PreciosSesion precioSesion : preciosSesion) {
            Assert.assertEquals(LOCALIZACION_CODE, precioSesion.getLocalizacion().getCodigo());
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionInternosDeSesionConPlantillaConVentaAnticipadaCaducada() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesion(sesionConPlantillaConVentaAnticipadaCaducada.getId(), usuario.getUsuario());
        Assert.assertEquals(2, preciosSesion.size());
        for (PreciosSesion precioSesion : preciosSesion) {
            Assert.assertEquals(LOCALIZACION_CODE, precioSesion.getLocalizacion().getCodigo());
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionInternosDeSesionSinVentaAnticipada() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesion(sesionSinVentaAnticipada.getId(), usuario.getUsuario());
        Assert.assertEquals(2, preciosSesion.size());
        for (PreciosSesion precioSesion : preciosSesion) {
            Assert.assertEquals(LOCALIZACION_CODE, precioSesion.getLocalizacion().getCodigo());
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionInternosDeSesionConPlantillaSinVentaAnticipada() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesion(sesionConPlantillaSinVentaAnticipada.getId(), usuario.getUsuario());
        Assert.assertEquals(2, preciosSesion.size());
        for (PreciosSesion precioSesion : preciosSesion) {
            Assert.assertEquals(LOCALIZACION_CODE, precioSesion.getLocalizacion().getCodigo());
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionPublicoDeSesionConVentaAnticipadaActiva() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesionPublicos(sesionConVentaAnticipadaActiva.getId(), usuario.getUsuario());
        Assert.assertEquals(2, preciosSesion.size());
        for (PreciosSesion precioSesion : preciosSesion) {
            Assert.assertEquals(LOCALIZACION_CODE, precioSesion.getLocalizacion().getCodigo());
            if (precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionPublicoDeSesionConPlantillaConVentaAnticipadaActiva() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesionPublicos(sesionConPlantillaConVentaAnticipadaActiva.getId(), usuario.getUsuario());
        Assert.assertEquals(2, preciosSesion.size());
        for (PreciosSesion precioSesion : preciosSesion) {
            Assert.assertEquals(LOCALIZACION_CODE, precioSesion.getLocalizacion().getCodigo());
            if (precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionPublicoDeSesionConVentaAnticipadaCaducada() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesionPublicos(sesionConVentaAnticipadaCaducada.getId(), usuario.getUsuario());
        Assert.assertEquals(1, preciosSesion.size());
        Assert.assertEquals(new BigDecimal("20.00"), preciosSesion.get(0).getPrecio());
        Assert.assertEquals(LOCALIZACION_CODE, preciosSesion.get(0).getLocalizacion().getCodigo());
    }

    @Test
    public void getPreciosSesionPublicoDeSesionConPlantillaConVentaAnticipadaCaducada() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesionPublicos(sesionConPlantillaConVentaAnticipadaCaducada.getId(), usuario.getUsuario());
        Assert.assertEquals(1, preciosSesion.size());
        Assert.assertEquals(new BigDecimal("20.00"), preciosSesion.get(0).getPrecio());
        Assert.assertEquals(LOCALIZACION_CODE, preciosSesion.get(0).getLocalizacion().getCodigo());
    }

    @Test
    public void getPreciosSesionPublicoDeSesionSinVentaAnticipada() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesionPublicos(sesionSinVentaAnticipada.getId(), usuario.getUsuario());
        Assert.assertEquals(1, preciosSesion.size());
        Assert.assertEquals(new BigDecimal("20.00"), preciosSesion.get(0).getPrecio());
        Assert.assertEquals(LOCALIZACION_CODE, preciosSesion.get(0).getLocalizacion().getCodigo());
    }

    @Test
    public void getPreciosSesionPublicoDeSesionConPlantillaSinVentaAnticipada() {
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesionPublicos(sesionConPlantillaSinVentaAnticipada.getId(), usuario.getUsuario());
        Assert.assertEquals(1, preciosSesion.size());
        Assert.assertEquals(new BigDecimal("20.00"), preciosSesion.get(0).getPrecio());
        Assert.assertEquals(LOCALIZACION_CODE, preciosSesion.get(0).getLocalizacion().getCodigo());
    }

    @Test
    public void getTotalPreciosSesion() {
        int totalPreciosSesion = preciosService.getTotalPreciosSesion(sesionConVentaAnticipadaActiva.getId());
        Assert.assertEquals(3, totalPreciosSesion);
    }

    @Test
    public void getPreciosSesionPorLocalizacionDeSesionConVentaAnticipadaActiva() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionPorLocalizacion(sesionConVentaAnticipadaActiva.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(3, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());

        for (PreciosSesion precioSesion : preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values()) {
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecio());
            }
            else if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("on")) {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("2.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionPorLocalizacionDeSesionConPlantillaConVentaAnticipadaActiva() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionPorLocalizacion(sesionConPlantillaConVentaAnticipadaActiva.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(3, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());

        for (PreciosSesion precioSesion : preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values()) {
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecio());
            }
            else if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("on")) {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("2.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionNormalPublicosPorLocalizacionDeSesionConVentaAnticipadaActiva() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionNormalPublicosPorLocalizacion(sesionConVentaAnticipadaActiva.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());
        PreciosSesion precioSesion = preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values().iterator().next();
        Assert.assertNotEquals("on", precioSesion.getTarifa().getIsAbono());
        Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecio());
    }

    @Test
    public void getPreciosSesionAbonoPublicosPorLocalizacionDeSesionConVentaAnticipadaActiva() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionAbonosPublicosPorLocalizacion(sesionConVentaAnticipadaActiva.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());
        PreciosSesion precioSesion = preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values().iterator().next();
        Assert.assertEquals("on", precioSesion.getTarifa().getIsAbono());
        Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
    }

    @Test
    public void getPreciosSesionNormalPublicosPorLocalizacionDeSesionConPlantillaConVentaAnticipadaActiva() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionNormalPublicosPorLocalizacion(sesionConPlantillaConVentaAnticipadaActiva.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());
        PreciosSesion precioSesion = preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values().iterator().next();
        Assert.assertNotEquals("on", precioSesion.getTarifa().getIsAbono());
        Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecio());
    }

    @Test
    public void getPreciosSesionAbonosPublicosPorLocalizacionDeSesionConPlantillaConVentaAnticipadaActiva() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionAbonosPublicosPorLocalizacion(sesionConPlantillaConVentaAnticipadaActiva.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());
        PreciosSesion precioSesion = preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values().iterator().next();
        Assert.assertEquals("on", precioSesion.getTarifa().getIsAbono());
        Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
    }

    @Test
    public void getPreciosSesionPorLocalizacionDeSesionConVentaAnticipadaCaducada() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionPorLocalizacion(sesionConVentaAnticipadaCaducada.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(2, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());

        for (PreciosSesion precioSesion : preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values()) {
            if (precioSesion.getTarifa().getIsPublica().equals("on")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionPorLocalizacionDeSesionConPlantillaConVentaAnticipadaCaducada() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionPorLocalizacion(sesionConPlantillaConVentaAnticipadaCaducada.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(2, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());

        for (PreciosSesion precioSesion : preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values()) {
            if (precioSesion.getTarifa().getIsPublica().equals("on")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionPublicosPorLocalizacionDeSesionConVentaAnticipadaCaducada() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionNormalPublicosPorLocalizacion(sesionConVentaAnticipadaCaducada.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());

        for (PreciosSesion precioSesion : preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values()) {
            Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
        }
    }

    @Test
    public void getPreciosSesionPublicosPorLocalizacionDeSesionConPlantillaConVentaAnticipadaCaducada() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionNormalPublicosPorLocalizacion(sesionConPlantillaConVentaAnticipadaCaducada.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());

        for (PreciosSesion precioSesion : preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values()) {
            Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
        }
    }

    @Test
    public void getPreciosSesionPorLocalizacionDeSesionSinVentaAnticipada() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionPorLocalizacion(sesionSinVentaAnticipada.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(2, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());

        for (PreciosSesion precioSesion : preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values()) {
            if (precioSesion.getTarifa().getIsPublica().equals("on")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionPorLocalizacionDeSesionConPlantillaSinVentaAnticipada() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionPorLocalizacion(sesionConPlantillaSinVentaAnticipada.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(2, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());

        for (PreciosSesion precioSesion : preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values()) {
            if (precioSesion.getTarifa().getIsPublica().equals("on")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
            }
            else {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
            }
        }
    }

    @Test
    public void getPreciosSesionPublicosPorLocalizacionDeSesionSinVentaAnticipada() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionNormalPublicosPorLocalizacion(sesionSinVentaAnticipada.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());

        for (PreciosSesion precioSesion : preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values()) {
            Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
        }
    }

    @Test
    public void getPreciosSesionPublicosPorLocalizacionDeSesionConPlantillaSinVentaAnticipada() {
        Map<String, Map<Long, PreciosSesion>> preciosSesionPublicosPorLocalizacion =
            preciosService.getPreciosSesionNormalPublicosPorLocalizacion(sesionConPlantillaSinVentaAnticipada.getId(), usuario.getUsuario());

        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.size());
        Assert.assertEquals(1, preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).size());

        for (PreciosSesion precioSesion : preciosSesionPublicosPorLocalizacion.get(LOCALIZACION_CODE).values()) {
            Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
        }
    }
}