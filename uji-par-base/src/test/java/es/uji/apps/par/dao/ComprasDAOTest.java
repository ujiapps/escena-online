package es.uji.apps.par.dao;

import com.mysema.query.Tuple;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.uji.apps.par.builders.EventoBuilder;
import es.uji.apps.par.builders.LocalizacionBuilder;
import es.uji.apps.par.builders.SesionBuilder;
import es.uji.apps.par.builders.TarifaBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.enums.TipoPago;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.services.ComprasService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager")
@ContextConfiguration(locations = {"/applicationContext-db-test.xml"})
@Transactional
public class ComprasDAOTest extends BaseDAOTest {
    @Autowired
    ClientesDAO clientesDAO;

    @Autowired
    ComprasDAO comprasDAO;

    @Autowired
    ButacasDAO butacasDAO;

    @Autowired
    ComprasService comprasService;

    LocalizacionDTO localizacion;
    SesionDTO sesion;
    TarifaDTO tarifaGeneral;
    EventoDTO evento;

    @Before
    public void before() {
        localizacion = new LocalizacionBuilder("anfiteatro", "ANFITEATRO", BigDecimal.valueOf(100)).withSala(sala1)
            .build(entityManager);

        tarifaGeneral = new TarifaBuilder("General").withPublic().build(cine1, entityManager);

        TipoEventoDTO tipoEvento =
            new TipoEventoBuilder("tipo evento", "tipo esdeveniment", false, cine1).build(entityManager);

        evento = new EventoBuilder("test tit es", "test tit ca", cine1, tipoEvento).withTpv(tpv).build(entityManager);

        sesion = new SesionBuilder("test", sala1, evento).withVentaOnline()
            .withPrecio(BigDecimal.valueOf(10), localizacion, tarifaGeneral).build(entityManager);
    }

    @Test
    public void guardaCompraOk() {
        CompraDTO compraDTO = comprasDAO
            .insertaCompra(sesion.getId(), new Date(), true, new BigDecimal(1.04), new BigDecimal(1),
                new BigDecimal(0.035), null, usuario1.getUsuario());

        assertNotNull(compraDTO.getId());
    }

    @Test
    public void testGetRecaudacionSinCompras() {
        assertEquals(new BigDecimal(0).doubleValue(),
            comprasDAO.getRecaudacionSesiones(Arrays.asList(Sesion.SesionDTOToSesion(sesion)), true).doubleValue(),
            0.00001);
    }

    @Test
    public void testGetRecaudacion() {
        CompraDTO compra1 = comprasDAO
            .insertaCompra(sesion.getId(), new Date(), true, new BigDecimal(1.04), new BigDecimal(1),
                new BigDecimal(0.035), null, usuario1.getUsuario());
        ButacaDTO butaca1 = preparaButaca(sesion, localizacion, "1", "2", new BigDecimal(1.04), new BigDecimal(1));

        butaca1.setParCompra(compra1);
        butacasDAO.addButaca(butaca1);
        comprasDAO.marcarPagada(compra1.getId(), TipoPago.TARJETA);

        CompraDTO compra2 = comprasDAO
            .insertaCompra(sesion.getId(), new Date(), true, new BigDecimal(1.05), new BigDecimal(1),
                new BigDecimal(0.05), null, usuario1.getUsuario());
        ButacaDTO butaca2 = preparaButaca(sesion, localizacion, "3", "4", new BigDecimal(1.05), new BigDecimal(1));
        butaca2.setParCompra(compra2);
        butacasDAO.addButaca(butaca2);
        comprasDAO.marcarPagada(compra2.getId(), TipoPago.TARJETA);

        CompraDTO compra3 = comprasDAO
            .insertaCompra(sesion.getId(), new Date(), true, new BigDecimal(1.05), new BigDecimal(1),
                new BigDecimal(0.05), null, usuario1.getUsuario());
        ButacaDTO butaca3 = preparaButaca(sesion, localizacion, "4", "5", new BigDecimal(1.05), new BigDecimal(1));
        butaca3.setParCompra(compra3);
        butacasDAO.addButaca(butaca3);

        assertEquals(new BigDecimal(2.09).doubleValue(),
            comprasDAO.getRecaudacionSesiones(Arrays.asList(Sesion.SesionDTOToSesion(sesion)), true).doubleValue(),
            0.00001);
    }

    @Test
    public void testGetRecaudacionVariasSesiones() {
        CompraDTO compra1 = comprasDAO
            .insertaCompra(sesion.getId(), new Date(), true, new BigDecimal(1.04), new BigDecimal(1),
                new BigDecimal(0.035), null, usuario1.getUsuario());
        ButacaDTO butaca1 = preparaButaca(sesion, localizacion, "1", "2", new BigDecimal(1.04), new BigDecimal(1));
        butaca1.setParCompra(compra1);
        butacasDAO.addButaca(butaca1);
        comprasDAO.marcarPagada(compra1.getId(), TipoPago.TARJETA);

        SesionDTO sesion2 = new SesionBuilder("test", sala1, evento).withVentaOnline()
            .withPrecio(BigDecimal.valueOf(10), localizacion, tarifaGeneral).build(entityManager);

        CompraDTO compra2 = comprasDAO
            .insertaCompra(sesion2.getId(), new Date(), true, new BigDecimal(1.05), new BigDecimal(1),
                new BigDecimal(0.05), null, usuario1.getUsuario());
        ButacaDTO butaca2 = preparaButaca(sesion2, localizacion, "1", "2", new BigDecimal(1.05), new BigDecimal(1));
        butaca2.setParCompra(compra2);
        butacasDAO.addButaca(butaca2);
        comprasDAO.marcarPagada(compra2.getId(), TipoPago.TARJETA);

        CompraDTO compra3 = comprasDAO
            .insertaCompra(sesion2.getId(), new Date(), true, new BigDecimal(2.10), new BigDecimal(2),
                new BigDecimal(0.05), null, usuario1.getUsuario());
        ButacaDTO butaca3 = preparaButaca(sesion2, localizacion, "3", "4", new BigDecimal(2.10), new BigDecimal(2));
        butaca3.setParCompra(compra3);
        butacasDAO.addButaca(butaca3);
        comprasDAO.marcarPagada(compra3.getId(), TipoPago.TARJETA);

        CompraDTO compra4NoPagada = comprasDAO
            .insertaCompra(sesion2.getId(), new Date(), true, new BigDecimal(2.10), new BigDecimal(2),
                new BigDecimal(0.05), null, usuario1.getUsuario());
        ButacaDTO butaca4 = preparaButaca(sesion2, localizacion, "4", "5", new BigDecimal(2.10), new BigDecimal(2));
        butaca4.setParCompra(compra4NoPagada);
        butacasDAO.addButaca(butaca4);

        assertEquals(new BigDecimal(4.19).doubleValue(), comprasDAO
            .getRecaudacionSesiones(Arrays.asList(Sesion.SesionDTOToSesion(sesion), Sesion.SesionDTOToSesion(sesion2)),
                true).doubleValue(), 0.00001);
    }

    @Test
    public void testGetQueryComprasBySesion() {
        CompraDTO compraDTO = comprasDAO
            .insertaCompra(sesion.getId(), new Date(), true, new BigDecimal(1.04), new BigDecimal(1),
                new BigDecimal(0.035), null, usuario1.getUsuario());
        ButacaDTO butaca1 = preparaButaca(sesion, localizacion, "1", "2", new BigDecimal(1.04), new BigDecimal(1));
        butaca1.setParCompra(compraDTO);
        butacasDAO.addButaca(butaca1);

        assertNotNull(compraDTO.getId());

        List<Tuple> tuples = comprasDAO.getComprasBySesion(sesion.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(tuples);
    }

    @Test
    public void getClientes() {
        final String MAIL = "test@test.com";

        CompraDTO compraDTO = comprasDAO
            .insertaCompra(sesion.getId(), new Date(), true, new BigDecimal(1.04), new BigDecimal(1),
                new BigDecimal(0.035), null, usuario1.getUsuario());
        ButacaDTO butaca1 = preparaButaca(sesion, localizacion, "1", "2", new BigDecimal(1.04), new BigDecimal(1));

        compraDTO.setInfoPeriodica(true);
        compraDTO.setEmail(MAIL);

        butaca1.setParCompra(compraDTO);
        butacasDAO.addButaca(butaca1);

        List<Tuple> clientes = clientesDAO.getClientes("nombre", 0, 10, null, usuario1.getUsuario());

        assertNotNull(clientes);
        assertEquals(clientes.size(), clientesDAO.getTotalClientes(null, usuario1.getUsuario()));
        assertTrue(clientes.get(0).get(8, String.class).equals(MAIL));
    }

    @Test
    public void noCaducaComprasPendientesMenorATiempoLimite() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -10);
        Timestamp fecha10minutosAntes = new Timestamp(calendar.getTimeInMillis());

        new SesionBuilder("test compra pendiente", sala1, evento, "18:00")
            .withCompra("Sergio", BigDecimal.valueOf(10), 2, tarifaGeneral, localizacion, "test-pendiente-10", fecha10minutosAntes, false)
            .build(entityManager);

        CompraDTO compraDTO = comprasService.getCompraByUuid("test-pendiente-10");

        comprasService.eliminaPendientes();
        CompraDTO compraById = comprasService.getCompraById(compraDTO.getId());

        Assert.assertEquals(false, compraById.getCaducada());
    }

    @Test
    public void caducaComprasPendientesMayorTiempoLimite() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -36);
        Timestamp fecha36minutosAntes = new Timestamp(calendar.getTimeInMillis());

        new SesionBuilder("test compra pendiente", sala1, evento, "18:00")
            .withCompra("Sergio", BigDecimal.valueOf(10), 2, tarifaGeneral, localizacion, "test-pendiente-36", fecha36minutosAntes, false)
            .build(entityManager);

        CompraDTO compraDTO = comprasService.getCompraByUuid("test-pendiente-36");

        comprasService.eliminaPendientes();
        CompraDTO compraById = comprasService.getCompraById(compraDTO.getId());

        Assert.assertEquals(true, compraById.getCaducada());
    }
}
