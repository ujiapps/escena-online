package es.uji.apps.par.builders;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.model.Tarifa;

public class TarifaBuilder
{
	private TarifaDTO tarifa;

	public TarifaBuilder(String nombre)
	{
		tarifa = new TarifaDTO();
		tarifa.setIsAbono(false);
		tarifa.setNombre(nombre);
	}

	public TarifaBuilder withPublic() {
		tarifa.setIsPublica(true);
		tarifa.setDefecto(true);

		return this;
	}

	public Tarifa get() {
		return Tarifa.tarifaDTOToTarifa(this.tarifa);
	}

	public TarifaDTO build(CineDTO cine, EntityManager entityManager)
	{
        tarifa.setParCine(cine);
		entityManager.persist(tarifa);

		return tarifa;
	}
}