package es.uji.apps.par.ficheros.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.TpvBuilder;
import es.uji.apps.par.dao.CinesDAO;
import es.uji.apps.par.dao.EventosDAO;
import es.uji.apps.par.dao.LocalizacionesDAO;
import es.uji.apps.par.dao.PlantillasDAO;
import es.uji.apps.par.dao.PreciosPlantillaDAO;
import es.uji.apps.par.dao.SalasDAO;
import es.uji.apps.par.dao.SesionesDAO;
import es.uji.apps.par.dao.TarifasDAO;
import es.uji.apps.par.dao.TiposEventosDAO;
import es.uji.apps.par.dao.UsuariosDAO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.enums.TipoPago;
import es.uji.apps.par.exceptions.ButacaOcupadaException;
import es.uji.apps.par.exceptions.CompraSinButacasException;
import es.uji.apps.par.exceptions.IncidenciaNotFoundException;
import es.uji.apps.par.exceptions.NoHayButacasLibresException;
import es.uji.apps.par.exceptions.PrecioRepetidoException;
import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.CompraRequest;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.model.Plantilla;
import es.uji.apps.par.model.PreciosEditablesSesion;
import es.uji.apps.par.model.PreciosPlantilla;
import es.uji.apps.par.model.ResultadoCompra;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.Tarifa;
import es.uji.apps.par.model.TipoEvento;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.ComprasService;
import es.uji.apps.par.services.SesionesService;
import es.uji.apps.par.utils.DateUtils;

@Transactional
public class FicherosServiceBaseTest
{
    @Autowired
    private CinesDAO cinesDao;

    @Autowired
    private SalasDAO salasDao;

    @Autowired
    private EventosDAO eventosDAO;

    @Autowired
    private TiposEventosDAO tiposEventoDAO;

    @Autowired
    private SesionesDAO sesionesDAO;

    @Autowired
    private PlantillasDAO plantillasDAO;

    @Autowired
    private LocalizacionesDAO localizacionesDAO;

    @Autowired
    private PreciosPlantillaDAO preciosPlantillaDAO;

	@Autowired
	private TarifasDAO tarifasDAO;

    @Autowired
    private ComprasService comprasService;

    @Autowired
    private SesionesService sesionesService;

	@Autowired
	private UsuariosDAO usuariosDAO;

    @PersistenceContext
    protected EntityManager entityManager;

    protected Cine cine;
    protected Localizacion localizacion;
    protected Plantilla plantilla;
    protected PreciosEditablesSesion precioSesion;
    protected Sala sala;
    protected TipoEvento tipoEvento;
    protected Evento evento;
	protected Tarifa tarifa;
	protected Usuario usuario;

    protected void setup() throws PrecioRepetidoException {
		usuario = creaUsuario();
        cine = creaCine();
        localizacion = creaLocalizacion("Platea");
        sala = creaSala("567", "Sala 1");

        SalaDTO salaDTO = new SalaDTO();
        salaDTO.setId(sala.getId());
        new TpvBuilder("ejemplo")
        .withSala(salaDTO)
        .build(entityManager);

        entityManager.flush();
        entityManager.clear();

        addSalaUsuario(sala, usuario);
        tipoEvento = creaTipoEvento();
        plantilla = creaPlantilla();
		tarifa = creaTarifa();
        PreciosPlantilla precioPlantilla = creaPrecioPlantilla(1.10);
        precioSesion = creaPrecioSesion(precioPlantilla);
        evento = creaEvento(tipoEvento);
    }

	protected void addSalaUsuario(Sala sala, Usuario usuario)
	{
		usuariosDAO.addSalaUsuario(sala, usuario);
	}

	private Usuario creaUsuario() {
		Usuario us = new Usuario();
		us.setUsuario("login");
		us.setMail("mail");
		us.setNombre("nombre");

		return usuariosDAO.addUser(us);
	}

	private Tarifa creaTarifa() {
		Tarifa tarifa = new Tarifa();
		tarifa.setNombre("tarifa1");
		tarifa.setDefecto("true");
		TarifaDTO tarifaDTO = Tarifa.toDTO(tarifa);
		tarifasDAO.add(tarifaDTO);
		tarifa.setId(tarifaDTO.getId());
		return tarifa;
	}

	protected void registraCompra(Sesion sesion1, String userUID, Butaca... butacas) throws NoHayButacasLibresException,
			ButacaOcupadaException, CompraSinButacasException, IncidenciaNotFoundException {
        CompraRequest compraRequest = new CompraRequest();
        compraRequest.setButacasSeleccionadas(Arrays.asList(butacas));
        ResultadoCompra resultado1 = comprasService.registraCompraTaquilla(sesion1.getId(), compraRequest, userUID);
        comprasService.marcaPagada(resultado1.getId(), TipoPago.METALICO);
    }

    protected Sesion creaSesion(Sala sala, Evento evento, String fecha, String hora, String userUID) throws ParseException
    {
        Sesion sesion = new Sesion();
        sesion.setFechaCelebracionWithDate(DateUtils.spanishStringWithHourstoDate(fecha + " " + hora));
        sesion.setFechaInicioVentaOnline("1/12/2011");
        sesion.setFechaFinVentaOnline("11/12/2012");
        sesion.setEvento(evento);
        sesion.setSala(sala);
        sesion.setPlantillaPrecios(plantilla);
        sesion.setPreciosSesion(Arrays.asList(precioSesion));
        sesion.setVersionLinguistica("1");
        sesion.getEvento().setFormato("3");

        return sesionesService.creaSesionConAnulacionDeSesionesMismaHora(sesion, true, userUID);
    }

    protected Sesion creaSesion(Sala sala, Evento evento, String hora, String userUID) throws ParseException
    {
        return creaSesion(sala, evento, "11/12/2013", hora, userUID);
    }

	protected Sesion creaSesion(Sala sala, Evento evento, String userUID) throws ParseException
	{
		return creaSesion(sala, evento, "11/12/2013", "22:00", userUID);
	}

    protected PreciosEditablesSesion creaPrecioSesion(PreciosPlantilla precioPlantilla)
    {
        PreciosEditablesSesion precioSesion = new PreciosEditablesSesion(precioPlantilla);

        return sesionesDAO.addPrecioSesion(PreciosEditablesSesion.precioSesionToPrecioSesionDTO(precioSesion));
    }

    protected Plantilla creaPlantilla()
    {
        Plantilla plantilla = new Plantilla("test");
		plantilla.setSala(sala);

		return plantillasDAO.add(plantilla);
    }

    protected Evento creaEvento(TipoEvento tipoEvento)
    {
        return creaEvento(tipoEvento, "1a", "2a", "3a", "4a", "5", "6");
    }

    protected Evento creaEvento(TipoEvento tipoEvento, String expediente, String titulo, String codigoDistribuidora,
            String nombreDistribuidora, String vo, String subtitulos)
    {
        Evento evento = new Evento();
        evento.setTipoEvento(tipoEvento.getId());
        evento.setExpediente(expediente);
        evento.setTituloEs(titulo);
        evento.setCodigoDistribuidora(codigoDistribuidora);
        evento.setNombreDistribuidora(nombreDistribuidora);
        evento.setVo(vo);
        evento.setSubtitulos(subtitulos);
		evento.setFormato("3");
		evento.setCine(cine);

        return eventosDAO.addEvento(evento, usuario.getUsuario());
    }

    protected TipoEvento creaTipoEvento()
    {
        TipoEvento tipoEvento = new TipoEvento();
        tipoEvento.setNombreEs("Cine");
        tipoEvento.setNombreVa("Cinema");

        return tiposEventoDAO.addTipoEvento(tipoEvento);
    }

    protected Cine creaCine()
    {
        Cine cine = new Cine();
        cine.setCodigo("123");
        cine.setCodigoIcaa("123");
        return cinesDao.addCine(cine);
    }

    protected Sala creaSala(String codigo, String nombre)
    {
        Sala sala = new Sala();
        sala.setCodigo(codigo);
        sala.setNombre(nombre);
        sala.setCine(cine);

        return salasDao.addSala(sala);
    }

    protected Localizacion creaLocalizacion(String nombre)
    {
        Localizacion localizacion = new Localizacion(nombre);
        localizacion.setCodigo(nombre);

        return localizacionesDAO.add(localizacion);
    }

    protected Butaca creaButaca(String fila, String numero)
    {
        Butaca butaca = new Butaca();

        butaca.setLocalizacion("Platea");
        butaca.setFila(fila);
        butaca.setNumero(numero);
        butaca.setTipo(String.valueOf(tarifa.getId()));

        return butaca;
    }

    protected PreciosPlantilla creaPrecioPlantilla(double normal) throws PrecioRepetidoException {
        PreciosPlantilla preciosPlantilla = new PreciosPlantilla(localizacion, plantilla);
        preciosPlantilla.setPrecio(new BigDecimal(normal));
		preciosPlantilla.setTarifa(tarifa);

        return preciosPlantillaDAO.add(preciosPlantilla);
    }
}
