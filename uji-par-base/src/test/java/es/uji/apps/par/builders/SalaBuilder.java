package es.uji.apps.par.builders;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.model.Sala;

public class SalaBuilder {
    private SalaDTO sala;

    public SalaBuilder(
        String nombre,
        CineDTO cine
    ) {
        sala = new SalaDTO();
        sala.setNombre(nombre);
        sala.setParCine(cine);
    }

    public SalaBuilder withClaseEntradaTaquilla(String claseEntradaTaquilla) {
        sala.setClaseEntradaTaquilla(claseEntradaTaquilla);

        return this;
    }

    public SalaBuilder withClaseEntradaOnline(String claseEntradaOnline) {
        sala.setClaseEntradaOnline(claseEntradaOnline);

        return this;
    }

    public Sala get() {
        return Sala.salaDTOtoSala(this.sala);
    }

    public SalaDTO build(EntityManager entityManager) {
        entityManager.persist(sala);

        return sala;
    }
}