package es.uji.apps.par.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import es.uji.apps.par.builders.AbonoBuilder;
import es.uji.apps.par.dao.PlantillasDAO;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.EventoConCompras;
import es.uji.apps.par.exceptions.TarifaYaExisteException;
import es.uji.apps.par.exceptions.UsuarioNoExisteException;
import es.uji.apps.par.exceptions.UsuarioObligatorioException;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.model.Abono;
import es.uji.apps.par.model.Evento;

@RunWith(SpringJUnit4ClassRunner.class)
public class AbonosServiceTest extends BaseServiceTest {
    @Autowired
    AbonosService abonosService;

    @Autowired
    EventosService eventosService;

    @Autowired
    PlantillasDAO plantillasDAO;

    @Test
    public void getAbonos() {
        List<Abono> abonos = abonosService.getAll(SORT, START, LIMIT, usuario.getUsuario());

        Assert.assertNotNull(abonos);
        Assert.assertEquals(2, abonos.size());

        long total = abonosService.getTotal(usuario.getUsuario());
        Assert.assertEquals(2, total);
    }

    @Test(expected = CampoRequeridoException.class)
    public void addAbonoSinNombre() throws IOException {
        Abono nuevoAbono = new AbonoBuilder("").withPrecio(BigDecimal.valueOf(2), localizacion).get();
        abonosService.add(nuevoAbono, usuario.getUsuario());
    }

    @Test(expected = CampoRequeridoException.class)
    public void addAbonoSinAforo() throws IOException {
        Abono nuevoAbono = new AbonoBuilder("Abono de prueba").withPrecio(BigDecimal.valueOf(2), localizacion).get();
        nuevoAbono.setParLocalizacion(null);
        abonosService.add(nuevoAbono, usuario.getUsuario());
    }

    @Test(expected = CampoRequeridoException.class)
    public void addAbonoSinPrecio() throws IOException {
        Abono nuevoAbono = new AbonoBuilder("Abono de prueba").withPrecio(BigDecimal.valueOf(2), localizacion).get();
        nuevoAbono.setPrecio(null);
        abonosService.add(nuevoAbono, usuario.getUsuario());
    }

    @Test(expected = CampoRequeridoException.class)
    public void addAbonoSinFechaInicio() throws IOException {
        Abono nuevoAbono = new AbonoBuilder("Abono de prueba").withPrecio(BigDecimal.valueOf(2), localizacion).get();
        nuevoAbono.setFechaInicioVentaOnlineWithDate(null);
        abonosService.add(nuevoAbono, usuario.getUsuario());
    }

    @Test(expected = CampoRequeridoException.class)
    public void addAbonoSinFechaFin() throws IOException {
        Abono nuevoAbono = new AbonoBuilder("Abono de prueba").withPrecio(BigDecimal.valueOf(2), localizacion).get();
        nuevoAbono.setFechaFinVentaOnlineWithDate(null);
        abonosService.add(nuevoAbono, usuario.getUsuario());
    }

    @Test(expected = UsuarioObligatorioException.class)
    public void addAbonoSinUsuario() throws IOException {
        Abono nuevoAbono = new AbonoBuilder("Abono test").withPrecio(BigDecimal.valueOf(2), localizacion).get();
        abonosService.add(nuevoAbono, null);
    }

    @Test(expected = UsuarioNoExisteException.class)
    public void addAbonoConUsuarioSinCine() throws IOException {
        Abono nuevoAbono = new AbonoBuilder("Abono test").withPrecio(BigDecimal.valueOf(2), localizacion).get();
        abonosService.add(nuevoAbono, usuarioSinCine.getUsuario());
    }

    @Test(expected = TarifaYaExisteException.class)
    public void addAbonoNombreDuplicado() throws IOException {
        Abono nuevoAbono = new AbonoBuilder("Abono Juvilado").withPrecio(BigDecimal.valueOf(2), localizacion).get();
        abonosService.add(nuevoAbono, usuario.getUsuario());
    }

    @Test
    public void addAbonoNombreDuplicadoDiferenteUsuario() throws ParseException, IOException {
        final String NOMBRE_ABONO = "Abono Juvilado";
        Abono nuevoAbono = new AbonoBuilder(NOMBRE_ABONO).withPrecio(BigDecimal.valueOf(2), localizacion2).get();
        abonosService.add(nuevoAbono, usuario2.getUsuario());

        ExtGridFilterList extGridFilterList = ExtGridFilterList.fromString(String.format("[{\"property\": \"tituloEs\", \"operator\": \"=\", \"value\":\"%s\"}]", NOMBRE_ABONO));
        List<Evento> eventos = eventosService
            .getAbonos(SORT, 0, Integer.MAX_VALUE, extGridFilterList, usuario2.getUsuario(), cine2.getId());
        Assert.assertEquals(1, eventos.size());
        Assert.assertEquals(true, eventos.get(0).getAbono());
    }

    @Test
    public void addAbono() throws ParseException, IOException {
        entityManager.flush();
        entityManager.clear();
        final String NOMBRE_ABONO = "Abono Nuevo";
        Abono nuevoAbono = new AbonoBuilder(NOMBRE_ABONO).withPrecio(BigDecimal.valueOf(2), localizacion).get();
        abonosService.add(nuevoAbono, usuario.getUsuario());
        ExtGridFilterList extGridFilterList = ExtGridFilterList.fromString(String.format("[{\"property\": \"tituloEs\", \"operator\": \"=\", \"value\":\"%s\"}]", NOMBRE_ABONO));
        List<Evento> eventos = eventosService
            .getAbonos(SORT, 0, Integer.MAX_VALUE, extGridFilterList, usuario.getUsuario(), cine.getId());
        Assert.assertEquals(1, eventos.size());
        Assert.assertEquals(true, eventos.get(0).getAbono());
    }

    @Test(expected = TarifaYaExisteException.class)
    public void updateAbonoConNombreDuplicado() throws IOException {
        Abono nuevoAbono = new AbonoBuilder("Abono Nuevo").withPrecio(BigDecimal.valueOf(2), localizacion).get();
        nuevoAbono = abonosService.add(nuevoAbono, usuario.getUsuario());
        nuevoAbono.setNombre("Abono Juvilado");
        abonosService.update(nuevoAbono, usuario.getUsuario());
    }

    @Test(expected = EventoConCompras.class)
    public void updateAbonoConCompras() throws IOException {
        final String NOMBRE_MODIFICADO = "Abono Modificado";

        Abono abono = abonosService.getById(abonoJuvilado.getId());
        abono.setNombre(NOMBRE_MODIFICADO);
        abonosService.update(abono, usuario.getUsuario());
    }

    @Test
    public void updateAbono() throws ParseException, IOException {
        final String NOMBRE_MODIFICADO = "Abono Modificado";

        Abono nuevoAbono = new AbonoBuilder("Abono Nuevo").withPrecio(BigDecimal.valueOf(2), localizacion).get();
        nuevoAbono = abonosService.add(nuevoAbono, usuario.getUsuario());
        nuevoAbono.setNombre(NOMBRE_MODIFICADO);
        Abono abonoModificado = abonosService.update(nuevoAbono, usuario.getUsuario());

        Assert.assertNotNull(abonoModificado);
        Assert.assertEquals(nuevoAbono.getId(), abonoModificado.getId());
        Assert.assertEquals(NOMBRE_MODIFICADO, abonoModificado.getNombre());

        ExtGridFilterList extGridFilterList = ExtGridFilterList.fromString(String.format("[{\"property\": \"tituloEs\", \"operator\": \"=\", \"value\":\"%s\"}]", NOMBRE_MODIFICADO));
        List<Evento> eventos = eventosService
            .getAbonos(SORT, 0, Integer.MAX_VALUE, extGridFilterList, usuario.getUsuario(), cine.getId());
        Assert.assertEquals(1, eventos.size());
        Assert.assertEquals(true, eventos.get(0).getAbono());
    }
}