package es.uji.apps.par.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import es.uji.apps.par.builders.SesionBuilder;
import es.uji.apps.par.dao.ComprasDAO;
import es.uji.apps.par.exceptions.EventoConCompras;
import es.uji.apps.par.exceptions.SalaConSesionesMismaHoraException;
import es.uji.apps.par.model.PreciosEditablesSesion;
import es.uji.apps.par.model.Sesion;

@RunWith(SpringJUnit4ClassRunner.class)
public class SesionesServiceTest extends BaseServiceTest {
    @Autowired
    SesionesService sesionesService;

    @Autowired
    ComprasDAO comprasDAO;

    @Test
    public void getPreciosEditablesSesionInternos() {
        List<PreciosEditablesSesion> preciosSesion = sesionesService.getPreciosEditablesSesion(
            sesionConVentaAnticipadaActiva.getId(), usuario.getUsuario());
        Assert.assertEquals(3, preciosSesion.size());
        for (PreciosEditablesSesion precioSesion : preciosSesion) {
            Assert.assertEquals(precioSesion.getLocalizacion().getCodigo(), LOCALIZACION_CODE);
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
                Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecioAnticipado());
            }
            else if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("on")) {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
                Assert.assertNull(precioSesion.getPrecioAnticipado());
            }
            else {
                Assert.assertEquals(new BigDecimal("2.00"), precioSesion.getPrecio());
                Assert.assertNull(precioSesion.getPrecioAnticipado());
            }
        }
    }

    @Test
    public void getPreciosEditablesSesionConPlantillaInternos() {
        List<PreciosEditablesSesion> preciosSesion = sesionesService.getPreciosEditablesSesion(
            sesionConPlantillaConVentaAnticipadaActiva.getId(), usuario.getUsuario());
        Assert.assertEquals(3, preciosSesion.size());
        for (PreciosEditablesSesion precioSesion : preciosSesion) {
            Assert.assertEquals(precioSesion.getLocalizacion().getCodigo(), LOCALIZACION_CODE);
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
                Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecioAnticipado());
            }
            else if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("on")) {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
                Assert.assertNull(precioSesion.getPrecioAnticipado());
            }
            else {
                Assert.assertEquals(new BigDecimal("2.00"), precioSesion.getPrecio());
                Assert.assertNull(precioSesion.getPrecioAnticipado());
            }
        }
    }

    @Test
    public void getPreciosEditablesSesionPublico() {
        List<PreciosEditablesSesion> preciosSesion = sesionesService.getPreciosEditablesSesionPublicos(
            sesionConVentaAnticipadaActiva.getId(), usuario.getUsuario());
        Assert.assertEquals(2, preciosSesion.size());
        for (PreciosEditablesSesion precioSesion : preciosSesion) {
            Assert.assertEquals(precioSesion.getLocalizacion().getCodigo(), LOCALIZACION_CODE);
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
                Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecioAnticipado());
            }
            else if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("on")) {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
                Assert.assertNull(precioSesion.getPrecioAnticipado());
            }
        }
    }

    @Test
    public void getPreciosEditablesSesionConPlantillaPublico() {
        List<PreciosEditablesSesion> preciosSesion = sesionesService.getPreciosEditablesSesionPublicos(
            sesionConPlantillaConVentaAnticipadaActiva.getId(), usuario.getUsuario());
        Assert.assertEquals(2, preciosSesion.size());
        for (PreciosEditablesSesion precioSesion : preciosSesion) {
            Assert.assertEquals(precioSesion.getLocalizacion().getCodigo(), LOCALIZACION_CODE);
            if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("")) {
                Assert.assertEquals(new BigDecimal("20.00"), precioSesion.getPrecio());
                Assert.assertEquals(new BigDecimal("10.00"), precioSesion.getPrecioAnticipado());
            }
            else if (precioSesion.getTarifa().getIsPublica().equals("on") && precioSesion.getTarifa().getIsAbono().equals("on")) {
                Assert.assertEquals(new BigDecimal("1.00"), precioSesion.getPrecio());
                Assert.assertNull(precioSesion.getPrecioAnticipado());
            }
        }
    }

    @Test
    public void getTotalPreciosSesion() {
        int totalPreciosSesion = sesionesService.getTotalPreciosSesion(sesionConVentaAnticipadaActiva.getId());
        Assert.assertEquals(3, totalPreciosSesion);
    }

    @Test
    public void shouldCrearSesionesMismaHoraAnulandoLaPrevia() {
        Date date = new Date();
        Sesion sesion =
            new SesionBuilder("sesión con colisión", sala, evento, "13:00").withVentaOnline().get();
        sesion.setFechaCelebracionWithDate(date);

        for (int i = 0; i < 20; i++) {
            sesionesService.creaSesionConAnulacionDeSesionesMismaHora(sesion, true, usuario.getUsuario());
            entityManager.flush();
            entityManager.clear();
        }

        List<Sesion> sesiones = sesionesService.getSesionesActivas(evento.getId(), null, 0, Integer.MAX_VALUE, usuario.getUsuario());
        Assert.assertEquals(11, sesiones.size());
    }

    @Test(expected = SalaConSesionesMismaHoraException.class)
    public void shouldErrorCrearSesionesMismaHoraAnulandoLaPreviaConColision() {
        Date date = new Date();
        Sesion sesion =
            new SesionBuilder("sesión con colisión", sala, evento, "13:00").withVentaOnline().get();
        sesion.setFechaCelebracionWithDate(date);
        sesionesService.creaSesionConAnulacionDeSesionesMismaHora(sesion, true, usuario.getUsuario());
        entityManager.flush();
        entityManager.clear();
        sesionesService.creaSesionConAnulacionDeSesionesMismaHora(sesion, false, usuario.getUsuario());
    }

    @Test(expected = EventoConCompras.class)
    public void shouldErrorCrearSesionesMismaHoraAnulandoLaPreviaConCompras() {
        Sesion sesion =
            new SesionBuilder("sesión con colisión", sesionConCompras.getParSala(), sesionConCompras.getParEvento(), "18:00").withVentaOnline().get();
        sesion.setFechaCelebracionWithDate(sesionConCompras.getFechaCelebracion());
        sesionesService.creaSesionConAnulacionDeSesionesMismaHora(sesion, true, usuario.getUsuario());
    }
}