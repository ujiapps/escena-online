package es.uji.apps.par.dao;

import com.mysema.query.Tuple;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.EventoBuilder;
import es.uji.apps.par.builders.LocalizacionBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.SesionBuilder;
import es.uji.apps.par.builders.TarifaBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.OcupacionDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.exceptions.ButacaOcupadaException;
import es.uji.apps.par.exceptions.ButacasDiferentesLocalizacionesException;
import es.uji.apps.par.exceptions.IncidenciaNotFoundException;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.ButacasService;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager")
@ContextConfiguration(locations = { "/applicationContext-db-test.xml" })
@Transactional
public class ButacasDAOTest extends BaseDAOTest
{
    @Autowired
    ButacasDAO butacasDao;

    @Autowired
    ComprasDAO comprasDao;

    @Autowired
    ButacasService butacasService;

    private LocalizacionDTO localizacion;
    private TarifaDTO tarifa;
    private LocalizacionDTO localizacion2;
    private TarifaDTO tarifa2;
    private SalaDTO sala;
    private EventoDTO evento;
    private SesionDTO sesion;
    private UsuarioDTO usuario;

    @Before
    public void before()
    {
        CineDTO cine = new CineBuilder("Cine 2")
            .build(entityManager);

        TipoEventoDTO tipoEvento = new TipoEventoBuilder("tipo", "tipo", false, cine)
            .build(entityManager);

        sala = new SalaBuilder("Sala 2", cine)
            .build(entityManager);

        evento = new EventoBuilder("Evento 3", "Esdeveniment 3", cine, tipoEvento)
            .build(entityManager);

        localizacion = new LocalizacionBuilder("Localizacion1", "LOC1", new BigDecimal(200))
            .withSala(sala)
            .build(entityManager);

        tarifa = new TarifaBuilder("Tarifa 1").build(cine, entityManager);

        localizacion2 = new LocalizacionBuilder("Localizacion2", "LOC2", new BigDecimal(200))
            .withSala(sala)
            .build(entityManager);

        tarifa2 = new TarifaBuilder("Tarifa 2").build(cine, entityManager);

        sesion = new SesionBuilder("Sesión 1", sala, evento)
            .withPrecio(BigDecimal.valueOf(12.0), localizacion, tarifa)
            .withPrecio(BigDecimal.valueOf(10.0), localizacion, tarifa2)
            .withPrecio(BigDecimal.valueOf(5.0), localizacion2, tarifa2)
            .withCompra("Juan", BigDecimal.valueOf(12.0), 1, tarifa, localizacion, "1234-1")
            .withCompra("Pedro", BigDecimal.valueOf(24.0), 2, tarifa, localizacion, "1234-2")
            .withCompra("Samuel", BigDecimal.valueOf(10.0), 2, tarifa2, localizacion2, "1234-3")
            .build(entityManager);

        new SesionBuilder("Sesión 2", sala, evento)
            .withPrecio(BigDecimal.valueOf(12.0), localizacion, tarifa)
            .withPrecio(BigDecimal.valueOf(10.0), localizacion, tarifa2)
            .withPrecio(BigDecimal.valueOf(5.0), localizacion2, tarifa2)
            .withCompra("Emanuel", BigDecimal.valueOf(12.0), 10, tarifa, localizacion, "1234-10")
            .build(entityManager);

        usuario = new UsuarioBuilder("User", "user@test.com", "user")
            .withSala(sala)
            .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void getButacas()
    {
        List<Tuple> butacas = butacasDao.getButacas(sesion.getId(), Arrays.asList(localizacion.getCodigo()));

        Assert.assertEquals(3, butacas.size());
        Assert.assertEquals("1", butacas.get(0).get(1, String.class));
        Assert.assertEquals("1", butacas.get(0).get(2, String.class));
        Assert.assertEquals("2", butacas.get(1).get(1, String.class));
        Assert.assertEquals("1", butacas.get(1).get(2, String.class));
        Assert.assertEquals("2", butacas.get(2).get(1, String.class));
        Assert.assertEquals("2", butacas.get(2).get(2, String.class));
    }

    @Test
    public void butacasLibres()
    {
        Assert.assertTrue(butacasDao.estaOcupada(sesion.getId(), localizacion.getCodigo(), "1", "1"));
        Assert.assertFalse(butacasDao.estaOcupada(sesion.getId(), localizacion.getCodigo(), "1", "2"));
    }

    @Test
    public void cuentaButacasOcupadaDos() {
        Assert.assertEquals(3, butacasDao.getOcupadas(1L, sesion, localizacion.getCodigo()));
        List<OcupacionDTO> ocupadas = butacasDao.getOcupadas(Arrays.asList(sesion.getId()));
        Assert.assertEquals(2, ocupadas.size());
        for (OcupacionDTO ocupada : ocupadas) {
            String codigo = ocupada.getCodigo();
            if (codigo.equals(localizacion.getCodigo())) {
                Assert.assertTrue(3 == ocupada.getOcupadas());
            }
            else {
                Assert.assertTrue(2 == ocupada.getOcupadas());
            }
        }
    }

    @Test
    public void cuentaButacasOcupadasNoContarAnuladas() throws IncidenciaNotFoundException {
        CompraDTO compra = butacasDao.getCompra(sesion.getId(), localizacion.getCodigo(), "1", "1");
        Assert.assertEquals(3, butacasDao.getOcupadas(1L, sesion, localizacion.getCodigo()));

        for (ButacaDTO butacaDTO : compra.getParButacas()) {
            butacasDao.anularButaca(butacaDTO.getId(), new Usuario(usuario));
        }

        Assert.assertEquals(2, butacasDao.getOcupadas(1L, sesion, localizacion.getCodigo()));
    }


    @Test(expected = ButacaOcupadaException.class)
    public void cambiaFilaButacaOcupadaException() throws IncidenciaNotFoundException {
        CompraDTO compra = butacasDao.getCompra(sesion.getId(), localizacion.getCodigo(), "2", "1");

        butacasService.cambiaFilaNumero(compra.getParButacas().get(0).getId(), "2", "2", "es");
    }

    @Test
    public void cambiaFilaButaca() throws IncidenciaNotFoundException {
        CompraDTO compra = butacasDao.getCompra(sesion.getId(), localizacion.getCodigo(), "1", "1");

        butacasService.cambiaFilaNumero(compra.getParButacas().get(0).getId(), "1", "10", "es");

        compra = butacasDao.getCompra(sesion.getId(), localizacion.getCodigo(), "1", "10");
        Assert.assertTrue(compra.getParButacas().size() > 0);
    }

    @Test
    public void cambiaTipoTarifaButaca() throws IncidenciaNotFoundException {
        ArrayList<Long> ids = new ArrayList<>();

        CompraDTO compra = butacasDao.getCompra(sesion.getId(), localizacion.getCodigo(), "1", "1");
        ids.add(compra.getParButacas().get(0).getId());

        butacasService.cambiaTipo(ids, String.valueOf(tarifa2.getId()), usuario.getUsuario());

        compra = butacasDao.getCompra(sesion.getId(), localizacion.getCodigo(), "1", "1");
        for (ButacaDTO butacaDTO : compra.getParButacas()) {
            Assert.assertTrue(butacaDTO.getTipo().equals(String.valueOf(tarifa2.getId())));
        }
    }

    @Test(expected = ButacasDiferentesLocalizacionesException.class)
    public void cambiaTipoTarifaButacaDiferenteLocalizacionException() throws IncidenciaNotFoundException {
        ArrayList<Long> ids = new ArrayList<>();

        CompraDTO compra = butacasDao.getCompra(sesion.getId(), localizacion.getCodigo(), "1", "1");
        ids.add(compra.getParButacas().get(0).getId());
        compra = butacasDao.getCompra(sesion.getId(), localizacion2.getCodigo(), "3", "1");
        ids.add(compra.getParButacas().get(0).getId());

        butacasService.cambiaTipo(ids, String.valueOf(tarifa2.getId()), usuario.getUsuario());
    }
}
