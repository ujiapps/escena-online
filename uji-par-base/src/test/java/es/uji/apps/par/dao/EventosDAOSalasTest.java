package es.uji.apps.par.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.EventoBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.builders.TpvBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.model.Evento;

import static org.hamcrest.core.Is.is;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-db-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class EventosDAOSalasTest {

    private static final String SORT = "[{\"property\":\"tituloVa\", \"direction\":\"ASC\"}]";
    private static final int START = 0;
    private static final int LIMIT = 100;

    @Autowired
    EventosDAO eventosDAO;

    @PersistenceContext
    protected EntityManager entityManager;
    private CineDTO cine1;
    private SalaDTO sala1;
    private SalaDTO sala2;
    private EventoDTO evento1;
    private UsuarioDTO usuarioConEventos;

    @Test
    public void usuarioSoloVeEventosDeSusSalasYEventosSinSesionTest() throws ParseException {
        crearDosEventosUnoConSesionYOtroSinSesion();
        List<Evento> eventos = eventosDAO.getEventos(SORT, START, LIMIT, null, usuarioConEventos.getUsuario(), cine1.getId(), null);
        Assert.assertThat(eventos.size(), is(2));
    }

    @Test
    public void usuarioSoloVeEventosDeSusSalaConSesiones() throws ParseException {
        crearDosEventosConSesiones();
        List<Evento> eventos = eventosDAO.getEventos(SORT, START, LIMIT, null, usuarioConEventos.getUsuario(),
            cine1.getId(), null);
        Assert.assertThat(eventos.size(), is(1));
        Assert.assertThat(eventos.get(0).getTituloEs(), is("Evento 1"));
    }

    @Test
    public void usuarioConDosSalasTieneQueVerLosDosEventosTest() throws ParseException {
        crearUnUsuarioConDosSalasYDosEventosConUnaSesionEnCadaSala();
        List<Evento> eventos = eventosDAO.getEventos(SORT, START, LIMIT, null, usuarioConEventos.getUsuario(), cine1.getId(), null);
        Assert.assertThat(eventos.size(), is(2));
    }

    @Test
    public void usuarioNoPuedeVerEventosDeOtroCineTest() throws ParseException {
        crearDosCinesYUnUsuarioConDosEventosuNoEnCadaCine();
        List<Evento> eventos = eventosDAO.getEventos(SORT, START, LIMIT, null, usuarioConEventos.getUsuario(), cine1.getId(), null);
        Assert.assertThat(eventos.size(), is(1));
        Assert.assertThat(eventos.get(0).getTituloEs(), is("Evento 1"));
    }

    @Test
    public void eventosDeTaquillaSoloSiTienenSesionesTest(){
        crearDosEventosUnoConSesionYOtroSinSesion();
        List<Evento> eventos = eventosDAO.getEventosTaquilla(SORT, START, LIMIT, null, usuarioConEventos.getUsuario(), cine1.getId());
        Assert.assertThat(eventos.size(), is(1));
        Assert.assertThat(eventos.get(0).getTituloEs(), is("Evento 1"));
    }

    private void crearDosCinesYUnUsuarioConDosEventosuNoEnCadaCine() {
        cine1 = new CineBuilder("Cine 1")
            .build(entityManager);

        CineDTO cine2 = new CineBuilder("Cine 2").build(entityManager);

        TipoEventoDTO tipoEvento = new TipoEventoBuilder("tipo", "tipo", false, cine1)
            .build(entityManager);

        sala1 = new SalaBuilder("Sala 1", cine1)
            .build(entityManager);

        sala2 = new SalaBuilder("Sala 2", cine2)
            .build(entityManager);

        new TpvBuilder("ejemplo")
            .withSala(sala1)
            .build(entityManager);

        new TpvBuilder("ejemplo1")
            .withSala(sala2)
            .build(entityManager);

        evento1 = new EventoBuilder("Evento 1", "Esdeveniment 1", cine1, tipoEvento)
            .withSesion("Sesión 1", sala1)
            .build(entityManager);

        new EventoBuilder("Evento 2", "Esdeveniment 2", cine2, tipoEvento)
            .withSesion("sesion 2", sala2)
            .build(entityManager);

        usuarioConEventos = new UsuarioBuilder("User 1", "user1@test.com", "user1")
            .withSala(sala1)
            .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    private void crearUnUsuarioConDosSalasYDosEventosConUnaSesionEnCadaSala() {
        cine1 = new CineBuilder("Cine 1")
            .build(entityManager);

        TipoEventoDTO tipoEvento = new TipoEventoBuilder("tipo", "tipo", false, cine1)
            .build(entityManager);

        sala1 = new SalaBuilder("Sala 1", cine1)
            .build(entityManager);

        sala2 = new SalaBuilder("Sala 2", cine1)
            .build(entityManager);

        new TpvBuilder("ejemplo")
            .withSala(sala1)
            .build(entityManager);

        new TpvBuilder("ejemplo1")
            .withSala(sala2)
            .build(entityManager);

        evento1 = new EventoBuilder("Evento 1", "Esdeveniment 1", cine1, tipoEvento)
            .withSesion("Sesión 1", sala1)
            .build(entityManager);

        new EventoBuilder("Evento 2", "Esdeveniment 2", cine1, tipoEvento)
            .withSesion("sesion 2", sala2)
            .build(entityManager);

        usuarioConEventos = new UsuarioBuilder("User 1", "user1@test.com", "user1")
            .withSala(sala1)
            .withSala(sala2)
            .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    private void crearDosEventosUnoConSesionYOtroSinSesion() {
        cine1 = new CineBuilder("Cine 1")
            .build(entityManager);

        TipoEventoDTO tipoEvento = new TipoEventoBuilder("tipo", "tipo", false, cine1)
            .build(entityManager);

        sala1 = new SalaBuilder("Sala 1", cine1)
            .build(entityManager);

        sala2 = new SalaBuilder("Sala 2", cine1)
            .build(entityManager);

        new TpvBuilder("ejemplo")
            .withSala(sala1)
            .build(entityManager);

        new TpvBuilder("ejemplo1")
            .withSala(sala2)
            .build(entityManager);

        evento1 = new EventoBuilder("Evento 1", "Esdeveniment 1", cine1, tipoEvento)
            .withSesion("Sesión 1", sala1)
            .build(entityManager);

        new EventoBuilder("Evento 2", "Esdeveniment 2", cine1, tipoEvento)
            .build(entityManager);

        usuarioConEventos = new UsuarioBuilder("User 1", "user1@test.com", "user1")
            .withSala(sala1)
            .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    private void crearDosEventosConSesiones() {
        cine1 = new CineBuilder("Cine 1")
            .build(entityManager);

        TipoEventoDTO tipoEvento = new TipoEventoBuilder("tipo", "tipo", false, cine1)
            .build(entityManager);

        sala1 = new SalaBuilder("Sala 1", cine1)
            .build(entityManager);

        sala2 = new SalaBuilder("Sala 2", cine1)
            .build(entityManager);

        new TpvBuilder("ejemplo")
            .withSala(sala1)
            .build(entityManager);

        new TpvBuilder("ejemplo1")
            .withSala(sala2)
            .build(entityManager);

        evento1 = new EventoBuilder("Evento 1", "Esdeveniment 1", cine1, tipoEvento)
            .withSesion("Sesión 1", sala1)
            .build(entityManager);

        new EventoBuilder("Evento 2", "Esdeveniment 2", cine1, tipoEvento)
            .withSesion("sesion 2", sala2)
            .build(entityManager);

        usuarioConEventos = new UsuarioBuilder("User 1", "user1@test.com", "user1")
            .withSala(sala1)
            .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

}
