package es.uji.apps.par.builders;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.PlantillaDTO;
import es.uji.apps.par.db.PreciosSesionDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.enums.TipoPago;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.utils.DateUtils;

public class SesionBuilder {
    private SesionDTO sesion;
    private List<CompraDTO> compras;
    private List<PreciosSesionDTO> precios;

    public SesionBuilder(
        String nombre,
        SalaDTO sala,
        EventoDTO evento,
        String horaCelebracion
    ) {
        compras = new ArrayList<>();
        precios = new ArrayList<>();

        sesion = new SesionDTO();
        sesion.setNombre(nombre);
        sesion.setParSala(sala);
        sesion.setParEvento(evento);

        Date today = new Date();
        Calendar twoDaysAfter = Calendar.getInstance();
        twoDaysAfter.setTime(today);
        twoDaysAfter.add(Calendar.DATE, 2);

        Date fechaCelebracion = new Timestamp(twoDaysAfter.getTime().getTime());
        if (horaCelebracion != null)
            fechaCelebracion = DateUtils.addTimeToDate(fechaCelebracion, horaCelebracion);
        sesion.setFechaCelebracion(DateUtils.dateToTimestampSafe(fechaCelebracion));
    }

    public SesionBuilder(
        String nombre,
        SalaDTO sala,
        EventoDTO evento
    ) {
        this(nombre, sala, evento, null);
    }

    public SesionBuilder withRssId(String rssId) {
        sesion.setRssId(rssId);

        return this;
    }

    private SesionBuilder withCompra(
        String nombre,
        BigDecimal importe,
        Timestamp fecha,
        Integer numeroButacas,
        TarifaDTO tarifa,
        LocalizacionDTO localizacion,
        String uuid,
        String referenciaPago,
        boolean reserva,
        boolean pagada,
        UsuarioDTO usuario
    ) {
        return withCompra(nombre, importe, fecha, numeroButacas, tarifa, localizacion, uuid, referenciaPago, reserva, pagada, false, usuario);
    }

    private SesionBuilder withCompra(
        String nombre,
        BigDecimal importe,
        Timestamp fecha,
        Integer numeroButacas,
        TarifaDTO tarifa,
        LocalizacionDTO localizacion,
        String uuid,
        String referenciaPago,
        boolean reserva,
        boolean pagada,
        boolean anulada,
        UsuarioDTO usuario
    ) {
        CompraDTO compraDTO = new CompraDTO();
        compraDTO.setNombre(nombre);
        if (nombre != null) {
            compraDTO.setEmail(String.format("%s@test.com", nombre.toLowerCase()));
        }

        if (fecha != null) {
            compraDTO.setFecha(fecha);
        }
        else {
            compraDTO.setFecha(new Timestamp(new Date().getTime()));
        }

        compraDTO.setAnulada(anulada);
        compraDTO.setCaducada(false);
        compraDTO.setPagada(!reserva && pagada);
        compraDTO.setTipoPago(TipoPago.METALICO.toString());
        compraDTO.setReserva(reserva);
        compraDTO.setImporte(importe);
        compraDTO.setImporteSinComision(importe);
        compraDTO.setInfoPeriodica(true);
        compraDTO.setUuid(uuid);
        compraDTO.setTaquilla(usuario != null || reserva);
        compraDTO.setUsuario(usuario);
        compraDTO.setPorcentajeIva(BigDecimal.ZERO);
        compraDTO.setComision(BigDecimal.ZERO);

        if (referenciaPago != null)
            compraDTO.setReferenciaPago(referenciaPago);

        BigDecimal precioButaca = importe.divide(BigDecimal.valueOf(numeroButacas), RoundingMode.HALF_UP);
        List<ButacaDTO> butacas = new ArrayList<>();
        for (int i = 0; i < numeroButacas; i++) {
            ButacaDTO butaca = new ButacaDTO();
            butaca.setNumero(String.valueOf(i));
            butaca.setAnulada(anulada);
            butaca.setPrecio(precioButaca);
            butaca.setPrecioSinComision(precioButaca);

            if (localizacion != null)
                butaca.setParLocalizacion(localizacion);

            if (tarifa != null)
                butaca.setTipo(String.valueOf(tarifa.getId()));

            butacas.add(butaca);
        }
        compraDTO.setParButacas(butacas);

        compras.add(compraDTO);

        return this;
    }

    public SesionBuilder withCompra(
        String nombre,
        BigDecimal importe,
        Integer numeroButacas
    ) {
        return withCompra(nombre, importe, null, numeroButacas, null, null, null, null, false, true, null);
    }

    public SesionBuilder withCompra(
        String nombre,
        BigDecimal importe,
        Integer numeroButacas,
        TarifaDTO tarifa,
        LocalizacionDTO localizacion,
        String uuid
    ) {
        return withCompra(nombre, importe, null, numeroButacas, tarifa, localizacion, uuid, null, false, true, null);
    }

    public SesionBuilder withCompra(
        String nombre,
        BigDecimal importe,
        Integer numeroButacas,
        TarifaDTO tarifa,
        LocalizacionDTO localizacion,
        String uuid,
        Timestamp fecha,
        boolean pagada
    ) {
        return withCompra(nombre, importe, fecha, numeroButacas, tarifa, localizacion, uuid, null, false, pagada, null);
    }

    public SesionBuilder withCompras(
        List<String> nombres,
        BigDecimal importe,
        Integer numeroButacas
    ) {
        SesionBuilder sesionBuilder = this;
        for (String nombre : nombres) {
            sesionBuilder = withCompra(nombre, importe, null, numeroButacas, null, null, null, null, false, true, null);
        }
        return sesionBuilder;
    }

    public SesionBuilder withPlantilla(PlantillaDTO plantilla) {
        sesion.setParPlantilla(plantilla);

        return this;
    }

    public SesionBuilder withReservaAnulada(
        BigDecimal importe,
        Integer numeroButacas,
        TarifaDTO tarifa,
        LocalizacionDTO localizacion,
        String uuid,
        UsuarioDTO usuario
    ) {
        return withCompra(null, importe, null, numeroButacas, tarifa, localizacion, uuid, null, true, false, true, usuario);
    }

    public SesionBuilder withReserva(
        BigDecimal importe,
        Integer numeroButacas,
        TarifaDTO tarifa,
        LocalizacionDTO localizacion,
        String uuid,
        UsuarioDTO usuario
    ) {
        return withCompra(null, importe, null, numeroButacas, tarifa, localizacion, uuid, null, true, false, usuario);
    }

    public SesionBuilder withCompraTaquilla(
        String nombre,
        BigDecimal importe,
        Integer numeroButacas,
        TarifaDTO tarifa,
        LocalizacionDTO localizacion,
        String uuid,
        UsuarioDTO usuario
    ) {
        return withCompra(nombre, importe, null, numeroButacas, tarifa, localizacion, uuid, null, false, true, usuario);
    }

    public SesionBuilder withCompraTaquillaTPV(
        String nombre,
        BigDecimal importe,
        Timestamp fecha,
        Integer numeroButacas,
        TarifaDTO tarifa,
        LocalizacionDTO localizacion,
        String uuid,
        String referenciaPago,
        UsuarioDTO usuario
    ) {
        return withCompra(nombre, importe, fecha, numeroButacas, tarifa, localizacion, uuid, referenciaPago, false, true, usuario);
    }

    public SesionBuilder withVentaOnline() {
        Date now = new Date();

        sesion.setCanalInternet(true);
        sesion.setFechaInicioVentaOnline(new Timestamp(now.getTime()));
        sesion.setFechaFinVentaOnline(sesion.getFechaCelebracion());

        return this;
    }

    public SesionBuilder withPrecio(
        BigDecimal precio,
        LocalizacionDTO localizacion,
        TarifaDTO tarifa
    ) {
        PreciosSesionDTO preciosSesionDTO = new PreciosSesionDTO();
        preciosSesionDTO.setPrecio(precio);
        preciosSesionDTO.setParLocalizacione(localizacion);
        preciosSesionDTO.setParTarifa(tarifa);

        precios.add(preciosSesionDTO);

        return this;
    }

    public SesionBuilder withPrecioAnticipado(
        BigDecimal precio,
        BigDecimal precioAnticipado,
        LocalizacionDTO localizacion,
        TarifaDTO tarifa
    ) {
        PreciosSesionDTO preciosSesionDTO = new PreciosSesionDTO();
        preciosSesionDTO.setPrecio(precio);
        preciosSesionDTO.setPrecioAnticipado(precioAnticipado);
        preciosSesionDTO.setParLocalizacione(localizacion);
        preciosSesionDTO.setParTarifa(tarifa);

        precios.add(preciosSesionDTO);

        return this;
    }

    public SesionBuilder withFechaFinAnticipada(Timestamp date) {
        sesion.setFechaFinVentaAnticipada(date);

        return this;
    }

    public Sesion get() {
        return Sesion.SesionDTOToSesion(sesion);
    }

    public SesionDTO build(EntityManager entityManager) {
        entityManager.persist(sesion);

        Integer fila = 1;
        for (CompraDTO compra : compras) {
            List<ButacaDTO> butacas = compra.getParButacas();

            compra.setParSesion(sesion);

            entityManager.persist(compra);
            entityManager.flush();

            Integer numero = 1;
            for (ButacaDTO butaca : butacas) {
                butaca.setFila(fila.toString());
                butaca.setNumero(numero.toString());
                butaca.setParSesion(sesion);
                butaca.setParCompra(compra);
                entityManager.persist(butaca);

                numero++;
            }

            compra.setParButacas(butacas);
            entityManager.persist(compra);
            entityManager.flush();

            fila++;
        }

        for (PreciosSesionDTO precio : precios) {
            precio.setParSesione(sesion);

            entityManager.persist(precio);
            entityManager.flush();
        }

        sesion.setParPreciosSesions(precios);
        sesion.setParCompras(compras);

        return sesion;
    }
}