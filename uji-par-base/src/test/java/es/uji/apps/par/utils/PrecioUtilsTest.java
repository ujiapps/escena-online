package es.uji.apps.par.utils;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import es.uji.apps.par.model.GastosGestion;

public class PrecioUtilsTest {
    @Test
    public void testMonedaToCents() {
        String precio = PrecioUtils.monedaToCents(new BigDecimal(109));
        Assert.assertEquals(precio, "10900");

        precio = PrecioUtils.monedaToCents(new BigDecimal(109.3));
        Assert.assertEquals(precio, "10930");

        precio = PrecioUtils.monedaToCents(new BigDecimal(109.30));
        Assert.assertEquals(precio, "10930");

        precio = PrecioUtils.monedaToCents(new BigDecimal(109.3000000));
        Assert.assertEquals(precio, "10930");

        precio = PrecioUtils.monedaToCents(new BigDecimal(109.3000001));
        Assert.assertEquals(precio, "10930");

        precio = PrecioUtils.monedaToCents(new BigDecimal(109.3000009));
        Assert.assertEquals(precio, "10930");
    }

    @Test
    public void testGetPrecioConComision() {
        BigDecimal precioConComision = PrecioUtils.getPrecioConComision(new BigDecimal(1), new BigDecimal(0.031));
        Assert.assertEquals(precioConComision, new BigDecimal("1.04"));

        precioConComision = PrecioUtils.getPrecioConComision(new BigDecimal(105), new BigDecimal(0.0409));
        Assert.assertEquals(precioConComision, new BigDecimal("109.30"));

        precioConComision = PrecioUtils.getPrecioConComision(new BigDecimal(20), new BigDecimal(0.04235));
        Assert.assertEquals(precioConComision, new BigDecimal("20.85"));
    }

    @Test
    public void testGetGestosGestion() {
        GastosGestion gastosGestion = PrecioUtils.getGastosGestion(new BigDecimal(109.30), new BigDecimal(105));

        Assert.assertEquals(gastosGestion.getGastosGestion(), new BigDecimal("4.30"));
        Assert.assertEquals(gastosGestion.getGastosGestionSinIVA(), new BigDecimal("3.55"));
        Assert.assertEquals(gastosGestion.getIVAgastosGestion(), new BigDecimal("0.75"));
    }
}
