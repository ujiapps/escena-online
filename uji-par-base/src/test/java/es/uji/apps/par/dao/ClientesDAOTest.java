package es.uji.apps.par.dao;

import com.mysema.query.Tuple;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.AbonoBuilder;
import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.EventoBuilder;
import es.uji.apps.par.builders.LocalizacionBuilder;
import es.uji.apps.par.builders.PlantillaBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.SesionBuilder;
import es.uji.apps.par.builders.TarifaBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.PlantillaDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.db.UsuarioDTO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager")
@ContextConfiguration(locations = {"/applicationContext-db-test.xml"})
@Transactional
public class ClientesDAOTest extends BaseDAOTest {
    private static final String SORT = "[{\"property\":\"nombre\", \"direction\":\"ASC\"}]";
    private static final int START = 0;
    private static final int LIMIT = 100;

    @Autowired
    ClientesDAO clientesDAO;

    @PersistenceContext
    protected EntityManager entityManager;

    UsuarioDTO usuarioConSala1;
    UsuarioDTO usuarioConSala2;
    EventoDTO evento1;
    SesionDTO sesion1;
    SesionDTO sesion2;
    PlantillaDTO plantilla;
    TarifaDTO abono1;
    final String RSS_ID = "100";

    @Before
    public void setUp() {
        //Usuario1 -> Cine1 -> Evento1 -> Sesion1 (Sala1)
        //   "          "   ->    "    -> Sesion2 (Sala1)

        //Usuario2 -> Cine2 -> Evento3 -> Sesion3 (Sala2)

        //   -     ->  null -> Evento4 -> Sesion4 (Sala2)

        CineDTO cine1 = new CineBuilder("Cine 1").build(entityManager);

        TipoEventoDTO tipoEvento = new TipoEventoBuilder("tipo", "tipo", false, cine1).build(entityManager);

        SalaDTO sala1 = new SalaBuilder("Sala 1", cine1).build(entityManager);

        LocalizacionDTO localizacion =
            new LocalizacionBuilder("platea", "PL", BigDecimal.valueOf(200)).withSala(sala1).build(entityManager);

        TarifaDTO tarifa = new TarifaBuilder("General").withPublic().build(cine1, entityManager);

        plantilla = new PlantillaBuilder("Plantilla 1", sala1).build(entityManager);

        evento1 =
            new EventoBuilder("Evento 1", "Esdeveniment 1", cine1, tipoEvento).withRssId(RSS_ID).build(entityManager);

        sesion1 = new SesionBuilder("Sesión 1", sala1, evento1).withCompra("Luis", BigDecimal.valueOf(10.0), 3)
            .withCompra("Luis", BigDecimal.valueOf(25.0), 4).build(entityManager);

        sesion2 = new SesionBuilder("Sesión 2", sala1, evento1).withCompra("Juan", BigDecimal.valueOf(10.0), 2)
            .withCompra("Pepe", BigDecimal.valueOf(10.0), 3).build(entityManager);

        abono1 = new AbonoBuilder("Abono 1").withPrecio(BigDecimal.ONE, localizacion)
            .build(cine1, sala1, tipoEvento, tarifa, entityManager);

        usuarioConSala1 = new UsuarioBuilder("User 1", "user1@test.com", "user1").withSala(sala1).build(entityManager);

        CineDTO cine2 = new CineBuilder("Cine 2").build(entityManager);

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine2).build(entityManager);

        TarifaDTO tarifa2 = new TarifaBuilder("General").withPublic().build(cine2, entityManager);

        new AbonoBuilder("Abono 2").withPrecio(BigDecimal.ONE, localizacion)

            .build(cine2, sala2, tipoEvento, tarifa2, entityManager);

        usuarioConSala2 = new UsuarioBuilder("User 2", "user2@test.com", "user2").withSala(sala2).build(entityManager);

        new EventoBuilder("Evento 4", "Esdeveniment 4", null, tipoEvento).withSesion("Sesión 4", sala2)
            .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void getClientes() {
        List<Tuple> clientes = clientesDAO
            .getClientes(ClientesDAOTest.SORT, ClientesDAOTest.START, ClientesDAOTest.LIMIT, null,
                usuarioConSala1.getUsuario());

        int totalClientes = clientesDAO.getTotalClientes(null, usuarioConSala1.getUsuario());

        assertNotNull(clientes);
        assertEquals(3, clientes.size());
        assertEquals(totalClientes, clientes.size());
    }

    @Test
    public void removeInfoPeriodica() {
        clientesDAO.removeInfoPeriodica("luis@test.com", usuarioConSala1.getUsuario());

        List<Tuple> clientes = clientesDAO
            .getClientes(ClientesDAOTest.SORT, ClientesDAOTest.START, ClientesDAOTest.LIMIT, null,
                usuarioConSala1.getUsuario());
        int totalClientes = clientesDAO.getTotalClientes(null, usuarioConSala1.getUsuario());

        assertNotNull(clientes);
        assertEquals(2, clientes.size());
        assertEquals(totalClientes, clientes.size());
    }

    @Test
    public void noRemoveInfoPeriodicaFromOtherUserClients() {
        clientesDAO.removeInfoPeriodica("luis@test.com", usuarioConSala2.getUsuario());

        List<Tuple> clientes = clientesDAO
            .getClientes(ClientesDAOTest.SORT, ClientesDAOTest.START, ClientesDAOTest.LIMIT, null,
                usuarioConSala1.getUsuario());
        int totalClientes = clientesDAO.getTotalClientes(null, usuarioConSala1.getUsuario());

        assertNotNull(clientes);
        assertEquals(3, clientes.size());
        assertEquals(totalClientes, clientes.size());
    }
}
