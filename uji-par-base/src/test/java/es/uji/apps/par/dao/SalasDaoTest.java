package es.uji.apps.par.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Usuario;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager")
@ContextConfiguration(locations = { "/applicationContext-db-test.xml" })
public class SalasDaoTest extends BaseDAOTest
{
    @Autowired
    UsuariosDAO usuariosDAO;

    @Autowired
    SalasDAO salasDao;

    @Autowired
    CinesDAO cinesDao;

    @PersistenceContext
    protected EntityManager entityManager;

    CineDTO cine1;
    UsuarioDTO usuarioConSala1;
    UsuarioDTO usuarioConSala2;

    @Before
    public void setUp()
    {
        //Usuario1 -> Sala1

        //Usuario2 -> Sala2

        cine1 = new CineBuilder("Cine 1")
                .build(entityManager);

        SalaDTO sala1 = new SalaBuilder("Sala 1", cine1)
                .build(entityManager);

        usuarioConSala1 = new UsuarioBuilder("User 1", "user1@test.com", "user1")
                .withSala(sala1)
                .build(entityManager);


        CineDTO cine2 = new CineBuilder("Cine 2")
                .build(entityManager);

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine2)
                .build(entityManager);

        usuarioConSala2 = new UsuarioBuilder("User 2", "user2@test.com", "user2")
                .withSala(sala2)
                .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    @Transactional
    public void getSalas()
    {
        List<Sala> salas = salasDao.getSalas(usuarioConSala1.getUsuario());

        Assert.assertEquals(1, salas.size());
    }

    @Test
    @Transactional
    public void insertaUna()
    {
        Sala sala = new Sala("sala2");
        sala.setCine(Cine.cineDTOToCine(cine1, false));
        Usuario usuario = new Usuario(usuarioConSala1);

        salasDao.addSala(sala);
        usuariosDAO.addSalaUsuario(sala, usuario);

        List<Sala> salas = salasDao.getSalas(usuarioConSala1.getUsuario());

        Assert.assertEquals(2, salas.size());
    }
}
