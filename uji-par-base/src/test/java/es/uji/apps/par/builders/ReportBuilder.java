package es.uji.apps.par.builders;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.ReportDTO;

public class ReportBuilder
{
	private ReportDTO report;

	public ReportBuilder(String className, String tipo, CineDTO cine)
	{
		report = new ReportDTO();
		report.setClase(className);
		report.setTipo(tipo);
		report.setParCine(cine);
	}

	public ReportDTO build(EntityManager entityManager)
	{
		entityManager.persist(report);

		return report;
	}
}