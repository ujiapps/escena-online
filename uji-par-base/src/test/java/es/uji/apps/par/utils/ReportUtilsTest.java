package es.uji.apps.par.utils;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class ReportUtilsTest {
    @Test
    public void testFormatEuros()
    {
        String euros = ReportUtils.formatEuros(new BigDecimal(16));
        Assert.assertEquals(euros, "16,00");

        euros = ReportUtils.formatEuros(new BigDecimal(16.1));
        Assert.assertEquals(euros, "16,10");

        euros = ReportUtils.formatEuros(new BigDecimal(16.04));
        Assert.assertEquals(euros, "16,04");

        euros = ReportUtils.formatEuros(new BigDecimal(16.0400000));
        Assert.assertEquals(euros, "16,04");

        euros = ReportUtils.formatEuros(new BigDecimal(16.0400001));
        Assert.assertEquals(euros, "16,04");

        euros = ReportUtils.formatEuros(new BigDecimal(16.0400009));
        Assert.assertEquals(euros, "16,04");
    }
}
