package es.uji.apps.par.builders;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.MailsBlacklistDTO;

public class MailBlacklistBuilder
{
	private MailsBlacklistDTO blacklist;

	public MailBlacklistBuilder(String email)
	{
		blacklist = new MailsBlacklistDTO();
		blacklist.setEmail(email);
	}

	public MailsBlacklistDTO build(EntityManager entityManager)
	{
		entityManager.persist(blacklist);

		return blacklist;
	}
}