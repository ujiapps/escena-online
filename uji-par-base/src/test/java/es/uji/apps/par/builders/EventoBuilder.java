package es.uji.apps.par.builders;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.db.TpvsDTO;
import es.uji.apps.par.model.Evento;

public class EventoBuilder {
    private EventoDTO evento;
    private List<SesionDTO> sesiones;

    public EventoBuilder(
        String tituloEs,
        String tituloVa,
        CineDTO cine,
        TipoEventoDTO tipoEvento
    ) {
        sesiones = new ArrayList<>();

        evento = new EventoDTO();
        evento.setTituloEs(tituloEs);
        evento.setTituloVa(tituloVa);
        evento.setParTiposEvento(tipoEvento);
        evento.setParCine(cine);
        evento.setRetencionSgae(BigDecimal.ZERO);
        evento.setIvaSgae(BigDecimal.ZERO);
        evento.setPorcentajeIva(BigDecimal.ZERO);
        evento.setIsabono(false);
        evento.setAsientosNumerados(true);
        evento.setEntradasNominales(false);
        evento.setEntradasLimitadas(false);
    }

    public EventoBuilder isAbono() {
        evento.setIsabono(true);

        return this;
    }

    public EventoBuilder withAsientosNoNumerados() {
        evento.setAsientosNumerados(false);

        return this;
    }

    public EventoBuilder withRssId(String rssId) {
        evento.setRssId(rssId);

        return this;
    }

    public EventoBuilder withTpv(TpvsDTO tpv) {
        evento.setParTpv(tpv);

        return this;
    }

    public EventoBuilder withSesion(
        String nombre,
        SalaDTO sala
    ) {
        return withSesion(new SesionDTO(), nombre, sala);
    }

    public EventoBuilder withMultisesion(
        String nombre,
        SalaDTO sala
    ) {
        SesionDTO sesionDTO = new SesionDTO();
        sesionDTO.setMultisesion(true);
        return withSesion(sesionDTO, nombre, sala);
    }

    private EventoBuilder withSesion(
        SesionDTO sesionDTO,
        String nombre,
        SalaDTO sala
    ) {
        sesionDTO.setNombre(nombre);
        sesionDTO.setParSala(sala);
        sesionDTO.setFechaCelebracion(new Timestamp(new Date().getTime()));

        sesiones.add(sesionDTO);

        return this;
    }

    public EventoBuilder withLimiteEntradasPorEmail(int limite) {
        evento.setEntradasLimitadas(true);
        evento.setEntradasPorEmail(limite);

        return this;
    }

    public Evento get() {
        return Evento.eventoDTOtoEvento(evento);
    }

    public EventoDTO build(EntityManager entityManager) {
        if (evento.getParTpv() == null) {
            evento.setParTpv(new TpvBuilder("tpv").build(entityManager));
        }

        entityManager.persist(evento);

        for (SesionDTO sesion : sesiones) {
            sesion.setParEvento(evento);
            entityManager.persist(sesion);
        }
        evento.setParSesiones(sesiones);

        return evento;
    }
}