package es.uji.apps.par.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import es.uji.apps.par.builders.TarifaBuilder;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.TarifaYaExisteException;
import es.uji.apps.par.exceptions.UsuarioNoExisteException;
import es.uji.apps.par.exceptions.UsuarioObligatorioException;
import es.uji.apps.par.model.Tarifa;

@RunWith(SpringJUnit4ClassRunner.class)
public class TarifasServiceTest extends BaseServiceTest {
    @Autowired
    TarifasService tarifasService;

    @Test
    public void getTarifas() {
        List<Tarifa> tarifas = tarifasService.getEditables(SORT, START, LIMIT, usuario.getUsuario());

        Assert.assertNotNull(tarifas);
        Assert.assertEquals(3, tarifas.size());
        for (Tarifa tarifa : tarifas) {
            Assert.assertEquals("", tarifa.getIsAbono());
        }

        long total = tarifasService.getTotalEditables(usuario.getUsuario());
        Assert.assertEquals(3, total);
    }

    @Test
    public void getTarifasYAbonos() {
        List<Tarifa> tarifas = tarifasService.getAll(SORT, START, LIMIT, usuario.getUsuario());

        Assert.assertNotNull(tarifas);
        Assert.assertEquals(5, tarifas.size());

        long total = tarifasService.getTotal(usuario.getUsuario());
        Assert.assertEquals(5, total);
    }

    @Test(expected = CampoRequeridoException.class)
    public void addTarifaSinNombre() {
        Tarifa nuevaTarifa = new TarifaBuilder("").get();
        tarifasService.add(nuevaTarifa, usuario.getUsuario());
    }

    @Test(expected = UsuarioObligatorioException.class)
    public void addTarifaSinUsuario() {
        Tarifa nuevaTarifa = new TarifaBuilder("Tarifa test").get();
        tarifasService.add(nuevaTarifa, null);
    }

    @Test(expected = UsuarioNoExisteException.class)
    public void addTarifaConUsuarioSinCine() {
        Tarifa nuevaTarifa = new TarifaBuilder("Tarifa test").get();
        tarifasService.add(nuevaTarifa, usuarioSinCine.getUsuario());
    }

    @Test(expected = TarifaYaExisteException.class)
    public void addTarifaNombreDuplicado() {
        Tarifa nuevaTarifa = new TarifaBuilder("General").get();
        tarifasService.add(nuevaTarifa, usuario.getUsuario());
    }

    @Test
    public void addTarifaNombreDuplicadoDiferenteUsuario() {
        Tarifa nuevaTarifa = new TarifaBuilder("Taquilla").get();
        tarifasService.add(nuevaTarifa, usuario2.getUsuario());
    }

    @Test
    public void addTarifa() {
        Tarifa nuevaTarifa = new TarifaBuilder("Tarifa Nueva").get();
        tarifasService.add(nuevaTarifa, usuario.getUsuario());
    }

    @Test(expected = TarifaYaExisteException.class)
    public void updateTarifaConNombreDuplicado() {
        Tarifa nuevaTarifa = new TarifaBuilder("Tarifa Nueva").get();
        nuevaTarifa = tarifasService.add(nuevaTarifa, usuario.getUsuario());
        nuevaTarifa.setNombre("General");
        tarifasService.update(nuevaTarifa, usuario.getUsuario());
    }

    @Test
    public void updateTarifa() {
        final String NOMBRE_MODIFICADO = "Tarifa Modificada";

        Tarifa nuevaTarifa = new TarifaBuilder("Tarifa Nuevo").get();
        nuevaTarifa = tarifasService.add(nuevaTarifa, usuario.getUsuario());
        nuevaTarifa.setNombre(NOMBRE_MODIFICADO);
        Tarifa TarifaModificado = tarifasService.update(nuevaTarifa, usuario.getUsuario());

        Assert.assertNotNull(TarifaModificado);
        Assert.assertEquals(nuevaTarifa.getId(), TarifaModificado.getId());
        Assert.assertEquals(NOMBRE_MODIFICADO, TarifaModificado.getNombre());
    }
}