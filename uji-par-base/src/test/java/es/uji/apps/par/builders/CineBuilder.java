package es.uji.apps.par.builders;

import java.math.BigDecimal;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.CineDTO;

public class CineBuilder
{
	private CineDTO cine;

	public CineBuilder(String nombre)
	{
		cine = new CineDTO();
		cine.setNombre(nombre);
	}

	public CineBuilder withComision(BigDecimal comision) {
		cine.setComision(comision);

		return this;
	}

	public CineBuilder withAnularCompraDesdeMail(boolean anularCompraDesdeMail) {
		cine.setAnulableDesdeEnlace(anularCompraDesdeMail);
		cine.setTiempoRestanteAnulableDesdeEnlace(Integer.MIN_VALUE);

		return this;
	}

	public CineBuilder withLimiteEntradas(Integer limiteEntradasGratis) {
		cine.setLimiteEntradasGratuitasPorCompra(limiteEntradasGratis);
		cine.setLimiteNoGratuitas(true);

		return this;
	}

	public CineDTO build(EntityManager entityManager)
	{
		entityManager.persist(cine);

		return cine;
	}
}