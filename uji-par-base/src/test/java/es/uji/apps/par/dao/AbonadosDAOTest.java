package es.uji.apps.par.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.AbonoBuilder;
import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.EventoBuilder;
import es.uji.apps.par.builders.LocalizacionBuilder;
import es.uji.apps.par.builders.PlantillaBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.SesionBuilder;
import es.uji.apps.par.builders.TarifaBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.PlantillaDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.model.Abonado;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext-db-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class AbonadosDAOTest {
    private static final String SORT = "[{\"property\":\"nombre\", \"direction\":\"ASC\"}]";

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    AbonadosDAO abonadosDAO;

    UsuarioDTO usuarioConSala1;
    UsuarioDTO usuarioConSala2;
    CineDTO cine1;
    EventoDTO evento1;
    SesionDTO sesion1;
    SesionDTO sesion2;
    TarifaDTO abono1;
    TarifaDTO abono2;
    PlantillaDTO plantilla;
    final String RSS_ID = "100";

    @Before
    public void setUp() {
        cine1 = new CineBuilder("Cine 1").build(entityManager);

        TipoEventoDTO tipoEvento = new TipoEventoBuilder("tipo", "tipo", false, cine1).build(entityManager);

        SalaDTO sala1 = new SalaBuilder("Sala 1", cine1).build(entityManager);

        LocalizacionDTO localizacion =
            new LocalizacionBuilder("platea", "PL", BigDecimal.valueOf(200)).withSala(sala1).build(entityManager);

        TarifaDTO tarifa = new TarifaBuilder("General").withPublic().build(cine1, entityManager);

        plantilla = new PlantillaBuilder("Plantilla 1", sala1).build(entityManager);

        evento1 =
            new EventoBuilder("Evento 1", "Esdeveniment 1", cine1, tipoEvento).withRssId(RSS_ID).build(entityManager);

        sesion1 = new SesionBuilder("Sesión 1", sala1, evento1).withCompra("Juan", BigDecimal.valueOf(25.0), 4)
            .build(entityManager);

        sesion2 = new SesionBuilder("Sesión 2", sala1, evento1).withCompra("Juan", BigDecimal.valueOf(10.0), 2)
            .withCompra("Pepe", BigDecimal.valueOf(10.0), 3).build(entityManager);

        abono1 = new AbonoBuilder("Abono 1").withAbonado("sergio").withPrecio(BigDecimal.ONE, localizacion)
            .build(cine1, sala1, tipoEvento, tarifa, entityManager);

        abono2 = new AbonoBuilder("Abono Juvilados").withPrecio(BigDecimal.ONE, localizacion)
            .build(cine1, sala1, tipoEvento, tarifa, entityManager);

        new TarifaBuilder("Tarifa 1").build(cine1, entityManager);

        usuarioConSala1 = new UsuarioBuilder("User 1", "user1@test.com", "user1").withSala(sala1).build(entityManager);

        CineDTO cine2 = new CineBuilder("Cine 2").build(entityManager);

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine2).build(entityManager);

        TarifaDTO tarifa2 = new TarifaBuilder("General").withPublic().build(cine2, entityManager);

        new AbonoBuilder("Abono 2").withPrecio(BigDecimal.ONE, localizacion)
            .build(cine2, sala2, tipoEvento, tarifa2, entityManager);

        usuarioConSala2 = new UsuarioBuilder("User 2", "user2@test.com", "user2").withSala(sala2).build(entityManager);

        new EventoBuilder("Evento 4", "Esdeveniment 4", null, tipoEvento).withSesion("Sesión 4", sala2)
            .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void getAbonados() {
        List<Abonado> abonados = abonadosDAO.getAbonadosByAbonoId(abono1.getId(), SORT, 0, Integer.MAX_VALUE);

        Assert.assertNotNull(abonados);
        Assert.assertEquals(abonados.size(), 1);
        Assert.assertEquals(abonados.get(0).getEmail(), "sergio@test.com");

        abonados = abonadosDAO.getAbonadosByAbonoId(abono2.getId(), SORT, 0, Integer.MAX_VALUE);

        Assert.assertNotNull(abonados);
        Assert.assertEquals(abonados.size(), 0);
    }

    @Test
    public void getTotalAbonados() {
        int total = abonadosDAO.getTotalAbonadosByAbonoId(abono1.getId());

        Assert.assertEquals(total, 1);

        total = abonadosDAO.getTotalAbonadosByAbonoId(abono2.getId());

        Assert.assertEquals(total, 0);
    }
}
