package es.uji.apps.par.builders;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.model.TipoEvento;

public class TipoEventoBuilder {
    private TipoEventoDTO tipoEvento;

    public TipoEventoBuilder(
        String nombreEs,
        String nombreVa,
        boolean icaa,
        CineDTO cine
    ) {
        tipoEvento = new TipoEventoDTO();
        tipoEvento.setNombreEs(nombreEs);
        tipoEvento.setNombreVa(nombreVa);
        tipoEvento.setExportarICAA(icaa);
        tipoEvento.setParCine(cine);
    }

    public TipoEvento get() {
        return TipoEvento.tipoEventoDTOToTipoEvento(this.tipoEvento);
    }

    public TipoEventoDTO build(EntityManager entityManager) {
        entityManager.persist(tipoEvento);

        return tipoEvento;
    }
}