package es.uji.apps.par.builders;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.TpvsDTO;
import es.uji.apps.par.db.TpvsSalasDTO;

public class TpvBuilder
{
	private TpvsDTO tpv;
	private List<TpvsSalasDTO> salas;

	public TpvBuilder(String nombre)
	{
		salas = new ArrayList<>();

		tpv = new TpvsDTO();
		tpv.setVisible(true);
		tpv.setNombre(nombre);
	}

	public TpvBuilder withSala(SalaDTO sala)
	{
		TpvsSalasDTO tpvsSalasDTO = new TpvsSalasDTO();
		tpvsSalasDTO.setSala(sala);

		this.salas.add(tpvsSalasDTO);

		return this;
	}

	public TpvsDTO build(EntityManager entityManager)
	{
		entityManager.persist(tpv);
		for (TpvsSalasDTO tpvsSalas : salas)
		{
			tpvsSalas.setTpv(tpv);
			entityManager.persist(tpvsSalas);
		}

		tpv.setParTpvsSalas(salas);

		return tpv;
	}
}