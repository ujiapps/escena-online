package es.uji.apps.par.builders;

import java.math.BigDecimal;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.model.Localizacion;

public class LocalizacionBuilder
{
	private LocalizacionDTO localizacion;

	public LocalizacionBuilder(String nombre, String codigo, BigDecimal total)
	{
		localizacion = new LocalizacionDTO();
		localizacion.setNombreEs(nombre);
		localizacion.setNombreVa(nombre);
		localizacion.setTotalEntradas(total);
		localizacion.setCodigo(codigo);
	}

	public LocalizacionBuilder withSala(SalaDTO sala) {
		localizacion.setSala(sala);

		return this;
	}

	public Localizacion get() {
		return Localizacion.localizacionDTOtoLocalizacion(localizacion);
	}

	public LocalizacionDTO build(EntityManager entityManager)
	{
		entityManager.persist(localizacion);

		return localizacion;
	}
}