package es.uji.apps.par.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import es.uji.apps.par.builders.EventoBuilder;
import es.uji.apps.par.exceptions.EventoConCompras;
import es.uji.apps.par.exceptions.EventoNoEncontradoException;
import es.uji.apps.par.model.Evento;

@RunWith(SpringJUnit4ClassRunner.class)
public class EventosServiceTest extends BaseServiceTest {
    @Autowired
    EventosService eventosService;

    @Test
    public void getEvento() {
        Evento eventoEncontrado = eventosService.getEvento(this.evento.getId(), usuario.getUsuario());
        Assert.assertNotNull(eventoEncontrado);
        Assert.assertEquals(evento.getId(), eventoEncontrado.getId());
    }

    @Test(expected = EventoNoEncontradoException.class)
    public void getEventoDeOtroUsuario() {
        eventosService.getEvento(this.evento.getId(), usuario2.getUsuario());
    }

    @Test
    public void getEventos() throws ParseException {
        List<Evento> eventos =
            eventosService.getEventos(SORT, 0, Integer.MAX_VALUE, null, usuario.getUsuario(), cine.getId(), null);
        int totalEventos = eventosService.getTotalEventos(null, usuario.getUsuario(), cine.getId());
        Assert.assertNotNull(eventos);
        Assert.assertEquals(eventos.size(), totalEventos);
        Assert.assertEquals(6, eventos.size());
    }

    @Test
    public void getEventosActivos() throws ParseException {
        List<Evento> eventos =
            eventosService.getEventosActivos(SORT, 0, Integer.MAX_VALUE, null, usuario.getUsuario(), cine.getId(), null);
        int totalEventos = eventosService.getTotalEventosActivos(null, usuario.getUsuario(), cine.getId());
        Assert.assertNotNull(eventos);
        Assert.assertEquals(eventos.size(), totalEventos);
        Assert.assertEquals(6, eventos.size());
    }

    @Test
    public void getEventosActivosTaquilla() {
        List<Evento> eventos =
            eventosService.getEventosActivosTaquilla(SORT, 0, Integer.MAX_VALUE, null, usuario.getUsuario(), cine.getId());
        int totalEventos = eventosService.getTotalEventosActivosTaquilla(null, usuario.getUsuario());
        Assert.assertNotNull(eventos);
        Assert.assertEquals(eventos.size(), totalEventos);
        Assert.assertEquals(6, eventos.size());
    }

    @Test
    public void getEventosUsuario2() throws ParseException {
        List<Evento> eventos =
            eventosService.getEventos(SORT, 0, Integer.MAX_VALUE, null, usuario2.getUsuario(), cine2.getId(), null);
        int totalEventos = eventosService.getTotalEventos(null, usuario2.getUsuario(), cine2.getId());
        Assert.assertNotNull(eventos);
        Assert.assertEquals(eventos.size(), totalEventos);
        Assert.assertEquals(3, eventos.size());
    }

    @Test
    public void getEventosActivosUsuario2() throws ParseException {
        List<Evento> eventos =
            eventosService.getEventosActivos(SORT, 0, Integer.MAX_VALUE, null, usuario2.getUsuario(), cine2.getId(), null);
        int totalEventos = eventosService.getTotalEventosActivos(null, usuario2.getUsuario(), cine2.getId());
        Assert.assertNotNull(eventos);
        Assert.assertEquals(eventos.size(), totalEventos);
        Assert.assertEquals(3, eventos.size());
    }

    @Test
    public void getEventosActivosTaquillaUsuario2() {
        List<Evento> eventos =
            eventosService.getEventosActivosTaquilla(SORT, 0, Integer.MAX_VALUE, null, usuario2.getUsuario(), cine2.getId());
        int totalEventos = eventosService.getTotalEventosActivosTaquilla(null, usuario2.getUsuario());
        Assert.assertNotNull(eventos);
        Assert.assertEquals(eventos.size(), totalEventos);
        Assert.assertEquals(3, eventos.size());
    }

    @Test
    public void addEvento() throws IOException {
        int totalEventos = eventosService.getTotalEventos(null, usuario.getUsuario(), cine.getId());
        Assert.assertEquals(6, totalEventos);

        Evento eventoNuevo = new EventoBuilder("tit test", "tit ca test", cine, tipoEvento).withTpv(tpv).get();
        Evento evento = eventosService.addEvento(eventoNuevo, usuario.getUsuario());
        Assert.assertNotNull(evento);
        Assert.assertTrue(evento.getId() > 0);

        totalEventos = eventosService.getTotalEventos(null, usuario.getUsuario(), cine.getId());
        Assert.assertEquals(7, totalEventos);

        totalEventos = eventosService.getTotalEventosActivos(null, usuario.getUsuario(), cine.getId());
        Assert.assertEquals(7, totalEventos);
    }

    @Test(expected = EventoConCompras.class)
    public void removeEventoConCompras() {
        eventosService.removeEvento(evento.getId());
    }

    @Test(expected = EventoNoEncontradoException.class)
    public void removeEventoNoExistente() {
        eventosService.removeEvento(321897312L);
    }

    @Test(expected = EventoNoEncontradoException.class)
    public void removeEvento() throws IOException {
        Evento eventoNuevo = new EventoBuilder("tit test", "tit ca test", cine, tipoEvento).withTpv(tpv).get();
        Evento evento = eventosService.addEvento(eventoNuevo, usuario.getUsuario());
        Assert.assertNotNull(evento);
        Assert.assertTrue(evento.getId() > 0);

        eventosService.removeEvento(evento.getId());
        eventosService.getEvento(evento.getId(), usuario.getUsuario());
    }
}