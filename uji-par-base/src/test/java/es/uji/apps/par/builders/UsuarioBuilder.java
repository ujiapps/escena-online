package es.uji.apps.par.builders;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import es.uji.apps.par.auth.Role;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SalasUsuarioDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.model.Usuario;

public class UsuarioBuilder
{
	private UsuarioDTO usuario;
	private List<SalasUsuarioDTO> salas;

	public UsuarioBuilder(String nombre, String mail, String login)
	{
		salas = new ArrayList<>();

		usuario = new UsuarioDTO();
		usuario.setNombre(nombre);
		usuario.setMail(mail);
		usuario.setUsuario(login);
		usuario.setRole(Role.ADMIN);
		usuario.setPuedeAnular(true);
	}

	public UsuarioBuilder withRole(Role role)
	{
		usuario.setRole(role);

		return this;
	}

	public UsuarioBuilder withPuedeAnular(boolean puedeAnular)
	{
		usuario.setPuedeAnular(puedeAnular);

		return this;
	}


	public UsuarioBuilder withURL(String url)
	{
		usuario.setUrl(url);

		return this;
	}

	public UsuarioBuilder withSala(SalaDTO sala)
	{
		SalasUsuarioDTO salasUsuarioDTO = new SalasUsuarioDTO();
		salasUsuarioDTO.setParSala(sala);

		salas.add(salasUsuarioDTO);

		return this;
	}

	public Usuario get() {
		return new Usuario(this.usuario);
	}

	public UsuarioDTO build(EntityManager entityManager)
	{
		entityManager.persist(usuario);

		for (SalasUsuarioDTO salaUsuario : salas)
		{
			salaUsuario.setParUsuario(usuario);
			entityManager.persist(salaUsuario);
		}

		return usuario;
	}
}