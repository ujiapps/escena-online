package es.uji.apps.par.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.EventoBuilder;
import es.uji.apps.par.builders.LocalizacionBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.SesionBuilder;
import es.uji.apps.par.builders.TarifaBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.dao.ButacasDAO;
import es.uji.apps.par.dao.SesionesDAO;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.exceptions.AbonoUsadoMaxEnDistintosEventosException;
import es.uji.apps.par.exceptions.AbonoUsadoMaxEnEventoException;
import es.uji.apps.par.exceptions.ButacaOcupadaAlActivarException;
import es.uji.apps.par.exceptions.ButacaOcupadaException;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.CompraConCosteNoAnulableDesdeEnlace;
import es.uji.apps.par.exceptions.CompraNoExistente;
import es.uji.apps.par.exceptions.EmailBlacklistException;
import es.uji.apps.par.exceptions.EmailCompraNoCoincideException;
import es.uji.apps.par.exceptions.EmailVerificacionException;
import es.uji.apps.par.exceptions.EntradaConCosteNoAnulableDesdeEnlace;
import es.uji.apps.par.exceptions.EntradaNoExistente;
import es.uji.apps.par.exceptions.EntradasNoAnulablesDesdeEnlace;
import es.uji.apps.par.exceptions.LimiteEntradasGratisSuperadoException;
import es.uji.apps.par.exceptions.LimiteEntradasPorEmailSuperadoException;
import es.uji.apps.par.exceptions.NoAbonadoException;
import es.uji.apps.par.exceptions.NoHayButacasLibresException;
import es.uji.apps.par.exceptions.TiempoMinioRequeridoParaAnularDesdeEnlace;
import es.uji.apps.par.exceptions.UsuarioNoPuedeAnularException;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.model.Compra;
import es.uji.apps.par.model.CompraRequest;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.ResultadoCompra;
import es.uji.apps.par.model.Sesion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ComprasServiceTest extends BaseServiceTest {
    final String EMAIL_NO_ABONADO = "noabonado@gmail.com";

    @Autowired
    private ComprasService comprasService;

    @Autowired
    private EventosService eventosService;

    @Autowired
    private SesionesService sesionesService;

    @Autowired
    AbonadosService abonadosService;

    @Autowired
    SesionesDAO sesionesDAO;

    @Autowired
    ButacasDAO butacasDAO;

    @PersistenceContext
    protected EntityManager entityManager;

    @Test(expected = NoHayButacasLibresException.class)
    public void testNoHayButacasLibresConAforoDe20PorComprasDe2Exception() {
        testAforoPorButacas(11, 20, 2);
    }

    @Test(expected = NoHayButacasLibresException.class)
    public void testNoHayButacasLibresConAforo20PorComprasDe1Exception() {
        testAforoPorButacas(21, 20, 1);
    }

    @Test(expected = NoHayButacasLibresException.class)
    public void testNoHayButacasLibresConAforoDe20PorComprasDe2YUltima1Exception() {
        testAforoPorButacas(10, 20, 2);

        comprasService.realizaCompraInternet(this.sesionEventoNoNumerado.getId(),
            getNButacasNoNumeradasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1), null, usuario.getUsuario());
    }

    @Test
    public void testHayButacasLibresConAforoDe20PorComprasDe2Exception() {
        testAforoPorButacas(10, 20, 2);
    }

    @Test
    public void testHayButacasLibresConAforo20PorComprasDe1Exception() {
        testAforoPorButacas(20, 20, 1);
    }

    @Test(expected = NoHayButacasLibresException.class)
    public void testNoHayButacasLibresConAforoDe10PorComprasDe1Exception() {
        testAforoPorCompras(11, 10, 1);
    }

    @Test(expected = NoHayButacasLibresException.class)
    public void testNoHayButacasLibresConAforoDe10PorComprasDe4Exception() {
        testAforoPorCompras(11, 10, 4);
    }

    @Test(expected = NoHayButacasLibresException.class)
    public void testNoHayButacasLibresConAforoDe10PorComprasDe4YUltimaDe1Exception() {
        testAforoPorCompras(10, 10, 4);

        comprasService.realizaCompraInternet(this.sesionEventoNoNumerado.getId(),
            getNButacasNoNumeradasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1), null, usuario.getUsuario());
    }

    @Test
    public void testHayButacasLibresConAforoDe10PorComprasDe1Exception() {
        testAforoPorCompras(10, 10, 1);
    }

    @Test
    public void testHayButacasLibresConAforoDe10PorComprasDe4Exception() {
        testAforoPorCompras(10, 10, 4);
    }

    private void testAforoPorCompras(final int compras, final int aforo, final int butacasPorCompra) {
        testAforo(compras, aforo, butacasPorCompra, true);
    }

    private void testAforoPorButacas(final int compras, final int aforo, final int butacasPorCompra) {
        testAforo(compras, aforo, butacasPorCompra, false);
    }

    private void testAforo(final int compras, final int aforo, final int butacasPorCompra, final boolean aforoPorComprasHabilitado) {
        cine.setLimiteNoGratuitas(false);
        entityManager.merge(cine);
        sala.setAforoPorCompras(aforoPorComprasHabilitado);
        entityManager.merge(sala);
        localizacion.setTotalEntradas(new BigDecimal(aforo));
        entityManager.merge(localizacion);
        entityManager.flush();
        entityManager.clear();

        List<Butaca> nButacasNoNumeradasSeleccionadas =
            getNButacasNoNumeradasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, butacasPorCompra);
        for (int i = 0; i < compras; i++) {
            comprasService.realizaCompraInternet(this.sesionEventoNoNumerado.getId(),
                nButacasNoNumeradasSeleccionadas, null, usuario.getUsuario());
        }
    }

    @Test(expected = ButacaOcupadaException.class)
    public void testButacaOcupada() {
        List<Butaca> butacasSeleccionadasTarifaTaquilla = getButacasSeleccionadas(tarifaTaquilla, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasTarifaTaquilla,
                null, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        List<Butaca> butacasSeleccionadasPublicas = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasPublicas, null,
                usuario.getUsuario());
    }

    @Test
    public void testCalculoImporteConComisionDeSesionSinVentaAnticipada() {
        List<Butaca> butacasSeleccionadas = getButacasSeleccionadas(tarifaTaquilla, LOCALIZACION_CODE, 1);

        BigDecimal importe = comprasService
            .calculaImporteButacas(this.sesionSinVentaAnticipada.getId(), butacasSeleccionadas, true,
                usuario.getUsuario());

        Assert.assertNotNull(importe);
        Assert.assertEquals(new BigDecimal("2.08"), importe);

        List<Butaca> butacasSeleccionadasTarifaPublica = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        importe = comprasService
            .calculaImporteButacas(this.sesionSinVentaAnticipada.getId(), butacasSeleccionadasTarifaPublica, true,
                usuario.getUsuario());

        Assert.assertNotNull(importe);
        Assert.assertEquals(new BigDecimal("41.24"), importe);
    }

    @Test
    public void testCalculoImporteConComisionDeSesionConVentaAnticipadaActiva() {
        List<Butaca> butacasSeleccionadas = getButacasSeleccionadas(tarifaTaquilla, LOCALIZACION_CODE, 1);

        BigDecimal importe = comprasService
            .calculaImporteButacas(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadas, true,
                usuario.getUsuario());

        Assert.assertNotNull(importe);
        Assert.assertEquals(new BigDecimal("4.14"), importe);

        List<Butaca> butacasSeleccionadasTarifaPublica = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        importe = comprasService
            .calculaImporteButacas(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasTarifaPublica, true,
                usuario.getUsuario());

        Assert.assertNotNull(importe);
        Assert.assertEquals(new BigDecimal("20.62"), importe);
    }

    @Test
    public void testCalculoImporteConComision35MasIVA() {
        CineDTO cine2 = new CineBuilder("Cine 2").withComision(new BigDecimal(0.04235)).build(entityManager);
        TipoEventoDTO tipoEvento2 = new TipoEventoBuilder("tipo 2", "tipo 2", false, cine2).build(entityManager);
        SalaDTO sala2 = new SalaBuilder("Sala 2", cine2).build(entityManager);
        EventoDTO evento2 = new EventoBuilder("Evento 2", "Esdeveniment 2", cine2, tipoEvento2).build(entityManager);
        TarifaDTO tarifa2 = new TarifaBuilder("General").build(cine2, entityManager);
        LocalizacionDTO localizacion =
            new LocalizacionBuilder("platea", "PL", new BigDecimal(200)).withSala(sala2).build(entityManager);
        SesionDTO sesion2 =
            new SesionBuilder("Sesión 2", sala2, evento2).withPrecio(new BigDecimal(1), localizacion, tarifa2)
                .withVentaOnline().build(entityManager);
        UsuarioDTO usuario =
            new UsuarioBuilder("User 1", "user1@test.com", "user1").withSala(sala2).build(entityManager);

        List<Butaca> butacasSeleccionadas = Butaca.parseaJSON(
            String.format("[{fila: \"1\", numero: \"1\", tipo: \"%s\", localizacion: \"PL\"}]", tarifa2.getId()));

        BigDecimal importe =
            comprasService.calculaImporteButacas(sesion2.getId(), butacasSeleccionadas, true, usuario.getUsuario());

        Assert.assertNotNull(importe);
        Assert.assertEquals(importe, new BigDecimal("1.05"));
    }

    @Test
    public void testCalculoImporteSinComisionSinVentaAnticipada() {
        List<Butaca> butacasSeleccionadas = getButacasSeleccionadas(tarifaTaquilla, LOCALIZACION_CODE, 1);

        BigDecimal importe = comprasService
            .calculaImporteButacas(this.sesionSinVentaAnticipada.getId(), butacasSeleccionadas, false,
                usuario.getUsuario());

        Assert.assertNotNull(importe);
        Assert.assertEquals(importe, new BigDecimal("2.00"));

        List<Butaca> butacasSeleccionadasTarifaPublica = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        importe = comprasService
            .calculaImporteButacas(this.sesionSinVentaAnticipada.getId(), butacasSeleccionadasTarifaPublica, false,
                usuario.getUsuario());

        Assert.assertNotNull(importe);
        Assert.assertEquals(importe, new BigDecimal("40.00"));
    }

    @Test
    public void testCalculoImporteSinComisionConVentaAnticipada() {
        List<Butaca> butacasSeleccionadas = getButacasSeleccionadas(tarifaTaquilla, LOCALIZACION_CODE, 1);

        BigDecimal importe = comprasService
            .calculaImporteButacas(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadas, false,
                usuario.getUsuario());

        Assert.assertNotNull(importe);
        Assert.assertEquals(importe, new BigDecimal("4.00"));

        List<Butaca> butacasSeleccionadasTarifaPublica = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        importe = comprasService
            .calculaImporteButacas(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasTarifaPublica,
                false, usuario.getUsuario());

        Assert.assertNotNull(importe);
        Assert.assertEquals(importe, new BigDecimal("20.00"));
    }

    @Test
    public void testRealizaCompraInternetSinVentaAnticipada() {
        List<Butaca> butacasSeleccionadas = getButacasSeleccionadas(tarifaTaquilla, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionSinVentaAnticipada.getId(), butacasSeleccionadas, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        CompraDTO compraByUuid = comprasService.getCompraByUuid(resultadoCompra.getUuid());
        Assert.assertTrue(compraByUuid.getComision().compareTo(new BigDecimal("0.031")) == 0);

        Assert.assertEquals(compraByUuid.getImporte(), new BigDecimal("2.08"));
        Assert.assertEquals(compraByUuid.getImporteSinComision(), new BigDecimal("2.00"));

        for (ButacaDTO butacaDTO : compraByUuid.getParButacas()) {
            Assert.assertEquals(butacaDTO.getPrecio(), new BigDecimal("1.04"));
            Assert.assertEquals(butacaDTO.getPrecioSinComision(), new BigDecimal("1.00"));
        }

        List<Butaca> butacasSeleccionadasTarifaPublica = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 3);

        resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionSinVentaAnticipada.getId(), butacasSeleccionadasTarifaPublica, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        compraByUuid = comprasService.getCompraByUuid(resultadoCompra.getUuid());
        Assert.assertTrue(compraByUuid.getComision().compareTo(new BigDecimal("0.031")) == 0);

        Assert.assertEquals(compraByUuid.getImporte(), new BigDecimal("41.24"));
        Assert.assertEquals(compraByUuid.getImporteSinComision(), new BigDecimal("40.00"));

        for (ButacaDTO butacaDTO : compraByUuid.getParButacas()) {
            Assert.assertEquals(butacaDTO.getPrecio(), new BigDecimal("20.62"));
            Assert.assertEquals(butacaDTO.getPrecioSinComision(), new BigDecimal("20.00"));
        }
    }

    @Test
    public void testRealizaCompraInternetConVentaAnticipada() {
        List<Butaca> butacasSeleccionadasTarifaTaquilla = getButacasSeleccionadas(tarifaTaquilla, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasTarifaTaquilla,
                null, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        CompraDTO compraByUuid = comprasService.getCompraByUuid(resultadoCompra.getUuid());
        Assert.assertEquals(0, compraByUuid.getComision().compareTo(new BigDecimal("0.031")));

        Assert.assertEquals(new BigDecimal("4.14"), compraByUuid.getImporte());
        Assert.assertEquals(new BigDecimal("4.00"), compraByUuid.getImporteSinComision());

        for (ButacaDTO butacaDTO : compraByUuid.getParButacas()) {
            Assert.assertEquals(new BigDecimal("2.07"), butacaDTO.getPrecio());
            Assert.assertEquals(new BigDecimal("2.00"), butacaDTO.getPrecioSinComision());
        }

        List<Butaca> butacasSeleccionadasTarifaPublica = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 3);

        resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasTarifaPublica, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        compraByUuid = comprasService.getCompraByUuid(resultadoCompra.getUuid());
        Assert.assertTrue(compraByUuid.getComision().compareTo(new BigDecimal("0.031")) == 0);

        Assert.assertEquals(compraByUuid.getImporte(), new BigDecimal("20.62"));
        Assert.assertEquals(compraByUuid.getImporteSinComision(), new BigDecimal("20.00"));

        for (ButacaDTO butacaDTO : compraByUuid.getParButacas()) {
            Assert.assertEquals(butacaDTO.getPrecio(), new BigDecimal("10.31"));
            Assert.assertEquals(butacaDTO.getPrecioSinComision(), new BigDecimal("10.00"));
        }
    }

    @Test
    public void testRealizaCompraInternetCon35MasIVA() {
        List<Butaca> butacasSeleccionadas = Butaca.parseaJSON(String.format(
            "[{fila: \"1\", numero: \"1\", tipo: \"%s\", localizacion: \"PL\"}, {fila: \"1\", numero: \"2\", tipo: \"%s\", localizacion: \"PL\"}]",
            tarifaGeneralCine2.getId(), tarifaGeneralCine2.getId()));

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion2.getId(), butacasSeleccionadas, null, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        CompraDTO compraByUuid = comprasService.getCompraByUuid(resultadoCompra.getUuid());
        Assert.assertTrue(compraByUuid.getComision().compareTo(new BigDecimal("0.04235")) == 0);

        Assert.assertEquals(compraByUuid.getImporte(), new BigDecimal("41.70"));
        Assert.assertEquals(compraByUuid.getImporteSinComision(), new BigDecimal("40.00"));

        for (ButacaDTO butacaDTO : compraByUuid.getParButacas()) {
            Assert.assertEquals(butacaDTO.getPrecio(), new BigDecimal("20.85"));
            Assert.assertEquals(butacaDTO.getPrecioSinComision(), new BigDecimal("20.00"));
        }
    }

    @Test
    public void testRealizaCompraTaquillaSinVentaAnticipada() {
        List<Butaca> butacasSeleccionadas = Butaca.parseaJSON(String
            .format("[{fila: \"1\", numero: \"1\", tipo: \"%s\", localizacion: \"%s\"}]", tarifaGeneralCine2.getId(),
                LOCALIZACION_CODE));

        CompraRequest compraRequest = new CompraRequest();
        compraRequest.setButacasSeleccionadas(butacasSeleccionadas);
        ResultadoCompra resultadoCompra =
            comprasService.registraCompraTaquilla(this.sesion2.getId(), compraRequest, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        CompraDTO compraByUuid = comprasService.getCompraByUuid(resultadoCompra.getUuid());
        Assert.assertTrue(compraByUuid.getComision().compareTo(BigDecimal.ZERO) == 0);

        Assert.assertEquals(compraByUuid.getImporte(), new BigDecimal("20.00"));
        Assert.assertEquals(compraByUuid.getImporteSinComision(), new BigDecimal("20.00"));

        for (ButacaDTO butacaDTO : compraByUuid.getParButacas()) {
            Assert.assertEquals(butacaDTO.getPrecio(), new BigDecimal("20.00"));
            Assert.assertEquals(butacaDTO.getPrecioSinComision(), new BigDecimal("20.00"));
        }
    }

    @Test
    public void testRealizaCompraTaquillaConVentaAnticipadaActiva() {
        List<Butaca> butacasSeleccionadas = getButacasSeleccionadas(tarifaTaquilla, LOCALIZACION_CODE, 1);

        CompraRequest compraRequest = new CompraRequest();
        compraRequest.setButacasSeleccionadas(butacasSeleccionadas);
        ResultadoCompra resultadoCompra = comprasService
            .registraCompraTaquilla(this.sesionConVentaAnticipadaActiva.getId(), compraRequest, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        CompraDTO compraByUuid = comprasService.getCompraByUuid(resultadoCompra.getUuid());
        Assert.assertTrue(compraByUuid.getComision().compareTo(BigDecimal.ZERO) == 0);

        Assert.assertEquals(compraByUuid.getImporte(), new BigDecimal("4.00"));
        Assert.assertEquals(compraByUuid.getImporteSinComision(), new BigDecimal("4.00"));

        for (ButacaDTO butacaDTO : compraByUuid.getParButacas()) {
            Assert.assertEquals(butacaDTO.getPrecio(), new BigDecimal("2.00"));
            Assert.assertEquals(butacaDTO.getPrecioSinComision(), new BigDecimal("2.00"));
        }

        List<Butaca> butacasSeleccionadasTarifaPublica = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 3);

        compraRequest = new CompraRequest();
        compraRequest.setButacasSeleccionadas(butacasSeleccionadasTarifaPublica);
        resultadoCompra = comprasService
            .registraCompraTaquilla(this.sesionConVentaAnticipadaActiva.getId(), compraRequest, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        compraByUuid = comprasService.getCompraByUuid(resultadoCompra.getUuid());
        Assert.assertTrue(compraByUuid.getComision().compareTo(BigDecimal.ZERO) == 0);

        Assert.assertEquals(compraByUuid.getImporte(), new BigDecimal("20.00"));
        Assert.assertEquals(compraByUuid.getImporteSinComision(), new BigDecimal("20.00"));

        for (ButacaDTO butacaDTO : compraByUuid.getParButacas()) {
            Assert.assertEquals(butacaDTO.getPrecio(), new BigDecimal("10.00"));
            Assert.assertEquals(butacaDTO.getPrecioSinComision(), new BigDecimal("10.00"));
        }
    }

    @Test
    public void testRealizaReservaSinVentaAnticipada() {
        realizaReservaConVentaAnticipadaActiva(this.sesionSinVentaAnticipada);
    }

    @Test
    public void testRealizaReservaConVentaAnticipadaActiva() {
        realizaReservaConVentaAnticipadaActiva(this.sesionConVentaAnticipadaActiva);
    }

    private void realizaReservaConVentaAnticipadaActiva(SesionDTO sesion) {
        List<Butaca> butacasSeleccionadas = getButacasSeleccionadas(tarifaTaquilla, LOCALIZACION_CODE, 1);

        Date today = new Date();
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.setTime(today);
        tomorrow.add(Calendar.DATE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .reservaButacas(sesion.getId(), today, tomorrow.getTime(), butacasSeleccionadas, "", 0, 0, 0, 0,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        CompraDTO compraByUuid = comprasService.getCompraByUuid(resultadoCompra.getUuid());
        Assert.assertTrue(compraByUuid.getComision().compareTo(BigDecimal.ZERO) == 0);

        Assert.assertEquals(compraByUuid.getImporte(), new BigDecimal("0.00"));
        Assert.assertEquals(compraByUuid.getImporteSinComision(), new BigDecimal("0.00"));

        List<Butaca> butacasSeleccionadasTarifaPublica = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 3);

        resultadoCompra = comprasService
            .reservaButacas(sesion.getId(), today, tomorrow.getTime(), butacasSeleccionadasTarifaPublica, "", 0, 0, 0,
                0, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);

        compraByUuid = comprasService.getCompraByUuid(resultadoCompra.getUuid());
        Assert.assertTrue(compraByUuid.getComision().compareTo(BigDecimal.ZERO) == 0);

        Assert.assertEquals(compraByUuid.getImporte(), new BigDecimal("0.00"));
        Assert.assertEquals(compraByUuid.getImporteSinComision(), new BigDecimal("0.00"));
    }

    @Test(expected = CampoRequeridoException.class)
    public void testRellenaDatosCompradorSinDatos() {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService
            .rellenaDatosComprador(resultadoCompra.getUuid(), "", "", "", "", "", "", "", "", "", null, "", "on", false,
                "", null, "", null);
    }

    @Test(expected = CampoRequeridoException.class)
    public void testRellenaDatosCompradorSinCheckCondiciones() {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_ABONADO, EMAIL_ABONADO, null, "", "", false, "", null, "", null);
    }

    @Test(expected = CampoRequeridoException.class)
    public void testRellenaDatosCompradorSinCheckCondicionesCancelacion() {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_ABONADO, EMAIL_ABONADO, null, "", "on", false, "", "http://test.com", "", null);
    }

    @Test(expected = CampoRequeridoException.class)
    public void testRellenaDatosCompradorSinEmail() {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService
            .rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000", "", "",
                null, "", "on", false, "", null, "", null);
    }

    @Test(expected = CampoRequeridoException.class)
    public void testRellenaDatosCompradorContratoGeneralSinCheck() {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_ABONADO, EMAIL_ABONADO, null, "", "on", true, "", null, "", null);
    }

    @Test(expected = EmailVerificacionException.class)
    public void testRellenaDatosCompradorEmailVerificacionIncorrecto() {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_ABONADO, "otromailinventado@gmail.com", null, "", "on", false, "", null, "", null);
    }

    @Test
    public void testRellenaDatosComprador() {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        CompraDTO compraDTO = comprasService
            .rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
                EMAIL_ABONADO, EMAIL_ABONADO, null, "", "on", true, "on", "http://test.com", "on", null);

        CompraDTO compraById = comprasService.getCompraById(compraDTO.getId());

        Assert.assertNotNull(compraById);
    }

    @Test
    public void testRellenaDatosCompradorActualizaFechaCompra() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -500);
        Timestamp fecha500minutosAntes = new Timestamp(calendar.getTimeInMillis());

        new SesionBuilder("test compra pasada", sala, evento, "18:00")
            .withCompra("Sergio", BigDecimal.valueOf(10), 2, tarifaGeneral, localizacion, "test-pendiente-500",
                fecha500minutosAntes, false).build(entityManager);

        CompraDTO compraDTO = comprasService.getCompraByUuid("test-pendiente-500");

        comprasService
            .rellenaDatosComprador(compraDTO.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000", EMAIL_ABONADO,
                EMAIL_ABONADO, null, "", "on", false, "", null, "", null);

        CompraDTO compraById = comprasService.getCompraById(compraDTO.getId());

        Calendar fechaCompra = Calendar.getInstance();
        Calendar fechaActual = Calendar.getInstance();
        fechaCompra.setTime(compraById.getFecha());
        fechaActual.setTime(new Date());

        Assert.assertEquals(fechaActual.get(Calendar.DAY_OF_YEAR), fechaCompra.get(Calendar.DAY_OF_YEAR));
        Assert.assertEquals(fechaActual.get(Calendar.HOUR_OF_DAY), fechaCompra.get(Calendar.HOUR_OF_DAY));
    }

    @Test(expected = LimiteEntradasGratisSuperadoException.class)
    public void testRellenaDatosCompradorSuperaLimiteEntradasCine() {
        List<Butaca> butacasSeleccionadas = getNButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1, 11);

        comprasService.realizaCompraInternet(this.sesionConVentaAnticipadaCaducada.getId(), butacasSeleccionadas, null,
            usuario.getUsuario());
    }

    @Test
    public void testNoNumeradoSecuencia() {
        List<Butaca> butacasSeleccionadas = getNButacasNoNumeradasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 4);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionEventoNoNumerado.getId(), butacasSeleccionadas, null,
                usuario.getUsuario());
        CompraDTO compra = comprasService.getCompraById(resultadoCompra.getId());
        for (ButacaDTO butaca : compra.getParButacas()) {
            Assert.assertNotNull(butaca.getNumero());
        }
    }

    @Test
    public void testNoNumeradoSecuenciaConAnuladas() {
        List<Butaca> butacasSeleccionadas = getNButacasNoNumeradasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 4);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionEventoNoNumerado.getId(), butacasSeleccionadas, null,
                usuario.getUsuario());
        CompraDTO compra = comprasService.getCompraById(resultadoCompra.getId());
        comprasService.anularCompra(compra.getId());

        butacasSeleccionadas = getNButacasNoNumeradasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);
        resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionEventoNoNumerado.getId(), butacasSeleccionadas, null,
                usuario.getUsuario());
        compra = comprasService.getCompraById(resultadoCompra.getId());
        Assert.assertTrue(compra.getParButacas().size() == 1);
        Assert.assertTrue(compra.getParButacas().get(0).getNumero().equals("1"));
    }

    @Test
    public void testRellenaDatosCompradorSuperaLimiteEntradasCinePeroEstaActivaLimitacionEntradasPorEmail() {
        List<Butaca> butacasSeleccionadas = getNButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1, 11);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion1EventoLimiteEntradasPorEmail.getId(), butacasSeleccionadas, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());
    }

    @Test(expected = LimiteEntradasPorEmailSuperadoException.class)
    public void testRellenaDatosCompradorSuperaLimiteEntradasPorEmail() {
        List<Butaca> butacasSeleccionadas = getNButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1, 16);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion1EventoLimiteEntradasPorEmail.getId(), butacasSeleccionadas, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_NO_ABONADO, EMAIL_NO_ABONADO, null, "", "on", false, "", null, "", null);
    }

    @Test(expected = LimiteEntradasPorEmailSuperadoException.class)
    public void testRellenaDatosCompradorSuperaLimiteEntradasPorEmailDiferentesSesionesMismoEvento() {
        List<Butaca> butacasSeleccionadas = getNButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1, 7);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion1EventoLimiteEntradasPorEmail.getId(), butacasSeleccionadas, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_NO_ABONADO, EMAIL_NO_ABONADO, null, "", "on", false, "", null, "", null);

        butacasSeleccionadas = getNButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1, 9);

        resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion2EventoLimiteEntradasPorEmail.getId(), butacasSeleccionadas, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_NO_ABONADO, EMAIL_NO_ABONADO, null, "", "on", false, "", null, "", null);
    }

    @Test
    public void testRellenaDatosCompradorNoSuperaLimiteEntradasPorEmailPorAnulacion() {
        List<Butaca> butacasSeleccionadas = getNButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1, 2);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion1EventoLimiteEntradasPorEmail.getId(), butacasSeleccionadas, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_NO_ABONADO, EMAIL_NO_ABONADO, null, "", "on", false, "", null, "", null);

        comprasService.anularCompraReserva(resultadoCompra.getId(), usuario.getUsuario());

        butacasSeleccionadas = getNButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1, 1);

        resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion2EventoLimiteEntradasPorEmail.getId(), butacasSeleccionadas, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_NO_ABONADO, EMAIL_NO_ABONADO, null, "", "on", false, "", null, "", null);
    }

    @Test
    public void compra2AbonosConMailDeNoAbonado() throws ParseException {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ExtGridFilterList extGridFilterList = ExtGridFilterList.fromString(String
            .format("[{\"property\": \"tituloEs\", \"operator\": \"=\", \"value\":\"%s\"}]", NOMBRE_ABONO_JUVILADO));
        List<Evento> abonos = eventosService
            .getAbonos("[{\"property\":\"fechaPrimeraSesion\",\"direction\":\"ASC\"}]", 0, Integer.MAX_VALUE,
                extGridFilterList, usuario.getUsuario(), cine.getId());

        List<Sesion> sesiones = sesionesService.getSesiones(abonos.get(0).getId(), usuario.getUsuario());

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(sesiones.get(0).getId(), butacasSeleccionadasAbono, null, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        CompraDTO compra = comprasService
            .rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
                EMAIL_NO_ABONADO, EMAIL_NO_ABONADO, null, "", "on", false, "", null, "", null);
        Assert.assertNotNull(compra);
        Assert.assertEquals(EMAIL_NO_ABONADO, compra.getEmail());

        comprasService.marcaPagadaPasarela(compra.getId(), "TEST");

        entityManager.flush();
        entityManager.clear();

        int numeroAbonosByAbonoIdAndEmail =
            abonadosService.getNumeroAbonosByAbonoIdAndEmail(abonoJuvilado.getId(), EMAIL_NO_ABONADO);
        Assert.assertEquals(2, numeroAbonosByAbonoIdAndEmail);
    }

    @Test
    public void compra2AbonoConMailDeYaAbonado() throws ParseException {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ExtGridFilterList extGridFilterList = ExtGridFilterList.fromString(String
            .format("[{\"property\": \"tituloEs\", \"operator\": \"=\", \"value\":\"%s\"}]", NOMBRE_ABONO_JUVILADO));
        List<Evento> abonos = eventosService
            .getAbonos("[{\"property\":\"fechaPrimeraSesion\",\"direction\":\"ASC\"}]", 0, Integer.MAX_VALUE,
                extGridFilterList, usuario.getUsuario(), cine.getId());

        List<Sesion> sesiones = sesionesService.getSesiones(abonos.get(0).getId(), usuario.getUsuario());

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(sesiones.get(0).getId(), butacasSeleccionadasAbono, null, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        CompraDTO compra = comprasService
            .rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
                EMAIL_ABONADO, EMAIL_ABONADO, null, "", "on", false, "", null, "", null);
        Assert.assertNotNull(compra);
        Assert.assertEquals(EMAIL_ABONADO, compra.getEmail());

        comprasService.marcaPagadaPasarela(compra.getId(), "TEST");

        entityManager.flush();
        entityManager.clear();

        int numeroAbonosByAbonoIdAndEmail =
            abonadosService.getNumeroAbonosByAbonoIdAndEmail(abonoJuvilado.getId(), EMAIL_ABONADO);
        Assert.assertEquals(3, numeroAbonosByAbonoIdAndEmail);
    }

    @Test
    public void testRellenaDatosCompradorConTarifaGeneralConEmailNoAbonado() {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        CompraDTO compra = comprasService
            .rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
                EMAIL_NO_ABONADO, EMAIL_NO_ABONADO, null, "", "on", false, "", null, "", null);
        Assert.assertNotNull(compra);
        Assert.assertEquals(EMAIL_NO_ABONADO, compra.getEmail());
    }

    @Test(expected = NoAbonadoException.class)
    public void testRellenaDatosCompradorConTarifaAbonoConEmailNoAbonado() {
        List<Butaca> butacasSeleccionadasAbono = getNButacasSeleccionadas(abonoJuvilado, LOCALIZACION_CODE, 1, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_NO_ABONADO, EMAIL_NO_ABONADO, null, "", "on", false, "", null, "", null);
    }

    @Test
    public void testRellenaDatosCompradorConTarifaAbonoConEmailAbonado() {
        List<Butaca> butacasSeleccionadasAbono = getNButacasSeleccionadas(abonoJuvilado, LOCALIZACION_CODE, 1, 1);
        butacasSeleccionadasAbono.addAll(getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 3));

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        CompraDTO compra = comprasService
            .rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
                EMAIL_ABONADO, EMAIL_ABONADO, null, "", "on", false, "", null, "", null);
        Assert.assertNotNull(compra);
        Assert.assertEquals(EMAIL_ABONADO, compra.getEmail());
    }

    @Test(expected = AbonoUsadoMaxEnEventoException.class)
    public void testRellenaDatosCompradorConTarifaAbono2ComprasMismaSesionConEmailAbonado() {
        List<Butaca> butacasSeleccionadasAbono = getNButacasSeleccionadas(abono2Usos, LOCALIZACION_CODE, 1, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion2.getId(), butacasSeleccionadasAbono, null, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_ABONADO, EMAIL_ABONADO, null, "", "on", false, "", null, "", null);

        butacasSeleccionadasAbono = getNButacasSeleccionadas(abono2Usos, LOCALIZACION_CODE, 2, 1);

        resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion2.getId(), butacasSeleccionadasAbono, null, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_ABONADO, EMAIL_ABONADO, null, "", "on", false, "", null, "", null);
    }

    @Test(expected = AbonoUsadoMaxEnDistintosEventosException.class)
    public void testRellenaDatosCompradorConTarifaAbonoMaximo2EventosConEmailAbonado() {
        List<Butaca> butacasSeleccionadasAbono = getNButacasSeleccionadas(abono2Usos, LOCALIZACION_CODE, 1, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion2.getId(), butacasSeleccionadasAbono, null, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_ABONADO, EMAIL_ABONADO, null, "", "on", false, "", null, "", null);

        resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion3.getId(), butacasSeleccionadasAbono, null, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_ABONADO, EMAIL_ABONADO, null, "", "on", false, "", null, "", null);

        resultadoCompra = comprasService
            .realizaCompraInternet(this.sesion4.getId(), butacasSeleccionadasAbono, null, usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_ABONADO, EMAIL_ABONADO, null, "", "on", false, "", null, "", null);
    }

    @Test
    public void testComprasBySesion() {
        List<Compra> compras = comprasService.getComprasBySesion(sesionConCompras.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(compras);
        assertEquals(4, compras.size());
    }

    @Test(expected = UsuarioNoPuedeAnularException.class)
    public void testAnularComprasSesionSinPermisoAnulacion() {
        List<Compra> compras = comprasService.getComprasBySesion(sesionConCompras.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(compras);
        assertEquals(4, compras.size());

        for (Compra compra : compras) {
            comprasService.anularCompraReserva(compra.getId(), usuarioSinPermisoAnular.getUsuario());
        }
    }

    @Test
    public void testAnularReservaSesionSinPermisoAnulacion() {
        List<Compra> compras = comprasService.getComprasBySesion(sesionConCompras.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(compras);
        assertEquals(4, compras.size());

        for (Compra compra : compras) {
            if (compra.isReserva())
                comprasService.anularCompraReserva(compra.getId(), usuario.getUsuario());
        }

        compras = comprasService.getComprasBySesion(sesionConCompras.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(compras);
        assertEquals(3, compras.size());

        List<Compra> comprasAnuladas =
            comprasService.getComprasBySesion(sesionConCompras.getId(), 1, 1, "", "id", 0, 10, null);
        assertNotNull(comprasAnuladas);
        assertEquals(4, comprasAnuladas.size());
    }

    @Test
    public void testAnularComprasSesion() {
        List<Compra> compras = comprasService.getComprasBySesion(sesionConCompras.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(compras);
        assertEquals(4, compras.size());

        for (Compra compra : compras) {
            comprasService.anularCompraReserva(compra.getId(), usuario.getUsuario());
        }

        compras = comprasService.getComprasBySesion(sesionConCompras.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(compras);
        assertEquals(0, compras.size());

        List<Compra> comprasAnuladas =
            comprasService.getComprasBySesion(sesionConCompras.getId(), 1, 1, "", "id", 0, 10, null);
        assertNotNull(comprasAnuladas);
        assertEquals(4, comprasAnuladas.size());
    }

    @Test(expected = UsuarioNoPuedeAnularException.class)
    public void testAnularButacaCompraSesionSinPermisoAnulacion() {
        CompraDTO compraDTO = comprasService.getCompraByUuid("test-uuid-1");
        assertNotNull(compraDTO);

        List<Long> idsButacas = compraDTO.getParButacas().stream().map(ButacaDTO::getId).collect(Collectors.toList());

        comprasService.anularButacas(idsButacas, usuarioSinPermisoAnular.getUsuario());
    }

    @Test
    public void testAnularButacaReservaSesionSinPermisoAnulacion() {
        CompraDTO compraDTO = comprasService.getCompraByUuid("test-reserva-1");
        assertNotNull(compraDTO);

        List<Long> idsButacas = compraDTO.getParButacas().stream().map(ButacaDTO::getId).collect(Collectors.toList());

        comprasService.anularButacas(idsButacas, usuarioSinPermisoAnular.getUsuario());

        entityManager.flush();
        entityManager.clear();
        compraDTO = comprasService.getCompraByUuid("test-reserva-1");
        Assert.assertTrue(compraDTO.getAnulada());

        for (ButacaDTO parButaca : compraDTO.getParButacas()) {
            Assert.assertTrue(parButaca.getAnulada());
        }

        List<Compra> compras = comprasService.getComprasBySesion(sesionConCompras.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(compras);
        assertEquals(3, compras.size());
    }

    @Test
    public void testAnularButacaCompraSesion() {
        CompraDTO compraDTO = comprasService.getCompraByUuid("test-uuid-1");
        assertNotNull(compraDTO);

        List<Long> idsButacas = compraDTO.getParButacas().stream().map(ButacaDTO::getId).collect(Collectors.toList());
        comprasService.anularButacas(idsButacas, usuario.getUsuario());

        entityManager.flush();
        entityManager.clear();
        compraDTO = comprasService.getCompraByUuid("test-uuid-1");
        Assert.assertTrue(compraDTO.getAnulada());

        for (ButacaDTO parButaca : compraDTO.getParButacas()) {
            Assert.assertTrue(parButaca.getAnulada());
        }

        List<Compra> compras = comprasService.getComprasBySesion(sesionConCompras.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(compras);
        assertEquals(3, compras.size());
    }

    @Test
    public void testAnularButacaIndividualCompraSesion() {
        CompraDTO compraDTO = comprasService.getCompraByUuid("test-uuid-1");
        assertNotNull(compraDTO);

        Long idsButacas = compraDTO.getParButacas().stream().map(ButacaDTO::getId).findFirst().get();
        comprasService.anularButacas(Arrays.asList(idsButacas), usuario.getUsuario());

        entityManager.flush();
        entityManager.clear();
        compraDTO = comprasService.getCompraByUuid("test-uuid-1");
        Assert.assertFalse(compraDTO.getAnulada());

        for (ButacaDTO parButaca : compraDTO.getParButacas()) {
            if (parButaca.getId() == idsButacas) {
                Assert.assertTrue(parButaca.getAnulada());
            } else {
                Assert.assertFalse(parButaca.getAnulada());
            }
        }

        List<Compra> compras = comprasService.getComprasBySesion(sesionConCompras.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(compras);
        assertEquals(4, compras.size());
    }

    @Test
    public void testAnularButacaIndividualTraEliminacionPreciosSesion() {
        CompraDTO compraDTO = comprasService.getCompraByUuid("test-uuid-1");
        assertNotNull(compraDTO);
        sesionesDAO.deleteExistingPreciosSesion(compraDTO.getParSesion().getId());

        Long idsButacas = compraDTO.getParButacas().stream().map(ButacaDTO::getId).findFirst().get();
        comprasService.anularButacas(Arrays.asList(idsButacas), usuario.getUsuario());

        entityManager.flush();
        entityManager.clear();
        compraDTO = comprasService.getCompraByUuid("test-uuid-1");
        Assert.assertFalse(compraDTO.getAnulada());

        for (ButacaDTO parButaca : compraDTO.getParButacas()) {
            if (parButaca.getId() == idsButacas) {
                Assert.assertTrue(parButaca.getAnulada());
            } else {
                Assert.assertFalse(parButaca.getAnulada());
            }
        }

        List<Compra> compras = comprasService.getComprasBySesion(sesionConCompras.getId(), 0, 1, "", "id", 0, 10, null);
        assertNotNull(compras);
        assertEquals(4, compras.size());
    }

    @Test
    public void testDesanularButacaCompraSesion() {
        CompraDTO compraDTO = comprasService.getCompraByUuid("test-uuid-1");
        assertNotNull(compraDTO);

        comprasService.anularCompraReserva(compraDTO.getId(), usuario.getUsuario());
        entityManager.flush();
        entityManager.clear();
        compraDTO = comprasService.getCompraByUuid("test-uuid-1");
        Assert.assertTrue(compraDTO.getAnulada());
        comprasService.desanularCompraReserva(compraDTO.getId(), usuario.getUsuario());
        entityManager.flush();
        entityManager.clear();
        compraDTO = comprasService.getCompraByUuid("test-uuid-1");
        Assert.assertFalse(compraDTO.getAnulada());
    }

    @Test(expected = ButacaOcupadaAlActivarException.class)
    public void testDesanularButacaYaCompradaEnOtraCompraCompra() {
        List<Butaca> butacasSeleccionadasPublicas = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);
        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionSinVentaAnticipada.getId(), butacasSeleccionadasPublicas, null,
                usuario.getUsuario());

        Assert.assertTrue(resultadoCompra.getCorrecta());

        comprasService.anularCompraReserva(resultadoCompra.getId(), usuario.getUsuario());
        Long compraAnuladaId = resultadoCompra.getId();
        entityManager.flush();
        entityManager.clear();
        CompraDTO compraAnulada = comprasService.getCompraById(compraAnuladaId);
        Assert.assertTrue(compraAnulada.getAnulada());

        resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionSinVentaAnticipada.getId(), butacasSeleccionadasPublicas, null,
                usuario.getUsuario());

        Assert.assertTrue(resultadoCompra.getCorrecta());

        comprasService.desanularCompraReserva(compraAnuladaId, usuario.getUsuario());
    }

    @Test(expected = ButacaOcupadaException.class)
    public void testButacaOcupadaEnOtraCompraCompra() {
        List<Butaca> butacasSeleccionadasPublicas = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);
        comprasService.realizaCompraInternet(this.sesionSinVentaAnticipada.getId(), butacasSeleccionadasPublicas, null,
            usuario.getUsuario());

        comprasService.realizaCompraInternet(this.sesionSinVentaAnticipada.getId(), butacasSeleccionadasPublicas, null,
            usuario.getUsuario());
    }

    @Test(expected = EmailBlacklistException.class)
    public void testEmailInBlacklist() {
        List<Butaca> butacasSeleccionadasAbono = getButacasSeleccionadas(tarifaGeneral, LOCALIZACION_CODE, 1);

        ResultadoCompra resultadoCompra = comprasService
            .realizaCompraInternet(this.sesionConVentaAnticipadaActiva.getId(), butacasSeleccionadasAbono, null,
                usuario.getUsuario());

        Assert.assertNotNull(resultadoCompra);
        Assert.assertNotNull(resultadoCompra.getUuid());

        comprasService.rellenaDatosComprador(resultadoCompra.getUuid(), "Sergio", "Gragera", "", "", "", "", "000000",
            EMAIL_BLACKLIST, EMAIL_BLACKLIST, null, "", "on", false, "", null, "", null);
    }

    @Test(expected = CompraNoExistente.class)
    public void shouldThrowCompraNoExistenteWhenAnulaCompraDesdeEnlaceWithUuidNoExistente()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        comprasService.anularCompraDesdeMail("uuid-que-no-existe", "inventado@4tic.com");
    }

    @Test(expected = CompraNoExistente.class)
    public void shouldThrowCompraNoExistenteWhenAnulaButacaDesdeEnlaceWithUuidNoExistente()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        comprasService.anularButacaDesdeMail("uuid-que-no-existe", 1, "inventado@4tic.com");
    }

    @Test(expected = EntradaNoExistente.class)
    public void shouldThrowEntradaNoExistenteWhenAnulaButacaDesdeEnlaceWithButacaIdNoExistente()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        comprasService.anularButacaDesdeMail("test-uuid-1", 1000, "sergio@test.com");
    }

    @Test(expected = EntradasNoAnulablesDesdeEnlace.class)
    public void shouldThrowEntradasNoAnulablesDesdeEnlaceWhenAnulaCompraDesdeEnlaceWithCineWithoutCancelFromLinkEnabled()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        cine.setAnulableDesdeEnlace(false);
        entityManager.merge(cine);
        entityManager.flush();
        entityManager.clear();

        comprasService.anularCompraDesdeMail("test-uuid-1", "inventado@4tic.com");
    }

    @Test(expected = EntradasNoAnulablesDesdeEnlace.class)
    public void shouldThrowEntradasNoAnulablesDesdeEnlaceWhenAnulaButacaDesdeEnlaceWithCineWithoutCancelFromLinkEnabled()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        cine.setAnulableDesdeEnlace(false);
        entityManager.merge(cine);
        entityManager.flush();
        entityManager.clear();

        comprasService.anularButacaDesdeMail("test-uuid-1", 1000, "sergio@test.com");
    }

    @Test(expected = EmailCompraNoCoincideException.class)
    public void shouldThrowEmailCompraNoCoincideExceptionWhenAnulaCompraDesdeEnlaceWithCineWithoutCompraEmail()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        comprasService.anularCompraDesdeMail("test-uuid-1", "inventado@4tic.com");
    }

    @Test(expected = EmailCompraNoCoincideException.class)
    public void shouldThrowEmailCompraNoCoincideExceptionWhenAnulaButacaDesdeEnlaceWithCineWithoutCompraEmail()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        ButacaDTO butacaDTO = comprasService.getCompraByUuid("test-uuid-1").getParButacas().get(0);
        comprasService.anularButacaDesdeMail("test-uuid-1", butacaDTO.getId(), "inventado@4tic.com");
    }

    @Test(expected = CompraConCosteNoAnulableDesdeEnlace.class)
    public void shouldThrowCompraConCosteNoAnulableDesdeEnlaceExceptionWhenAnulaCompraConCoste()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        comprasService.anularCompraDesdeMail("test-uuid-1", "sergio@test.com");
    }

    @Test(expected = EntradaConCosteNoAnulableDesdeEnlace.class)
    public void shouldThrowEntradaConCosteNoAnulableDesdeEnlaceExceptionWhenAnulaButacaConCoste()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        ButacaDTO butacaDTO = comprasService.getCompraByUuid("test-uuid-1").getParButacas().get(0);
        comprasService.anularButacaDesdeMail("test-uuid-1", butacaDTO.getId(), "sergio@test.com");
    }

    @Test(expected = TiempoMinioRequeridoParaAnularDesdeEnlace.class)
    public void shouldThrowTiempoMinioRequeridoParaAnularDesdeEnlaceExceptionWhenDateStartEventoIsLessThanTiempoRestanteCancelableDesdeEnlace()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        CompraDTO compra = comprasService.getCompraByUuid("test-uuid-1");
        Timestamp fechaCelebracion = compra.getParSesion().getFechaCelebracion();
        long diffTime = fechaCelebracion.getTime() - new Date().getTime();

        cine.setTiempoRestanteAnulableDesdeEnlace(new Long((diffTime - 1l) / 1000 / 60).intValue());
        entityManager.merge(cine);
        entityManager.flush();
        entityManager.clear();

        comprasService.anularCompraDesdeMail("test-uuid-1", "sergio@test.com");
    }

    @Test
    public void shouldAnulaCompraWhenAnulaCompraDesdeEnlace()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        comprasService.anularCompraDesdeMail("test-uuid-gratis", "gratis@test.com");

        entityManager.flush();
        entityManager.clear();

        CompraDTO compra = comprasService.getCompraByUuid("test-uuid-gratis");
        Assert.assertTrue(compra.getAnulada());
    }

    @Test
    public void shouldAnulaEntradaWhenAnulaButacaDesdeEnlace()
        throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        ButacaDTO butacaDTO = comprasService.getCompraByUuid("test-uuid-gratis").getParButacas().get(0);
        comprasService.anularButacaDesdeMail("test-uuid-gratis", butacaDTO.getId(), "gratis@test.com");

        entityManager.flush();
        entityManager.clear();

        CompraDTO compra = comprasService.getCompraByUuid("test-uuid-gratis");
        Assert.assertFalse(compra.getAnulada());
        Assert.assertTrue(
            compra.getParButacas().stream().filter(b -> b.getId() == butacaDTO.getId()).map(ButacaDTO::getAnulada)
                .findAny().orElse(false));
    }
}
