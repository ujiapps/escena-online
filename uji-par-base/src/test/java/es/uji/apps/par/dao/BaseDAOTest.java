package es.uji.apps.par.dao;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.TpvBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TpvsDTO;
import es.uji.apps.par.db.UsuarioDTO;

public class BaseDAOTest
{
    @Autowired
    EventosDAO eventosDao;
    
    @Autowired
    SesionesDAO sesionesDao;

    @Autowired
    LocalizacionesDAO localizacionesDao;

    @Autowired
    TarifasDAO tarifasDAO;

    @Autowired
    protected CinesDAO cinesDAO;

    @Autowired
    private SalasDAO salasDao;

    @Autowired
    TiposEventosDAO tiposEventosDAO;

    @Autowired
    private UsuariosDAO usuariosDAO;

    @PersistenceContext
    protected EntityManager entityManager;

    CineDTO cine1;
    UsuarioDTO usuario1;
    SalaDTO sala1;
    TpvsDTO tpv;

    @Before
    public void setUp()
    {
        cine1 = new CineBuilder("Cine 1")
            .build(entityManager);

        sala1 = new SalaBuilder("Sala 1", cine1)
            .build(entityManager);

        tpv = new TpvBuilder("ejemplo").withSala(sala1).build(entityManager);

        usuario1 = new UsuarioBuilder("User 1", "user1@test.com", "user1")
            .withSala(sala1)
            .build(entityManager);


        entityManager.flush();
        entityManager.clear();
    }

    protected ButacaDTO preparaButaca(SesionDTO sesion, LocalizacionDTO localizacion, String fila, String numero,
            BigDecimal precio, BigDecimal precioSinComision)
    {
        ButacaDTO butacaDTO = new ButacaDTO();

        butacaDTO.setAnulada(false);
        butacaDTO.setFila(fila);
        butacaDTO.setNumero(numero);
        butacaDTO.setPrecio(precio);
        butacaDTO.setPrecioSinComision(precioSinComision);
        butacaDTO.setParSesion(sesion);
        butacaDTO.setParLocalizacion(localizacion);

        return butacaDTO;
    }

}
