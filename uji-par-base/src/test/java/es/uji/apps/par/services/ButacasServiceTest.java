package es.uji.apps.par.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.model.DisponiblesLocalizacion;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ButacasServiceTest extends BaseServiceTest {
    @Autowired
    private ButacasService butacasService;

    @PersistenceContext
    protected EntityManager entityManager;

    @Test
    public void testDisponiblesSesionSinPrecios() {
        List<DisponiblesLocalizacion> disponibles =
            butacasService.getDisponibles(sesionSinPrecios.getId());

        Assert.assertEquals(200, disponibles.get(0).getTotales());
    }

    @Test
    public void testDisponiblesSesionPreciosCon2ReservasY2ReservasAnuladas() {
        List<DisponiblesLocalizacion> disponibles =
            butacasService.getDisponibles(sesionConReservas.getId());

        Assert.assertEquals(200, disponibles.get(0).getTotales());
        Assert.assertEquals(2, disponibles.get(0).getOcupadas());
        Assert.assertEquals(198, disponibles.get(0).getDisponibles());
    }

    @Test
    public void testDisponiblesSesionPreciosCon14EntradasCompradas() {
        List<DisponiblesLocalizacion> disponibles =
            butacasService.getDisponibles(sesionConCompras.getId());

        Assert.assertEquals(200, disponibles.get(0).getTotales());
        Assert.assertEquals(17, disponibles.get(0).getOcupadas());
        Assert.assertEquals(183, disponibles.get(0).getDisponibles());
    }

    @Test
    public void testDisponiblesSesionPlantillaCon2EntradasCompradas() {
        List<DisponiblesLocalizacion> disponibles =
            butacasService.getDisponibles(sesionConPlantillaConCompras.getId());

        Assert.assertEquals(200, disponibles.get(0).getTotales());
        Assert.assertEquals(2, disponibles.get(0).getOcupadas());
        Assert.assertEquals(198, disponibles.get(0).getDisponibles());
    }

    @Test
    public void testDisponiblesSesionPreciosSinCompras() {
        List<DisponiblesLocalizacion> disponibles =
            butacasService.getDisponibles(sesionSinVentaAnticipada.getId());

        Assert.assertEquals(200, disponibles.get(0).getTotales());
        Assert.assertEquals(0, disponibles.get(0).getOcupadas());
        Assert.assertEquals(200, disponibles.get(0).getDisponibles());
    }

    @Test
    public void testDisponiblesSesionPlantillaSinCompras() {
        List<DisponiblesLocalizacion> disponibles =
            butacasService.getDisponibles(sesionConPlantillaConVentaAnticipadaActiva.getId());

        Assert.assertEquals(200, disponibles.get(0).getTotales());
        Assert.assertEquals(0, disponibles.get(0).getOcupadas());
        Assert.assertEquals(200, disponibles.get(0).getDisponibles());
    }
}
