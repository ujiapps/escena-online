package es.uji.apps.par.builders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.PlantillaDTO;
import es.uji.apps.par.db.PreciosPlantillaDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.model.Plantilla;

public class PlantillaBuilder
{
	private PlantillaDTO plantilla;
	private List<PreciosPlantillaDTO> precios;

	public PlantillaBuilder(String nombre, SalaDTO sala)
	{
		precios = new ArrayList<>();

		plantilla = new PlantillaDTO();
		plantilla.setNombre(nombre);
		plantilla.setSala(sala);
	}

	public PlantillaBuilder withPrecio(
		BigDecimal precio,
		LocalizacionDTO localizacion,
		TarifaDTO tarifa
	) {
		PreciosPlantillaDTO preciosPlantillaDTO = new PreciosPlantillaDTO();
		preciosPlantillaDTO.setPrecio(precio);
		preciosPlantillaDTO.setParLocalizacione(localizacion);
		preciosPlantillaDTO.setParTarifa(tarifa);

		precios.add(preciosPlantillaDTO);

		return this;
	}

	public PlantillaBuilder withPrecioAnticipado(
		BigDecimal precio,
		BigDecimal precioAnticipado,
		LocalizacionDTO localizacion,
		TarifaDTO tarifa
	) {
		PreciosPlantillaDTO preciosPlantillaDTO = new PreciosPlantillaDTO();
		preciosPlantillaDTO.setPrecio(precio);
		preciosPlantillaDTO.setPrecioAnticipado(precioAnticipado);
		preciosPlantillaDTO.setParLocalizacione(localizacion);
		preciosPlantillaDTO.setParTarifa(tarifa);

		precios.add(preciosPlantillaDTO);

		return this;
	}

	public Plantilla get() {
		return Plantilla.plantillaPreciosDTOtoPlantillaPrecios(this.plantilla);
	}

	public PlantillaDTO build(EntityManager entityManager)
	{
		entityManager.persist(plantilla);

		for (PreciosPlantillaDTO precio : precios) {
			precio.setParPlantilla(plantilla);

			entityManager.persist(precio);
			entityManager.flush();
		}

		plantilla.setParPreciosPlantillas(precios);

		return plantilla;
	}
}