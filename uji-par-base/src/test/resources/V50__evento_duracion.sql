drop table IF EXISTS fechas_duracion_sesion;

create
or replace view fechas_duracion_sesion as
select s.id, s.SALA_ID, s.fecha_celebracion as fecha_inicio, s.fecha_celebracion + e.duracion as fecha_fin
from par_eventos e
         join par_sesiones s on e.id = s.evento_id
where ISABONO = 0;