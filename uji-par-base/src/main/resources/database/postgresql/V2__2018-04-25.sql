ALTER TABLE par_butacas ADD fecha_anulada timestamp NULL;
ALTER TABLE par_butacas ADD par_usuario_anula INTEGER NULL;
ALTER TABLE par_butacas ADD CONSTRAINT PAR_BUTACAS_USUARIO_ANULA FOREIGN KEY (par_usuario_anula) REFERENCES par_usuarios (ID) ON DELETE RESTRICT;

INSERT INTO par_version_bbdd (VERSION) VALUES ('2018-04-25.sql');