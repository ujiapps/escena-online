CREATE TABLE log_compras
(
  id_compra     integer,
  observaciones varchar(3500),
  old_fecha     timestamp,
  old_reserva   boolean,
  old_anulada   boolean,
  old_caducada  boolean,
  old_pagada    boolean,
  old_tipo      varchar(250),
  fecha         timestamp,
  reserva       boolean,
  anulada       boolean,
  caducada      boolean,
  pagada        boolean,
  tipo          varchar(250),
  fecha_log     timestamp default now()
);

CREATE OR REPLACE FUNCTION log_compra() RETURNS TRIGGER AS
$BODY$
BEGIN
  INSERT INTO log_compras(id_compra, observaciones, old_fecha, old_reserva, old_anulada, old_caducada, old_pagada,
                          old_tipo,
                          fecha, reserva, anulada, caducada, pagada, tipo)
  VALUES (new.id, new.observaciones_reserva, old.fecha, old.reserva, old.anulada, old.caducada, old.pagada, old.tipo,
          new.fecha,
          new.reserva,
          new.anulada, new.caducada, new.pagada, new.tipo);

  RETURN new;
END;

$BODY$
  language plpgsql;

DROP TRIGGER IF EXISTS log_update_compra ON par_compras;

CREATE TRIGGER log_update_compra
  AFTER UPDATE
  ON par_compras
  FOR EACH ROW
  WHEN (OLD.taquilla is TRUE AND OLD.* IS DISTINCT FROM NEW.*)
EXECUTE PROCEDURE log_compra();