CREATE TABLE par_plantillas_reservas
(
    id      serial       not null primary key,
    nombre  varchar(255) NOT NULL,
    sala_id bigint
);
ALTER TABLE par_plantillas_reservas
    ADD UNIQUE (nombre, sala_id);

ALTER TABLE par_plantillas_reservas
    ADD CONSTRAINT par_pr_sala_fkey FOREIGN KEY (sala_id) REFERENCES par_salas (id) ON DELETE
        CASCADE NOT
        DEFERRABLE INITIALLY IMMEDIATE;

CREATE TABLE par_butacas_plantilla_reservas
(
    id              serial not null primary key,
    plantilla_id    bigint NOT NULL,
    localizacion_id bigint NOT NULL,
    fila            varchar(255),
    numero          varchar(255)
);

ALTER TABLE par_butacas_plantilla_reservas
    ADD CONSTRAINT par_bpr_plantilla_fkey FOREIGN KEY (plantilla_id) REFERENCES par_plantillas_reservas (id) ON DELETE
        CASCADE NOT
        DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE par_butacas_plantilla_reservas
    ADD CONSTRAINT par_bpr_local_fkey FOREIGN KEY (localizacion_id) REFERENCES par_localizaciones (id) ON DELETE
        CASCADE NOT
        DEFERRABLE INITIALLY IMMEDIATE;