alter table par_cines
  add menu_abonos boolean default false not null;

alter table par_tarifas
  add isabono boolean default false not null;

alter table par_abonados
  alter column email set not null;

ALTER TABLE par_abonados
  ADD UNIQUE (abono_id, email);

alter table par_tarifas
  alter column cine_id set not null;

ALTER TABLE par_tarifas
  ADD UNIQUE (nombre, cine_id);