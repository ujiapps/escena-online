alter table par_cines
    add is_anulable_desde_enlace boolean default false not null;

alter table par_cines
    add tiempo_restante_anulable_desde_enlace int default 60 not null;