create or replace view butacas_csv
as
select c.uuid || '-' || pb.id as entrada_uuid,
       pb.sesion_id,
       c.id                   as compra_id,
       c.nombre,
       c.apellidos,
       c.email,
       pb.presentada,
       pb.fila,
       pb.numero,
       pb.nombre_completo,
       pl.nombre_es,
       pl.nombre_va
from par_compras c
         join par_butacas pb on c.id = pb.compra_id
         left join par_localizaciones pl on pl.id = pb.localizacion_id
where c.anulada is FALSE
  and c.caducada is FALSE
  and c.pagada is TRUE
  and pb.anulada is FALSE;