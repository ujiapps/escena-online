DROP TRIGGER IF EXISTS alerta_venta_online ON par_sesiones;

CREATE
    OR REPLACE FUNCTION check_numero_sesiones_a_la_venta()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS
$$
declare
    alertas_ultimas_12_horas  int;
    sesiones int;
    entradas_libres
                              int;
BEGIN
    IF NEW.fecha_inicio_venta_online > now() THEN
        select count(*)
        into alertas_ultimas_12_horas
        from par_mails
        where fecha_creado > now() - interval '12 hours'
          and compra_uuid like 'alert_%';

        IF alertas_ultimas_12_horas = 0 THEN
            select count(*), sum(ocupacion.libres)
            into sesiones, entradas_libres
            from (
                     select count(*) localizaciones, sum(total_entradas) - sum(ocupadas) libres
                     from par_ocupacion
                     where FECHA_INICIO_VENTA_ONLINE::date = NEW.fecha_inicio_venta_online::date
                     group by SESION_ID) as ocupacion;
            IF
                    sesiones > 20 THEN
                insert into par_mails (para, de, titulo, texto, compra_uuid, url_public, url_pie_entrada, fecha_creado)
                VALUES ('soporte@4tic.com', 'no_reply@4tic.com', 'Alerta sesiones mismo día de venta',
                        'El día ' || to_char(NEW.fecha_inicio_venta_online, 'dd-MM-YYYY') || ' hay ' || sesiones ||
                        ' sesiones a la venta con ' ||
                        entradas_libres || ' entradas disponibles. Esta alerta ha sido generada automáticamente.',
                        'alert_' || md5(random()::text || clock_timestamp()::text)::uuid, 'cuatroochenta.com',
                        'cuatroochenta.com', now());
            END IF;
        END IF;
    END IF;
    RETURN NEW;
END;
$$;

CREATE TRIGGER alerta_venta_online
    AFTER INSERT OR
        UPDATE
    ON par_sesiones
    FOR EACH ROW
EXECUTE PROCEDURE check_numero_sesiones_a_la_venta();