alter table par_mails
  drop constraint par_mails_pkey;

alter table par_mails
  drop constraint par_mails_id_key;

-- alter table par_mails drop column id;

alter table par_mails
  alter column id drop not null;

alter table par_mails
  alter column id drop default;

alter table par_mails
  add constraint par_mails_pk
    primary key (compra_uuid);

create unique index par_mails_compra_uuid_uindex
  on par_mails (compra_uuid);

INSERT INTO par_version_bbdd (VERSION)
VALUES ('2019-01-29.SQL');