alter table par_precios_sesion
  add precio_anticipado real;

alter table par_precios_plantilla
  add precio_anticipado real;

alter table par_cines
  add menu_venta_anticipada boolean default FALSE;

alter table par_sesiones
  add fecha_fin_venta_anticipada timestamp;

INSERT INTO par_version_bbdd (VERSION)
VALUES ('2019-02-18.SQL');