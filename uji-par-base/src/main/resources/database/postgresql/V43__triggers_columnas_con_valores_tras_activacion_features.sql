CREATE OR REPLACE FUNCTION check_required_values_on_features_enabled()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS
$$
BEGIN
    IF NEW.is_anulable_desde_enlace is TRUE THEN
        IF NEW.email IS NULL THEN
            RAISE EXCEPTION 'El cine tiene que que contener un valor en el campo email ya que la pantalla de anulación muestra este campo';
        END IF;
    END IF;

    IF NEW.passbook_activado is TRUE THEN
        IF NEW.logo_uuid IS NULL AND NEW.logo_src IS NULL THEN
            RAISE EXCEPTION 'El cine tiene que tener logo ya que el passbook generado lo muestra';
        END IF;

        IF NEW.banner_uuid IS NULL AND NEW.banner_src IS NULL THEN
            RAISE EXCEPTION 'El cine tiene que tener banner ya que el passbook generado lo muestra';
        END IF;
    END IF;

    IF NEW.show_logo_entrada is TRUE THEN
        IF NEW.logo_uuid IS NULL AND NEW.logo_src IS NULL THEN
            RAISE EXCEPTION 'El cine tiene que tener logo ya que se quiere mostrar en la entrada';
        END IF;
    END IF;

    RETURN NEW;
END;
$$;

DROP TRIGGER IF EXISTS required_values_on_features_enabled ON par_cines;
CREATE TRIGGER required_values_on_features_enabled
    BEFORE INSERT OR UPDATE
    ON par_cines
    FOR EACH ROW
EXECUTE PROCEDURE check_required_values_on_features_enabled();