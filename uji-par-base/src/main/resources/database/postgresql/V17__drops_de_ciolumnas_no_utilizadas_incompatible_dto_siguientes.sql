alter table par_abonados
  drop constraint par_abonados_abonos_fk1;

alter table par_abonados
  add constraint par_abonados_abonos_fk1
    foreign key (abono_id) references par_tarifas;

DROP TABLE par_sesiones_abonos;

DROP TABLE par_abonos;

alter table par_abonados
  drop column importe;

alter table par_abonados
  drop column abonados;

alter table par_compras
  drop column abonado_id;

alter table par_compras_borradas
  drop column abonado_id;

alter table par_abonados
  drop column anulado;

alter table par_mails
  drop column id;

drop sequence IF EXISTS par_mails_id_seq;