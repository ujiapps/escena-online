alter table par_salas
    alter column codigo type varchar(1000) using codigo::varchar(1000);

alter table par_localizaciones
    alter column codigo type varchar(1000) using codigo::varchar(1000);