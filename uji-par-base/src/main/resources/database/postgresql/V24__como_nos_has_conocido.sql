CREATE TABLE par_como_nos_conociste
(
    id      serial NOT NULL,
    cine_id integer,
    motivo  character varying(2500),
    CONSTRAINT par_como_nos_conociste_pkey PRIMARY KEY (id)
)
    WITH (
        OIDS= FALSE
    );

alter table par_como_nos_conociste
    add constraint par_como_nos_conociste_par_cines_id_fk
        foreign key (cine_id) references par_cines (id);

alter table par_cines
    add show_como_nos_conociste boolean default false;

alter table par_compras
    add como_nos_conociste_id int;

alter table par_compras
    add constraint par_compras_par_como_nos_conociste_id_fk
        foreign key (como_nos_conociste_id) references par_como_nos_conociste (id);