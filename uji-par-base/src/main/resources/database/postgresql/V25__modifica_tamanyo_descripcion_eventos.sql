drop view IF EXISTS par_abonos;

alter table par_eventos alter column descripcion_es type varchar(7000) using descripcion_es::varchar(7000);
alter table par_eventos alter column descripcion_va type varchar(7000) using descripcion_va::varchar(7000);

CREATE OR REPLACE VIEW par_abonos as
select t.id,
       t.nombre,
       t.max_eventos,
       e.descripcion_es,
       e.descripcion_va,
       e.imagen,
       e.imagen_uuid,
       e.imagen_content_type,
       e.imagen_src,
       e.tpv_id,
       e.id as evento_id,
       l.id as localizacion_id,
       precio_ses.precio,
       precio_ses.precio_anticipado,
       ses.canal_internet,
       ses.fecha_inicio_venta_online,
       ses.fecha_fin_venta_online,
       ses.fecha_fin_venta_anticipada
from par_tarifas t
         left join par_eventos e
                   on t.nombre = e.titulo_es and t.nombre = e.titulo_va and e.cine_id = t.cine_id and e.isabono is TRUE
         left join par_sesiones ses on e.id = ses.evento_id
         left join par_precios_sesion precio_ses on ses.id = precio_ses.sesion_id
         left join par_precios_sesion ps on ps.sesion_id = ses.id
         left join par_localizaciones l on l.id = ps.localizacion_id
where t.isabono is TRUE;