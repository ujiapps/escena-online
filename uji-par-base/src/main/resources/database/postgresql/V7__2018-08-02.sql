create or replace view butacas_csv
  as
    select
      c.sesion_id,
      c.uuid || '-' || pb.id as csv
    from par_compras c
      join par_butacas pb on c.id = pb.compra_id
    where c.anulada is FALSE and c.caducada is FALSE and c.pagada is TRUE and pb.anulada is FALSE;

create or replace view informe_comision as
  SELECT c.uuid,
    c.fecha,
    c.email,
    (sum(pb.precio))::numeric AS precio,
    ((sum(pb.precio))::numeric - (sum(pb.precio_sin_comision))::numeric) AS comision,
    c2.id AS cine_id,
    c2.nombre AS cine,
    e.titulo_es AS titulo,
    s.fecha_celebracion
  FROM ((((par_compras c
    LEFT JOIN par_butacas pb ON ((c.id = pb.compra_id)))
    LEFT JOIN par_sesiones s ON ((c.sesion_id = s.id)))
    LEFT JOIN par_eventos e ON ((s.evento_id = e.id)))
    LEFT JOIN par_cines c2 ON ((e.cine_id = c2.id)))
  WHERE ((c.anulada is FALSE and c.caducada is FALSE and c.pagada is TRUE and pb.anulada is FALSE) AND (s.id <> 4751))
  GROUP BY c.id, s.fecha_celebracion, e.titulo_es, c2.id
  ORDER BY s.fecha_celebracion DESC;

INSERT INTO par_version_bbdd (VERSION) VALUES ('2018-08-02.SQL');