alter table par_eventos
  add duracion integer default 0;

create or replace view fechas_duracion_sesion as
    select s.id, s.sala_id, s.fecha_celebracion as fecha_inicio, s.fecha_celebracion + (e.duracion * interval '1 minute') as fecha_fin
    from par_eventos e
    join par_sesiones s on e.id = s.evento_id
where e.isabono is false;