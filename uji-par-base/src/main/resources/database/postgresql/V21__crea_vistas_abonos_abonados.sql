drop table if exists par_abonados;

drop table if exists par_abonos;

drop view IF EXISTS par_abonos;

drop view IF EXISTS par_abonados;

CREATE OR REPLACE VIEW par_abonos as
select t.id,
       t.nombre,
       t.max_eventos,
       e.descripcion_es,
       e.descripcion_va,
       e.imagen,
       e.imagen_uuid,
       e.imagen_content_type,
       e.imagen_src,
       e.tpv_id,
       e.id as evento_id,
       l.id as localizacion_id,
       precio_ses.precio,
       precio_ses.precio_anticipado,
       ses.canal_internet,
       ses.fecha_inicio_venta_online,
       ses.fecha_fin_venta_online,
       ses.fecha_fin_venta_anticipada
from par_tarifas t
         left join par_eventos e
                   on t.nombre = e.titulo_es and t.nombre = e.titulo_va and e.cine_id = t.cine_id and e.isabono is TRUE
         left join par_sesiones ses on e.id = ses.evento_id
         left join par_precios_sesion precio_ses on ses.id = precio_ses.sesion_id
         left join par_precios_sesion ps on ps.sesion_id = ses.id
         left join par_localizaciones l on l.id = ps.localizacion_id
where t.isabono is TRUE;

CREATE OR REPLACE VIEW par_abonados as
select c.id,
       t.id as abono_id,
       c.nombre,
       c.apellidos,
       c.tfno,
       c.email,
       c.direccion,
       c.poblacion,
       c.fecha,
       c.cp,
       c.provincia,
       c.info_periodica,
       c.importe,
       c.codigo_pago_pasarela,
       c.uuid
from par_compras c
         join par_butacas b on c.id = b.compra_id and c.anulada is FALSE and c.pagada is TRUE
         join par_sesiones s on b.sesion_id = s.id
         join par_eventos e on s.evento_id = e.id and e.isabono is TRUE
         join par_tarifas t on t.nombre = e.titulo_es and t.nombre = e.titulo_va and e.cine_id = t.cine_id;
