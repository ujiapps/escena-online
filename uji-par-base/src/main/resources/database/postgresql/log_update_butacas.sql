CREATE TABLE log_butacas
(
  id_butaca   integer,
  compra_id   integer,
  old_anulada boolean,
  anulada     boolean,
  fecha_log   timestamp default now()
);

CREATE OR REPLACE FUNCTION log_butaca() RETURNS TRIGGER AS
$BODY$
BEGIN
  INSERT INTO log_butacas(id_butaca, compra_id, old_anulada, anulada)
  VALUES (new.id, new.compra_id, old.anulada, new.anulada);

  RETURN new;
END;

$BODY$
  language plpgsql;

DROP TRIGGER IF EXISTS log_update_butaca ON par_butacas;

CREATE TRIGGER log_update_butaca
  AFTER UPDATE
  ON par_butacas
  FOR EACH ROW
  WHEN (OLD.anulada IS DISTINCT FROM NEW.anulada)
EXECUTE PROCEDURE log_butaca();