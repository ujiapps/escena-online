alter table PAR_CINES add informes VARCHAR(2500);
alter table PAR_CINES add informes_generales VARCHAR(2500);

alter table PAR_CINES add menu_tpv BOOLEAN DEFAULT true;
alter table PAR_CINES add menu_clientes BOOLEAN DEFAULT true;
alter table PAR_CINES add menu_integraciones BOOLEAN DEFAULT true;
alter table PAR_CINES add menu_salas BOOLEAN DEFAULT false;
alter table PAR_CINES add menu_perfil BOOLEAN DEFAULT false;
alter table PAR_CINES add menu_ayuda BOOLEAN DEFAULT true;
alter table PAR_CINES add menu_taquilla BOOLEAN DEFAULT true;
alter table PAR_CINES add check_contratacion BOOLEAN DEFAULT false;

INSERT INTO par_version_bbdd (VERSION) VALUES ('2018-07-16.SQL');