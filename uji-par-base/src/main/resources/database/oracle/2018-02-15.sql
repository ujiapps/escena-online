ALTER TABLE par_compras
  ADD par_usuario NUMBER NULL;
ALTER TABLE par_compras
  ADD CONSTRAINT PAR_COMPRAS_USUARIO FOREIGN KEY (par_usuario) REFERENCES par_usuarios (id);

ALTER TABLE par_usuarios
  ADD password VARCHAR(250);
ALTER TABLE par_usuarios
  ADD role VARCHAR(255);

INSERT INTO par_version_bbdd (VERSION)
VALUES ('2018-02-15.SQL');