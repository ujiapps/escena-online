ALTER TABLE par_cines
  ADD formas_pago VARCHAR2(2500) DEFAULT 'metalico,tarjetaOffline,transferencia,credito';

INSERT INTO par_version_bbdd (VERSION)
VALUES ('2018-10-01.SQL');