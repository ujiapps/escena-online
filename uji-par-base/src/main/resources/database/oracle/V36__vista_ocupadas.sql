CREATE OR REPLACE VIEW par_ocupacion as
select distinct parsesione2_.id || localizaci0_.CODIGO ID,
                    count(parbutacas3_.id)      OCUPADAS,
                localizaci0_.CODIGO         CODIGO,
                localizaci0_.TOTAL_ENTRADAS TOTAL_ENTRADAS,
                parsesione2_.id             SESION_ID
from PAR_LOCALIZACIONES localizaci0_
         inner join PAR_SALAS saladto1_ on localizaci0_.SALA_ID = saladto1_.id
         inner join PAR_SESIONES parsesione2_ on saladto1_.id = parsesione2_.SALA_ID
         left outer join PAR_BUTACAS parbutacas3_
                         on localizaci0_.id = parbutacas3_.LOCALIZACION_ID and (parbutacas3_.ANULADA = 0)
                             and parbutacas3_.SESION_ID = parsesione2_.ID
group by parsesione2_.id, localizaci0_.CODIGO, localizaci0_.TOTAL_ENTRADAS