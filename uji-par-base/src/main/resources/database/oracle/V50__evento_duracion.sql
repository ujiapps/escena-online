alter table PAR_EVENTOS
  add DURACION NUMBER(*,0) DEFAULT 0;

create or replace view fechas_duracion_sesion as
select s.id, s.SALA_ID, s.fecha_celebracion as fecha_inicio, s.fecha_celebracion + e.duracion as fecha_fin
from par_eventos e
         join par_sesiones s on e.id = s.evento_id
where ISABONO = 0;