alter table PAR_ABONADOS
  drop constraint PAR_ABONADOS_ABONOS_FK1;

UPDATE PAR_COMPRAS
SET ABONADO_ID = NULL
WHERE ABONADO_ID IS NOT NULL;
truncate table PAR_SESIONES_ABONOS;
DELETE
FROM PAR_ABONADOS;

alter table PAR_ABONADOS
  add constraint PAR_ABONADOS_ABONOS_FK1
    foreign key (ABONO_ID) references PAR_TARIFAS (ID);

DROP TABLE PAR_SESIONES_ABONOS;

DROP TABLE PAR_ABONOS;

alter table PAR_ABONADOS
  drop column IMPORTE;

alter table PAR_ABONADOS
  drop column ABONADOS;

alter table PAR_COMPRAS
  drop column ABONADO_ID;

alter table PAR_COMPRAS_BORRADAS
  drop column ABONADO_ID;

alter table PAR_ABONADOS
  drop column ANULADO;

alter table PAR_MAILS
  drop column ID;

CREATE OR REPLACE TRIGGER eventos_publicos
  BEFORE INSERT OR UPDATE
  on PAR_EVENTOS
  FOR EACH ROW
BEGIN
  IF :new.PUBLICO is NULL
  THEN
    :new.PUBLICO := 1;
  end if;
END;