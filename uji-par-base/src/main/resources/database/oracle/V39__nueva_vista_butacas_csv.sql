drop view butacas_csv;

create or replace view butacas_csv
as
select c.uuid || '-' || pb.id as entrada_uuid,
       c.sesion_id,
       c.id                   as compra_id,
       c.nombre,
       c.apellidos,
       c.email,
       pb.presentada,
       pb.fila,
       pb.numero,
       pb.nombre_completo,
       pl.nombre_es,
       pl.nombre_va
from par_compras c
         join par_butacas pb on c.id = pb.compra_id
         left join par_localizaciones pl on pb.localizacion_id = pl.id
where c.anulada = 0
  and c.caducada = 0
  and c.pagada = 1
  and pb.anulada = 0;