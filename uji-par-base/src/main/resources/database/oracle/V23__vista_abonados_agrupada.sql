drop view par_abonados;

CREATE OR REPLACE VIEW par_abonados as
select c.id,
       t.id        as abono_id,
       c.nombre,
       c.apellidos,
       c.tfno,
       c.email,
       c.direccion,
       c.poblacion,
       c.fecha,
       c.cp,
       c.provincia,
       c.info_periodica,
       c.importe,
       c.codigo_pago_pasarela,
       c.uuid,
       count(c.id) as abonos
from par_compras c
         join par_butacas b on c.id = b.compra_id and c.anulada = 0 and c.pagada = 1
         join par_sesiones s on b.sesion_id = s.id
         join par_eventos e on s.evento_id = e.id and e.isabono = 1
         join par_tarifas t on t.nombre = e.titulo_es and t.nombre = e.titulo_va and e.cine_id = t.cine_id
group by c.id, t.id, c.nombre, c.APELLIDOS, c.TFNO, c.EMAIL, c.DIRECCION, c.POBLACION, c.FECHA, c.cp, c.PROVINCIA,
         c.INFO_PERIODICA, c.IMPORTE, c.CODIGO_PAGO_PASARELA, c.UUID;

