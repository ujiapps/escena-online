create or replace view butacas_csv
as
select c.sesion_id,
       c.uuid || '-' || pb.id as csv
from par_compras c
         join par_butacas pb on c.id = pb.compra_id
where c.anulada = 0
  and c.caducada = 0
  and c.pagada = 1
  and pb.anulada = 0;