CREATE TABLE PAR_PLANTILLAS_RESERVAS
(
    id      integer       NOT NULL,
    nombre  varchar2(255) NOT NULL,
    sala_id integer
);

ALTER TABLE PAR_PLANTILLAS_RESERVAS
    ADD FOREIGN KEY (sala_id) REFERENCES PAR_SALAS (id) ON DELETE CASCADE;

ALTER TABLE PAR_PLANTILLAS_RESERVAS
    ADD UNIQUE (nombre, sala_id);
ALTER TABLE PAR_PLANTILLAS_RESERVAS
    ADD PRIMARY KEY (id);

CREATE TABLE PAR_BUTACAS_PLANTILLA_RESERVAS
(
    id              integer not null primary key,
    plantilla_id    integer NOT NULL,
    localizacion_id integer NOT NULL,
    fila            varchar2(255),
    numero          varchar2(255)
);

ALTER TABLE PAR_BUTACAS_PLANTILLA_RESERVAS
    ADD FOREIGN KEY (plantilla_id) REFERENCES
        PAR_PLANTILLAS_RESERVAS (id) ON DELETE
            CASCADE;

ALTER TABLE PAR_BUTACAS_PLANTILLA_RESERVAS
    ADD FOREIGN KEY (localizacion_id) REFERENCES PAR_LOCALIZACIONES (ID) ON DELETE
        CASCADE;