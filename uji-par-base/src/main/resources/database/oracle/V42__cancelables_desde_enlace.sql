alter table PAR_CINES
    add IS_ANULABLE_DESDE_ENLACE number(1) default 0 not null;

alter table PAR_CINES
    add TIEMPO_ANULABLE_DESDE_ENLACE number default 60 not null;