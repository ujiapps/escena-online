alter table par_cines
  add menu_abonos NUMBER(1) default 0 not null;

alter table PAR_TARIFAS
  add ISABONO NUMBER(1) default 0 not null;

UPDATE PAR_COMPRAS
SET ABONADO_ID = NULL
WHERE ABONADO_ID IN (SELECT ID
                     FROM PAR_ABONADOS
                     where EMAIL is null);

DELETE
FROM PAR_ABONADOS
where EMAIL is null;

alter table PAR_ABONADOS
  modify EMAIL not null;

ALTER TABLE PAR_ABONADOS
  ADD UNIQUE (ABONO_ID, EMAIL);

UPDATE PAR_TARIFAS
SET CINE_ID = (SELECT *
               FROM (SELECT ID
                     FROM PAR_CINES)
               where ROWNUM = 1);

alter table PAR_TARIFAS
  modify CINE_ID not null;

ALTER TABLE PAR_TARIFAS
  ADD UNIQUE (NOMBRE, CINE_ID);