ALTER TABLE PAR_MAILS
  DISABLE constraint PAR_MAILS_PK;

create unique index PAR_MAILS_COMPRA_UUID_uindex
  on PAR_MAILS (COMPRA_UUID);

alter table PAR_MAILS
  drop primary key;

-- alter table PAR_MAILS
--   drop column ID;

alter table PAR_MAILS modify ID null;

alter table PAR_MAILS
  add constraint PAR_MAILS_pk
    primary key (COMPRA_UUID);

INSERT INTO par_version_bbdd (VERSION)
VALUES ('2019-01-29.SQL');