ALTER TABLE par_salas ADD CLASE_ENTRADA_TAQUILLA VARCHAR2(255);
ALTER TABLE par_salas ADD CLASE_ENTRADA_ONLINE VARCHAR2(255);

update par_salas ps set CLASE_ENTRADA_TAQUILLA = (select clase from par_reports where tipo='ENTRADATAQUILLA' and cine_id=ps.cine_id);
update par_salas ps set CLASE_ENTRADA_ONLINE = (select clase from par_reports where tipo='ENTRADAONLINE' and cine_id=ps.cine_id);

DELETE FROM par_reports WHERE tipo = 'ENTRADATAQUILLA';
DELETE FROM par_reports WHERE tipo = 'ENTRADAONLINE';

INSERT INTO par_version_bbdd (VERSION) VALUES ('2018-03-22.sql');