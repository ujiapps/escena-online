CREATE OR REPLACE TRIGGER check_req_val_on_feat_enabled
    BEFORE INSERT OR UPDATE
    on PAR_CINES
    FOR EACH ROW
BEGIN
    IF (:NEW.is_anulable_desde_enlace = 1)
        THEN
        IF (:NEW.email IS NULL)
            THEN
            RAISE_APPLICATION_ERROR(-20001, 'El cine tiene que que contener un valor en el campo email ya que la pantalla de anulación muestra este campo');
        END IF;
    END IF;

    IF (:NEW.passbook_activado = 1)
        THEN
        IF (:NEW.logo_uuid IS NULL AND :NEW.logo_src IS NULL)
            THEN
                RAISE_APPLICATION_ERROR(-20001, 'El cine tiene que tener logo ya que el passbook generado lo muestra');
        END IF;

        IF NEW.banner_uuid IS NULL AND :NEW.banner_src IS NULL THEN
            RAISE_APPLICATION_ERROR(-20001, 'El cine tiene que tener banner ya que el passbook generado lo muestra');
        END IF;
    END IF;

    IF (:NEW.show_logo_entrada = 1)
        THEN
        IF (:NEW.logo_uuid IS NULL AND :NEW.logo_src IS NULL)
            THEN
            RAISE_APPLICATION_ERROR(-20001, 'El cine tiene que tener logo ya que se quiere mostrar en la entrada');
        END IF;
    END IF;
END;