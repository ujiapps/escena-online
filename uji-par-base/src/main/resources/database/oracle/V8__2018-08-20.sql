ALTER TABLE par_salas
  DROP COLUMN asientos;
ALTER TABLE par_salas
  DROP COLUMN asiento_disc;
ALTER TABLE par_salas
  DROP COLUMN asiento_nores;

INSERT INTO par_version_bbdd (VERSION)
VALUES ('2018-08-20.SQL');