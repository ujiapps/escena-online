ALTER TABLE par_cines DROP COLUMN url_admin;

ALTER TABLE par_cines ADD LOGO BLOB;
ALTER TABLE par_cines ADD LOGO_CONTENT_TYPE VARCHAR2(255);
ALTER TABLE par_cines ADD LOGO_SRC VARCHAR2(255);
ALTER TABLE par_cines ADD LOGO_UUID VARCHAR2(255);

ALTER TABLE par_cines ADD BANNER BLOB;
ALTER TABLE par_cines ADD BANNER_CONTENT_TYPE VARCHAR2(255);
ALTER TABLE par_cines ADD BANNER_SRC VARCHAR2(255);
ALTER TABLE par_cines ADD BANNER_UUID VARCHAR2(255);

INSERT INTO par_version_bbdd (VERSION) VALUES ('2018-03-08.sql');