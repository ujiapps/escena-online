alter table PAR_CINES add informes VARCHAR2(2500);
alter table PAR_CINES add informes_generales VARCHAR2(2500);

alter table PAR_CINES add menu_tpv NUMBER(1,0) DEFAULT 1;
alter table PAR_CINES add menu_clientes NUMBER(1,0) DEFAULT 1;
alter table PAR_CINES add menu_integraciones NUMBER(1,0) DEFAULT 1;
alter table PAR_CINES add menu_salas NUMBER(1,0) DEFAULT 0;
alter table PAR_CINES add menu_perfil NUMBER(1,0) DEFAULT 0;
alter table PAR_CINES add menu_ayuda NUMBER(1,0) DEFAULT 1;
alter table PAR_CINES add menu_taquilla NUMBER(1,0) DEFAULT 1;
alter table PAR_CINES add check_contratacion NUMBER(1,0) DEFAULT 0;

INSERT INTO par_version_bbdd (VERSION) VALUES ('2018-07-16.SQL');