package es.uji.apps.par.exceptions;

public class SalaConSesionesMismaHoraException extends GeneralPARException {

    public SalaConSesionesMismaHoraException() {
        super(SALA_CON_SESIONES_MISMA_HORA_CODE, SALA_CON_SESIONES_MISMA_HORA);
    }
}
