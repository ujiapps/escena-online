package es.uji.apps.par.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.OutputStream;
import java.util.Date;

import es.uji.apps.fopreports.serialization.ReportSerializationException;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.exceptions.CompraNoExistente;
import es.uji.apps.par.report.EntradaPassbookReport;
import es.uji.apps.par.report.EntradaReportOnlineInterface;

@Service
public class PassbookService extends EntradasService {
    private static final Logger log = LoggerFactory.getLogger(EntradasService.class);

    synchronized public void generaEntradaPassbook(
        String idCompra,
        long idButaca,
        OutputStream outputStream,
        String userUID,
        String urlPublicSinHTTPS,
        String urlPieEntrada
    ) throws ReportSerializationException {
        EntradaReportOnlineInterface entrada =
            generaPassbookYRellena(idCompra, idButaca, userUID, urlPublicSinHTTPS, urlPieEntrada);
        if (entrada != null) {
            entrada.serialize(outputStream);
        }
    }

    public EntradaReportOnlineInterface generaPassbookYRellena(
        String idCompra,
        long idButaca,
        String userUID,
        String urlPublicSinHTTPS,
        String urlPieEntrada
    ) throws ReportSerializationException {
        CompraDTO compra = comprasDAO.getCompraByUuid(idCompra);
        if (compra == null)
            throw new CompraNoExistente();
        EntradaReportOnlineInterface entrada = new EntradaPassbookReport();
        entrada.create(getLocale(userUID), configuration);
        try {
            rellenaEntrada(compra, idButaca, entrada, userUID, urlPublicSinHTTPS, urlPieEntrada);
        }
        catch (Exception e) {
            if (compra.getParSesion().getFechaCelebracion().after(new Date())) {
                log.error(e.getMessage(), e);
                throw e;
            }
            else {
                log.warn(e.getMessage(), e);
                return null;
            }
        }
        return entrada;
    }
}
