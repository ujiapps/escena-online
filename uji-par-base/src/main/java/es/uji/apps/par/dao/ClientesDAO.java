package es.uji.apps.par.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.par.db.QComoNosConocisteDTO;
import es.uji.apps.par.db.QCompraDTO;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSalasUsuarioDTO;
import es.uji.apps.par.db.QSesionDTO;
import es.uji.apps.par.db.QUsuarioDTO;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.utils.Utils;

@Repository
public class ClientesDAO extends BaseDAO {
    private QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
    private QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
    private QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
    private QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
    private QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;
    private QComoNosConocisteDTO qComoNosConocisteDTO = QComoNosConocisteDTO.comoNosConocisteDTO;

    @Autowired
    ComprasDAO comprasDAO;

    @Transactional
    public List<Tuple> getClientes(
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID
    ) {
        JPAQuery jpaQuery = getQueryClientes(filter, userUID);

        List<Tuple> compras = new ArrayList<Tuple>();
        if (jpaQuery != null) {
            compras = jpaQuery.orderBy(getSort(qCompraDTO, sortParameter)).limit(limit).offset(start)
                .list(qCompraDTO.id, qCompraDTO.nombre, qCompraDTO.apellidos, qCompraDTO.direccion,
                    qCompraDTO.poblacion, qCompraDTO.cp, qCompraDTO.provincia, qCompraDTO.telefono, qCompraDTO.email,
                    qComoNosConocisteDTO.motivo, qCompraDTO.fecha);
        }

        return compras;
    }

    @Transactional
    public int getTotalClientes(
        ExtGridFilterList filter,
        String userUID
    ) {
        JPAQuery jpaQuery = getQueryClientes(filter, userUID);

        if (jpaQuery != null) {
            return jpaQuery.list(qCompraDTO.id).size();
        } else {
            return 0;
        }
    }

    private JPAQuery getQueryClientes(
        ExtGridFilterList filter,
        String userUID
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery subquery = new JPAQuery(entityManager);

        BooleanBuilder builder =
            new BooleanBuilder(qUsuarioDTO.usuario.eq(userUID).and(qCompraDTO.infoPeriodica.isTrue()));
        builder.and(comprasDAO.getFilterWhere(filter));

        List<Long> ids =
            subquery.from(qCompraDTO).join(qCompraDTO.parSesion, qSesionDTO).join(qSesionDTO.parSala, qSalaDTO)
                .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).join(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
                .where(builder).groupBy(qCompraDTO.email).distinct().list(qCompraDTO.id.min());

        if (ids != null && ids.size() > 0) {
            BooleanExpression whereStatement = Utils.getInSublistLimited(ids, qCompraDTO.id);

            return query.from(qCompraDTO).leftJoin(qCompraDTO.comoNosConociste, qComoNosConocisteDTO).where(whereStatement);
        } else {
            return null;
        }
    }

    @Transactional
    public boolean removeInfoPeriodica(
        String email,
        String userUID
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        List<Long> ids =
            query.from(qCompraDTO).join(qCompraDTO.parSesion, qSesionDTO).join(qSesionDTO.parSala, qSalaDTO)
                .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).join(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
                .where(qUsuarioDTO.usuario.eq(userUID).and(qCompraDTO.email.eq(email))).list(qCompraDTO.id);

        if (ids != null && ids.size() > 0) {
            BooleanExpression whereStatement = Utils.getInSublistLimited(ids, qCompraDTO.id);

            JPAUpdateClause update = new JPAUpdateClause(entityManager, qCompraDTO);
            update.set(qCompraDTO.infoPeriodica, false).where(whereStatement);
            return update.execute() > 0;
        }
        return false;
    }
}
