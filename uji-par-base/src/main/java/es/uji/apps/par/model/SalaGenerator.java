package es.uji.apps.par.model;

import org.apache.log4j.Logger;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SalaGenerator {
    public static Logger log = Logger.getLogger(SalaGenerator.class);

    private Sala sala;
    private Path fragment;
    private List<Path> butacas;
    private List<Localizacion> localizaciones;
    private long tpvId;

    public SalaGenerator(
        Sala sala,
        List<Localizacion> localizaciones,
        long tpvId
    ) {
        this.sala = sala;
        this.localizaciones = localizaciones;
        this.tpvId = tpvId;
    }

    public String getSql() {
        String array_nombre_es =
            localizaciones.stream().map(l -> String.format("'%s'", l.getNombreEs())).collect(Collectors.joining(","));
        String array_codigo =
            localizaciones.stream().map(l -> String.format("'%s'", l.getCodigo())).collect(Collectors.joining(","));
        String array_total =
            localizaciones.stream().map(l -> String.valueOf(l.getTotalEntradas())).collect(Collectors.joining(","));

        String sqlSala = String.format(
            "select add_sala_usuarios_existentes(%s, '%s', '%s', %s, null," + " ARRAY [%s]," + " ARRAY [%s],"
                + " ARRAY [%s]);", tpvId, sala.getCodigo(), sala.getNombre(), sala.getCine().getId(), array_nombre_es, array_codigo,
            array_total);

        String updateSQL = String.format(
            "UPDATE par_salas SET html_template_name = '%s', asientos_numerados     = true,"
                + " clase_entrada_taquilla = '%s', clase_entrada_online = '%s',"
                + " direccion = '%s' WHERE codigo='%s';", sala.getHtmlTemplateName(), sala.getClaseEntradaTaquilla(),
            sala.getClaseEntradaOnline(), sala.getDireccion(), sala.getCodigo());

        return sqlSala + updateSQL;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public Path getFragment() {
        return fragment;
    }

    public void setFragment(Path fragment) {
        this.fragment = fragment;
    }

    public List<Path> getButacas() {
        return butacas;
    }

    public void setButacas(List<Path> butacas) {
        this.butacas = butacas;
    }
}