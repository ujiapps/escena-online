package es.uji.apps.par.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.db.MailDTO;
import es.uji.apps.par.db.QCompraDTO;
import es.uji.apps.par.db.QMailDTO;
import es.uji.apps.par.db.QMailsBlacklistDTO;
import es.uji.apps.par.utils.DateUtils;

@Repository
public class MailDAO {
    @PersistenceContext
    private EntityManager entityManager;

    private QMailsBlacklistDTO qMailsBlacklistDTO = QMailsBlacklistDTO.mailsBlacklistDTO;
    private QMailDTO qMailDTO = QMailDTO.mailDTO;
    private QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

    @Transactional
    public void insertaMail(
        String de,
        String para,
        String titulo,
        String texto,
        String uuid,
        String urlPublic,
        String urlPieEntrada
    ) {
        MailDTO mailDTO = new MailDTO();

        mailDTO.setDe(de);
        mailDTO.setPara(para);
        mailDTO.setTitulo(titulo);
        mailDTO.setTexto(texto);
        mailDTO.setFechaCreado(new Timestamp(DateUtils.getCurrentDate().getTime()));
        mailDTO.setUuid(uuid);
        mailDTO.setUrlPublic(urlPublic);
        mailDTO.setUrlPieEntrada(urlPieEntrada);
        entityManager.persist(mailDTO);
    }

    public List<MailDTO> getMailsPendientes() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -1);

        List<MailDTO> pendientesCompras = new JPAQuery(entityManager).from(qMailDTO, qCompraDTO).where(qMailDTO.uuid.eq(qCompraDTO.uuid).and(
            qMailDTO.fechaEnviado.isNull().and(qMailDTO.fechaCreado.before(new Timestamp(calendar.getTimeInMillis())))
                .and(qCompraDTO.caducada.isFalse().and(qCompraDTO.pagada.isTrue()).and(qCompraDTO.anulada.isFalse()))))
            .list(qMailDTO);

        return pendientesCompras;
    }

    public List<MailDTO> getAlertasPendientes() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -1);

        List<MailDTO> pendientesAlertas = new JPAQuery(entityManager).from(qMailDTO).where(qMailDTO.uuid.startsWith("alert_").and(
            qMailDTO.fechaEnviado.isNull().and(qMailDTO.fechaCreado.before(new Timestamp(calendar.getTimeInMillis())))))
            .list(qMailDTO);

        return pendientesAlertas;
    }

    @Transactional
    public void marcaEnviado(String uuid) {
        MailDTO mail = getMailByUUID(uuid);

        mail.setFechaEnviado(new Timestamp(DateUtils.getCurrentDate().getTime()));

        entityManager.persist(mail);
    }

    @Transactional
    public MailDTO getMailByUUID(String uuid) {
        return entityManager.find(MailDTO.class, uuid);
    }

    @Transactional
    public boolean isEmailInBlacklist(String email) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qMailsBlacklistDTO).where(qMailsBlacklistDTO.email.eq(email)).exists();
    }
}
