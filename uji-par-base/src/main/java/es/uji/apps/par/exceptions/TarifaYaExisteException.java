package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class TarifaYaExisteException extends GeneralPARException
{
    public TarifaYaExisteException(String message)
    {
        super(TARIFA_EXISTE_CODE, TARIFA_EXISTE + ": " + message);
    }
}
