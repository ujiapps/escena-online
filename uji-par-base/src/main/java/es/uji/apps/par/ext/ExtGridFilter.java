package es.uji.apps.par.ext;

public class ExtGridFilter {

    private String operator;
    private Object value;
    private String property;

    public ExtGridFilter( String property, String operator, Object value)
    {
        this.property = property;
        this.operator = operator;
        this.value = value;
    }

    public ExtGridFilter() {
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Object getValue() {
        return this.value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
