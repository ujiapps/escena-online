package es.uji.apps.par.exceptions;

public class SalaConEventosException extends GeneralPARException {

    public SalaConEventosException() {
        super(SALA_CON_EVENTOS_CODE, SALA_CON_EVENTOS);
    }
}
