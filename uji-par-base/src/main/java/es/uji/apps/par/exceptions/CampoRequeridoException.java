package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class CampoRequeridoException extends GeneralPARException
{
    String message;

    public CampoRequeridoException(String message)
    {
        super(REQUIRED_FIELD_CODE, REQUIRED_FIELD + message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
