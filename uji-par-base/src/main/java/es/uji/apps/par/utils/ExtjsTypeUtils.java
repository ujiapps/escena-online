package es.uji.apps.par.utils;

public class ExtjsTypeUtils {
    public static String booleanToString(Boolean bool) {
        if (bool != null && bool)
            return "on";
        else
            return "";
    }

    public static Boolean stringToBoolean(String str) {
        if (str == null || !str.equals("on"))
            return false;
        else
            return true;
    }
}
