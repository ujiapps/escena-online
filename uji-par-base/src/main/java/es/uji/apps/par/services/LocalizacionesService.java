package es.uji.apps.par.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import es.uji.apps.par.dao.LocalizacionesDAO;
import es.uji.apps.par.dao.SalasDAO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.LocalizacionConEventosException;
import es.uji.apps.par.model.Localizacion;

@Service
public class LocalizacionesService {

    @Autowired
    private LocalizacionesDAO localizacionesDAO;

    @Autowired
    private SalasDAO salasDAO;

    @Autowired
    private LangService langService;

    public List<Localizacion> get(
        String sortParameter,
        int start,
        int limit,
        Integer salaId
    ) {
        List<Localizacion> listaLocalizaciones = new ArrayList<Localizacion>();

        for (LocalizacionDTO localizacionDB : localizacionesDAO.get(sortParameter, start, limit, salaId))
            listaLocalizaciones.add(new Localizacion(localizacionDB));

        return listaLocalizaciones;
    }

    public List<Localizacion> getAforosNoNumeradosDistintos(String userUID) {
        List<Localizacion> listaLocalizaciones = new ArrayList<>();

        for (LocalizacionDTO localizacionDB : localizacionesDAO.getNoNumeradas(userUID)) {
            if (listaLocalizaciones.stream().map(Localizacion::getTotalEntradas)
                .filter(t -> t == localizacionDB.getTotalEntradas().intValue()).count() == 0) {
                listaLocalizaciones.add(new Localizacion(localizacionDB));
            }
        }

        return listaLocalizaciones;
    }

    public void remove(Integer id) {
        try {
            localizacionesDAO.remove(id);
        } catch (PersistenceException e) {
            throw new LocalizacionConEventosException();
        }
    }

    public Localizacion add(Localizacion localizacion) throws CampoRequeridoException {
        checkRequiredFields(localizacion);
        return localizacionesDAO.add(localizacion);
    }

    public Localizacion add(
        Localizacion localizacion,
        Long idSala
    ) throws CampoRequeridoException {
        checkRequiredFields(localizacion);
        SalaDTO salaDTO = this.salasDAO.getSala(idSala);
        localizacion
            .setCodigo(CodigoService.getCodigoFromNombre(salaDTO.getCodigo(), localizacion.getNombreEs(), localizacion.isDiscapacitados()));
        return localizacionesDAO.add(localizacion, salaDTO);
    }

    public void update(Long localizacionId, Localizacion localizacion, String userUID) throws CampoRequeridoException {
        checkRequiredFields(localizacion);
        SalaDTO salaDTO = this.salasDAO.getSalaByLocalizacionId(localizacionId, userUID);
        localizacion
            .setCodigo(CodigoService.getCodigoFromNombre(salaDTO.getCodigo(), localizacion.getNombreEs(), localizacion.isDiscapacitados()));
        localizacionesDAO.update(localizacion);
    }

    private void checkRequiredFields(Localizacion localizacion) throws CampoRequeridoException {
        if (langService.isLangAllowed("es") && (localizacion.getNombreEs() == null || localizacion.getNombreEs().isEmpty()))
            throw new CampoRequeridoException("Nombre");
        if (langService.isLangAllowed("ca") && (localizacion.getNombreVa() == null || localizacion.getNombreVa().isEmpty()))
            throw new CampoRequeridoException("Nom");
    }

    public int getTotalLocalizaciones(Integer salaId) {
        return localizacionesDAO.getTotalLocalizaciones(salaId);
    }

    public List<Localizacion> getLocalizacionesSesion(Long sesionId) {
        List<Localizacion> listaLocalizaciones = new ArrayList<Localizacion>();

        for (LocalizacionDTO localizacionDB : localizacionesDAO.getFromSesion(sesionId))
            listaLocalizaciones.add(new Localizacion(localizacionDB));

        return listaLocalizaciones;
    }
}
