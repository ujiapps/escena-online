package es.uji.apps.par.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import static es.uji.apps.par.exceptions.GeneralPARException.TAMANYO_IMAGEN_ERROR_CODE;

public class UploadFilter implements Filter {
    public final static int MAX_BINARY_SIZE = 1 * 1024 * 1024;
    public final static int MAX_UPLOAD_SIZE = 2 * MAX_BINARY_SIZE;

    public void init(FilterConfig filterConfig) {
    }

    public void destroy() {
    }

    public void doFilter(
        ServletRequest request,
        ServletResponse response,
        FilterChain chain
    ) throws IOException, ServletException {
        if (request.getContentType() != null && request.getContentType().startsWith(MediaType.MULTIPART_FORM_DATA) && request.getContentLength() > MAX_UPLOAD_SIZE) {
            HttpServletResponse sResponse = (HttpServletResponse) response;
            sResponse.reset();
            sResponse.setHeader("Content-Type", "application/json;charset=UTF-8");
            sResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().write(String.format("{\"success\":false, \"codi\":%s}", TAMANYO_IMAGEN_ERROR_CODE));
        } else {
            chain.doFilter(request, response);
        }
    }
}