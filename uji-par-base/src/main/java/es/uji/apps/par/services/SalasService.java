package es.uji.apps.par.services;

import com.google.common.base.Strings;

import com.mysema.query.Tuple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.dao.EventosDAO;
import es.uji.apps.par.dao.LocalizacionesDAO;
import es.uji.apps.par.dao.SalasDAO;
import es.uji.apps.par.dao.TpvsDAO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.TpvsDTO;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.exceptions.SalaConEventosException;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.SalaGenerator;
import es.uji.apps.par.model.SalaHTML;
import es.uji.apps.par.model.Usuario;

@Service
public class SalasService {
    @Autowired
    private Configuration configuration;

    @Autowired
    private SalasDAO salasDAO;

    @Autowired
    private UsersService usersService;

    @Autowired
    private EventosDAO eventosDAO;

    @Autowired
    private LocalizacionesDAO localizacionesDAO;

    @Autowired
    private TpvsDAO tpvsDAO;

    public Sala getSalaByLocalizacionId(
        long localizacionId,
        String userUID
    ) {
        SalaDTO salaDTO = salasDAO.getSalaByLocalizacionId(localizacionId, userUID);
        return Sala.salaDTOtoSala(salaDTO);
    }

    public List<Sala> getSalas(String userUID) {
        return salasDAO.getSalas(userUID);
    }

    public List<Sala> getSalas(
        String userUID,
        ExtGridFilterList filter
    ) {
        return salasDAO.getSalas(userUID, filter);
    }

    public List<Sala> getSalasDisponiblesParaEvento(
        String userUID,
        Long eventoId
    ) {
        List<Sala> salas = new ArrayList<>();

        EventoDTO eventoById = eventosDAO.getEventoById(eventoId, userUID);
        List<Sala> allSalas = salasDAO.getSalasWithLocalizacion(userUID);
        if (eventoById.getAsientosNumerados()) {
            for (Sala sala : allSalas) {
                if (sala.isAsientosNumerados()) {
                    salas.add(sala);
                }
            }
            return salas;
        } else {
            return allSalas;
        }
    }

    @Transactional
    public Sala addSala(
        Sala sala,
        String userUID
    ) {
        Cine cine = usersService.getUserCineByUserUID(userUID);
        return addSala(sala, cine);
    }

    @Transactional
    public Sala addSala(
        Sala sala,
        Cine cine
    ) {
        Sala salaCreada = salasDAO.addSala(createSalaWithDefaultValues(sala, cine));

        List<Usuario> usuariosCine = usersService.getUsersByCine(cine.getId());
        for (Usuario usuario : usuariosCine) {
            usersService.addSalaToUser(salaCreada, usuario);
        }
        return salaCreada;
    }

    public Sala editSala(
        Sala salaEdit
    ) {
        SalaDTO salaDTO = salasDAO.getSala(salaEdit.getId());
        Sala sala = Sala.salaDTOtoSala(salaDTO);
        sala.setNombre(salaEdit.getNombre());
        sala.setDireccion(salaEdit.getDireccion());

        return salasDAO.editSala(sala);
    }

    private Sala createSalaWithDefaultValues(
        Sala sala,
        Cine cine
    ) {
        Sala salaWithDefaultValues = new Sala();
        salaWithDefaultValues.setNombre(sala.getNombre());
        salaWithDefaultValues.setDireccion(sala.getDireccion());
        salaWithDefaultValues.setCine(cine);
        salaWithDefaultValues.setAsientosNumerados(false);
        salaWithDefaultValues.setCodigo(sala.getCodigo() != null ? sala.getCodigo() : CodigoService.getCodigoFromNombre(cine.getCodigo(), sala.getNombre(), false));
        salaWithDefaultValues.setFormato("1");
        salaWithDefaultValues.setHtmlTemplateName("n/a");
        salaWithDefaultValues.setSubtitulo("1");
        salaWithDefaultValues.setTipo("1");
        salaWithDefaultValues.setAforoPorCompras(sala.isAforoPorCompras());

        Tuple clasesEntrada = salasDAO.getClaseEntradasFromCine(cine.getId());
        if (clasesEntrada != null) {
            salaWithDefaultValues.setClaseEntradaTaquilla(clasesEntrada.get(0, String.class));
            salaWithDefaultValues.setClaseEntradaOnline(clasesEntrada.get(1, String.class));
        } else {
            salaWithDefaultValues.setClaseEntradaTaquilla("es.uji.apps.par.report.EntradaTaquillaComisionReport");
            salaWithDefaultValues.setClaseEntradaOnline("es.uji.apps.par.report.EntradaComisionReport");
        }

        return salaWithDefaultValues;
    }

    public void deleteSala(
        Long idSala
    ) {
        try {
            salasDAO.deleteSala(idSala);
        } catch (PersistenceException e) {
            throw new SalaConEventosException();
        }
    }

    @Transactional
    public SalaGenerator createFromHtml(
        SalaHTML salaHTML,
        String ansibleFilesPath,
        Usuario usuario
    ) throws IOException {
        long cineId = salaHTML.getCineId();
        Sala sala = salaHTML.toSala(salasDAO.getClaseEntradasFromCine(cineId));
        List<Localizacion> localizaciones = salaHTML.getLocalizaciones(sala.getCodigo());
        TpvsDTO tpvDefault = tpvsDAO.getTpvDefault(usuario.getUsuario());
        SalaGenerator salaGenerator = new SalaGenerator(sala, localizaciones, tpvDefault.getId());

        Path templatesPath;
        Path butacasPath;
        if (!Strings.isNullOrEmpty(ansibleFilesPath)) {
            templatesPath = Paths.get(ansibleFilesPath, "templates");
            butacasPath = Paths.get(ansibleFilesPath, "par/butacas");
        } else {
            templatesPath = Paths.get("/etc/uji/templates/");
            butacasPath = Paths.get(configuration.getPathJson());

            sala = salasDAO.addSala(sala);
            List<Usuario> usuariosCine = usersService.getUsersByCine(cineId);
            for (Usuario usuarioCine : usuariosCine) {
                usersService.addSalaToUser(sala, usuarioCine);
            }
            for (Localizacion localizacion : localizaciones) {
                localizacionesDAO.add(localizacion, Sala.salaToSalaDTO(sala));
            }
            salaGenerator.setSala(sala);
        }

        salaGenerator.setFragment(salaHTML.writeFragment(Paths.get(templatesPath.toString(),
            Constantes.PLANTILLAS_DIR + sala.getCine().getCodigo() + "/" + sala.getHtmlTemplateName() + ".html")));
        salaGenerator.setButacas(salaHTML.writeButacasJSON(butacasPath));

        return salaGenerator;
    }
}
