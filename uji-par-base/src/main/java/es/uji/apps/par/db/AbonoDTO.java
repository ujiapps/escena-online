package es.uji.apps.par.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PAR_ABONOS")
public class AbonoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "PAR_ABONOS_ID_GENERATOR", sequenceName = "HIBERNATE_SEQUENCE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAR_ABONOS_ID_GENERATOR")
    private long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "MAX_EVENTOS")
    private Long maxEventos;

    @Column(name = "DESCRIPCION_ES", length = 1700)
    private String descripcionEs;

    @Column(name = "DESCRIPCION_VA", length = 1700)
    private String descripcionVa;

    @Lob
    private byte[] imagen;

    @Column(name = "IMAGEN_CONTENT_TYPE")
    private String imagenContentType;

    @Column(name = "IMAGEN_SRC")
    private String imagenSrc;

    @Column(name = "IMAGEN_UUID")
    private String imagenUUID;

    @ManyToOne
    @JoinColumn(name = "ID")
    private TarifaDTO parTarifa;

    @ManyToOne
    @JoinColumn(name = "TPV_ID")
    private TpvsDTO parTpv;

    @ManyToOne
    @JoinColumn(name = "EVENTO_ID")
    private EventoDTO parEvento;

    @ManyToOne
    @JoinColumn(name = "LOCALIZACION_ID")
    private LocalizacionDTO parLocalizacion;

    private BigDecimal precio;

    @Column(name = "PRECIO_ANTICIPADO")
    private BigDecimal precioAnticipado;

    @Column(name = "CANAL_INTERNET")
    private Boolean canalInternet;

    @Column(name = "FECHA_INICIO_VENTA_ONLINE")
    private Timestamp fechaInicioVentaOnline;

    @Column(name = "FECHA_FIN_VENTA_ONLINE")
    private Timestamp fechaFinVentaOnline;

    @Column(name = "FECHA_FIN_VENTA_ANTICIPADA")
    private Timestamp fechaFinVentaAnticipada;

    public AbonoDTO() {
    }

    public AbonoDTO(long eventoId) {
        this.id = eventoId;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getMaxEventos() {
        return maxEventos;
    }

    public void setMaxEventos(Long maxEventos) {
        this.maxEventos = maxEventos;
    }

    public String getDescripcionEs() {
        return descripcionEs;
    }

    public void setDescripcionEs(String descripcionEs) {
        this.descripcionEs = descripcionEs;
    }

    public String getDescripcionVa() {
        return descripcionVa;
    }

    public void setDescripcionVa(String descripcionVa) {
        this.descripcionVa = descripcionVa;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public String getImagenContentType() {
        return imagenContentType;
    }

    public void setImagenContentType(String imagenContentType) {
        this.imagenContentType = imagenContentType;
    }

    public String getImagenSrc() {
        return imagenSrc;
    }

    public void setImagenSrc(String imagenSrc) {
        this.imagenSrc = imagenSrc;
    }

    public String getImagenUUID() {
        return imagenUUID;
    }

    public void setImagenUUID(String imagenUUID) {
        this.imagenUUID = imagenUUID;
    }

    public TarifaDTO getParTarifa() {
        return parTarifa;
    }

    public void setParTarifa(TarifaDTO parTarifa) {
        this.parTarifa = parTarifa;
    }

    public TpvsDTO getParTpv() {
        return parTpv;
    }

    public void setParTpv(TpvsDTO parTpv) {
        this.parTpv = parTpv;
    }

    public EventoDTO getParEvento() {
        return parEvento;
    }

    public void setParEvento(EventoDTO parEvento) {
        this.parEvento = parEvento;
    }

    public LocalizacionDTO getParLocalizacion() {
        return parLocalizacion;
    }

    public void setParLocalizacion(LocalizacionDTO parLocalizacion) {
        this.parLocalizacion = parLocalizacion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public BigDecimal getPrecioAnticipado() {
        return precioAnticipado;
    }

    public void setPrecioAnticipado(BigDecimal precioAnticipado) {
        this.precioAnticipado = precioAnticipado;
    }

    public Boolean getCanalInternet() {
        return canalInternet;
    }

    public void setCanalInternet(Boolean canalInternet) {
        this.canalInternet = canalInternet;
    }

    public Timestamp getFechaFinVentaOnline() {
        return fechaFinVentaOnline;
    }

    public void setFechaFinVentaOnline(Timestamp fechaFinVentaOnline) {
        this.fechaFinVentaOnline = fechaFinVentaOnline;
    }

    public Timestamp getFechaInicioVentaOnline() {
        return fechaInicioVentaOnline;
    }

    public void setFechaInicioVentaOnline(Timestamp fechaInicioVentaOnline) {
        this.fechaInicioVentaOnline = fechaInicioVentaOnline;
    }

    public Timestamp getFechaFinVentaAnticipada() {
        return fechaFinVentaAnticipada;
    }

    public void setFechaFinVentaAnticipada(Timestamp fechaFinVentaAnticipada) {
        this.fechaFinVentaAnticipada = fechaFinVentaAnticipada;
    }
}