package es.uji.apps.par.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CompraRequest
{
    private String observaciones;
    private Long idCompra;
    private List<Butaca> butacasSeleccionadas;

    public Long getIdCompra()
    {
        return idCompra;
    }

    public void setIdCompra(Long idCompra)
    {
        this.idCompra = idCompra;
    }

    public List<Butaca> getButacasSeleccionadas()
    {
        return butacasSeleccionadas;
    }

    public void setButacasSeleccionadas(List<Butaca> butacasSeleccionadas)
    {
        List<Butaca> butacas = Butaca.getNumeradas(butacasSeleccionadas);
        List<Butaca> butacasNoNumeradas = Butaca.getNoNumeradas(butacasSeleccionadas);
        for (Butaca butacaNoNumerada : butacasNoNumeradas) {
            for (int i = 0; i < butacaNoNumerada.getCantidad(); i++) {
                Butaca butaca = new Butaca(butacaNoNumerada.getLocalizacion(), butacaNoNumerada.getTipo());
                butaca.setTexto(butacaNoNumerada.getTexto());
                butacas.add(butaca);
            }
        }
        this.butacasSeleccionadas = butacas;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }
}