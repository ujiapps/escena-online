package es.uji.apps.par.model;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.utils.ExtjsTypeUtils;

@XmlRootElement
public class Tarifa
{
    private long id;
    private String nombre;
    private String isPublica;
	private String isAbono;
    private String defecto;
	private Cine cine;

    public Tarifa()
    {
    }

    public Tarifa(long id)
    {
        this.id = id;
    }

    public Tarifa(TarifaDTO tarifaDTO)
    {
        this.id = tarifaDTO.getId();
        this.nombre = tarifaDTO.getNombre();
        this.isPublica = ExtjsTypeUtils.booleanToString(tarifaDTO.getIsPublica());
		this.isAbono = ExtjsTypeUtils.booleanToString(tarifaDTO.getIsAbono());
        this.defecto = ExtjsTypeUtils.booleanToString(tarifaDTO.getDefecto());
    }

	public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

	public static TarifaDTO toDTO(Tarifa tarifa) {
		TarifaDTO tarifaDTO = new TarifaDTO();
		tarifaDTO.setId(tarifa.getId());
		tarifaDTO.setNombre(tarifa.getNombre());
		tarifaDTO.setIsPublica(ExtjsTypeUtils.stringToBoolean(tarifa.getIsPublica()));
		tarifaDTO.setIsAbono(false);
		tarifaDTO.setDefecto(ExtjsTypeUtils.stringToBoolean(tarifa.getDefecto()));

		return tarifaDTO;
	}

	public static TarifaDTO toDTO(Tarifa tarifa, Cine cine) {
		TarifaDTO tarifaDTO = toDTO(tarifa);
		tarifaDTO.setParCine(new CineDTO(cine.getId()));

		return tarifaDTO;
	}

	public String getIsPublica() {
		return isPublica;
	}

	public void setIsPublica(String isPublica) {
		this.isPublica = isPublica;
	}

	public String getIsAbono() {
		return isAbono;
	}

	public void setIsAbono(String isAbono) {
		this.isAbono = isAbono;
	}

	public static Tarifa tarifaDTOToTarifa(TarifaDTO tarifaDTO) {
		Tarifa tarifa = new Tarifa();
		tarifa.setId(tarifaDTO.getId());
		tarifa.setNombre(tarifaDTO.getNombre());
		tarifa.setIsPublica(ExtjsTypeUtils.booleanToString(tarifaDTO.getIsPublica()));
		tarifa.setIsAbono(ExtjsTypeUtils.booleanToString(tarifaDTO.getIsAbono()));
		tarifa.setDefecto(ExtjsTypeUtils.booleanToString(tarifaDTO.getDefecto()));
		return tarifa;
	}

	public String getDefecto() {
		return defecto;
	}

	public void setDefecto(String defecto) {
		this.defecto = defecto;
	}

	public void setCine(Cine cine)
	{
		this.cine = cine;
	}

	public Cine getCine()
	{
		return cine;
	}
}