package es.uji.apps.par.services;

import org.springframework.stereotype.Service;

import java.text.Normalizer;

@Service
public class CodigoService {
    public static String getCodigoFromNombre(
        String codigo,
        String nombre,
        boolean discapacitados
    ) {
        String codigoNormalized = Normalizer.normalize(codigo != null ? codigo : "", Normalizer.Form.NFD);
        String codigoAccentRemoved = codigoNormalized.replaceAll("[^A-Za-z0-9]", "");

        String nombreNormalized = Normalizer.normalize(nombre, Normalizer.Form.NFD);
        String nombreAccentRemoved = nombreNormalized.replaceAll("[^A-Za-z0-9]", "");

        nombreAccentRemoved = discapacitados ? "discapacitados_" + nombreAccentRemoved : nombreAccentRemoved;
        return String.format("%s_%s", codigoAccentRemoved, nombreAccentRemoved);
    }
}
