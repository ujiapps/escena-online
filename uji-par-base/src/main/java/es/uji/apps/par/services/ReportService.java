package es.uji.apps.par.services;

import com.mysema.query.Tuple;

import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import es.uji.apps.fopreports.serialization.ReportSerializationException;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.config.ConfigurationSelector;
import es.uji.apps.par.dao.ButacasDAO;
import es.uji.apps.par.dao.CinesDAO;
import es.uji.apps.par.dao.ComprasDAO;
import es.uji.apps.par.dao.ReportsDAO;
import es.uji.apps.par.dao.SesionesDAO;
import es.uji.apps.par.dao.UsuariosDAO;
import es.uji.apps.par.database.DatabaseHelper;
import es.uji.apps.par.database.DatabaseHelperFactory;
import es.uji.apps.par.db.ButacaSinEagerDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.enums.TipoPago;
import es.uji.apps.par.exceptions.SinIvaException;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.Informe;
import es.uji.apps.par.model.InformeSesion;
import es.uji.apps.par.model.Report;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.report.EntradaReportFactory;
import es.uji.apps.par.report.InformeAbonoReport;
import es.uji.apps.par.report.InformeInterface;
import es.uji.apps.par.report.InformeModelReport;
import es.uji.apps.par.report.InformeTotalesModelReport;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.Utils;

@Service
public class ReportService {

    @Autowired
    ComprasDAO comprasDAO;

    @Autowired
    ButacasDAO butacasDAO;

    @Autowired
    SesionesDAO sesionesDAO;

    @Autowired
    CinesDAO cinesDAO;

    @Autowired
    UsuariosDAO usuariosDAO;

    @Autowired
    ReportsDAO reportsDAO;

    Configuration configuration;

    ConfigurationSelector configurationSelector;

    private DatabaseHelper dbHelper;

    private InformeInterface informeReport;

    @Autowired
    public ReportService(
        Configuration configuration,
        ConfigurationSelector configurationSelector
    ) {
        this.configuration = configuration;
        this.configurationSelector = configurationSelector;
        dbHelper = DatabaseHelperFactory.newInstance(this.configuration);
    }

    public ByteArrayOutputStream getExcelTaquilla(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        String tpvId
    ) throws IOException, ParseException {
        ExcelService excelService = getExcelServiceVentas(fechaInicio, fechaFin, locale, userUID, false, tpvId);
        return excelService.getExcel();
    }

    public ByteArrayOutputStream getExcelVentas(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        String tpvId
    ) throws IOException, ParseException {
        ExcelService excelService = getExcelServiceVentas(fechaInicio, fechaFin, locale, userUID, true, tpvId);
        return excelService.getExcel();
    }

    public ExcelService getExcelServiceVentas(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        boolean online,
        String tpvId
    ) throws ParseException {
        List<Object[]> files = comprasDAO.getComprasInFechas(fechaInicio, fechaFin, userUID, online, tpvId);
        ExcelService excelService = new ExcelService();
        int rownum = 0;

        if (files != null && files.size() > 0) {
            excelService.addFulla(
                ResourceProperties.getProperty(locale, online ? "excelVentas.titulo" : "excelTaquilla.titulo") + " "
                    + fechaInicio + " - " + fechaFin);

            excelService.generaCeldes(excelService.getEstilNegreta(), 0,
                ResourceProperties.getProperty(locale, "excelTaquilla.evento"),
                ResourceProperties.getProperty(locale, "excelTaquilla.sesion"),
                ResourceProperties.getProperty(locale, "excelTaquilla.tipoEntrada"),
                ResourceProperties.getProperty(locale, "excelTaquilla.localizacion"),
                ResourceProperties.getProperty(locale, "excelTaquilla.numeroEntradas"),
                ResourceProperties.getProperty(locale, "excelTaquilla.total"));

            for (Object[] fila : files) {
                rownum++;
                addDadesTaquilla(rownum, objectToInforme(fila), excelService);
            }
        }
        return excelService;
    }

    private void addDadesTaquilla(
        int i,
        Informe fila,
        ExcelService excelService
    ) {
        Row row = excelService.getNewRow(i);
        excelService.addCell(0, fila.getEvento(), null, row);
        excelService.addCell(1, fila.getSesion(), null, row);
        excelService.addCell(2, fila.getTipoEntrada(), null, row);
        excelService.addCell(3, fila.getLocalizacion(), null, row);
        excelService.addCell(4, fila.getNumeroEntradas(), null, row);
        excelService.addCell(5, fila.getTotal().floatValue(), row);
    }

    private void addDadesEvento(
        int i,
        InformeModelReport fila,
        ExcelService excelService
    ) {
        Row row = excelService.getNewRow(i);
        excelService.addCell(0, fila.getEvento(), null, row);
        excelService.addCell(1, fila.getTipoEntrada(), null, row);
        excelService.addCell(2, fila.getTipoCompra(), null, row);
        excelService.addCell(3, fila.getNumeroEntradas(), null, row);
        excelService.addCell(4, fila.getTotal().floatValue(), row);
    }

    private void addDadesCompra(
        int i,
        InformeModelReport fila,
        ExcelService excelService,
        Locale locale
    ) {
        Row row = excelService.getNewRow(i);
        excelService.addCell(0, fila.getEvento(), null, row);
        excelService.addCell(1, fila.getSesion(), null, row);
        excelService.addCell(2, fila.getFechaCompra(), null, row);
        excelService.addCell(3, fila.getTipoEntrada(), null, row);
        excelService.addCell(4, ResourceProperties.getProperty(locale, "paymode." + fila.getTipoPago().toString().toLowerCase()), null, row);
        excelService.addCell(5, Integer.toString(fila.getNumeroEntradas()), null, row);
        excelService.addCell(6, String.format("%.2f", fila.getTotal()), null, row);
    }

    public ByteArrayOutputStream getExcelEventos(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        String tpvId
    ) throws IOException, ParseException {
        ExcelService excelService = getExcelServiceEventos(fechaInicio, fechaFin, locale, userUID, tpvId);
        return excelService.getExcel();
    }

    public ExcelService getExcelServiceEventos(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        String tpvId
    ) throws ParseException {
        List<Object[]> files = comprasDAO.getComprasAgrupadasPorEventoInFechas(fechaInicio, fechaFin, userUID, tpvId);
        ExcelService excelService = new ExcelService();
        int rownum = 0;

        if (files != null && files.size() > 0) {
            excelService.addFulla(
                ResourceProperties.getProperty(locale, "excelEventos.titulo") + " " + fechaInicio + " - " + fechaFin);
            excelService.generaCeldes(excelService.getEstilNegreta(), 0,
                ResourceProperties.getProperty(locale, "excelEventos.evento"),
                ResourceProperties.getProperty(locale, "excelEventos.tipoEntrada"),
                ResourceProperties.getProperty(locale, "excelEventos.tipo"),
                ResourceProperties.getProperty(locale, "excelEventos.numeroEntradas"),
                ResourceProperties.getProperty(locale, "excelEventos.total"));

            for (Object[] fila : files) {
                rownum++;
                addDadesEvento(rownum, objectToInformeEvento(fila), excelService);
            }
        }
        return excelService;
    }

    synchronized public void getTaquillaCompras(
        String fechaInicio,
        String fechaFin,
        OutputStream bos,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        boolean groupedByCompras,
        String tpvId,
        boolean formatExcel
    ) throws ReportSerializationException, ParseException, IOException {
        if (formatExcel) {
            ExcelService excelService = generaYRellenaInformeExcelTaquilla(fechaInicio, fechaFin, locale, userUID, groupedByCompras, tpvId);
            bos.write(excelService.getExcel().toByteArray());
        } else {
            InformeInterface informe =
                generaYRellenaInformePDFTaquilla(fechaInicio, fechaFin, locale, userUID, logoReport, showIVA,
                    showTotalesSinComision, location, sort, groupedByCompras, tpvId);
            informe.serialize(bos);
        }
    }

    private List<InformeModelReport> getInformeModelReportsTaquilla(
        String fechaInicio,
        String fechaFin,
        String userUID,
        boolean groupedByCompras,
        String tpvId
    ) throws ParseException {
        List<InformeModelReport> compras;
        if (groupedByCompras) {
            compras = objectsToInformes(comprasDAO.getComprasPorEventoInFechas(fechaInicio, fechaFin, userUID, tpvId));
        } else {
            compras = objectsToInformes(
                comprasDAO.getComprasAgrupadasPorEventoInFechas(fechaInicio, fechaFin, userUID, tpvId));
        }
        return compras;
    }

    public ExcelService generaYRellenaInformeExcelTaquilla(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        boolean groupedByCompras,
        String tpvId
    ) throws ParseException {
        List<InformeModelReport> compras =
            getInformeModelReportsTaquilla(fechaInicio, fechaFin, userUID, groupedByCompras, tpvId);

        ExcelService excelService = new ExcelService();
        int rownum = 0;

        if (compras != null && compras.size() > 0) {
            excelService.addFulla(
                ResourceProperties.getProperty(locale, "excelEventos.titulo") + " " + fechaInicio + " - " + fechaFin);
            excelService.generaCeldes(excelService.getEstilNegreta(), 0,
                ResourceProperties.getProperty(locale, "informeTaquilla.tabla.evento"),
                ResourceProperties.getProperty(locale, "informeTaquilla.tabla.fechaSesion"),
                ResourceProperties.getProperty(locale, "informeTaquilla.tabla.fechaCompra"),
                ResourceProperties.getProperty(locale, "informeTaquilla.tabla.tipo"),
                ResourceProperties.getProperty(locale, "informeTaquilla.tabla.paymode"),
                ResourceProperties.getProperty(locale, "informeTaquilla.tabla.numero"),
                ResourceProperties.getProperty(locale, "informeTaquilla.tabla.total"));

            for (InformeModelReport compra : compras) {
                rownum++;
                addDadesCompra(rownum, compra, excelService, locale);
            }
        }
        return excelService;
    }

    public InformeInterface generaYRellenaInformePDFTaquilla(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        boolean groupedByCompras,
        String tpvId
    ) throws ParseException {
        List<InformeModelReport> compras =
            getInformeModelReportsTaquilla(fechaInicio, fechaFin, userUID, groupedByCompras, tpvId);

        String className;
        if (groupedByCompras) {
            className = usuariosDAO
                .getReportClassNameForUserAndType(userUID, EntradaReportFactory.TIPO_INFORME_PDF_MI_TAQUILLA);
        } else {
            className =
                usuariosDAO.getReportClassNameForUserAndType(userUID, EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA);
        }
        InformeInterface informeTaquillaReport = EntradaReportFactory.newInstanceInformeTaquilla(className);
        InformeInterface informe =
            informeTaquillaReport.create(locale, configuration, logoReport, showIVA, showTotalesSinComision, location);

        Object[] taquillaTpv =
            comprasDAO.getTotalNueva(fechaInicio, fechaFin, userUID, TipoPago.TARJETA, true, false, tpvId);
        Object[] taquillaTpvOffline =
            comprasDAO.getTotalNueva(fechaInicio, fechaFin, userUID, TipoPago.TARJETAOFFLINE, true, false, tpvId);
        Object[] taquillaEfectivo =
            comprasDAO.getTotalNueva(fechaInicio, fechaFin, userUID, TipoPago.METALICO, true, false, tpvId);
        Object[] taquillaCredito =
            comprasDAO.getTotalNueva(fechaInicio, fechaFin, userUID, TipoPago.CREDITO, true, false, tpvId);
        Object[] taquillaTransferencia =
            comprasDAO.getTotalNueva(fechaInicio, fechaFin, userUID, TipoPago.TRANSFERENCIA, true, false, tpvId);
        Object[] online =
            comprasDAO.getTotalNueva(fechaInicio, fechaFin, userUID, TipoPago.TARJETA, false, false, tpvId);

        InformeTotalesModelReport informeTotalesModelReport =
            getInformeTotalesModelReport(taquillaTpv, taquillaTpvOffline, taquillaEfectivo, taquillaCredito,
                taquillaTransferencia, online);

        Usuario usuario = usuariosDAO.getUserById(userUID);
        informe.genera(getSpanishStringDateFromBBDDString(fechaInicio), getSpanishStringDateFromBBDDString(fechaFin),
            compras, informeTotalesModelReport, usuario.getNombre(), sort);
        return informe;
    }

    synchronized public void getPdfMiTaquilla(
        String fechaInicio,
        String fechaFin,
        OutputStream bos,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        boolean dateSession,
        String tpvId
    ) throws ReportSerializationException, ParseException {
        InformeInterface informe =
            generaYRellenaInformePDFMiTaquilla(fechaInicio, fechaFin, locale, userUID, logoReport, showIVA,
                showTotalesSinComision, location, sort, dateSession, tpvId);
        informe.serialize(bos);
    }

    public InformeInterface generaYRellenaInformePDFMiTaquilla(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        boolean dateSession,
        String tpvId
    ) throws ParseException {
        String className =
            usuariosDAO.getReportClassNameForUserAndType(userUID, EntradaReportFactory.TIPO_INFORME_PDF_MI_TAQUILLA);
        InformeInterface informeTaquillaReport = EntradaReportFactory.newInstanceInformeTaquilla(className);
        InformeInterface informe =
            informeTaquillaReport.create(locale, configuration, logoReport, showIVA, showTotalesSinComision, location);

        List<InformeModelReport> compras = objectsToInformes(
            comprasDAO.getComprasMiTaquillaPorEventoInFechas(fechaInicio, fechaFin, dateSession, userUID, tpvId));

        Object[] taquillaTpv =
            comprasDAO.getTotalNuevaTaquilla(fechaInicio, fechaFin, userUID, TipoPago.TARJETA, dateSession, tpvId);
        Object[] taquillaTpvOffline = comprasDAO
            .getTotalNuevaTaquilla(fechaInicio, fechaFin, userUID, TipoPago.TARJETAOFFLINE, dateSession, tpvId);
        Object[] taquillaEfectivo =
            comprasDAO.getTotalNuevaTaquilla(fechaInicio, fechaFin, userUID, TipoPago.METALICO, dateSession, tpvId);
        Object[] taquillaCredito =
            comprasDAO.getTotalNuevaTaquilla(fechaInicio, fechaFin, userUID, TipoPago.CREDITO, dateSession, tpvId);
        Object[] taquillaTransferencia = comprasDAO
            .getTotalNuevaTaquilla(fechaInicio, fechaFin, userUID, TipoPago.TRANSFERENCIA, dateSession, tpvId);

        InformeTotalesModelReport informeTotalesModelReport =
            getInformeTotalesModelReport(taquillaTpv, taquillaTpvOffline, taquillaEfectivo, taquillaCredito,
                taquillaTransferencia, null);

        Usuario usuario = usuariosDAO.getUserById(userUID);
        informe.genera(getSpanishStringDateFromBBDDString(fechaInicio), getSpanishStringDateFromBBDDString(fechaFin),
            compras, informeTotalesModelReport, usuario.getNombre(), sort);
        return informe;
    }

    synchronized public void generaYRellenaInformePDFIncidencias(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String logoReport,
        String location,
        String userUID,
        OutputStream bos
    ) throws ParseException, ReportSerializationException {
        String className =
            usuariosDAO.getReportClassNameForUserAndType(userUID, EntradaReportFactory.TIPO_INFORME_PDF_INCIDENCIAS);
        InformeInterface informeIncidenciaReport = EntradaReportFactory.newInstanceInformeReport(className, null);
        InformeInterface informe =
            informeIncidenciaReport.create(locale, configuration, logoReport, false, false, location);

        List<SesionDTO> sesionesDTO = sesionesDAO
            .getSesionesCinePorFechas(DateUtils.databaseWithSecondsToDate(fechaInicio + " " + "00:00:00"),
                DateUtils.databaseWithSecondsToDate(fechaFin + " 23:59:59"),  true,"fechaCelebracion", userUID);
        List<InformeSesion> listaInformesSesion = new ArrayList<InformeSesion>();

        for (SesionDTO sesionDTO : sesionesDTO) {
            Sesion sesion = Sesion.SesionDTOToSesion(sesionDTO);
            Sala sala = Sala.salaDTOtoSala(sesionDTO.getParSala());
            Evento evento = Evento.eventoDTOtoEvento(sesionDTO.getParEvento());

            InformeSesion informeSesion = new InformeSesion();
            informeSesion.setSala(sala);
            informeSesion.setSesion(sesion);
            informeSesion.setEvento(evento);
            informeSesion.setTipoIncidenciaId(sesion.getIncidenciaId());
            listaInformesSesion.add(informeSesion);
        }

        Cine cine = Cine.cineDTOToCine(cinesDAO.getCines(userUID).get(0), false);
        informe.genera(cine, getSpanishStringDateFromBBDDString(fechaInicio), getSpanishStringDateFromBBDDString(fechaFin),
            listaInformesSesion);
        informe.serialize(bos);
    }

    private String getSpanishStringDateFromBBDDString(String fecha) throws ParseException {
        Date dt = DateUtils.databaseStringToDate(fecha);
        return DateUtils.dateToSpanishString(dt);
    }

    synchronized public void getPdfEfectivo(
        String fechaInicio,
        String fechaFin,
        OutputStream bos,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        String tpvId
    ) throws ReportSerializationException, ParseException, SinIvaException {
        InformeInterface informe =
            generaYRellenaInformePDFEfectivo(fechaInicio, fechaFin, locale, userUID, logoReport, showIVA,
                showTotalesSinComision, location, sort, tpvId);
        informe.serialize(bos);
    }

    public InformeInterface generaYRellenaInformePDFEfectivo(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        String tpvId
    ) throws ParseException {
        String className =
            usuariosDAO.getReportClassNameForUserAndType(userUID, EntradaReportFactory.TIPO_INFORME_PDF_EFECTIVO);
        InformeInterface informeEfectivoReport = EntradaReportFactory.newInstanceInformeEfectivo(className);
        InformeInterface informe =
            informeEfectivoReport.create(locale, configuration, logoReport, showIVA, showTotalesSinComision, location);
        List<InformeModelReport> compras =
            objectsSesionesToInformesIva(comprasDAO.getComprasEfectivo(fechaInicio, fechaFin, userUID, tpvId));
        List<InformeAbonoReport> abonos = new ArrayList<InformeAbonoReport>();

        informe.genera(getSpanishStringDateFromBBDDString(fechaInicio), getSpanishStringDateFromBBDDString(fechaFin),
            compras, abonos, configuration.getCargoInformeEfectivo(), configuration.getFirmanteInformeEfectivo(), sort);
        return informe;
    }

    synchronized public void getPdfTpvSubtotales(
        String fechaInicio,
        String fechaFin,
        OutputStream bos,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        String tpvId
    ) throws ReportSerializationException, ParseException, SinIvaException {
        InformeInterface informe =
            generaYRellenaInformePDFTaquillaTPVSubtotales(fechaInicio, fechaFin, locale, userUID, logoReport, showIVA,
                showTotalesSinComision, location, sort, tpvId);
        informe.serialize(bos);
    }

    synchronized public void getPdfTpvOnlineSubtotales(
        String fechaInicio,
        String fechaFin,
        OutputStream bos,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        String tpvId
    ) throws ReportSerializationException, ParseException, SinIvaException {
        InformeInterface informe =
            generaYRellenaInformePDFOnlineTPVSubtotales(fechaInicio, fechaFin, locale, userUID, logoReport, showIVA,
                showTotalesSinComision, location, sort, tpvId);
        informe.serialize(bos);
    }

    public InformeInterface generaYRellenaInformePDFTaquillaTPVSubtotales(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        String tpvId
    ) throws ParseException {
        return generaYRellenaInformePDFTypeTPVSubtotales(fechaInicio, fechaFin, locale, userUID, false, logoReport,
            showIVA, showTotalesSinComision, location, sort, tpvId);
    }

    public InformeInterface generaYRellenaInformePDFOnlineTPVSubtotales(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        String tpvId
    ) throws ParseException {
        return generaYRellenaInformePDFTypeTPVSubtotales(fechaInicio, fechaFin, locale, userUID, true, logoReport,
            showIVA, showTotalesSinComision, location, sort, tpvId);
    }

    private InformeInterface generaYRellenaInformePDFTypeTPVSubtotales(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        boolean onlyOnline,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        String tpvId
    ) throws ParseException {
        String className = usuariosDAO
            .getReportClassNameForUserAndType(userUID, EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA_TPV_SUBTOTALES);
        InformeInterface informeTaquillaTpvSubtotalesReport =
            EntradaReportFactory.newInstanceInformeTaquillaTpvSubtotalesReport(className);
        InformeInterface informe = informeTaquillaTpvSubtotalesReport
            .create(locale, configuration, logoReport, showIVA, showTotalesSinComision, location);
        List<Object[]> comprasTpv = onlyOnline ?
            comprasDAO.getComprasTpvOnline(fechaInicio, fechaFin, userUID, tpvId) :
            comprasDAO.getComprasTpv(fechaInicio, fechaFin, userUID, tpvId);
        List<InformeModelReport> compras = objectsSesionesToInformesTpv(comprasTpv);

        String title = onlyOnline ?
            ResourceProperties.getProperty(locale, "informeTaquillaTpvSubtotales.tituloCabeceraOnline") :
            ResourceProperties.getProperty(locale, "informeTaquillaTpvSubtotales.tituloCabecera");
        informe.genera(title, getSpanishStringDateFromBBDDString(fechaInicio),
            getSpanishStringDateFromBBDDString(fechaFin), compras, null, configuration.getCargoInformeEfectivo(),
            configuration.getFirmanteInformeEfectivo(), sort);
        return informe;
    }

    synchronized public void getPdfEventos(
        String fechaInicio,
        String fechaFin,
        OutputStream bos,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        String tpvId
    ) throws ReportSerializationException, ParseException, SinIvaException {
        InformeInterface informe =
            generaYRellenaInformePDFEventos(fechaInicio, fechaFin, locale, userUID, logoReport, showIVA,
                showTotalesSinComision, location, sort, tpvId);
        informe.serialize(bos);
    }

    public InformeInterface generaYRellenaInformePDFEventos(
        String fechaInicio,
        String fechaFin,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort,
        String tpvId
    ) throws ParseException {
        String className =
            usuariosDAO.getReportClassNameForUserAndType(userUID, EntradaReportFactory.TIPO_INFORME_PDF_EVENTOS);
        InformeInterface informeEventosReport = EntradaReportFactory.newInstanceInformeEventosReport(className);
        InformeInterface informe =
            informeEventosReport.create(locale, configuration, logoReport, showIVA, showTotalesSinComision, location);

        List<InformeModelReport> compras =
            objectsSesionesToInformesEventos(comprasDAO.getComprasEventos(fechaInicio, fechaFin, userUID, tpvId));

        informe.genera(getSpanishStringDateFromBBDDString(fechaInicio), getSpanishStringDateFromBBDDString(fechaFin),
            compras, sort);
        return informe;
    }

    private List<InformeModelReport> objectsToInformes(List<Object[]> compras) {
        List<InformeModelReport> result = new ArrayList<InformeModelReport>();

        for (Object[] compra : compras) {
            result.add(objectToInformeEvento(compra));
        }

        return result;
    }

    private List<InformeModelReport> objectsSesionesToInformesIva(
        List<Object[]> compras
    ) {
        List<InformeModelReport> result = new ArrayList<InformeModelReport>();

        for (Object[] compra : compras) {
            result.add(objectToInformeIva(compra));
        }

        return result;
    }

    private List<InformeAbonoReport> objectsAbonosToInformesIva(List<Object[]> abonos) {
        List<InformeAbonoReport> result = new ArrayList<InformeAbonoReport>();

        for (Object[] abono : abonos) {
            result.add(objectToInformeAbonoIva(abono));
        }

        return result;
    }

    private List<InformeModelReport> objectsSesionesToInformesTpv(
        List<Object[]> compras
    ) {
        List<InformeModelReport> result = new ArrayList<InformeModelReport>();

        for (Object[] compra : compras) {
            result.add(objectToInformeTpv(compra));
        }

        return result;
    }

    private List<InformeModelReport> objectsSesionesToInformesEventos(
        List<Object[]> compras
    ) {
        List<InformeModelReport> result = new ArrayList<InformeModelReport>();

        for (Object[] compra : compras) {
            result.add(objectToEvento(compra));
        }

        return result;
    }

    synchronized public void getPdfSesion(
        long sesionId,
        ByteArrayOutputStream bos,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        boolean anuladas,
        String sort
    ) throws SinIvaException, ReportSerializationException {
        Sesion sesion = new Sesion(new Long(sesionId).intValue());
        InformeInterface informe =
            generaYRellenaPDFSesiones(Arrays.asList(sesion), locale, userUID, logoReport, showIVA,
                showTotalesSinComision, location, anuladas, sort,  false, null, null);
        informe.serialize(bos);
    }

    synchronized public void getPdfSesionOnline(
        long sesionId,
        ByteArrayOutputStream bos,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort
    ) throws SinIvaException, ReportSerializationException {
        Sesion sesion = new Sesion(new Long(sesionId).intValue());
        InformeInterface informe =
            generaYRellenaPDFSesionesOnline(Arrays.asList(sesion), locale, userUID, logoReport, showIVA,
                showTotalesSinComision, location, sort);
        informe.serialize(bos);
    }

    synchronized public void getPdfSesiones(
        List<Sesion> sesiones,
        ByteArrayOutputStream bos,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        boolean anuladas,
        String sort,
        boolean icaa,
        String fechaInicioString,
        String fechaFinString
    ) throws SinIvaException, ReportSerializationException {
        InformeInterface informe =
            generaYRellenaPDFSesiones(sesiones, locale, userUID, logoReport, showIVA, showTotalesSinComision, location,
                anuladas, sort, icaa, fechaInicioString, fechaFinString);
        informe.serialize(bos);
    }

    public InformeInterface generaYRellenaPDFSesiones(
        List<Sesion> sesiones,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        boolean anuladas,
        String sort,
        boolean icaa,
        String fechaInicioString,
        String fechaFinString
    ) {
        String className = usuariosDAO.getReportClassNameForUserAndType(userUID, anuladas ?
            EntradaReportFactory.TIPO_INFORME_PDF_SESIONES :
            EntradaReportFactory.TIPO_INFORME_PDF_SESIONES_NO_ANULADAS);
        InformeInterface informeSesionReport = EntradaReportFactory.newInstanceInformeSesionReport(className);
        InformeInterface informe =
            informeSesionReport.create(locale, configuration, logoReport, showIVA, showTotalesSinComision, location);
        List<InformeSesion> informesSesion = new ArrayList<InformeSesion>();
        Cine cine = Cine.cineDTOToCine(cinesDAO.getCines(userUID).get(0), false);

        for (Sesion sesion : sesiones) {
            long sesionId = sesion.getId();
            informesSesion.add(getInformeSesion(sesionId, userUID, true));
        }

        boolean printSesion = (sesiones.size() == 1) ? true : false;
        informe
            .genera(configuration.getCargoInformeEfectivo(), configuration.getFirmanteInformeEfectivo(), informesSesion,
                cine, printSesion, !icaa, fechaInicioString, fechaFinString, sort);
        return informe;
    }

    public InformeInterface generaYRellenaPDFSesionesOnline(
        List<Sesion> sesiones,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location,
        String sort
    ) {
        String className = usuariosDAO
            .getReportClassNameForUserAndType(userUID, EntradaReportFactory.TIPO_INFORME_PDF_SESIONES_ONLINE);
        InformeInterface informeSesionReport = EntradaReportFactory.newInstanceInformeSesionReport(className);
        InformeInterface informe =
            informeSesionReport.create(locale, configuration, logoReport, showIVA, showTotalesSinComision, location);
        List<InformeSesion> informesSesion = new ArrayList<InformeSesion>();
        Cine cine = Cine.cineDTOToCine(cinesDAO.getCines(userUID).get(0), false);

        for (Sesion sesion : sesiones) {
            long sesionId = sesion.getId();
            informesSesion.add(getInformeSesion(sesionId, userUID));
        }

        boolean printSesion = (sesiones.size() == 1) ? true : false;
        informe
            .genera(configuration.getCargoInformeEfectivo(), configuration.getFirmanteInformeEfectivo(), informesSesion,
                cine, printSesion, true, null, null, sort);
        return informe;
    }

    synchronized public void getPdf(
        long sesionId,
        ByteArrayOutputStream bos,
        String tipo,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location
    ) throws SinIvaException, ReportSerializationException {
        String className = usuariosDAO
            .getReportClassNameForUserAndType(userUID, tipo);
        informeReport = EntradaReportFactory.newInstanceInformeReport(className, configuration);
        InformeInterface informe =
            informeReport.create(locale, configuration, logoReport, showIVA, showTotalesSinComision, location);
        informe.genera(sesionId, userUID);

        informe.serialize(bos);
    }

    private InformeSesion getInformeSesion(
        long sesionId,
        String userUID
    ) {
        return getInformeSesion(sesionId, userUID, false);
    }

    private InformeSesion getInformeSesion(
        long sesionId,
        String userUID,
        boolean allCompras
    ) {
        SesionDTO sesionDTO = sesionesDAO.getSesion(sesionId, userUID);
        Sesion sesion = Sesion.SesionDTOToSesion(sesionDTO);
        Sala sala = Sala.salaDTOtoSala(sesionDTO.getParSala());
        Evento evento = Evento.eventoDTOtoEvento(sesionDTO.getParEvento());
        InformeModelReport resumen = comprasDAO.getResumenSesion(sesionId, userUID, allCompras);
        List<Tuple> butacasYTarifas = butacasDAO.getButacasPagadas(sesionId, userUID, allCompras);

        List<InformeModelReport> compras = new ArrayList<>();
        String horasVentaAnticipada = configuration.getHorasVentaAnticipada();
        boolean localizacionEnValenciano = configurationSelector.getLocalizacionEnValenciano();
        for (Tuple butacaYTarifa : butacasYTarifas) {
            ButacaSinEagerDTO butacaDTO = butacaYTarifa.get(0, ButacaSinEagerDTO.class);
            String nombreTarifa = butacaYTarifa.get(1, String.class);
            InformeModelReport informeModel =
                InformeModelReport.fromButaca(butacaDTO, horasVentaAnticipada, localizacionEnValenciano);
            informeModel.setTipoEntrada(nombreTarifa);
            compras.add(informeModel);
        }

        InformeSesion informeSesion = new InformeSesion();
        informeSesion.setSala(sala);
        informeSesion.setSesion(sesion);
        informeSesion.setEvento(evento);
        informeSesion.setVendidas(resumen.getNumeroEntradas());
        informeSesion.setAnuladas(resumen.getCanceladasTaquilla());
        informeSesion.setTotalSinComision(resumen.getTotalSinComision());
        informeSesion.setTotal(resumen.getTotal());
        informeSesion.setCompras(compras);

        return informeSesion;
    }

    synchronized public void getPdfPorFechas(
        String fechaInicio,
        String fechaFin,
        String tipo,
        ByteArrayOutputStream ostream,
        Locale locale,
        String userUID,
        String logoReport,
        boolean showIVA,
        boolean showTotalesSinComision,
        String location
    ) throws ReportSerializationException, ParseException {
        informeReport = EntradaReportFactory.newInstanceInformeReport(tipo, configuration);
        InformeInterface informe =
            informeReport.create(locale, configuration, logoReport, showIVA, showTotalesSinComision, location);
        informe.genera(fechaInicio, fechaFin, userUID);
        informe.serialize(ostream);
    }

    public void addReportsToCine(Cine cine) {
        List<Report> reports = Arrays.asList(new Report(cine.getId(), "es.uji.apps.par.report.InformeEfectivoReport",
                EntradaReportFactory.TIPO_INFORME_PDF_EFECTIVO),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeEventosReport",
                EntradaReportFactory.TIPO_INFORME_PDF_EVENTOS),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeIncidenciasReport",
                EntradaReportFactory.TIPO_INFORME_PDF_INCIDENCIAS),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeSesionNoAnuladasReport",
                EntradaReportFactory.TIPO_INFORME_PDF_SESIONES_NO_ANULADAS),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeSesionReport",
                EntradaReportFactory.TIPO_INFORME_PDF_SESIONES),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeTaquillaReport",
                EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA),
            new Report(cine.getId(), "es.uji.apps.par.report.InformeTaquillaTpvSubtotalesReport",
                EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA_TPV_SUBTOTALES));

        reportsDAO.addReports(reports);
    }

    private InformeTotalesModelReport getInformeTotalesModelReport(
        Object[] taquillaTpv,
        Object[] taquillaTpvOffline,
        Object[] taquillaEfectivo,
        Object[] taquillaCredito,
        Object[] taquillaTransferencia,
        Object[] online
    ) {
        InformeTotalesModelReport informeTotalesModelReport = new InformeTotalesModelReport();
        if (taquillaTpv != null) {
            if (taquillaTpv.length > 0 && taquillaTpv[0] != null) {
                informeTotalesModelReport.setTotalTaquillaTpv(dbHelper.castBigDecimal(taquillaTpv[0]));
            }

            if (taquillaTpv.length > 1 && taquillaTpv[1] != null) {
                informeTotalesModelReport.setNumeroEntradasTPV(dbHelper.castBigDecimal(taquillaTpv[1]));
            }
        }

        if (taquillaTpvOffline != null) {
            if (taquillaTpvOffline.length > 0 && taquillaTpvOffline[0] != null) {
                informeTotalesModelReport.setTotalTaquillaTpv(informeTotalesModelReport.getTotalTaquillaTpv()
                    .add(dbHelper.castBigDecimal(taquillaTpvOffline[0])));
            }
            if (taquillaTpvOffline.length > 1 && taquillaTpvOffline[1] != null) {
                informeTotalesModelReport.setNumeroEntradasTPV(informeTotalesModelReport.getNumeroEntradasTPV()
                    .add(dbHelper.castBigDecimal(taquillaTpvOffline[1])));
            }
        }

        if (taquillaEfectivo != null) {
            if (taquillaEfectivo.length > 0 && taquillaEfectivo[0] != null) {
                informeTotalesModelReport.setTotalTaquillaEfectivo(dbHelper.castBigDecimal(taquillaEfectivo[0]));
            }
            if (taquillaEfectivo.length > 1 && taquillaEfectivo[1] != null) {
                informeTotalesModelReport.setNumeroEntradasEfectivo(dbHelper.castBigDecimal(taquillaEfectivo[1]));
            }
        }

        if (taquillaCredito != null) {
            if (taquillaCredito.length > 0 && taquillaCredito[0] != null) {
                informeTotalesModelReport.setTotalTaquillaCredito(dbHelper.castBigDecimal(taquillaCredito[0]));
            }
            if (taquillaCredito.length > 1 && taquillaCredito[1] != null) {
                informeTotalesModelReport.setNumeroEntradasCredito(dbHelper.castBigDecimal(taquillaCredito[1]));
            }
        }

        if (taquillaTransferencia != null) {
            if (taquillaTransferencia.length > 0 && taquillaTransferencia[0] != null) {
                informeTotalesModelReport
                    .setTotalTaquillaTransferencia(dbHelper.castBigDecimal(taquillaTransferencia[0]));
            }
            if (taquillaTransferencia.length > 1 && taquillaTransferencia[1] != null) {
                informeTotalesModelReport
                    .setNumeroEntradasTransferencia(dbHelper.castBigDecimal(taquillaTransferencia[1]));
            }
        }

        if (online != null) {
            if (online.length > 0 && online[0] != null) {
                informeTotalesModelReport.setTotalOnline(dbHelper.castBigDecimal(online[0]));
            }
            if (online.length > 1 && online[1] != null) {
                informeTotalesModelReport.setNumeroEntradasOnline(dbHelper.castBigDecimal(online[1]));
            }
        }
        return informeTotalesModelReport;
    }

    private Informe objectToInforme(Object[] fila) {
        Informe informe = new Informe();
        informe.setEvento(Utils.safeObjectToString(fila[0]));
        informe.setSesion(DateUtils.dateToSpanishStringWithHour(Utils.objectToDate(fila[1])).toString());
        String tipoEntrada = Utils.safeObjectToString(fila[8]);
        informe.setTipoEntrada(tipoEntrada);
        informe.setLocalizacion(Utils.safeObjectToString(fila[7]));
        informe.setNumeroEntradas(Utils.safeObjectBigDecimalToInt(dbHelper.castBigDecimal(fila[4])));
        informe.setTotal(dbHelper.castBigDecimal(fila[5]));

        return informe;
    }

    private InformeModelReport objectToInformeIva(Object[] fila) {
        InformeModelReport informe = new InformeModelReport();
        informe.setEvento(Utils.safeObjectToString(fila[0]));
        informe.setSesion(DateUtils.dateToSpanishStringWithHour(Utils.objectToDate(fila[1])).toString());
        String tipoEntrada = Utils.safeObjectToString(fila[7]);
        informe.setTipoEntrada(tipoEntrada);
        informe.setNumeroEntradas(Utils.safeObjectBigDecimalToInt(dbHelper.castBigDecimal(fila[3])));
        informe.setTotal(dbHelper.castBigDecimal(fila[4]));
        informe.setIva(dbHelper.castBigDecimal(fila[6]));

        return informe;
    }

    private InformeAbonoReport objectToInformeAbonoIva(Object[] fila) {
        InformeAbonoReport informeAbonoReport = new InformeAbonoReport();
        informeAbonoReport.setNombre(Utils.safeObjectToString(fila[0]));
        informeAbonoReport.setAbonados(Utils.safeObjectBigDecimalToInt(dbHelper.castBigDecimal(fila[1])));
        informeAbonoReport.setTotal(dbHelper.castBigDecimal(fila[2]));

        return informeAbonoReport;
    }

    private InformeModelReport objectToInformeTpv(Object[] fila) {
        InformeModelReport informe = new InformeModelReport();
        informe.setEvento(Utils.safeObjectToString(fila[0]));
        informe.setSesion(DateUtils.dateToSpanishStringWithHour(Utils.objectToDate(fila[1])).toString());
        String tipoEntrada = Utils.safeObjectToString(fila[8]);
        informe.setTipoEntrada(tipoEntrada);
        informe.setNumeroEntradas(Utils.safeObjectBigDecimalToInt(dbHelper.castBigDecimal(fila[3])));
        informe.setTotal(dbHelper.castBigDecimal(fila[4]));
        informe.setTotalSinComision(dbHelper.castBigDecimal(fila[9]));
        informe.setIva(dbHelper.castBigDecimal(fila[6]));
        informe.setFechaCompra(DateUtils.dateToSpanishString(Utils.objectToDate(fila[7])));
        informe.setPresentadas(Utils.safeObjectBigDecimalToInt(dbHelper.castBigDecimal(fila[10])));

        return informe;
    }

    private InformeModelReport objectToEvento(Object[] fila) {
        InformeModelReport informe = new InformeModelReport();
        informe.setEvento(Utils.safeObjectToString(fila[0]));
        informe.setSesion(DateUtils.dateToSpanishStringWithHour(Utils.objectToDate(fila[1])).toString());
        String tipoEntrada = Utils.safeObjectToString(fila[9]);
        informe.setTipoEntrada(tipoEntrada);
        informe.setNumeroEntradas(Utils.safeObjectBigDecimalToInt(dbHelper.castBigDecimal(fila[3])));
        informe.setTotal(dbHelper.castBigDecimal(fila[4]));
        informe.setIva(dbHelper.castBigDecimal(fila[5]));
        informe.setEventoId(Utils.safeObjectBigDecimalToLong(dbHelper.castBigDecimal(fila[7])));
        informe.setSesionId(Utils.safeObjectBigDecimalToLong(dbHelper.castBigDecimal(fila[8])));

        return informe;
    }

    private InformeModelReport objectToInformeEvento(Object[] fila) {
        InformeModelReport informe = new InformeModelReport();
        informe.setEvento(Utils.safeObjectToString(fila[1]));
        String tipoEntrada = Utils.safeObjectToString(fila[6]);
        informe.setTipoEntrada(tipoEntrada);
        informe.setNumeroEntradas(Utils.safeObjectBigDecimalToInt(dbHelper.castBigDecimal(fila[3])));
        informe.setTotal(dbHelper.castBigDecimal(fila[4]));
        informe.setTotalSinComision(dbHelper.castBigDecimal(fila[4]));

        int taquilla = Utils.safeObjectBigDecimalToInt(dbHelper.castBigDecimal(fila[5]));
        informe.setTipoCompra((taquilla == 0) ? "ONLINE" : "TAQUILLA");

        if (fila.length >= 9) {
            informe.setSesion(Utils.safeObjectToString(fila[7]));
            informe.setFechaCompra(Utils.safeObjectToString(fila[8]));
            informe.setNumeroEntradasAnuladas(Utils.safeObjectBigDecimalToInt(dbHelper.castBigDecimal(fila[9])));
            informe.setTipoPago(TipoPago.valueOf(Utils.safeObjectToString(fila[10]).toUpperCase()));
        }

        return informe;
    }
}
