package es.uji.apps.par.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.FormDataMultiPart;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.ws.rs.core.MediaType;

import es.uji.apps.par.exceptions.GuardarImagenException;

@Service
public class ImageService {
    private static final Logger log = LoggerFactory.getLogger(ImageService.class);

    @Value("${uji.par.fitxersServiceURL:}")
    private String fitxersServiceURL;

    @Value("${uji.par.authToken:}")
    private String authToken;

    public boolean hasADEConfigured() {
        return !fitxersServiceURL.isEmpty() && !authToken.isEmpty();
    }

    public boolean isImageValidFormat(byte[] imagen) {
        boolean isImage = true;
        try {
            BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imagen));
            if (bufferedImage == null) {
                isImage = false;
            }
        } catch (Exception e) {
            isImage = false;
        }
        return isImage;
    }

    @Cacheable(value = "adeCache", sync = true)
    public byte[] getImageFromAdeWS(String uuid) {
        try {
            ClientResponse response = getWebResource(fitxersServiceURL + "/storage/" + uuid).get(ClientResponse.class);
            InputStream is = response.getEntityInputStream();
            return IOUtils.toByteArray(is);
        } catch (Exception e) {
            log.error("No se puede recuperar la imagen con uuid " + uuid);
        }
        return null;
    }

    public String insertImageToAdeWS(
        byte[] imagen,
        String imagenSrc,
        String imagenContentType
    ) {
        try {
            FormDataMultiPart responseMultipart = getFormDataMultiPart(imagenSrc, imagenContentType, imagen);
            ClientResponse response = getWebResource(fitxersServiceURL + "/storage").type("multipart/form-data")
                .post(ClientResponse.class, responseMultipart);
            JsonNode responseMessage = response.getEntity(JsonNode.class);
            return responseMessage.path("data").path("reference").asText();
        } catch (Exception e) {
            log.error("No se ha podido guardar la imagen " + imagenSrc);
            throw new GuardarImagenException(imagenSrc);
        }
    }

    @CacheEvict("adeCache")
    public void removeImageFromAdeWS(String uuid) {
        if (uuid != null) {
            try {
                ClientResponse response =
                    getWebResource(fitxersServiceURL + "/storage/" + uuid).delete(ClientResponse.class);
                response.getEntityInputStream();
            } catch (Exception e) {
                log.error("No se ha podido eliminar la imagen con uuid " + uuid);
            }
        }
    }

    private WebResource.Builder getWebResource(String url) {
        Client client = Client.create();
        return client.resource(url).header("X-UJI-AuthToken", authToken);
    }

    private FormDataMultiPart getFormDataMultiPart(
        String name,
        String mimeType,
        byte[] bytes
    ) {
        FormDataMultiPart responseMultipart = new FormDataMultiPart();
        responseMultipart.field("name", name);
        responseMultipart.field("mimetype", mimeType);
        responseMultipart.field("contents", bytes, MediaType.valueOf(mimeType));
        return responseMultipart;
    }

    public byte[] getImageWithoutAlpha(byte[] dataBinary) throws IOException {
        if (dataBinary != null) {
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(dataBinary));
            if (img != null && img.getColorModel().hasAlpha()) {
                BufferedImage copy = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D g2d = copy.createGraphics();
                g2d.setColor(Color.WHITE);
                g2d.fillRect(0, 0, copy.getWidth(), copy.getHeight());
                g2d.drawImage(img, 0, 0, null);
                g2d.dispose();

                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ImageIO.write(copy, "jpeg", os);
                return os.toByteArray();
            }
            return dataBinary;
        }
        return null;
    }
}
