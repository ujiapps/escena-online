package es.uji.apps.par.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="PAR_TPVS_SALAS")
public class TpvsSalasDTO
{
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name="PAR_TPV_SALA_ID_GENERATOR", sequenceName="HIBERNATE_SEQUENCE")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAR_TPV_SALA_ID_GENERATOR")
    private long id;

    @ManyToOne
    @JoinColumn(name="TPV_ID")
    private TpvsDTO tpv;

    @ManyToOne
    @JoinColumn(name="SALA_ID")
    private SalaDTO sala;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public TpvsDTO getTpv()
    {
        return tpv;
    }

    public void setTpv(TpvsDTO tpv)
    {
        this.tpv = tpv;
    }

    public SalaDTO getSala() {
        return sala;
    }

    public void setSala(SalaDTO sala) {
        this.sala = sala;
    }
}
