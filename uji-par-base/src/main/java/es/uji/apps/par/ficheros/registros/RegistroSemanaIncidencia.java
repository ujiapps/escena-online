package es.uji.apps.par.ficheros.registros;

import java.util.Locale;

import es.uji.apps.par.exceptions.RegistroSerializaException;
import es.uji.apps.par.model.Sala;

public class RegistroSemanaIncidencia
{
    private String codigo;
    private String semana;
    private String observaciones;

    public RegistroSemanaIncidencia(
        String codigo,
        String semana,
        String observaciones
    ) {
        this.codigo = codigo;
        this.semana = semana;
        this.observaciones = observaciones;
    }

    public String serializa() throws RegistroSerializaException
    {
        Sala.checkValidity(codigo);
        String result = String.format(Locale.ENGLISH, "1%-12s%-7s%-20s", codigo, semana, observaciones);
        return result;
    }
}
