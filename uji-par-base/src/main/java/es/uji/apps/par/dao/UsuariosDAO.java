package es.uji.apps.par.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.QCineDTO;
import es.uji.apps.par.db.QReportDTO;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSalasUsuarioDTO;
import es.uji.apps.par.db.QUsuarioDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SalasUsuarioDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.exceptions.DominioNoExisteException;
import es.uji.apps.par.exceptions.UsuarioNoExisteException;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Usuario;

@Repository
public class UsuariosDAO extends BaseDAO {
    private QUsuarioDTO qUserDTO = QUsuarioDTO.usuarioDTO;
    private QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
    private QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
    private QCineDTO qCineDTO = QCineDTO.cineDTO;

    @Transactional
    public List<Usuario> getUsers(
        String sortParameter,
        int start,
        int limit
    ) {
        List<Usuario> users = new ArrayList<Usuario>();
        List<UsuarioDTO> usuariosDTO =
            getQueryUsuarios().orderBy(getSort(qUserDTO, sortParameter)).limit(limit).offset(start).list(qUserDTO);

        for (UsuarioDTO userDB : usuariosDTO) {
            users.add(new Usuario(userDB));
        }

        return users;
    }

    @Transactional
    public Usuario getUserById(String userUID) {
        JPAQuery query = new JPAQuery(entityManager);

        UsuarioDTO usuarioDTO = query.from(qUserDTO).where(qUserDTO.usuario.eq(userUID)).uniqueResult(qUserDTO);
        if (usuarioDTO == null || usuarioDTO.getId() == 0) {
            return null;
        }
        return new Usuario(usuarioDTO);
    }

    private JPAQuery getQueryUsuarios() {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qUserDTO);
    }

    @Transactional
    public long removeUser(long id) {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qUserDTO);
        return delete.where(qUserDTO.id.eq(id)).execute();
    }

    @Transactional
    public Usuario addUser(Usuario user) {
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setNombre(user.getNombre());
        usuarioDTO.setMail(user.getMail());
        usuarioDTO.setUsuario(user.getUsuario());
        usuarioDTO.setUrl(user.getUrl());
        usuarioDTO.setPassword(user.getPassword());
        usuarioDTO.setRole(user.getRole());
        usuarioDTO.setTokenValidacion(user.getTokenValidacion());
        usuarioDTO.setExpiracionToken(new Timestamp(System.currentTimeMillis() + (24 * 60 * 60 * 1000)));

        entityManager.persist(usuarioDTO);

        user.setId(usuarioDTO.getId());
        return user;
    }

    @Transactional
    public Usuario updateUser(Usuario user) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qUserDTO);
        update.set(qUserDTO.nombre, user.getNombre()).set(qUserDTO.mail, user.getMail())
            .set(qUserDTO.usuario, user.getUsuario()).where(qUserDTO.id.eq(user.getId())).execute();

        return user;
    }

    @Transactional
    public String updatePassword(
        String userUID,
        String password
    ) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qUserDTO);
        update.set(qUserDTO.password, password).where(qUserDTO.usuario.eq(userUID)).execute();

        return userUID;
    }

    @Transactional
    public String updateUrl(
        String userUID,
        String url
    ) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qUserDTO);
        update.set(qUserDTO.url, url).where(qUserDTO.usuario.eq(userUID)).execute();

        return userUID;
    }

    @Transactional
    public boolean userExists(Usuario user) {
        JPAQuery query = new JPAQuery(entityManager);
        List<UsuarioDTO> usuarios = query.from(qUserDTO).where(qUserDTO.usuario.eq(user.getUsuario())).list(qUserDTO);

        if (usuarios.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public int getTotalUsuarios() {
        return (int) getQueryUsuarios().count();
    }

    @Transactional
    public void addSalaUsuario(
        Sala sala,
        Usuario usuario
    ) {
        SalasUsuarioDTO cinesUsuarioDTO = new SalasUsuarioDTO();
        cinesUsuarioDTO.setParSala(new SalaDTO(sala.getId()));
        cinesUsuarioDTO.setParUsuario(new UsuarioDTO(usuario.getId()));
        entityManager.persist(cinesUsuarioDTO);
    }

    @Transactional
    public String getReportClassNameForUserAndType(
        String login,
        String tipoInformePdf
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QReportDTO qReportDTO = QReportDTO.reportDTO;
        QCineDTO qCineDTO = QCineDTO.cineDTO;

        return query.from(qUsuarioDTO).join(qUsuarioDTO.parSalasUsuario, qSalasUsuarioDTO)
            .join(qSalasUsuarioDTO.parSala, qSalaDTO).join(qSalaDTO.parCine, qCineDTO)
            .join(qCineDTO.parReports, qReportDTO).where(qUsuarioDTO.usuario.toUpperCase().eq(login.toUpperCase())
                .and(qReportDTO.tipo.toUpperCase().eq(tipoInformePdf.toUpperCase()))).uniqueResult(qReportDTO.clase);
    }

    public Usuario getUserByServerName(String serverName) {
        JPAQuery query = new JPAQuery(entityManager);

        List<UsuarioDTO> users = query.from(qUserDTO).where(qUserDTO.url.eq(serverName)).list(qUserDTO);

        try {
            return new Usuario(users.get(0));
        } catch (IndexOutOfBoundsException e) {
            throw new DominioNoExisteException(serverName);
        }
    }

    public CineDTO getCineDTOByServerName(String serverName) {
        JPAQuery query = new JPAQuery(entityManager);

        try {
            return query.from(qUserDTO).join(qUserDTO.parSalasUsuario, qSalasUsuarioDTO)
                .join(qSalasUsuarioDTO.parSala, qSalaDTO).join(qSalaDTO.parCine, qCineDTO)
                .where(qUserDTO.url.eq(serverName)).list(qCineDTO).get(0);
        } catch (IndexOutOfBoundsException e) {
            throw new DominioNoExisteException(serverName);
        }
    }

    public Cine getUserCineByUserUID(
        String userUID,
        boolean crearConImagen
    ) {
        JPAQuery query = new JPAQuery(entityManager);

        List<CineDTO> cines = query.from(qUserDTO).join(qUserDTO.parSalasUsuario, qSalasUsuarioDTO)
            .join(qSalasUsuarioDTO.parSala, qSalaDTO).join(qSalaDTO.parCine, qCineDTO)
            .where(qUserDTO.usuario.eq(userUID)).list(qCineDTO);

        try {
            return Cine.cineDTOToCine(cines.get(0), crearConImagen);
        } catch (IndexOutOfBoundsException e) {
            throw new UsuarioNoExisteException(userUID);
        }
    }

    public Cine getUserCineByUserUID(String userUID) {
        return getUserCineByUserUID(userUID, false);
    }

    public List<UsuarioDTO> getUsersByCine(long idCine) {
        JPAQuery query = new JPAQuery(entityManager);
        QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QCineDTO qCineDTO = QCineDTO.cineDTO;

        return query.from(qUsuarioDTO).join(qUsuarioDTO.parSalasUsuario, qSalasUsuarioDTO)
            .join(qSalasUsuarioDTO.parSala, qSalaDTO).join(qSalaDTO.parCine, qCineDTO).where(qCineDTO.id.eq(idCine))
            .distinct().list(qUsuarioDTO);
    }

    public UsuarioDTO getUsuarioByTokenValidacion(String tokenValidacion) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qUserDTO).where(qUserDTO.tokenValidacion.eq(tokenValidacion)).singleResult(qUserDTO);
    }

    @Transactional
    public void validarToken(
        String userUID,
        String tokenValidacion
    ) {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qUserDTO);
        updateClause.set(qUserDTO.emailVerificado, true)
            .where(qUserDTO.usuario.eq(userUID).and(qUserDTO.tokenValidacion.eq(tokenValidacion))).execute();
    }

    @Transactional
    public void actualizarExpiracionToken(
        String usuario,
        Timestamp timestamp
    ) {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qUserDTO);
        updateClause.set(qUserDTO.expiracionToken, timestamp).where(qUserDTO.usuario.eq(usuario)).execute();
    }
}
