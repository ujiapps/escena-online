package es.uji.apps.par.model;

import com.sun.jersey.core.util.Base64;

import java.util.Locale;

import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.utils.DateUtils;

public class ButacaPassbook {

    private long id;
    private String enlace;
    private String enlaceCancelacion;
    private String zona;
    private String fila;
    private String numero;
    private String uuidCompra;
    private String urlPublic;
    private String fecha;
    private String horaApertura;

    public ButacaPassbook(
        ButacaDTO butaca,
        Locale locale,
        String uuidCompra,
        String urlPublic
    ) {
        this.id = butaca.getId();
        this.fila = butaca.getFila();
        this.numero = butaca.getNumero();
        this.urlPublic = urlPublic;
        this.uuidCompra = uuidCompra;
        this.fecha = DateUtils.dateToSpanishStringWithHour(butaca.getParSesion().getFechaCelebracion());
        this.horaApertura = butaca.getParSesion().getHoraApertura();
        this.enlace = String.format("%s/rest/compra/%s/%s/passbook", urlPublic, uuidCompra, this.id);
        if(locale.getLanguage().equals("es")){
            this.zona = butaca.getParLocalizacion().getNombreEs();
        } else {
            this.zona = butaca.getParLocalizacion().getNombreVa();
        }
    }

    public String getEnlace() {
        return enlace;
    }

    public String getZona() {
        return zona;
    }

    public String getFila() {
        return fila;
    }

    public String getNumero() {
        return numero;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHoraApertura() {
        return horaApertura;
    }

    public void setEnlaceCancelacion(String email) {
        this.enlaceCancelacion = String.format("%s/rest/compra/%s/butaca/%s/%s/anular", urlPublic, uuidCompra, id, new String(Base64.encode(email)));
    }

    public String getEnlaceCancelacion() {
        return enlaceCancelacion;
    }
}
