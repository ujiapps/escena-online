package es.uji.apps.par.exceptions;


public class EmailVerificacionException extends GeneralPARException
{
    public EmailVerificacionException()
    {
        super(EMAIL_VERIFICACION_CODE, EMAIL_VERIFICACION);
    }
}
