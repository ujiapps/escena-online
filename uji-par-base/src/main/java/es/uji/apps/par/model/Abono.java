package es.uji.apps.par.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.db.AbonoDTO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.utils.DateUtils;

@XmlRootElement
public class Abono {
    private long id;
    private String nombre;
    private Long maxEventos;
    private String descripcionEs;
    private String descripcionVa;
    private byte[] imagen;
    private String imagenSrc;
    private String imagenUUID;
    private String imagenContentType;
    private Tpv parTpv;
    private Evento parEvento;
    private Localizacion parLocalizacion;
    private BigDecimal precio;
    private BigDecimal precioAnticipado;
    private Boolean canalInternet;
    private Date fechaInicioVentaOnline;
    private Date fechaFinVentaOnline;
    private Date fechaFinVentaAnticipada;
    private String horaInicioVentaOnline;
    private String horaFinVentaOnline;
    private String horaFinVentaAnticipada;

    public Abono() {

    }

    public Abono(
        String nombre,
        Long maxEventos,
        String descripcionEs,
        String descripcionVa,
        byte[] dataBinary,
        String nombreArchivo,
        String mediaType,
        BigDecimal precio,
        BigDecimal precioAnticipado,
        Integer tpvId,
        Integer localizacionId,
        String canalInternet,
        String fechaInicioVentaOnline,
        String fechaFinVentaOnline,
        String fechaFinVentaAnticipada,
        String horaInicioVentaOnline,
        String horaFinVentaOnline,
        String horaFinVentaAnticipada
    ) {
        this.nombre = nombre;
        this.maxEventos = maxEventos;
        this.descripcionEs = descripcionEs;
        this.descripcionVa = descripcionVa;
        this.imagen = dataBinary;
        this.imagenSrc = nombreArchivo;
        this.imagenContentType = mediaType;
        this.precio = precio;
        this.precioAnticipado = precioAnticipado;

        setCanalInternet(canalInternet);
        if (this.canalInternet) {
            this.fechaInicioVentaOnline = DateUtils.spanishStringToDate(fechaInicioVentaOnline);
            this.horaInicioVentaOnline = horaInicioVentaOnline;
            this.fechaFinVentaOnline = DateUtils.spanishStringToDate(fechaFinVentaOnline);
            this.horaFinVentaOnline = horaFinVentaOnline;

            if (fechaFinVentaAnticipada != null) {
                this.fechaFinVentaAnticipada = DateUtils.spanishStringToDate(fechaFinVentaAnticipada);
                this.horaFinVentaAnticipada = horaFinVentaAnticipada;
            }
        }

        if (tpvId != null) {
            this.parTpv = new Tpv();
            this.parTpv.setId(tpvId);
        }

        if (localizacionId != null) {
            this.parLocalizacion = new Localizacion();
            this.parLocalizacion.setId(localizacionId);
        }
    }

    public Abono(long id) {
        this.id = id;
    }

    public Abono(TarifaDTO tarifaDTO) {
        this.id = tarifaDTO.getId();
        this.nombre = tarifaDTO.getNombre();
        this.maxEventos = tarifaDTO.getMaxEventos();
    }

    public Abono(
        AbonoDTO abonoDTO
    ) {
        this.id = abonoDTO.getId();
        this.nombre = abonoDTO.getNombre();
        this.maxEventos = abonoDTO.getMaxEventos();
        this.descripcionEs = abonoDTO.getDescripcionEs();
        this.descripcionVa = abonoDTO.getDescripcionVa();

        this.imagen = abonoDTO.getImagen();
        this.imagenUUID = abonoDTO.getImagenUUID();
        this.imagenContentType = abonoDTO.getImagenContentType();
        this.imagenSrc = abonoDTO.getImagenSrc();

        if (abonoDTO.getParTpv() != null) {
            this.parTpv = Tpv.tpvDTOToTpv(abonoDTO.getParTpv());
        }

        if (abonoDTO.getParLocalizacion() != null) {
            this.parLocalizacion = Localizacion.localizacionDTOtoLocalizacion(abonoDTO.getParLocalizacion());
        }

        if (abonoDTO.getParEvento() != null) {
            this.parEvento = Evento.eventoDTOtoEvento(abonoDTO.getParEvento());
        }

        this.precio = abonoDTO.getPrecio();
        this.precioAnticipado = abonoDTO.getPrecioAnticipado();

        this.canalInternet = abonoDTO.getCanalInternet();
        if (abonoDTO.getFechaInicioVentaOnline() != null) {
            this.fechaInicioVentaOnline = abonoDTO.getFechaInicioVentaOnline();
            this.horaInicioVentaOnline =
                DateUtils.getHourAndMinutesWithLeadingZeros(abonoDTO.getFechaInicioVentaOnline());
        }
        if (abonoDTO.getFechaFinVentaOnline() != null) {
            this.fechaFinVentaOnline = abonoDTO.getFechaFinVentaOnline();
            this.horaFinVentaOnline = DateUtils.getHourAndMinutesWithLeadingZeros(abonoDTO.getFechaFinVentaOnline());
        }
        if (abonoDTO.getFechaFinVentaAnticipada() != null) {
            this.fechaFinVentaAnticipada = abonoDTO.getFechaFinVentaAnticipada();
            this.horaFinVentaAnticipada =
                DateUtils.getHourAndMinutesWithLeadingZeros(abonoDTO.getFechaFinVentaAnticipada());
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getMaxEventos() {
        return maxEventos;
    }

    public void setMaxEventos(Long maxEventos) {
        this.maxEventos = maxEventos;
    }

    public String getDescripcionEs() {
        return descripcionEs;
    }

    public void setDescripcionEs(String descripcionEs) {
        this.descripcionEs = descripcionEs;
    }

    public String getDescripcionVa() {
        return descripcionVa;
    }

    public void setDescripcionVa(String descripcionVa) {
        this.descripcionVa = descripcionVa;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public String getImagenSrc() {
        return imagenSrc;
    }

    public void setImagenSrc(String imagenSrc) {
        this.imagenSrc = imagenSrc;
    }

    public String getImagenUUID() {
        return imagenUUID;
    }

    public void setImagenUUID(String imagenUUID) {
        this.imagenUUID = imagenUUID;
    }

    public String getImagenContentType() {
        return imagenContentType;
    }

    public void setImagenContentType(String imagenContentType) {
        this.imagenContentType = imagenContentType;
    }

    public Tpv getParTpv() {
        return parTpv;
    }

    public void setParTpv(Tpv parTpv) {
        this.parTpv = parTpv;
    }

    public Evento getParEvento() {
        return parEvento;
    }

    public void setParEvento(Evento parEvento) {
        this.parEvento = parEvento;
    }

    public Localizacion getParLocalizacion() {
        return parLocalizacion;
    }

    public void setParLocalizacion(Localizacion parLocalizacion) {
        this.parLocalizacion = parLocalizacion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public BigDecimal getPrecioAnticipado() {
        return precioAnticipado;
    }

    public void setPrecioAnticipado(BigDecimal precioAnticipado) {
        this.precioAnticipado = precioAnticipado;
    }


    public Boolean getCanalInternet() {
        return (canalInternet == null) ? false : canalInternet;
    }

    public void setCanalInternet(String canalInternet) {
        this.canalInternet = (canalInternet != null && (canalInternet.equals("1") || canalInternet.equals("true")));
    }

    public Date getFechaInicioVentaOnline() {
        return fechaInicioVentaOnline;
    }

    public Date getFechaFinVentaOnline() {
        return fechaFinVentaOnline;
    }

    public Date getFechaFinVentaAnticipada() {
        return fechaFinVentaAnticipada;
    }

    public void setFechaInicioVentaOnline(String fechaInicioVentaOnline) {
        this.fechaInicioVentaOnline = DateUtils.spanishStringToDate(fechaInicioVentaOnline);
    }

    public void setFechaInicioVentaOnlineWithDate(Date fechaInicioVentaOnline) {
        this.fechaInicioVentaOnline = fechaInicioVentaOnline;
    }

    public void setFechaFinVentaOnline(String fechaFinVentaOnline) {
        this.fechaFinVentaOnline = DateUtils.spanishStringToDate(fechaFinVentaOnline);
    }

    public void setFechaFinVentaOnlineWithDate(Date fechaFinVentaOnline) {
        this.fechaFinVentaOnline = fechaFinVentaOnline;
    }

    public void setFechaFinVentaAnticipada(String fechaFinVentaAnticipada) {
        this.fechaFinVentaAnticipada = DateUtils.spanishStringToDate(fechaFinVentaAnticipada);
    }

    public void setFechaFinVentaAnticipadaWithDate(Date fechaFinVentaAnticipada) {
        this.fechaFinVentaAnticipada = fechaFinVentaAnticipada;
    }

    public String getHoraInicioVentaOnline() {
        return horaInicioVentaOnline;
    }

    public void setHoraInicioVentaOnline(String horaInicioVentaOnline) {
        this.horaInicioVentaOnline = horaInicioVentaOnline;
    }

    public String getHoraFinVentaOnline() {
        return horaFinVentaOnline;
    }

    public void setHoraFinVentaOnline(String horaFinVentaOnline) {
        this.horaFinVentaOnline = horaFinVentaOnline;
    }

    public String getHoraFinVentaAnticipada() {
        return horaFinVentaAnticipada;
    }

    public void setHoraFinVentaAnticipada(String horaFinVentaAnticipada) {
        this.horaFinVentaAnticipada = horaFinVentaAnticipada;
    }

    public static TarifaDTO toTarifaDTO(
        Abono abono,
        Cine cine
    ) {
        TarifaDTO tarifaDTO = new TarifaDTO();
        tarifaDTO.setId(abono.getId());
        tarifaDTO.setNombre(abono.getNombre());
        tarifaDTO.setIsPublica(true);
        tarifaDTO.setIsAbono(true);
        tarifaDTO.setDefecto(false);
        tarifaDTO.setMaxEventos(abono.getMaxEventos());
        tarifaDTO.setParCine(new CineDTO(cine.getId()));

        return tarifaDTO;
    }

    public static Evento toEvento(
        Abono abono,
        TipoEvento tipoEvento,
        Cine cine
    ) {
        return new Evento(abono, tipoEvento, cine);
    }

    public static PreciosEditablesSesion toPrecioEditableSesion(
        Abono abono,
        TarifaDTO tarifaPublica
    ) {
        PreciosEditablesSesion preciosEditablesSesion = new PreciosEditablesSesion();
        preciosEditablesSesion.setLocalizacion(abono.getParLocalizacion());
        preciosEditablesSesion.setPrecio(abono.getPrecio());
        preciosEditablesSesion.setPrecioAnticipado(abono.getPrecioAnticipado());
        preciosEditablesSesion.setTarifa(Tarifa.tarifaDTOToTarifa(tarifaPublica));

        return preciosEditablesSesion;
    }

    public static Sesion toSesion(
        Abono abono,
        Sala sala,
        PreciosEditablesSesion preciosSesion
    ) {
        Sesion sesion = new Sesion();
        sesion.setCanalInternet(abono.getCanalInternet().toString());
        sesion.setFechaCelebracionWithDate(abono.getFechaFinVentaOnline());
        sesion.setHoraCelebracion(abono.getHoraFinVentaOnline());
        sesion.setFechaFinVentaOnlineWithDate(abono.getFechaFinVentaOnline());
        sesion.setHoraFinVentaOnline(abono.getHoraFinVentaOnline());
        sesion.setFechaFinVentaAnticipadaWithDate(abono.getFechaFinVentaAnticipada());
        sesion.setHoraFinVentaAnticipada(abono.getHoraFinVentaAnticipada());
        sesion.setFechaInicioVentaOnlineWithDate(abono.getFechaInicioVentaOnline());
        sesion.setHoraInicioVentaOnline(abono.getHoraInicioVentaOnline());
        sesion.setSala(sala);
        sesion.setPreciosSesion(Arrays.asList(preciosSesion));

        return sesion;
    }
}