package es.uji.apps.par.model;

import java.math.BigDecimal;

public class GastosGestion {
    BigDecimal gastosGestion;
    BigDecimal gastosGestionSinIVA;
    BigDecimal IVAgastosGestion;

    public BigDecimal getGastosGestion() {
        return gastosGestion;
    }

    public void setGastosGestion(BigDecimal gastosGestion) {
        this.gastosGestion = gastosGestion;
    }

    public BigDecimal getGastosGestionSinIVA() {
        return gastosGestionSinIVA;
    }

    public void setGastosGestionSinIVA(BigDecimal gastosGestionSinIVA) {
        this.gastosGestionSinIVA = gastosGestionSinIVA;
    }

    public BigDecimal getIVAgastosGestion() {
        return IVAgastosGestion;
    }

    public void setIVAgastosGestion(BigDecimal IVAgastosGestion) {
        this.IVAgastosGestion = IVAgastosGestion;
    }
}
