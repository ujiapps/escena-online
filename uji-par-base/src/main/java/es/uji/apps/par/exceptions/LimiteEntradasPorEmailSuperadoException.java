package es.uji.apps.par.exceptions;

@SuppressWarnings("serial")
public class LimiteEntradasPorEmailSuperadoException extends GeneralPARException
{
    int limite;

    public LimiteEntradasPorEmailSuperadoException(int limite)
    {
        super(LIMITE_COMPRA_EVENTO_EMAIL_CODE, "Límite: " + limite);
        this.limite = limite;
    }

	public LimiteEntradasPorEmailSuperadoException()
	{
		super(LIMITE_COMPRA_EVENTO_EMAIL_CODE);
	}

    public int getLimite() {
        return limite;
    }
}
