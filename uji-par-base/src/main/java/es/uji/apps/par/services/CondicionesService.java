package es.uji.apps.par.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.MissingResourceException;

import es.uji.apps.par.i18n.ResourceProperties;

public class CondicionesService {
    private static final Logger log = LoggerFactory.getLogger(CondicionesService.class);

    private static String getCondicionesSuffix(
        String codigoCine,
        Locale locale
    ) {
        String suffix = codigoCine + ".";
        try {
            if (ResourceProperties.getProperty(locale, String.format("entrada.%scondiciones", suffix)) == null) {
                suffix = "";
            }
        } catch (MissingResourceException e) {
            suffix = "";
        }
        return suffix;
    }

    public static String getCondiciones(
        String codigoCine,
        Locale locale
    ) {
        String suffix = getCondicionesSuffix(codigoCine, locale);

        String puntos = "";
        String condicion;
        int i = 1;
        do {
            try {
                condicion = ResourceProperties.getProperty(locale, String.format("entrada.%scondicion%d", suffix, i));
                puntos += condicion;
                i++;
            } catch (MissingResourceException e) {
                condicion = null;
            }

        } while (condicion != null);

        return puntos;
    }

    public static String getCondicionesTitulo(
        String codigoCine,
        Locale locale
    ) {
        String suffix = getCondicionesSuffix(codigoCine, locale);

        try {
            return ResourceProperties.getProperty(locale, String.format("entrada.%scondiciones", suffix));
        } catch (MissingResourceException e) {
            return ResourceProperties.getProperty(locale, "entrada.condiciones");
        }
    }
}