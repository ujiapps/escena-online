package es.uji.apps.par.exceptions;

public class CompraConCosteNoAnulableDesdeEnlace extends Throwable {
    public CompraConCosteNoAnulableDesdeEnlace(String uuidCompra) {
        super(String.format("La compra %s que está intentando anular tiene coste", uuidCompra));
    }
}
