package es.uji.apps.par.exceptions;

public class EntradaConCosteNoAnulableDesdeEnlace extends Throwable {
    public EntradaConCosteNoAnulableDesdeEnlace(String uuidCompra, long butacaId) {
        super(String.format("La entrada %s que está intentando anular de la compra %s tiene coste", butacaId, uuidCompra));
    }
}
