package es.uji.apps.par.exceptions;

public class TipoEventoConEventosException extends GeneralPARException {

    public TipoEventoConEventosException() {
        super(TIPO_EVENTO_CON_EVENTOS_CODE, TIPO_EVENTO_CON_EVENTOS);
    }
}
