package es.uji.apps.par.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.persistence.PersistenceException;

import es.uji.apps.par.db.PlantaSalaDTO;
import es.uji.apps.par.db.QLocalizacionDTO;
import es.uji.apps.par.db.QPlantaSalaDTO;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSalasUsuarioDTO;
import es.uji.apps.par.db.QSesionDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.exceptions.MinimoUnaSalaException;
import es.uji.apps.par.ext.ExtGridFilter;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.model.PlantaSala;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Sesion;

@Repository
public class SalasDAO extends BaseDAO {
    private QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
    private QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
    private QLocalizacionDTO qLocalizacionDTO = QLocalizacionDTO.localizacionDTO;

    @Transactional
    public SalaDTO getSalaByLocalizacionId(
        long localizacionId,
        String userUID
    ) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qSalaDTO).join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .leftJoin(qSalaDTO.parLocalizaciones, qLocalizacionDTO)
            .where(qLocalizacionDTO.id.eq(localizacionId).and(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID)))
            .uniqueResult(qSalaDTO);
    }

    @Transactional
    public List<Sala> getSalas(String userUID) {
        JPAQuery query = new JPAQuery(entityManager);

        List<SalaDTO> list =
            query.from(qSalaDTO).join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).leftJoin(qSalaDTO.parCine).fetch()
                .leftJoin(qSalaDTO.parLocalizaciones, qLocalizacionDTO).fetch()
                .where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID)).list(qSalaDTO);

        return Sala.salasDTOtoSalas(list);
    }

    @Transactional
    public List<Sala> getSalasWithLocalizacion(String userUID) {
        JPAQuery query = new JPAQuery(entityManager);

        List<SalaDTO> list =
            query.from(qSalaDTO).join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).leftJoin(qSalaDTO.parCine)
                .join(qSalaDTO.parLocalizaciones, qLocalizacionDTO)
                .where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID)).list(qSalaDTO);

        return Sala.salasDTOtoSalas(list);
    }

    @Transactional
    public List<Sala> getSalas(
        String userUID,
        ExtGridFilterList filter
    ) {
        BooleanExpression whereClause = qSalasUsuarioDTO.parUsuario.usuario.eq(userUID);
        if (filter != null) {
            final ExtGridFilter noNumeradasFilter = filter.findFiltroByProperty("asientosNumerados");
            if (noNumeradasFilter != null) {
                whereClause = whereClause.and(qSalaDTO.asientosNumerados.eq((boolean) noNumeradasFilter.getValue()));
            }
        }

        JPAQuery query = new JPAQuery(entityManager);

        List<SalaDTO> list =
            query.from(qSalaDTO).join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).leftJoin(qSalaDTO.parCine).fetch()
                .leftJoin(qSalaDTO.parLocalizaciones, qLocalizacionDTO).fetch().where(whereClause).list(qSalaDTO);

        return Sala.salasDTOtoSalas(list);
    }

    @Transactional
    public Sala addSala(Sala sala) {
        SalaDTO salaDTO = Sala.salaToSalaDTO(sala);

        entityManager.persist(salaDTO);

        sala.setId(salaDTO.getId());
        return sala;
    }

    @Transactional
    public List<PlantaSala> getPlantas(long idSala) {
        QPlantaSalaDTO qPlantaSalaDTO = QPlantaSalaDTO.plantaSalaDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<PlantaSalaDTO> list =
            query.from(qPlantaSalaDTO).where(qPlantaSalaDTO.parSala.id.eq(idSala)).list(qPlantaSalaDTO);

        return PlantaSala.plantasSalasDTOToPlantasSalas(list);
    }

    @Transactional
    public PlantaSala addPlanta(PlantaSala plantaSala) {
        PlantaSalaDTO plantaDTO = PlantaSala.plantaSalaToPlantaSalaDTO(plantaSala);

        entityManager.persist(plantaDTO);

        plantaSala.setId(plantaDTO.getId());

        return plantaSala;
    }

    @Transactional
    public List<Sala> getSalas(List<Sesion> sesiones) {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<SalaDTO> salasDTO = query.from(qSalaDTO).join(qSalaDTO.parSesiones, qSesionDTO).
            where(qSesionDTO.id.in(Sesion.getIdsSesiones(sesiones))).orderBy(qSalaDTO.id.asc()).distinct()
            .list(qSalaDTO);

        return Sala.salasDTOtoSalas(salasDTO);
    }

    public SalaDTO getSala(Long idSala) {
        return entityManager.find(SalaDTO.class, idSala);
    }

    @Transactional
    public Sala editSala(Sala sala) {
        SalaDTO salaDTO = Sala.salaToSalaDTO(sala);
        return Sala.salaDTOtoSala(entityManager.merge(salaDTO));
    }

    @Transactional
    public void deleteSala(Long idSala) throws MinimoUnaSalaException, PersistenceException {
        SalaDTO sala = getSala(idSala);
        List<Sala> salas = getSalas(sala.getParSalasUsuario().get(0).getParUsuario().getUsuario());
        if (salas.size() > 1) {

            JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qLocalizacionDTO);
            deleteClause.where(qLocalizacionDTO.sala.id.eq(idSala)).execute();

            deleteClause = new JPADeleteClause(entityManager, qSalasUsuarioDTO);
            deleteClause.where(qSalasUsuarioDTO.parSala.id.eq(idSala)).execute();

            deleteClause = new JPADeleteClause(entityManager, qSalaDTO);
            deleteClause.where(qSalaDTO.id.eq(idSala)).execute();
        } else {
            throw new MinimoUnaSalaException();
        }
    }

    @Transactional
    public Tuple getClaseEntradasFromCine(long cineId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qSalaDTO).
            where(qSalaDTO.parCine.id.eq(cineId)).orderBy(qSalaDTO.id.desc())
            .singleResult(qSalaDTO.claseEntradaTaquilla, qSalaDTO.claseEntradaOnline);
    }
}
