package es.uji.apps.par.model;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.exceptions.GeneralPARException;
import es.uji.apps.par.exceptions.RegistroSerializaException;
import es.uji.apps.par.ficheros.registros.RegistroPelicula;

@XmlRootElement
public class Evento implements IImagenesEvento {
    private static final Logger logger = LoggerFactory.getLogger(Evento.class);

    private long id;
    private String tituloEs;
    private String tituloVa;
    private String descripcionEs;
    private String descripcionVa;
    private String companyiaEs;
    private String companyiaVa;
    private String interpretesEs;
    private String interpretesVa;
    private int duracion;
    private String duracionEs;
    private String duracionVa;
    private byte[] imagen;
    private String imagenSrc;
    private String imagenUUID;
    private String imagenContentType;
    private byte[] imagenPubli;
    private String imagenPubliSrc;
    private String imagenPubliUUID;
    private String imagenPubliContentType;
    private String premiosEs;
    private String premiosVa;
    private String caracteristicasEs;
    private String caracteristicasVa;
    private String comentariosEs;
    private String comentariosVa;
    private TipoEvento parTiposEvento;
    private String promotor;
    private String nifPromotor;
    private String emailPromotor;
    private String telefonoPromotor;
    private String direccionPromotor;
    private Tpv parTpv;
    private long tipoEvento;
    private Boolean asientosNumerados;
    private Boolean entradasNominales;
    private BigDecimal porcentajeIVA;
    private BigDecimal ivaSGAE;
    private BigDecimal retencionSGAE;
    private Date fechaPrimeraSesion;
    private List<Sesion> sesiones;
    private String rssId;
    private Boolean multipleTpv;
    private String formato;
    private String expediente;
    private String codigoDistribuidora;
    private String nombreDistribuidora;
    private String nacionalidad;
    private String vo;
    private String metraje;
    private String subtitulos;
    private String multisesion; //representa al checkbox
    private List<EventoMultisesion> eventosMultisesion;
    private Cine cine;
    private Boolean publico;
    private Boolean reservasPublicas;
    private Boolean abono;
    private Boolean entradasLimitadas;
    private Integer entradasPorEmail;

    public Evento() {
        sesiones = new ArrayList<Sesion>();
    }

    public Evento(Integer idEvento) {
        this.id = idEvento;
    }

    public static Evento eventoDTOtoEvento(EventoDTO eventoDTO) {
        Evento evento = new Evento();

        evento.setId(eventoDTO.getId());
        evento.setTituloEs(eventoDTO.getTituloEs());
        evento.setTituloVa(eventoDTO.getTituloVa());

        evento.setDescripcionEs(eventoDTO.getDescripcionEs());
        evento.setDescripcionVa(eventoDTO.getDescripcionVa());

        evento.setCompanyiaEs(eventoDTO.getCompanyiaEs());
        evento.setCompanyiaVa(eventoDTO.getCompanyiaVa());

        evento.setInterpretesEs(eventoDTO.getInterpretesEs());
        evento.setInterpretesVa(eventoDTO.getInterpretesVa());

        evento.setDuracionEs(eventoDTO.getDuracionEs());
        evento.setDuracionVa(eventoDTO.getDuracionVa());
        evento.setDuracion(eventoDTO.getDuracion());

        evento.setImagen(eventoDTO.getImagen());
        evento.setImagenContentType(eventoDTO.getImagenContentType());
        evento.setImagenSrc(eventoDTO.getImagenSrc());
        evento.setImagenUUID(eventoDTO.getImagenUUID());

        evento.setImagenPubli(eventoDTO.getImagenPubli());
        evento.setImagenPubliContentType(eventoDTO.getImagenPubliContentType());
        evento.setImagenPubliSrc(eventoDTO.getImagenPubliSrc());
        evento.setImagenPubliUUID(eventoDTO.getImagenPubliUUID());

        evento.setPremiosEs(eventoDTO.getPremiosEs());
        evento.setPremiosVa(eventoDTO.getPremiosVa());

        evento.setCaracteristicasEs(eventoDTO.getCaracteristicasEs());
        evento.setCaracteristicasVa(eventoDTO.getCaracteristicasVa());

        evento.setComentariosEs(eventoDTO.getComentariosEs());
        evento.setComentariosVa(eventoDTO.getComentariosVa());

        if (eventoDTO.getParTiposEvento() != null) {
            evento.setParTipoEvento(TipoEvento.tipoEventoDTOToTipoEvento(eventoDTO.getParTiposEvento()));
        }

        if (eventoDTO.getParTpv() != null) {
            evento.setParTpv(Tpv.tpvDTOToTpv(eventoDTO.getParTpv()));
        }

        if (eventoDTO.getParCine() != null) {
            evento.setCine(Cine.cineDTOToCine(eventoDTO.getParCine(), false));
        }

        evento.setAsientosNumerados(eventoDTO.getAsientosNumerados());
        evento.setEntradasNominales(eventoDTO.getEntradasNominales());
        evento.setIvaSGAE(eventoDTO.getIvaSgae());
        evento.setRetencionSGAE(eventoDTO.getRetencionSgae());
        evento.setPorcentajeIVA(eventoDTO.getPorcentajeIva());

        evento.setRssId(eventoDTO.getRssId());

        evento.setFormato(eventoDTO.getFormato());
        evento.setExpediente(eventoDTO.getExpediente());
        evento.setCodigoDistribuidora(eventoDTO.getCodigoDistribuidora());
        evento.setNombreDistribuidora(eventoDTO.getNombreDistribuidora());
        evento.setNacionalidad(eventoDTO.getNacionalidad());
        evento.setVo(eventoDTO.getVo());
        evento.setMetraje(eventoDTO.getMetraje());
        evento.setSubtitulos(eventoDTO.getSubtitulos());
        evento.setPromotor(eventoDTO.getPromotor());
        evento.setNifPromotor(eventoDTO.getNifPromotor());
        evento.setTelefonoPromotor(eventoDTO.getTelefonoPromotor());
        evento.setEmailPromotor(eventoDTO.getEmailPromotor());
        evento.setDireccionPromotor(eventoDTO.getDireccionPromotor());
        evento.setPublico(eventoDTO.getPublico());
        evento.setReservasPublicas(eventoDTO.getReservasPublicas());
        evento.setAbono(eventoDTO.getIsabono());
        evento.setEntradasLimitadas(eventoDTO.getEntradasLimitadas());
        evento.setEntradasPorEmail(eventoDTO.getEntradasPorEmail());

        return evento;
    }

    public static EventoDTO eventoToEventoDTO(Evento evento) {
        EventoDTO eventoDTO = new EventoDTO();

        eventoDTO.setId(evento.getId());
        eventoDTO.setTituloEs(evento.getTituloEs());
        eventoDTO.setTituloVa(evento.getTituloVa());

        eventoDTO.setDescripcionEs(evento.getDescripcionEs());
        eventoDTO.setDescripcionVa(evento.getDescripcionVa());

        eventoDTO.setCompanyiaEs(evento.getCompanyiaEs());
        eventoDTO.setCompanyiaVa(evento.getCompanyiaVa());

        eventoDTO.setInterpretesEs(evento.getInterpretesEs());
        eventoDTO.setInterpretesVa(evento.getInterpretesVa());

        eventoDTO.setDuracionEs(evento.getDuracionEs());
        eventoDTO.setDuracionVa(evento.getDuracionVa());
        eventoDTO.setDuracion(evento.getDuracion());

        eventoDTO.setImagen(evento.getImagen());
        eventoDTO.setImagenContentType(evento.getImagenContentType());
        eventoDTO.setImagenSrc(evento.getImagenSrc());

        eventoDTO.setImagenPubli(evento.getImagenPubli());
        eventoDTO.setImagenPubliContentType(evento.getImagenPubliContentType());
        eventoDTO.setImagenPubliSrc(evento.getImagenPubliSrc());

        eventoDTO.setPremiosEs(evento.getPremiosEs());
        eventoDTO.setPremiosVa(evento.getPremiosVa());

        eventoDTO.setCaracteristicasEs(evento.getCaracteristicasEs());
        eventoDTO.setCaracteristicasVa(evento.getCaracteristicasVa());

        eventoDTO.setComentariosEs(evento.getComentariosEs());
        eventoDTO.setComentariosVa(evento.getComentariosVa());

        if (eventoDTO.getParTiposEvento() != null)
            eventoDTO.setParTiposEvento(TipoEvento.tipoEventoToTipoEventoDTO(evento.getParTiposEvento()));
        else if (evento.getParTiposEvento() != null)
            eventoDTO.setParTiposEvento(TipoEvento.tipoEventoToTipoEventoDTO(evento.getParTiposEvento()));

        if (evento.getParTpv() != null) {
            eventoDTO.setParTpv(Tpv.tpvToTpvDTO(evento.getParTpv()));
        }

        eventoDTO.setAsientosNumerados(evento.getAsientosNumerados());
        eventoDTO.setIvaSgae(evento.getIvaSGAE());
        eventoDTO.setRetencionSgae(evento.getRetencionSGAE());
        eventoDTO.setPorcentajeIva(evento.getPorcentajeIVA());

        eventoDTO.setFormato(evento.getFormato());
        eventoDTO.setExpediente(evento.getExpediente());
        eventoDTO.setCodigoDistribuidora(evento.getCodigoDistribuidora());
        eventoDTO.setNombreDistribuidora(evento.getNombreDistribuidora());
        eventoDTO.setNacionalidad(evento.getNacionalidad());
        eventoDTO.setVo(evento.getVo());
        eventoDTO.setMetraje(evento.getMetraje());
        eventoDTO.setSubtitulos(evento.getSubtitulos());
        eventoDTO.setPublico(evento.getPublico());
        eventoDTO.setReservasPublicas(evento.getReservasPublicas());
        eventoDTO.setIsabono(evento.getAbono());
        eventoDTO.setEntradasNominales(evento.getEntradasNominales());
        eventoDTO.setEntradasLimitadas(evento.getEntradasLimitadas());
        eventoDTO.setEntradasPorEmail(evento.getEntradasPorEmail());

        return eventoDTO;
    }

    public Evento(
        EventoDTO eventoDTO,
        boolean crearConImagen
    ) {
        this.sesiones = new ArrayList<Sesion>();

        this.id = eventoDTO.getId();
        this.tituloEs = eventoDTO.getTituloEs();
        this.tituloVa = eventoDTO.getTituloVa();

        this.descripcionEs = eventoDTO.getDescripcionEs();
        this.descripcionVa = eventoDTO.getDescripcionVa();

        this.companyiaEs = eventoDTO.getCompanyiaEs();
        this.companyiaVa = eventoDTO.getCompanyiaVa();

        this.interpretesEs = eventoDTO.getInterpretesEs();
        this.interpretesVa = eventoDTO.getInterpretesVa();

        this.duracionEs = eventoDTO.getDuracionEs();
        this.duracionVa = eventoDTO.getDuracionVa();
        this.duracion = eventoDTO.getDuracion();

        if (crearConImagen) {
            this.imagen = eventoDTO.getImagen();
            this.imagenUUID = eventoDTO.getImagenUUID();
        }

        this.imagenContentType = eventoDTO.getImagenContentType();
        this.imagenSrc = eventoDTO.getImagenSrc();

        if (eventoDTO.getImagenPubli() != null || eventoDTO.getImagenPubliUUID() != null) {
            if (crearConImagen) {
                this.imagenPubli = eventoDTO.getImagenPubli();
                this.imagenPubliUUID = eventoDTO.getImagenPubliUUID();
            }
            this.imagenPubliContentType = eventoDTO.getImagenPubliContentType();
            this.imagenPubliSrc = eventoDTO.getImagenPubliSrc();
        }

        this.premiosEs = eventoDTO.getPremiosEs();
        this.premiosVa = eventoDTO.getPremiosVa();

        this.caracteristicasEs = eventoDTO.getCaracteristicasEs();
        this.caracteristicasVa = eventoDTO.getCaracteristicasVa();

        this.comentariosEs = eventoDTO.getComentariosEs();
        this.comentariosVa = eventoDTO.getComentariosVa();

        if (eventoDTO.getParTiposEvento() != null) {
            this.parTiposEvento = new TipoEvento();
            this.parTiposEvento.setId(eventoDTO.getParTiposEvento().getId());
            this.parTiposEvento.setNombreEs(eventoDTO.getParTiposEvento().getNombreEs());
            this.parTiposEvento.setNombreVa(eventoDTO.getParTiposEvento().getNombreVa());
            this.tipoEvento = eventoDTO.getParTiposEvento().getId();
        }

        if (eventoDTO.getParTpv() != null) {
            this.parTpv = Tpv.tpvDTOToTpv(eventoDTO.getParTpv());
        }

        this.asientosNumerados = eventoDTO.getAsientosNumerados();
        this.entradasNominales = eventoDTO.getEntradasNominales();
        this.ivaSGAE = eventoDTO.getIvaSgae();
        this.retencionSGAE = eventoDTO.getRetencionSgae();
        this.porcentajeIVA = eventoDTO.getPorcentajeIva();
        this.rssId = eventoDTO.getRssId();
        this.expediente = eventoDTO.getExpediente();
        this.codigoDistribuidora = eventoDTO.getCodigoDistribuidora();
        this.nombreDistribuidora = eventoDTO.getNombreDistribuidora();
        this.vo = eventoDTO.getVo();
        this.subtitulos = eventoDTO.getSubtitulos();
        this.promotor = eventoDTO.getPromotor();
        this.nifPromotor = eventoDTO.getPromotor();
        this.telefonoPromotor = eventoDTO.getTelefonoPromotor();
        this.emailPromotor = eventoDTO.getEmailPromotor();
        this.direccionPromotor = eventoDTO.getDireccionPromotor();
        this.publico = eventoDTO.getPublico();
        this.reservasPublicas = eventoDTO.getReservasPublicas();
        this.abono = eventoDTO.getIsabono();
        this.entradasLimitadas = eventoDTO.getEntradasLimitadas();
        this.entradasPorEmail = eventoDTO.getEntradasPorEmail();
    }

    public Evento(
        String tituloEs,
        TipoEvento tipoEvento
    ) {
        this.sesiones = new ArrayList<Sesion>();
        this.parTiposEvento = new TipoEvento();
        this.parTiposEvento = tipoEvento;
        this.tituloEs = tituloEs;
    }

    public Evento(
        Abono abono,
        TipoEvento tiposEvento,
        Cine cine
    ) {
        this(null, abono.getNombre(), abono.getDescripcionEs(), null, null, null, null, null, null, abono.getNombre(),
            abono.getDescripcionVa(), null, null, null, null, null, null, abono.getImagen(), abono.getImagenSrc(),
            abono.getImagenContentType(), null, null, null, Long.valueOf(tiposEvento.getId()).intValue(),
            abono.getParTpv() != null ? Long.valueOf(abono.getParTpv().getId()).intValue() : null, 0, BigDecimal.ZERO,
            null, null, false, false, null, null, null, null, null, null, null, null, cine, null, null, null, null,
            null,
            true, false, false, null);
        this.abono = true;
    }

    public Evento(
        String rssId,
        String tituloEs,
        String descripcionEs,
        String companyiaEs,
        String interpretesEs,
        String duracionEs,
        String premiosEs,
        String caracteristicasEs,
        String comentariosEs,
        String tituloVa,
        String descripcionVa,
        String companyiaVa,
        String interpretesVa,
        String duracionVa,
        String premiosVa,
        String caracteristicasVa,
        String comentariosVa,
        byte[] dataBinary,
        String nombreArchivo,
        String mediaType,
        byte[] dataBinaryPubli,
        String nombreArchivoPubli,
        String mediaTypePubli,
        Integer tipoEventoId,
        Integer tpvId,
        int duracion,
        BigDecimal porcentajeIVA,
        BigDecimal retencionSGAE,
        BigDecimal ivaSGAE,
        Boolean asientosNumerados,
        Boolean entradasNominales,
        String expediente,
        String codigoDistribuidora,
        String nombreDistribuidora,
        String nacionalidad,
        String vo,
        String metraje,
        String subtitulos,
        String formato,
        Cine cine,
        String promotor,
        String nifPromotor,
        String emailPromotor,
        String telefonoPromotor,
        String direccionPromotor,
        Boolean publico,
        Boolean reservasPublicas,
        Boolean entradasLimitadas,
        Integer entradasPorEmail
    ) {
        this.sesiones = new ArrayList<Sesion>();
        this.rssId = rssId;
        this.tituloEs = tituloEs;
        this.descripcionEs = descripcionEs;
        this.companyiaEs = companyiaEs;
        this.interpretesEs = interpretesEs;
        this.duracionEs = duracionEs;
        this.premiosEs = premiosEs;
        this.caracteristicasEs = caracteristicasEs;
        this.comentariosEs = comentariosEs;

        this.tituloVa = tituloVa;
        this.descripcionVa = descripcionVa;
        this.companyiaVa = companyiaVa;
        this.interpretesVa = interpretesVa;
        this.duracionVa = duracionVa;
        this.premiosVa = premiosVa;
        this.caracteristicasVa = caracteristicasVa;
        this.comentariosVa = comentariosVa;

        this.imagen = dataBinary;
        this.imagenSrc = nombreArchivo;
        this.imagenContentType = mediaType;

        this.imagenPubli = dataBinaryPubli;
        this.imagenPubliSrc = nombreArchivoPubli;
        this.imagenPubliContentType = mediaTypePubli;

        if (tipoEventoId != null) {
            this.parTiposEvento = new TipoEvento();
            this.parTiposEvento.setId(tipoEventoId);
            this.tipoEvento = tipoEventoId;
        }

        if (tpvId != null) {
            this.parTpv = new Tpv();
            this.parTpv.setId(tpvId);
        }

        this.promotor = promotor;
        this.nifPromotor = nifPromotor;
        this.emailPromotor = emailPromotor;
        this.telefonoPromotor = telefonoPromotor;
        this.direccionPromotor = direccionPromotor;

        this.porcentajeIVA = porcentajeIVA;
        this.retencionSGAE = retencionSGAE;
        this.ivaSGAE = ivaSGAE;
        this.asientosNumerados = asientosNumerados;
        this.entradasNominales = entradasNominales;

        this.formato = formato;
        this.expediente = expediente;
        this.codigoDistribuidora = codigoDistribuidora;
        this.nombreDistribuidora = nombreDistribuidora;
        this.nacionalidad = nacionalidad;
        this.vo = vo;
        this.metraje = metraje;
        this.subtitulos = subtitulos;
        this.cine = cine;

        this.publico = publico;
        this.reservasPublicas = reservasPublicas;
        this.abono = false;
        this.entradasLimitadas = entradasLimitadas;
        this.entradasPorEmail = entradasPorEmail;
        this.duracion = duracion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlTransient
    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public void setImagenSrc(String imagenSrc) {
        this.imagenSrc = imagenSrc;
    }

    public String getImagenContentType() {
        return imagenContentType;
    }

    public void setImagenContentType(String imagenContentType) {
        this.imagenContentType = imagenContentType;
    }

    public String getImagenSrc() {
        return imagenSrc;
    }

    @XmlTransient
    public byte[] getImagenPubli() {
        return imagenPubli;
    }

    public void setImagenPubli(byte[] imagenPubli) {
        this.imagenPubli = imagenPubli;
    }

    public String getImagenPubliSrc() {
        return imagenPubliSrc;
    }

    public void setImagenPubliSrc(String imagenPubliSrc) {
        this.imagenPubliSrc = imagenPubliSrc;
    }

    public String getImagenPubliContentType() {
        return imagenPubliContentType;
    }

    public void setImagenPubliContentType(String imagenPubliContentType) {
        this.imagenPubliContentType = imagenPubliContentType;
    }

    public TipoEvento getParTiposEvento() {
        return parTiposEvento;
    }

    public void setParTipoEvento(TipoEvento parTiposEvento) {
        this.parTiposEvento = parTiposEvento;
    }

    public long getTipoEvento() {
        if (parTiposEvento != null)
            return parTiposEvento.getId();
        else
            return 0;
    }

    public Tpv getParTpv() {
        return parTpv;
    }

    public void setParTpv(Tpv parTpv) {
        this.parTpv = parTpv;
    }

    public void setTipoEvento(long tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public String getTituloEs() {
        return tituloEs;
    }

    public void setTituloEs(String tituloEs) {
        this.tituloEs = tituloEs;
    }

    public String getTituloVa() {
        return tituloVa;
    }

    public void setTituloVa(String tituloVa) {
        this.tituloVa = tituloVa;
    }

    public String getDescripcionEs() {
        return descripcionEs;
    }

    public void setDescripcionEs(String descripcionEs) {
        this.descripcionEs = descripcionEs;
    }

    public String getDescripcionVa() {
        return descripcionVa;
    }

    public void setDescripcionVa(String descripcionVa) {
        this.descripcionVa = descripcionVa;
    }

    public String getResumenDescripcionEs() {
        return descripcionEs != null ? Jsoup.parse(descripcionEs).text() : null;
    }

    public String getResumenDescripcionVa() {
        return descripcionVa != null ? Jsoup.parse(descripcionVa).text() : null;
    }

    public String getCompanyiaEs() {
        return companyiaEs;
    }

    public void setCompanyiaEs(String companyiaEs) {
        this.companyiaEs = companyiaEs;
    }

    public String getCompanyiaVa() {
        return companyiaVa;
    }

    public void setCompanyiaVa(String companyiaVa) {
        this.companyiaVa = companyiaVa;
    }

    public String getInterpretesEs() {
        return interpretesEs;
    }

    public void setInterpretesEs(String interpretesEs) {
        this.interpretesEs = interpretesEs;
    }

    public String getInterpretesVa() {
        return interpretesVa;
    }

    public void setInterpretesVa(String interpretesVa) {
        this.interpretesVa = interpretesVa;
    }

    public String getDuracionEs() {
        return duracionEs;
    }

    public void setDuracionEs(String duracionEs) {
        this.duracionEs = duracionEs;
    }

    public String getDuracionVa() {
        return duracionVa;
    }

    public void setDuracionVa(String duracionVa) {
        this.duracionVa = duracionVa;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getPremiosEs() {
        return premiosEs;
    }

    public void setPremiosEs(String premiosEs) {
        this.premiosEs = premiosEs;
    }

    public String getPremiosVa() {
        return premiosVa;
    }

    public void setPremiosVa(String premiosVa) {
        this.premiosVa = premiosVa;
    }

    public String getCaracteristicasEs() {
        return caracteristicasEs;
    }

    public void setCaracteristicasEs(String caracteristicasEs) {
        this.caracteristicasEs = caracteristicasEs;
    }

    public String getCaracteristicasVa() {
        return caracteristicasVa;
    }

    public void setCaracteristicasVa(String caracteristicasVa) {
        this.caracteristicasVa = caracteristicasVa;
    }

    public String getComentariosEs() {
        return comentariosEs;
    }

    public void setComentariosEs(String comentariosEs) {
        this.comentariosEs = comentariosEs;
    }

    public String getComentariosVa() {
        return comentariosVa;
    }

    public void setComentariosVa(String comentariosVa) {
        this.comentariosVa = comentariosVa;
    }

    public Boolean getAsientosNumerados() {
        return asientosNumerados;
    }

    public void setAsientosNumerados(Boolean asientosNumerados) {
        this.asientosNumerados = asientosNumerados;
    }

    public BigDecimal getPorcentajeIVA() {
        return porcentajeIVA;
    }

    public void setPorcentajeIVA(BigDecimal porcentajeIVA) {
        this.porcentajeIVA = porcentajeIVA;
    }

    public BigDecimal getIvaSGAE() {
        return ivaSGAE;
    }

    public void setIvaSGAE(BigDecimal ivaSGAE) {
        this.ivaSGAE = ivaSGAE;
    }

    public BigDecimal getRetencionSGAE() {
        return retencionSGAE;
    }

    public void setRetencionSGAE(BigDecimal retencionSGAE) {
        this.retencionSGAE = retencionSGAE;
    }

    public Date getFechaPrimeraSesion() {
        return fechaPrimeraSesion;
    }

    public void setFechaPrimeraSesion(Date fechaPrimeraSesion) {
        this.fechaPrimeraSesion = fechaPrimeraSesion;
    }

    public List<Sesion> getSesiones() {
        return sesiones;
    }

    public void setSesiones(List<Sesion> sesiones) {
        this.sesiones = sesiones;
    }

    public String getRssId() {
        return rssId;
    }

    public void setRssId(String rssId) {
        this.rssId = rssId;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public String getCodigoDistribuidora() {
        return codigoDistribuidora;
    }

    public void setCodigoDistribuidora(String codigoDistribuidora) {
        this.codigoDistribuidora = codigoDistribuidora;
    }

    public String getNombreDistribuidora() {
        return nombreDistribuidora;
    }

    public void setNombreDistribuidora(String nombreDistribuidora) {
        this.nombreDistribuidora = nombreDistribuidora;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getVo() {
        return vo;
    }

    public void setVo(String vo) {
        this.vo = vo;
    }

    public String getMetraje() {
        return metraje;
    }

    public void setMetraje(String metraje) {
        this.metraje = metraje;
    }

    public void setParTiposEvento(TipoEvento parTiposEvento) {
        this.parTiposEvento = parTiposEvento;
    }

    public String getSubtitulos() {
        return subtitulos;
    }

    public void setSubtitulos(String subtitulos) {
        this.subtitulos = subtitulos;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public Cine getCine() {
        return cine;
    }

    public void setCine(Cine cine) {
        this.cine = cine;
    }

    public String getPromotor() {
        return promotor;
    }

    public void setPromotor(String promotor) {
        this.promotor = promotor;
    }

    public String getNifPromotor() {
        return nifPromotor;
    }

    public void setNifPromotor(String nifPromotor) {
        this.nifPromotor = nifPromotor;
    }

    public static void checkValidity(
        RegistroPelicula registroPelicula
    ) {
        if (Strings.isNullOrEmpty(registroPelicula.getCodigoPelicula()))
            throw new RegistroSerializaException(GeneralPARException.CODIGO_PELICULA_NULO_CODE);

        if (registroPelicula.getCodigoExpediente() == null)
            throw new RegistroSerializaException(GeneralPARException.CODIGO_EXPEDIENTE_NULO_CODE);

        if (registroPelicula.getTitulo() == null)
            throw new RegistroSerializaException(GeneralPARException.TITULO_PELICULA_NULO_CODE);

        if (registroPelicula.getCodigoDistribuidora() == null)
            throw new RegistroSerializaException(GeneralPARException.CODIGO_DISTRIBUIDORA_NULO_CODE);

        if (registroPelicula.getNombreDistribuidora() == null)
            throw new RegistroSerializaException(GeneralPARException.NOMBRE_DISTRIBUIDORA_NULO_CODE);

        if (registroPelicula.getVersionOriginal() == null)
            throw new RegistroSerializaException(GeneralPARException.VERSION_ORIGINAL_NULO_CODE);

        if (registroPelicula.getVersionLinguistica() == null)
            throw new RegistroSerializaException(GeneralPARException.VERSION_LINGUISTICA_NULO_CODE);

        if (registroPelicula.getIdiomaSubtitulos() == null)
            throw new RegistroSerializaException(GeneralPARException.IDIOMA_SUBTITULOS_NULO_CODE);

        if (registroPelicula.getFormatoProyeccion() == null)
            throw new RegistroSerializaException(GeneralPARException.FORMATO_PROYECCION_NULO_CODE);

        if (registroPelicula.getCodigoPelicula().length() > 5) {
            logger.error("El codigo de película tiene más de 5 caracteres: " + registroPelicula.getCodigoPelicula());
            throw new RegistroSerializaException(GeneralPARException.CODIGO_PELICULA_LARGO_CODE);
        }

        if (registroPelicula.getCodigoExpediente().length() > 12) {
            logger.error("El codigo de expediente tiene más de 12 caracteres: " + registroPelicula.getCodigoExpediente());
            throw new RegistroSerializaException(GeneralPARException.CODIGO_EXPEDIENTE_LARGO_CODE);
        }

        if (registroPelicula.getCodigoDistribuidora().length() > 12) {
            logger.error("El codigo de distribuidora tiene más de 12 caracteres: " + registroPelicula.getCodigoDistribuidora());
            throw new RegistroSerializaException(GeneralPARException.CODIGO_DISTRIBUIDORA_LARGO_CODE);
        }

        if (registroPelicula.getNombreDistribuidora().length() > 50) {
            logger.error("El nombre de distribuidora tiene más de 50 caracteres: " + registroPelicula.getNombreDistribuidora());
            throw new RegistroSerializaException(GeneralPARException.NOMBRE_DISTRIBUIDORA_LARGO_CODE);
        }

        if (registroPelicula.getVersionOriginal().length() != 1)
            throw new RegistroSerializaException(GeneralPARException.DIGITOS_VERSION_ORIGINAL_CODE);

        if (registroPelicula.getVersionLinguistica().length() != 1)
            throw new RegistroSerializaException(GeneralPARException.DIGITOS_VERSION_LINGUISTICA_CODE);

        if (registroPelicula.getIdiomaSubtitulos().length() != 1)
            throw new RegistroSerializaException(GeneralPARException.DIGITOS_IDIOMA_SUBTITULOS_CODE);

        if (registroPelicula.getFormatoProyeccion().length() != 1)
            throw new RegistroSerializaException(GeneralPARException.DIGITOS_FORMATO_PROYECCION_CODE);
    }


    public List<EventoMultisesion> getEventosMultisesion() {
        return eventosMultisesion;
    }

    public void setEventosMultisesion(String jsonEventosMultisesion) {
        Gson gson = new Gson();
        List<EventoMultisesion> eventos =
            gson.fromJson(jsonEventosMultisesion, new TypeToken<List<EventoMultisesion>>() {
            }.getType());
        this.eventosMultisesion = eventos;
    }

    public String getMultisesion() {
        return multisesion;
    }

    public void setMultisesion(String multisesion) {
        this.multisesion = multisesion;
    }

    public Boolean getMultipleTpv() {
        return multipleTpv;
    }

    public void setMultipleTpv(Boolean multipleTpv) {
        this.multipleTpv = multipleTpv;
    }

    public String getImagenUUID() {
        return imagenUUID;
    }

    public void setImagenUUID(String imagenUUID) {
        this.imagenUUID = imagenUUID;
    }

    public String getImagenPubliUUID() {
        return imagenPubliUUID;
    }

    public void setImagenPubliUUID(String imagenPubliUUID) {
        this.imagenPubliUUID = imagenPubliUUID;
    }

    public String getEmailPromotor() {
        return emailPromotor;
    }

    public void setEmailPromotor(String emailPromotor) {
        this.emailPromotor = emailPromotor;
    }

    public String getTelefonoPromotor() {
        return telefonoPromotor;
    }

    public void setTelefonoPromotor(String telefonoPromotor) {
        this.telefonoPromotor = telefonoPromotor;
    }

    public String getDireccionPromotor() {
        return direccionPromotor;
    }

    public void setDireccionPromotor(String direccionPromotor) {
        this.direccionPromotor = direccionPromotor;
    }

    public Boolean getPublico() {
        return publico;
    }

    public void setPublico(Boolean publico) {
        this.publico = publico;
    }

    public Boolean getAbono() {
        return abono;
    }

    public void setAbono(Boolean abono) {
        this.abono = abono;
    }

    public Boolean getEntradasNominales() {
        return entradasNominales;
    }

    public void setEntradasNominales(Boolean entradasNominales) {
        this.entradasNominales = entradasNominales;
    }

    public Boolean getReservasPublicas() {
        return reservasPublicas;
    }

    public void setReservasPublicas(Boolean reservasPublicas) {
        this.reservasPublicas = reservasPublicas;
    }

    public Boolean getEntradasLimitadas() {
        return entradasLimitadas;
    }

    public void setEntradasLimitadas(Boolean entradasLimitadas) {
        this.entradasLimitadas = entradasLimitadas;
    }

    public Integer getEntradasPorEmail() {
        return entradasPorEmail;
    }

    public void setEntradasPorEmail(Integer entradasPorEmail) {
        this.entradasPorEmail = entradasPorEmail;
    }
}