package es.uji.apps.par.report;

import java.math.BigDecimal;

public class InformeTotalesModelReport {
    private BigDecimal numeroEntradasTPV = BigDecimal.ZERO;
    private BigDecimal numeroEntradasEfectivo = BigDecimal.ZERO;
    private BigDecimal numeroEntradasCredito = BigDecimal.ZERO;
    private BigDecimal numeroEntradasTransferencia = BigDecimal.ZERO;
    private BigDecimal numeroEntradasOnline = BigDecimal.ZERO;
    private BigDecimal totalTaquillaTpv = BigDecimal.ZERO;
    private BigDecimal totalTaquillaEfectivo = BigDecimal.ZERO;
    private BigDecimal totalTaquillaCredito = BigDecimal.ZERO;
    private BigDecimal totalTaquillaTransferencia = BigDecimal.ZERO;
    private BigDecimal totalOnline = BigDecimal.ZERO;

    public InformeTotalesModelReport() {
    }

    public BigDecimal getNumeroEntradasTPV() {
        return numeroEntradasTPV;
    }

    public void setNumeroEntradasTPV(BigDecimal numeroEntradasTPV) {
        this.numeroEntradasTPV = numeroEntradasTPV;
    }

    public BigDecimal getNumeroEntradasEfectivo() {
        return numeroEntradasEfectivo;
    }

    public void setNumeroEntradasEfectivo(BigDecimal numeroEntradasEfectivo) {
        this.numeroEntradasEfectivo = numeroEntradasEfectivo;
    }

    public BigDecimal getNumeroEntradasCredito() {
        return numeroEntradasCredito;
    }

    public void setNumeroEntradasCredito(BigDecimal numeroEntradasCredito) {
        this.numeroEntradasCredito = numeroEntradasCredito;
    }

    public BigDecimal getNumeroEntradasOnline() {
        return numeroEntradasOnline;
    }

    public BigDecimal getNumeroEntradasTransferencia() {
        return numeroEntradasTransferencia;
    }

    public void setNumeroEntradasTransferencia(BigDecimal numeroEntradasTransferencia) {
        this.numeroEntradasTransferencia = numeroEntradasTransferencia;
    }

    public void setNumeroEntradasOnline(BigDecimal numeroEntradasOnline) {
        this.numeroEntradasOnline = numeroEntradasOnline;
    }

    public BigDecimal getTotalTaquillaTpv() {
        return totalTaquillaTpv;
    }

    public void setTotalTaquillaTpv(BigDecimal totalTaquillaTpv) {
        this.totalTaquillaTpv = totalTaquillaTpv;
    }

    public BigDecimal getTotalTaquillaEfectivo() {
        return totalTaquillaEfectivo;
    }

    public void setTotalTaquillaEfectivo(BigDecimal totalTaquillaEfectivo) {
        this.totalTaquillaEfectivo = totalTaquillaEfectivo;
    }

    public BigDecimal getTotalTaquillaCredito() {
        return totalTaquillaCredito;
    }

    public void setTotalTaquillaCredito(BigDecimal totalTaquillaCredito) {
        this.totalTaquillaCredito = totalTaquillaCredito;
    }

    public BigDecimal getTotalTaquillaTransferencia() {
        return totalTaquillaTransferencia;
    }

    public void setTotalTaquillaTransferencia(BigDecimal totalTaquillaTransferencia) {
        this.totalTaquillaTransferencia = totalTaquillaTransferencia;
    }

    public BigDecimal getTotalOnline() {
        return totalOnline;
    }

    public void setTotalOnline(BigDecimal totalOnline) {
        this.totalOnline = totalOnline;
    }
}
