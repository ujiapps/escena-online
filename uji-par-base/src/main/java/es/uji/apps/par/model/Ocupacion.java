package es.uji.apps.par.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Ocupacion
{
    private long disponibles;
    private long vendidas;
    private long reservadas;

    public Ocupacion()
    {
    }

    public Ocupacion(Sesion sesion)
    {
        this.disponibles = sesion.getButacasDisponibles();
        this.vendidas = sesion.getButacasVendidas();
        this.reservadas = sesion.getButacasReservadas() ;
    }

    public long getDisponibles() {
        return disponibles;
    }

    public long getVendidas() {
        return vendidas;
    }

    public long getReservadas() {
        return reservadas;
    }
}