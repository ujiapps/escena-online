package es.uji.apps.par.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import es.uji.apps.par.dao.PlantillasReservasDAO;
import es.uji.apps.par.db.PlantillaReservasDTO;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.PlantillaConEventosException;
import es.uji.apps.par.model.Plantilla;
import es.uji.apps.par.model.Sesion;

@Service
public class PlantillasReservasService {
	@Autowired
	private SesionesService sesionesService;

	@Autowired
    private PlantillasReservasDAO plantillasReservasDAO;

	public List<Plantilla> get(
		String sortParameter,
		int start,
		int limit,
		String userUID
	) {
		List<Plantilla> plantillaPrecios = new ArrayList<Plantilla>();
		
		for (PlantillaReservasDTO plantillaDTO: plantillasReservasDAO.get(sortParameter, start, limit, userUID)) {
			plantillaPrecios.add(Plantilla.plantillaRservasDTOtoPlantilla(plantillaDTO));
		}
		return plantillaPrecios;
	}

	public void remove(int id) {
		try {
			plantillasReservasDAO.remove(id);
		} catch (PersistenceException e) {
			throw new PlantillaConEventosException();
		}
	}

	public Plantilla add(Plantilla plantillaPrecios) throws CampoRequeridoException {
		checkRequiredFields(plantillaPrecios);
		return plantillasReservasDAO.add(plantillaPrecios);
	}

	private void checkRequiredFields(Plantilla plantillaPrecios) throws CampoRequeridoException {
		if (plantillaPrecios.getNombre() == null || plantillaPrecios.getNombre().isEmpty())
			throw new CampoRequeridoException("Nombre");
	}

	public void update(Plantilla plantillaPrecios) throws CampoRequeridoException {
		checkRequiredFields(plantillaPrecios);
		PlantillaReservasDTO oldPlantillaPrecios = plantillasReservasDAO.getById(plantillaPrecios.getId());
		if (oldPlantillaPrecios.getSala().getId() != plantillaPrecios.getSala().getId()) {
			plantillasReservasDAO.removeButacasFromPlantilla(plantillaPrecios.getId());
		}
		plantillasReservasDAO.update(plantillaPrecios);
	}

	public int getTotalPlantillaReservas(String userUID) {
		return plantillasReservasDAO.getTotalPlantillaReservas(userUID);
	}

	public List<Plantilla> getBySesion(Long sesionId, String sortParameter, int start, int limit, String userUID)
	{
		Sesion sesion = sesionesService.getSesion(sesionId, userUID);
		List<Plantilla> plantillaPrecios = new ArrayList<Plantilla>();

		if (sesion.getSala() != null)
		{
			for (PlantillaReservasDTO plantillaDTO : plantillasReservasDAO
				.getBySala(sesion.getSala().getId(), sortParameter, start, limit, userUID))
			{
				plantillaPrecios.add(Plantilla.plantillaRservasDTOtoPlantilla(plantillaDTO));
			}
		}
		return plantillaPrecios;
	}

	public int getTotalPlantillaReservasBySala(Long salaId, String userUID)
	{
		return plantillasReservasDAO.getTotalPlantillaResevasBySala(salaId, userUID);
	}

    public Plantilla getById(long plantillaId) {
		return Plantilla.plantillaRservasDTOtoPlantilla(plantillasReservasDAO.getById(plantillaId));
    }
}
