package es.uji.apps.par.enums;

public enum TipoTarifa {
    ABONO,
    NORMAL,
    TODAS
}
