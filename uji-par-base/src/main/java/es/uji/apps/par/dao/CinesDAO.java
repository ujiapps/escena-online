package es.uji.apps.par.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.QCineDTO;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSalasUsuarioDTO;
import es.uji.apps.par.db.QUsuarioDTO;
import es.uji.apps.par.model.Cine;

@Repository
public class CinesDAO extends BaseDAO
{
    private QCineDTO qCineDTO = QCineDTO.cineDTO;

    @Transactional
    public List<CineDTO> getCines()
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qCineDTO).list(qCineDTO);
    }

	@Transactional
	public List<CineDTO> getCines(String userUID)
	{
		JPAQuery query = new JPAQuery(entityManager);
		QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
		QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
		QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;

		return query.from(qCineDTO)
				.join(qCineDTO.parSalas, qSalaDTO)
				.join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
				.join(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
				.where(qUsuarioDTO.usuario.eq(userUID))
				.list(qCineDTO);
	}

    @Transactional
    public CineDTO getCineById(Long cineId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qCineDTO)
            .where(qCineDTO.id.eq(cineId))
            .uniqueResult(qCineDTO);
    }

    @Transactional
    public Cine addCine(Cine cine)
    {
        CineDTO cineDTO = Cine.cineToCineDTO(cine);

        entityManager.persist(cineDTO);
        
        cine.setId(cineDTO.getId());
        
        return cine;
    }

    @Transactional
    public void updateDatosRegistro(Cine cine) {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qCineDTO);
        updateClause.set(qCineDTO.cif, cine.getCif())
            .set(qCineDTO.empresa, cine.getEmpresa())
            .set(qCineDTO.cp, cine.getCp())
            .set(qCineDTO.codigoMunicipio, cine.getCodigoMunicipio())
            .set(qCineDTO.nombreMunicipio, cine.getNombreMunicipio())
            .set(qCineDTO.telefono, cine.getTelefono())
            .set(qCineDTO.direccion, cine.getDireccion())
            .where(qCineDTO.id.eq(cine.getId()))
            .execute();
    }

    @Transactional
    public void updateCine(Cine cine) {
        CineDTO cineDTO = getCineById(cine.getId());
        cineDTO = rellenarParCineDTOConParCine(cine, cineDTO);

        entityManager.persist(cineDTO);
    }

    private CineDTO rellenarParCineDTOConParCine(
        Cine cine,
        CineDTO cineDTO
    ) {
        byte[] logo = cine.getLogo();
        if (cine.getLogoUUID() != null || (logo != null && logo.length > 0)) {
            if (cine.getLogoUUID() != null) {
                cineDTO.setLogoUUID(cine.getLogoUUID());
            } else {
                cineDTO.setLogo(cine.getLogo());
            }
            cineDTO.setLogoSrc(cine.getLogoSrc());
            cineDTO.setLogoContentType(cine.getLogoContentType());
        }

        byte[] banner = cine.getBanner();
        if (cine.getBannerUUID() != null || (banner != null && banner.length > 0)) {
            if (cine.getBannerUUID() != null) {
                cineDTO.setBannerUUID(cine.getBannerUUID());
            } else {
                cineDTO.setBanner(cine.getBanner());
            }
            cineDTO.setBannerSrc(cine.getBannerSrc());
            cineDTO.setBannerContentType(cine.getBannerContentType());
        }

        return cineDTO;
    }
}
