package es.uji.apps.par.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TipoInforme {
    private String id;
    private String nombre;
    private String nombreCA;
    private String nombreES;
    private String prefix;
    private String suffix;
    private List<SorterInforme> sorters;
    private boolean sessionType;

    public TipoInforme() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCA() {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA) {
        this.nombreCA = nombreCA;
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public List<SorterInforme> getSorters() {
        return sorters;
    }

    public void setSorters(List<SorterInforme> sorters) {
        this.sorters = sorters;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public boolean isSessionType() {
        return sessionType;
    }

    public void setSessionType(boolean sessionType) {
        this.sessionType = sessionType;
    }

    public static String getDefaultGenerales() {
        return "[{\"id\":\"excelTaquilla\",\"nombreCA\":\"Excel de taquilla\",\"nombreES\":\"Excel de taquilla\", \"prefix\": \"taquilla/\", \"suffix\": \"\"},{\"id\":\"excelVentas\",\"nombreCA\":\"Excel de vendes\",\"nombreES\":\"Excel de ventas\",\"prefix\": \"ventas/\", \"suffix\": \"\"},{\"id\":\"excelEventos\",\"nombreCA\":\"Excel dels events\",\"nombreES\":\"Excel de los eventos\", \"prefix\": \"eventos/\",\"suffix\": \"\"},{\"id\":\"excelTaquillaCompras\",\"nombreCA\":\"Excel de taquilla agrupat per compres\",\"nombreES\":\"Excel de taquilla agrupado por compras\",\"prefix\": \"taquilla/\",\"suffix\": \"/excel/compras\"},{\"id\":\"pdfTaquilla\",\"nombreCA\":\"PDF de taquilla\",\"nombreES\":\"PDF de taquilla\",\"prefix\": \"taquilla/\",\"suffix\": \"/pdf\"},{\"id\":\"pdfTaquillaCompras\",\"nombreCA\":\"PDF de taquilla agrupat per compres\",\"nombreES\":\"PDF de taquilla agrupado por compras\",\"prefix\": \"taquilla/\",\"suffix\": \"/pdf/compras\", \"sorters\":[{\"id\": \"evento\", \"nombreCA\": \"Nom de l'espectacle\", \"nombreES\": \"Nombre del evento\"},{\"id\": \"fechaCompra\", \"nombreCA\": \"Data de la compra\", \"nombreES\": \"Fecha de la compra\"}, {\"id\": \"sesion\", \"nombreCA\": \"Data de la sessió\", \"nombreES\": \"Fecha de la sesión\"}]}, {\"id\":\"pdfMiTaquilla\",\"nombreCA\":\"PDF de la meua taquilla\",\"nombreES\":\"PDF de mi taquilla\",\"prefix\": \"mitaquilla/\",\"suffix\": \"/pdf\", \"sorters\":[{\"id\": \"evento\", \"nombreCA\": \"Nom de l'espectacle\", \"nombreES\": \"Nombre del evento\"},{\"id\": \"fechaCompra\", \"nombreCA\": \"Data de la compra\", \"nombreES\": \"Fecha de la compra\"}, {\"id\": \"sesion\", \"nombreCA\": \"Data de la sessió\", \"nombreES\": \"Fecha de la sesión\"}]},{\"id\":\"pdfMiTaquillaSesion\",\"nombreCA\":\"PDF de les sessions\",\"nombreES\":\"PDF de las sesiones\",\"prefix\": \"mitaquillasesion/\",\"suffix\": \"/pdf\", \"sessionType\": \"true\", \"sorters\":[{\"id\": \"evento\", \"nombreCA\": \"Nom de l'espectacle\", \"nombreES\": \"Nombre del evento\"},{\"id\": \"fechaCompra\", \"nombreCA\": \"Data de la compra\", \"nombreES\": \"Fecha de la compra\"}, {\"id\": \"sesion\", \"nombreCA\": \"Data de la sessió\", \"nombreES\": \"Fecha de la sesión\"}]},{\"id\":\"pdfEfectiu\",\"nombreCA\":\"PDF de taquilla en efectiu\",\"nombreES\":\"PDF de taquilla en efectivo\", \"prefix\": \"taquilla/\", \"suffix\": \"/efectivo/pdf\"},{\"id\":\"pdfTpv\",\"nombreCA\":\"PDF de TPVs taquilla\",\"nombreES\":\"PDF de TPVs taquilla\", \"prefix\": \"taquilla/\", \"suffix\": \"/tpv/pdf\"}, {\"id\":\"pdfTpvOnline\",\"nombreCA\":\"PDF de TPVs online\",\"nombreES\":\"PDF de TPVs online\", \"prefix\": \"taquilla/\", \"suffix\": \"/tpv/online/pdf\"}, {\"id\":\"pdfSGAE\",\"nombreCA\":\"PDF SGAE\",\"nombreES\":\"PDF SGAE\", \"prefix\": \"taquilla/\", \"suffix\": \"/eventos/pdf\"}]";
    }
}
