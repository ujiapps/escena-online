package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.config.ConfigurationSelector;
import es.uji.apps.par.exceptions.ResponseMessage;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.utils.LocaleUtils;

public class BaseResource {
    public static final String MENU_ABONO = "menuAbono";
    public static final String APP = "par";

    @Context
    HttpServletRequest currentRequest;

    @InjectParam
    Configuration configuration;

    @InjectParam
    protected ConfigurationSelector configurationSelector;

    protected String getBaseUrlPublic() {
        return configurationSelector.getUrlPublic();
    }

    //para evitar el problema de proxy de benicassim en las redirecciones absolutas
    //deberían arreglarlo ellos pero no lo quieren hacer
    protected String getBaseUrlPublicLimpio() {
        String urlPublicLimpio = configurationSelector.getUrlPublicLimpio();
        urlPublicLimpio = (urlPublicLimpio == null) ? configurationSelector.getUrlPublic() : urlPublicLimpio;
        return urlPublicLimpio;
    }

    public Response errorResponse(String messageProperty, Object... values) {
        String errorMessage = getProperty(messageProperty, values);
        return Response.serverError().entity(new ResponseMessage(false, errorMessage)).build();
    }

    public String getProperty(String messageProperty, Object... values) {
        return ResourceProperties.getProperty(LocaleUtils.getLocale(configurationSelector, currentRequest), messageProperty, values);
    }

    protected boolean correctApiKey(HttpServletRequest request) {
        String requestApiKey = request.getParameter("key");
        String userApiKey = configurationSelector.getApiKey();

        return userApiKey.equals(requestApiKey);
    }

    protected boolean isAdminByToken(HttpServletRequest request) {
        String requestToken = request.getParameter("token");
        String adminToken = configuration.getAdminToken();

        return adminToken != null && requestToken != null && adminToken.equals(requestToken);
    }

    protected Response apiAccessDenied() {
        return Response.status(HttpStatus.UNAUTHORIZED.value()).build();
    }
}
