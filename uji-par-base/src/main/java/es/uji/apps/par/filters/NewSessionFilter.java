package es.uji.apps.par.filters;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import es.uji.apps.par.config.Configuration;

import static es.uji.apps.par.filters.QueueFilter.QUEUE_PATTERN;
import static es.uji.apps.par.filters.QueueFilter.QUEU_ID_ATTR;
import static es.uji.apps.par.filters.QueueFilter.SPRING_PREFIX;
import static es.uji.apps.par.filters.QueueFilter.TMP_DIR;
import static es.uji.apps.par.filters.QueueFilter.TMP_PREFIX;

public class NewSessionFilter implements Filter {
    protected Configuration configuration;

    public void init(FilterConfig filterConfig) {

    }

    public void destroy() {
    }

    public void doFilter(
        ServletRequest servletRequest,
        ServletResponse servletResponse,
        FilterChain filterChain
    ) throws IOException, ServletException {
        initService(servletRequest);

        if (configuration.getQueueDomains() != null) {
            HttpServletRequest sRequest = (HttpServletRequest) servletRequest;
            if (QUEUE_PATTERN.matcher(sRequest.getRequestURI()).matches()) {
                HttpSession session = sRequest.getSession();
                if (session.isNew()) {
                    String uuid = (String) servletRequest.getAttribute(QUEU_ID_ATTR);
                    Path tempFilePath = Paths.get(TMP_DIR, TMP_PREFIX + uuid);
                    Path newFilePath = Paths.get(TMP_DIR, SPRING_PREFIX + session.getId());
                    if (uuid != null && Files.exists(tempFilePath)) {
                        Files.move(tempFilePath, newFilePath);
                    } else {
                        new File(newFilePath.toString()).createNewFile();
                    }
                }
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void initService(ServletRequest servletRequest) {
        if (configuration == null) {
            ServletContext servletContext = servletRequest.getServletContext();
            WebApplicationContext webApplicationContext =
                WebApplicationContextUtils.getWebApplicationContext(servletContext);
            configuration = (Configuration) webApplicationContext.getBean("Configuration");
        }
    }
}