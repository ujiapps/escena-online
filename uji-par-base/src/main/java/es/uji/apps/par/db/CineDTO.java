package es.uji.apps.par.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the PAR_CINES database table.
 */
@Entity
@Table(name = "PAR_CINES")
public class CineDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "PAR_CINES_ID_GENERATOR", sequenceName = "HIBERNATE_SEQUENCE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAR_CINES_ID_GENERATOR")
    private long id;

    @Column(name = "CODIGO")
    private String codigo;

    @Column(name = "CODIGO_ICAA")
    private String codigoIcaa;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "CIF")
    private String cif;

    @Column(name = "DIRECCION")
    private String direccion;

    @Column(name = "COD_MUNICIPIO")
    private String codigoMunicipio;

    @Column(name = "NOM_MUNICIPIO")
    private String nombreMunicipio;

    @Column(name = "CP")
    private String cp;

    @Column(name = "EMPRESA")
    private String empresa;

    @Column(name = "COD_REGISTRO")
    private String codigoRegistro;

    @Column(name = "TFNO")
    private String telefono;

    @Column(name = "IVA")
    private BigDecimal iva;

    @Column(name = "URL_PUBLIC")
    private String urlPublic;

    @Column(name = "URL_PRIVACIDAD")
    private String urlPrivacidad;

    @Column(name = "URL_CANCELACION")
    private String urlCancelacion;

    @Column(name = "URL_COMO_LLEGAR")
    private String urlComoLlegar;

    @Column(name = "MAIL_FROM")
    private String mailFrom;

    @Column(name = "LOGO_REPORT")
    private String logoReport;

    @Column(name = "URL_PIE_ENTRADA")
    private String urlPieEntrada;

    @Column(name = "API_KEY")
    private String apiKey;

    @Column(name = "BUTACASENTRADOENDISTINTOCOLOR")
    private Boolean showButacasQueHanEntradoEnDistintoColor;

    @Column(name = "LANGS")
    private String langs;

    @Column(name = "DEFAULT_LANG")
    private String defaultLang;

    @Column(name = "SHOW_IVA")
    private Boolean showIVA;

    @Column(name = "PASSBOOK_ACTIVADO")
    private Boolean passbookActivado;

    @Column(name = "LIMITE_ENTRADAS_GRATIS")
    private Integer limiteEntradasGratuitasPorCompra;

    @Column(name = "LIMITE_NO_GRATUITAS")
    private Boolean limiteNoGratuitas;

    @Column(name = "COMISION", scale = 10, precision = 10)
    private BigDecimal comision;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "REGISTRO_MERCANTIL")
    private String registroMercantil;

    private byte[] logo;

    @Column(name="LOGO_CONTENT_TYPE")
    private String logoContentType;

    @Column(name="LOGO_SRC")
    private String logoSrc;

    @Column(name="LOGO_UUID")
    private String logoUUID;

    private byte[] banner;

    @Column(name="BANNER_CONTENT_TYPE")
    private String bannerContentType;

    @Column(name="BANNER_SRC")
    private String bannerSrc;

    @Column(name="BANNER_UUID")
    private String bannerUUID;

    @Column(name="CLASE_POSTVENTA")
    private String clasePostventa;

    @Column(name="INFORMES")
    private String informes;

    @Column(name="INFORMES_GENERALES")
    private String informesGenerales;

    @Column(name="FORMAS_PAGO")
    private String formasPago;

    @Column(name="MENU_TPV")
    private boolean menuTpv;

    @Column(name="MENU_CLIENTES")
    private boolean menuClientes;

    @Column(name="MENU_INTEGRACIONES")
    private boolean menuIntegraciones;

    @Column(name="MENU_SALAS")
    private boolean menuSalas;

    @Column(name="MENU_PERFIL")
    private boolean menuPerfil;

    @Column(name="MENU_AYUDA")
    private boolean menuAyuda;

    @Column(name="MENU_TAQUILLA")
    private boolean menuTaquilla;

    @Column(name="MENU_VENTA_ANTICIPADA")
    private boolean menuVentaAnticipada;

    @Column(name="MENU_ABONOS")
    private boolean menuAbonos;

    @Column(name="MENU_ICAA")
    private boolean menuICAA;

    @Column(name="SHOW_COMO_NOS_CONOCISTE")
    private boolean showComoNosConociste;

    @Column(name="CHECK_CONTRATACION")
    private boolean checkContratacion;

    @Column(name="SHOW_LOGO_ENTRADA")
    private boolean showLogoEntrada;

    @Column(name="NO_NUMERADAS_SECUENCIAL")
    private boolean noNumeradasSecuencia;

    @Column(name="IS_ANULABLE_DESDE_ENLACE")
    private boolean anulableDesdeEnlace;

    @Column(name="TIEMPO_ANULABLE_DESDE_ENLACE")
    private int tiempoRestanteAnulableDesdeEnlace;

    @OneToMany(mappedBy = "parCine", fetch = FetchType.LAZY)
    private List<SalaDTO> parSalas;

    @OneToMany(mappedBy = "parCine")
    private List<TarifasCineDTO> parTarifasCine;

    @OneToMany(mappedBy = "parCine", fetch = FetchType.LAZY)
    private List<SalaDTO> parEventos;

    @OneToMany(mappedBy = "parCine", fetch = FetchType.LAZY)
    private List<TarifaDTO> parTarifas;

    @OneToMany(mappedBy = "parCine", fetch = FetchType.LAZY)
    private List<TipoEventoDTO> parTiposEvento;

    @OneToMany(mappedBy = "parCine", fetch = FetchType.LAZY)
    private List<ReportDTO> parReports;

    @OneToMany(mappedBy = "parCine", fetch = FetchType.LAZY)
    private List<ComoNosConocisteDTO> motivosComoNosConociste;

    public CineDTO() {
    }

    public CineDTO(long id) {
        this.id = id;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigoIcaa() {
        return codigoIcaa;
    }

    public void setCodigoIcaa(String codigoIcaa) {
        this.codigoIcaa = codigoIcaa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCodigoRegistro() {
        return codigoRegistro;
    }

    public void setCodigoRegistro(String codigoRegistro) {
        this.codigoRegistro = codigoRegistro;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    public List<SalaDTO> getParSalas() {
        return parSalas;
    }

    public void setParSalas(List<SalaDTO> parSalas) {
        this.parSalas = parSalas;
    }

    public List<TarifasCineDTO> getParTarifasCine() {
        return parTarifasCine;
    }

    public void setParTarifasCine(List<TarifasCineDTO> parTarifasCine) {
        this.parTarifasCine = parTarifasCine;
    }

    public List<SalaDTO> getParEventos() {
        return parEventos;
    }

    public void setParEventos(List<SalaDTO> parEventos) {
        this.parEventos = parEventos;
    }

    public List<TarifaDTO> getParTarifas() {
        return parTarifas;
    }

    public void setParTarifas(List<TarifaDTO> parTarifas) {
        this.parTarifas = parTarifas;
    }

    public List<TipoEventoDTO> getParTiposEvento() {
        return parTiposEvento;
    }

    public void setParTiposEvento(List<TipoEventoDTO> parTiposEvento) {
        this.parTiposEvento = parTiposEvento;
    }

    public String getUrlPublic() {
        return urlPublic;
    }

    public void setUrlPublic(String urlPublic) {
        this.urlPublic = urlPublic;
    }

    public String getUrlPrivacidad() {
        return urlPrivacidad;
    }

    public void setUrlPrivacidad(String urlPrivacidad) {
        this.urlPrivacidad = urlPrivacidad;
    }

    public String getUrlCancelacion() {
        return urlCancelacion;
    }

    public void setUrlCancelacion(String urlCancelacion) {
        this.urlCancelacion = urlCancelacion;
    }

    public String getUrlComoLlegar() {
        return urlComoLlegar;
    }

    public void setUrlComoLlegar(String urlComoLlegar) {
        this.urlComoLlegar = urlComoLlegar;
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    public String getLogoReport() {
        return logoReport;
    }

    public void setLogoReport(String logoReport) {
        this.logoReport = logoReport;
    }

    public String getUrlPieEntrada() {
        return urlPieEntrada;
    }

    public void setUrlPieEntrada(String urlPieEntrada) {
        this.urlPieEntrada = urlPieEntrada;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Boolean getShowButacasQueHanEntradoEnDistintoColor() {
        return showButacasQueHanEntradoEnDistintoColor;
    }

    public void setShowButacasQueHanEntradoEnDistintoColor(Boolean showButacasQueHanEntradoEnDistintoColor) {
        this.showButacasQueHanEntradoEnDistintoColor = showButacasQueHanEntradoEnDistintoColor;
    }

    public String getLangs() {
        return langs;
    }

    public void setLangs(String langs) {
        this.langs = langs;
    }

    public String getDefaultLang() {
        return defaultLang;
    }

    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    public Boolean getShowIVA() {
        return showIVA;
    }

    public void setShowIVA(Boolean showIVA) {
        this.showIVA = showIVA;
    }

    public Boolean getPassbookActivado() {
        return passbookActivado;
    }

    public void setPassbookActivado(Boolean passbookActivado) {
        this.passbookActivado = passbookActivado;
    }

    public Integer getLimiteEntradasGratuitasPorCompra() {
        return limiteEntradasGratuitasPorCompra;
    }

    public void setLimiteEntradasGratuitasPorCompra(Integer limiteEntradasGratuitasPorCompra) {
        this.limiteEntradasGratuitasPorCompra = limiteEntradasGratuitasPorCompra;
    }

    public Boolean getLimiteNoGratuitas() {
        return limiteNoGratuitas;
    }

    public void setLimiteNoGratuitas(Boolean limiteOnline) {
        this.limiteNoGratuitas = limiteOnline;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistroMercantil() {
        return registroMercantil;
    }

    public void setRegistroMercantil(String registroMercantil) {
        this.registroMercantil = registroMercantil;
    }

    public BigDecimal getComision() {
        return comision;
    }

    public void setComision(BigDecimal comision) {
        this.comision = comision;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public String getLogoSrc() {
        return logoSrc;
    }

    public void setLogoSrc(String logoSrc) {
        this.logoSrc = logoSrc;
    }

    public String getLogoUUID() {
        return logoUUID;
    }

    public void setLogoUUID(String logoUUID) {
        this.logoUUID = logoUUID;
    }

    public byte[] getBanner() {
        return banner;
    }

    public void setBanner(byte[] banner) {
        this.banner = banner;
    }

    public String getBannerContentType() {
        return bannerContentType;
    }

    public void setBannerContentType(String bannerContentType) {
        this.bannerContentType = bannerContentType;
    }

    public String getBannerSrc() {
        return bannerSrc;
    }

    public void setBannerSrc(String bannerSrc) {
        this.bannerSrc = bannerSrc;
    }

    public String getBannerUUID() {
        return bannerUUID;
    }

    public void setBannerUUID(String bannerUUID) {
        this.bannerUUID = bannerUUID;
    }

    public String getClasePostventa() {
        return clasePostventa;
    }

    public void setClasePostventa(String clasePostventa) {
        this.clasePostventa = clasePostventa;
    }

    public String getInformes() {
        return informes;
    }

    public void setInformes(String informes) {
        this.informes = informes;
    }

    public String getInformesGenerales() {
        return informesGenerales;
    }

    public void setInformesGenerales(String informesGenerales) {
        this.informesGenerales = informesGenerales;
    }

    public boolean isMenuTpv() {
        return menuTpv;
    }

    public void setMenuTpv(boolean menuTpv) {
        this.menuTpv = menuTpv;
    }

    public boolean isMenuClientes() {
        return menuClientes;
    }

    public void setMenuClientes(boolean menuClientes) {
        this.menuClientes = menuClientes;
    }

    public boolean isMenuIntegraciones() {
        return menuIntegraciones;
    }

    public void setMenuIntegraciones(boolean menuIntegraciones) {
        this.menuIntegraciones = menuIntegraciones;
    }

    public boolean isMenuSalas() {
        return menuSalas;
    }

    public void setMenuSalas(boolean menuSalas) {
        this.menuSalas = menuSalas;
    }

    public boolean isMenuPerfil() {
        return menuPerfil;
    }

    public void setMenuPerfil(boolean menuPerfil) {
        this.menuPerfil = menuPerfil;
    }

    public boolean isMenuAyuda() {
        return menuAyuda;
    }

    public void setMenuAyuda(boolean menuAyuda) {
        this.menuAyuda = menuAyuda;
    }

    public boolean isMenuTaquilla() {
        return menuTaquilla;
    }

    public void setMenuTaquilla(boolean menuTaquilla) {
        this.menuTaquilla = menuTaquilla;
    }

    public boolean isMenuVentaAnticipada() {
        return menuVentaAnticipada;
    }

    public void setMenuVentaAnticipada(boolean menuVentaAnticipada) {
        this.menuVentaAnticipada = menuVentaAnticipada;
    }

    public boolean isMenuAbonos() {
        return menuAbonos;
    }

    public void setMenuAbonos(boolean menuAbonos) {
        this.menuAbonos = menuAbonos;
    }

    public boolean isMenuICAA() {
        return menuICAA;
    }

    public void setMenuICAA(boolean menuICAA) {
        this.menuICAA = menuICAA;
    }

    public boolean isShowComoNosConociste() {
        return showComoNosConociste;
    }

    public void setShowComoNosConociste(boolean showComoNosConociste) {
        this.showComoNosConociste = showComoNosConociste;
    }

    public List<ComoNosConocisteDTO> getMotivosComoNosConociste() {
        return motivosComoNosConociste;
    }

    public void setMotivosComoNosConociste(List<ComoNosConocisteDTO> motivosComoNosConociste) {
        this.motivosComoNosConociste = motivosComoNosConociste;
    }

    public boolean isCheckContratacion() {
        return checkContratacion;
    }

    public void setCheckContratacion(boolean checkContratacion) {
        this.checkContratacion = checkContratacion;
    }

    public String getFormasPago() {
        return formasPago;
    }

    public void setFormasPago(String formasPago) {
        this.formasPago = formasPago;
    }

    public boolean isShowLogoEntrada() {
        return showLogoEntrada;
    }

    public void setShowLogoEntrada(boolean showLogoEntrada) {
        this.showLogoEntrada = showLogoEntrada;
    }

    public boolean isNoNumeradasSecuencia() {
        return noNumeradasSecuencia;
    }

    public void setNoNumeradasSecuencia(boolean noNumeradasSecuencia) {
        this.noNumeradasSecuencia = noNumeradasSecuencia;
    }

    public boolean isAnulableDesdeEnlace() {
        return anulableDesdeEnlace;
    }

    public void setAnulableDesdeEnlace(boolean cancelableDesdeEnlace) {
        this.anulableDesdeEnlace = cancelableDesdeEnlace;
    }

    public int getTiempoRestanteAnulableDesdeEnlace() {
        return tiempoRestanteAnulableDesdeEnlace;
    }

    public void setTiempoRestanteAnulableDesdeEnlace(int tiempoRestanteCancelableDesdeEnlace) {
        this.tiempoRestanteAnulableDesdeEnlace = tiempoRestanteCancelableDesdeEnlace;
    }
}