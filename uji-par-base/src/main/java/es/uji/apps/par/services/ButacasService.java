package es.uji.apps.par.services;

import com.mysema.query.Tuple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.par.comparator.ButacaComparator;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.config.ConfigurationSelector;
import es.uji.apps.par.dao.ButacasDAO;
import es.uji.apps.par.dao.LocalizacionesDAO;
import es.uji.apps.par.dao.TarifasDAO;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.OcupacionDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.exceptions.ButacaOcupadaException;
import es.uji.apps.par.exceptions.ButacasDiferentesLocalizacionesException;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.model.Compra;
import es.uji.apps.par.model.DisponiblesLocalizacion;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.model.PreciosSesion;

@Service
public class ButacasService {
    @Autowired
    private ButacasDAO butacasDAO;

    @Autowired
    private LocalizacionesDAO localizacionesDAO;

    @Autowired
    private PreciosService preciosService;

    @Autowired
    private EventosService eventosService;

    @Autowired
    private PlantillasButacasReservasService plantillasButacasReservasService;

    @Autowired
    private TarifasDAO tarifasDAO;

    @Autowired
    Configuration configuration;

    @Autowired
    ConfigurationSelector configurationSelector;

    public ButacaDTO getButaca(long idButaca) {
        return butacasDAO.getButaca(idButaca);
    }

    public List<ButacaDTO> getButacas(List<Long> ids) {
        return butacasDAO.getButacas(ids);
    }

    public List<Butaca> getButacas(
        long idSesion,
        List<String> codigosLocalizacion
    ) {
        List<Butaca> butacas = new ArrayList<>();

        List<Tuple> butacasConLocalizacion = butacasDAO.getButacas(idSesion, codigosLocalizacion);
        for (Tuple butacaConLocalizacion: butacasConLocalizacion) {
            Butaca butaca = new Butaca();
            butaca.setAnulada(butacaConLocalizacion.get(0, Boolean.class));
            butaca.setFila(butacaConLocalizacion.get(1, String.class));
            butaca.setNumero(butacaConLocalizacion.get(2, String.class));
            butaca.setPresentada(butacaConLocalizacion.get(3, Date.class));
            butaca.setReserva(butacaConLocalizacion.get(4, Boolean.class));
            butaca.setLocalizacion(butacaConLocalizacion.get(5, String.class));

            butacas.add(butaca);
        }

        return butacas;
    }

    public boolean estaOcupada(
        long idSesion,
        String codigoLocalizacion,
        String fila,
        String numero
    ) {
        return butacasDAO.estaOcupada(idSesion, codigoLocalizacion, fila, numero);
    }

    public Map<String, String> estilosButacasOcupadas(
        Long sesionId,
        List<Localizacion> localizaciones,
        boolean mostrarReservadas
    ) {
        mostrarReservadas = mostrarReservadas || eventosService.isEventoReservasPublicasBySesionId(sesionId);

        Map<String, String> clasesOcupadas = new HashMap<>();
        boolean showButacasHanEntradoEnDistintoColor = configurationSelector.showButacasHanEntradoEnDistintoColor();
        List<Butaca> butacas =
            getButacas(sesionId, localizaciones.stream().map(Localizacion::getCodigo).collect(Collectors.toList()));
        for (Butaca butaca : butacas) {
            if (!butaca.isAnulada()) {
                String key = String
                    .format("%s_%s_%s", butaca.getLocalizacion(), butaca.getFila(), butaca.getNumero());
                String value =
                    (showButacasHanEntradoEnDistintoColor && butaca.getPresentada() != null
                        && mostrarReservadas) ? "mapaPresentada" : "mapaOcupada";
                if (mostrarReservadas && butaca.isReserva())
                    value = "mapaReservada";

                clasesOcupadas.put(key, value);
            }
        }

        return clasesOcupadas;
    }

    public List<Butaca> estanOcupadas(
        long sesionId,
        List<Butaca> butacas,
        String uuidCompra
    ) {
        List<Butaca> ocupadas = new ArrayList<Butaca>();

        List<Tuple> butacasOcupadasNoAnuladasPorSesion = butacasDAO.getButacasOcupadasNoAnuladasPorSesionNotInCompra(sesionId, uuidCompra);
        for (Butaca butaca : butacas) {
            if (butaca.getFila() != null && butaca.getNumero() != null && butaca.getLocalizacion() != null) {
                if (butacasOcupadasNoAnuladasPorSesion.stream().filter(
                    b -> b.get(0, String.class).equals(butaca.getFila())
                            && b.get(1, String.class).equals(butaca.getNumero())
                            && b.get(2, String.class).equals(butaca.getLocalizacion())).findAny().isPresent()) {
                    ocupadas.add(butaca);
                }
            }
        }

        return ocupadas;
    }

    public Compra getCompra(
        long sesionId,
        String localizacion,
        String fila,
        String butaca
    ) {
        CompraDTO compraDTO = butacasDAO.getCompra(sesionId, localizacion, fila, butaca);

        if (compraDTO != null) {
            Compra compra = Compra.compraDTOtoCompra(compraDTO);
            compra.setParButacas(getButacasCompra(compra.getId(), "", 0, Integer.MAX_VALUE, null, ""));
            return compra;
        }

        return null;
    }

    public List<DisponiblesLocalizacion> getDisponibles(long idSesion) {
        return getDisponibles(Arrays.asList(idSesion));
    }

    public List<DisponiblesLocalizacion> getDisponibles(List<Long> sesionIds) {
        List<DisponiblesLocalizacion> disponibles = new ArrayList<DisponiblesLocalizacion>();
        List<OcupacionDTO> ocupaciones = butacasDAO.getOcupadas(sesionIds);
        for (OcupacionDTO ocupacion : ocupaciones) {
            DisponiblesLocalizacion disponible = new DisponiblesLocalizacion(ocupacion.getCodigo(), ocupacion.getTotalEntradas(), ocupacion.getOcupadas(), ocupacion.getSesionId());

            disponibles.add(disponible);
        }

        return disponibles;
    }

    public List<Butaca> getButacasCompra(
        Long idCompra,
        String sort,
        int start,
        int limit,
        ExtGridFilterList filter,
        String language
    ) {
        List<Tuple> listaButacasDTO = butacasDAO.getButacasCompra(idCompra, sort, start, limit, filter);
        List<Butaca> listaButacas = new ArrayList<Butaca>();
        for (Tuple objButacaTarifa : listaButacasDTO) {
            ButacaDTO butacaDTO = objButacaTarifa.get(0, ButacaDTO.class);
            TarifaDTO tarifaDTO = objButacaTarifa.get(1, TarifaDTO.class);
            Butaca butaca = new Butaca(butacaDTO, configuration.isIdEntrada(), language);
            butaca.setTipo(tarifaDTO.getNombre());
            listaButacas.add(butaca);
        }

        return listaButacas;
    }

    public int getTotalButacasCompra(Long idCompra, ExtGridFilterList filter) {
        return butacasDAO.getTotalButacasCompra(idCompra, filter);
    }

    public List<Butaca> getButacasNoAnuladas(
        Long idSesion,
        String language
    ) {
        List<ButacaDTO> butacasNoAnuladas = new ArrayList<>();
        for (Tuple butacaConUUID : butacasDAO.getButacasNoAnuladas(idSesion)) {
            ButacaDTO butacaDTO = new ButacaDTO();
            butacaDTO.setId(butacaConUUID.get(0, Long.class));
            butacaDTO.setFila(butacaConUUID.get(1, String.class));
            butacaDTO.setNumero(butacaConUUID.get(2, String.class));
            butacaDTO.setPrecio(butacaConUUID.get(3, BigDecimal.class));
            butacaDTO.setPrecioSinComision(butacaConUUID.get(4, BigDecimal.class));

            String localizacionCodigo = butacaConUUID.get(5, String.class);
            if (localizacionCodigo != null) {
                LocalizacionDTO localizacionDTO = new LocalizacionDTO();
                localizacionDTO.setCodigo(localizacionCodigo);
                localizacionDTO.setNombreEs(butacaConUUID.get(6, String.class));
                localizacionDTO.setNombreVa(butacaConUUID.get(7, String.class));
                butacaDTO.setParLocalizacion(localizacionDTO);
            }

            butacaDTO.setTipo(butacaConUUID.get(8, String.class));
            butacaDTO.setPresentada(butacaConUUID.get(9, Date.class));
            butacaDTO.setAnulada(butacaConUUID.get(10, Boolean.class));
            butacaDTO.setFechaAnulada(butacaConUUID.get(11, Date.class));

            String compraUUID = butacaConUUID.get(12, String.class);
            if (compraUUID != null) {
                CompraDTO compraDTO = new CompraDTO();
                compraDTO.setUuid(compraUUID);
                compraDTO.setReserva(butacaConUUID.get(13, Boolean.class));
                butacaDTO.setParCompra(compraDTO);
            }

            butacasNoAnuladas.add(butacaDTO);
        }

        return Butaca
            .butacasDTOToButacas(butacasNoAnuladas, configuration.isIdEntrada(), language);
    }

    public void updatePresentadas(List<Butaca> butacas) {
        butacasDAO.updatePresentadas(butacas);
    }

    public long presentarButaca(Butaca butaca) {
        if (butaca.getPresentada() == null) {
            butaca.setPresentada(new Date());
        }
        return butacasDAO.updatePresentada(butaca);
    }

    public List<Butaca> getButacasDisponibles(
        Long butacaId,
        String tarifaId,
        Locale locale
    ) throws IOException {
        ButacaDTO butacaDTO = getButaca(butacaId);
        List<ButacaDTO> butacasOcupadas =
            getButacasNoAnuladasPorLocalizacion(butacaDTO.getParSesion().getId(), tarifaId);

        HashMap<String, List<String>> posicionesOcupadas = new HashMap<>();
        for (ButacaDTO butacaOcupada : butacasOcupadas) {
            if (!posicionesOcupadas.containsKey(butacaOcupada.getFila())) {
                posicionesOcupadas.put(butacaOcupada.getFila(), new ArrayList<String>());
            }
            posicionesOcupadas.get(butacaOcupada.getFila()).add(butacaOcupada.getNumero());
        }

        List<Butaca> butacasDisponibles = new ArrayList<>();


        byte[] encoded = Files.readAllBytes(Paths.get(configuration.getPathJson() + tarifaId + ".json"));
        List<Butaca> butacasTotales = Butaca.parseaJSON(new String(encoded, "UTF-8"));

        int i = 1;
        for (Butaca butaca : butacasTotales) {
            if (posicionesOcupadas.get(butaca.getFila()) == null || !posicionesOcupadas.get(butaca.getFila())
                .contains(butaca.getNumero())) {
                butaca.setId(i);
                butaca.setTexto(String
                    .format(ResourceProperties.getProperty(locale, "entrada.butaca"), butaca.getFila(),
                        butaca.getNumero()));
                butaca.setUuid(butaca.getFila() + "#" + butaca.getNumero());

                butacasDisponibles.add(butaca);
                i++;
            }
        }

        Collections.sort(butacasDisponibles, new ButacaComparator());

        return butacasDisponibles;
    }

    public List<ButacaDTO> getButacasNoAnuladasPorLocalizacion(
        long id,
        String tarifaId
    ) {
        return butacasDAO.getButacasOcupadasNoAnuladasPorLocalizacion(id, tarifaId);
    }

    public void cambiaFilaNumero(
        Long butacaId,
        String fila,
        String numero,
        String locale
    ) {
        ButacaDTO butaca = getButaca(butacaId);
        List<ButacaDTO> butacasOcupadasPorLocalizacion =
            getButacasNoAnuladasPorLocalizacion(butaca.getParSesion().getId(), butaca.getParLocalizacion().getCodigo());

        for (ButacaDTO butacaDTO : butacasOcupadasPorLocalizacion) {
            if (butacaDTO.getFila().equals(fila) && butacaDTO.getNumero().equals(numero)) {
                String nombreLocalizacion = butaca.getParLocalizacion().getNombreEs();
                if(locale.equalsIgnoreCase("ca")) {
                    nombreLocalizacion = butaca.getParLocalizacion().getNombreVa();
                }
                throw new ButacaOcupadaException(butaca.getParSesion().getId(), nombreLocalizacion,
                    fila, numero);
            }
        }

        butacasDAO.cambiaFilaNumero(butacaId, fila, numero);
    }

    public void cambiaTipo(
        List<Long> ids,
        String tipo,
        String userUID
    ) {
        List<ButacaDTO> butacas = getButacas(ids);

        if (butacas.size() > 0) {
            if (!hasDiferentesLocalizaciones(butacas)) {
                ButacaDTO butaca = butacas.get(0);
                Map<String, Map<Long, PreciosSesion>> preciosSesionPorLocalizacion =
                    preciosService.getPreciosSesionPorLocalizacion(butaca.getParSesion().getId(), userUID);
                Map<Long, PreciosSesion> preciosSesionMap =
                    preciosSesionPorLocalizacion.get(butaca.getParLocalizacion().getCodigo());
                butacasDAO.cambiaTipo(butacas, tipo, preciosSesionMap);
            }
            else {
                throw new ButacasDiferentesLocalizacionesException();
            }
        }
    }

    private boolean hasDiferentesLocalizaciones(List<ButacaDTO> butacas) {
        boolean diferentesLocalizaciones = false;
        LocalizacionDTO localizacionAnterior = null;
        for (ButacaDTO butaca : butacas) {
            if (localizacionAnterior == null) {
                localizacionAnterior = butaca.getParLocalizacion();
            }

            if (localizacionAnterior.getId() != butaca.getParLocalizacion().getId()) {
                diferentesLocalizaciones = true;
                break;
            }
        }

        return diferentesLocalizaciones;
    }

    public void cargarInformacionButacas(List<Butaca> butacas, String language) {
        for(Butaca butaca : butacas){
            Localizacion localizacion =
                Localizacion.localizacionDTOtoLocalizacion(localizacionesDAO.getLocalizacionByCodigo(butaca.getLocalizacion()));
            String nombreLocalizacion = localizacion.getNombreEs();
            if(language.equalsIgnoreCase("ca")) {
                nombreLocalizacion = localizacion.getNombreVa();
            }
            butaca.setLocalizacionNombre(nombreLocalizacion);
        }
    }

    public List<Butaca> getButacas(List<ButacaDTO> parButacas, String language) {
        List<Butaca> butacas = Butaca.butacasDTOToButacas(parButacas, false, language);
        for (Butaca butaca : butacas) {
            String nombreTarifa = tarifasDAO.getNombre(Integer.valueOf(butaca.getTipo()));
            butaca.setTipo(nombreTarifa);
        }
        return butacas;
    }

    public List<Long> getSesionesButacas(List<ButacaDTO> butacas) {
        return butacas.stream().map(ButacaDTO::getParSesion).map(SesionDTO::getId).distinct()
            .collect(Collectors.toList());
    }
}
