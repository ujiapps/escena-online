package es.uji.apps.par.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import es.uji.apps.par.auth.Role;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.exceptions.DominioNoExisteException;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.mails.MailEntrada;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.rest.BaseResource;
import es.uji.commons.web.template.HTMLTemplate;

@Service
public class VendedoresService {

    private static final Logger log = LoggerFactory.getLogger(VendedoresService.class);

    @Autowired
    UsersService usersService;

    @Autowired
    CineService cineService;

    @Autowired
    SalasService salasService;

    @Autowired
    ReportService reportService;

    @Autowired
    JavaHtmlMailService javaHtmlMailService;

    @Autowired
    Configuration configuration;

    @Transactional
    public Usuario addVendedor(
        Usuario user,
        Cine cine,
        Sala sala
    ) {
        user.setRole(Role.ADMIN);
        user.setTokenValidacion(UUID.randomUUID().toString());
        Usuario usuarioCreado = usersService.addUser(user);

        Cine cineCreado = cineService.addCine(cine);
        Sala salaCreada = salasService.addSala(sala, cineCreado);

        reportService.addReportsToCine(cineCreado);
        usersService.addSalaToUser(salaCreada, usuarioCreado);

        return usuarioCreado;
    }

    public void enviarEmailValidacion(
        String cineNombre,
        Usuario ususario,
        String urlAdmin,
        Locale locale
    ) {
        HTMLTemplate template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + "mail_validacion", locale, BaseResource.APP);

        String urlValidacion = String.format("%s/rest/validacion/%s", urlAdmin, ususario.getTokenValidacion());

        String titulo =
            String.format("%s %s", ResourceProperties.getProperty(locale, "mail.validacion.titulo"), cineNombre);

        String texto = null;
        try {
            MailEntrada mailEntrada = new MailEntrada();
            Map<String, Object> properties = new HashMap<>();
            properties.put("titulo", titulo);
            properties.put("cabecera", ResourceProperties.getProperty(locale, "mail.validacion.cabecera.html"));
            properties.put("mensaje", ResourceProperties.getProperty(locale, "mail.validacion.mensaje.html"));
            properties.put("validar", ResourceProperties.getProperty(locale, "mail.validacion.validar.html"));
            properties.put("src", urlValidacion);

            texto = mailEntrada.compose(template, properties);
        } catch (Exception e) {
            log.error("Error generando el mail de validación", e);
        }

        try {
            javaHtmlMailService
                .enviaMail(configuration.getMailDefaultSender(), ususario.getMail(), titulo, texto);
            usersService.actualizarExpiracionToken(ususario.getUsuario(),
                new Timestamp(System.currentTimeMillis() + (24 * 60 * 60 * 1000)));
        } catch (MessagingException e) {
            log.error("No se ha podido enviar el email de validación a " + ususario.getMail(), e);
        }
    }

    public boolean consultaSubdominioDisponible(String subdominio) {
        try {
            usersService.getUserByServerName(subdominio);
        } catch (DominioNoExisteException e) {
            return true;
        }
        return false;
    }
}
