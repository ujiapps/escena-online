package es.uji.apps.par.report;

import java.io.OutputStream;
import java.util.Locale;

import es.uji.apps.fopreports.serialization.ReportSerializationException;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.model.Cine;

public interface EntradaReportOnlineInterface extends EntradaReportInterface {
	EntradaReportOnlineInterface create(Locale locale, Configuration configuration);
	void setTitulo(String titulo);
	void setCif(String cif);
	void setPromotor(String promotor);
	void setNifPromotor(String nifPromotor);
	void serialize(OutputStream output) throws ReportSerializationException;
	void setUrlPublicidad(String urlPublicidad);
	void setUrlPortada(String urlPortada);
	boolean esAgrupada();
	void setTotalButacas(int totalButacas);
	void setNombreEntidad(String nombreEntidad);
	void setDireccion(String direccion);
    void setCodigoCine(String codigo);
	void setEmailCompra(String email);
	void setCine(Cine cine);
}
