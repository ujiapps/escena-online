package es.uji.apps.par.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

public class SecurityHeaderFilters implements Filter {
    private FilterConfig filterConfig;
    private String contentSecurityPolicy;

    @Context
    ServletContext ctx;

    @Override
    public void init(FilterConfig config) throws ServletException {
        filterConfig = config;
        ctx = config.getServletContext();

        contentSecurityPolicy = filterConfig.getInitParameter("contentSecurityPolicy");
    }

    @Override
    public void doFilter(
        ServletRequest servletRequest,
        ServletResponse servletResponse,
        FilterChain filterChain
    ) throws IOException, ServletException {
        HttpServletResponse clientResponse = (HttpServletResponse) servletResponse;

        clientResponse.setHeader("X-Frame-Options", "SAMEORIGIN");
        clientResponse.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
        if (contentSecurityPolicy != null) {
            clientResponse.setHeader("Content-Security-Policy",
                String.format(contentSecurityPolicy, servletRequest.getServerName()));
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
