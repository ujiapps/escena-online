package es.uji.apps.par.utils;

public class Environment {

    public static String getParHome(){
        return System.getProperty("par.home", "/etc/uji/par");
    }
}
