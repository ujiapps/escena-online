package es.uji.apps.par.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BUTACAS_CSV")
public class ButacaCSVDTO implements Serializable {
    @Id
    @Column(name = "ENTRADA_UUID")
    private String uuid;

    @Column(name = "SESION_ID")
    private long sesionId;

    @Column(name = "COMPRA_ID")
    private long compraId;

    @Column(name = "FILA")
    private String fila;

    @Column(name = "NUMERO")
    private String numero;

    @Column(name = "NOMBRE_ES")
    private String localizacionNombreEs;

    @Column(name = "NOMBRE_VA")
    private String localizacionNombreVa;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "APELLIDOS")
    private String apellidos;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PRESENTADA")
    private Date presentada;

    @Column(name = "NOMBRE_COMPLETO")
    private String nombreNominal;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getSesionId() {
        return sesionId;
    }

    public void setSesionId(long sesionId) {
        this.sesionId = sesionId;
    }

    public long getCompraId() {
        return compraId;
    }

    public void setCompraId(long compraId) {
        this.compraId = compraId;
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getPresentada() {
        return presentada;
    }

    public void setPresentada(Date presentada) {
        this.presentada = presentada;
    }

    public String getLocalizacionNombreEs() {
        return localizacionNombreEs;
    }

    public void setLocalizacionNombreEs(String localizacionNombreEs) {
        this.localizacionNombreEs = localizacionNombreEs;
    }

    public String getLocalizacionNombreVa() {
        return localizacionNombreVa;
    }

    public void setLocalizacionNombreVa(String localizacionNombreVa) {
        this.localizacionNombreVa = localizacionNombreVa;
    }

    public String getNombreNominal() {
        return nombreNominal;
    }

    public void setNombreNominal(String nombreNominal) {
        this.nombreNominal = nombreNominal;
    }
}