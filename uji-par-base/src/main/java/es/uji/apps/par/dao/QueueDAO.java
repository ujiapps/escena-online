package es.uji.apps.par.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.Query;

@Repository
public class QueueDAO extends BaseDAO {
    @Transactional
    public List getActiveSessions(
    ) {
        Query query = entityManager.createNativeQuery("select session_id from spring_session");

        return query.getResultList();
    }
}
