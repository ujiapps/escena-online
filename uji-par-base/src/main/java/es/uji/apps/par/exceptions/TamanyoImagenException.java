package es.uji.apps.par.exceptions;

public class TamanyoImagenException extends GeneralPARException
{
    public TamanyoImagenException()
    {
        super(TAMANYO_IMAGEN_ERROR_CODE, TAMANYO_IMAGEN_ERROR);
    }
}
