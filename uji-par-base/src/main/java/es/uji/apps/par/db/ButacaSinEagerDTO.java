package es.uji.apps.par.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


/**
 * The persistent class for the PAR_BUTACAS database table.
 * 
 */
@Entity
@Table(name="PAR_BUTACAS", uniqueConstraints={@UniqueConstraint(columnNames={"SESION_ID", "LOCALIZACION_ID", "FILA", "NUMERO", "ANULADA"})})
public class ButacaSinEagerDTO implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PAR_BUTACAS_ID_GENERATOR", sequenceName="HIBERNATE_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAR_BUTACAS_ID_GENERATOR")
	private long id;

	@Column(name="FILA")
	private String fila;

	@Column(name="NUMERO")
	private String numero;

	@Column(name="PRECIO")
	private BigDecimal precio;

    @Column(name = "PRECIO_SIN_COMISION")
    private BigDecimal precioSinComision;

    //bi-directional many-to-one association to SesionDTO
    @ManyToOne
    @JoinColumn(name="SESION_ID")
    private SesionDTO parSesion;

    //bi-directional many-to-one association to LocalizacionDTO
    @ManyToOne
    @JoinColumn(name="LOCALIZACION_ID")
    private LocalizacionDTO parLocalizacion;

    //bi-directional many-to-one association to CompraDTO
    @ManyToOne
    @JoinColumn(name="COMPRA_ID")
    private CompraSinEagerDTO parCompra;

    @Column(name="TIPO")
    private String tipo;

    @Column(name = "ANULADA")
    private Boolean anulada;

    @Column(name = "FECHA_ANULADA")
    private Date fechaAnulada;

    @Column(name = "PRESENTADA")
    private Date presentada;

    @Column(name = "ID_ENTRADA")
    private Integer idEntrada;

    @Column(name="NOMBRE_COMPLETO")
    private String nombreCompleto;

	public ButacaSinEagerDTO() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

    public String getFila()
    {
        return fila;
    }

    public void setFila(String fila)
    {
        this.fila = fila;
    }

    public String getNumero()
    {
        return numero;
    }

    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    public BigDecimal getPrecio()
    {
        return precio;
    }

    public void setPrecio(BigDecimal precio)
    {
        this.precio = precio;
    }

    public BigDecimal getPrecioSinComision() {
        return precioSinComision;
    }

    public void setPrecioSinComision(BigDecimal precioSinComision) {
        this.precioSinComision = precioSinComision;
    }

    public SesionDTO getParSesion()
    {
        return parSesion;
    }

    public void setParSesion(SesionDTO parSesion)
    {
        this.parSesion = parSesion;
    }
    
    public LocalizacionDTO getParLocalizacion()
    {
        return parLocalizacion;
    }

    public void setParLocalizacion(LocalizacionDTO parLocalizacion)
    {
        this.parLocalizacion = parLocalizacion;
    }    

    public CompraSinEagerDTO getParCompra()
    {
        return parCompra;
    }

    public void setParCompra(CompraSinEagerDTO parCompra)
    {
        this.parCompra = parCompra;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

	public Boolean getAnulada() {
		return anulada;
	}

	public void setAnulada(Boolean anulada) {
		this.anulada = anulada;
	}

    public Date getFechaAnulada() {
        return fechaAnulada;
    }

    public void setFechaAnulada(Date fechaAnulada) {
        this.fechaAnulada = fechaAnulada;
    }

    public Date getPresentada() {
        return presentada;
    }

    public void setPresentada(Date presentada) {
        this.presentada = presentada;
    }

    public Integer getIdEntrada() {
        return idEntrada;
    }

    public void setIdEntrada(Integer idEntrada) {
        this.idEntrada = idEntrada;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }
}