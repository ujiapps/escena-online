package es.uji.apps.par.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ComoNosConociste {
    private long id;
    private String motivo;

    public ComoNosConociste(
        long id,
        String motivo
    ) {
        this.id = id;
        this.motivo = motivo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
}