package es.uji.apps.par.report;

public class EntradaModelReport
{

	private String fila;
	private String numero;
	private String total;
	private String totalSinComision;
	private String gastosGestion;
	private String gastosGestionSinIVA;
	private String IVAGastosGestion;
	private String barcode;
	private String zona;
	private String tipo;
	private String sala;
	private String tipoEvento;
	private Boolean tarifaDefecto;
	private String cine;
	private Boolean showIVA;
	private String nombreCompleto;

	public EntradaModelReport()
	{

	}

	public String getFila()
	{
		return fila;
	}

	public void setFila(String fila)
	{
		this.fila = fila;
	}

	public String getNumero()
	{
		return numero;
	}

	public void setNumero(String numero)
	{
		this.numero = numero;
	}

	public String getTotal()
	{
		return total;
	}

	public void setTotal(String total)
	{
		this.total = total;
	}

	public String getBarcode()
	{
		return barcode;
	}

	public void setBarcode(String barcode)
	{
		this.barcode = barcode;
	}

	public String getZona()
	{
		return zona;
	}

	public void setZona(String zona)
	{
		this.zona = zona;
	}

	public String getTipo()
	{
		return tipo;
	}

	public void setTipo(String tipo)
	{
		this.tipo = tipo;
	}

	public Boolean getTarifaDefecto()
	{
		return tarifaDefecto;
	}

	public void setTarifaDefecto(Boolean tarifaDefecto)
	{
		this.tarifaDefecto = tarifaDefecto;
	}

	public String getCine() {
		return cine;
	}

	public void setCine(String cine) {
		this.cine = cine;
	}

	public String getSala()
	{
		return sala;
	}

	public void setSala(String sala)
	{
		this.sala = sala;
	}

	public Boolean getShowIVA() {
		return showIVA;
	}

	public void setShowIVA(Boolean showIVA) {
		this.showIVA = showIVA;
	}

	public String getTotalSinComision() {
		return totalSinComision;
	}

	public void setTotalSinComision(String totalSinComision) {
		this.totalSinComision = totalSinComision;
	}

	public String getGastosGestion() {
		return gastosGestion;
	}

	public void setGastosGestion(String gastosGestion) {
		this.gastosGestion = gastosGestion;
	}

	public String getGastosGestionSinIVA() {
		return gastosGestionSinIVA;
	}

	public void setGastosGestionSinIVA(String gastosGestionSinIVA) {
		this.gastosGestionSinIVA = gastosGestionSinIVA;
	}

	public String getIVAGastosGestion() {
		return IVAGastosGestion;
	}

	public void setIVAGastosGestion(String IVAGastosGestion) {
		this.IVAGastosGestion = IVAGastosGestion;
	}

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
}
