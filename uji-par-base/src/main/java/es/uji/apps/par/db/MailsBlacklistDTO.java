package es.uji.apps.par.db;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PAR_MAILS_BLACKLIST")
public class MailsBlacklistDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}