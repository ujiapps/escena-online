package es.uji.apps.par.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.utils.DateUtils;

@XmlRootElement
public class PreciosSesion
{

	private long id;
	private Localizacion localizacion;
	private Sesion sesion;
	private BigDecimal precio;
	private Tarifa tarifa;

	public PreciosSesion()
	{

	}

	public PreciosSesion(PreciosEditablesSesion preciosEditablesSesion)
	{
		this.id = preciosEditablesSesion.getId();
		this.localizacion = preciosEditablesSesion.getLocalizacion();
		this.tarifa = preciosEditablesSesion.getTarifa();

		Sesion sesion = preciosEditablesSesion.getSesion();
		if (sesion != null && sesion.getFechaFinVentaAnticipada() != null) {
			Date fechaFinVentaAnticipada = DateUtils.getDateWithHora(sesion.getFechaFinVentaAnticipada(), sesion.getHoraFinVentaAnticipada());
			Date now = new Date();
			if (preciosEditablesSesion.getPrecioAnticipado() != null && now.before(fechaFinVentaAnticipada)) {
				this.precio = preciosEditablesSesion.getPrecioAnticipado();
			}
			else {
				this.precio = preciosEditablesSesion.getPrecio();
			}
		}
		else {
			this.precio = preciosEditablesSesion.getPrecio();
		}
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Localizacion getLocalizacion()
	{
		return localizacion;
	}

	public void setLocalizacion(Localizacion localizacion)
	{
		this.localizacion = localizacion;
	}

	public BigDecimal getPrecio()
	{
		return precio != null ? precio.setScale(2, RoundingMode.HALF_UP) : precio;
	}

	public void setPrecio(BigDecimal precio)
	{
		this.precio = precio;
	}

	public Sesion getSesion()
	{
		return sesion;
	}

	public void setSesion(Sesion sesion)
	{
		this.sesion = sesion;
	}

	public Tarifa getTarifa()
	{
		return tarifa;
	}

	public void setTarifa(Tarifa tarifa)
	{
		this.tarifa = tarifa;
	}

	public String toStrButaca() {
		return String
			.format("{'localizacion': %s, 'precio': %s, 'tipo': %s}", localizacion.getCodigo(), precio, tarifa.getId());
	}
}
