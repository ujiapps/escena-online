package es.uji.apps.par.model;

public class LimitacionVenta {
    Integer limite;
    Boolean byEmail;

    public LimitacionVenta(
        Integer limite,
        Boolean byEmail
    ) {
        this.limite = limite;
        this.byEmail = byEmail;
    }

    public Integer getLimite() {
        return limite;
    }

    public Boolean getByEmail() {
        return byEmail;
    }

    public Boolean isByEmail() {
        return byEmail;
    }
}
