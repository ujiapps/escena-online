package es.uji.apps.par.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import es.uji.apps.par.dao.SalasDAO;
import es.uji.apps.par.dao.UsuariosDAO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.GeneralPARException;
import es.uji.apps.par.exceptions.UsuarioYaExisteException;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Usuario;

@Service
public class UsersService {
    @Autowired
    private UsuariosDAO usuariosDAO;

    @Autowired
    private SalasDAO salasDAO;

    @Autowired
    private CineService cineService;

    public List<Usuario> getUsuarios(
        String sortParameter,
        int start,
        int limit
    ) {
        return usuariosDAO.getUsers(sortParameter, start, limit);
    }

    public void removeUser(Integer id) {
        usuariosDAO.removeUser(id);
    }

    @Transactional
    public Usuario addUser(Usuario user) throws GeneralPARException {
        checkRequiredFields(user);

        if (usuariosDAO.userExists(user))
            throw new UsuarioYaExisteException();
        else
            return usuariosDAO.addUser(user);
    }

    private void checkRequiredFields(Usuario user) throws CampoRequeridoException {
        if (user.getMail() == null || user.getMail().isEmpty())
            throw new CampoRequeridoException("Mail");
        if (user.getNombre() == null || user.getNombre().isEmpty())
            throw new CampoRequeridoException("Nombre");
        if (user.getUsuario() == null || user.getUsuario().isEmpty())
            throw new CampoRequeridoException("Usuario");
    }

    public void updateUser(Usuario user) throws CampoRequeridoException {
        checkRequiredFields(user);
        usuariosDAO.updateUser(user);
    }

    public void updatePassword(
        String userUID,
        String password
    ) {
        usuariosDAO.updatePassword(userUID, password);
    }

    public void updateUrl(
        String userUID,
        String url
    ) {
        usuariosDAO.updateUrl(userUID, url);
    }

    public int getTotalUsuarios() {
        return usuariosDAO.getTotalUsuarios();
    }

    public Usuario getUserByServerName(String serverName) {
        return usuariosDAO.getUserByServerName(serverName);
    }

    public Cine getUserCineByServerName(String serverName) {
        return Cine.cineDTOToCine(usuariosDAO.getCineDTOByServerName(serverName), false);
    }

    public Cine getUserCineConImagenByServerName(String serverName) {
        return cineService.getCineConImagen(usuariosDAO.getCineDTOByServerName(serverName));
    }

    public Cine getUserCineByUserUID(String userUID) {
        return usuariosDAO.getUserCineByUserUID(userUID);
    }

    public Usuario getUserById(String userUID) {
        return usuariosDAO.getUserById(userUID);
    }

    public void addSalaToUser(
        Sala salaCreada,
        Usuario usuario
    ) {
        usuariosDAO.addSalaUsuario(salaCreada, usuario);
    }

    public List<Usuario> getUsersByCine(long idCine) {
        List<UsuarioDTO> usuariosDTO = usuariosDAO.getUsersByCine(idCine);
        return usuariosDTO.stream().map(Usuario::new).collect(Collectors.toList());
    }

    public Usuario getUserByTokenValidacion(String tokenValidacion) {
        UsuarioDTO usuarioDTO = usuariosDAO.getUsuarioByTokenValidacion(tokenValidacion);
        Usuario usuario = new Usuario(usuarioDTO);
        usuario.setTokenValidacion(usuarioDTO.getTokenValidacion());
        usuario.setExpiracionToken(usuarioDTO.getExpiracionToken());
        return usuario;
    }

    public void validarToken(
        String userUID,
        String tokenValidacion
    ) {
        usuariosDAO.validarToken(userUID, tokenValidacion);
    }

    public void actualizarExpiracionToken(
        String usuario,
        Timestamp timestamp
    ) {
        usuariosDAO.actualizarExpiracionToken(usuario, timestamp);
    }

    public boolean hasSalasNumeradas(String userUID) {
        return salasDAO.getSalas(userUID).stream().filter(Sala::isAsientosNumerados).findAny().isPresent();
    }
}
