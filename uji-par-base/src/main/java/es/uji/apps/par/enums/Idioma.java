package es.uji.apps.par.enums;

import java.util.Arrays;
import java.util.List;

public enum Idioma {
    CASTELLANO("1", Arrays.asList("S", "W"), "Castellano"),
    CATALAN("2", Arrays.asList("R", "V"), "Catalán"),
    EUSKERA("3", Arrays.asList("Q", "U"), "Euskera"),
    GALLEGO("4", Arrays.asList("P", "T"), "Gallego"),
    VALENCIANO("5", Arrays.asList("B", "Y"), "Valenciano"),
    INGLES("6", Arrays.asList(), "Inglés"),
    FRANCES("7", Arrays.asList(), "Francés"),
    ALEMAN("8", Arrays.asList(), "Alemán"),
    PORTUGUES("9", Arrays.asList(), "Portugués"),
    ITALIANO("A", Arrays.asList(), "Italiano"),
    OTRO("Z", Arrays.asList("Z", "X"), "Otros");

    private final String vo;
    private final List<String> vl;
    private final String label;

    Idioma(String vo, List<String> vl, String label) {
        this.vo = vo;
        this.vl = vl;
        this.label = label;
    }

    public static Idioma fromVo(String vo) {
        return Arrays.stream(Idioma.values()).filter(idioma -> idioma.vo.equals(vo)).findFirst().orElse(null);
    }

    public static Idioma fromVl(String vl) {
        return Arrays.stream(Idioma.values()).filter(idioma -> idioma.vl.contains(vl)).findFirst().orElse(null);
    }

    public String toString() {
        return this.label;
    }
}
