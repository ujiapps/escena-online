package es.uji.apps.par.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import es.uji.apps.par.dao.QueueDAO;

@Service
public class QueueService {
    @Autowired
    private QueueDAO queueDAO;

    public List<String> getActiveSessions(
    ) {
        return queueDAO.getActiveSessions();
    }
}
