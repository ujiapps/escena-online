package es.uji.apps.par.exceptions;

public class EntradasNoAnulablesDesdeEnlace extends Throwable {
    public EntradasNoAnulablesDesdeEnlace(long cineId, String cineCodigo) {
        super(String.format("El cine %s con código %s no tiene habilitada la opción de anulación de entradas y compras desde enlace", cineId, cineCodigo));
    }
}
