package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class TPVVisibleMinimoException extends GeneralPARException
{
    public TPVVisibleMinimoException()
    {
        super(TPV_VISIBLE_MINIMO_CODE, TPV_VISIBLE_MINIMO);
    }
}
