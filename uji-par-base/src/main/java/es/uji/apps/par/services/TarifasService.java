package es.uji.apps.par.services;

import com.google.common.base.Strings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.PersistenceException;

import es.uji.apps.par.dao.TarifasDAO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.TarifaConEventosException;
import es.uji.apps.par.exceptions.UsuarioObligatorioException;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Tarifa;

@Service
public class TarifasService {
    @Autowired
    protected TarifasDAO tarifasDAO;

    @Autowired
    private UsersService usersService;

    public List<Tarifa> getEditables(
        String sort,
        int start,
        int limit,
        String userUID
    ) {
        List<TarifaDTO> tarifasDTO = tarifasDAO.getAll(sort, start, limit, userUID, false);
        return tarifasDTO.stream().map(Tarifa::new).collect(Collectors.toList());
    }

    public long getTotalEditables(String userUID) {
        return tarifasDAO.getTotal(userUID, false);
    }

    public List<Tarifa> getAll(
        String sort,
        int start,
        int limit,
        String userUID
    ) {
        List<TarifaDTO> tarifasDTO = tarifasDAO.getAll(sort, start, limit, userUID);
        return tarifasDTO.stream().map(Tarifa::new).collect(Collectors.toList());
    }

    public long getTotal(String userUID) {
        return tarifasDAO.getTotal(userUID);
    }

    public Tarifa add(
        Tarifa tarifa,
        String userUID
    ) {
        if (userUID != null) {
            if (Strings.isNullOrEmpty(tarifa.getNombre())) {
                throw new CampoRequeridoException("Nombre");
            } else {
                Cine cine = usersService.getUserCineByUserUID(userUID);
                TarifaDTO tarifaDTO = Tarifa.toDTO(tarifa, cine);
                tarifaDTO = tarifasDAO.add(tarifaDTO);

                return Tarifa.tarifaDTOToTarifa(tarifaDTO);
            }
        } else {
            throw new UsuarioObligatorioException();
        }
    }

    public Tarifa update(
        Tarifa tarifa,
        String userUID
    ) {
        if (userUID != null) {
            if (Strings.isNullOrEmpty(tarifa.getNombre())) {
                throw new CampoRequeridoException("Nombre");
            } else {
                Cine cine = usersService.getUserCineByUserUID(userUID);
                TarifaDTO tarifaDTO = Tarifa.toDTO(tarifa, cine);
                tarifasDAO.update(tarifaDTO);

                return Tarifa.tarifaDTOToTarifa(tarifaDTO);
            }
        } else {
            throw new UsuarioObligatorioException();
        }
    }

    public void remove(long id) {
        try {
            if (!tarifasDAO.hasFutureEvents(id)) {
                tarifasDAO.remove(id);
            }
            else {
                throw new TarifaConEventosException();
            }
        } catch (PersistenceException e) {
            throw new TarifaConEventosException();
        }
    }
}
