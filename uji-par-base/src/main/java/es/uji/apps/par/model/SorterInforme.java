package es.uji.apps.par.model;

import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SorterInforme {
    private String id;
    private String nombre;
    private String nombreCA;
    private String nombreES;

    public SorterInforme() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCA() {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA) {
        this.nombreCA = nombreCA;
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public static List<SorterInforme> getDefaultSorters(String language) {
        SorterInforme sorterInforme = new SorterInforme();
        if (language.equals("es"))
            sorterInforme.setNombre("Por defecto");
        else
            sorterInforme.setNombre("Per defecte");

        return Arrays.asList(sorterInforme);
    }
}
