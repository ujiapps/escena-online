package es.uji.apps.par.services;

import com.google.common.base.Strings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.OutputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Locale;

import es.uji.apps.fopreports.serialization.ReportSerializationException;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.config.ConfigurationSelector;
import es.uji.apps.par.dao.ComprasDAO;
import es.uji.apps.par.dao.TarifasDAO;
import es.uji.apps.par.dao.UsuariosDAO;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.exceptions.CompraSinButacasException;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.EventoMultisesion;
import es.uji.apps.par.model.GastosGestion;
import es.uji.apps.par.model.Promotor;
import es.uji.apps.par.report.EntradaModelReport;
import es.uji.apps.par.report.EntradaReportFactory;
import es.uji.apps.par.report.EntradaReportInterface;
import es.uji.apps.par.report.EntradaReportOnlineInterface;
import es.uji.apps.par.report.EntradaReportTaquillaInterface;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.PrecioUtils;
import es.uji.apps.par.utils.ReportUtils;

@Service
public class EntradasService {
    public static final String BUTACAS_COMPRA = "butacasCompra";
    public static final String UUID_COMPRA = "uuidCompra";
    public static final String ID_SESION = "idSesion";
    public static final String BEAN_REPORT_SUFFIX = "Report";
    public static final String BEAN_REPORT_PREFIX = "Entrada";
    private static final Logger log = LoggerFactory.getLogger(EntradasService.class);

    @Autowired
    private EventosService eventosService;

    @Autowired
    protected ComprasDAO comprasDAO;

    @Autowired
    CineService cineService;

    @Autowired
    private UsuariosDAO usuariosDAO;

    @Autowired
    private TarifasDAO tarifasDAO;

    @Autowired
    protected Configuration configuration;

    @Autowired
    ConfigurationSelector configurationSelector;

    private static EntradaReportTaquillaInterface entradaTaquillaReport;
    private static EntradaReportOnlineInterface entradaOnlineReport;

    synchronized public void generaEntrada(
        String uuidCompra,
        OutputStream outputStream,
        String userUID,
        String urlPublic,
        String urlPieEntrada
    ) throws ReportSerializationException, IOException, SAXException {
        Instant start = Instant.now();
        CompraDTO compra = comprasDAO.getCompraByUuid(uuidCompra);
        EventoDTO evento = compra.getParSesion().getParEvento();
        if (evento.getIsabono() != null && evento.getIsabono()) {
            EntradaReportTaquillaInterface entrada = generaEntradaAbonoYRellena(compra, userUID, urlPublic);
            entrada.serialize(outputStream);
        } else {
            EntradaReportOnlineInterface entrada =
                generaEntradaOnlineYRellena(compra, userUID, urlPublic, urlPieEntrada);
            entrada.serialize(outputStream);
        }

        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        log.info(String.format("Tiempo generando el PDF de la entrada: %s seg.", timeElapsed / 1000));
    }

    public EntradaReportOnlineInterface generaEntradaOnlineYRellena(
        CompraDTO compra,
        String userUID,
        String urlPublic,
        String urlPieEntrada
    ) {
        EventoDTO evento = compra.getParSesion().getParEvento();
        try {
            String nombreEsTipoEvento = evento.getParTiposEvento().getNombreEs();
            String className = "";
            if (!Strings.isNullOrEmpty(nombreEsTipoEvento)) {
                className = "es.uji.apps.par.report." + BEAN_REPORT_PREFIX + evento.getParTiposEvento().getNombreEs()
                    + BEAN_REPORT_SUFFIX;
            } else {
                className = compra.getParSesion().getParSala().getClaseEntradaOnline();
            }
            entradaOnlineReport = EntradaReportFactory.newInstanceByClassName(className);
        } catch (Exception e) {
            String claseEntradaOnline = compra.getParSesion().getParSala().getClaseEntradaOnline();
            entradaOnlineReport = EntradaReportFactory.newInstanceOnline(claseEntradaOnline);
        }

        EntradaReportOnlineInterface entrada = entradaOnlineReport.create(getLocale(userUID), configuration);
        rellenaEntrada(compra, entrada, userUID, urlPublic, urlPieEntrada);
        return entrada;
    }

    synchronized public void generaEntradaTaquilla(
        String uuidCompra,
        OutputStream outputStream,
        String userUID,
        String urlPublic
    ) throws ReportSerializationException, SAXException, IOException {
        CompraDTO compra = comprasDAO.getCompraByUuid(uuidCompra);
        EntradaReportTaquillaInterface entrada = generaEntradaTaquillaYRellena(compra, userUID, urlPublic);
        entrada.serialize(outputStream);
    }

    public EntradaReportTaquillaInterface generaEntradaAbonoYRellena(
        CompraDTO compra,
        String userUID,
        String urlPublic
    ) throws SAXException, IOException {
        String reportClass = "es.uji.apps.par.report.EntradaAbonoReport";
        return generaEntradaTaquillaYRellena(reportClass, compra, userUID, urlPublic);
    }

    public EntradaReportTaquillaInterface generaEntradaTaquillaYRellena(
        CompraDTO compra,
        String userUID,
        String urlPublic
    ) throws SAXException, IOException {
        String reportClass = compra.getParSesion().getParSala().getClaseEntradaTaquilla();
        return generaEntradaTaquillaYRellena(reportClass, compra, userUID, urlPublic);
    }

    private EntradaReportTaquillaInterface generaEntradaTaquillaYRellena(
        String reportClass,
        CompraDTO compra,
        String userUID,
        String urlPublic
    ) throws SAXException, IOException {
        entradaTaquillaReport = EntradaReportFactory.newInstanceTaquilla(reportClass);

        EntradaReportTaquillaInterface entrada = entradaTaquillaReport.create(getLocale(userUID), configuration);

        if (rellenaEntradaTaquilla(compra, entrada, userUID, urlPublic)) {
            return entrada;
        } else {
            throw new CompraSinButacasException();
        }
    }

    public boolean compraHasButacas(String uuidCompra) {
        CompraDTO compra = comprasDAO.getCompraByUuid(uuidCompra);

        for (ButacaDTO butaca : compra.getParButacas()) {
            if (butaca.getAnulada() == null || butaca.getAnulada() == false) {
                return true;
            }
        }

        if (compra.getReciboPinpad() != null) {
            for (int i = 0; i < 2; i++) {
                return true;
            }
        }

        return false;
    }

    private boolean rellenaEntradaTaquilla(
        CompraDTO compra,
        EntradaReportTaquillaInterface entrada,
        String userUID,
        String urlPublic
    ) {
        String titulo;
        List<EventoMultisesion> peliculas = eventosService.getPeliculas(compra.getParSesion().getParEvento().getId());
        Locale locale = getLocale(userUID);
        titulo = getTitulo(compra, peliculas, locale);
        String fecha = DateUtils.dateToSpanishString(compra.getParSesion().getFechaCelebracion());
        String hora = DateUtils.dateToHourString(compra.getParSesion().getFechaCelebracion());
        String horaApertura = compra.getParSesion().getHoraApertura();

        entrada.setTitulo(titulo);
        entrada.setFecha(fecha);
        entrada.setHora(hora);
        entrada.setHoraApertura(horaApertura);
        entrada.setUrlPortada(
            urlPublic + "/rest/evento/" + compra.getParSesion().getParEvento().getId() + "/imagenEntrada");
        entrada.setNombreEntidad(compra.getParSesion().getParEvento().getParCine().getNombre());
        String direccionSala = compra.getParSesion().getParSala().getDireccion();
        if (direccionSala != null && direccionSala.trim().length() > 0) {
            entrada.setDireccion(direccionSala);
        } else {
            entrada.setDireccion(String
                .format("%s %s %s", compra.getParSesion().getParEvento().getParCine().getDireccion(),
                    compra.getParSesion().getParEvento().getParCine().getCp(),
                    compra.getParSesion().getParEvento().getParCine().getNombreMunicipio()));
        }
        entrada.setUrlCondiciones(compra.getParSesion().getParEvento().getParCine().getUrlPrivacidad());
        entrada.setTipoPago(compra.getTipoPago());

        entrada.setCif(compra.getParSesion().getParEvento().getParTpv().getCif());
        Promotor promotor = getPromotor(compra);
        entrada.setPromotor(promotor.getNombre());
        entrada.setNifPromotor(promotor.getNif());

        boolean compraConButacas = false;
        for (ButacaDTO butaca : compra.getParButacas()) {
            if (butaca.getAnulada() == null || butaca.getAnulada() == false) {
                EntradaModelReport entradaModelReport = new EntradaModelReport();
                rellenaButaca(entradaModelReport, compra, entrada, butaca, userUID, urlPublic);
                compraConButacas = true;
            }
        }

        if (compra.getReciboPinpad() != null) {
            for (int i = 0; i < 2; i++) {
                entrada.generaPaginasReciboPinpad(compra.getReciboPinpad());
                compraConButacas = true;
            }
        }

        return compraConButacas;
    }

    private void rellenaEntrada(
        CompraDTO compra,
        EntradaReportOnlineInterface entrada,
        String userUID,
        String urlPublic,
        String urlPieEntrada
    ) {
        rellenaEntrada(compra, null, entrada, userUID, urlPublic, urlPieEntrada);
    }

    protected void rellenaEntrada(
        CompraDTO compra,
        Long butacaId,
        EntradaReportOnlineInterface entrada,
        String userUID,
        String urlPublic,
        String urlPieEntrada
    ) throws NullPointerException {
        String titulo;
        List<EventoMultisesion> peliculas = eventosService.getPeliculas(compra.getParSesion().getParEvento().getId());
        Locale locale = getLocale(userUID);
        titulo = getTitulo(compra, peliculas, locale);
        String fecha = DateUtils.dateToSpanishString(compra.getParSesion().getFechaCelebracion());
        String hora = DateUtils.dateToHourString(compra.getParSesion().getFechaCelebracion());
        String horaApertura = compra.getParSesion().getHoraApertura();

        entrada.setTitulo(titulo);
        entrada.setFecha(fecha);
        entrada.setHora(hora);
        entrada.setHoraApertura(horaApertura);
        entrada.setUrlPortada(
            urlPublic + "/rest/evento/" + compra.getParSesion().getParEvento().getId() + "/imagenEntrada");

        if (compra.getParSesion().getParEvento().getImagenPubliSrc() != null) {
            entrada.setUrlPublicidad(
                urlPublic + "/rest/evento/" + compra.getParSesion().getParEvento().getId() + "/imagenPubliEntrada");
        } else {
            entrada.setUrlPublicidad(urlPieEntrada);
        }

        CineDTO cineDTO = compra.getParSesion().getParEvento().getParCine();
        Cine cineConImagen = cineService.getCineConImagen(cineDTO);
        entrada.setCine(cineConImagen);
        entrada.setCodigoCine(cineDTO.getCodigo());
        entrada.setEmailCompra(compra.getEmail());
        entrada.setNombreEntidad(cineDTO.getNombre());
        String direccionSala = compra.getParSesion().getParSala().getDireccion();
        if (direccionSala != null && direccionSala.trim().length() > 0) {
            entrada.setDireccion(direccionSala);
        } else {
            entrada.setDireccion(String
                .format("%s %s %s", cineDTO.getDireccion(),
                    cineDTO.getCp(),
                    cineDTO.getNombreMunicipio()));
        }

        entrada.setCif(compra.getParSesion().getParEvento().getParTpv().getCif());
        Promotor promotor = getPromotor(compra);
        entrada.setPromotor(promotor.getNombre());
        entrada.setNifPromotor(promotor.getNif());

        int totalButacas = 0;

        for (ButacaDTO butaca : compra.getParButacas()) {
            if (butacaId == null || Long.valueOf(butaca.getId()).equals(butacaId)) {
                if (entrada.esAgrupada()) {
                    totalButacas++;
                } else {
                    EntradaModelReport entradaModelReport = new EntradaModelReport();
                    rellenaButaca(entradaModelReport, compra, entrada, butaca, userUID, urlPublic);
                }
            }
        }
        if (entrada.esAgrupada()) {
            EntradaModelReport entradaModelReport = new EntradaModelReport();
            entrada.setTotalButacas(totalButacas);
            rellenaButaca(entradaModelReport, compra, entrada, compra.getParButacas().get(0), userUID, urlPublic);
        }
    }

    private Promotor getPromotor(CompraDTO compra) {
        String nombrePromotor = compra.getParSesion().getParEvento().getPromotor();
        String nifPromotor = compra.getParSesion().getParEvento().getNifPromotor();
        if (Strings.isNullOrEmpty(nombrePromotor) || Strings.isNullOrEmpty(nifPromotor)) {
            nombrePromotor = compra.getParSesion().getParEvento().getParCine().getEmpresa();
            nifPromotor = compra.getParSesion().getParEvento().getParCine().getCif();
        }
        Promotor promotor = new Promotor();
        promotor.setNombre(nombrePromotor);
        promotor.setNif(nifPromotor);
        return promotor;
    }

    private String getTitulo(
        CompraDTO compra,
        List<EventoMultisesion> peliculas,
        Locale locale
    ) {
        EventoDTO parEvento = compra.getParSesion().getParEvento();
        boolean catalan = locale.getLanguage().equalsIgnoreCase("ca");
        String titulo = catalan ? parEvento.getTituloVa() : parEvento.getTituloEs();
        if (peliculas.size() > 0) {
            titulo += ": ";
            for (EventoMultisesion pelicula : peliculas) {
                titulo += (catalan ? pelicula.getTituloVa() : pelicula.getTituloEs()) + ", ";
            }
            titulo = titulo.substring(0, titulo.length() - 2);
        }
        return titulo;
    }

    private void rellenaButaca(
        EntradaModelReport entradaModelReport,
        CompraDTO compra,
        EntradaReportInterface entrada,
        ButacaDTO butaca,
        String userUID,
        String urlPublic
    ) {
        TarifaDTO tarifaCompra = tarifasDAO.get(Integer.valueOf(butaca.getTipo()), userUID);
        Locale locale = getLocale(userUID);
        if (locale.getLanguage().equals("ca")) {
            entradaModelReport.setZona(butaca.getParLocalizacion().getNombreVa());
        } else {
            entradaModelReport.setZona(butaca.getParLocalizacion().getNombreEs());
        }
        entradaModelReport.setFila(butaca.getFila());
        entradaModelReport.setNumero(butaca.getNumero());
        entradaModelReport.setTotal(ReportUtils.formatEuros(butaca.getPrecio()));
        entradaModelReport.setTotalSinComision(ReportUtils.formatEuros(butaca.getPrecioSinComision()));

        GastosGestion gastosGestion = PrecioUtils.getGastosGestion(butaca.getPrecio(), butaca.getPrecioSinComision());
        entradaModelReport.setGastosGestion(ReportUtils.formatEuros(gastosGestion.getGastosGestion()));
        entradaModelReport.setGastosGestionSinIVA(ReportUtils.formatEuros(gastosGestion.getGastosGestionSinIVA()));
        entradaModelReport.setIVAGastosGestion(ReportUtils.formatEuros(gastosGestion.getIVAgastosGestion()));

        Cine cine = usuariosDAO.getUserCineByUserUID(userUID);

        if (configuration.isIdEntrada()) {
            entradaModelReport.setBarcode(compra.getUuid() + "-" + butaca.getIdEntrada());
        } else {
            entradaModelReport.setBarcode(compra.getUuid() + "-" + butaca.getId());
        }
        entradaModelReport.setCine(cine.getCodigo());
        entradaModelReport.setTipo(tarifaCompra.getNombre());
        entradaModelReport.setTarifaDefecto(
            (tarifaCompra.getIsAbono() != null && tarifaCompra.getIsAbono()) || (tarifaCompra.getDefecto() != null
                && tarifaCompra.getDefecto()));
        SesionDTO parSesion = butaca.getParSesion();
        entradaModelReport.setSala(parSesion.getParSala().getNombre());
        EventoDTO evento = parSesion.getParEvento();

        Boolean showIVA = cine.getShowIVA();
        if (showIVA != null) {
            entradaModelReport.setShowIVA(cine.getShowIVA());
            if (evento.getPorcentajeIva() != null && evento.getParTiposEvento().getExportarICAA() != null && evento.getParTiposEvento().getExportarICAA()) {
                entradaModelReport.setIVAGastosGestion(String.format("%s%%", evento.getPorcentajeIva().toString()));
            }
        } else {
            entradaModelReport.setShowIVA(configurationSelector.showIVA());
        }

        //TODO: evaluar si es interesante para cualqueir tipo de sesión y no hacerlo fuera
        String fecha = DateUtils.dateToSpanishString(parSesion.getFechaCelebracion());
        String hora = DateUtils.dateToHourString(parSesion.getFechaCelebracion());
        String horaApertura = parSesion.getHoraApertura();

        entrada.setFecha(fecha);
        entrada.setHora(hora);
        entrada.setHoraApertura(horaApertura);

        if (locale.getLanguage().equals("ca")) {
            entradaModelReport.setTipoEvento(evento.getParTiposEvento().getNombreVa());
        } else {
            entradaModelReport.setTipoEvento(evento.getParTiposEvento().getNombreEs());
        }

        if (evento.getEntradasNominales() != null && evento.getEntradasNominales()) {
            entradaModelReport.setNombreCompleto(butaca.getNombreCompleto());
        }
        entrada.generaPaginaButaca(entradaModelReport, urlPublic);
    }

    protected Locale getLocale(String userUID) {
        Cine cine = usuariosDAO.getUserCineByUserUID(userUID);
        String defaultLang = cine.getDefaultLang();
        if (defaultLang != null) {
            return new Locale(cine.getDefaultLang(), "ES");
        } else {
            return new Locale(configurationSelector.getIdiomaPorDefecto(), "ES");
        }
    }
}
