package es.uji.apps.par.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import es.uji.apps.par.db.QReportDTO;
import es.uji.apps.par.db.ReportDTO;
import es.uji.apps.par.model.Report;

@Repository
public class ReportsDAO extends BaseDAO {
    @Transactional
    public void addReports(List<Report> reports) {
        for (Report report : reports) {
            entityManager.persist(Report.reportToReportDTO(report));
        }
    }

    public List<ReportDTO> getReportsByCine(long idCine) {
        QReportDTO qReportDTO = QReportDTO.reportDTO;

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReportDTO).where(qReportDTO.parCine.id.eq(idCine)).list(qReportDTO);
    }
}
