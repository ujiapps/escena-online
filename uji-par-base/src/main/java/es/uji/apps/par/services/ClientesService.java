package es.uji.apps.par.services;

import com.mysema.query.Tuple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.par.dao.ClientesDAO;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.model.Cliente;

@Service
public class ClientesService {
    @Autowired
    private ClientesDAO clientesDAO;

    public List<Cliente> getClientes(
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID
    ) {
        List<Tuple> compras = clientesDAO.getClientes(sortParameter, start, limit, filter, userUID);

        List<Cliente> clientes = new ArrayList<Cliente>();
        for (Tuple compra : compras) {
            Cliente cliente = new Cliente();
            cliente.setId(compra.get(0, Long.class));
            cliente.setNombre(compra.get(1, String.class));
            cliente.setApellidos(compra.get(2, String.class));
            cliente.setDireccion(compra.get(3, String.class));
            cliente.setPoblacion(compra.get(4, String.class));
            cliente.setCp(compra.get(5, String.class));
            cliente.setProvincia(compra.get(6, String.class));
            cliente.setTelefono(compra.get(7, String.class));
            cliente.setEmail(compra.get(8, String.class));
            cliente.setComoNosConociste(compra.get(9, String.class));
            cliente.setFecha(compra.get(10, Timestamp.class));
            clientes.add(cliente);
        }

        return clientes;
    }

    public int getTotalClientes(
        ExtGridFilterList filter,
        String userUID
    ) {
        return clientesDAO.getTotalClientes(filter, userUID);
    }

    public List<String> getMails(
        String sortParameter,
        ExtGridFilterList filter,
        String userUID
    ) {
        List<Tuple> clientes = clientesDAO.getClientes(sortParameter, 0, Integer.MAX_VALUE, filter, userUID);
        return clientes.stream().map(c -> c.get(8, String.class)).collect(Collectors.toList());
    }

    public boolean removeInfoPeriodica(
        String email,
        String userUID
    ) {
        return clientesDAO.removeInfoPeriodica(email, userUID);
    }
}
