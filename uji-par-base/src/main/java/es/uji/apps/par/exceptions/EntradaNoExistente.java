package es.uji.apps.par.exceptions;

public class EntradaNoExistente extends Throwable {
    public EntradaNoExistente(
        String uuidCompra,
        Long butacaId
    ) {
        super(String.format("La entrada %s que está intentando anular de la compra %s no existe", butacaId, uuidCompra));
    }
}
