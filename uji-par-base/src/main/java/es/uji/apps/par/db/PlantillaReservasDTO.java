package es.uji.apps.par.db;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PAR_PLANTILLAS_RESERVAS")
public class PlantillaReservasDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "PAR_PLANTILLAS_RES_ID_GENERATOR", sequenceName = "HIBERNATE_SEQUENCE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAR_PLANTILLAS_RES_ID_GENERATOR")
    private long id;

    private String nombre;

    @OneToMany(mappedBy = "parPlantilla")
    private List<ButacasPlantillaReservasDTO> parButacasPlantillas;

    @ManyToOne
    @JoinColumn(name = "SALA_ID")
    private SalaDTO sala;

    public PlantillaReservasDTO() {
    }

    public PlantillaReservasDTO(long id) {
        this.id = id;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public SalaDTO getSala() {
        return sala;
    }

    public void setSala(SalaDTO sala) {
        this.sala = sala;
    }

    public List<ButacasPlantillaReservasDTO> getParButacasPlantillas() {
        return parButacasPlantillas;
    }

    public void setParButacasPlantillas(List<ButacasPlantillaReservasDTO> parButacasPlantillas) {
        this.parButacasPlantillas = parButacasPlantillas;
    }
}