package es.uji.apps.par.exceptions;

public class FormatoImagenException extends GeneralPARException
{
    public FormatoImagenException()
    {
        super(FORMATO_IMAGEN_ERROR_CODE, FORMATO_IMAGEN_ERROR);
    }
}
