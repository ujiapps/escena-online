package es.uji.apps.par.model;

import org.apache.log4j.Logger;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ButacaHTML {
    public static Logger log = Logger.getLogger(ButacaHTML.class);

    private String fila;
    private String numero;
    private String localizacion;

    public ButacaHTML() {
    }

    public ButacaHTML(
        String localizacion,
        String fila,
        String numero
    ) {
        this.localizacion = localizacion;
        this.fila = fila;
        this.numero = numero;
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLocalizacion() {
        return localizacion;
    }
}