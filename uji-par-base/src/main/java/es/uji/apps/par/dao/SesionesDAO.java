package es.uji.apps.par.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.PreciosSesionDTO;
import es.uji.apps.par.db.QButacaDTO;
import es.uji.apps.par.db.QCompraDTO;
import es.uji.apps.par.db.QEnvioDTO;
import es.uji.apps.par.db.QEnviosSesionDTO;
import es.uji.apps.par.db.QEventoDTO;
import es.uji.apps.par.db.QEventoMultisesionDTO;
import es.uji.apps.par.db.QOcupacionDTO;
import es.uji.apps.par.db.QPreciosPlantillaDTO;
import es.uji.apps.par.db.QPreciosSesionDTO;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSalasUsuarioDTO;
import es.uji.apps.par.db.QSesionDTO;
import es.uji.apps.par.db.QSesionDuracionDTO;
import es.uji.apps.par.db.QSesionSesionesDTO;
import es.uji.apps.par.db.QTarifaDTO;
import es.uji.apps.par.db.QTipoEventoDTO;
import es.uji.apps.par.db.QUsuarioDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.SesionFormatoIdiomaICAADTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.exceptions.EventoCambioFechaConCompras;
import es.uji.apps.par.exceptions.EventoConCompras;
import es.uji.apps.par.exceptions.IncidenciaNotFoundException;
import es.uji.apps.par.exceptions.SesionSinFormatoIdiomaIcaaException;
import es.uji.apps.par.ext.ExtGridFilter;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.ficheros.registros.RegistroPelicula;
import es.uji.apps.par.ficheros.registros.RegistroSesion;
import es.uji.apps.par.ficheros.registros.RegistroSesionPelicula;
import es.uji.apps.par.ficheros.registros.TipoIncidencia;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.model.Plantilla;
import es.uji.apps.par.model.PreciosEditablesSesion;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.Tarifa;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.Pair;
import es.uji.apps.par.utils.Utils;

@Repository
public class SesionesDAO extends BaseDAO {
    private static SimpleDateFormat HOUR_FORMAT = new SimpleDateFormat("HHmm");

    private QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
    private QSesionSesionesDTO qSesionSesionesDTO = QSesionSesionesDTO.sesionSesionesDTO;
    private QPreciosSesionDTO qPreciosSesionDTO = QPreciosSesionDTO.preciosSesionDTO;
    private QSesionDuracionDTO qSesionDuracionDTO = QSesionDuracionDTO.sesionDuracionDTO;

    @Autowired
    Configuration configuration;

    @Autowired
    private EventosDAO eventosDAO;

    @Transactional
    public List<SesionDTO> getSesiones(
        long eventoId,
        String sortParameter,
        int start,
        int limit,
        String userUID
    ) {
        return getSesiones(eventoId, false, sortParameter, start, limit, userUID);
    }

    @Transactional
    public List<SesionDTO> getSesionesActivas(
        long eventoId,
        String sortParameter,
        int start,
        int limit,
        String userUID
    ) {
        return getSesiones(eventoId, true, sortParameter, start, limit, userUID);
    }

    private List<SesionDTO> getSesiones(
        long eventoId,
        boolean activos,
        String sortParameter,
        int start,
        int limit,
        String userUID
    ) {
        List<SesionDTO> sesion;

        if (activos)
            sesion = getQuerySesionesActivas(eventoId, null, userUID, null).orderBy(getSort(qSesionDTO, sortParameter))
                .offset(start).limit(limit).list(qSesionDTO);
        else
            sesion = getQuerySesiones(eventoId, null, userUID, null).orderBy(getSort(qSesionDTO, sortParameter))
                .offset(start).limit(limit).list(qSesionDTO);

        return sesion;
    }

    @Transactional
    public List<SesionDTO> getSesionesPorRssId(
        String rssId,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qSesionDTO).leftJoin(qSesionDTO.parSala, qSalaDTO).fetch()
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID).and(qSesionDTO.parEvento.rssId.eq(rssId)))
            .list(qSesionDTO);
    }

    @Transactional
    public List<Tuple> getSesionesConButacasVendidas(
        long eventoId,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID
    ) {
        return getSesionesConButacasVendidas(eventoId, false, sortParameter, start, limit, filter, userUID);
    }

    @Transactional
    public List<Tuple> getSesionesActivasConButacasVendidas(
        long eventoId,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID
    ) {
        return getSesionesConButacasVendidas(eventoId, true, sortParameter, start, limit, filter, userUID);
    }

    private List<Tuple> getSesionesConButacasVendidas(
        long eventoId,
        boolean activas,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID
    ) {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QOcupacionDTO qOcupacionDTO = QOcupacionDTO.ocupacionDTO;

        List<Long> idsSesiones = getIdsSesiones(eventoId, activas, sortParameter, start, limit, filter, userUID);

        JPAQuery query;

        if (activas)
            query = getQuerySesionesActivas(eventoId, filter, userUID, idsSesiones);
        else
            query = getQuerySesiones(eventoId, filter, userUID, idsSesiones);

        JPASubQuery queryVendidas = new JPASubQuery();
        queryVendidas.from(qButacaDTO, qCompraDTO);
        queryVendidas.where(qSesionDTO.id.eq(qButacaDTO.parSesion.id).and(qCompraDTO.pagada.eq(true))
            .and(qButacaDTO.anulada.eq(false).or(qButacaDTO.anulada.isNull()))
            .and(qButacaDTO.parCompra.id.eq(qCompraDTO.id).and(qCompraDTO.reserva.eq(false))));

        JPASubQuery queryReservadas = new JPASubQuery();
        queryReservadas.from(qButacaDTO, qCompraDTO);
        queryReservadas.where(
            qSesionDTO.id.eq(qButacaDTO.parSesion.id).and(qButacaDTO.anulada.eq(false).or(qButacaDTO.anulada.isNull()))
                .and(qButacaDTO.parCompra.id.eq(qCompraDTO.id).and(qCompraDTO.reserva.eq(true))));

        JPASubQuery queryOcupacion = new JPASubQuery();
        queryOcupacion.from(qOcupacionDTO);
        queryOcupacion.where(qSesionDTO.id.eq(qOcupacionDTO.sesionId));

        List<Tuple> sesiones = query.orderBy(getSort(qSesionDTO, sortParameter))
            .list(qSesionDTO, queryVendidas.list(qButacaDTO).count(), queryReservadas.list(qButacaDTO).count(),
                queryOcupacion.unique(qOcupacionDTO.totalEntradas.sum()));

        return sesiones;
    }

    private List<Long> getIdsSesiones(
        long eventoId,
        boolean activas,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID
    ) {
        JPAQuery query;

        if (activas)
            query = getQuerySesionesActivas(eventoId, filter, userUID, null);
        else
            query = getQuerySesiones(eventoId, filter, userUID, null);

        return query.orderBy(getSort(qSesionDTO, sortParameter)).offset(start).limit(limit).list(qSesionDTO.id);
    }

    private JPAQuery getQuerySesionesActivas(
        long eventoId,
        ExtGridFilterList filter,
        String userUID,
        List<Long> idsSesiones
    ) {
        return getQuerySesiones(eventoId, filter, userUID, idsSesiones, true);
    }

    private JPAQuery getQuerySesiones(
        long eventoId,
        ExtGridFilterList filter,
        String userUID,
        List<Long> idsSesiones
    ) {
        return getQuerySesiones(eventoId, filter, userUID, idsSesiones, false);
    }

    private JPAQuery getQuerySesiones(
        long eventoId,
        ExtGridFilterList filter,
        String userUID,
        List<Long> idsSesiones,
        boolean activas
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;

        JPAQuery query = new JPAQuery(entityManager);

        BooleanBuilder where = new BooleanBuilder(
            qSalasUsuarioDTO.parUsuario.usuario.eq(userUID).and(qSesionDTO.parEvento.id.eq(eventoId)));
        if (activas) {
            Timestamp now = new Timestamp(configuration.dateConMargenTrasVenta().getTime());
            where = where.and(qSesionDTO.fechaCelebracion.after(now))
                .and(qSesionDTO.anulada.isNull().or(qSesionDTO.anulada.eq(false)));
        }

        return getQuerySesiones(filter, idsSesiones, qSalaDTO, qSalasUsuarioDTO, query, where);
    }

    private JPAQuery getQuerySesiones(
        ExtGridFilterList filter,
        List<Long> idsSesiones,
        QSalaDTO qSalaDTO,
        QSalasUsuarioDTO qSalasUsuarioDTO,
        JPAQuery query,
        BooleanBuilder where
    ) {
        where.and(getFilterWhere(filter));

        if (idsSesiones != null && idsSesiones.size() > 0) {
            return query.from(qSesionDTO).leftJoin(qSesionDTO.parSala, qSalaDTO).fetch()
                .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
                .where(where.and(Utils.getInSublistLimited(idsSesiones, qSesionDTO.id)));
        } else {
            return query.from(qSesionDTO).leftJoin(qSesionDTO.parSala, qSalaDTO)
                .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).where(where);
        }
    }

    private BooleanBuilder getFilterWhere(
        ExtGridFilterList filter
    ) {
        BooleanBuilder where = new BooleanBuilder();
        if (filter != null) {
            final ExtGridFilter fechaCelebracionFilter = filter.findFiltroByProperty("fechaCelebracion");
            if (fechaCelebracionFilter != null) {
                String fechaSinHora = fechaCelebracionFilter.getValue().toString().split("T")[0];
                String fechaInicial = String.format("%s 00:00:00", fechaSinHora);
                String fechaFinal = String.format("%s 23:59:59", fechaSinHora);
                where.and(qSesionDTO.fechaCelebracion.goe(Timestamp.valueOf(fechaInicial))
                    .and(qSesionDTO.fechaCelebracion.loe(Timestamp.valueOf(fechaFinal))));
            }

            final ExtGridFilter multisesionFilter = filter.findFiltroByProperty("multisesion");
            if (multisesionFilter != null) {
                where.and(qSesionDTO.multisesion.eq(Boolean.parseBoolean(multisesionFilter.getValue().toString())));
            }
        }
        return where;
    }

    @Transactional
    public List<SesionDTO> getSesionesPorSala(
        long eventoId,
        long salaId,
        String sortParameter,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<SesionDTO> sesiones = query.from(qSesionDTO).leftJoin(qSesionDTO.parSala, qSalaDTO).fetch()
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID)
                .and(qSesionDTO.parEvento.id.eq(eventoId).and(qSalaDTO.id.eq(salaId))))
            .orderBy(getSort(qSesionDTO, sortParameter)).list(qSesionDTO);

        return sesiones;
    }

    @Transactional
    public long anulaSesion(long id) {
        JPAUpdateClause jpaUpdate = new JPAUpdateClause(entityManager, qSesionDTO);
        return jpaUpdate.set(qSesionDTO.anulada, true).set(qSesionDTO.canalInternet, false).where(qSesionDTO.id.eq(id))
            .execute();
    }

    @Transactional
    public SesionDTO persistSesion(SesionDTO sesionDTO) {
        entityManager.persist(sesionDTO);
        return sesionDTO;
    }

    //TODO - aparentemente esta mal, ya que deberia ser con sesion y no con evento
    private void addSesionFormatoIdiomaICAA(
        String formato,
        String versionLinguistica,
        long eventoId
    ) {
        SesionFormatoIdiomaICAADTO sesionFormatoIdiomaICAA = new SesionFormatoIdiomaICAADTO();
        sesionFormatoIdiomaICAA.setFormato(formato);
        sesionFormatoIdiomaICAA.setVersionLinguistica(versionLinguistica);
        sesionFormatoIdiomaICAA.setParEvento(new EventoDTO(eventoId));
        entityManager.persist(sesionFormatoIdiomaICAA);
    }

    @Transactional(rollbackForClassName = {"EdicionSesionAnuladaException"})
    public SesionDTO updateSesion(
        SesionDTO sesionDTO,
        Sesion sesion,
        boolean hasCompras,
        boolean cambiaFechaConCompras
    ) {
        Timestamp fechaCelebracion = DateUtils
            .dateToTimestampSafe(DateUtils.addTimeToDate(sesion.getFechaCelebracion(), sesion.getHoraCelebracion()));

        if (sesionDTO.getParSala().getId() != sesion.getSala().getId() && hasCompras)
            throw new EventoConCompras(sesion.getEvento().getId());

        if (!sesionDTO.getFechaCelebracion().equals(fechaCelebracion) && hasCompras) {
            if (sesionDTO.getFechaCelebracion().before(new Date())) {
                throw new EventoConCompras(sesion.getEvento().getId());
            } else if (!cambiaFechaConCompras) {
                throw new EventoCambioFechaConCompras(sesion.getEvento().getId());
            }
        }

        if (sesion.getFechaInicioVentaOnline() != null)
            sesionDTO.setFechaInicioVentaOnline(DateUtils.dateToTimestampSafe(
                DateUtils.addTimeToDate(sesion.getFechaInicioVentaOnline(), sesion.getHoraInicioVentaOnline())));
        else
            sesionDTO.setFechaInicioVentaOnline(null);

        if (sesion.getFechaFinVentaOnline() != null)
            sesionDTO.setFechaFinVentaOnline(DateUtils.dateToTimestampSafe(
                DateUtils.addTimeToDate(sesion.getFechaFinVentaOnline(), sesion.getHoraFinVentaOnline())));
        else
            sesionDTO.setFechaFinVentaOnline(null);

        if (sesion.getFechaFinVentaAnticipada() != null)
            sesionDTO.setFechaFinVentaAnticipada(DateUtils.dateToTimestampSafe(
                DateUtils.addTimeToDate(sesion.getFechaFinVentaAnticipada(), sesion.getHoraFinVentaAnticipada())));
        else
            sesionDTO.setFechaFinVentaAnticipada(null);

        sesionDTO.setFechaCelebracion(fechaCelebracion);
        sesionDTO.setCanalInternet(sesion.getCanalInternet());
        sesionDTO.setCanalTaquilla(sesion.getCanalTaquilla());
        sesionDTO.setHoraApertura(sesion.getHoraApertura());
        sesionDTO.setParEvento(Evento.eventoToEventoDTO(sesion.getEvento()));
        sesionDTO.setParPlantilla(Plantilla.plantillaPreciosToPlantillaPreciosDTO(sesion.getPlantillaPrecios()));
        sesionDTO.setVersionLinguistica(sesion.getVersionLinguistica());
        sesionDTO.setRssId(sesion.getRssId());
        sesionDTO.setParSala(Sala.salaToSalaDTO(sesion.getSala()));
        entityManager.merge(sesionDTO);

        return sesionDTO;
    }

    @Transactional
    public boolean hasVentasDegradadas(
        long sesionId,
        Timestamp fechaCelebracion
    ) {
        Timestamp fechaTopeParaSaberSiEsDegradada = configuration.getDataTopePerASaberSiEsDegradada(fechaCelebracion);
        JPAQuery query = new JPAQuery(entityManager);
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

        query.from(qCompraDTO)
            .where(qCompraDTO.parSesion.id.eq(sesionId).and(qCompraDTO.fecha.gt(fechaTopeParaSaberSiEsDegradada)));
        return (query.count() > 0L);
    }

    @Transactional
    public PreciosEditablesSesion addPrecioSesion(PreciosSesionDTO precioSesionDTO) {
        PreciosEditablesSesion precioSesion = new PreciosEditablesSesion();
        precioSesion.setAulaTeatro(precioSesionDTO.getAulaTeatro());
        precioSesion.setDescuento(precioSesionDTO.getDescuento());
        precioSesion.setInvitacion(precioSesionDTO.getInvitacion());
        precioSesion.setPrecio(precioSesionDTO.getPrecio());
        precioSesion.setPrecioAnticipado(precioSesionDTO.getPrecioAnticipado());
        precioSesion.setTarifa(new Tarifa(precioSesionDTO.getParTarifa()));
        precioSesion.setLocalizacion(new Localizacion(precioSesionDTO.getParLocalizacione()));

        entityManager.persist(precioSesionDTO);
        precioSesion.setId(precioSesionDTO.getId());

        return precioSesion;
    }

    @Transactional
    public void deleteExistingPreciosSesion(long sesionId) {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qPreciosSesionDTO);
        delete.where(qPreciosSesionDTO.parSesione.id.eq(sesionId)).execute();
    }

    @Transactional
    public List<PreciosSesionDTO> getPreciosSesion(
        long sesionId,
        String sortParameter,
        int start,
        int limit
    ) {
        return getQueryPreciosSesion(sesionId).orderBy(getSort(qPreciosSesionDTO, sortParameter)).offset(start)
            .limit(limit).list(qPreciosSesionDTO);
    }

    private JPAQuery getQueryPreciosSesion(long sesionId) {
        QTarifaDTO qTarifa = QTarifaDTO.tarifaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPreciosSesionDTO, qTarifa)
            .where(qPreciosSesionDTO.parSesione.id.eq(sesionId).and(qTarifa.id.eq(qPreciosSesionDTO.parTarifa.id)));
    }

    @Transactional
    public SesionDTO getSesion(
        long sesionId,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qSesionDTO).leftJoin(qSesionDTO.parPreciosSesions, qPreciosSesionDTO).fetch()
            .leftJoin(qSesionDTO.parSala, qSalaDTO).fetch().join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID).and(qSesionDTO.id.eq(sesionId)))
            .uniqueResult(qSesionDTO);
    }

    @Transactional
    public int getTotalSesionesActivas(
        Long eventoId,
        ExtGridFilterList filter,
        String userUID
    ) {
        return (int) getQuerySesionesActivas(eventoId, filter, userUID, null).count();
    }

    @Transactional
    public int getTotalSesiones(
        Long eventoId,
        ExtGridFilterList filter,
        String userUID
    ) {
        return (int) getQuerySesiones(eventoId, filter, userUID, null).count();
    }

    @Transactional
    public int getTotalPreciosSesion(Long sesionId) {
        return (int) getQueryPreciosSesion(sesionId).count();
    }

    @Transactional(rollbackForClassName = {"IncidenciaNotFoundException"})
    public int getNumeroSesionesValidasParaFicheroICAA(List<Sesion> sesionesAValidar)
        throws IncidenciaNotFoundException {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;

        List<Long> idsSesionesAValidar = Sesion.getIdsSesiones(sesionesAValidar);
        JPAQuery query = new JPAQuery(entityManager);

        long numeroSesiones =
            query.from(qSesionDTO).where(qSesionDTO.id.in(idsSesionesAValidar).and(qSesionDTO.anulada.isFalse()))
                .distinct().count();

        return (int) numeroSesiones;
    }

    @Transactional(rollbackForClassName = {"IncidenciaNotFoundException"})
    public List<RegistroSesion> getRegistrosSesiones(List<Sesion> sesiones) throws IncidenciaNotFoundException {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;
        QEventoMultisesionDTO eventoMultisesionDTO = new QEventoMultisesionDTO("eventoMultisesionDTO");

        List<Long> idsSesiones = Sesion.getIdsSesiones(sesiones);
        JPAQuery query = new JPAQuery(entityManager);

        List<Tuple> resultado =
            query.from(qSesionDTO).join(qSesionDTO.parSala, qSalaDTO).leftJoin(qSesionDTO.parCompras, qCompraDTO)
                .on(qCompraDTO.anulada.isNull().or(qCompraDTO.anulada.eq(false)))
                .leftJoin(qCompraDTO.parButacas, qButacaDTO)
                .on(qButacaDTO.anulada.isNull().or(qButacaDTO.anulada.eq(false))).where(qSesionDTO.id.in(idsSesiones))
                .distinct()
                .groupBy(qSesionDTO.id, qSalaDTO.codigo, qSesionDTO.fechaCelebracion, qSesionDTO.incidenciaId)
                .list(qSesionDTO.id, qSalaDTO.codigo, qSesionDTO.fechaCelebracion, qButacaDTO.precio.sum(),
                    qSesionDTO.incidenciaId, new JPASubQuery().from(eventoMultisesionDTO)
                        .where(eventoMultisesionDTO.parEvento.id.eq(qSesionDTO.parEvento.id)).count());

        List<RegistroSesion> registros = new ArrayList<RegistroSesion>();

        for (Tuple row : resultado) {
            int idIncidencia = Utils.safeObjectToInt(row.get(4, BigDecimal.class));

            long idSesion = row.get(0, Long.class);
            String codigoSala = (String) row.get(1, String.class);
            Date fechaCelebracion = row.get(2, Date.class);
            BigDecimal recaudacion = row.get(3, BigDecimal.class);
            Long peliculasMultisesion = row.get(5, Long.class);
            Long espectadores = getEspectadores(idSesion);

            RegistroSesion registro = new RegistroSesion();

            registro.setCodigoSala(codigoSala);
            registro.setEspectadores(espectadores.intValue());
            registro.setPeliculas((peliculasMultisesion == 0L) ? 1 : peliculasMultisesion.intValue());
            registro.setFecha(fechaCelebracion);
            registro.setHora(HOUR_FORMAT.format(fechaCelebracion));
            registro.setIncidencia(TipoIncidencia.intToTipoIncidencia(idIncidencia));

            if (recaudacion == null)
                registro.setRecaudacion(BigDecimal.ZERO);
            else
                registro.setRecaudacion(recaudacion);

            registros.add(registro);
        }

        return registros;
    }

    private Long getEspectadores(long sesionId) {
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qButacaDTO).where(
            qButacaDTO.parSesion.id.eq(sesionId).and(qButacaDTO.anulada.eq(false).or(qButacaDTO.anulada.isNull())))
            .count();
    }

    @Transactional(rollbackForClassName = {"SesionSinFormatoIdiomaIcaaException", "IncidenciaNotFoundException"})
    public List<RegistroSesionPelicula> getRegistrosSesionesPeliculas(List<Sesion> sesiones)
        throws SesionSinFormatoIdiomaIcaaException, IncidenciaNotFoundException {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;

        List<Long> idsSesiones = Sesion.getIdsSesiones(sesiones);

        JPAQuery query = new JPAQuery(entityManager);

        List<SesionDTO> resultado =
            query.from(qSesionDTO).join(qSesionDTO.parEvento, qEventoDTO).join(qSesionDTO.parSala).fetch()
                .where(qSesionDTO.id.in(idsSesiones)).orderBy(qSesionDTO.fechaCelebracion.asc()).distinct()
                .list(qSesionDTO);

        List<RegistroSesionPelicula> registros = new ArrayList<RegistroSesionPelicula>();

        for (SesionDTO sesionDTO : resultado) {
            if (sesionDTO.getParEvento().getFormato() == null)
                throw new SesionSinFormatoIdiomaIcaaException(sesionDTO.getParEvento().getTituloVa());

            List<Tuple> peliculasMultisesion = eventosDAO.getPeliculasMultisesion(sesionDTO.getParEvento().getId());

            if (peliculasMultisesion.size() > 0) {
                for (Tuple peliculaMultisesion : peliculasMultisesion) {
                    EventoDTO eventoDTO = peliculaMultisesion.get(0, EventoDTO.class);
                    String versionLinguistica = peliculaMultisesion.get(1, String.class);

                    try {
                        Sesion.checkSesionValoresIcaa(eventoDTO.getFormato(), eventoDTO.getId(), versionLinguistica);
                    } catch (Exception e) {
                        throw new SesionSinFormatoIdiomaIcaaException(sesionDTO.getParEvento().getId(),
                            sesionDTO.getParEvento().getFormato(), versionLinguistica);
                    }

                    RegistroSesionPelicula registro = new RegistroSesionPelicula();
                    registro.setCodigoSala(sesionDTO.getParSala().getCodigo());
                    registro.setCodigoPelicula((int) eventoDTO.getId(), versionLinguistica);
                    registro.setFecha(sesionDTO.getFechaCelebracion());
                    registro.setHora(HOUR_FORMAT.format(sesionDTO.getFechaCelebracion()));
                    registros.add(registro);
                }
            } else {
                try {
                    Sesion
                        .checkSesionValoresIcaa(sesionDTO.getParEvento().getFormato(), sesionDTO.getParEvento().getId(),
                            sesionDTO.getVersionLinguistica());
                } catch (Exception e) {
                    throw new SesionSinFormatoIdiomaIcaaException(sesionDTO.getParEvento().getId(),
                        sesionDTO.getParEvento().getFormato(), sesionDTO.getVersionLinguistica());
                }

                RegistroSesionPelicula registro = new RegistroSesionPelicula();
                registro.setCodigoSala(sesionDTO.getParSala().getCodigo());
                registro.setCodigoPelicula((int) sesionDTO.getParEvento().getId(), sesionDTO.getVersionLinguistica());
                registro.setFecha(sesionDTO.getFechaCelebracion());
                registro.setHora(HOUR_FORMAT.format(sesionDTO.getFechaCelebracion()));
                registros.add(registro);
            }
        }

        return registros;
    }

    @Transactional(rollbackForClassName = {"IncidenciaNotFoundException"})
    public List<RegistroPelicula> getRegistrosPeliculas(List<Sesion> sesiones) throws IncidenciaNotFoundException {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;

        List<Long> idsSesiones = Sesion.getIdsSesiones(sesiones);

        JPAQuery query = new JPAQuery(entityManager);

        List<Tuple> resultado =
            query.from(qSesionDTO).join(qSesionDTO.parEvento, qEventoDTO).join(qSesionDTO.parSala).fetch()
                .where(qSesionDTO.id.in(idsSesiones)).distinct().list(qSesionDTO, qEventoDTO.id);

        List<RegistroPelicula> registros = new ArrayList<RegistroPelicula>();

        for (Tuple row : resultado) {
            SesionDTO sesion = row.get(0, SesionDTO.class);
            Long idEvento = row.get(1, Long.class);

            List<Tuple> peliculasMultisesion = eventosDAO.getPeliculasMultisesion(idEvento);
            if (peliculasMultisesion.size() > 0) {
                for (Tuple peliculaMultisesion : peliculasMultisesion) {
                    EventoDTO eventoDTO = peliculaMultisesion.get(0, EventoDTO.class);
                    String versionLinguistica = peliculaMultisesion.get(1, String.class);

                    RegistroPelicula registro = new RegistroPelicula((int) eventoDTO.getId(), versionLinguistica);
                    registro.setCodigoSala(sesion.getParSala().getCodigo());
                    registro.setCodigoExpediente(eventoDTO.getExpediente());
                    registro.setTitulo(Utils.stripAccents(eventoDTO.getTituloEs()));
                    registro.setCodigoDistribuidora(eventoDTO.getCodigoDistribuidora());
                    registro.setNombreDistribuidora(Utils.stripAccents(eventoDTO.getNombreDistribuidora()));
                    registro.setVersionOriginal(eventoDTO.getVo());
                    registro.setIdiomaSubtitulos(eventoDTO.getSubtitulos());
                    registro.setFormatoProyeccion(eventoDTO.getFormato());
                    registros.add(registro);
                }
            } else {
                RegistroPelicula registro = new RegistroPelicula(idEvento.intValue(), sesion.getVersionLinguistica());
                registro.setCodigoSala(sesion.getParSala().getCodigo());
                registro.setCodigoExpediente(sesion.getParEvento().getExpediente());
                registro.setTitulo(Utils.stripAccents(sesion.getParEvento().getTituloEs()));
                registro.setCodigoDistribuidora(sesion.getParEvento().getCodigoDistribuidora());
                registro.setNombreDistribuidora(Utils.stripAccents(sesion.getParEvento().getNombreDistribuidora()));
                registro.setVersionOriginal(sesion.getParEvento().getVo());
                registro.setIdiomaSubtitulos(sesion.getParEvento().getSubtitulos());
                registro.setFormatoProyeccion(sesion.getParEvento().getFormato());
                registros.add(registro);
            }
        }

        return registros;
    }

    @Transactional
    public List<SesionDTO> getSesionesOrdenadas(
        List<Sesion> sesiones,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;

        List<Long> idsSesiones = Sesion.getIdsSesiones(sesiones);

        JPAQuery query = new JPAQuery(entityManager);

        List<SesionDTO> resultado = query.from(qSesionDTO).join(qSesionDTO.parSala, qSalaDTO).fetch()
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID).and(qSesionDTO.id.in(idsSesiones)))
            .orderBy(qSalaDTO.id.asc(), qSesionDTO.fechaCelebracion.asc()).list(qSesionDTO);

        return resultado;
    }

    @Transactional
    public Sesion getSesionByRssId(
        String rssId,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;

        JPAQuery query = new JPAQuery(entityManager);

        SesionDTO uniqueResult = query.from(QSesionDTO.sesionDTO).leftJoin(qSesionDTO.parSala, qSalaDTO)
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID).and(QSesionDTO.sesionDTO.rssId.eq(rssId)))
            .uniqueResult(QSesionDTO.sesionDTO);

        if (uniqueResult == null)
            return null;
        else
            return Sesion.SesionDTOToSesion(uniqueResult);
    }

    @Transactional
    public List<SesionDTO> getSesionesCinePorFechas(
        Date dtInicio,
        Date dtFin,
        boolean icaa,
        String sort,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
        QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qSesionDTO).join(qSesionDTO.parEvento, qEventoDTO).join(qSesionDTO.parSala, qSalaDTO).fetch()
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).join(qSalasUsuarioDTO.parUsuario, qUsuarioDTO);

        if (dtInicio != null || dtFin != null) {
            BooleanBuilder condicion = new BooleanBuilder();

            if (icaa) {
                condicion = condicion.and(qEventoDTO.parTiposEvento.exportarICAA.isTrue());
            }

            if (dtInicio != null)
                condicion = condicion.and(qSesionDTO.fechaCelebracion.goe(new Timestamp(dtInicio.getTime())));

            if (dtFin != null)
                condicion = condicion.and(qSesionDTO.fechaCelebracion.loe(new Timestamp(dtFin.getTime())));

            query.where(condicion.and(qUsuarioDTO.usuario.eq(userUID)));
        } else
            query.where(qUsuarioDTO.usuario.eq(userUID));

        return query.orderBy(getSort(qSesionDTO, sort)).distinct().list(qSesionDTO);
    }

    @Transactional
    public List<SesionDTO> getSesiones(
        List<Long> ids,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
        QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qSesionDTO).join(qSesionDTO.parEvento, qEventoDTO).leftJoin(qSesionDTO.parSala, qSalaDTO)
            .fetch().join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID).and(qSesionDTO.id.in(ids))).list(qSesionDTO);
    }

    @Transactional
    public List<SesionDTO> getSesionesICAAPorFechas(
        Date dtInicio,
        Date dtFin,
        String sort,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
        QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;
        QEnviosSesionDTO qEnviosSesion = QEnviosSesionDTO.enviosSesionDTO;
        QEnvioDTO qEnvioDTO = QEnvioDTO.envioDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qSesionDTO).join(qSesionDTO.parEvento, qEventoDTO).leftJoin(qSesionDTO.parSala, qSalaDTO).fetch()
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).leftJoin(qSesionDTO.parEnviosSesion, qEnviosSesion)
            .fetch().leftJoin(qEnviosSesion.parEnvio, qEnvioDTO).fetch();
        BooleanExpression condicion =
            qSalasUsuarioDTO.parUsuario.usuario.eq(userUID).and(qEventoDTO.parTiposEvento.exportarICAA.eq(true));

        if (dtInicio != null)
            condicion = condicion.and(qSesionDTO.fechaCelebracion.goe(new Timestamp(dtInicio.getTime())));

        if (dtFin != null)
            condicion = condicion.and(qSesionDTO.fechaCelebracion.loe(new Timestamp(dtFin.getTime())));

        query.where(condicion.and(qSesionDTO.anulada.isFalse()));

        return query.orderBy(getSort(qSesionDTO, sort)).distinct().list(qSesionDTO);
    }

    @Transactional
    public List<SesionDTO> getSesionesPorFechas(
        Date dtInicio,
        Date dtFin,
        String sort,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
        QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qSesionDTO).join(qSesionDTO.parEvento, qEventoDTO).leftJoin(qSesionDTO.parSala, qSalaDTO).fetch()
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO);

        BooleanBuilder condicion = new BooleanBuilder(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID));
        if (dtInicio != null)
            condicion = condicion.and(qSesionDTO.fechaCelebracion.goe(new Timestamp(dtInicio.getTime())));

        if (dtFin != null)
            condicion = condicion.and(qSesionDTO.fechaCelebracion.loe(new Timestamp(dtFin.getTime())));

        query.where(condicion);

        return query.orderBy(getSort(qSesionDTO, sort)).list(qSesionDTO);
    }

    @Transactional
    public List<TarifaDTO> getTarifasPreciosSesion(
        Long sesionId,
        Long localizacionId
    ) {
        QTarifaDTO qTarifa = QTarifaDTO.tarifaDTO;

        BooleanExpression where =
            qPreciosSesionDTO.parSesione.id.eq(sesionId).and(qTarifa.id.eq(qPreciosSesionDTO.parTarifa.id));

        if (localizacionId != null) {
            where = where.and(qPreciosSesionDTO.parLocalizacione.id.eq(localizacionId));
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPreciosSesionDTO, qTarifa).where(where).distinct().list(qTarifa);
    }

    @Transactional
    public List<TarifaDTO> getTarifasPreciosPlantilla(
        Long sesionId,
        Long localizacionId
    ) {
        QTarifaDTO qTarifa = QTarifaDTO.tarifaDTO;
        QPreciosPlantillaDTO qPreciosPlantilla = QPreciosPlantillaDTO.preciosPlantillaDTO;

        BooleanExpression where = qPreciosPlantilla.parPlantilla.id
            .eq(new JPASubQuery().from(qSesionDTO).where(qSesionDTO.id.eq(sesionId)).unique(qSesionDTO.parPlantilla.id))
            .and(qTarifa.id.eq(qPreciosPlantilla.parTarifa.id));

        if (localizacionId != null) {
            where = where.and(qPreciosPlantilla.parLocalizacione.id.eq(localizacionId));
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPreciosPlantilla, qTarifa).where(where).distinct().list(qTarifa);
    }

    @Transactional
    public void setIncidencia(
        long sesionId,
        int incidenciaId
    ) {
        JPAUpdateClause jpaUpdate = new JPAUpdateClause(entityManager, qSesionDTO);
        jpaUpdate.set(qSesionDTO.incidenciaId, incidenciaId).where(qSesionDTO.id.eq(sesionId)).execute();
    }

    @Transactional
    public long getButacasAnuladasTotal(long idSesion) {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qSesionDTO).join(qSesionDTO.parCompras, qCompraDTO).join(qCompraDTO.parButacas, qButacaDTO)
            .on(qButacaDTO.anulada.eq(true)).where(qSesionDTO.id.eq(idSesion)).count();
    }

    @Transactional(rollbackFor = IncidenciaNotFoundException.class)
    public int getTipoIncidenciaSesion(
        long totalAnuladas,
        boolean isVentaDegradada,
        boolean isSesionReprogramada
    ) throws IncidenciaNotFoundException {
        int tipoIncidenciaId = TipoIncidencia.tipoIncidenciaToInt(TipoIncidencia.SIN_INCIDENCIAS);

        if (totalAnuladas > 0) {
            if (!isVentaDegradada && !isSesionReprogramada)
                tipoIncidenciaId = TipoIncidencia.tipoIncidenciaToInt(TipoIncidencia.ANULACIO_VENDES);
            else if (isVentaDegradada && !isSesionReprogramada)
                tipoIncidenciaId = TipoIncidencia.tipoIncidenciaToInt(TipoIncidencia.VENDA_MANUAL_I_ANULACIO_VENTES);
            else if (isVentaDegradada && isSesionReprogramada)
                tipoIncidenciaId =
                    TipoIncidencia.tipoIncidenciaToInt(TipoIncidencia.VENDA_MANUAL_I_ANULACIO_VENTES_I_REPROGRAMACIO);
            else if (!isVentaDegradada && isSesionReprogramada)
                tipoIncidenciaId = TipoIncidencia.tipoIncidenciaToInt(TipoIncidencia.ANULACIO_VENTES_I_REPROGRAMACIO);
        } else if (isVentaDegradada) {
            if (isSesionReprogramada)
                tipoIncidenciaId = TipoIncidencia.tipoIncidenciaToInt(TipoIncidencia.VENDA_MANUAL_I_REPROGRAMACIO);
            else
                tipoIncidenciaId = TipoIncidencia.tipoIncidenciaToInt(TipoIncidencia.VENDA_MANUAL_DEGRADADA);
        } else if (isSesionReprogramada)
            tipoIncidenciaId = TipoIncidencia.tipoIncidenciaToInt(TipoIncidencia.REPROGRAMACIO);

        return tipoIncidenciaId;
    }

    @Transactional
    public Pair getCantidadSesionesMismaFechaYLocalizacion(
        Timestamp fechaCelebracion,
        long salaId,
        Long sesionId,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery queryCompras = new JPAQuery(entityManager);

        Pair result;
        if (sesionId != null) {
            result = new Pair(query.from(qSesionDTO).leftJoin(qSesionDTO.parSala, qSalaDTO)
                .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID)
                    .and(qSesionDTO.fechaCelebracion.eq(fechaCelebracion).and(qSesionDTO.parSala.id.eq(salaId))
                        .and(qSesionDTO.id.ne(sesionId).and(qSesionDTO.anulada.ne(true))))).count(),
                queryCompras.from(qSesionDTO).leftJoin(qSesionDTO.parSala, qSalaDTO)
                    .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).join(qSesionDTO.parCompras, qCompraDTO)
                    .join(qCompraDTO.parButacas, qButacaDTO).on(qButacaDTO.anulada.ne(true)).where(
                    qSalasUsuarioDTO.parUsuario.usuario.eq(userUID).and(qSesionDTO.id.eq(sesionId).and(
                        qSesionDTO.anulada.ne(true).and(
                            qSesionDTO.fechaCelebracion.eq(fechaCelebracion).and(qSesionDTO.parSala.id.eq(salaId))
                                .and(qSesionDTO.id.ne(sesionId)))))).count());
        } else {
            result = new Pair(query.from(qSesionDTO).leftJoin(qSesionDTO.parSala, qSalaDTO)
                .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID)
                    .and(qSesionDTO.fechaCelebracion.eq(fechaCelebracion)
                        .and(qSesionDTO.parSala.id.eq(salaId).and(qSesionDTO.anulada.ne(true))))).count(),
                queryCompras.from(qSesionDTO).leftJoin(qSesionDTO.parSala, qSalaDTO)
                    .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).join(qSesionDTO.parCompras, qCompraDTO)
                    .join(qCompraDTO.parButacas, qButacaDTO).on(qButacaDTO.anulada.ne(true)).where(
                    qSalasUsuarioDTO.parUsuario.usuario.eq(userUID).and(qSesionDTO.fechaCelebracion.eq(fechaCelebracion)
                        .and(qSesionDTO.parSala.id.eq(salaId).and(qSesionDTO.anulada.ne(true))))).count());
        }

        return result;
    }

    @Transactional
    public boolean isSesionReprogramada(
        Timestamp fechaCelebracion,
        long salaId,
        Long sesionId,
        String userUID
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qSesionDTO).leftJoin(qSesionDTO.parSala, qSalaDTO)
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID).and(
                qSesionDTO.fechaCelebracion.eq(fechaCelebracion).and(qSesionDTO.parSala.id.eq(salaId))
                    .and(qSesionDTO.id.ne(sesionId)))).count() > 0;
    }

    @Transactional
    public List<SesionDTO> getSesionesMismaFechaYLocalizacion(
        Timestamp fechaCelebracion,
        int duracion,
        long salaId
    ) {
        JPAQuery query = new JPAQuery(entityManager);

        Timestamp fechaCelebracionInicio = new Timestamp(fechaCelebracion.getTime());
        Timestamp fechaCelebracionFin = new Timestamp(fechaCelebracion.getTime() + TimeUnit.MINUTES.toMillis(duracion));

        List<Long> sesionIds = query.from(qSesionDuracionDTO).where(
            (qSesionDuracionDTO.fechaInicio.between(fechaCelebracionInicio, fechaCelebracionFin)
                .or(qSesionDuracionDTO.fechaFin.between(fechaCelebracionInicio, fechaCelebracionFin)))
                .and(qSesionDuracionDTO.salaId.eq(salaId))).list(qSesionDuracionDTO.id);

        if (sesionIds.size() > 0) {
            return query.from(qSesionDTO).where(qSesionDTO.id.in(sesionIds)).list(qSesionDTO);
        } else {
            return new ArrayList<>();
        }
    }

    @Transactional
    public void removeAnulacionVentasFromSesion(
        long sesionId,
        String userUID
    ) {
        SesionDTO sesionDTO = getSesion(sesionId, userUID);
        sesionDTO
            .setIncidenciaId(TipoIncidencia.removeAnulacionVentasFromIncidenciaActual(sesionDTO.getIncidenciaId()));
        entityManager.merge(sesionDTO);
    }

    @Transactional
    public List<SesionDTO> getMultiplesSesionesBySesionId(Long sesionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qSesionSesionesDTO).where(qSesionSesionesDTO.sesionMultiple.id.eq(sesionId))
            .list(qSesionSesionesDTO.sesion);
    }

    @Transactional
    public List<Long> getSesionesIdsBySesionId(Long sesionId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<Long> list = query.from(qSesionSesionesDTO).where(qSesionSesionesDTO.sesionMultiple.id.eq(sesionId))
            .list(qSesionSesionesDTO.sesion.id);
        if (list.size() > 0) {
            return list;
        }
        else {
            return Collections.singletonList(sesionId);
        }
    }

    @Transactional
    public List<Long> getIdsSesionesWhereSesionIdInMultiplesSesiones(long sesionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qSesionSesionesDTO).where(qSesionSesionesDTO.sesion.id.eq(sesionId)).distinct()
            .list(qSesionSesionesDTO.sesionMultiple.id);
    }

    public List<SesionDTO> getSesionesICAABySalaBetweenFechas(
        long salaId,
        Date startDate,
        Date endDate
    ) {
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;
        QTipoEventoDTO qTipoEventoDTO = QTipoEventoDTO.tipoEventoDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qSesionDTO).join(qSesionDTO.parEvento, qEventoDTO)
            .join(qEventoDTO.parTiposEvento, qTipoEventoDTO).leftJoin(qSesionDTO.parSala, qSalaDTO).where(
                qTipoEventoDTO.exportarICAA.isTrue().and(qSalaDTO.id.eq(salaId)).and(qSesionDTO.anulada.isFalse()).and(
                    qSesionDTO.fechaCelebracion
                        .between(new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()))))
            .list(qSesionDTO);
    }
}
