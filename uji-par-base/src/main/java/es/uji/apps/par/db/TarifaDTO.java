package es.uji.apps.par.db;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the PAR_TARIFAS database table.
 * 
 */
@Entity
@Table(name="PAR_TARIFAS", uniqueConstraints = {@UniqueConstraint(columnNames = {"NOMBRE", "CINE_ID"})})
public class TarifaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PAR_TARIFAS_ID_GENERATOR", sequenceName="HIBERNATE_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAR_TARIFAS_ID_GENERATOR")
	private long id;
	private String nombre;
	private Boolean isPublica;
    private Boolean isAbono;
    private Boolean defecto;

    @Column(name = "MAX_EVENTOS")
    private Long maxEventos;

	@OneToMany(mappedBy = "parTarifa")
	private List<TarifasCineDTO> parTarifasCine;

	@OneToMany(mappedBy = "tarifa")
	private List<AbonadoDTO> abonados;

	@ManyToOne
	@JoinColumn(name="CINE_ID")
	private CineDTO parCine;

	public TarifaDTO() {
	}

	public TarifaDTO(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<TarifasCineDTO> getParTarifasCine() {
		return parTarifasCine;
	}

	public void setParTarifasCine(List<TarifasCineDTO> parTarifasCine) {
		this.parTarifasCine = parTarifasCine;
	}

	public Boolean getIsPublica() {
		return isPublica;
	}

	public void setIsPublica(Boolean isPublica) {
		this.isPublica = isPublica;
	}

    public Boolean getIsAbono() {
        return isAbono;
    }

    public void setIsAbono(Boolean isAbono) {
        this.isAbono = isAbono;
    }

	public Boolean getDefecto() {
		return defecto;
	}

	public void setDefecto(Boolean defecto) {
		this.defecto = defecto;
	}

	public Long getMaxEventos() {
		return maxEventos;
	}

	public void setMaxEventos(Long maxEventos) {
		this.maxEventos = maxEventos;
	}

	public CineDTO getParCine()
	{
		return parCine;
	}

	public void setParCine(CineDTO parCine)
	{
		this.parCine = parCine;
	}

	public List<AbonadoDTO> getAbonados() {
		return abonados;
	}

	public void setAbonados(List<AbonadoDTO> abonados) {
		this.abonados = abonados;
	}
}