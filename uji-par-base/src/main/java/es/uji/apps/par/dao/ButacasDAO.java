package es.uji.apps.par.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import es.uji.apps.par.auth.Role;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.OcupacionDTO;
import es.uji.apps.par.db.QButacaDTO;
import es.uji.apps.par.db.QButacaSinEagerDTO;
import es.uji.apps.par.db.QCompraDTO;
import es.uji.apps.par.db.QCompraSinEagerDTO;
import es.uji.apps.par.db.QLocalizacionDTO;
import es.uji.apps.par.db.QOcupacionDTO;
import es.uji.apps.par.db.QSesionDTO;
import es.uji.apps.par.db.QTarifaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.exceptions.ButacaOcupadaException;
import es.uji.apps.par.exceptions.IncidenciaNotFoundException;
import es.uji.apps.par.exceptions.NoHayButacasLibresException;
import es.uji.apps.par.exceptions.SesionSinFormatoIdiomaIcaaException;
import es.uji.apps.par.ext.ExtGridFilter;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.ficheros.registros.TipoIncidencia;
import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.model.PreciosSesion;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.utils.PrecioUtils;
import es.uji.apps.par.utils.Utils;

import static es.uji.apps.par.db.QButacaDTO.butacaDTO;

@Repository
public class ButacasDAO extends BaseDAO {
    @Autowired
    private LocalizacionesDAO localizacionesDAO;

    @Autowired
    private SesionesDAO sesionesDAO;

    @Autowired
    private UsuariosDAO usuariosDAO;

    @Autowired
    private ComprasDAO comprasDAO;

    @Autowired
    Configuration configuration;

    private QButacaDTO qButacaDTO = butacaDTO;
    private QCompraSinEagerDTO qCompraSinEagerDTO = QCompraSinEagerDTO.compraSinEagerDTO;
    private QButacaSinEagerDTO qButacaSinEagerDTO = QButacaSinEagerDTO.butacaSinEagerDTO;

    @Transactional
    public ButacaDTO getButaca(long id) {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qButacaDTO).leftJoin(qButacaDTO.parSesion, qSesionDTO).fetch().where(qButacaDTO.id.eq(id))
            .uniqueResult(qButacaDTO);
    }

    @Transactional
    public List<ButacaDTO> getButacas(List<Long> ids) {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qButacaDTO).leftJoin(qButacaDTO.parSesion, qSesionDTO).fetch().where(qButacaDTO.id.in(ids))
            .list(qButacaDTO);
    }

    @Transactional
    public List<ButacaDTO> getButacasOcupadasNoAnuladasPorLocalizacion(
        long idSesion,
        String codigoLocalizacion
    ) {
        QLocalizacionDTO qLocalizacionDTO = QLocalizacionDTO.localizacionDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<Long> sesionesIds = sesionesDAO.getSesionesIdsBySesionId(idSesion);

        List<ButacaDTO> list = query.from(qButacaDTO).join(qButacaDTO.parSesion, qSesionDTO)
            .join(qButacaDTO.parLocalizacion, qLocalizacionDTO).leftJoin(qButacaDTO.parCompra, qCompraDTO).fetch()
            .where(qSesionDTO.id.in(sesionesIds).and(qLocalizacionDTO.codigo.eq(codigoLocalizacion))
                .and(qButacaDTO.anulada.eq(false))).list(qButacaDTO);

        return list;
    }

    @Transactional
    public List<Tuple> getButacasOcupadasNoAnuladasPorSesionNotInCompra(
        long idSesion,
        String uuidCompra
    ) {
        QLocalizacionDTO qLocalizacionDTO = QLocalizacionDTO.localizacionDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QCompraSinEagerDTO qCompraSinEagerDTO = QCompraSinEagerDTO.compraSinEagerDTO;
        QButacaSinEagerDTO qButacaSinEagerDTO = QButacaSinEagerDTO.butacaSinEagerDTO;

        List<Long> sesionesIds = sesionesDAO.getSesionesIdsBySesionId(idSesion);

        BooleanExpression where = qSesionDTO.id.in(sesionesIds).and(qButacaSinEagerDTO.anulada.eq(false));
        JPAQuery query =
            new JPAQuery(entityManager).from(qButacaSinEagerDTO).join(qButacaSinEagerDTO.parSesion, qSesionDTO)
                .join(qButacaSinEagerDTO.parLocalizacion, qLocalizacionDTO)
                .join(qButacaSinEagerDTO.parCompra, qCompraSinEagerDTO).where(
                where.and(qCompraSinEagerDTO.pagada.isTrue().or(qCompraSinEagerDTO.uuid.ne(uuidCompra)))
                    .and(qButacaSinEagerDTO.anulada.isNull().or(qButacaSinEagerDTO.anulada.eq(false)))
            .and(qButacaSinEagerDTO.fila.isNotNull())
                    .and(qButacaSinEagerDTO.numero.isNotNull()
                        .and(qLocalizacionDTO.codigo.isNotNull())));

        return query.list(qButacaSinEagerDTO.fila, qButacaSinEagerDTO.numero, qLocalizacionDTO.codigo);
    }

    @Transactional
    public List<Tuple> getButacas(
        long idSesion,
        List<String> codigosLocalizacion
    ) {
        QLocalizacionDTO qLocalizacionDTO = QLocalizacionDTO.localizacionDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QCompraSinEagerDTO qCompraSinEagerDTO = QCompraSinEagerDTO.compraSinEagerDTO;

        JPAQuery query = new JPAQuery(entityManager);
        List<Long> sesionesIds = sesionesDAO.getSesionesIdsBySesionId(idSesion);

        return query.from(qButacaSinEagerDTO).join(qButacaSinEagerDTO.parSesion, qSesionDTO)
            .join(qButacaSinEagerDTO.parLocalizacion, qLocalizacionDTO)
            .leftJoin(qButacaSinEagerDTO.parCompra, qCompraSinEagerDTO)
            .where(qSesionDTO.id.in(sesionesIds).and(qLocalizacionDTO.codigo.in(codigosLocalizacion)))
            .distinct()
            .list(qButacaSinEagerDTO.anulada, qButacaSinEagerDTO.fila, qButacaSinEagerDTO.numero,
                qButacaSinEagerDTO.presentada, qCompraSinEagerDTO.reserva.coalesce(false), qLocalizacionDTO.codigo);
    }

    @Transactional
    public List<Tuple> getButacasPagadas(
        long idSesion,
        String userUID,
        boolean allCompras
    ) {
        QLocalizacionDTO qLocalizacionDTO = QLocalizacionDTO.localizacionDTO;
        QCompraSinEagerDTO qCompraSinEagerDTO = QCompraSinEagerDTO.compraSinEagerDTO;
        QTarifaDTO qTarifaDTO = QTarifaDTO.tarifaDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<Long> sesionesIds = sesionesDAO.getSesionesIdsBySesionId(idSesion);

        BooleanExpression where =
            qCompraSinEagerDTO.parSesion.id.in(sesionesIds)
                .and(qCompraSinEagerDTO.reserva.eq(false)).and(qCompraSinEagerDTO.caducada.eq(false))
                .and(qCompraSinEagerDTO.pagada.eq(true)).and(qButacaSinEagerDTO.tipo.castToNum(Long.class).eq(qTarifaDTO.id));

        Usuario user = usuariosDAO.getUserById(userUID);
        if (!allCompras && user.getRole().equals(Role.TAQUILLA)) {
            where = where.and(qCompraSinEagerDTO.usuario.id.eq(user.getId()).or(qCompraSinEagerDTO.usuario.isNull()));
        }

        return query.from(qCompraSinEagerDTO, qTarifaDTO).join(qCompraSinEagerDTO.parButacas, qButacaSinEagerDTO)
            .join(qButacaSinEagerDTO.parLocalizacion, qLocalizacionDTO).fetch().where(where).distinct().list(qButacaSinEagerDTO, qTarifaDTO.nombre, qTarifaDTO.id);
    }

    @Transactional
    public void addButaca(ButacaDTO butacaDTO) {
        entityManager.persist(butacaDTO);
    }

    @Transactional
    public boolean estaOcupada(
        long idSesion,
        String codigoLocalizacion,
        String fila,
        String numero
    ) {
        QLocalizacionDTO qLocalizacionDTO = QLocalizacionDTO.localizacionDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<Long> sesionesIds = sesionesDAO.getSesionesIdsBySesionId(idSesion);

        List<ButacaDTO> list = query.from(qButacaDTO, qSesionDTO, qLocalizacionDTO).where(
            qButacaDTO.parSesion.id.eq(qSesionDTO.id).and(qSesionDTO.id.in(sesionesIds))
                .and(qLocalizacionDTO.codigo.eq(codigoLocalizacion))
                .and(qButacaDTO.parLocalizacion.id.eq(qLocalizacionDTO.id)).and(qButacaDTO.anulada.eq(false))
                .and(qButacaDTO.fila.eq(fila)).and(qButacaDTO.numero.eq(numero))).distinct().list(qButacaDTO);

        return list.size() > 0;
    }

    @Transactional
    public CompraDTO getCompra(
        long idSesion,
        String codigoLocalizacion,
        String fila,
        String numero
    ) {
        QLocalizacionDTO qLocalizacionDTO = QLocalizacionDTO.localizacionDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<CompraDTO> list = query.from(qButacaDTO).join(qButacaDTO.parSesion, qSesionDTO)
            .join(qButacaDTO.parLocalizacion, qLocalizacionDTO).join(qButacaDTO.parCompra, qCompraDTO).where(
                qSesionDTO.id.eq(idSesion).and(qLocalizacionDTO.codigo.eq(codigoLocalizacion))
                    .and(qButacaDTO.fila.eq(fila)).and(qButacaDTO.numero.eq(numero))
                    .and(qButacaDTO.anulada.isNull().or(qButacaDTO.anulada.eq(false)))).list(qCompraDTO);

        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    private void deleteButacas(CompraDTO compraDTO) {
        QButacaDTO qButacaDTO = butacaDTO;

        new JPADeleteClause(entityManager, qButacaDTO).where(qButacaDTO.parCompra.id.eq(compraDTO.getId())).execute();
    }

    @Transactional(rollbackFor = SesionSinFormatoIdiomaIcaaException.class)
    public void cambiaTipo(
        List<ButacaDTO> butacas,
        String tipo,
        Map<Long, PreciosSesion> mapaTarifasPrecios
    ) throws IncidenciaNotFoundException {
        BigDecimal precio = mapaTarifasPrecios.get(Long.valueOf(tipo)).getPrecio();
        BigDecimal precioSinComision = precio.setScale(2, BigDecimal.ROUND_UP);
        BigDecimal precioConComision = precio.setScale(2, BigDecimal.ROUND_UP);
        for (ButacaDTO butaca : butacas) {
            butaca.setTipo(tipo);
            butaca.setPrecioSinComision(precioSinComision);
            butaca.setPrecio(precioConComision);

            entityManager.merge(butaca);
        }
    }

    @Transactional(rollbackForClassName = {"NoHayButacasLibresException", "ButacaOcupadaException"})
    public void reservaButacas(
        Long sesionId,
        CompraDTO compraDTO,
        List<Butaca> butacas,
        Map<String, Map<Long, PreciosSesion>> preciosSesionPorLocalizacion,
        boolean taquilla,
        String userUID,
        boolean numeracionSecuenciaEnabled
    ) throws ButacaOcupadaException, NoHayButacasLibresException {
        Butaca butacaActual = null;

        try {
            deleteButacas(compraDTO);
            SesionDTO sesionConsultadaDTO = sesionesDAO.getSesion(sesionId, userUID);
            List<SesionDTO> sesiones = new ArrayList<>();
            if (sesionConsultadaDTO.isMultisesion()) {
                sesiones = sesionesDAO.getMultiplesSesionesBySesionId(sesionId);
            }
            else {
                sesiones.add(sesionConsultadaDTO);
            }

            for (SesionDTO sesionDTO : sesiones) {
                if ((sesionDTO.getParEvento() != null && sesionDTO.getParEvento().getAsientosNumerados() != null && !sesionDTO.getParEvento().getAsientosNumerados()) && numeracionSecuenciaEnabled) {
                    numeraButacasSecuencialmente(sesionId, butacas, sesionDTO);
                }

                for (Butaca butaca : butacas) {
                    butacaActual = butaca;
                    LocalizacionDTO localizacionDTO = localizacionesDAO.getLocalizacionByCodigo(butaca.getLocalizacion());

                    if (((sesionDTO.getParEvento() != null && sesionDTO.getParEvento().getAsientosNumerados() != null && !sesionDTO.getParEvento().getAsientosNumerados()) || (butaca.getFila() == null
                        && butaca.getNumero() == null)) && noHayButacasLibres(compraDTO.getId(), sesionDTO, localizacionDTO))
                        throw new NoHayButacasLibresException(sesionId, butaca.getLocalizacionNombre());

                    ButacaDTO butacaDTO = Butaca.butacaToButacaDTO(butaca);
                    butacaDTO.setParSesion(sesionDTO);
                    butacaDTO.setParCompra(compraDTO);
                    butacaDTO.setParLocalizacion(localizacionDTO);
                    butacaDTO.setTipo(butaca.getTipo());
                    butacaDTO.setAnulada(false);

                    Map<Long, PreciosSesion> mapaTarifasPrecios = preciosSesionPorLocalizacion.get(butaca.getLocalizacion());
                    BigDecimal precio = mapaTarifasPrecios.get(Long.valueOf(butaca.getTipo())).getPrecio();
                    butacaDTO.setPrecioSinComision(precio.setScale(2, BigDecimal.ROUND_UP));
                    if (taquilla) {
                        butacaDTO.setPrecio(precio.setScale(2, BigDecimal.ROUND_UP));
                    } else {
                        BigDecimal comision = sesionDTO.getParEvento().getParCine().getComision();
                        butacaDTO.setPrecio(PrecioUtils.getPrecioConComision(precio, comision));
                    }

                    entityManager.persist(butacaDTO);
                    entityManager.flush();
                    entityManager.clear();
                }
            }
        } catch (Exception e) {
            if (butacaActual != null && e.getCause() != null && (e.getCause()
                instanceof ConstraintViolationException || e.getCause()
                .getCause() instanceof ConstraintViolationException))
                throw new ButacaOcupadaException(sesionId, butacaActual.getLocalizacionNombre(), butacaActual.getFila(),
                    butacaActual.getNumero());
            else
                throw e;
        }
    }

    private void numeraButacasSecuencialmente(
        Long sesionId,
        List<Butaca> butacas,
        SesionDTO sesionDTO
    ) {
        List<String> numerosAsignados = getNumerosButacasNoAnuladas(sesionId);
        BigDecimal aforoTotal =
            sesionDTO.getParSala().getParLocalizaciones().stream().map(LocalizacionDTO::getTotalEntradas)
                .reduce(BigDecimal::add).get();
        for (Butaca butaca : butacas) {
            if (butaca.getNumero() == null) {
                boolean foundSecuentialNum = false;
                int i = 1;
                do {
                    String currentNumero = String.valueOf(i);
                    if (!numerosAsignados.contains(currentNumero)) {
                        numerosAsignados.add(currentNumero);
                        butaca.setNumero(currentNumero);
                        foundSecuentialNum = true;
                    }
                    i++;
                } while (!foundSecuentialNum && i <= aforoTotal.intValue());
            }
        }
    }

    @Transactional
    public boolean noHayButacasLibres(
        Long compraId,
        SesionDTO sesionDTO,
        LocalizacionDTO localizacionDTO
    ) {
        return getOcupadas(compraId, sesionDTO, localizacionDTO.getCodigo()) >= localizacionDTO.getTotalEntradas()
            .intValue();
    }

    @Transactional
    public int getOcupadas(
        Long compraId,
        SesionDTO sesionDTO,
        String codigoLocalizacion
    ) {
        QLocalizacionDTO qLocalizacionDTO = QLocalizacionDTO.localizacionDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        JPAQuery query = new JPAQuery(entityManager);
        List<Long> list;
        List<Long> sesionesIds = sesionesDAO.getSesionesIdsBySesionId(sesionDTO.getId());
        if (sesionDTO.getParSala().isAforoPorCompras()) {
            list = query.from(qCompraSinEagerDTO).join(qCompraSinEagerDTO.parButacas, qButacaSinEagerDTO)
                .innerJoin(qButacaSinEagerDTO.parSesion, qSesionDTO)
                .innerJoin(qButacaSinEagerDTO.parLocalizacion, qLocalizacionDTO).where(
                qCompraSinEagerDTO.id.ne(compraId).and(
                    qSesionDTO.id.in(sesionesIds).and(qCompraSinEagerDTO.anulada.eq(false)).and(qButacaSinEagerDTO.anulada.eq(false))
                        .and(qLocalizacionDTO.codigo.eq(codigoLocalizacion)))).groupBy(qCompraSinEagerDTO.id)
                .distinct().list(qCompraSinEagerDTO.id);

            return list.size();
        }
        else {

            list = query.from(qButacaSinEagerDTO).join(qButacaSinEagerDTO.parCompra, qCompraSinEagerDTO)
                .innerJoin(qButacaSinEagerDTO.parSesion, qSesionDTO)
                .innerJoin(qButacaSinEagerDTO.parLocalizacion, qLocalizacionDTO).where(
                    qSesionDTO.id.in(sesionesIds).and(qButacaSinEagerDTO.anulada.eq(false))
                        .and(qLocalizacionDTO.codigo.eq(codigoLocalizacion))).distinct().list(qButacaSinEagerDTO.id);
        }
        return list.size();
    }

    @Transactional
    public List<OcupacionDTO> getOcupadas(
        List<Long> sesionIds
    ) {
        if (sesionIds != null && sesionIds.size() > 0) {
            QOcupacionDTO qOcupacionDTO = QOcupacionDTO.ocupacionDTO;
            BooleanExpression onStatement = Utils.getInSublistLimited(sesionIds, qOcupacionDTO.sesionId);

            return new JPAQuery(entityManager).from(qOcupacionDTO).where(onStatement).list(qOcupacionDTO);
        } else {
            return null;
        }
    }

    @Transactional
    public List<Tuple> getButacasCompra(
        Long idCompra,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter
    ) {
        QTarifaDTO qTarifaDTO = QTarifaDTO.tarifaDTO;
        return getQueryButacasCompra(idCompra, filter).orderBy(getSort(qButacaDTO, sortParameter)).offset(start).limit(limit)
            .list(qButacaDTO, qTarifaDTO);
    }

    private BooleanBuilder getFilterWhere(
        ExtGridFilterList filter
    ) {
        BooleanBuilder builder = new BooleanBuilder();
        if (filter != null) {
            final ExtGridFilter sesionFilter = filter.findFiltroByProperty("sesionId");
            if (sesionFilter != null) {
                List<Long> sesionesIds = sesionesDAO.getSesionesIdsBySesionId(Long.valueOf(sesionFilter.getValue().toString()));
                builder.and(
                    qButacaDTO.parSesion.id.in(sesionesIds));
            }
        }
        return builder;
    }

    private JPAQuery getQueryButacasCompra(Long idCompra, ExtGridFilterList filter) {
        JPAQuery query = new JPAQuery(entityManager);
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QTarifaDTO qTarifaDTO = QTarifaDTO.tarifaDTO;

        BooleanExpression builder = qCompraDTO.id.eq(idCompra).and(qTarifaDTO.id.eq(qButacaDTO.tipo.castToNum(Long.class)));
        builder = builder.and(getFilterWhere(filter));
        return query.from(qButacaDTO, qTarifaDTO).join(qButacaDTO.parCompra, qCompraDTO).fetch()
            .where(builder);
    }

    @Transactional
    public int getTotalButacasCompra(Long idCompra, ExtGridFilterList filter) {
        return (int) getQueryButacasCompra(idCompra, filter).distinct().count();
    }

    @Transactional
    public List<Tuple> getButacasNoAnuladas(Long idSesion) {
        QCompraSinEagerDTO qCompraSinEagerDTO = QCompraSinEagerDTO.compraSinEagerDTO;

        List<Long> sesionesIds = sesionesDAO.getSesionesIdsBySesionId(idSesion);
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qButacaSinEagerDTO).leftJoin(qButacaSinEagerDTO.parCompra, qCompraSinEagerDTO).where(
            qButacaSinEagerDTO.parSesion.id.in(sesionesIds).and(qCompraSinEagerDTO.reserva.eq(false))
                .and(qButacaSinEagerDTO.anulada.eq(false))).distinct()
            .list(qButacaSinEagerDTO.id, qButacaSinEagerDTO.fila, qButacaSinEagerDTO.numero, qButacaSinEagerDTO.precio,
                qButacaSinEagerDTO.precioSinComision, qButacaSinEagerDTO.parLocalizacion.codigo,
                qButacaSinEagerDTO.parLocalizacion.nombreEs, qButacaSinEagerDTO.parLocalizacion.nombreVa,
                qButacaSinEagerDTO.tipo, qButacaSinEagerDTO.presentada, qButacaSinEagerDTO.anulada,
                qButacaSinEagerDTO.fechaAnulada, qCompraSinEagerDTO.uuid, qCompraSinEagerDTO.reserva);
    }

    private List<String> getNumerosButacasNoAnuladas(Long idSesion) {
        List<Long> sesionesIds = sesionesDAO.getSesionesIdsBySesionId(idSesion);
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qButacaSinEagerDTO).where(
            qButacaSinEagerDTO.parSesion.id.in(sesionesIds)
                .and(qButacaSinEagerDTO.anulada.eq(false)))
            .distinct()
            .list(qButacaSinEagerDTO.numero);
    }

    @Transactional
    public void updatePresentadas(List<Butaca> butacas) {
        for (Butaca butaca : butacas) {
            updatePresentada(butaca);
        }
    }

    @Transactional
    public long updatePresentada(Butaca butaca) {
        return new JPAUpdateClause(entityManager, qButacaDTO)
            .where(qButacaDTO.id.eq(butaca.getId()).and(qButacaDTO.presentada.isNull()))
            .set(qButacaDTO.presentada, butaca.getPresentada()).execute();
    }

    private SesionDTO getSesion(Long idButaca) {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QButacaDTO qButacaDTO = butacaDTO;
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qButacaDTO).join(qButacaDTO.parCompra, qCompraDTO).join(qCompraDTO.parSesion, qSesionDTO)
            .where(qButacaDTO.id.eq(idButaca)).uniqueResult(qSesionDTO);
    }

    @Transactional(rollbackFor = SesionSinFormatoIdiomaIcaaException.class)
    public void anularButaca(
        Long idButaca,
        Usuario usuario
    ) throws IncidenciaNotFoundException {
        QButacaDTO qButacaDTO = butacaDTO;

        JPAUpdateClause updateButacas = new JPAUpdateClause(entityManager, qButacaDTO);

        if (usuario != null) {
            updateButacas = updateButacas.set(qButacaDTO.usuarioAnula, Usuario.toDTO(usuario));
        }

        updateButacas.set(qButacaDTO.anulada, true).set(qButacaDTO.fechaAnulada, new Date())
            .where(qButacaDTO.id.eq(idButaca)).execute();

        if (!isButacaFromReserva(idButaca)) {
            SesionDTO sesionDTO = getSesion(idButaca);
            sesionesDAO.setIncidencia(sesionDTO.getId(),
                TipoIncidencia.addAnulacionVentasToIncidenciaActual(sesionDTO.getIncidenciaId()));
        }
    }

    @Transactional(rollbackFor = SesionSinFormatoIdiomaIcaaException.class)
    public void cambiaFilaNumero(
        Long idButaca,
        String fila,
        String numero
    ) throws IncidenciaNotFoundException {
        QButacaDTO qButacaDTO = butacaDTO;

        JPAUpdateClause updateButacas = new JPAUpdateClause(entityManager, qButacaDTO);
        updateButacas.set(qButacaDTO.fila, fila).set(qButacaDTO.numero, numero).
            where(qButacaDTO.id.eq(idButaca)).execute();
    }

    @Transactional
    protected boolean isButacaFromReserva(Long idButaca) {
        QButacaDTO qButacaDTO = butacaDTO;
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

        JPAQuery query = new JPAQuery(entityManager);
        CompraDTO compra =
            query.from(qButacaDTO).join(qButacaDTO.parCompra, qCompraDTO).where(qButacaDTO.id.eq(idButaca))
                .uniqueResult(qCompraDTO);
        if (!compra.isReserva())
            return false;
        else
            return true;
    }

    @Transactional
    public void asignarIdEntrada(Long idCompra) {
        QButacaDTO qButacaDTO = butacaDTO;
        Integer idEntradaConfiguracion = configuration.getIdEntrada();
        JPAQuery query = new JPAQuery(entityManager);
        Integer maxIdEntrada =
            query.from(qButacaDTO).uniqueResult(qButacaDTO.idEntrada.max().coalesce(idEntradaConfiguracion));
        maxIdEntrada = (maxIdEntrada < idEntradaConfiguracion) ? idEntradaConfiguracion : maxIdEntrada;

        List<Long> idsButacasCompra = getIdsButacasCompra(idCompra);
        if (idsButacasCompra != null) {
            for (Long idButaca : idsButacasCompra) {
                maxIdEntrada++;
                JPAUpdateClause updateButacas = new JPAUpdateClause(entityManager, qButacaDTO);
                updateButacas.set(qButacaDTO.idEntrada, maxIdEntrada).
                    where(qButacaDTO.id.eq(idButaca)).execute();
            }
        }
    }

    private List<Long> getIdsButacasCompra(Long idCompra) {
        QButacaDTO qButacaDTO = butacaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qButacaDTO).where(qButacaDTO.parCompra.id.eq(idCompra)).list(qButacaDTO.id);
    }

    @Transactional
    public void borraButaca(Long idButaca) {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qButacaDTO);
        deleteClause.where(qButacaDTO.id.eq(idButaca)).execute();
    }

    @Transactional
    public List<Long> getComprasConTodasButacasAnuladas(List<Long> idsButacas) {
        QButacaDTO qButacaDTO = butacaDTO;
        QButacaDTO qButacaDTO1 = new QButacaDTO("qButacaDTO1");
        QButacaDTO qButacaDTO2 = new QButacaDTO("qButacaDTO2");
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<Long> comprasConButacas =
            new JPAQuery(entityManager).from(qButacaDTO1).where(qButacaDTO1.id.in(idsButacas)).distinct()
                .list(qButacaDTO1.parCompra.id);

        List<Long> comprasConEntradasNoAnuladas = new JPAQuery(entityManager).from(qButacaDTO2)
            .where(qButacaDTO2.parCompra.id.in(comprasConButacas).and(qButacaDTO2.anulada.eq(false))).distinct()
            .list(qButacaDTO2.parCompra.id);

        if (comprasConEntradasNoAnuladas == null || comprasConEntradasNoAnuladas.size() == 0) {
            return comprasConButacas;
        } else {
            return query.from(qCompraDTO).join(qCompraDTO.parButacas, qButacaDTO)
                .where(qButacaDTO.id.in(idsButacas).and(qCompraDTO.id.notIn(comprasConEntradasNoAnuladas))).distinct()
                .list(qCompraDTO.id);
        }
    }
}
