package es.uji.apps.par.auth;

import javax.servlet.http.HttpServletRequest;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.services.UsersService;

public interface Authenticator
{
    int AUTH_OK = 0;
    int AUTH_FAILED = 1;

    String USER_ATTRIBUTE = "user";
    String READONLY_ATTRIBUTE = "readonly";
    String TAQUILLA_ATTRIBUTE = "taquilla";
	String ERROR_LOGIN = "errorLogin";

	int authenticate(HttpServletRequest request);
	void setConfiguration(Configuration configuration);
	void setUserService(UsersService userService);
}