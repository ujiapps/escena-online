package es.uji.apps.par.db;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.uji.apps.par.auth.Role;

/**
 * The persistent class for the PAR_USUARIOS database table.
 * 
 */
@Entity
@Table(name="PAR_USUARIOS")
public class UsuarioDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PAR_USUARIOS_ID_GENERATOR", sequenceName="HIBERNATE_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAR_USUARIOS_ID_GENERATOR")
	private long id;

	private String mail;

	private String nombre;

	private String usuario;

	private String url;

	private String password;

	@Column(name = "TOKEN_VALIDACION")
	private String tokenValidacion;

	@Column(name = "EXPIRACION_TOKEN")
	private Timestamp expiracionToken;

	@Column(name = "EMAIL_VERIFICADO")
	private Boolean emailVerificado;

	@Column(name = "PUEDE_ANULAR")
	private Boolean puedeAnular;

	@Enumerated(EnumType.STRING)
	private Role role;

	@OneToMany(mappedBy = "parUsuario", fetch = FetchType.LAZY)
	private List<SalasUsuarioDTO> parSalasUsuario;

	@OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
	private List<CompraDTO> parCompras;

	public UsuarioDTO() {
	}

	public UsuarioDTO(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<SalasUsuarioDTO> getParSalasUsuario() {
		return parSalasUsuario;
	}

	public void setParSalasUsuario(List<SalasUsuarioDTO> parSalasUsuario) {
		this.parSalasUsuario = parSalasUsuario;
	}

	public List<CompraDTO> getParCompras() {
		return parCompras;
	}

	public void setParCompras(List<CompraDTO> parCompras) {
		this.parCompras = parCompras;
	}

	public String getTokenValidacion() {
		return tokenValidacion;
	}

	public void setTokenValidacion(String tokenValidacion) {
		this.tokenValidacion = tokenValidacion;
	}

	public Timestamp getExpiracionToken() {
		return expiracionToken;
	}

	public void setExpiracionToken(Timestamp expiracionToken) {
		this.expiracionToken = expiracionToken;
	}

	public Boolean getEmailVerificado() {
		return emailVerificado == null || emailVerificado == false ? false : true;
	}

	public void setEmailVerificado(Boolean emailVerificado) {
		this.emailVerificado = emailVerificado;
	}

	public Boolean getPuedeAnular() {
		return puedeAnular == null || puedeAnular == false ? false : true;
	}

	public void setPuedeAnular(Boolean puedeAnular) {
		this.puedeAnular = puedeAnular;
	}
}