package es.uji.apps.par.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.ButacasPlantillaReservasDTO;

@XmlRootElement
public class Butaca
{
    public static Logger log = Logger.getLogger(Butaca.class);

    private long id;
    private String fila;
    private String numero;
    private BigDecimal precio;
    private BigDecimal precioSinComision;
    private String localizacion;
    private String localizacionNombre;
    private String x;
    private String y;
    private String tipo;
    private Integer cantidad;
    private Date presentada;
    private String uuid;
    private String texto;
    private boolean anulada;
    private Date fechaAnulada;
    private Usuario usuarioAnula;
    private boolean reserva;

    public static ButacaDTO butacaToButacaDTO(Butaca butaca)
    {
        ButacaDTO butacaDTO = new ButacaDTO();

        butacaDTO.setId(butaca.getId());
        butacaDTO.setFila(butaca.getFila());
        butacaDTO.setNumero(butaca.getNumero());

        return butacaDTO;
    }

    public static List<Butaca> butacasDTOToButacas(List<ButacaDTO> butacasDTO, boolean isIdEntrada, String language)
    {
        List<Butaca> butacas = new ArrayList<Butaca>();
         
         for (ButacaDTO butacaDTO: butacasDTO)
         {
             butacas.add(new Butaca(butacaDTO, isIdEntrada, language));
         }
         
         return butacas;
    }
    
    public Butaca()
    {
    }

    public Butaca(String localizacion, String tipo)
    {
        this.localizacion = localizacion;
        this.tipo = tipo;
    }

    public Butaca(ButacaDTO butacaDTO) {
        this(butacaDTO, false, "es");
    }

    public Butaca(ButacaDTO butacaDTO, boolean isIdEntrada, String language)
    {
        id = butacaDTO.getId();
        fila = butacaDTO.getFila();
        numero = butacaDTO.getNumero();
        precio = butacaDTO.getPrecio();
        precioSinComision = butacaDTO.getPrecioSinComision();
        localizacion = butacaDTO.getParLocalizacion().getCodigo();
        if (language.equals("ca"))
        {
            localizacionNombre = butacaDTO.getParLocalizacion().getNombreVa();
        }
        else
        {
            localizacionNombre = butacaDTO.getParLocalizacion().getNombreEs();
        }
        tipo = butacaDTO.getTipo();
        presentada = butacaDTO.getPresentada();
        
        if (butacaDTO.getAnulada() != null) {
            anulada = butacaDTO.getAnulada();
        }
        fechaAnulada = butacaDTO.getFechaAnulada();

        if (butacaDTO.getUsuarioAnula() != null) {
            usuarioAnula = new Usuario(butacaDTO.getUsuarioAnula());
        }
        
        if (butacaDTO.getParCompra() != null) {
            reserva = butacaDTO.getParCompra().getReserva();
            if (isIdEntrada) {
                uuid = butacaDTO.getParCompra().getUuid() + "-" + butacaDTO.getIdEntrada();
            } else {
                uuid = butacaDTO.getParCompra().getUuid() + "-" + butacaDTO.getId();
            }
        }
    }

    public static List<Butaca> parseaJSON(String jsonButacas)
    {
        Gson gson = new Gson();
        return gson.fromJson(jsonButacas, new TypeToken<List<Butaca>>()
        {
        }.getType());
    }
    
    public static String toJSON(List<Butaca> butacas)
    {
        Gson gson = new Gson();
        return gson.toJson(butacas, new TypeToken<List<Butaca>>()
        {
        }.getType());
    }

    public static List<Butaca> getNumeradas(List<Butaca> butacasSeleccionadas) {
        return butacasSeleccionadas.stream().filter(b -> b.getCantidad() == null).collect(Collectors.toList());
    }

    public static List<Butaca> getNoNumeradas(List<Butaca> butacasSeleccionadas) {
        return butacasSeleccionadas.stream().filter(b -> b.getCantidad() != null).collect(Collectors.toList());
    }

    public static Butaca plantillaButacaReservaDTOToButaca(ButacasPlantillaReservasDTO butacasPlantillaReservasDTO,
        String language) {
        Butaca butaca = new Butaca();
        butaca.setFila(butacasPlantillaReservasDTO.getFila());
        butaca.setNumero(butacasPlantillaReservasDTO.getNumero());
        butaca.setLocalizacion(butacasPlantillaReservasDTO.getParLocalizacion().getCodigo());
        String nombreLocalizacion = butacasPlantillaReservasDTO.getParLocalizacion().getNombreEs();
        if(language.equalsIgnoreCase("ca")) {
            nombreLocalizacion = butacasPlantillaReservasDTO.getParLocalizacion().getNombreVa();
        }
        butaca.setLocalizacionNombre(nombreLocalizacion);

        return butaca;
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getFila()
    {
        return fila;
    }

    public void setFila(String fila)
    {
        this.fila = fila;
    }

    public String getNumero()
    {
        return numero;
    }

    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    public BigDecimal getPrecio()
    {
        return precio;
    }

    public void setPrecio(BigDecimal precio)
    {
        this.precio = precio;
    }

    public BigDecimal getPrecioSinComision() {
        return precioSinComision;
    }

    public void setPrecioSinComision(BigDecimal precioSinComision) {
        this.precioSinComision = precioSinComision;
    }

    public String getLocalizacion()
    {
        return localizacion;
    }

    public void setLocalizacion(String localizacion)
    {
        this.localizacion = localizacion;
    }

    public String getX()
    {
        return x;
    }

    public void setX(String x)
    {
        this.x = x;
    }

    public String getY()
    {
        return y;
    }

    public void setY(String y)
    {
        this.y = y;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString()
    {
        StringBuffer buff = new StringBuffer();
        
        buff.append("<id=");
        buff.append(id);
        buff.append(", localizacion=");
        buff.append(localizacion);
        buff.append(", fila=");
        buff.append(fila);
        buff.append(", numero=");
        buff.append(numero);
        buff.append(", tipo=");
        buff.append(tipo);        
        
        return buff.toString();
    }

    public Date getPresentada()
    {
        return presentada;
    }

    public void setPresentada(Date presentada)
    {
        this.presentada = presentada;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public boolean isAnulada() {
		return anulada;
	}

	public void setAnulada(boolean anulada) {
		this.anulada = anulada;
	}

    public Date getFechaAnulada() {
        return fechaAnulada;
    }

    public void setFechaAnulada(Date fechaAnulada) {
        this.fechaAnulada = fechaAnulada;
    }

    public Usuario getUsuarioAnula() {
        return usuarioAnula;
    }

    public void setUsuarioAnula(Usuario usuarioAnula) {
        this.usuarioAnula = usuarioAnula;
    }

    public String getLocalizacionNombre() {
		return localizacionNombre;
	}

	public void setLocalizacionNombre(String localizacionNombre) {
		this.localizacionNombre = localizacionNombre;
	}

    public boolean isReserva() {
        return reserva;
    }

    public void setReserva(boolean reserva) {
        this.reserva = reserva;
    }
}