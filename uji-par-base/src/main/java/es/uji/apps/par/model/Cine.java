package es.uji.apps.par.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.exceptions.GeneralPARException;
import es.uji.apps.par.exceptions.RegistroSerializaException;
import es.uji.commons.web.template.Template;

@XmlRootElement
public class Cine implements Serializable {
    private static final long serialVersionUID = 1L;

    private long id;
    private String codigo;
    private String nombre;
    private String cif;
    private String direccion;
    private String codigoMunicipio;
    private String nombreMunicipio;
    private String cp;
    private String empresa;
    private String codigoRegistro;
    private String telefono;
    private BigDecimal iva;
    private String urlPublic;
    private String urlPrivacidad;
    private String urlCancelacion;
    private String urlComoLlegar;
    private String urlPieEntrada;
    private String mailFrom;
    private String logoReport;
    private Boolean showButacasQueHanEntradoEnDistintoColor;
    private String langs;
    private String defaultLang;
    private Boolean showIVA;
    private String apiKey;
    private Integer limiteEntradasGratuitasPorCompra;
    private BigDecimal comision;
    private String email;
    private String registroMercantil;
    private byte[] logo;
    private String logoContentType;
    private String logoSrc;
    private String logoUUID;
    private byte[] banner;
    private String bannerContentType;
    private String bannerSrc;
    private String bannerUUID;
    private String clasePostventa;
    private String informes;
    private String informesGenerales;
    private String formasPago;
    private boolean menuTpv;
    private boolean menuClientes;
    private boolean menuIntegraciones;
    private boolean menuSalas;
    private boolean menuPerfil;
    private boolean menuAyuda;
    private boolean menuTaquilla;
    private boolean checkContratacion;
    private boolean menuVentaAnticipada;
    private boolean menuAbonos;
    private boolean showComoNosConociste;
    private boolean noNumeradasSecuencial;
    private boolean menuICAA;
    private Boolean limiteNoGratuitas;
    private String codigoIcaa;

    private boolean showLogoEntrada;

    public Cine() {
    }

    public Cine(
        String codigo,
        String nombre,
        String cif,
        String direccion,
        String codigoMunicipio,
        String nombreMunicipio,
        String cp,
        String empresa,
        String codigoRegistro,
        String telefono,
        BigDecimal iva,
        String mailFrom,
        byte[] logo,
        String logoContentType,
        String logoSrc,
        byte[] banner,
        String bannerContentType,
        String bannerSrc,
        String url
    ) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.cif = cif;
        this.direccion = direccion;
        this.codigoMunicipio = codigoMunicipio;
        this.nombreMunicipio = nombreMunicipio;
        this.cp = cp;
        this.empresa = empresa;
        this.codigoRegistro = codigoRegistro;
        this.telefono = telefono;
        this.iva = iva;
        this.mailFrom = mailFrom;
        this.logoSrc = logoSrc;
        this.logoContentType = logoContentType;
        this.logo = logo;
        this.bannerSrc = bannerSrc;
        this.bannerContentType = bannerContentType;
        this.banner = banner;
        this.urlPublic = url + "/par-public";
    }

    public static CineDTO cineToCineDTO(Cine cine) {
        CineDTO cineDTO = new CineDTO();

        cineDTO.setId(cine.getId());
        cineDTO.setCodigo(cine.getCodigo());
        cineDTO.setCodigoIcaa(cine.getCodigoIcaa());
        cineDTO.setNombre(cine.getNombre());
        cineDTO.setCif(cine.getCif());
        cineDTO.setDireccion(cine.getDireccion());
        cineDTO.setCodigoMunicipio(cine.getCodigoMunicipio());
        cineDTO.setNombreMunicipio(cine.getNombreMunicipio());
        cineDTO.setCp(cine.getCp());
        cineDTO.setEmpresa(cine.getEmpresa());
        cineDTO.setCodigoRegistro(cine.getCodigoRegistro());
        cineDTO.setTelefono(cine.getTelefono());
        cineDTO.setIva(cine.getIva());
        cineDTO.setUrlPublic(cine.getUrlPublic());
        cineDTO.setUrlPrivacidad(cine.getUrlPrivacidad());
        cineDTO.setUrlCancelacion(cine.getUrlCancelacion());
        cineDTO.setUrlComoLlegar(cine.getUrlComoLlegar());
        cineDTO.setUrlPieEntrada(cine.getUrlPieEntrada());
        cineDTO.setMailFrom(cine.getMailFrom());
        cineDTO.setLogoReport(cine.getLogoReport());
        cineDTO.setApiKey(cine.getApiKey());
        cineDTO.setLimiteEntradasGratuitasPorCompra(cine.getLimiteEntradasGratuitasPorCompra());
        cineDTO.setDefaultLang(cine.getDefaultLang());
        cineDTO.setComision(cine.getComision());
        cineDTO.setEmail(cine.getEmail());
        cineDTO.setRegistroMercantil(cine.getRegistroMercantil());
        cineDTO.setClasePostventa(cine.getClasePostventa());
        cineDTO.setInformes(cine.getInformes());
        cineDTO.setInformesGenerales(cine.getInformesGenerales());
        cineDTO.setFormasPago(cine.getFormasPago());
        cineDTO.setMenuTpv(cine.isMenuTpv());
        cineDTO.setMenuClientes(cine.isMenuClientes());
        cineDTO.setMenuIntegraciones(cine.isMenuIntegraciones());
        cineDTO.setMenuSalas(cine.isMenuSalas());
        cineDTO.setMenuPerfil(cine.isMenuPerfil());
        cineDTO.setMenuICAA(cine.isMenuICAA());
        cineDTO.setMenuAyuda(cine.isMenuTaquilla());
        cineDTO.setMenuTaquilla(cine.isMenuTaquilla());
        cineDTO.setMenuVentaAnticipada(cine.isMenuVentaAnticipada());
        cineDTO.setMenuAbonos(cine.isMenuAbonos());
        cineDTO.setShowComoNosConociste(cine.isShowComoNosConociste());
        cineDTO.setCheckContratacion(cine.isCheckContratacion());
        cineDTO.setLimiteNoGratuitas(cine.getLimiteNoGratuitas());
        cineDTO.setShowLogoEntrada(cine.isShowLogoEntrada());
        cineDTO.setNoNumeradasSecuencia(cine.isNoNumeradasSecuencial());

        byte[] logo = cine.getLogo();
        if (cine.getLogoUUID() != null || (logo != null && logo.length > 0)) {
            if (cine.getLogoUUID() != null) {
                cineDTO.setLogoUUID(cine.getLogoUUID());
            } else {
                cineDTO.setLogo(cine.getLogo());
            }
            cineDTO.setLogoSrc(cine.getLogoSrc());
            cineDTO.setLogoContentType(cine.getLogoContentType());
        }

        byte[] banner = cine.getBanner();
        if (cine.getBannerUUID() != null || (banner != null && banner.length > 0)) {
            if (cine.getBannerUUID() != null) {
                cineDTO.setBannerUUID(cine.getBannerUUID());
            } else {
                cineDTO.setBanner(cine.getBanner());
            }
            cineDTO.setBannerSrc(cine.getBannerSrc());
            cineDTO.setBannerContentType(cine.getBannerContentType());
        }

        return cineDTO;
    }

    public static Cine cineDTOToCine(CineDTO cineDTO, boolean crearConImagen) {
        Cine cine = new Cine();

        cine.setId(cineDTO.getId());
        cine.setCodigo(cineDTO.getCodigo());
        cine.setNombre(cineDTO.getNombre());
        cine.setCif(cineDTO.getCif());
        cine.setDireccion(cineDTO.getDireccion());
        cine.setCodigoMunicipio(cineDTO.getCodigoMunicipio());
        cine.setNombreMunicipio(cineDTO.getNombreMunicipio());
        cine.setCp(cineDTO.getCp());
        cine.setEmpresa(cineDTO.getEmpresa());
        cine.setCodigoRegistro(cineDTO.getCodigoRegistro());
        cine.setTelefono(cineDTO.getTelefono());
        cine.setIva(cineDTO.getIva());
        cine.setUrlPublic(cineDTO.getUrlPublic());
        cine.setUrlPrivacidad(cineDTO.getUrlPrivacidad());
        cine.setUrlCancelacion(cineDTO.getUrlCancelacion());
        cine.setUrlComoLlegar(cineDTO.getUrlComoLlegar());
        cine.setUrlPieEntrada(cineDTO.getUrlPieEntrada());
        cine.setMailFrom(cineDTO.getMailFrom());
        cine.setLogoReport(cineDTO.getLogoReport());
        cine.setShowButacasQueHanEntradoEnDistintoColor(cineDTO.getShowButacasQueHanEntradoEnDistintoColor());
        cine.setLangs(cineDTO.getLangs());
        cine.setDefaultLang(cineDTO.getDefaultLang());
        cine.setShowIVA(cineDTO.getShowIVA());
        cine.setApiKey(cineDTO.getApiKey());
        cine.setLimiteEntradasGratuitasPorCompra(cineDTO.getLimiteEntradasGratuitasPorCompra());
        cine.setComision(cineDTO.getComision());
        cine.setEmail(cineDTO.getEmail());
        cine.setRegistroMercantil(cineDTO.getRegistroMercantil());
        cine.setClasePostventa(cineDTO.getClasePostventa());
        cine.setInformes(cineDTO.getInformes());
        cine.setInformesGenerales(cineDTO.getInformesGenerales());
        cine.setFormasPago(cineDTO.getFormasPago());
        cine.setMenuTpv(cineDTO.isMenuTpv());
        cine.setMenuClientes(cineDTO.isMenuClientes());
        cine.setMenuIntegraciones(cineDTO.isMenuIntegraciones());
        cine.setMenuSalas(cineDTO.isMenuSalas());
        cine.setMenuPerfil(cineDTO.isMenuPerfil());
        cine.setMenuICAA(cineDTO.isMenuICAA());
        cine.setMenuAyuda(cineDTO.isMenuTaquilla());
        cine.setMenuTaquilla(cineDTO.isMenuTaquilla());
        cine.setMenuVentaAnticipada(cineDTO.isMenuVentaAnticipada());
        cine.setMenuAbonos(cineDTO.isMenuAbonos());
        cine.setShowComoNosConociste(cineDTO.isShowComoNosConociste());
        cine.setCheckContratacion(cineDTO.isCheckContratacion());
        cine.setLimiteNoGratuitas(cineDTO.getLimiteNoGratuitas());
        cine.setShowLogoEntrada(cineDTO.isShowLogoEntrada());
        cine.setNoNumeradasSecuencial(cineDTO.isNoNumeradasSecuencia());

        if (cineDTO.getLogo() != null || cineDTO.getLogoUUID() != null)
        {
            if (crearConImagen)
            {
                cine.setLogo(cineDTO.getLogo());
                cine.setLogoUUID(cineDTO.getLogoUUID());
            }
            cine.setLogoContentType(cineDTO.getLogoContentType());
            cine.setLogoSrc(cineDTO.getLogoSrc());
        }

        if (cineDTO.getBanner() != null || cineDTO.getBannerUUID() != null)
        {
            if (crearConImagen)
            {
                cine.setBanner(cineDTO.getBanner());
                cine.setBannerUUID(cineDTO.getBannerUUID());
            }
            cine.setBannerContentType(cineDTO.getBannerContentType());
            cine.setBannerSrc(cineDTO.getBannerSrc());
        }

        return cine;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoIcaa() {
        return codigoIcaa;
    }

    public void setCodigoIcaa(String codigoIcaa) {
        this.codigoIcaa = codigoIcaa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCodigoRegistro() {
        return codigoRegistro;
    }

    public void setCodigoRegistro(String codigoRegistro) {
        this.codigoRegistro = codigoRegistro;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    public String getUrlPublic() {
        return urlPublic;
    }

    public void setUrlPublic(String urlPublic) {
        this.urlPublic = urlPublic;
    }

    public String getUrlPrivacidad() {
        return urlPrivacidad;
    }

    public void setUrlPrivacidad(String urlPrivacidad) {
        this.urlPrivacidad = urlPrivacidad;
    }

    public String getUrlCancelacion() {
        return urlCancelacion;
    }

    public void setUrlCancelacion(String urlCancelacion) {
        this.urlCancelacion = urlCancelacion;
    }

    public String getUrlComoLlegar() {
        return urlComoLlegar;
    }

    public void setUrlComoLlegar(String urlComoLlegar) {
        this.urlComoLlegar = urlComoLlegar;
    }

    public String getUrlPieEntrada() {
        return urlPieEntrada;
    }

    public void setUrlPieEntrada(String urlPieEntrada) {
        this.urlPieEntrada = urlPieEntrada;
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    public String getLogoReport() {
        return logoReport;
    }

    public void setLogoReport(String logoReport) {
        this.logoReport = logoReport;
    }

    public static void checkValidity(String codi) throws RegistroSerializaException {
        if (codi == null)
            throw new RegistroSerializaException(GeneralPARException.CODIGO_CINE_NULO_CODE);

        if (codi.length() != 3)
            throw new RegistroSerializaException(GeneralPARException.FORMATO_CODIGO_CINE_INCORRECTO_CODE);
    }

    public Boolean getShowButacasQueHanEntradoEnDistintoColor() {
        return showButacasQueHanEntradoEnDistintoColor;
    }

    public void setShowButacasQueHanEntradoEnDistintoColor(Boolean showButacasQueHanEntradoEnDistintoColor) {
        this.showButacasQueHanEntradoEnDistintoColor = showButacasQueHanEntradoEnDistintoColor;
    }

    public String getLangs() {
        return langs;
    }

    public void setLangs(String langs) {
        this.langs = langs;
    }

    public String getDefaultLang() {
        return defaultLang;
    }

    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    public Boolean getShowIVA() {
        return showIVA != null ? showIVA : false;
    }

    public void setShowIVA(Boolean showIVA) {
        this.showIVA = showIVA;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Integer getLimiteEntradasGratuitasPorCompra() {
        return limiteEntradasGratuitasPorCompra;
    }

    public void setLimiteEntradasGratuitasPorCompra(Integer limiteEntradasGratuitasPorCompra) {
        this.limiteEntradasGratuitasPorCompra = limiteEntradasGratuitasPorCompra;
    }

    public BigDecimal getComision() {
        return comision;
    }

    public void setComision(BigDecimal comision) {
        this.comision = comision;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRegistroMercantil(String registroMercantil) {
        this.registroMercantil = registroMercantil;
    }

    public String getEmail() {
        return email != null ? email : "";
    }

    public String getRegistroMercantil() {
        return registroMercantil;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public String getLogoSrc() {
        return logoSrc;
    }

    public void setLogoSrc(String logoSrc) {
        this.logoSrc = logoSrc;
    }

    public String getLogoUUID() {
        return logoUUID;
    }

    public void setLogoUUID(String logoUUID) {
        this.logoUUID = logoUUID;
    }

    public byte[] getBanner() {
        return banner;
    }

    public void setBanner(byte[] banner) {
        this.banner = banner;
    }

    public String getBannerContentType() {
        return bannerContentType;
    }

    public void setBannerContentType(String bannerContentType) {
        this.bannerContentType = bannerContentType;
    }

    public String getBannerSrc() {
        return bannerSrc;
    }

    public void setBannerSrc(String bannerSrc) {
        this.bannerSrc = bannerSrc;
    }

    public String getBannerUUID() {
        return bannerUUID;
    }

    public void setBannerUUID(String bannerUUID) {
        this.bannerUUID = bannerUUID;
    }

    public String getClasePostventa() {
        return clasePostventa;
    }

    public void setClasePostventa(String clasePostventa) {
        this.clasePostventa = clasePostventa;
    }

    public String getInformes() {
        return informes;
    }

    public void setInformes(String informes) {
        this.informes = informes;
    }

    public String getInformesGenerales() {
        return informesGenerales;
    }

    public void setInformesGenerales(String informesGenerales) {
        this.informesGenerales = informesGenerales;
    }

    public String getFormasPago() {
        return formasPago;
    }

    public void setFormasPago(String formasPago) {
        this.formasPago = formasPago;
    }

    public boolean isMenuTpv() {
        return menuTpv;
    }

    public void setMenuTpv(boolean menuTpv) {
        this.menuTpv = menuTpv;
    }

    public boolean isMenuClientes() {
        return menuClientes;
    }

    public void setMenuClientes(boolean menuClientes) {
        this.menuClientes = menuClientes;
    }

    public boolean isMenuICAA() {
        return menuICAA;
    }

    public void setMenuICAA(boolean menuICAA) {
        this.menuICAA = menuICAA;
    }

    public boolean isMenuIntegraciones() {
        return menuIntegraciones;
    }

    public void setMenuIntegraciones(boolean menuIntegraciones) {
        this.menuIntegraciones = menuIntegraciones;
    }

    public boolean isMenuSalas() {
        return menuSalas;
    }

    public void setMenuSalas(boolean menuSalas) {
        this.menuSalas = menuSalas;
    }

    public boolean isMenuPerfil() {
        return menuPerfil;
    }

    public void setMenuPerfil(boolean menuPerfil) {
        this.menuPerfil = menuPerfil;
    }

    public boolean isMenuAyuda() {
        return menuAyuda;
    }

    public void setMenuAyuda(boolean menuAyuda) {
        this.menuAyuda = menuAyuda;
    }

    public boolean isMenuTaquilla() {
        return menuTaquilla;
    }

    public void setMenuTaquilla(boolean menuTaquilla) {
        this.menuTaquilla = menuTaquilla;
    }

    public boolean isMenuVentaAnticipada() {
        return menuVentaAnticipada;
    }

    public void setMenuVentaAnticipada(boolean menuVentaAnticipada) {
        this.menuVentaAnticipada = menuVentaAnticipada;
    }

    public boolean isMenuAbonos() {
        return menuAbonos;
    }

    public void setMenuAbonos(boolean menuAbonos) {
        this.menuAbonos = menuAbonos;
    }

    public boolean isShowComoNosConociste() {
        return showComoNosConociste;
    }

    public void setShowComoNosConociste(boolean showComoNosConociste) {
        this.showComoNosConociste = showComoNosConociste;
    }

    public boolean isCheckContratacion() {
        return checkContratacion;
    }

    public void setCheckContratacion(boolean checkContratacion) {
        this.checkContratacion = checkContratacion;
    }

    public Boolean getLimiteNoGratuitas() {
        return limiteNoGratuitas;
    }

    public void setLimiteNoGratuitas(Boolean limiteNoGratuitas) {
        this.limiteNoGratuitas = limiteNoGratuitas;
    }

    public boolean isShowLogoEntrada() {
        return showLogoEntrada;
    }

    public void setShowLogoEntrada(boolean showLogoEntrada) {
        this.showLogoEntrada = showLogoEntrada;
    }

    public boolean isNoNumeradasSecuencial() {
        return noNumeradasSecuencial;
    }

    public void setNoNumeradasSecuencial(boolean noNumeradasSecuencial) {
        this.noNumeradasSecuencial = noNumeradasSecuencial;
    }

    public void serializeTemplate(Template template, boolean mobile) {
        template.put("cineNombre", this.getNombre());
        template.put("cineDireccion", this.getDireccion());
        template.put("cineEmpresa", this.getEmpresa());
        template.put("cineMunicipio", this.getNombreMunicipio());
        template.put("cineTelefono", this.getTelefono());
        template.put("cineCif", this.getCif());
        template.put("logo", this.getLogoReport());
        template.put("cineEmail", this.getEmail());
        template.put("cineRegistroMercantil", this.getRegistroMercantil());
        template.put("cineCorreoPostal", this.getDireccion() + ", " + this.getCp() + " " + this.getNombreMunicipio());

        if (mobile) {
            template.put("css", String.format("/statics/%s/css/mobile.css", this.getCodigo()));
        }
    }
}