package es.uji.apps.par.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.db.PreciosSesionDTO;
import es.uji.apps.par.db.SesionDTO;

@XmlRootElement
public class PreciosEditablesSesion
{

	private long id;
	private Localizacion localizacion;
	private Sesion sesion;
	private BigDecimal descuento;
	private BigDecimal invitacion;
	private BigDecimal precio;
	private BigDecimal precioAnticipado;
	private BigDecimal aulaTeatro;
	private Tarifa tarifa;

	public PreciosEditablesSesion()
	{

	}

	public PreciosEditablesSesion(PreciosPlantilla preciosPlantilla)
	{
		this.id = preciosPlantilla.getId();
		this.localizacion = preciosPlantilla.getLocalizacion();
		this.precio = preciosPlantilla.getPrecio();
		this.precioAnticipado = preciosPlantilla.getPrecioAnticipado();
		this.tarifa = preciosPlantilla.getTarifa();
	}

	public PreciosEditablesSesion(PreciosSesionDTO preciosSesion)
	{
		this.id = preciosSesion.getId();
		this.localizacion = Localizacion.localizacionDTOtoLocalizacion(preciosSesion.getParLocalizacione());
		this.precio =  preciosSesion.getPrecio() != null ? preciosSesion.getPrecio().setScale(2, RoundingMode.HALF_UP) : preciosSesion.getPrecio();
		this.precioAnticipado = preciosSesion.getPrecioAnticipado() != null ? preciosSesion.getPrecioAnticipado().setScale(2, RoundingMode.HALF_UP) : preciosSesion.getPrecioAnticipado();
		this.tarifa = Tarifa.tarifaDTOToTarifa(preciosSesion.getParTarifa());

		if (preciosSesion.getParSesione() != null) {
			this.sesion = Sesion.SesionDTOToSesion(preciosSesion.getParSesione());
		}
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Localizacion getLocalizacion()
	{
		return localizacion;
	}

	public void setLocalizacion(Localizacion localizacion)
	{
		this.localizacion = localizacion;
	}

	public BigDecimal getDescuento()
	{
		return descuento != null ? descuento.setScale(2, RoundingMode.HALF_UP) : descuento;
	}

	public void setDescuento(BigDecimal descuento)
	{
		this.descuento = descuento;
	}

	public BigDecimal getInvitacion()
	{
		return invitacion != null ? invitacion.setScale(2, RoundingMode.HALF_UP) : invitacion;
	}

	public void setInvitacion(BigDecimal invitacion)
	{
		this.invitacion = invitacion;
	}

	public BigDecimal getPrecio()
	{
		return precio != null ? precio.setScale(2, RoundingMode.HALF_UP) : precio;
	}

	public void setPrecio(BigDecimal precio)
	{
		this.precio = precio;
	}

	public BigDecimal getPrecioAnticipado() {
		return precioAnticipado;
	}

	public void setPrecioAnticipado(BigDecimal precioAnticipado) {
		this.precioAnticipado = precioAnticipado;
	}

	public BigDecimal getAulaTeatro()
	{
		return aulaTeatro != null ? aulaTeatro.setScale(2, RoundingMode.HALF_UP) : aulaTeatro;
	}

	public void setAulaTeatro(BigDecimal aulaTeatro)
	{
		this.aulaTeatro = aulaTeatro;
	}

	public Sesion getSesion()
	{
		return sesion;
	}

	public void setSesion(Sesion sesion)
	{
		this.sesion = sesion;
	}

	public static PreciosSesionDTO precioSesionToPrecioSesionDTO(PreciosEditablesSesion preciosSesion)
	{
		PreciosSesionDTO preciosSesionDTO = new PreciosSesionDTO();
		preciosSesionDTO.setPrecio(preciosSesion.getPrecio());
		preciosSesionDTO.setPrecioAnticipado(preciosSesion.getPrecioAnticipado());
		preciosSesionDTO.setParTarifa(Tarifa.toDTO(preciosSesion.getTarifa()));
		preciosSesionDTO.setParLocalizacione(Localizacion.localizacionToLocalizacionDTO(preciosSesion.getLocalizacion()));

		if (preciosSesion.getSesion() != null)
		{
			SesionDTO sesionDTO = new SesionDTO();
			sesionDTO.setId(preciosSesion.getSesion().getId());
			preciosSesionDTO.setParSesione(sesionDTO);
		}

		return preciosSesionDTO;
	}

	public Tarifa getTarifa()
	{
		return tarifa;
	}

	public void setTarifa(Tarifa tarifa)
	{
		this.tarifa = tarifa;
	}
}
