package es.uji.apps.par.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the PAR_SALAS database table.
 * 
 */
@Entity
@Table(name = "PAR_SALAS")
public class SalaDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "PAR_SALAS_ID_GENERATOR", sequenceName = "HIBERNATE_SEQUENCE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAR_SALAS_ID_GENERATOR")
    private long id;

    @Column(name = "CODIGO")
    private String codigo;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "FORMATO")
    private String formato;

    @Column(name = "SUBTITULO")
    private String subtitulo;

    @Column(name = "HTML_TEMPLATE_NAME")
    private String htmlTemplateName;

    @Column(name="ASIENTOS_NUMERADOS")
    private Boolean asientosNumerados;

    @Column(name="URL_COMO_LLEGAR")
    private String urlComoLlegar;

    @Column(name="CLASE_ENTRADA_TAQUILLA")
    private String claseEntradaTaquilla;

    @Column(name="CLASE_ENTRADA_ONLINE")
    private String claseEntradaOnline;

    @Column(name = "DIRECCION")
    private String direccion;

    @Column(name="IS_AFORO_POR_COMPRAS")
    private boolean aforoPorCompras;

    @ManyToOne
    @JoinColumn(name = "CINE_ID")
    private CineDTO parCine;

    @OneToMany(mappedBy = "parSala", fetch = FetchType.LAZY)
    private List<PlantaSalaDTO> parPlantas;

    @OneToMany(mappedBy = "parSala", fetch = FetchType.LAZY)
    private List<SesionDTO> parSesiones;
    
    @OneToMany(mappedBy = "sala", fetch = FetchType.LAZY)
    private List<LocalizacionDTO> parLocalizaciones;
    
    @OneToMany(mappedBy = "sala", fetch = FetchType.LAZY)
    private List<PlantillaDTO> parPlantillas;

	@OneToMany(mappedBy = "parSala", fetch = FetchType.LAZY)
	private List<SalasUsuarioDTO> parSalasUsuario;

    @OneToMany(mappedBy="sala")
    private List<TpvsSalasDTO> parTpvsSalas;

    public SalaDTO()
    {
    }

    public SalaDTO(CineDTO cineDTO, String codigo, String nombre, String direccion, String tipo, String formato, String subtitulo)
    {
        this.codigo = codigo;
        this.nombre = nombre;
        this.direccion = direccion;
        this.tipo = tipo;
        this.formato = formato;
        this.subtitulo = subtitulo;
        this.parCine = cineDTO;
        this.parPlantas = new ArrayList<PlantaSalaDTO>();
    }

    public SalaDTO(long id) {
		setId(id);
	}

	public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getFormato()
    {
        return formato;
    }

    public void setFormato(String formato)
    {
        this.formato = formato;
    }

    public String getSubtitulo()
    {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo)
    {
        this.subtitulo = subtitulo;
    }

    public CineDTO getParCine()
    {
        return parCine;
    }

    public void setParCine(CineDTO parCine)
    {
        this.parCine = parCine;
    }

    public List<PlantaSalaDTO> getParPlantas()
    {
        return parPlantas;
    }

    public void setParPlantas(List<PlantaSalaDTO> parPlantas)
    {
        this.parPlantas = parPlantas;
    }

    public List<SesionDTO> getParSesiones()
    {
        return parSesiones;
    }

    public void setParSesiones(List<SesionDTO> parSesiones)
    {
        this.parSesiones = parSesiones;
    }

	public List<LocalizacionDTO> getParLocalizaciones() {
		return parLocalizaciones;
	}

	public void setParLocalizaciones(List<LocalizacionDTO> parLocalizaciones) {
		this.parLocalizaciones = parLocalizaciones;
	}

	public List<PlantillaDTO> getParPlantillas() {
		return parPlantillas;
	}

	public void setParPlantillas(List<PlantillaDTO> parPlantillas) {
		this.parPlantillas = parPlantillas;
	}

    public String getHtmlTemplateName() {
        return htmlTemplateName;
    }

    public void setHtmlTemplateName(String htmlTemplateName) {
        this.htmlTemplateName = htmlTemplateName;
    }

	public List<SalasUsuarioDTO> getParSalasUsuario() {
		return parSalasUsuario;
	}

	public void setParSalasUsuario(List<SalasUsuarioDTO> parSalasUsuario) {
		this.parSalasUsuario = parSalasUsuario;
	}

    public Boolean getAsientosNumerados()
    {
        return asientosNumerados;
    }

    public void setAsientosNumerados(Boolean asientosNumerados)
    {
        this.asientosNumerados = asientosNumerados;
    }

    public List<TpvsSalasDTO> getParTpvsSalas() {
        return parTpvsSalas;
    }

    public void setParTpvsSalas(List<TpvsSalasDTO> parTpvsSalas) {
        this.parTpvsSalas = parTpvsSalas;
    }

    public String getUrlComoLlegar() {
        return urlComoLlegar;
    }

    public void setUrlComoLlegar(String urlComoLlegar) {
        this.urlComoLlegar = urlComoLlegar;
    }

    public String getClaseEntradaTaquilla() {
        return claseEntradaTaquilla;
    }

    public void setClaseEntradaTaquilla(String claseEntradaTaquilla) {
        this.claseEntradaTaquilla = claseEntradaTaquilla;
    }

    public String getClaseEntradaOnline() {
        return claseEntradaOnline;
    }

    public void setClaseEntradaOnline(String claseEntradaOnline) {
        this.claseEntradaOnline = claseEntradaOnline;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isAforoPorCompras() {
        return aforoPorCompras;
    }

    public void setAforoPorCompras(Boolean aforoPorCompras) {
        this.aforoPorCompras = aforoPorCompras != null ? aforoPorCompras : false;
    }
}