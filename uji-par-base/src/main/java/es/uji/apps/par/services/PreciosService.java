package es.uji.apps.par.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.par.enums.TipoTarifa;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.model.PreciosEditablesSesion;
import es.uji.apps.par.model.PreciosSesion;
import es.uji.apps.par.model.Sesion;

@Service
public class PreciosService {
    @Autowired
    private SesionesService sesionesService;

    public PreciosSesion getPrecioAbono(
        Long eventoId,
        String userUID
    ) {
        List<Sesion> sesiones = sesionesService.getSesiones(eventoId, "", 0, Integer.MAX_VALUE, userUID);
        List<PreciosSesion> preciosSesion = getPreciosSesion(sesiones.get(0).getId(), userUID);

        if (preciosSesion.size() > 0) {
            return preciosSesion.get(0);
        } else {
            return null;
        }
    }

    public List<PreciosSesion> getPreciosSesion(
        Long sesionId,
        String userUID
    ) {
        List<PreciosEditablesSesion> preciosEditablesSesion =
            sesionesService.getPreciosEditablesSesion(sesionId, "", 0, Integer.MAX_VALUE, true, userUID);

        return preciosEditablesSesion.stream().map(PreciosSesion::new).collect(Collectors.toList());
    }

    public List<PreciosSesion> getPreciosSesionPublicos(
        Long sesionId,
        String userUID
    ) {
        List<PreciosEditablesSesion> preciosEditablesSesion =
            sesionesService.getPreciosEditablesSesion(sesionId, "", 0, Integer.MAX_VALUE, false, userUID);

        return preciosEditablesSesion.stream().map(PreciosSesion::new).collect(Collectors.toList());
    }

    public int getTotalPreciosSesion(Long sesionId) {
        return sesionesService.getTotalPreciosSesion(sesionId);
    }

    private Map<String, Map<Long, PreciosSesion>> getPreciosSesionPorLocalizacion(
        Long sesionId,
        boolean mostrarTarifasInternas,
        TipoTarifa tipoTarifa,
        String userUID
    ) {
        Map<String, Map<Long, PreciosSesion>> resultado = new HashMap<>();

        List<PreciosSesion> preciosSesion = getPreciosSesion(sesionId, userUID);
        for (Localizacion localizacion : localizacionesPorSesion(preciosSesion)) {
            Map<Long, PreciosSesion> tarifasPrecios = new HashMap<>();
            for (PreciosSesion precio : preciosSesion) {
                if (precio.getLocalizacion().getCodigo().equals(localizacion.getCodigo())) {
                    if (!precio.getTarifa().getIsPublica().equals("on") && !mostrarTarifasInternas
                    || tipoTarifa.equals(TipoTarifa.ABONO) && !precio.getTarifa().getIsAbono().equals("on")
                    || tipoTarifa.equals(TipoTarifa.NORMAL) && precio.getTarifa().getIsAbono().equals("on"))
                        continue;

                    tarifasPrecios.put(precio.getTarifa().getId(), precio);
                }
            }
            resultado.put(localizacion.getCodigo(), tarifasPrecios);
        }

        return resultado;
    }

    public Map<String, Map<Long, PreciosSesion>> getPreciosSesionAbonosPublicosPorLocalizacion(
        long sesionId,
        String userUID
    ) {
        return getPreciosSesionPorLocalizacion(sesionId, false, TipoTarifa.ABONO, userUID);
    }

    public Map<String, Map<Long, PreciosSesion>> getPreciosSesionNormalPublicosPorLocalizacion(
        long sesionId,
        String userUID
    ) {
        return getPreciosSesionPorLocalizacion(sesionId, false, TipoTarifa.NORMAL, userUID);
    }

    public Map<String, Map<Long, PreciosSesion>> getPreciosSesionNormalPorLocalizacion(
        long sesionId,
        String userUID
    ) {
        return getPreciosSesionPorLocalizacion(sesionId, true, TipoTarifa.NORMAL, userUID);
    }

    public Map<String, Map<Long, PreciosSesion>> getPreciosSesionPorLocalizacion(
        long sesionId,
        String userUID
    ) {
        return getPreciosSesionPorLocalizacion(sesionId, true, TipoTarifa.TODAS, userUID);
    }

    private List<Localizacion> localizacionesPorSesion(List<PreciosSesion> preciosSesion) {
        List<Localizacion> localizaciones = new ArrayList<Localizacion>();

        for (PreciosSesion precioSesion : preciosSesion) {
            Localizacion localizacion = precioSesion.getLocalizacion();
            if (!localizaciones.contains(localizacion))
                localizaciones.add(localizacion);
        }

        return localizaciones;
    }
}
