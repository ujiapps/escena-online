package es.uji.apps.par.services;

import com.google.common.base.Strings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.PersistenceException;

import es.uji.apps.par.dao.AbonosDAO;
import es.uji.apps.par.dao.TarifasDAO;
import es.uji.apps.par.db.AbonoDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.exceptions.AbonoNoValidoException;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.TarifaConEventosException;
import es.uji.apps.par.exceptions.UsuarioObligatorioException;
import es.uji.apps.par.model.Abono;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.PreciosEditablesSesion;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.TipoEvento;

@Service
public class AbonosService {

    @Autowired
    private AbonosDAO abonosDAO;

    @Autowired
    private EventosService eventosService;

    @Autowired
    private TiposEventosService tiposEventosService;

    @Autowired
    protected TarifasDAO tarifasDAO;

    @Autowired
    private SalasService salasService;

    @Autowired
    private AbonadosService abonadosService;

    @Autowired
    private SesionesService sesionesService;

    @Autowired
    private UsersService usersService;

    public void removeAbonado(
        Long id,
        String userUID
    ) {
        abonadosService.removeAbonado(id, userUID);
    }

    public List<Abono> getAll(
        String sort,
        int start,
        int limit,
        String userUID
    ) {
        List<AbonoDTO> abonosDTO = abonosDAO.getAll(sort, start, limit, userUID);
        List<Abono> abonos = abonosDTO.stream().map(Abono::new).collect(Collectors.toList());
        for (Abono abono : abonos)
        {
            if (abono.getFechaInicioVentaOnline() != null)
                abono.setFechaInicioVentaOnlineWithDate(new Date(abono.getFechaInicioVentaOnline().getTime()/1000));
            if (abono.getFechaFinVentaOnline() != null)
                abono.setFechaFinVentaOnlineWithDate(new Date(abono.getFechaFinVentaOnline().getTime()/1000));
            if (abono.getFechaFinVentaAnticipada() != null)
                abono.setFechaFinVentaAnticipadaWithDate(new Date(abono.getFechaFinVentaAnticipada().getTime()/1000));
        }
        return abonos;
    }

    public Abono getById(
        long abonoId
    ) {
        AbonoDTO abono = abonosDAO.getById(abonoId);
        return new Abono(abono);
    }

    public long getTotal(String userUID) {
        return abonosDAO.getTotal(userUID);
    }

    @Transactional
    public Abono add(
        Abono abono,
        String userUID
    ) throws IOException {
        if (userUID != null) {
            if (checkAbono(abono)) {
                Cine cine = usersService.getUserCineByUserUID(userUID);

                TarifaDTO tarifaDTO = Abono.toTarifaDTO(abono, cine);
                tarifaDTO = tarifasDAO.add(tarifaDTO);

                addEventoSesionAbono(abono, userUID, cine);
                return getById(tarifaDTO.getId());
            } else {
                throw new AbonoNoValidoException();
            }
        } else {
            throw new UsuarioObligatorioException();
        }
    }

    private void addEventoSesionAbono(
        Abono abono,
        String userUID,
        Cine cine
    ) throws IOException {
        if (abono.getCanalInternet() != null && abono.getCanalInternet() && checkAbonoVentaOnline(abono)) {
            TipoEvento tipoEvento = tiposEventosService.getTiposEventos(null, 0, 1, userUID).get(0);

            Evento evento = Abono.toEvento(abono, tipoEvento, cine);
            evento = eventosService.addEvento(evento, userUID);

            TarifaDTO tarifaPublica = tarifasDAO.getPublicPorDefecto(userUID);
            PreciosEditablesSesion preciosEditablesSesion = Abono.toPrecioEditableSesion(abono, tarifaPublica);

            Sala sala = salasService.getSalaByLocalizacionId(abono.getParLocalizacion().getId(), userUID);
            Sesion sesion = Abono.toSesion(abono, sala, preciosEditablesSesion);
            sesionesService.addSesion(evento.getId(), sesion, false, userUID);
        }
    }

    @Transactional
    public Abono update(
        Abono abono,
        String userUID
    ) throws IOException {
        if (userUID != null) {
            if (checkAbono(abono)) {
                Cine cine = usersService.getUserCineByUserUID(userUID);

                Abono oldAbono = getById(abono.getId());
                if (oldAbono.getParEvento() != null) {
                    Evento eventoAbono = eventosService.getEvento(oldAbono.getParEvento().getId(), userUID);
                    updateEventoSesionAbono(abono, userUID, cine, eventoAbono);
                }
                else {
                    addEventoSesionAbono(abono, userUID, cine);
                }

                TarifaDTO tarifaDTO = Abono.toTarifaDTO(abono, cine);
                tarifasDAO.update(tarifaDTO);
                return getById(tarifaDTO.getId());
            } else {
                throw new AbonoNoValidoException();
            }
        } else {
            throw new UsuarioObligatorioException();
        }
    }

    private void updateEventoSesionAbono(
        Abono abono,
        String userUID,
        Cine cine,
        Evento eventoAbono
    ) throws IOException {
        if (abono.getCanalInternet() != null && abono.getCanalInternet() && checkAbonoVentaOnline(abono)) {
            Evento evento = Abono.toEvento(abono, new TipoEvento(eventoAbono.getTipoEvento()), cine);
            evento.setId(eventoAbono.getId());
            eventosService.updateEvento(evento, userUID);

            TarifaDTO tarifaPublica = tarifasDAO.getPublicPorDefecto(userUID);
            PreciosEditablesSesion preciosEditablesSesion =
                Abono.toPrecioEditableSesion(abono, tarifaPublica);

            Sala sala = salasService.getSalaByLocalizacionId(abono.getParLocalizacion().getId(), userUID);
            Sesion sesion = Abono.toSesion(abono, sala, preciosEditablesSesion);

            List<Sesion> sesiones = sesionesService.getSesiones(evento.getId(), userUID);
            if (sesiones.size() > 0) {
                sesion.setId(sesiones.get(0).getId());
                sesionesService.updateSesion(evento.getId(), sesion, false, false, userUID);
            } else {
                sesionesService.addSesion(evento.getId(), sesion, false, userUID);
            }
        } else {
            eventosService.removeEvento(eventoAbono.getId());
        }
    }

    private boolean checkAbono(Abono abono) {
        if (Strings.isNullOrEmpty(abono.getNombre())) {
            throw new CampoRequeridoException("Nombre");
        } else {
            return true;
        }
    }

    private boolean checkAbonoVentaOnline(Abono abono) {
        if (abono.getParLocalizacion() == null) {
            throw new CampoRequeridoException("Aforo");
        } else if (abono.getPrecio() == null) {
            throw new CampoRequeridoException("Precio");
        } else if (abono.getFechaInicioVentaOnline() == null) {
            throw new CampoRequeridoException("Fecha inicio");
        } else if (abono.getFechaFinVentaOnline() == null) {
            throw new CampoRequeridoException("Fecha fin");
        } else {
            return true;
        }
    }

    @Transactional
    public void remove(long id) {
        try {
            AbonoDTO abonoDTO = abonosDAO.getById(id);

            tarifasDAO.remove(id);
            if (abonoDTO.getParEvento() != null) {
                eventosService.removeEvento(abonoDTO.getParEvento().getId());
            }
        } catch (PersistenceException e) {
            throw new TarifaConEventosException();
        }
    }
}
