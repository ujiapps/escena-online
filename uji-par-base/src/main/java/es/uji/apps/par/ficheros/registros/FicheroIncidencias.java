package es.uji.apps.par.ficheros.registros;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.List;

import es.uji.apps.par.exceptions.RegistroSerializaException;

public class FicheroIncidencias
{
    private RegistroBuzonIncidencias registroBuzonIncidencias;
    private List<RegistroSemanaIncidencia> registroSemanaIncidencias;

    public void setRegistroBuzonIncidencias(RegistroBuzonIncidencias registroBuzonIncidencias) {
        this.registroBuzonIncidencias = registroBuzonIncidencias;
    }

    public void setRegistroSemanaIncidencias(List<RegistroSemanaIncidencia> registroSemanaIncidencias) {
        this.registroSemanaIncidencias = registroSemanaIncidencias;
    }

    public String serializa() throws RegistroSerializaException
    {
        StringBuffer buff = new StringBuffer();

        buff.append(registroBuzonIncidencias.serializa());
        buff.append("\n");

        for (RegistroSemanaIncidencia registroSemanaIncidencia : registroSemanaIncidencias)
        {
            buff.append(registroSemanaIncidencia.serializa());
            buff.append("\n");
        }

        return buff.toString();
    }

	public byte[] toByteArray() throws RegistroSerializaException {
		String contenidor = this.serializa();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PrintWriter out = new PrintWriter(outputStream);
		out.write(contenidor);
		out.close();
		return outputStream.toByteArray();
	}
}
