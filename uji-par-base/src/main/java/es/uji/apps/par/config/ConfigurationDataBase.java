package es.uji.apps.par.config;

import com.google.common.base.Strings;

import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.TipoInforme;
import es.uji.apps.par.services.UsersService;

public class ConfigurationDataBase implements ConfigurationSelector {
    UsersService usersService;

    HttpServletRequest currentRequest;

    @Autowired
    public ConfigurationDataBase(
        UsersService usersService,
        HttpServletRequest currentRequest
    ) {
        this.usersService = usersService;
        this.currentRequest = currentRequest;
    }

    public String getUrlPublic() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getUrlPublic();
    }

    public String getUrlPublicLimpio() {
        return getUrlPublic();
    }

    public String getUrlAdmin() {
        return String.format("%s://%s:%s/par", this.currentRequest.getScheme(), this.currentRequest.getServerName(),
            this.currentRequest.getServerPort());
    }

    public String getHtmlTitle() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getNombre();
    }

    public String getMailFrom() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getMailFrom();
    }

    public String getUrlComoLlegar() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getUrlComoLlegar();
    }

    public String getUrlCondicionesPrivacidad() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getUrlPrivacidad();
    }

    public String getUrlCondicionesCancelacion() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getUrlCancelacion();
    }

    public String getUrlPieEntrada() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getUrlPieEntrada();
    }

    public String getLogoReport() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getLogoReport();
    }

    public String getClasePostventa() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getClasePostventa();
    }

    public String getInformes() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        String informes = cine.getInformes();
        if (Strings.isNullOrEmpty(informes)) {
            return "[{\"id\":\"informeEventosNoAnuladas\", \"nombreCA\":\"Esdeveniment\", \"nombreES\":\"Evento\"}, {\"id\":\"informeSesionNoAnuladas\", \"nombreCA\":\"Sessió\", \"nombreES\":\"Sesión\"}, {\"id\":\"informeEventos\", \"nombreCA\":\"Esdeveniment amb anul·lades\", \"nombreES\":\"Evento con anuladas\"}, {\"id\":\"informeSesion\", \"nombreCA\":\"Sessió amb anul·lades\", \"nombreES\":\"Sesión con anuladas\"}]";
        } else {
            return informes;
        }
    }

    public String getInformesGenerales() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        String informesGenerales = cine.getInformesGenerales();
        if (Strings.isNullOrEmpty(informesGenerales)) {
            return TipoInforme.getDefaultGenerales();
        } else {
            return informesGenerales;
        }
    }

    @Override
    public String getNombreMunicipio() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getNombreMunicipio();
    }

    public String getApiKey() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.getApiKey();
    }

    @Override
    public String getLangsAllowed() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        String langsAllowed = cine.getLangs();

        if (langsAllowed != null && langsAllowed.length() > 0)
            return langsAllowed;
        return "[{'locale':'es', 'alias': 'Español'}]";
    }

    @Override
    public boolean getLocalizacionEnValenciano() {
        String langsAllowed = getLangsAllowed();
        return (langsAllowed.toUpperCase().contains("VALENCI") || langsAllowed.toUpperCase().contains("CATAL"));
    }

    @Override
    public String getIdiomaPorDefecto() {
        try {
            String serverName = this.currentRequest.getServerName();
            Cine cine = usersService.getUserCineByServerName(serverName);

            String defaultLang = cine.getDefaultLang();
            if (defaultLang != null && defaultLang.length() > 0)
                return defaultLang;
        } catch (IllegalStateException e) {
        }

        return "es";
    }

    @Override
    public boolean showButacasHanEntradoEnDistintoColor() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return (cine.getShowButacasQueHanEntradoEnDistintoColor() != null && cine
            .getShowButacasQueHanEntradoEnDistintoColor()) ? true : false;
    }

    @Override
    public boolean showIVA() {
        try {
            String serverName = this.currentRequest.getServerName();
            Cine cine = usersService.getUserCineByServerName(serverName);

            return cine.getShowIVA();
        } catch (IllegalStateException e) {
        }

        return true;
    }

    @Override
    public boolean showTotalesSinComision() {
        try {
            String serverName = this.currentRequest.getServerName();
            Cine cine = usersService.getUserCineByServerName(serverName);

            return cine.getComision().compareTo(BigDecimal.ZERO) != 0;
        } catch (IllegalStateException e) {
        }

        return true;
    }

    public boolean isMenuSalasEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isMenuSalas();
    }

    public boolean isMenuIntegracionesEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isMenuIntegraciones();
    }

    public boolean isMenuPerfilEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isMenuPerfil();
    }

    public boolean hasHelp() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isMenuAyuda();
    }

    public boolean isMenuClientesEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isMenuClientes();
    }

    public boolean isContratoGeneralEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isCheckContratacion();
    }

    public Boolean isMenuTpvEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isMenuTpv();
    }

    public boolean isMenuVentaAnticipadaEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isMenuVentaAnticipada();
    }

    public boolean isMenuAbonosEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isMenuAbonos();
    }

    public boolean isShowComoNosConocisteEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isShowComoNosConociste();
    }

    public boolean isCompraTaquillaDisabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return !cine.isMenuTaquilla();
    }

    public boolean isMenuICAAEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isMenuICAA();
    }

    public boolean hasNumeracionSecuencialEnabled() {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());

        return cine.isNoNumeradasSecuencial();
    }

    public String getPayModes(Locale locale) {
        Cine cine = usersService.getUserCineByServerName(this.currentRequest.getServerName());
        return Configuration.getPayModes(Arrays.asList(cine.getFormasPago().split(",")), locale);
    }
}
