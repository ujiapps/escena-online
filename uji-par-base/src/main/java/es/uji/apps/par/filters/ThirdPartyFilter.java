package es.uji.apps.par.filters;

import com.google.common.base.Strings;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ThirdPartyFilter implements Filter {
    public final static String MOBILE = "mobile";
    public final static String EMAIL = "email";

    public void init(FilterConfig filterConfig) {
    }

    public void destroy() {
    }

    public void doFilter(
        ServletRequest servletRequest,
        ServletResponse servletResponse,
        FilterChain filterChain
    ) throws IOException, ServletException {
        HttpServletRequest clientRequest = (HttpServletRequest) servletRequest;
        String mobile = clientRequest.getParameter(MOBILE);
        String email = clientRequest.getParameter(EMAIL);
        if (clientRequest.getMethod().equals("GET") && (!Strings.isNullOrEmpty(mobile) || !Strings.isNullOrEmpty(email))) {
            HttpServletRequest sRequest = (HttpServletRequest) servletRequest;
            HttpSession session = sRequest.getSession();

            if (!Strings.isNullOrEmpty(mobile)) {
                session.setAttribute(MOBILE, mobile);
            }

            if (!Strings.isNullOrEmpty(email)) {
                session.setAttribute(EMAIL, email);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}