package es.uji.apps.par.ext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExtGridFilterList {

    private List<ExtGridFilter> filtros;

    public ExtGridFilterList()
    {
        filtros = new ArrayList<ExtGridFilter>();
    }

    public ExtGridFilterList(String filtros){
        if (filtros != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                this.filtros = objectMapper.readValue(filtros, new TypeReference<List<ExtGridFilter>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static ExtGridFilterList fromString(String json) {
        return new ExtGridFilterList(json);
    }


    public List<ExtGridFilter> getFiltros() {
        return filtros;
    }

    public void setFiltros(List<ExtGridFilter> filtros) {
        this.filtros = filtros;
    }

    public void addFilter(ExtGridFilter extGridFilter)
    {
        filtros.add(extGridFilter);
    }

    public ExtGridFilter findFiltroByProperty(String property) {
        for(ExtGridFilter filtro : filtros) {
            if(filtro.getProperty().equals(property)) {
                return filtro;
            }
        }
        return null;
    }
}
