package es.uji.apps.par.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import es.uji.apps.par.db.AbonoDTO;
import es.uji.apps.par.db.QAbonoDTO;
import es.uji.apps.par.db.QCineDTO;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSalasUsuarioDTO;
import es.uji.apps.par.db.QTarifaDTO;
import es.uji.apps.par.db.QUsuarioDTO;

@Repository
public class AbonosDAO extends BaseDAO {
    QAbonoDTO qAbonoDTO = QAbonoDTO.abonoDTO;
    QTarifaDTO qTarifaDTO = QTarifaDTO.tarifaDTO;
    QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
    QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
    QCineDTO qCineDTO = QCineDTO.cineDTO;
    QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;

    private JPAQuery getAbonoQuery(
        String userUID,
        JPAQuery query
    ) {
        return query.from(qAbonoDTO).leftJoin(qAbonoDTO.parTarifa, qTarifaDTO).leftJoin(qTarifaDTO.parCine, qCineDTO)
            .leftJoin(qCineDTO.parSalas, qSalaDTO).leftJoin(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .leftJoin(qSalasUsuarioDTO.parUsuario, qUsuarioDTO).where((qUsuarioDTO.usuario.eq(userUID))).distinct();
    }

    @Transactional
    public List<AbonoDTO> getAll(
        String sortParameter,
        int start,
        int limit,
        String userUID
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        return getAbonoQuery(userUID, query).orderBy(getSort(qAbonoDTO, sortParameter)).offset(start).limit(limit)
            .list(qAbonoDTO);
    }

    @Transactional
    public List<AbonoDTO> getAll(
        String userUID
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        return getAbonoQuery(userUID, query).list(qAbonoDTO);
    }

    @Transactional
    public List<AbonoDTO> getByIds(
        List<Long> ids
    ) {
        entityManager.flush();
        entityManager.clear();

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qAbonoDTO).where(qAbonoDTO.id.in(ids)).list(qAbonoDTO);
    }

    @Transactional
    public AbonoDTO getById(
        Long ids
    ) {
        List<AbonoDTO> abonos = getByIds(Arrays.asList(ids));
        if (abonos.size() > 0) {
            return abonos.get(0);
        }
        else {
            return null;
        }
    }

    @Transactional
    public long getTotal(
        String userUID
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        return getAbonoQuery(userUID, query).count();
    }
}
