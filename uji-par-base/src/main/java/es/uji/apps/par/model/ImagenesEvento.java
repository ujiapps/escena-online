package es.uji.apps.par.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ImagenesEvento implements IImagenesEvento
{
    private byte[] imagen;
    private String imagenUUID;
    private String imagenContentType;
    private byte[] imagenPubli;
    private String imagenPubliUUID;
    private String imagenPubliContentType;

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public String getImagenUUID() {
        return imagenUUID;
    }

    public void setImagenUUID(String imagenUUID) {
        this.imagenUUID = imagenUUID;
    }

    public byte[] getImagenPubli() {
        return imagenPubli;
    }

    public void setImagenPubli(byte[] imagenPubli) {
        this.imagenPubli = imagenPubli;
    }

    public String getImagenPubliUUID() {
        return imagenPubliUUID;
    }

    public void setImagenPubliUUID(String imagenPubliUUID) {
        this.imagenPubliUUID = imagenPubliUUID;
    }

    public String getImagenContentType() {
        return imagenContentType;
    }

    public void setImagenContentType(String imagenContentType) {
        this.imagenContentType = imagenContentType;
    }

    public String getImagenPubliContentType() {
        return imagenPubliContentType;
    }

    public void setImagenPubliContentType(String imagenPubliContentType) {
        this.imagenPubliContentType = imagenPubliContentType;
    }
}