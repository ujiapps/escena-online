package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class AbonoNoValidoException extends GeneralPARException
{
    public AbonoNoValidoException()
    {
        super(ABONO_INVALIDO_CODE);
    }
}
