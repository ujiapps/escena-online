package es.uji.apps.par.database;

import java.math.BigDecimal;
import java.math.BigInteger;

public class DatabaseHelperH2 implements DatabaseHelper
{
	@Override
	public String paginate(int start, int limit, String sql)
	{
		return " select * from (" + sql + ") as s " +
				" limit " + limit + " offset " + start;
	}

	@Override
	public Integer castId(Object id)
	{
		return ((BigInteger)id).intValue();
	}

	@Override
	public Boolean castBoolean(Object value)
	{
		return (Boolean) value;
	}

	@Override
	public BigDecimal castBigDecimal(Object value)
	{
		return (value.toString().equals("true")) ? new BigDecimal(1) : ((value.toString().equals("false")) ? new BigDecimal(0) : new BigDecimal(value.toString()));
	}

	@Override
	public String caseString(String condicion, String[] condicionesValores)
	{
		return "1";
	}

	@Override
	public String trueString()
	{
		return "true";
	}

	@Override
	public String falseString()
	{
		return "false";
	}

	@Override
	public String trunc(String campo, String formato)
	{
		return campo;
	}

	@Override
	public int booleanToNumber(Object valor)
	{
		return 0;
	}

	@Override
	public String toInteger(String columna)
	{
		return columna;
	}

	@Override
	public String toDate() {
		return "parseDateTime";
	}

	@Override
	public String isNotEmptyString(String columna)
	{
		return String.format("%s <> ''", columna);
	}

	@Override
	public String dateFormatter() {
		return "dd/MM/yyyy HH:mm";
	}

	@Override
	public String dateHourSecondsFormatter() {
		return "dd/MM/yyyy HH:mm:ss";
	}
}
