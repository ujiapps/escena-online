package es.uji.apps.par.exceptions;

public class PlantillaConEventosException extends GeneralPARException {

    public PlantillaConEventosException(){
        super(PLANTILLA_CON_EVENTOS_CODE, PLANTILLA_CON_EVENTOS);
    }
}
