package es.uji.apps.par.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PAR_OCUPACION")
public class OcupacionDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private Integer ocupadas;

	private String codigo;

	@Column(name = "TOTAL_ENTRADAS")
	private Integer totalEntradas;

	@Column(name = "SESION_ID")
	private Long sesionId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getOcupadas() {
		return ocupadas;
	}

	public void setOcupadas(Integer ocupadas) {
		this.ocupadas = ocupadas;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Integer getTotalEntradas() {
		return totalEntradas;
	}

	public void setTotalEntradas(Integer totalEntradas) {
		this.totalEntradas = totalEntradas;
	}

	public Long getSesionId() {
		return sesionId;
	}

	public void setSesionId(Long sesionId) {
		this.sesionId = sesionId;
	}
}