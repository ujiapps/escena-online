package es.uji.apps.par.db;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="PAR_ABONADOS", uniqueConstraints = {@UniqueConstraint(columnNames = {"ABONO_ID", "EMAIL"})})
public class AbonadoDTO {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name="PAR_ABONADOS_ID_GENERATOR", sequenceName="HIBERNATE_SEQUENCE")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAR_ABONADOS_ID_GENERATOR")
    private long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "APELLIDOS")
    private String apellidos;

    @Column(name = "DIRECCION")
    private String direccion;

    @Column(name = "POBLACION")
    private String poblacion;

    @Column(name = "CP")
    private String cp;

    @Column(name = "PROVINCIA")
    private String provincia;

    @Column(name = "TFNO")
    private String telefono;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "INFO_PERIODICA")
    private Boolean infoPeriodica;

    @Column(name = "FECHA")
    private Timestamp fecha;

    @ManyToOne
    @JoinColumn(name="ABONO_ID")
    private TarifaDTO tarifa;

    @Column(name = "IMPORTE")
    private BigDecimal importe;

    @Column(name = "CODIGO_PAGO_PASARELA")
    private String codigoPagoPasarela;

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "ABONOS")
    private int abonos;

    public AbonadoDTO() {
    }

    public AbonadoDTO(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getInfoPeriodica() {
        return infoPeriodica;
    }

    public void setInfoPeriodica(Boolean infoPeriodica) {
        this.infoPeriodica = infoPeriodica;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public TarifaDTO getTarifa() {
        return tarifa;
    }

    public void setTarifa(TarifaDTO tarifa) {
        this.tarifa = tarifa;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getCodigoPagoPasarela() {
        return codigoPagoPasarela;
    }

    public void setCodigoPagoPasarela(String codigoPagoPasarela) {
        this.codigoPagoPasarela = codigoPagoPasarela;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getAbonos() {
        return abonos;
    }

    public void setAbonos(int abonos) {
        this.abonos = abonos;
    }
}
