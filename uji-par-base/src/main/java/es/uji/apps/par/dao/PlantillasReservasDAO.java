package es.uji.apps.par.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.persistence.PersistenceException;

import es.uji.apps.par.db.ButacasPlantillaReservasDTO;
import es.uji.apps.par.db.PlantillaReservasDTO;
import es.uji.apps.par.db.QButacasPlantillaReservasDTO;
import es.uji.apps.par.db.QPlantillaReservasDTO;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSalasUsuarioDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.model.Plantilla;

@Repository
public class PlantillasReservasDAO extends BaseDAO {

    private QPlantillaReservasDTO qPlantillaReservasDTO = QPlantillaReservasDTO.plantillaReservasDTO;
    private QButacasPlantillaReservasDTO qButacasPlantillaReservasDTO =
        QButacasPlantillaReservasDTO.butacasPlantillaReservasDTO;

    @Autowired
    LocalizacionesDAO localizacionesDAO;

    @Transactional
    public List<PlantillaReservasDTO> get(
        String sortParameter,
        int start,
        int limit,
        String userUID
    ) {
        return getQueryPlantillas(userUID).orderBy(getSort(qPlantillaReservasDTO, sortParameter)).
            offset(start).limit(limit).list(qPlantillaReservasDTO);
    }

    @Transactional
    private JPAQuery getQueryPlantillas(String userUID) {
        QSalaDTO qSalaDTO = new QSalaDTO("qSalaDTO");
        QSalasUsuarioDTO qSalasUsuarioDTO = new QSalasUsuarioDTO("qSalasUsuarioDTO");

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPlantillaReservasDTO).leftJoin(qPlantillaReservasDTO.sala, qSalaDTO).fetch()
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).where(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID));
    }

    @Transactional
    private JPAQuery getQueryPlantillasBySala(
        Long salaId,
        String userUID
    ) {
        QSalaDTO qSalaDTO = new QSalaDTO("qSalaDTO");
        QSalasUsuarioDTO qSalasUsuarioDTO = new QSalasUsuarioDTO("qSalasUsuarioDTO");

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPlantillaReservasDTO).leftJoin(qPlantillaReservasDTO.sala, qSalaDTO).fetch()
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .where(qSalaDTO.id.eq(salaId).and(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID)));
    }

    @Transactional
    public void remove(long id) throws PersistenceException {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qPlantillaReservasDTO);
        delete.where(qPlantillaReservasDTO.id.eq(id)).execute();
    }

    @Transactional
    public Plantilla add(Plantilla plantillaReservas) {
        PlantillaReservasDTO plantillaDTO = new PlantillaReservasDTO();
        plantillaDTO.setNombre(plantillaReservas.getNombre());
        plantillaDTO.setSala(new SalaDTO(plantillaReservas.getSala().getId()));

        entityManager.persist(plantillaDTO);
        plantillaReservas.setId(plantillaDTO.getId());

        return plantillaReservas;
    }

    @Transactional
    public Plantilla update(Plantilla plantillaPrecios) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPlantillaReservasDTO);
        update.set(qPlantillaReservasDTO.nombre, plantillaPrecios.getNombre())
            .set(qPlantillaReservasDTO.sala, new SalaDTO(plantillaPrecios.getSala().getId()))
            .where(qPlantillaReservasDTO.id.eq(plantillaPrecios.getId())).execute();

        return plantillaPrecios;
    }

    @Transactional
    public int getTotalPlantillaReservas(String userUID) {
        return (int) getQueryPlantillas(userUID).count();
    }

    @Transactional
    public List<PlantillaReservasDTO> getBySala(
        Long salaId,
        String sortParameter,
        int start,
        int limit,
        String userUID
    ) {
        return getQueryPlantillasBySala(salaId, userUID).orderBy(getSort(qPlantillaReservasDTO, sortParameter)).
            offset(start).limit(limit).list(qPlantillaReservasDTO);
    }

    @Transactional
    public int getTotalPlantillaResevasBySala(
        Long salaId,
        String userUID
    ) {
        return (int) getQueryPlantillasBySala(salaId, userUID).count();
    }

    public List<ButacasPlantillaReservasDTO> getButacasOfPlantilla(
        Long plantillaId,
        String sortParameter,
        int start,
        int limit
    ) {
        return getQueryButacasPlantillas(plantillaId).orderBy(getSort(qButacasPlantillaReservasDTO, sortParameter)).
            offset(start).limit(limit).list(qButacasPlantillaReservasDTO);
    }

    @Transactional
    private JPAQuery getQueryButacasPlantillas(
        Long plantillaId
    ) {
        QSalaDTO qSalaDTO = new QSalaDTO("qSalaDTO");

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qButacasPlantillaReservasDTO)
            .leftJoin(qButacasPlantillaReservasDTO.parPlantilla, qPlantillaReservasDTO).fetch()
            .leftJoin(qPlantillaReservasDTO.sala, qSalaDTO).fetch().where(qPlantillaReservasDTO.id.eq(plantillaId));
    }

    public long getTotalButacasOfPlantilla(
        Long plantillaId
    ) {
        return getQueryButacasPlantillas(plantillaId).count();
    }

    public PlantillaReservasDTO getById(
        long plantillaId
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPlantillaReservasDTO).where(qPlantillaReservasDTO.id.eq(plantillaId))
            .uniqueResult(qPlantillaReservasDTO);
    }

    @Transactional
    public void removeButacasFromPlantilla(long plantillaId) {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qButacasPlantillaReservasDTO);
        deleteClause.where(qButacasPlantillaReservasDTO.parPlantilla.id.eq(plantillaId)).execute();
    }

    @Transactional
    public void guardaButacasByPlantilla(
        Long plantillaId,
        List<Butaca> butacasSeleccionadas
    ) {
        for (Butaca butacasSeleccionada : butacasSeleccionadas) {
            ButacasPlantillaReservasDTO butacasPlantillaReservasDTO = new ButacasPlantillaReservasDTO();
            butacasPlantillaReservasDTO.setFila(butacasSeleccionada.getFila());
            butacasPlantillaReservasDTO.setNumero(butacasSeleccionada.getNumero());
            butacasPlantillaReservasDTO
                .setParLocalizacion(localizacionesDAO.getLocalizacionByCodigo(butacasSeleccionada.getLocalizacion()));
            butacasPlantillaReservasDTO.setParPlantilla(new PlantillaReservasDTO(plantillaId));

            entityManager.merge(butacasPlantillaReservasDTO);
        }
    }
}
