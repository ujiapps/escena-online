package es.uji.apps.par.exceptions;

public class MinimoUnaSalaException extends GeneralPARException {
    public MinimoUnaSalaException() {
        super(MINIMO_UNA_SALA_ERROR_CODE, MINIMO_UNA_SALA_ERROR);
    }
}
