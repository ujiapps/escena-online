package es.uji.apps.par.report;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

import es.uji.apps.fopreports.serialization.ReportSerializationException;
import es.uji.apps.par.config.Configuration;

public interface EntradaReportTaquillaInterface extends EntradaReportInterface {
	EntradaReportTaquillaInterface create(Locale locale, Configuration configuration) throws SAXException, IOException;
	void setTitulo(String titulo);
	void setCif(String cif);
	void setPromotor(String promotor);
	void setNifPromotor(String nifPromotor);
	void generaPaginasReciboPinpad(String reciboPinpad);
	void serialize(OutputStream output) throws ReportSerializationException;
	void setUrlPortada(String urlPortada);
	void setNombreEntidad(String nombreEntidad);
	void setDireccion(String direccion);
	void setUrlCondiciones(String urlCondiciones);
    void setTipoPago(String tipoPago);
}
