package es.uji.apps.par.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.PersistenceException;

import es.uji.apps.par.db.QCineDTO;
import es.uji.apps.par.db.QPlantillaDTO;
import es.uji.apps.par.db.QPreciosPlantillaDTO;
import es.uji.apps.par.db.QPreciosSesionDTO;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSalasUsuarioDTO;
import es.uji.apps.par.db.QSesionDTO;
import es.uji.apps.par.db.QTarifaDTO;
import es.uji.apps.par.db.QUsuarioDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.exceptions.TarifaYaExisteException;

@Repository
public class TarifasDAO extends BaseDAO {
    QTarifaDTO qTarifaDTO = QTarifaDTO.tarifaDTO;
    QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
    QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
    QCineDTO qCineDTO = QCineDTO.cineDTO;
    QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;

    @Transactional
    public String getNombre(
        int idTarifa
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qTarifaDTO).where(qTarifaDTO.id.eq(new Long(idTarifa))).distinct()
            .singleResult(qTarifaDTO.nombre);
    }

    @Transactional
    public TarifaDTO get(
        int idTarifa,
        String userUID
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        List<TarifaDTO> tarifaDTO =
            query.from(qTarifaDTO).leftJoin(qTarifaDTO.parCine, qCineDTO).leftJoin(qCineDTO.parSalas, qSalaDTO)
                .leftJoin(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).leftJoin(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
                .where(
                    (qUsuarioDTO.usuario.eq(userUID).or(qCineDTO.isNull())).and(qTarifaDTO.id.eq(new Long(idTarifa))))
                .distinct().list(qTarifaDTO);
        if (tarifaDTO.size() == 1)
            return tarifaDTO.get(0);
        else
            return null;
    }


    @Transactional
    public TarifaDTO getPublicPorDefecto(String userUID) {
        JPAQuery query = new JPAQuery(entityManager);
        List<TarifaDTO> tarifaDTO =
            query.from(qTarifaDTO).leftJoin(qTarifaDTO.parCine, qCineDTO).leftJoin(qCineDTO.parSalas, qSalaDTO)
                .leftJoin(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).leftJoin(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
                .where((qUsuarioDTO.usuario.eq(userUID).or(qCineDTO.isNull())).and(
                    qTarifaDTO.isPublica.isTrue().and(qTarifaDTO.defecto.isTrue().and(qTarifaDTO.isAbono.isFalse()))))
                .distinct().list(qTarifaDTO);
        if (tarifaDTO.size() > 0)
            return tarifaDTO.get(0);
        else
            return null;
    }

    @Transactional
    public List<TarifaDTO> getAll(
        String sortParameter,
        int start,
        int limit,
        String userUID,
        boolean isAbono
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        return getTarifaQuery(userUID, isAbono, query).orderBy(getSort(qTarifaDTO, sortParameter)).offset(start)
            .limit(limit).list(qTarifaDTO);
    }

    @Transactional
    public List<TarifaDTO> getAll(
        String sortParameter,
        int start,
        int limit,
        String userUID
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        return getTarifaQuery(userUID, query).orderBy(getSort(qTarifaDTO, sortParameter)).offset(start).limit(limit)
            .list(qTarifaDTO);
    }

    @Transactional
    public long getTotal(
        String userUID
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        return getTarifaQuery(userUID, query).count();
    }

    @Transactional
    public long getTotal(
        String userUID,
        boolean isAbono
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        return getTarifaQuery(userUID, isAbono, query).count();
    }

    private JPAQuery getTarifaQuery(
        String userUID,
        JPAQuery query
    ) {
        return query.from(qTarifaDTO).leftJoin(qTarifaDTO.parCine, qCineDTO).leftJoin(qCineDTO.parSalas, qSalaDTO)
            .leftJoin(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).leftJoin(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
            .where((qUsuarioDTO.usuario.eq(userUID).or(qCineDTO.isNull()))).distinct();
    }

    private JPAQuery getTarifaQuery(
        String userUID,
        boolean isAbono,
        JPAQuery query
    ) {
        return getTarifaQuery(userUID, query).where(qTarifaDTO.isAbono.eq(isAbono)).distinct();
    }

    @Transactional
    public TarifaDTO add(TarifaDTO tarifa) {
        try {
            entityManager.persist(tarifa);
            entityManager.flush();
            entityManager.clear();
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause() != null && e.getCause().getCause() != null && e.getCause()
                .getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
                throw new TarifaYaExisteException(tarifa.getNombre());
            } else {
                throw e;
            }
        }
        return tarifa;
    }

    @Transactional
    public void update(TarifaDTO tarifa) {
        try {
            JPAUpdateClause update = new JPAUpdateClause(entityManager, qTarifaDTO);
            update.set(qTarifaDTO.nombre, tarifa.getNombre()).set(qTarifaDTO.isPublica, tarifa.getIsPublica()).
                set(qTarifaDTO.defecto, tarifa.getDefecto()).
                set(qTarifaDTO.maxEventos, tarifa.getMaxEventos()).
                where(qTarifaDTO.id.eq(tarifa.getId())).execute();
        } catch (Exception e) {
            if (e.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
                throw new TarifaYaExisteException(tarifa.getNombre());
            } else {
                throw e;
            }
        }
    }

    @Transactional
    public void remove(long tarifaId) throws PersistenceException {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qTarifaDTO);
        delete.where(qTarifaDTO.id.eq(tarifaId)).execute();
    }

    public boolean hasFutureEvents(long id) {
        QPreciosSesionDTO qPreciosSesionDTO = QPreciosSesionDTO.preciosSesionDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QPreciosPlantillaDTO qPreciosPlantillaDTO = QPreciosPlantillaDTO.preciosPlantillaDTO;
        QPlantillaDTO qPlantillaDTO = QPlantillaDTO.plantillaDTO;

        JPAQuery queryPlantilla = new JPAQuery(entityManager);
        JPAQuery queryPrecioSesion = new JPAQuery(entityManager);

        return queryPlantilla.from(qPreciosPlantillaDTO).join(qPreciosPlantillaDTO.parPlantilla, qPlantillaDTO)
            .join(qPlantillaDTO.parSesiones, qSesionDTO).where(qPreciosPlantillaDTO.parTarifa.id.eq(id)
                .and(qSesionDTO.fechaCelebracion.goe(new Timestamp(System.currentTimeMillis())))).count() > 0 ||
            queryPrecioSesion.from(qPreciosSesionDTO).join(qPreciosSesionDTO.parSesione, qSesionDTO).where(
                qPreciosSesionDTO.parTarifa.id.eq(id)
                    .and(qSesionDTO.fechaCelebracion.goe(new Timestamp(System.currentTimeMillis())))).count() > 0;
    }
}
