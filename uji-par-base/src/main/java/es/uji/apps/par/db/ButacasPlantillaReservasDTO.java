package es.uji.apps.par.db;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="PAR_BUTACAS_PLANTILLA_RESERVAS")
public class ButacasPlantillaReservasDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PAR_BUTACAS_PLANTILLA_RES_ID_GENERATOR", sequenceName="HIBERNATE_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAR_BUTACAS_PLANTILLA_RES_ID_GENERATOR")
	private long id;

	@ManyToOne
	@JoinColumn(name="LOCALIZACION_ID")
	private LocalizacionDTO parLocalizacion;

	@ManyToOne
	@JoinColumn(name="PLANTILLA_ID")
	private PlantillaReservasDTO parPlantilla;

	private String fila;
	private String numero;

	public ButacasPlantillaReservasDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalizacionDTO getParLocalizacion() {
		return parLocalizacion;
	}

	public void setParLocalizacion(LocalizacionDTO parLocalizacion) {
		this.parLocalizacion = parLocalizacion;
	}

	public PlantillaReservasDTO getParPlantilla() {
		return parPlantilla;
	}

	public void setParPlantilla(PlantillaReservasDTO parPlantilla) {
		this.parPlantilla = parPlantilla;
	}

	public String getFila() {
		return fila;
	}

	public void setFila(String fila) {
		this.fila = fila;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
}