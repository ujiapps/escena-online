package es.uji.apps.par.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import es.uji.apps.par.db.AbonadoDTO;
import es.uji.apps.par.db.QAbonadoDTO;
import es.uji.apps.par.db.QButacaSinEagerDTO;
import es.uji.apps.par.db.QCompraSinEagerDTO;
import es.uji.apps.par.db.QEventoDTO;
import es.uji.apps.par.db.QSesionDTO;
import es.uji.apps.par.db.QTarifaDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.model.Abonado;

@Repository
public class AbonadosDAO extends BaseDAO {

    private QAbonadoDTO qAbonadoDTO = QAbonadoDTO.abonadoDTO;
    private QTarifaDTO qAbonoDTO = QTarifaDTO.tarifaDTO;

    @Transactional
    public List<Abonado> getAbonadosByAbonoId(
        long tarifaId,
        String sortParameter,
        int start,
        int limit
    ) {
        List<Abonado> abonados = new ArrayList<>();
        List<AbonadoDTO> abonadosDTO =
            getQueryAbonados().where(qAbonadoDTO.tarifa.id.eq(tarifaId)).orderBy(getSort(qAbonadoDTO, sortParameter)).
                offset(start).limit(limit).list(qAbonadoDTO);

        for (AbonadoDTO abonadoDB : abonadosDTO) {
            abonados.add(new Abonado(abonadoDB));
        }

        return abonados;
    }

    @Transactional
    public List<TarifaDTO> getAbonosByAbonadoEmail(
        String email
    ) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qAbonoDTO).leftJoin(qAbonoDTO.abonados, qAbonadoDTO).where(qAbonadoDTO.email.eq(email))
            .distinct().list(qAbonoDTO);
    }

    private JPAQuery getQueryAbonados() {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qAbonadoDTO);
    }

    @Transactional
    public int getTotalAbonadosByAbonoId(long abonoId) {
        return (int) getQueryAbonados().where(qAbonadoDTO.tarifa.id.eq(abonoId)).count();
    }

    @Transactional
    public Long usosAbonoEnMismoEvento(
        long eventoId,
        String email,
        long abonoId
    ) {
        QButacaSinEagerDTO qButacaSinEagerDTO = QButacaSinEagerDTO.butacaSinEagerDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;
        QCompraSinEagerDTO compraSinEagerDTO = QCompraSinEagerDTO.compraSinEagerDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return Optional.ofNullable(query.from(qButacaSinEagerDTO, qAbonoDTO).leftJoin(qButacaSinEagerDTO.parSesion, qSesionDTO)
            .leftJoin(qSesionDTO.parEvento, qEventoDTO).leftJoin(qButacaSinEagerDTO.parCompra, compraSinEagerDTO)
            .groupBy(compraSinEagerDTO.email, qAbonoDTO.id, qEventoDTO.id).where(
                qEventoDTO.id.eq(eventoId).and(compraSinEagerDTO.email.eq(email)).and(qAbonoDTO.id.eq(abonoId)).and(
                    qButacaSinEagerDTO.tipo.eq(String.valueOf(abonoId)).and(qButacaSinEagerDTO.anulada.isFalse())
                        .and(qAbonoDTO.isAbono.isTrue()))
                    .and(compraSinEagerDTO.anulada.isFalse().and(compraSinEagerDTO.taquilla.isFalse())))
            .uniqueResult(qButacaSinEagerDTO.id.count().coalesce((long) 0))).orElse((long) 0);
    }

    @Transactional
    public Long usosAbonoEnEventosDistintos(
        String email,
        long abonoId
    ) {
        QButacaSinEagerDTO qButacaSinEagerDTO = QButacaSinEagerDTO.butacaSinEagerDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;
        QCompraSinEagerDTO qCompraSinEagerDTO = QCompraSinEagerDTO.compraSinEagerDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return Optional.ofNullable(query.from(qButacaSinEagerDTO, qAbonoDTO).leftJoin(qButacaSinEagerDTO.parSesion, qSesionDTO)
            .leftJoin(qSesionDTO.parEvento, qEventoDTO).leftJoin(qButacaSinEagerDTO.parCompra, qCompraSinEagerDTO)
            .groupBy(qCompraSinEagerDTO.email, qAbonoDTO.id).where(qCompraSinEagerDTO.email.eq(email).and(qAbonoDTO.id.eq(abonoId)).and(
                qButacaSinEagerDTO.tipo.eq(String.valueOf(abonoId)).and(qButacaSinEagerDTO.anulada.isFalse())
                    .and(qAbonoDTO.isAbono.isTrue()))
                .and(qCompraSinEagerDTO.anulada.isFalse().and(qCompraSinEagerDTO.taquilla.isFalse())))
            .uniqueResult(qEventoDTO.id.countDistinct().coalesce((long) 0))).orElse((long) 0);
    }

    @Transactional
    public int getNumeroAbonosByAbonoIdAndEmail(
        long tarifaId,
        String email
    ) {
        JPAQuery query = new JPAQuery(entityManager);

        return Optional.ofNullable(query.from(qAbonadoDTO).where(qAbonadoDTO.email.eq(email).and(qAbonadoDTO.tarifa.id.eq(tarifaId)))
                .groupBy(qAbonadoDTO.tarifa.id).uniqueResult(qAbonadoDTO.abonos.sum().coalesce(0))).orElse(0);
    }
}
