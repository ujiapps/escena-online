package es.uji.apps.par.exceptions;

public class TiempoMinioRequeridoParaAnularDesdeEnlace extends Throwable {
    public TiempoMinioRequeridoParaAnularDesdeEnlace(
        long id,
        String codigo,
        int tiempoRestanteCancelableDesdeEnlace,
        long minutosParaLaCelebracion
    ) {
        super(String.format("El cine %s con código %s tiene un tiempo mínimo requerido de %s minutos para poder anular desde enlace y quedan %s minutos para celebrarse la sesión", id, codigo, tiempoRestanteCancelableDesdeEnlace, minutosParaLaCelebracion));
    }
}
