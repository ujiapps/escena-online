package es.uji.apps.par.config;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;

import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.Environment;

@Component
public class Configuration extends Properties {
    public static final String PROPERTIES_SEPARATOR = ",";

    public static final String SYNC_TOKEN = "uji.sync.token";
    public static final String SYNC_HEADER_TOKEN = "uji.sync.headerToken";
    public static final String SYNC_URL_TIPO = "uji.sync.rss";

    public static final String HORAS_VENTA_ANTICIPADA = "uji.reports.horaVentaAnticipada";

    private static final String MAIL_HOST = "uji.par.mail.host";
    private static final String MAIL_PORT = "uji.par.mail.port";
    private static final String MAIL_SSL = "uji.par.mail.ssl";
    private static final String MAIL_TLS = "uji.par.mail.tls";
    private static final String MAIL_USERNAME = "uji.par.mail.username";
    private static final String MAIL_PASSWORD = "uji.par.mail.password";
    private static final String MAIL_DEFAULT_SENDER = "uji.par.mail.defaultSender";
    private static final String MAILING_CLASS = "uji.par.mail.class";
    private static final String ENVIAR_MAILS_ENTRADAS = "uji.par.enviarMailsEntradas";

    private static final String GASTOS_GESTION = "uji.par.gastosGestion";
    private static final String ENTORNO = "uji.par.entorno";

    private static final String INFORME_EFECTIVO_CARGO = "uji.par.informeEfectivo.cargo";
    private static final String INFORME_EFECTIVO_FIRMANTE = "uji.par.informeEfectivo.firmante";
    private static final String MARGEN_VENTA_TAQUILLA_MINUTOS = "uji.par.margenVentaTaquillaMinutos";

    private static final String AUTH_CLASS = "uji.par.authClass";
    private static final String ADMIN_LOGIN = "uji.par.auth.admin.login";
    private static final String ADMIN_PASSWORD = "uji.par.auth.admin.password";

    private static final String JDBC_URL = "uji.db.jdbcUrl";
    private static final String DB_USER = "uji.db.username";
    private static final String DB_PASS = "uji.db.password";
    private static final String SYNC_TIPO = "uji.sync.lugar";

    private static final String BARCODE_WIDTH_HEIGHT = "uji.reports.barcodeWidthHeight";

    private static final String ENTRADA_ID = "uji.reports.entradaId";

    private static final String TMP_FOLDER = "uji.tmp.folder";

    private static final String PGP_PASSPHRASE = "uji.pgp.key";

    private static final String CODIGO_BUZON = "uji.codigo.buzon";

    private static final String ACTIVE_DIRECTORY_IP = "activedirectory.ip";
    private static final String ACTIVE_DIRECTORY_Port = "activedirectory.port";
    private static final String ACTIVE_DIRECTORY_DOMAIN = "activedirectory.domain";
    private static final String ACTIVE_DIRECTORY_DC = "activedirectory.dc";

    private static final String IMAGEN_SUSTITUTIVA = "uji.reports.imagenSustitutiva";
    private static final String IMAGEN_SUSTITUTIVA_CONTENT_TYPE = "uji.reports.imagenSustitutivaContentType";
    private static final String IMAGEN_PUBLI_SUSTITUTIVA = "uji.reports.imagenPubliSustitutiva";
    private static final String IMAGEN_PUBLI_SUSTITUTIVA_CONTENT_TYPE = "uji.reports.imagenPubliSustitutivaContentType";

    private static final String GENERAR_CIFRADO = "uji.pgp.generateCifrado";

    private static final String NOT_FOUND_URL = "uji.par.notfoundurl";

    private static final String JSON_LOCALIZACIONES_PATH = Environment.getParHome() + "/butacas/";

    private static final String IS_LOADED_FROM_RESOURCE = "uji.par.isLoadedFromResource";

    private static final String APPLEWWDRCA_PATH = "cert.applewwdrca.path";
    private static final String APPLE_KEYSTORE_PATH = "cert.apple.p12.path";
    private static final String APPLE_KEYSTORE_PASS = "cert.apple.p12.pass";
    private static final String APPLE_KEYSTORE_TEAMID = "cert.apple.p12.teamid";
    private static final String APPLE_KEYSTORE_NAME = "cert.apple.p12.name";

    private static final String ADMIN_TOKEN = "uji.admin.token";

    private static final String QUEUE_DOMAINS = "queue.domains";
    private static final String QUEUE_LIMIT = "queue.limit";

    private static final Logger log = LoggerFactory.getLogger(Configuration.class);

    private PropertiesConfiguration propertiesConfiguration;

    @Autowired
    public Configuration(PropertiesConfiguration propertiesConfiguration) throws IOException {
        super.load(new FileReader(propertiesConfiguration.getFile()));
        this.propertiesConfiguration = propertiesConfiguration;
        this.propertiesConfiguration.setDelimiterParsingDisabled(true);
    }

    @Override
    public String getProperty(String key) {
        String val = propertiesConfiguration.getString(key);
        super.setProperty(key, val);
        return val;
    }

    private String getNoObligatoryProperty(String propertyName) {
        try {
            String property = getProperty(propertyName);
            return property;
        } catch (Exception e) {
            return null;
        }
    }

    static public String getProperty(
        Properties properties,
        String propertyName
    ) {
        String value = properties.getProperty(propertyName);
        try {
            value = new String(value.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } catch (NullPointerException e) {
            log.debug("Propiedad " + propertyName + " nula", e);
        }

        return value.trim();
    }

    public boolean isIdEntrada() {
        String entradaId = getNoObligatoryProperty(ENTRADA_ID);
        if (entradaId != null && entradaId.length() > 0 && !entradaId.equals("false")) {
            try {
                Integer.parseInt(entradaId);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    public Integer getIdEntrada() {
        String entradaId = getNoObligatoryProperty(ENTRADA_ID);
        if (entradaId != null && entradaId.length() > 0) {
            try {
                return Integer.parseInt(entradaId);
            } catch (NumberFormatException e) {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public static String getPayModes(List<String> modes, Locale locale) {
        List<String> payModesJs = new ArrayList<>(Arrays
            .asList("['metalico', '" + ResourceProperties.getProperty(locale, "paymode.metalico") + "']",
                "['invitacion', '" + ResourceProperties.getProperty(locale, "paymode.invitacion") + "']",
                "['pinpad', '" + ResourceProperties.getProperty(locale, "paymode.pinpad") + "']",
                "['tarjeta', '" + ResourceProperties.getProperty(locale, "paymode.tarjeta") + "']",
                "['tarjetaOffline', '" + ResourceProperties.getProperty(locale, "paymode.tarjetaOffline") + "']",
                "['transferencia', '" + ResourceProperties.getProperty(locale, "paymode.transferencia") + "']",
                "['credito', '" + ResourceProperties.getProperty(locale, "paymode.credito") + "']"));

        List<String> payModes = new ArrayList<>();
        for (String mode : modes) {
            for (String payModeJs : payModesJs) {
                if (payModeJs.contains(mode)) {
                    payModes.add(payModeJs);
                    payModesJs.remove(payModeJs);
                    break;
                }
            }
        }

        return "[" + StringUtils.join(payModes, PROPERTIES_SEPARATOR) + "]";
    }

    public String getMailHost() {
        return getProperty(MAIL_HOST);
    }

    public boolean isTLS() {
        String isTLS = getNoObligatoryProperty(MAIL_TLS);
        return isTLS != null && isTLS.equals("true");
    }

    public String getMailPort() {
        String port = getNoObligatoryProperty(MAIL_PORT);
        if (port != null) {
            return port;
        }
        else {
            return "587";
        }
    }

    public boolean isMailSSL() {
        String ssl = getNoObligatoryProperty(MAIL_SSL);
        return ssl != null && ssl.equals("true");
    }

    public String getGastosGestion() {
        return getProperty(GASTOS_GESTION);
    }

    public String getCargoInformeEfectivo() {
        return getProperty(INFORME_EFECTIVO_CARGO);
    }

    public String getFirmanteInformeEfectivo() {
        return getProperty(INFORME_EFECTIVO_FIRMANTE);
    }

    public int getMargenVentaTaquillaMinutos() {
        try {
            return Integer.parseInt(getProperty(MARGEN_VENTA_TAQUILLA_MINUTOS));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public String getAuthClass() {
        return getProperty(AUTH_CLASS);
    }

    public List<String> getAdminLogin() {
        return Arrays.asList(getProperty(ADMIN_LOGIN).split(PROPERTIES_SEPARATOR));
    }

    public List<String> getAdminPassword() {
        return Arrays.asList(getProperty(ADMIN_PASSWORD).split(PROPERTIES_SEPARATOR));
    }

    public String getJdbUrl() {
        return getProperty(JDBC_URL);
    }

    public String getSyncTipo() {
        return getProperty(SYNC_TIPO);
    }

    public String[] getSyncUrlsRss() {
        return getProperty(SYNC_URL_TIPO).split(PROPERTIES_SEPARATOR);
    }

    public String getSyncUrlsHeaderToken() {
        return getNoObligatoryProperty(SYNC_HEADER_TOKEN);
    }

    public String getSyncUrlsToken() {
        return getNoObligatoryProperty(SYNC_TOKEN);
    }

    public String getBarcodeWidthHeight() {
        return getProperty(BARCODE_WIDTH_HEIGHT);
    }

    public String getMailingClass() {
        return getProperty(MAILING_CLASS);
    }

    public String getTmpFolder() {
        return getProperty(TMP_FOLDER);
    }

    public String getPassphrase() {
        return getProperty(PGP_PASSPHRASE);
    }

    public String getCodigoBuzon() {
        return getProperty(CODIGO_BUZON);
    }

    public String getDBUser() {
        return getProperty(DB_USER);
    }

    public String getDBPassword() {
        return getProperty(DB_PASS);
    }

    public String getActiveDirectoryIP() {
        return getProperty(ACTIVE_DIRECTORY_IP);
    }

    public String getActiveDirectoryPort() {
        return getProperty(ACTIVE_DIRECTORY_Port);
    }

    public String getActiveDirectoryDomain() {
        return getProperty(ACTIVE_DIRECTORY_DOMAIN);
    }

    public String getActiveDirectoryDC() {
        return getProperty(ACTIVE_DIRECTORY_DC);
    }

    public String getPathImagenSustitutiva() {
        return getNoObligatoryProperty(IMAGEN_SUSTITUTIVA);
    }

    public String getPathImagenPubliSustitutiva() {
        return getNoObligatoryProperty(IMAGEN_PUBLI_SUSTITUTIVA);
    }

    public String getImagenSustitutivaContentType() {
        return getNoObligatoryProperty(IMAGEN_SUSTITUTIVA_CONTENT_TYPE);
    }

    public String getImagenPubliSustitutivaContentType() {
        return getNoObligatoryProperty(IMAGEN_PUBLI_SUSTITUTIVA_CONTENT_TYPE);
    }

    public String getEnviarMailsEntradas() {
        return getNoObligatoryProperty(ENVIAR_MAILS_ENTRADAS);
    }

    public String getHorasVentaAnticipada() {
        return getNoObligatoryProperty(HORAS_VENTA_ANTICIPADA);
    }

    public boolean isDebug() {
        String debug = getNoObligatoryProperty(ENTORNO);
        if (debug == null || !debug.equalsIgnoreCase("prod"))
            return true;
        else
            return false;
    }

    public boolean getGenerarCifrado() {
        String generarCifrado = getNoObligatoryProperty(GENERAR_CIFRADO);
        if (generarCifrado == null || generarCifrado.equalsIgnoreCase("true"))
            return true;
        else
            return false;
    }

    public String getPathJson() {
        return JSON_LOCALIZACIONES_PATH;
    }

    public Date dateConMargenTrasVenta() {
        Calendar limite = Calendar.getInstance();
        limite.add(Calendar.MINUTE, -getMargenVentaTaquillaMinutos());

        return limite.getTime();
    }

    public boolean isDataDegradada(Timestamp data) {
        long aux = data.getTime() + (getMargenVentaTaquillaMinutos() * 60 * 1000);
        Timestamp timeAux = new Timestamp(aux);
        return (DateUtils.getCurrentDate().after(timeAux));
    }

    public Timestamp getDataTopePerASaberSiEsDegradada(Timestamp data) {
        long aux = data.getTime() + (getMargenVentaTaquillaMinutos() * 60 * 1000);
        return new Timestamp(aux);
    }

    public boolean isLoadedFromResource() {
        String isLoadedFromResource = getNoObligatoryProperty(IS_LOADED_FROM_RESOURCE);
        if (isLoadedFromResource == null || !isLoadedFromResource.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public String getMailPassword() {
        return getProperty(MAIL_PASSWORD);
    }

    public String getMailUsername() {
        return getProperty(MAIL_USERNAME);
    }

    public String getAppleWWDRCAPath() {
        return getProperty(APPLEWWDRCA_PATH);
    }

    public String getAppleKeyStorePath() {
        return getProperty(APPLE_KEYSTORE_PATH);
    }

    public String getAppleKeyStorePass() {
        return getProperty(APPLE_KEYSTORE_PASS);
    }

    public String getAppleTeamId() {
        return getProperty(APPLE_KEYSTORE_TEAMID);
    }

    public String getAppleCertName() {
        return getProperty(APPLE_KEYSTORE_NAME);
    }

    public String getMailDefaultSender() {
        String defaultSender = getNoObligatoryProperty(MAIL_DEFAULT_SENDER);
        String emailFrom = defaultSender != null && !defaultSender.isEmpty() ? defaultSender : "no_reply@4tic.com";
        return emailFrom;
    }

    public String getNotFoundURL() {
        String notFoundURL = getNoObligatoryProperty(NOT_FOUND_URL);
        if (notFoundURL == null)
            notFoundURL = "http://www.4tic.com/software-venta-entradas404.html";
        return notFoundURL;
    }

    public String getAdminToken() {
        return getNoObligatoryProperty(ADMIN_TOKEN);
    }

    public String getQueueDomains() {
        return getNoObligatoryProperty(QUEUE_DOMAINS);
    }

    public int getQueueLimit(int defaultValue) {
        String queueLimit = getNoObligatoryProperty(QUEUE_LIMIT);
        return queueLimit != null ? Integer.valueOf(queueLimit) : defaultValue;
    }
}
