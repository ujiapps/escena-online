package es.uji.apps.par.db;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the PAR_MOTIVOS_CONOCERNOS database table.
 * 
 */
@Entity
@Table(name = "PAR_COMO_NOS_CONOCISTE")
public class ComoNosConocisteDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "PAR_COMO_NOS_CONOCISTE_ID_GENERATOR", sequenceName = "HIBERNATE_SEQUENCE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAR_COMO_NOS_CONOCISTE_ID_GENERATOR")
	private long id;

    @ManyToOne
    @JoinColumn(name="CINE_ID")
    private CineDTO parCine;

	private String motivo;

    @OneToMany(mappedBy = "comoNosConociste")
    private List<CompraDTO> compras;

    public ComoNosConocisteDTO() {

    }

    public ComoNosConocisteDTO(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CineDTO getParCine() {
        return parCine;
    }

    public void setParCine(CineDTO parCine) {
        this.parCine = parCine;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public List<CompraDTO> getCompras() {
        return compras;
    }

    public void setCompras(List<CompraDTO> compras) {
        this.compras = compras;
    }
}