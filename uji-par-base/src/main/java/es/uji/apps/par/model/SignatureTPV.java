package es.uji.apps.par.model;

public enum SignatureTPV {
    HMAC_SHA256_V1, CECA_SHA1, CECA_SHA2, PAYNOPAIN
}
