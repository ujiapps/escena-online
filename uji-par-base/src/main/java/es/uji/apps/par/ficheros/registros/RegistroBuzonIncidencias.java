package es.uji.apps.par.ficheros.registros;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import es.uji.apps.par.exceptions.RegistroSerializaException;

public class RegistroBuzonIncidencias {
    private static SimpleDateFormat DAY_FORMAT = new SimpleDateFormat("ddMMyy");

    private String buzon;
    private Date fechaInicio;
    private Date fechaFin;

    public RegistroBuzonIncidencias(
        String buzon,
        Date fechaInicio,
        Date fechaFin
    ) {
        this.buzon = buzon;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public String serializa() throws RegistroSerializaException {
        return String
            .format(Locale.ENGLISH, "0%s%s%s", buzon, DAY_FORMAT.format(fechaInicio), DAY_FORMAT.format(fechaFin));
    }

}
