package es.uji.apps.par.exceptions;

public class ImagenConAlphaException extends GeneralPARException
{
    public ImagenConAlphaException()
    {
        super(IMAGEN_ALPHA_ERROR_CODE, IMAGEN_ALPHA_ERROR);
    }
}
