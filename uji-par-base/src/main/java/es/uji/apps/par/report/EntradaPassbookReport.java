package es.uji.apps.par.report;

import de.brendamour.jpasskit.PKBarcode;
import de.brendamour.jpasskit.PKField;
import de.brendamour.jpasskit.PKPass;
import de.brendamour.jpasskit.enums.PKBarcodeFormat;
import de.brendamour.jpasskit.passes.PKEventTicket;
import de.brendamour.jpasskit.signing.PKFileBasedSigningUtil;
import de.brendamour.jpasskit.signing.PKPassTemplateInMemory;
import de.brendamour.jpasskit.signing.PKSigningInformation;
import de.brendamour.jpasskit.signing.PKSigningInformationUtil;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.MissingResourceException;

import javax.imageio.ImageIO;

import es.uji.apps.fopreports.serialization.ReportSerializationException;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.utils.DateUtils;

public class EntradaPassbookReport implements EntradaReportOnlineInterface {
    private static final Logger log = LoggerFactory.getLogger(EntradaPassbookReport.class);
    private String appleWWDRCAPath;
    private String appleKeyStorePath;
    private String appleKeyStorePass;
    private String appleTeamId;
    private String appleCertName;
    private static final int NUM_MAX_CONDICIONES = 12;

    private String titulo;
    private String fecha;
    private String hora;
    private String horaApertura;
    private String zona;
    private String fila;
    private String numero;
    private String total;
    private String urlPublicidad;
    private String urlPortada;
    private String barcode;
    private String cif;
    private String nombreEntidad;
    private String direccion;
    private String promotor;
    private String nifPromotor;
    private String sala;
    private String tipo;
    private Boolean tarifaDefecto;
    private String emailCompra;
    private String codigoCine;
    private boolean showIVA;
    private Cine cine;
    Configuration configuration;
    Locale locale;

    public EntradaPassbookReport(
    ) {
    }

    public void generaPaginaButaca(
        EntradaModelReport entrada,
        String urlPublic
    ) {
        this.setFila(entrada.getFila());
        this.setNumero(entrada.getNumero());
        this.setZona(entrada.getZona());
        this.setTotal(entrada.getTotal());
        this.setBarcode(entrada.getBarcode());
        this.setSala(entrada.getSala());
        this.setTipo(entrada.getTipo());
        this.setTarifaDefecto(entrada.getTarifaDefecto());
        this.setShowIVA(entrada.getShowIVA());
    }

    public void serialize(OutputStream output) throws ReportSerializationException {
        try {
            PKSigningInformation pkSigningInformation = getPkSigningInformation();
            PKPassTemplateInMemory passTemplate = new PKPassTemplateInMemory();
            addImagesToTemplate(passTemplate);
            PKFileBasedSigningUtil pkSigningUtil = new PKFileBasedSigningUtil();
            PKPass pass = createPkPassFromCompra();
            byte[] signedAndZippedPkPassArchive =
                pkSigningUtil.createSignedAndZippedPkPassArchive(pass, passTemplate, pkSigningInformation);
            IOUtils.copy(new ByteArrayInputStream(signedAndZippedPkPassArchive), output);
        } catch (Exception e) {
            log.error("Error al crear el archivo passbook", e);
            throw new ReportSerializationException(e);
        }
    }

    private PKSigningInformation getPkSigningInformation() throws ReportSerializationException {
        PKSigningInformation pkSigningInformation;
        try {
            pkSigningInformation = new PKSigningInformationUtil()
                .loadSigningInformationFromPKCS12AndIntermediateCertificate(appleKeyStorePath, appleKeyStorePass,
                    appleWWDRCAPath);
        } catch (Exception e) {
            log.error("Error generando información firmada de passbook", e);
            throw new ReportSerializationException(e);
        }
        return pkSigningInformation;
    }

    private void addImagesToTemplate(PKPassTemplateInMemory passTemplate) throws IOException {
        addIconToPassTemplate(passTemplate);
        addLogoToPassTemplate(passTemplate);
        addEventoImagenToPassTemplate(passTemplate);
    }

    private void addEventoImagenToPassTemplate(PKPassTemplateInMemory passTemplate) throws IOException {
        byte[] imageBytes = IOUtils.toByteArray(URI.create(this.urlPortada));
        passTemplate.addFile(PKPassTemplateInMemory.PK_THUMBNAIL, escaleImage(90, 90, imageBytes));
        passTemplate.addFile(PKPassTemplateInMemory.PK_THUMBNAIL_RETINA, escaleImage(180, 180, imageBytes));
    }

    private void addLogoToPassTemplate(PKPassTemplateInMemory passTemplate) throws IOException {
        byte[] logoBytes = cine.getBanner();
        passTemplate.addFile(PKPassTemplateInMemory.PK_LOGO, escaleImage(50, 120, logoBytes));
        passTemplate.addFile(PKPassTemplateInMemory.PK_LOGO_RETINA, escaleImage(100, 240, logoBytes));
    }

    private void addIconToPassTemplate(PKPassTemplateInMemory passTemplate) throws IOException {
        byte[] iconBytes = cine.getLogo();
        passTemplate.addFile(PKPassTemplateInMemory.PK_ICON, escaleImage(29, 29, iconBytes));
        passTemplate.addFile(PKPassTemplateInMemory.PK_ICON_RETINA, escaleImage(60, 60, iconBytes));
    }

    private PKPass createPkPassFromCompra(
    ) throws ReportSerializationException {
        PKPass pass = new PKPass();
        PKEventTicket eventTicket = new PKEventTicket();
        createHeaderFields(eventTicket);
        createPrimaryFields(eventTicket);
        createSecondaryFields(eventTicket);
        createAuxiliaryFields(eventTicket);
        createBackFields(eventTicket);
        createBarcode(pass);
        pass.setEventTicket(eventTicket);
        pass.setLabelColor("rgb(96,151,191)");
        pass.setFormatVersion(1);
        pass.setPassTypeIdentifier(appleCertName);
        pass.setSerialNumber(this.barcode);
        pass.setTeamIdentifier(appleTeamId);
        pass.setOrganizationName(this.nombreEntidad);
        pass.setDescription(this.titulo);
        pass.setBackgroundColorAsObject(new Color(238, 242, 246));
        pass.setForegroundColor("rgb(71,76,78)");

        String fechaHora = String.format("%s %s", this.fecha, this.hora);
        Date relevantDate = null;
        try {
            relevantDate = DateUtils.spanishStringWithHourstoDate(fechaHora);
        } catch (ParseException e) {
            log.error("", e);
            throw new ReportSerializationException(e);
        }
        pass.setRelevantDate(relevantDate);

        return pass;
    }

    @Override
    public EntradaReportOnlineInterface create(
        Locale locale,
        Configuration configuration
    ) {
        this.locale = locale;
        this.configuration = configuration;

        this.appleWWDRCAPath = configuration.getAppleWWDRCAPath();
        this.appleKeyStorePath = configuration.getAppleKeyStorePath();
        this.appleKeyStorePass = configuration.getAppleKeyStorePass();
        this.appleTeamId = configuration.getAppleTeamId();
        this.appleCertName = configuration.getAppleCertName();

        return this;
    }

    private void createBarcode(PKPass pass) {
        PKBarcode barcode = new PKBarcode();
        barcode.setFormat(PKBarcodeFormat.PKBarcodeFormatQR);
        barcode.setMessage(this.barcode);
        barcode.setMessageEncoding(Charset.forName("utf-8"));
        pass.setBarcodes(Arrays.asList(barcode));
    }

    private void createHeaderFields(PKEventTicket eventTicket) {
        PKField headerField = new PKField();
        headerField.setKey("fechaHora");
        headerField.setLabel(this.fecha);
        headerField.setValue(this.hora);
        java.util.List<PKField> headerFields = new ArrayList<>();
        headerFields.add(headerField);
        eventTicket.setHeaderFields(headerFields);
    }

    private void createPrimaryFields(PKEventTicket eventTicket) {
        java.util.List<PKField> primaryFields = new ArrayList<PKField>();
        PKField balanceField = new PKField();
        balanceField.setKey("k1");
        String labelEvento = ResourceProperties.getProperty(locale, "informeTaquilla.tabla.evento");
        balanceField.setLabel(labelEvento);
        balanceField.setValue(this.titulo);
        primaryFields.add(balanceField);
        eventTicket.setPrimaryFields(primaryFields);
    }

    private void createSecondaryFields(PKEventTicket eventTicket) {
        java.util.List<PKField> secondaryFields = new ArrayList<>();
        String labelSala = ResourceProperties.getProperty(locale, "entrada.sala");
        PKField sala = new PKField("sala", labelSala, this.sala);
        secondaryFields.add(sala);
        eventTicket.setSecondaryFields(secondaryFields);
    }

    private void createAuxiliaryFields(PKEventTicket eventTicket) {
        java.util.List<PKField> auxiliarFields = new ArrayList<>();
        if (this.fila != null) {
            String labelFila = ResourceProperties.getProperty(locale, "entrada.filaS");
            PKField fila = new PKField("fila", labelFila, this.fila);
            auxiliarFields.add(fila);
        }
        if (this.numero != null) {
            String labelButaca = ResourceProperties.getProperty(locale, "entrada.butacaSimpleS");
            PKField asiento = new PKField("asiento", labelButaca, this.numero);
            auxiliarFields.add(asiento);
        }
        String labelZona = ResourceProperties.getProperty(locale, "entrada.zonaS");
        PKField zona = new PKField("zona", labelZona, this.zona);
        auxiliarFields.add(zona);
        String labelTarifa = ResourceProperties.getProperty(locale, "entrada.tarifaS");
        PKField tarifa = new PKField("tarifa", labelTarifa, this.tipo);
        auxiliarFields.add(tarifa);
        eventTicket.setAuxiliaryFields(auxiliarFields);
    }

    private void createBackFields(PKEventTicket eventTicket) {

        java.util.List<PKField> backFields = new ArrayList<>();
        if (!tarifaDefecto) {
            String labelTarifaObservaciones = ResourceProperties.getProperty(locale, "entrada.tarifaS");
            String valueTarifaObservaciones = ResourceProperties.getProperty(locale, "entrada.tarifaS.observaciones");
            PKField tarifaObservaciones =
                new PKField("tarifa_observaciones", labelTarifaObservaciones, valueTarifaObservaciones);
            backFields.add(tarifaObservaciones);
        }
        String labelApertura = ResourceProperties.getProperty(locale, "entrada.apertura");
        PKField aberturaPuertas = new PKField("abertura_puertas", labelApertura, this.horaApertura);
        backFields.add(aberturaPuertas);
        String labelCifPromotor = ResourceProperties.getProperty(locale, "entrada.nifS");
        PKField nifPromotor = new PKField("nifpromotor", labelCifPromotor, this.nifPromotor);
        backFields.add(nifPromotor);
        PKField uuidCompra = new PKField("uuidCompra", "Id", this.barcode);
        backFields.add(uuidCompra);
        String labelEvento = ResourceProperties.getProperty(locale, "informeTaquilla.tabla.evento");
        PKField tituloCompleto = new PKField("titulocompleto", labelEvento, this.titulo);
        backFields.add(tituloCompleto);
        PKField emailCompra = new PKField("emailCompra", "Email", this.emailCompra);
        backFields.add(emailCompra);
        String labelTotal = ResourceProperties.getProperty(locale, "entrada.total");
        PKField precioEntrada = new PKField("precioEntrada", labelTotal, this.total);
        backFields.add(precioEntrada);
        String valueEntradaValida = ResourceProperties.getProperty(locale, "entrada.entradaValida");
        PKField comentarios = new PKField("comentarios", " ", valueEntradaValida);
        backFields.add(comentarios);

        String condiciones = getCondiciones();
        PKField condicionesGenerales = new PKField("condicionesGenerales", " ", condiciones);
        backFields.add(condicionesGenerales);


        eventTicket.setBackFields(backFields);
    }

    private String getCondiciones() {
        StringBuilder sb = new StringBuilder();
        String condicionesLabel = ResourceProperties.getProperty(locale, "entrada.condiciones");
        sb.append(condicionesLabel);
        for (int i = 1; i <= NUM_MAX_CONDICIONES; i++) {
            String condicion = "";
            try {
                condicion = ResourceProperties.getProperty(locale, "entrada." + this.codigoCine + ".condicion" + i);
            } catch (MissingResourceException e) {
                try {
                    condicion = ResourceProperties.getProperty(locale, "entrada.condicion" + i);
                } catch (MissingResourceException m) {
                    log.warn("No se encuentra la propiedad entrada.condicion" + i);
                }
            }

            sb.append(condicion);
        }
        return sb.toString();
    }

    private ByteArrayInputStream escaleImage(
        int height,
        int width,
        byte[] image
    ) {
        try {
            ByteArrayInputStream imagenEventoIS = new ByteArrayInputStream(image);
            BufferedImage img = ImageIO.read(imagenEventoIS);
            if (height == 0) {
                height = (width * img.getHeight()) / img.getWidth();
            }
            if (width == 0) {
                width = (height * img.getWidth()) / img.getHeight();
            }
            Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(238, 242, 246), null);

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            ImageIO.write(imageBuff, "jpg", buffer);

            return new ByteArrayInputStream(buffer.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setHoraApertura(String horaApertura) {
        this.horaApertura = horaApertura;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public void setUrlPublicidad(String urlPublicidad) {
        this.urlPublicidad = urlPublicidad;
    }

    public void setUrlPortada(String urlPortada) {
        this.urlPortada = urlPortada;
    }

    public boolean esAgrupada() {
        return false;
    }

    public void setTotalButacas(int totalButacas) {
    }

    public void setCodigoCine(String codigoCine) {
        this.codigoCine = codigoCine;
    }

    public void setCine(Cine cine) {
        this.cine = cine;
    }

    public void setShowIVA(boolean showIVA) {
        this.showIVA = showIVA;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public void setNombreEntidad(String nombreEntidad) {
        this.nombreEntidad = nombreEntidad;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setPromotor(String promotor) {
        this.promotor = promotor;
    }

    public void setNifPromotor(String nifPromotor) {
        this.nifPromotor = nifPromotor;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setEmailCompra(String emailCompra) {
        this.emailCompra = emailCompra;
    }

    public void setTarifaDefecto(Boolean tarifaDefecto) {
        this.tarifaDefecto = tarifaDefecto;
    }
}
