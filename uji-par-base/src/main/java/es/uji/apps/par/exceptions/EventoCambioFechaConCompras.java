package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class EventoCambioFechaConCompras extends GeneralPARException {
	public EventoCambioFechaConCompras(long idEvento) {
		super(SESION_CAMBIO_FECHA_CON_COMPRAS_CODE);
	}
}
