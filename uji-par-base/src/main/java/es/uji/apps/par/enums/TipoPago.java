package es.uji.apps.par.enums;

public enum TipoPago
{
	METALICO, INVITACION, PINPAD, TARJETA, TARJETAOFFLINE, TRANSFERENCIA, CREDITO
}
