package es.uji.apps.par.services;

import com.google.common.base.Strings;

import com.mysema.query.Tuple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.config.ConfigurationSelector;
import es.uji.apps.par.dao.AbonadosDAO;
import es.uji.apps.par.dao.AbonosDAO;
import es.uji.apps.par.dao.ButacasDAO;
import es.uji.apps.par.dao.ComprasDAO;
import es.uji.apps.par.dao.SesionesDAO;
import es.uji.apps.par.dao.UsuariosDAO;
import es.uji.apps.par.db.AbonoDTO;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.ComoNosConocisteDTO;
import es.uji.apps.par.db.CompraBorradaDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.enums.TipoPago;
import es.uji.apps.par.exceptions.AbonoUsadoMaxEnDistintosEventosException;
import es.uji.apps.par.exceptions.AbonoUsadoMaxEnEventoException;
import es.uji.apps.par.exceptions.ButacaOcupadaAlActivarException;
import es.uji.apps.par.exceptions.ButacaOcupadaException;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.CompraCaducadaException;
import es.uji.apps.par.exceptions.CompraConCosteNoAnulableDesdeEnlace;
import es.uji.apps.par.exceptions.CompraNoExistente;
import es.uji.apps.par.exceptions.CompraSinButacasException;
import es.uji.apps.par.exceptions.EmailBlacklistException;
import es.uji.apps.par.exceptions.EmailCompraNoCoincideException;
import es.uji.apps.par.exceptions.EmailVerificacionException;
import es.uji.apps.par.exceptions.EntradaConCosteNoAnulableDesdeEnlace;
import es.uji.apps.par.exceptions.EntradaNoExistente;
import es.uji.apps.par.exceptions.EntradasNoAnulablesDesdeEnlace;
import es.uji.apps.par.exceptions.EventoNominalException;
import es.uji.apps.par.exceptions.FueraDePlazoVentaInternetException;
import es.uji.apps.par.exceptions.IncidenciaNotFoundException;
import es.uji.apps.par.exceptions.LimiteEntradasGratisSuperadoException;
import es.uji.apps.par.exceptions.LimiteEntradasPorEmailSuperadoException;
import es.uji.apps.par.exceptions.NoAbonadoException;
import es.uji.apps.par.exceptions.NoHayButacasLibresException;
import es.uji.apps.par.exceptions.TiempoMinioRequeridoParaAnularDesdeEnlace;
import es.uji.apps.par.exceptions.UsuarioNoPuedeAnularException;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.filters.ThirdPartyFilter;
import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.ComoNosConociste;
import es.uji.apps.par.model.Compra;
import es.uji.apps.par.model.CompraRequest;
import es.uji.apps.par.model.CompraYUso;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.LimitacionVenta;
import es.uji.apps.par.model.PreciosSesion;
import es.uji.apps.par.model.ResultadoCompra;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.PrecioUtils;

@Service
public class ComprasService {
    @Autowired
    private ComprasDAO comprasDAO;

    @Autowired
    private ButacasDAO butacasDAO;

    @Autowired
    private SesionesService sesionesService;

    @Autowired
    private PreciosService preciosService;

    @Autowired
    private ButacasService butacasService;

    @Autowired
    private SesionesDAO sesionesDAO;

    @Autowired
    private AbonosDAO abonosDAO;

    @Autowired
    private AbonadosDAO abonadosDAO;

    @Autowired
    private UsuariosDAO usuariosDAO;

    @Autowired
    Configuration configuration;

    @Autowired
    private MailComposerService mailComposerService;

    @Autowired
    private ConfigurationSelector configurationSelector;

    public boolean isMobile(HttpServletRequest request) {
        String mobile = (String) request.getSession().getAttribute(ThirdPartyFilter.MOBILE);
        return !Strings.isNullOrEmpty(mobile) && mobile.equals("true");
    }

    public String getSessionEmail(HttpServletRequest request) {
        return (String) request.getSession().getAttribute(ThirdPartyFilter.EMAIL);
    }

    public void eliminaCompraDeSesion(HttpServletRequest request) {
        request.getSession().invalidate();
        //        request.getSession().removeAttribute(EntradasService.BUTACAS_COMPRA);
        //        request.getSession().removeAttribute(EntradasService.UUID_COMPRA);
    }

    public ResultadoCompra registraCompraTaquilla(
        Long sesionId,
        CompraRequest compraRequest,
        String userUID
    ) throws NoHayButacasLibresException, ButacaOcupadaException, CompraSinButacasException,
        IncidenciaNotFoundException {
        return registraCompraTaquilla(sesionId, compraRequest.getButacasSeleccionadas(),
            compraRequest.getObservaciones(), new Date(), userUID);
    }

    public ResultadoCompra registraCompraTaquilla(
        Long sesionId,
        List<Butaca> butacasSeleccionadas,
        String observaciones,
        Date fechaCompra,
        String userUID
    ) throws NoHayButacasLibresException, ButacaOcupadaException, CompraSinButacasException,
        IncidenciaNotFoundException {
        return registraCompra(sesionId, butacasSeleccionadas, observaciones, true, fechaCompra, userUID);
    }

    public List<CompraYUso> getComprasYPresentadas(long sesionId) {
        List<CompraYUso> comprasPresentadas = new ArrayList<CompraYUso>();

        List<Tuple> resultados = comprasDAO.getComprasYPresentadas(sesionId);
        for (Tuple resultado : resultados) {
            String email = resultado.get(0, String.class);
            long compras = resultado.get(1, Long.class);
            long presentadas = resultado.get(2, Long.class);

            comprasPresentadas.add(new CompraYUso(email, compras, presentadas));
        }

        return comprasPresentadas;
    }

    @Transactional(rollbackForClassName = {"CompraButacaDescuentoNoDisponible", "FueraDePlazoVentaInternetException",
        "NoHayButacasLibresException", "ButacaOcupadaException", "CompraSinButacasException",
        "LimiteEntradasPorEmailSuperadoException", "LimiteEntradasGratisSuperadoException"})
    public ResultadoCompra realizaCompraInternet(
        Long sesionId,
        List<Butaca> butacasSeleccionadas,
        String uuidCompraActual,
        String userUID
    ) {
        Sesion sesion = sesionesService.getSesion(sesionId, userUID);

        if (!sesion.getEnPlazoVentaInternet())
            throw new FueraDePlazoVentaInternetException(sesionId);

        if (uuidCompraActual != null && !uuidCompraActual.equals(""))
            comprasDAO.borrarCompraNoPagada(uuidCompraActual);

        return registraCompra(sesionId, butacasSeleccionadas, null, false, new Date(), userUID);
    }

    private synchronized ResultadoCompra registraCompra(
        Long sesionId,
        List<Butaca> butacasSeleccionadas,
        boolean taquilla,
        CompraDTO compraDTO,
        String userUID
    ) throws NoHayButacasLibresException, ButacaOcupadaException, CompraSinButacasException,
        IncidenciaNotFoundException {
        ResultadoCompra resultadoCompra = new ResultadoCompra();
        Map<String, Map<Long, PreciosSesion>> preciosSesionPorLocalizacion =
            preciosService.getPreciosSesionPorLocalizacion(sesionId, userUID);
        butacasDAO
            .reservaButacas(sesionId, compraDTO, butacasSeleccionadas, preciosSesionPorLocalizacion, taquilla, userUID,
                configurationSelector.hasNumeracionSecuencialEnabled());

        resultadoCompra.setCorrecta(true);
        resultadoCompra.setId(compraDTO.getId());
        resultadoCompra.setUuid(compraDTO.getUuid());

        if (taquilla) {
            SesionDTO sesionDTO = sesionesDAO.getSesion(sesionId, userUID);
            long totalAnuladas = sesionesDAO.getButacasAnuladasTotal(sesionId);
            boolean isVentaDegradada = configuration.isDataDegradada(sesionDTO.getFechaCelebracion());
            boolean isReprogramada = sesionesDAO
                .isSesionReprogramada(sesionDTO.getFechaCelebracion(), sesionDTO.getParSala().getId(), sesionId,
                    userUID);

            int tipoIncidenciaId = sesionesDAO.getTipoIncidenciaSesion(totalAnuladas, isVentaDegradada, isReprogramada);
            sesionesDAO.setIncidencia(sesionId, tipoIncidenciaId);
        }

        return resultadoCompra;
    }

    private synchronized ResultadoCompra registraCompra(
        Long sesionId,
        List<Butaca> butacasSeleccionadas,
        String observaciones,
        boolean taquilla,
        Date fechaCompra,
        String userUID
    ) throws NoHayButacasLibresException, ButacaOcupadaException, CompraSinButacasException,
        IncidenciaNotFoundException, LimiteEntradasPorEmailSuperadoException, LimiteEntradasGratisSuperadoException {
        if (butacasSeleccionadas.size() == 0) {
            throw new CompraSinButacasException();
        }

        Sesion sesion = sesionesService.getSesion(sesionId, userUID);
        Cine cine = sesion.getSala().getCine();

        BigDecimal importeConComision = calculaImporteButacas(sesionId, butacasSeleccionadas, !taquilla, userUID);
        BigDecimal importeSinComision = calculaImporteButacas(sesionId, butacasSeleccionadas, false, userUID);
        BigDecimal comision = taquilla ? BigDecimal.ZERO : cine.getComision();

        if (!taquilla) {
            Evento parEvento = sesion.getEvento();
            if (isEntradasLimitadasPorEmail(Evento.eventoToEventoDTO(parEvento))) {
                if (butacasSeleccionadas.size() > parEvento.getEntradasPorEmail()) {
                    throw new LimiteEntradasPorEmailSuperadoException(parEvento.getEntradasPorEmail());
                }
            } else if (isEntradasLimitadasPorCine(cine)) {
                if (isCineEntradasLimitadasParaNoGratuitos(cine)
                    || importeConComision.compareTo(BigDecimal.ZERO) == 0) {
                    if (butacasSeleccionadas.size() > cine.getLimiteEntradasGratuitasPorCompra()) {
                        throw new LimiteEntradasGratisSuperadoException();
                    }
                }
            }
        }

        CompraDTO compraDTO = comprasDAO
            .insertaCompra(sesionId, fechaCompra != null ? fechaCompra : new Date(), taquilla, importeConComision,
                importeSinComision, comision, observaciones, userUID);

        return registraCompra(sesionId, butacasSeleccionadas, taquilla, compraDTO, userUID);
    }

    private boolean isCineEntradasLimitadasParaNoGratuitos(Cine cine) {
        return cine.getLimiteNoGratuitas() != null && cine.getLimiteNoGratuitas();
    }

    private boolean isEntradasLimitadasPorCine(
        Cine cine
    ) {
        return cine.getLimiteEntradasGratuitasPorCompra() != null;
    }

    public LimitacionVenta getLimiteEntradas(Evento evento) {
        Cine cine = evento.getCine();
        Integer limiteEntradas = null;
        boolean byEmail = false;
        if (isEntradasLimitadasPorEmail(Evento.eventoToEventoDTO(evento))) {
            limiteEntradas = evento.getEntradasPorEmail();
            byEmail = true;
        } else if (isCineEntradasLimitadasParaNoGratuitos(cine) && isEntradasLimitadasPorCine(cine)) {
            limiteEntradas = cine.getLimiteEntradasGratuitasPorCompra();
        }
        return new LimitacionVenta(limiteEntradas, byEmail);
    }

    @Transactional(rollbackForClassName = {"NoHayButacasLibresException", "ButacaOcupadaException",
        "CompraSinButacasException", "IncidenciaNotFoundException"})
    public synchronized ResultadoCompra registraCompra(
        Long sesionId,
        List<Butaca> butacasSeleccionadas,
        boolean taquilla,
        BigDecimal importe,
        BigDecimal importeSinComision,
        BigDecimal comision,
        String email,
        String nombre,
        String apellidos,
        String userUID
    ) throws NoHayButacasLibresException, ButacaOcupadaException, CompraSinButacasException,
        IncidenciaNotFoundException {
        if (butacasSeleccionadas.size() == 0)
            throw new CompraSinButacasException();

        CompraDTO compraDTO = comprasDAO
            .insertaCompra(sesionId, new Date(), taquilla, importe, importeSinComision, comision, email, nombre,
                apellidos, userUID);

        return registraCompra(sesionId, butacasSeleccionadas, taquilla, compraDTO, userUID);
    }

    public BigDecimal calculaImporteButacas(
        Long sesionId,
        List<Butaca> butacasSeleccionadas,
        boolean conComision,
        String userUID
    ) {
        Sesion sesion = sesionesService.getSesion(sesionId, userUID);
        BigDecimal importe = new BigDecimal("0");
        Map<String, Map<Long, PreciosSesion>> preciosLocalizacion =
            preciosService.getPreciosSesionPorLocalizacion(sesionId, userUID);

        for (Butaca butaca : butacasSeleccionadas) {
            Map<Long, PreciosSesion> mapaTarifasPrecios = preciosLocalizacion.get(butaca.getLocalizacion());

            BigDecimal precio = butaca.getPrecioSinComision();
            if (mapaTarifasPrecios != null) {
                PreciosSesion preciosSesion = mapaTarifasPrecios.get(Long.valueOf(butaca.getTipo()));
                if (preciosSesion != null) {
                    precio = preciosSesion.getPrecio();
                }
            }

            if (conComision) {
                BigDecimal comision = sesion.getSala().getCine().getComision();
                BigDecimal precioConComision = PrecioUtils.getPrecioConComision(precio, comision);

                importe = importe.add(precioConComision);
            } else {
                importe = importe.add(precio);
            }
        }

        return importe;
    }

    @Transactional
    public void marcaPagada(
        long idCompra,
        TipoPago tipoPago
    ) {
        comprasDAO.marcarPagada(idCompra, tipoPago);
        if (configuration.isIdEntrada()) {
            butacasDAO.asignarIdEntrada(idCompra);
        }
    }

    @Transactional
    public void marcarPagadaConReferenciaDePago(
        Long idCompra,
        String tipoPago,
        String referenciaDePago,
        String email,
        String nombre,
        String apellidos,
        String language
    ) {
        CompraDTO compraDTO =
            comprasDAO.marcarPagadaConReferenciaDePago(idCompra, tipoPago, referenciaDePago, email, nombre, apellidos);
        if (configuration.isIdEntrada()) {
            butacasDAO.asignarIdEntrada(idCompra);
        }

        if (!Strings.isNullOrEmpty(email) && isEmailValid(email) && compraDTO != null) {
            mailComposerService.registraMail(compraDTO, email, compraDTO.getUuid(), new Locale(language, "ES"));
        }
    }

    @Transactional
    public void cambiarFecha(
        Long idCompra,
        Date fecha
    ) {
        comprasDAO.cambiarFecha(idCompra, fecha);
    }

    public void eliminaPendientes() throws IncidenciaNotFoundException {
        comprasDAO.eliminaComprasPendientes();
    }

    public CompraDTO getCompraById(long idCompra) {
        return comprasDAO.getCompraById(idCompra);
    }

    public CompraDTO getCompraBorradaById(long idCompra) {
        CompraBorradaDTO compraBorrada = comprasDAO.getCompraBorradaById(idCompra);
        return new CompraDTO(compraBorrada);
    }

    public CompraDTO getCompraByUuid(String uuidCompra) {
        return comprasDAO.getCompraByUuid(uuidCompra);
    }

    public void marcaPagadaPasarela(
        long idCompra,
        String codigoPago
    ) {
        comprasDAO.marcarPagadaPasarela(idCompra, codigoPago);
    }

    @Transactional
    public CompraDTO rellenaDatosComprador(
        String uuidCompra,
        String nombre,
        String apellidos,
        String direccion,
        String poblacion,
        String cp,
        String provincia,
        String telefono,
        String email,
        String emailVerificacion,
        Integer comoNosConocisteId,
        String infoPeriodica,
        String condicionesPrivacidad,
        boolean contratoGeneral,
        String condicionesContratacion,
        String urlCondicionesCancelacion,
        String condicionesCancelacion,
        List<String> nombresEntradas
    ) {
        if (validDatosComprador(nombre, apellidos, email, emailVerificacion, condicionesPrivacidad, contratoGeneral,
            condicionesContratacion, urlCondicionesCancelacion, condicionesCancelacion)) {
            CompraDTO compraByUuid = comprasDAO.getCompraByUuid(uuidCompra);
            if (compraByUuid.getCaducada()) {
                throw new CompraCaducadaException();
            }
            EventoDTO parEvento = compraByUuid.getParSesion().getParEvento();
            if (isEntradasLimitadasPorEmail(parEvento) && superaLimiteEntradasPorEmail(email, compraByUuid,
                parEvento)) {
                throw new LimiteEntradasPorEmailSuperadoException(parEvento.getEntradasPorEmail());
            }

            if (parEvento.getEntradasNominales() != null && parEvento.getEntradasNominales()) {
                if (nombresEntradas.stream().map(String::trim).anyMatch(p -> p.length() == 0)) {
                    throw new EventoNominalException();
                } else {
                    List<Long> sesionesIds = butacasService.getSesionesButacas(compraByUuid.getParButacas());
                    for (Long sesionId : sesionesIds) {
                        List<ButacaDTO> butacas =
                            compraByUuid.getParButacas().stream().filter(b -> b.getParSesion().getId() == sesionId)
                                .collect(Collectors.toList());
                        for (int i = 0; i < butacas.size(); i++) {
                            butacas.get(i).setNombreCompleto(nombresEntradas.get(i));
                        }
                    }
                    comprasDAO.actualizaButacasNombres(compraByUuid.getParButacas());
                }
            }

            List<Long> tarifaIds =
                compraByUuid.getParButacas().stream().map(ButacaDTO::getTipo).distinct().map(Long::valueOf)
                    .collect(Collectors.toList());
            List<AbonoDTO> abonosDeLaCompra = abonosDAO.getByIds(tarifaIds);
            List<TarifaDTO> abonosDelComprador = abonadosDAO.getAbonosByAbonadoEmail(email);

            Optional<AbonoDTO> abonoEnCompraNoAbonado = abonosDeLaCompra.stream().filter(
                abono -> !abonosDelComprador.stream().map(TarifaDTO::getId).collect(Collectors.toList())
                    .contains(abono.getId())).findAny();

            if (!abonoEnCompraNoAbonado.isPresent()) {
                checkUsosAbono(email, parEvento, abonosDeLaCompra, compraByUuid.getParButacas());

                compraByUuid = comprasDAO
                    .rellenaDatosComprador(uuidCompra, nombre, apellidos, direccion, poblacion, cp, provincia, telefono,
                        email, comoNosConocisteId, infoPeriodica);
            } else {
                throw new NoAbonadoException(email, abonoEnCompraNoAbonado.get().getNombre());
            }

            return compraByUuid;
        }
        return null;
    }

    private boolean superaLimiteEntradasPorEmail(
        String email,
        CompraDTO compraByUuid,
        EventoDTO parEvento
    ) {
        return comprasDAO.getEntradasEventoYEmail(parEvento.getId(), email) + compraByUuid.getParButacas().size()
            > parEvento.getEntradasPorEmail();
    }

    private boolean isEntradasLimitadasPorEmail(EventoDTO parEvento) {
        return parEvento.getEntradasLimitadas() != null && parEvento.getEntradasLimitadas()
            && parEvento.getEntradasPorEmail() != null;
    }

    private void checkUsosAbono(
        String email,
        EventoDTO parEvento,
        List<AbonoDTO> abonosDeLaCompra,
        List<ButacaDTO> butacas
    ) {
        for (AbonoDTO abonoDTO : abonosDeLaCompra) {
            long numeroButacasCompraConAbono =
                butacas.stream().filter(b -> b.getTipo().equals(String.valueOf(abonoDTO.getId()))).count();

            int numeroAbonos = abonadosDAO.getNumeroAbonosByAbonoIdAndEmail(abonoDTO.getId(), email);
            Long usosAbonoEnMismoEvento =
                abonadosDAO.usosAbonoEnMismoEvento(parEvento.getId(), email, abonoDTO.getId());
            Long usosAbonoEnEventosDistintos = abonadosDAO.usosAbonoEnEventosDistintos(email, abonoDTO.getId());

            if (numeroButacasCompraConAbono + usosAbonoEnMismoEvento > numeroAbonos) {
                throw new AbonoUsadoMaxEnEventoException(email, abonoDTO.getNombre());
            } else if (abonoDTO.getMaxEventos() != null
                && abonoDTO.getMaxEventos() * numeroAbonos <= usosAbonoEnEventosDistintos) {
                throw new AbonoUsadoMaxEnDistintosEventosException(email, abonoDTO.getNombre());
            }
        }
    }

    private boolean validDatosComprador(
        String nombre,
        String apellidos,
        String email,
        String emailVerificacion,
        String condicionesPrivacidad,
        boolean contratoGeneral,
        String condicionesContratacion,
        String urlCondicionesCancelacion,
        String condicionesCancelacion
    ) {
        if (nombre == null || nombre.equals("")) {
            throw new CampoRequeridoException("nombre");
        }

        if (apellidos == null || apellidos.equals("")) {
            throw new CampoRequeridoException("apellidos");
        }

        if (Strings.isNullOrEmpty(email)) {
            throw new CampoRequeridoException("email");
        } else if (Strings.isNullOrEmpty(emailVerificacion) || !email.equalsIgnoreCase(emailVerificacion)) {
            throw new EmailVerificacionException();
        } else {
            if (!isEmailValid(email) || !isEmailValid(emailVerificacion)) {
                throw new CampoRequeridoException("emailIncorrecto");
            } else if (isEmailInBlacklist(email)) {
                throw new EmailBlacklistException();
            }
        }

        if (condicionesPrivacidad == null || condicionesPrivacidad.equals("")) {
            throw new CampoRequeridoException("condicionesPrivacidad");
        }

        if (contratoGeneral) {
            if (condicionesContratacion == null || condicionesContratacion.equals("")) {
                throw new CampoRequeridoException("condicionesContratacion");
            }
        }

        if (urlCondicionesCancelacion != null) {
            if (condicionesCancelacion == null || condicionesCancelacion.equals("")) {
                throw new CampoRequeridoException("condicionesCancelacion");
            }
        }
        return true;
    }

    private boolean isEmailInBlacklist(String email) {
        return mailComposerService.isEmailInBlacklist(email);
    }

    static public boolean isEmailValid(String email) {
        final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        if (!pattern.matcher(email).matches()) {
            return false;
        } else {
            return true;
        }
    }

    private Date addHoraMinutoToFecha(
        Date fecha,
        int hora,
        int minuto
    ) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        cal.set(Calendar.HOUR_OF_DAY, hora);
        cal.set(Calendar.MINUTE, minuto);
        return cal.getTime();
    }

    @Transactional(rollbackForClassName = {"NoHayButacasLibresException", "ButacaOcupadaException",
        "CompraSinButacasException"})
    public ResultadoCompra reservaButacas(
        Long sesionId,
        Date desde,
        Date hasta,
        List<Butaca> butacasSeleccionadas,
        String observaciones,
        int horaInicial,
        int horaFinal,
        int minutoInicial,
        int minutoFinal,
        String userUID
    ) throws NoHayButacasLibresException, ButacaOcupadaException, CompraSinButacasException {
        if (butacasSeleccionadas.size() == 0)
            throw new CompraSinButacasException();

        ResultadoCompra resultadoCompra = new ResultadoCompra();


        desde = addHoraMinutoToFecha(desde, horaInicial, minutoInicial);
        hasta = addHoraMinutoToFecha(hasta, horaFinal, minutoFinal);

        CompraDTO compraDTO = comprasDAO.reserva(sesionId, new Date(), desde, hasta, observaciones, userUID);
        Map<String, Map<Long, PreciosSesion>> preciosSesionPorLocalizacion =
            preciosService.getPreciosSesionPorLocalizacion(sesionId, userUID);
        butacasDAO
            .reservaButacas(sesionId, compraDTO, butacasSeleccionadas, preciosSesionPorLocalizacion, true, userUID,
                configurationSelector.hasNumeracionSecuencialEnabled());

        resultadoCompra.setCorrecta(true);
        resultadoCompra.setId(compraDTO.getId());
        resultadoCompra.setUuid(compraDTO.getUuid());

        return resultadoCompra;
    }

    public List<Compra> getComprasBySesion(
        long sesionId,
        int showAnuladas,
        int showOnline,
        String search,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter
    ) {
        List<Compra> result = new ArrayList<Compra>();

        List<Tuple> tuples = comprasDAO
            .getComprasBySesion(sesionId, showAnuladas, showOnline, search, sortParameter, start, limit, filter);

        for (Tuple tupla : tuples) {
            Tuple tupleCompra = tupla.get(0, Tuple.class);
            CompraDTO compraDTO = new CompraDTO();
            compraDTO.setId(tupleCompra.get(0, Long.class));
            compraDTO.setAnulada(tupleCompra.get(1, Boolean.class));
            compraDTO.setApellidos(tupleCompra.get(2, String.class));
            compraDTO.setCaducada(tupleCompra.get(3, Boolean.class));
            compraDTO.setCodigoPagoPasarela(tupleCompra.get(4, String.class));
            compraDTO.setCodigoPagoTarjeta(tupleCompra.get(5, String.class));
            compraDTO.setCp(tupleCompra.get(6, String.class));
            compraDTO.setDesde(tupleCompra.get(7, Timestamp.class));
            compraDTO.setDireccion(tupleCompra.get(8, String.class));
            compraDTO.setEmail(tupleCompra.get(9, String.class));
            compraDTO.setFecha(tupleCompra.get(10, Timestamp.class));
            compraDTO.setHasta(tupleCompra.get(11, Timestamp.class));
            compraDTO.setImporte(tupleCompra.get(12, BigDecimal.class));
            compraDTO.setImporteSinComision(tupleCompra.get(13, BigDecimal.class));
            compraDTO.setComision(tupleCompra.get(14, BigDecimal.class));
            compraDTO.setInfoPeriodica(tupleCompra.get(15, Boolean.class));
            compraDTO.setNombre(tupleCompra.get(16, String.class));
            compraDTO.setObservacionesReserva(tupleCompra.get(17, String.class));
            compraDTO.setPagada(tupleCompra.get(18, Boolean.class));
            compraDTO.setParSesion(new SesionDTO(tupleCompra.get(19, Long.class)));
            compraDTO.setPoblacion(tupleCompra.get(20, String.class));
            compraDTO.setProvincia(tupleCompra.get(21, String.class));
            compraDTO.setReciboPinpad(tupleCompra.get(22, String.class));
            compraDTO.setReferenciaPago(tupleCompra.get(23, String.class));
            compraDTO.setReserva(tupleCompra.get(24, Boolean.class));
            compraDTO.setTaquilla(tupleCompra.get(25, Boolean.class));
            compraDTO.setTelefono(tupleCompra.get(26, String.class));
            compraDTO.setUuid(tupleCompra.get(27, String.class));
            compraDTO.setTipoPago(tupleCompra.get(28, String.class));
            compraDTO.setDireccion(tupleCompra.get(29, String.class));
            compraDTO.setPoblacion(tupleCompra.get(30, String.class));
            compraDTO.setCp(tupleCompra.get(31, String.class));
            compraDTO.setProvincia(tupleCompra.get(32, String.class));
            compraDTO.setInfoPeriodica(tupleCompra.get(33, Boolean.class));

            result.add(new Compra(compraDTO));
        }

        return result;
    }


    public List<Compra> getComprasBySesionFechaSegundos(
        long sesionId,
        int showAnuladas,
        int showOnline,
        String search,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter
    ) {
        List<Compra> compras =
            getComprasBySesion(sesionId, showAnuladas, showOnline, search, sortParameter, start, limit, filter);

        for (Compra compra : compras) {
            compra.setFecha(new Date(compra.getFecha().getTime() / 1000));
            if (compra.getDesde() != null)
                compra.setDesde(new Date(compra.getDesde().getTime() / 1000));

            if (compra.getHasta() != null)
                compra.setHasta(new Date(compra.getHasta().getTime() / 1000));
        }

        return compras;
    }

    public int getTotalComprasBySesion(
        Long sesionId,
        int showAnuladas,
        int showOnline,
        String search,
        ExtGridFilterList filter
    ) {
        return comprasDAO.getTotalComprasBySesion(sesionId, showAnuladas, showOnline, 0, search, filter);
    }

    @Transactional
    public void anularCompraReserva(
        Long idCompraReserva,
        String userUID
    ) throws IncidenciaNotFoundException {
        Usuario userById = usuariosDAO.getUserById(userUID);
        CompraDTO compraDTO = getCompraById(idCompraReserva);
        if (compraDTO.isReserva() || userById.getPuedeAnular()) {
            comprasDAO.anularCompraReserva(idCompraReserva, userById, true);
        } else {
            throw new UsuarioNoPuedeAnularException();
        }
    }

    @Transactional
    public void anularCompraDesdeMail(
        String uuidCompra,
        String email
    ) throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        anularDesdeMail(uuidCompra, null, email);
    }

    @Transactional
    public void anularButacaDesdeMail(
        String uuidCompra,
        long butacaId,
        String email
    ) throws EntradaConCosteNoAnulableDesdeEnlace, EntradasNoAnulablesDesdeEnlace, EmailCompraNoCoincideException,
        CompraConCosteNoAnulableDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        anularDesdeMail(uuidCompra, butacaId, email);
    }

    private void anularDesdeMail(
        String uuidCompra,
        Long butacaId,
        String email
    ) throws EntradaConCosteNoAnulableDesdeEnlace, EmailCompraNoCoincideException, CompraConCosteNoAnulableDesdeEnlace,
        EntradasNoAnulablesDesdeEnlace, EntradaNoExistente, TiempoMinioRequeridoParaAnularDesdeEnlace {
        CompraDTO compra = getCompraByUuid(uuidCompra);
        if (compra != null) {
            CineDTO cine = compra.getParSesion().getParSala().getParCine();
            if (cine.isAnulableDesdeEnlace()) {
                int tiempoRestanteCancelableDesdeEnlace = cine.getTiempoRestanteAnulableDesdeEnlace();
                long minutosParaLaCelebracion =
                    (compra.getParSesion().getFechaCelebracion().getTime() - new Date().getTime()) / 1000 / 60;
                if (minutosParaLaCelebracion > tiempoRestanteCancelableDesdeEnlace) {
                    if (compra.getEmail().equals(email)) {
                        if (butacaId != null) {
                            List<ButacaDTO> butacas = compra.getParButacas().stream().filter(b -> b.getId() == butacaId)
                                .collect(Collectors.toList());
                            if (butacas != null && butacas.size() > 0) {
                                if (butacas.stream().map(ButacaDTO::getPrecio).reduce(BigDecimal.ZERO, BigDecimal::add)
                                    .compareTo(BigDecimal.ZERO) == 0) {
                                    butacasDAO.anularButaca(butacaId, null);
                                } else {
                                    throw new EntradaConCosteNoAnulableDesdeEnlace(uuidCompra, butacaId);
                                }
                            } else {
                                throw new EntradaNoExistente(uuidCompra, butacaId);
                            }
                        } else {
                            if (compra.getParButacas().stream().map(ButacaDTO::getPrecio)
                                .reduce(BigDecimal.ZERO, BigDecimal::add).compareTo(BigDecimal.ZERO) == 0) {
                                anularCompra(compra.getId());
                            } else {
                                throw new CompraConCosteNoAnulableDesdeEnlace(uuidCompra);
                            }
                        }
                    } else {
                        throw new EmailCompraNoCoincideException(uuidCompra, compra.getEmail(), email);
                    }
                } else {
                    throw new TiempoMinioRequeridoParaAnularDesdeEnlace(cine.getId(), cine.getCodigo(),
                        tiempoRestanteCancelableDesdeEnlace, minutosParaLaCelebracion);
                }
            } else {
                throw new EntradasNoAnulablesDesdeEnlace(cine.getId(), cine.getCodigo());
            }
        } else {
            throw new CompraNoExistente();
        }
    }

    @Transactional
    public void anularCompra(
        Long idCompra
    ) {
        comprasDAO.anularCompraReserva(idCompra, null, false);
    }

    @Transactional
    public void desanularCompraReserva(
        Long idCompraReserva,
        String userUID
    ) throws ButacaOcupadaAlActivarException {
        comprasDAO.desanularCompraReserva(idCompraReserva);
        comprasDAO.getEntityManager().flush();
        comprasDAO.getEntityManager().clear();

        CompraDTO compraDTO = getCompraById(idCompraReserva);

        long sesionId = compraDTO.getParSesion().getId();
        List<Butaca> butacasSeleccionadas =
            compraDTO.getParButacas().stream().map(Butaca::new).collect(Collectors.toList());
        BigDecimal importeConComision =
            calculaImporteButacas(sesionId, butacasSeleccionadas, !compraDTO.getTaquilla(), userUID);
        BigDecimal importeSinComision = calculaImporteButacas(sesionId, butacasSeleccionadas, false, userUID);

        compraDTO.setImporte(importeConComision);
        compraDTO.setImporteSinComision(importeSinComision);

        comprasDAO.getEntityManager().persist(compraDTO);
        if (!compraDTO.isReserva()) {
            long totalAnuladas = sesionesDAO.getButacasAnuladasTotal(sesionId);
            if (totalAnuladas == 0L) {
                sesionesDAO.removeAnulacionVentasFromSesion(sesionId, userUID);
            }
        }
    }

    public void anulaReservasCaducadas() throws IncidenciaNotFoundException {
        List<CompraDTO> aCaducar = comprasDAO.getReservasACaducar(new Date());

        for (CompraDTO compraDTO : aCaducar)
            comprasDAO.anularCompraReserva(compraDTO.getId(), null, false);
    }

    @Transactional
    public void anularButacas(
        List<Long> idsButacas,
        String userUID
    ) throws IncidenciaNotFoundException {
        if (idsButacas != null && idsButacas.size() > 0) {
            CompraDTO compraDTO = comprasDAO.getCompraByButacaId(idsButacas.get(0));
            Usuario userById = usuariosDAO.getUserById(userUID);
            if (compraDTO.isReserva() || userById.getPuedeAnular()) {
                for (Long idButaca : idsButacas) {
                    butacasDAO.anularButaca(idButaca, userById);
                }

                long sesionId = compraDTO.getParSesion().getId();
                List<Butaca> butacasSeleccionadas =
                    compraDTO.getParButacas().stream().filter(b -> !b.getAnulada() && !idsButacas.contains(b.getId()))
                        .map(Butaca::new).collect(Collectors.toList());
                BigDecimal importeConComision =
                    calculaImporteButacas(sesionId, butacasSeleccionadas, !compraDTO.getTaquilla(), userUID);
                BigDecimal importeSinComision = calculaImporteButacas(sesionId, butacasSeleccionadas, false, userUID);

                compraDTO.setImporteSinComision(importeSinComision);
                compraDTO.setImporte(importeConComision);

                comprasDAO.getEntityManager().persist(compraDTO);

                List<Long> comprasConTodasButacasAnuladas = butacasDAO.getComprasConTodasButacasAnuladas(idsButacas);
                for (Long comprasConTodasButacasAnulada : comprasConTodasButacasAnuladas) {
                    anularCompraReserva(comprasConTodasButacasAnulada, userUID);
                }
            } else {
                throw new UsuarioNoPuedeAnularException();
            }
        }
    }

    public void rellenaCodigoPagoPasarela(
        long idCompra,
        String recibo
    ) {
        comprasDAO.rellenaCodigoPagoPasarela(idCompra, recibo);
    }

    public void passarACompra(
        Long sesionId,
        Long idCompraReserva,
        String recibo,
        String tipoPago,
        Date fechaCompra,
        String email,
        String nombre,
        String apellidos,
        String userUID
    ) {
        comprasDAO
            .passarACompra(sesionId, idCompraReserva, recibo, tipoPago, fechaCompra, email, nombre, apellidos, userUID);
        if (configuration.isIdEntrada()) {
            butacasDAO.asignarIdEntrada(idCompraReserva);
        }
    }

    public void actualizarFormaPago(
        Long sesionId,
        Long idCompraReserva,
        String recibo,
        String tipoPago,
        Date fechaCompra,
        String email,
        String nombre,
        String apellidos,
        String userUID
    ) {
        comprasDAO
            .actualizarFormaPago(sesionId, idCompraReserva, recibo, tipoPago, fechaCompra, email, nombre, apellidos,
                userUID);
    }

    @Transactional
    public void passarButacasACompra(
        Long sesionId,
        Long idCompraReserva,
        String recibo,
        String tipoPago,
        Date fechaCompra,
        String email,
        String nombre,
        String apellidos,
        List<Long> idsButacas,
        String language,
        String userUID
    ) {
        CompraDTO compra = comprasDAO.getCompraById(idCompraReserva);

        List<ButacaDTO> butacasReserva = new ArrayList<>();
        List<ButacaDTO> butacasCompra = new ArrayList<>();
        for (ButacaDTO butacaDTO : compra.getParButacas()) {
            if (idsButacas.contains(butacaDTO.getId())) {
                butacasReserva.add(butacaDTO);
            } else {
                butacasCompra.add(butacaDTO);
            }
        }

        if (butacasReserva.size() > 0) {
            if (butacasCompra.size() > 0) {
                anularButacas(idsButacas, userUID);
                List<Butaca> butacas =
                    Butaca.butacasDTOToButacas(butacasReserva, configuration.isIdEntrada(), language);
                for (Butaca butaca : butacas) {
                    butaca.setId(0);
                }
                ResultadoCompra resultadoCompra = registraCompraTaquilla(sesionId, butacas, null, fechaCompra, userUID);
                marcarPagadaConReferenciaDePago(resultadoCompra.getId(), tipoPago, recibo, email, nombre, apellidos,
                    language);
            } else {
                passarACompra(sesionId, idCompraReserva, recibo, tipoPago, fechaCompra, email, nombre, apellidos,
                    userUID);
            }
        }
    }

    @Transactional
    public boolean existeCompraButaca(
        String uuidCompra,
        Long idButaca
    ) {
        return comprasDAO.existeCompraButaca(uuidCompra, idButaca);
    }

    public void exportCSV(
        ByteArrayOutputStream bos,
        Long sesionId,
        String userUID
    ) throws IOException {
        List<String> csvLines = comprasDAO.getDataCSVFromSesion(sesionId, userUID);
        for (String csvLine : csvLines) {
            bos.write(String.format("%s%s", csvLine, System.getProperty("line.separator")).getBytes());
        }
        bos.close();
    }

    public void exportCSVEntradas(
        ByteArrayOutputStream bos,
        Long sesionId,
        String userUID
    ) throws IOException {
        List<Tuple> csvLines = comprasDAO.getDataCSVEntradasFromSesion(sesionId, userUID);
        for (Tuple csvLine : csvLines) {
            Date date = csvLine.get(6, Date.class);
            bos.write(String.format("\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"%s",
                csvLine.get(0, String.class), csvLine.get(7, String.class), csvLine.get(1, String.class),
                csvLine.get(2, String.class), csvLine.get(3, String.class), csvLine.get(4, String.class),
                csvLine.get(5, String.class), csvLine.get(9, String.class),
                date != null ? DateUtils.dateToSpanishStringWithHour(date) : "", System.getProperty("line.separator"))
                .getBytes());
        }
        bos.close();
    }

    public void exportCSVDetail(
        ByteArrayOutputStream bos,
        Long sesionId,
        String userUID
    ) throws IOException {
        List<Tuple> csvLines = comprasDAO.getDataCSVDetailFromSesion(sesionId, userUID);
        for (Tuple csvLine : csvLines) {
            bos.write(String.format("\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"%s", csvLine.get(0, String.class),
                csvLine.get(1, String.class), csvLine.get(2, String.class), csvLine.get(3, String.class),
                csvLine.get(4, Integer.class), System.getProperty("line.separator")).getBytes());
        }
        bos.close();
    }

    public static boolean isTipoPagoTarjeta(String tipoPago) {
        return tipoPago != null && (
            tipoPago.trim().toLowerCase().equals(TipoPago.TARJETAOFFLINE.toString().toLowerCase()) || tipoPago.trim()
                .toLowerCase().equals(TipoPago.TARJETA.toString().toLowerCase()));
    }

    public static boolean isTipoPagoTaquilla(String tipoPago) {
        return tipoPago == null || !tipoPago.trim().toLowerCase().equals(TipoPago.TARJETA.toString().toLowerCase());
    }

    public List<ComoNosConociste> getMotivosComoNosConociste(long cineId) {
        List<ComoNosConocisteDTO> motivosComoNosConociste = comprasDAO.getMotivosComoNosConociste(cineId);
        return motivosComoNosConociste.stream().map(m -> new ComoNosConociste(m.getId(), m.getMotivo()))
            .collect(Collectors.toList());
    }
}