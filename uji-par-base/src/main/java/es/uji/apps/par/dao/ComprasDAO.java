package es.uji.apps.par.dao;

import com.google.common.base.Strings;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.Expression;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.expr.BooleanExpression;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import es.uji.apps.par.auth.Role;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.database.DatabaseHelper;
import es.uji.apps.par.database.DatabaseHelperFactory;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.ComoNosConocisteDTO;
import es.uji.apps.par.db.CompraBorradaDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.QButacaCSVDTO;
import es.uji.apps.par.db.QButacaDTO;
import es.uji.apps.par.db.QButacaSinEagerDTO;
import es.uji.apps.par.db.QCineDTO;
import es.uji.apps.par.db.QComoNosConocisteDTO;
import es.uji.apps.par.db.QCompraBorradaDTO;
import es.uji.apps.par.db.QCompraDTO;
import es.uji.apps.par.db.QCompraSinEagerDTO;
import es.uji.apps.par.db.QEventoDTO;
import es.uji.apps.par.db.QReportDTO;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSesionDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.enums.TipoPago;
import es.uji.apps.par.exceptions.ButacaOcupadaAlActivarException;
import es.uji.apps.par.exceptions.IncidenciaNotFoundException;
import es.uji.apps.par.ext.ExtGridFilter;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.ficheros.registros.TipoIncidencia;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.report.InformeModelReport;
import es.uji.apps.par.services.ComprasService;
import es.uji.apps.par.utils.DateUtils;

@Repository
public class ComprasDAO extends BaseDAO {
    public static Logger log = Logger.getLogger(ComprasDAO.class);
    public static final String NO_FINALIZADAS = "cnf";

    private QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
    private QButacaCSVDTO qButacaCSVDTO = QButacaCSVDTO.butacaCSVDTO;
    private QComoNosConocisteDTO qComoNosConocisteDTO = QComoNosConocisteDTO.comoNosConocisteDTO;

    @Autowired
    private SesionesDAO sesionDAO;

    @Autowired
    private ButacasDAO butacasDAO;

    @Autowired
    private UsuariosDAO usuariosDAO;

    private DatabaseHelper dbHelper;

    @Autowired
    public ComprasDAO(Configuration configuration) {
        dbHelper = DatabaseHelperFactory.newInstance(configuration);
    }

    @Transactional
    public CompraDTO insertaCompra(
        Long sesionId,
        Date fecha,
        boolean taquilla,
        BigDecimal importe,
        BigDecimal importeSinComision,
        BigDecimal comision,
        String observaciones,
        String userUID
    ) {
        SesionDTO sesion = sesionDAO.getSesion(sesionId, userUID);
        CompraDTO compraDTO =
            new CompraDTO(sesion, new Timestamp(fecha.getTime()), taquilla, importe, importeSinComision, comision,
                sesion.getParEvento().getPorcentajeIva(), UUID.randomUUID().toString());
        if (taquilla) {
            Usuario usuarioTaquilla = usuariosDAO.getUserById(userUID);
            compraDTO.setUsuario(new UsuarioDTO(usuarioTaquilla.getId()));

            if (observaciones != null) {
                compraDTO.setObservacionesReserva(observaciones);
            }
        }
        entityManager.persist(compraDTO);

        return compraDTO;
    }

    @Transactional
    public CompraDTO insertaCompra(
        Long sesionId,
        Date fecha,
        boolean taquilla,
        BigDecimal importe,
        BigDecimal importeSinComision,
        BigDecimal comision,
        String email,
        String nombre,
        String apellidos,
        String userUID
    ) {
        SesionDTO sesion = sesionDAO.getSesion(sesionId, userUID);
        CompraDTO compraDTO =
            new CompraDTO(sesion, new Timestamp(fecha.getTime()), taquilla, importe, importeSinComision, comision,
                sesion.getParEvento().getPorcentajeIva(), UUID.randomUUID().toString(), email, nombre, apellidos);
        if (taquilla) {
            Usuario usuarioTaquilla = usuariosDAO.getUserById(userUID);
            compraDTO.setUsuario(Usuario.toDTO(usuarioTaquilla));
        }
        entityManager.persist(compraDTO);

        return compraDTO;
    }

    @Transactional
    public void passarACompra(
        Long sesionId,
        Long idCompraReserva,
        String recibo,
        String tipoPago,
        Date fechaCompra,
        String email,
        String nombre,
        String apellidos,
        String userUID
    ) {
        SesionDTO sesion = sesionDAO.getSesion(sesionId, userUID);
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        JPAUpdateClause updateC = new JPAUpdateClause(entityManager, qCompraDTO);
        updateC.set(qCompraDTO.reserva, false).set(qCompraDTO.pagada, true)
            .set(qCompraDTO.porcentajeIva, sesion.getParEvento().getPorcentajeIva()).setNull(qCompraDTO.desde)
            .setNull(qCompraDTO.hasta).set(qCompraDTO.caducada, false).set(qCompraDTO.anulada, false)
            .set(qCompraDTO.referenciaPago, recibo).set(qCompraDTO.email, email).set(qCompraDTO.nombre, nombre)
            .set(qCompraDTO.apellidos, apellidos).set(qCompraDTO.taquilla, ComprasService.isTipoPagoTaquilla(tipoPago))
            .set(qCompraDTO.fecha, new Timestamp(fechaCompra != null ? fechaCompra.getTime() : new Date().getTime()))
            .set(qCompraDTO.tipoPago, tipoPago).where(qCompraDTO.id.eq(idCompraReserva)
            .and(qCompraDTO.parSesion.id.eq(sesionId).and(qCompraDTO.reserva.eq(true)))).execute();
    }

    @Transactional
    public void actualizarFormaPago(
        Long sesionId,
        Long idCompra,
        String recibo,
        String tipoPago,
        Date fechaCompra,
        String email,
        String nombre,
        String apellidos,
        String userUID
    ) {
        SesionDTO sesion = sesionDAO.getSesion(sesionId, userUID);
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        JPAUpdateClause updateC = new JPAUpdateClause(entityManager, qCompraDTO);
        updateC.set(qCompraDTO.porcentajeIva, sesion.getParEvento().getPorcentajeIva()).setNull(qCompraDTO.desde)
            .set(qCompraDTO.referenciaPago, recibo).set(qCompraDTO.email, email).set(qCompraDTO.nombre, nombre)
            .set(qCompraDTO.apellidos, apellidos).set(qCompraDTO.taquilla, ComprasService.isTipoPagoTaquilla(tipoPago))
            .set(qCompraDTO.fecha, new Timestamp(fechaCompra != null ? fechaCompra.getTime() : new Date().getTime()))
            .set(qCompraDTO.tipoPago, tipoPago).where(
            qCompraDTO.id.eq(idCompra).and(qCompraDTO.parSesion.id.eq(sesionId).and(qCompraDTO.taquilla.eq(true))))
            .execute();
    }

    @Transactional
    public CompraDTO reserva(
        Long sesionId,
        Date fecha,
        Date desde,
        Date hasta,
        String observaciones,
        String userUID
    ) {
        SesionDTO sesion = sesionDAO.getSesion(sesionId, userUID);
        Usuario usuarioRegistraCompra = usuariosDAO.getUserById(userUID);

        CompraDTO compraDTO =
            new CompraDTO(sesion, new Timestamp(fecha.getTime()), true, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.ZERO, BigDecimal.ZERO, UUID.randomUUID().toString());

        compraDTO.setReserva(true);
        compraDTO.setDesde(DateUtils.dateToTimestampSafe(desde));
        compraDTO.setHasta(DateUtils.dateToTimestampSafe(hasta));
        compraDTO.setObservacionesReserva(observaciones);
        compraDTO.setUsuario(new UsuarioDTO(usuarioRegistraCompra.getId()));

        entityManager.persist(compraDTO);

        return compraDTO;
    }

    @Transactional
    public List<Tuple> getComprasYPresentadas(long sesionId) {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qCompraDTO).leftJoin(qCompraDTO.parButacas, qButacaDTO).where(
            qCompraDTO.parSesion.id.eq(sesionId).and(qCompraDTO.email.isNotNull()).and(qCompraDTO.anulada.isFalse()))
            .groupBy(qCompraDTO.email).list(qCompraDTO.email, qCompraDTO.count(), qButacaDTO.presentada.count());
    }

    @Transactional
    public CompraDTO getCompraById(long id) {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<CompraDTO> compras = query.from(qCompraDTO).where(qCompraDTO.id.eq(id)).distinct().list(qCompraDTO);

        if (compras.size() == 0)
            return null;
        else
            return compras.get(0);
    }


    public CompraDTO getCompraByButacaId(Long butacaId) {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<CompraDTO> compras =
            query.from(qCompraDTO).leftJoin(qCompraDTO.parButacas, qButacaDTO).where(qButacaDTO.id.eq(butacaId))
                .distinct().list(qCompraDTO);

        if (compras.size() == 0)
            return null;
        else
            return compras.get(0);
    }

    @Transactional
    public CompraBorradaDTO getCompraBorradaById(long id) {
        QCompraBorradaDTO qCompraBorradaDTO = QCompraBorradaDTO.compraBorradaDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<CompraBorradaDTO> compras =
            query.from(qCompraBorradaDTO).where(qCompraBorradaDTO.compraId.eq(id)).distinct().list(qCompraBorradaDTO);

        if (compras.size() == 0)
            return null;
        else
            return compras.get(0);
    }

    @Transactional
    public void guardarCodigoPagoTarjeta(
        long idCompra,
        String codigo
    ) {
        CompraDTO compra = getCompraById(idCompra);

        compra.setCodigoPagoTarjeta(codigo);

        entityManager.persist(compra);
    }

    @Transactional
    public void marcarPagada(
        Long idCompra,
        TipoPago tipoPago
    ) {
        CompraDTO compra = getCompraById(idCompra);

        compra.setPagada(true);
        compra.setTipoPago(tipoPago.toString());

        entityManager.persist(compra);
    }

    @Transactional
    public CompraDTO marcarPagadaConReferenciaDePago(
        Long idCompra,
        String tipoPago,
        String referenciaPago,
        String email,
        String nombre,
        String apellidos
    ) {
        CompraDTO compra = getCompraById(idCompra);

        compra.setPagada(true);
        compra.setTipoPago(tipoPago);
        compra.setReferenciaPago(referenciaPago);
        compra.setEmail(email);

        if (!ComprasService.isTipoPagoTaquilla(tipoPago)) {
            compra.setTaquilla(false);
            compra.setNombre(nombre);
            compra.setApellidos(apellidos);
        }

        entityManager.persist(compra);
        return compra;
    }

    @Transactional
    public void cambiarFecha(
        Long idCompra,
        Date fecha
    ) {
        CompraDTO compra = getCompraById(idCompra);

        compra.setFecha(new Timestamp(fecha.getTime()));

        entityManager.persist(compra);
    }

    @Transactional
    public void marcarPagadaConRecibo(
        Long idCompra,
        String reciboPinpad
    ) {
        CompraDTO compra = getCompraById(idCompra);

        compra.setPagada(true);
        compra.setReciboPinpad(reciboPinpad);
        compra.setTipoPago(TipoPago.PINPAD.toString());

        entityManager.persist(compra);
    }

    @Transactional
    public void marcarPagadaPasarela(
        Long idCompra,
        String codigoPago
    ) {
        CompraDTO compra = getCompraById(idCompra);

        compra.setPagada(true);
        compra.setCodigoPagoPasarela(codigoPago);
        compra.setTipoPago(TipoPago.TARJETA.toString());

        entityManager.persist(compra);
    }

    @Transactional
    public void borrarCompraNoPagada(Long idCompra) {
        CompraDTO compra = getCompraById(idCompra);

        if (compra != null && !compra.getPagada()) {
            CompraBorradaDTO compraBorrada = new CompraBorradaDTO(compra);
            entityManager.persist(compraBorrada);
            entityManager.remove(compra);
        }
    }

    @Transactional
    public void borrarCompraNoPagada(String uuidCompra) {
        CompraDTO compra = getCompraByUuid(uuidCompra);

        if (compra != null && !compra.getPagada()) {
            CompraBorradaDTO compraBorrada = new CompraBorradaDTO(compra);
            entityManager.persist(compraBorrada);
            entityManager.remove(compra);
        }
    }

    @Transactional
    public CompraDTO getCompraByUuid(String uuid) {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<CompraDTO> compras = query.from(qCompraDTO).where(qCompraDTO.uuid.eq(uuid)).distinct().list(qCompraDTO);

        if (compras.size() == 0)
            return null;
        else
            return compras.get(0);
    }

    @Transactional
    public void eliminaComprasPendientes() throws IncidenciaNotFoundException {
        final int ELIMINA_PENDIENTES_MINUTOS = 35;
        List<CompraDTO> comprasACaducar = getComprasPendientesDeConfirmarPago(ELIMINA_PENDIENTES_MINUTOS);

        for (CompraDTO compra : comprasACaducar) {
            compra.setCaducada(true);
            entityManager.persist(compra);

            anularCompraReserva(compra.getId(), null, false);
        }
    }

    @Transactional
    public List<CompraDTO> getComprasPendientesDeConfirmarPago(final int ELIMINA_PENDIENTES_MINUTOS) {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -ELIMINA_PENDIENTES_MINUTOS);

        Timestamp limite = new Timestamp(calendar.getTimeInMillis());

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qCompraDTO).where(
            qCompraDTO.pagada.eq(false).and(qCompraDTO.anulada.eq(false)).and(qCompraDTO.reserva.eq(false))
                .and(qCompraDTO.fecha.lt(limite))).distinct().list(qCompraDTO);
    }

    @Transactional
    public CompraDTO rellenaDatosComprador(
        String uuidCompra,
        String nombre,
        String apellidos,
        String direccion,
        String poblacion,
        String cp,
        String provincia,
        String telefono,
        String email,
        Integer comoNosConocisteId,
        Object infoPeriodica
    ) {
        log.info(String.format("Rellena datos comprador de compra con uuid %s y email %s", uuidCompra, email));
        CompraDTO compra = getCompraByUuid(uuidCompra);

        compra.setNombre(nombre);
        compra.setApellidos(apellidos);
        compra.setDireccion(direccion);
        compra.setPoblacion(poblacion);
        compra.setCp(cp);
        compra.setProvincia(provincia);
        compra.setTelefono(telefono);
        compra.setEmail(email);
        compra.setInfoPeriodica(infoPeriodica != null && infoPeriodica.equals("si"));
        compra.setFecha(new Timestamp(new Date().getTime()));
        if (comoNosConocisteId != null) {
            compra.setComoNosConociste(new ComoNosConocisteDTO(comoNosConocisteId));
        }

        entityManager.persist(compra);
        return compra;
    }

    @Transactional
    public List<Tuple> getComprasBySesion(
        long sesionId,
        int showAnuladas,
        int showOnline,
        String search,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter
    ) {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;

        QTuple selectGroupByExpression = getSelectGroupByExpression();

        return getQueryComprasBySesion(sesionId, showAnuladas, showOnline, 0, search, true, selectGroupByExpression,
            filter).orderBy(getSort(qCompraDTO, sortParameter)).offset(start).limit(limit)
            .list(selectGroupByExpression, qButacaDTO.precio.sum());
    }

    private QTuple getSelectGroupByExpression() {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

        return new QTuple(qCompraDTO.id, qCompraDTO.anulada, qCompraDTO.apellidos, qCompraDTO.caducada,
            qCompraDTO.codigoPagoPasarela, qCompraDTO.codigoPagoTarjeta, qCompraDTO.cp, qCompraDTO.desde,
            qCompraDTO.direccion, qCompraDTO.email, qCompraDTO.fecha, qCompraDTO.hasta, qCompraDTO.importe,
            qCompraDTO.importeSinComision, qCompraDTO.comision, qCompraDTO.infoPeriodica, qCompraDTO.nombre,
            qCompraDTO.observacionesReserva, qCompraDTO.pagada, qCompraDTO.parSesion.id, qCompraDTO.poblacion,
            qCompraDTO.provincia, qCompraDTO.reciboPinpad, qCompraDTO.referenciaPago, qCompraDTO.reserva,
            qCompraDTO.taquilla, qCompraDTO.telefono, qCompraDTO.uuid, qCompraDTO.tipoPago, qCompraDTO.direccion,
            qCompraDTO.poblacion, qCompraDTO.cp, qCompraDTO.provincia, qCompraDTO.infoPeriodica);
    }

    private JPAQuery getQueryComprasBySesion(
        long sesionId,
        int showAnuladas,
        int showOnline,
        int showPagadas,
        String search,
        boolean doJoinButacas,
        QTuple selectGroupByExpression,
        ExtGridFilterList filter
    ) {
        List<Long> sesionesIds = new ArrayList<>(Arrays.asList(sesionId));
        List<Long> idsSesionesWhereSesionIdInMultiplesSesiones =
            sesionDAO.getIdsSesionesWhereSesionIdInMultiplesSesiones(sesionId);
        sesionesIds.addAll(idsSesionesWhereSesionIdInMultiplesSesiones);

        JPAQuery query = new JPAQuery(entityManager);
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;

        BooleanBuilder builder = new BooleanBuilder();
        builder.and(qCompraDTO.parSesion.id.in(sesionesIds));

        if (showAnuladas == 0) {
            builder.and(qCompraDTO.anulada.isNull().or(qCompraDTO.anulada.eq(false)));
            if (doJoinButacas)
                builder.and(qButacaDTO.anulada.isNull().or(qButacaDTO.anulada.eq(false)));
        }

        if (showOnline == 0)
            builder.and(qCompraDTO.taquilla.eq(true));

        if (showPagadas == 1) {
            builder.and(qCompraDTO.pagada.eq(true).or(qCompraDTO.reserva.isTrue()));
        }

        if (!"".equals(search)) {
            String isNumeric = "^[\\d]+$";
            if (search.matches(isNumeric)) {
                long numericSearch = Integer.parseInt(search);
                builder.and(qCompraDTO.uuid.toUpperCase().like('%' + search.toUpperCase() + '%')
                    .or(qCompraDTO.observacionesReserva.toUpperCase().like('%' + search.toUpperCase() + '%')
                        .or(qCompraDTO.id.eq(numericSearch))));
            } else {
                search = search.toUpperCase();
                builder.and(qCompraDTO.uuid.toUpperCase().like('%' + search + '%')
                    .or(qCompraDTO.observacionesReserva.toUpperCase().like('%' + search + '%'))
                    .or(qCompraDTO.nombre.toUpperCase().like('%' + search + '%')
                        .or(qCompraDTO.apellidos.toUpperCase().like('%' + search + '%'))));
            }
        }

        builder.and(getFilterWhere(filter));

        if (doJoinButacas) {
            query.from(qCompraDTO).leftJoin(qCompraDTO.parButacas, qButacaDTO).where(builder);

            for (Expression<?> expression : selectGroupByExpression.getArgs()) {
                query.groupBy(expression);
            }
        } else
            query.from(qCompraDTO).where(builder);

        return query;
    }

    public BooleanBuilder getFilterWhere(
        ExtGridFilterList filter
    ) {
        BooleanBuilder builder = new BooleanBuilder();
        if (filter != null) {
            final ExtGridFilter nombreFilter = filter.findFiltroByProperty("nombre");
            if (nombreFilter != null) {
                builder.and(
                    qCompraDTO.nombre.toUpperCase().like('%' + nombreFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter apellidosFilter = filter.findFiltroByProperty("apellidos");
            if (apellidosFilter != null) {
                builder.and(qCompraDTO.apellidos.toUpperCase()
                    .like('%' + apellidosFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter emailFilter = filter.findFiltroByProperty("email");
            if (emailFilter != null) {
                builder.and(
                    qCompraDTO.email.toUpperCase().like('%' + emailFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter telefonoFilter = filter.findFiltroByProperty("telefono");
            if (telefonoFilter != null) {
                builder.and(qCompraDTO.telefono.toUpperCase()
                    .like('%' + telefonoFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter direccionFilter = filter.findFiltroByProperty("direccion");
            if (direccionFilter != null) {
                builder.and(qCompraDTO.direccion.toUpperCase()
                    .like('%' + direccionFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter poblacionFilter = filter.findFiltroByProperty("poblacion");
            if (poblacionFilter != null) {
                builder.and(qCompraDTO.poblacion.toUpperCase()
                    .like('%' + poblacionFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter cpFilter = filter.findFiltroByProperty("cp");
            if (cpFilter != null) {
                builder.and(qCompraDTO.cp.toUpperCase().like('%' + cpFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter provinciaFilter = filter.findFiltroByProperty("provincia");
            if (provinciaFilter != null) {
                builder.and(qCompraDTO.provincia.toUpperCase()
                    .like('%' + provinciaFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter idDevolucionFilter = filter.findFiltroByProperty("idDevolucion");
            if (idDevolucionFilter != null) {
                builder.and(qCompraDTO.codigoPagoPasarela.toUpperCase()
                    .like('%' + idDevolucionFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter idFilter = filter.findFiltroByProperty("id");
            if (idFilter != null) {
                builder.and(qCompraDTO.id.like('%' + idFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter uuidFilter = filter.findFiltroByProperty("uuid");
            if (uuidFilter != null) {
                builder.and(
                    qCompraDTO.uuid.toUpperCase().like('%' + uuidFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter observacionesReservaFilter = filter.findFiltroByProperty("observacionesReserva");
            if (observacionesReservaFilter != null) {
                builder.and(qCompraDTO.observacionesReserva.toUpperCase()
                    .like('%' + observacionesReservaFilter.getValue().toString().toUpperCase() + '%'));
            }

            final ExtGridFilter fechaFilter = filter.findFiltroByProperty("fecha");
            if (fechaFilter != null) {
                String fechaSinHora = fechaFilter.getValue().toString().split("T")[0];
                String fechaInicial = String.format("%s 00:00:00", fechaSinHora);
                String fechaFinal = String.format("%s 23:59:59", fechaSinHora);
                builder.and(qCompraDTO.fecha.goe(Timestamp.valueOf(fechaInicial))
                    .and(qCompraDTO.fecha.loe(Timestamp.valueOf(fechaFinal))));
            }
        }
        return builder;
    }

    @Transactional
    public int getTotalComprasBySesion(
        Long sesionId,
        int showAnuladas,
        int showOnline,
        int showPagadas,
        String search,
        ExtGridFilterList filter
    ) {
        return (int) getQueryComprasBySesion(sesionId, showAnuladas, showOnline, showPagadas, search, false,
            getSelectGroupByExpression(), filter).count();
    }

    private SesionDTO getSesion(Long idCompra) {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qCompraDTO).join(qCompraDTO.parSesion, qSesionDTO).where(qCompraDTO.id.eq(idCompra))
            .uniqueResult(qSesionDTO);
    }

    @Transactional(rollbackForClassName = {"IncidenciaNotFoundException"})
    public void anularCompraReserva(
        Long idCompraReserva,
        Usuario usuario,
        boolean manual
    ) throws IncidenciaNotFoundException {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;

        JPAUpdateClause updateCompra = new JPAUpdateClause(entityManager, qCompraDTO);
        updateCompra.set(qCompraDTO.anulada, true).where(qCompraDTO.id.eq(idCompraReserva)).execute();

        JPAUpdateClause updateButacas = new JPAUpdateClause(entityManager, qButacaDTO);
        updateButacas = updateButacas.set(qButacaDTO.anulada, true).set(qButacaDTO.fechaAnulada, new Date());

        if (usuario != null) {
            updateButacas = updateButacas.set(qButacaDTO.usuarioAnula, Usuario.toDTO(usuario));
        }

        updateButacas.where(qButacaDTO.parCompra.id.eq(idCompraReserva)).execute();
        CompraDTO compra = getCompraById(idCompraReserva);

        if (manual && !compra.isReserva()) {
            SesionDTO sesionDTO = getSesion(idCompraReserva);
            sesionDAO.setIncidencia(sesionDTO.getId(),
                TipoIncidencia.addAnulacionVentasToIncidenciaActual(sesionDTO.getIncidenciaId()));
        }
    }

    @Transactional(rollbackForClassName = {"ButacaOcupadaAlActivarException"})
    public void desanularCompraReserva(Long idCompraReserva) throws ButacaOcupadaAlActivarException {
        for (ButacaDTO butaca : getCompraById(idCompraReserva).getParButacas()) {
            if ((butaca.getFila() == null && butaca.getNumero() == null && butacasDAO
                .noHayButacasLibres(idCompraReserva, butaca.getParSesion(), butaca.getParLocalizacion())) || (butaca.getFila() != null
                && butaca.getNumero() != null && butacaOcupada(butaca))) {
                String comprador = "";
                if (!Strings.isNullOrEmpty(butaca.getParCompra().getNombre()) && !Strings
                    .isNullOrEmpty(butaca.getParCompra().getApellidos())) {
                    comprador = butaca.getParCompra().getNombre() + " " + butaca.getParCompra().getApellidos();
                }

                throw new ButacaOcupadaAlActivarException(butaca.getParSesion().getId(), butaca.getParLocalizacion(),
                    butaca.getFila(), butaca.getNumero(), comprador, butaca.getParCompra().getTaquilla());
            }
        }

        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;
        JPAUpdateClause updateCompra = new JPAUpdateClause(entityManager, qCompraDTO);
        updateCompra.set(qCompraDTO.anulada, false).set(qCompraDTO.caducada, false)
            .where(qCompraDTO.id.eq(idCompraReserva)).execute();

        JPAUpdateClause updateButacas = new JPAUpdateClause(entityManager, qButacaDTO);
        updateButacas.set(qButacaDTO.anulada, false).setNull(qButacaDTO.fechaAnulada).setNull(qButacaDTO.usuarioAnula)
            .where(qButacaDTO.parCompra.id.eq(idCompraReserva)).execute();
    }

    private boolean butacaOcupada(ButacaDTO butaca) {
        QButacaDTO b = QButacaDTO.butacaDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(b).where(b.parSesion.id.eq(butaca.getParSesion().getId()).and(b.anulada.eq(false))
            .and(b.parLocalizacion.codigo.eq(butaca.getParLocalizacion().getCodigo()))
            .and(b.fila.isNotNull().and(b.fila.eq(butaca.getFila())))
            .and(b.numero.isNotNull().and(b.numero.eq(butaca.getNumero())))).exists();
    }

    @Transactional
    public List<CompraDTO> getReservasACaducar(Date date) {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<CompraDTO> compras = query.from(qCompraDTO).where(
            qCompraDTO.reserva.eq(true).and(qCompraDTO.anulada.eq(false))
                .and(qCompraDTO.hasta.lt(DateUtils.dateToTimestampSafe(date)))).distinct().list(qCompraDTO);

        return compras;
    }

    private String sqlConditionsToSkipAnuladasIReservas(
        String fechaInicio,
        String fechaFin
    ) throws ParseException {
        return sqlConditionsToSkipAnuladasIReservas(fechaInicio, fechaFin, false);
    }

    private String sqlConditionsToSkipAnuladasIReservas(
        String fechaInicio,
        String fechaFin,
        boolean anuladas
    ) throws ParseException {
        return sqlConditionsToSkipAnuladasIReservas(fechaInicio, fechaFin, anuladas, false);
    }

    private String sqlConditionsToSkipAnuladasIReservas(
        String fechaInicio,
        String fechaFin,
        boolean anuladas,
        boolean dateSession
    ) throws ParseException {
        String dateConditions = "";
        if (dateSession) {
            dateConditions += "and s.fecha_celebracion >= " + dbHelper.toDate() + "('" + DateUtils
                .dateToSpanishStringWithHourAndSeconds(DateUtils.databaseWithSecondsToDate(fechaInicio + " 00:00:00")) + "', '"
                + dbHelper.dateHourSecondsFormatter() + "') " + "and s.fecha_celebracion <= " + dbHelper.toDate() + "('"
                + DateUtils.dateToSpanishStringWithHourAndSeconds(DateUtils.databaseWithSecondsToDate(fechaFin + " 23:59:59"))
                + "', '" + dbHelper.dateHourSecondsFormatter() + "') ";
        } else {
            dateConditions += "and c.fecha >= " + dbHelper.toDate() + "('" + DateUtils
                .dateToSpanishStringWithHourAndSeconds(DateUtils.databaseWithSecondsToDate(fechaInicio + " 00:00:00")) + "', '"
                + dbHelper.dateHourSecondsFormatter() + "') " + "and c.fecha <= " + dbHelper.toDate() + "('" + DateUtils
                .dateToSpanishStringWithHourAndSeconds(DateUtils.databaseWithSecondsToDate(fechaFin + " 23:59:59")) + "', '"
                + dbHelper.dateHourSecondsFormatter() + "') ";
        }

        return dateConditions + "and c.pagada = " + dbHelper.trueString() + " " + "and c.reserva = " + dbHelper
            .falseString() + " " + "and (s.anulada is null or s.anulada = " + dbHelper.falseString() + ")" + " " + (
            anuladas ?
                "" :
                "and ((b.anulada = " + dbHelper.falseString() + " " + "and c.anulada = " + dbHelper.falseString()
                    + ") or (b.fecha_anulada is null or b.fecha_anulada >= " + dbHelper.toDate() + "('" + DateUtils
                    .dateToSpanishStringWithHourAndSeconds(DateUtils.databaseWithSecondsToDate(fechaFin + " 23:59:59")) + "', '"
                    + dbHelper.dateHourSecondsFormatter() + "'))) ");
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<Object[]> getComprasInFechas(
        String fechaInicio,
        String fechaFin,
        String userUID,
        boolean online,
        String tpvId
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);

        String sql =
            "select e.titulo_es, s.fecha_celebracion, b.tipo, l.codigo, count(b.id) as cantidad, sum(b.precio) as total, c.sesion_id, "
                + "l.nombre_es, f.nombre "
                + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, par_localizaciones l, par_tarifas f, "
                + "par_salas sala, par_salas_usuarios su, par_usuarios u "
                + "where b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id and l.id=b.localizacion_id "
                + (tpvId != null && !tpvId.equals("-1") ? " and e.tpv_id = " + tpvId + " " : " ")
                + "and sala.id = s.sala_id and u.usuario = '" + userUID
                + "' and sala.id = su.sala_id and su.usuario_id = u.id " + sqlConditionsToSkipAnuladasIReservas(
                fechaInicio, fechaFin) + (online ?
                " " :
                "and c.taquilla = " + dbHelper.trueString() + " and c.codigo_pago_tarjeta is null ") + "and f.id = "
                + dbHelper.toInteger("b.tipo") + " " + (!online && user.getRole().equals(Role.TAQUILLA) ?
                "and (c.par_usuario is NULL or c.par_usuario = " + user.getId() + ") " :
                " ")
                + "group by c.sesion_id, e.titulo_es, b.tipo, s.fecha_celebracion, l.codigo, l.nombre_es, f.nombre "
                + "order by e.titulo_es";

        return entityManager.createNativeQuery(sql).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<Object[]> getComprasAgrupadasPorEventoInFechas(
        String fechaInicio,
        String fechaFin,
        String userUID,
        String tpvId
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);

        String sql = "select e.id, e.titulo_es, b.tipo, count(b.id) as cantidad, sum(b.precio) as total, c.taquilla, "
            + "f.nombre " + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, par_tarifas f, "
            + "par_salas sala, par_salas_usuarios su, par_usuarios u "
            + "where b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id " + (tpvId != null && !tpvId.equals("-1") ?
            " and e.tpv_id = " + tpvId + " " :
            " ") + "and sala.id = s.sala_id and u.usuario = '" + userUID
            + "' and sala.id = su.sala_id and su.usuario_id = u.id " + sqlConditionsToSkipAnuladasIReservas(fechaInicio,
            fechaFin) + "and f.id = " + dbHelper.toInteger("b.tipo") + " " + (user.getRole().equals(Role.TAQUILLA) ?
            "and (c.par_usuario is NULL or c.par_usuario = " + user.getId() + ") " :
            " ") + "group by e.id, e.titulo_es, b.tipo, c.taquilla, f.nombre " + "order by e.titulo_es";

        return entityManager.createNativeQuery(sql).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<Object[]> getComprasPorEventoInFechas(
        String fechaInicio,
        String fechaFin,
        String userUID,
        String tpvId
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);

        String sql = "select e.id, e.titulo_es, b.tipo, count(b.id) as cantidad, sum(b.precio) as total, c.taquilla, "
            + "f.nombre, TO_CHAR(s.fecha_celebracion, 'YYYY-MM-DD HH24:MI') as sesion , TO_CHAR(c.fecha, 'YYYY-MM-DD') as compra, sum(CASE WHEN b.fecha_anulada >= s.fecha_celebracion THEN 1 ELSE 0 END) as cantidadanulada, c.tipo as tipoPago "
            + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, par_tarifas f, "
            + "par_salas sala, par_salas_usuarios su, par_usuarios u "
            + "where b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id " + (tpvId != null && !tpvId.equals("-1") ?
            " and e.tpv_id = " + tpvId + " " :
            " ") + "and sala.id = s.sala_id and u.usuario = '" + userUID
            + "' and sala.id = su.sala_id and su.usuario_id = u.id " + sqlConditionsToSkipAnuladasIReservas(fechaInicio,
            fechaFin) + "and f.id = " + dbHelper.toInteger("b.tipo") + " " + (user.getRole().equals(Role.TAQUILLA) ?
            "and (c.par_usuario is NULL or c.par_usuario = " + user.getId() + ") " :
            " ") + "group by 1, 2, 8, 9, 3, 7, 6, 11 having sum(CASE WHEN b.anulada = " + dbHelper.falseString()
            + " THEN 1 ELSE 0 END) + sum(CASE WHEN b.fecha_anulada >= s.fecha_celebracion THEN 1 ELSE 0 END) > 0  order by 9, 2, 8, 11";

        return entityManager.createNativeQuery(sql).getResultList();
    }

    public List<Object[]> getComprasMiTaquillaPorEventoInFechas(
        String fechaInicio,
        String fechaFin,
        boolean dateSession,
        String userUID,
        String tpvId
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);

        String sql =
            "select e.id, e.titulo_es, b.tipo, sum(CASE WHEN c.par_usuario = " + user.getId() + " and b.anulada = "
                + dbHelper.falseString() + " THEN 1 ELSE 0 END) as cantidad, sum(CASE WHEN c.par_usuario = " + user
                .getId() + " and b.anulada = " + dbHelper.falseString()
                + " THEN b.precio ELSE 0 END) as total, c.taquilla, "
                + "f.nombre, TO_CHAR(s.fecha_celebracion, 'YYYY-MM-DD HH24:MI') as sesion , TO_CHAR(c.fecha, 'YYYY-MM-DD') as compra, sum(CASE WHEN b.par_usuario_anula = "
                + user.getId()
                + " and b.fecha_anulada >= s.fecha_celebracion THEN 1 ELSE 0 END) as cantidadanulada, c.tipo as tipoPago "
                + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, par_tarifas f where c.taquilla = "
                + dbHelper.trueString() + (tpvId != null && !tpvId.equals("-1") ?
                " and e.tpv_id = " + tpvId + " " :
                " ") + " and b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id "
                + sqlConditionsToSkipAnuladasIReservas(fechaInicio, fechaFin, false, dateSession) + " and f.id = "
                + dbHelper.toInteger("b.tipo") + " and (c.par_usuario is NULL or c.par_usuario = " + user.getId()
                + " or  b.par_usuario_anula = " + user.getId()
                + ") group by 1, 2, 8, 9, 3, 7, 6, 11 having sum(CASE WHEN c.par_usuario = " + user.getId()
                + " and b.anulada = " + dbHelper.falseString()
                + " THEN 1 ELSE 0 END) + sum(CASE WHEN b.par_usuario_anula = " + user.getId()
                + " and b.fecha_anulada >= s.fecha_celebracion THEN 1 ELSE 0 END) > 0  order by 9, 2, 8, 11";

        return entityManager.createNativeQuery(sql).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<Object[]> getComprasEfectivo(
        String fechaInicio,
        String fechaFin,
        String userUID,
        String tpvId
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);

        String sql =
            "select e.titulo_es, s.fecha_celebracion, b.tipo, count(b.id) as cantidad, sum(b.precio) as total, "
                + "c.sesion_id, c.porcentaje_iva, " + "f.nombre "
                + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, par_tarifas f, "
                + "par_salas sala, par_salas_usuarios su, par_usuarios u "
                + "where b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id "
                + "and sala.id = s.sala_id and u.usuario = '" + userUID
                + "' and sala.id = su.sala_id and su.usuario_id = u.id " + sqlConditionsToSkipAnuladasIReservas(
                fechaInicio, fechaFin) + "and c.taquilla = " + dbHelper.trueString() + " " + getSQLCompraIsEfectivo()
                + "and f.id = " + dbHelper.toInteger("b.tipo") + " " + (user.getRole().equals(Role.TAQUILLA) ?
                "and (c.par_usuario is NULL or c.par_usuario = " + user.getId() + ") " :
                " ") + (tpvId != null && !tpvId.equals("-1") ? " and e.tpv_id = " + tpvId + " " : " ")
                + "group by c.sesion_id, e.titulo_es, b.tipo, s.fecha_celebracion, c.porcentaje_iva, f.nombre "
                + "order by e.titulo_es, s.fecha_celebracion, f.nombre";

        return entityManager.createNativeQuery(sql).getResultList();
    }

    private String getSQLCompraIsEfectivo() {
        return "and ((c.codigo_pago_tarjeta IS NULL or c.codigo_pago_tarjeta = '') "
            + "and (c.referencia_pago IS NULL or c.referencia_pago = '')) ";
    }

    private String getSQLCompraIsTPVTaquilla() {
        return getSQLCompraIsTPV(true);
    }

    private String getSQLCompraIsTPVOnline() {
        return getSQLCompraIsTPV(false);
    }

    private String getSQLCompraIsTPV(boolean taquilla) {
        String sql =
            "and ((c.codigo_pago_tarjeta is not null and " + dbHelper.isNotEmptyString("c.codigo_pago_tarjeta") + ") "
                + "or (c.codigo_pago_pasarela is not null and " + dbHelper.isNotEmptyString("c.codigo_pago_pasarela")
                + ") " + "or (c.referencia_pago is not null and " + dbHelper.isNotEmptyString("c.referencia_pago")
                + ")) " + "and c.taquilla = " + (taquilla ? dbHelper.trueString() : dbHelper.falseString()) + " ";
        return sql;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<Object[]> getComprasTpv(
        String fechaInicio,
        String fechaFin,
        String userUID,
        String tpvId
    ) throws ParseException {
        return getComprasTpvType(fechaInicio, fechaFin, userUID, false, tpvId);
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<Object[]> getComprasTpvOnline(
        String fechaInicio,
        String fechaFin,
        String userUID,
        String tpvId
    ) throws ParseException {
        return getComprasTpvType(fechaInicio, fechaFin, userUID, true, tpvId);
    }

    private List<Object[]> getComprasTpvType(
        String fechaInicio,
        String fechaFin,
        String userUID,
        boolean onlyOnline,
        String tpvId
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);

        String formato = "DD";
        String sql =
            "select e.titulo_es, s.fecha_celebracion, b.tipo, count(b.id) as cantidad, sum(b.precio) as total, c.sesion_id, "
                + "c.porcentaje_iva, " + dbHelper.trunc("c.fecha", formato) + ", f.nombre, sum(b.precio_sin_comision), COUNT(CASE WHEN b.presentada is not null THEN 1 END) AS presentadas "
                + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, par_tarifas f, "
                + "par_salas sala, par_salas_usuarios su, par_usuarios u "
                + "where b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id " + (tpvId != null && !tpvId.equals("-1") ?
                " and e.tpv_id = " + tpvId + " " :
                " ") + "and sala.id = s.sala_id and u.usuario = '" + userUID
                + "' and sala.id = su.sala_id and su.usuario_id = u.id " + sqlConditionsToSkipAnuladasIReservas(
                fechaInicio, fechaFin) + "and (c.caducada is null or c.caducada = " + dbHelper.falseString() + ") " + (
                onlyOnline ?
                    getSQLCompraIsTPVOnline() :
                    getSQLCompraIsTPVTaquilla()) + "and f.id = " + dbHelper.toInteger("b.tipo") + " " + (
                !onlyOnline && user.getRole().equals(Role.TAQUILLA) ?
                    "and (c.par_usuario is NULL or c.par_usuario = " + user.getId() + ") " :
                    " ") + "group by c.sesion_id, e.titulo_es, b.tipo, s.fecha_celebracion, c.porcentaje_iva, "
                + dbHelper.trunc("c.fecha", formato) + ", f.nombre " + "order by " + dbHelper.trunc("c.fecha", formato)
                + ", s.fecha_celebracion, f.nombre";

        return entityManager.createNativeQuery(sql).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<Object[]> getComprasEventos(
        String fechaInicio,
        String fechaFin,
        String userUID,
        String tpvId
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);

        String sql =
            "select e.titulo_es, s.fecha_celebracion, b.tipo, count(b.id) as cantidad, sum(b.precio) as total, "
                + "c.porcentaje_iva, " + dbHelper.caseString("b.tipo",
                new String[] {"'normal'", "1", "'descuento'", "2", "'aulaTeatro'", "3", "'invitacion'", "4"})
                + " as tipoOrden, e.id as eventoId, s.id as sesionId, f.nombre "
                + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, par_tarifas f, "
                + "par_salas sala, par_salas_usuarios su, par_usuarios u "
                + "where b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id " + (tpvId != null && !tpvId.equals("-1") ?
                " and e.tpv_id = " + tpvId + " " :
                " ") + "and sala.id = s.sala_id and u.usuario = '" + userUID
                + "' and sala.id = su.sala_id and su.usuario_id = u.id " + sqlConditionsToSkipAnuladasIReservas(
                fechaInicio, fechaFin) + "and f.id = " + dbHelper.toInteger("b.tipo") + " " + (user.getRole()
                .equals(Role.TAQUILLA) ? "and (c.par_usuario is NULL or c.par_usuario = " + user.getId() + ") " : " ")
                + "group by e.id, s.id, e.titulo_es, b.tipo, s.fecha_celebracion, c.porcentaje_iva, f.nombre "
                + "order by s.fecha_celebracion, tipoOrden";

        return entityManager.createNativeQuery(sql).getResultList();
    }

    @Transactional
    public Object[] getTotalTaquillaTpv(
        String fechaInicio,
        String fechaFin,
        String userUID
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);

        String sql = "select sum(b.precio), count(b.precio) "
            + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, "
            + "par_salas sala, par_salas_usuarios su, par_usuarios u "
            + "where b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id "
            + "and sala.id = s.sala_id and u.usuario = '" + userUID
            + "' and sala.id = su.sala_id and su.usuario_id = u.id " + sqlConditionsToSkipAnuladasIReservas(fechaInicio,
            fechaFin) + "and c.taquilla = " + dbHelper.trueString() + " "
            + "and ((c.codigo_pago_tarjeta is not null and " + dbHelper.isNotEmptyString("c.codigo_pago_tarjeta") + ") "
            + "or (c.codigo_pago_pasarela is not null and " + dbHelper.isNotEmptyString("c.codigo_pago_pasarela") + ") "
            + "or (c.referencia_pago is not null and " + dbHelper.isNotEmptyString("c.referencia_pago") + ")) " + (user
            .getRole().equals(Role.TAQUILLA) ?
            "and (c.par_usuario is NULL or c.par_usuario = " + user.getId() + ") " :
            " ");

        List<Object[]> result = entityManager.createNativeQuery(sql).getResultList();

        if (result.size() > 0)
            return result.get(0);
        else
            return null;
    }

    @Transactional
    public Object[] getTotalTaquillaEfectivo(
        String fechaInicio,
        String fechaFin,
        String userUID
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);

        String sql = "select sum(b.precio), count(b.precio) "
            + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, "
            + "par_salas sala, par_salas_usuarios su, par_usuarios u "
            + "where b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id "
            + "and sala.id = s.sala_id and u.usuario = '" + userUID
            + "' and sala.id = su.sala_id and su.usuario_id = u.id " + sqlConditionsToSkipAnuladasIReservas(fechaInicio,
            fechaFin) + "and c.taquilla = " + dbHelper.trueString() + " " + getSQLCompraIsEfectivo() + (user.getRole()
            .equals(Role.TAQUILLA) ? "and (c.par_usuario is NULL or c.par_usuario = " + user.getId() + ") " : " ");

        List<Object[]> result = entityManager.createNativeQuery(sql).getResultList();

        if (result.size() > 0)
            return result.get(0);
        else
            return null;
    }

    @Transactional
    public Object[] getTotalOnline(
        String fechaInicio,
        String fechaFin,
        String userUID
    ) throws ParseException {
        String sql = "select sum(b.precio), count(b.precio) "
            + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, "
            + "par_salas sala, par_salas_usuarios su, par_usuarios u "
            + "where b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id "
            + "and sala.id = s.sala_id and u.usuario = '" + userUID
            + "' and sala.id = su.sala_id and su.usuario_id = u.id " + sqlConditionsToSkipAnuladasIReservas(fechaInicio,
            fechaFin) + "and c.taquilla = " + dbHelper.falseString() + " ";

        List<Object[]> result = entityManager.createNativeQuery(sql).getResultList();

        if (result.size() > 0)
            return result.get(0);
        else
            return null;
    }

    /************************************ NUEVAS **********************************************************************************/
    @Transactional
    public Object[] getTotalNuevaTaquilla(
        String fechaInicio,
        String fechaFin,
        String userUID,
        TipoPago tipoPago,
        boolean dateSession,
        String tpvId
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);
        user.setRole(Role.TAQUILLA);

        return getTotalNueva(fechaInicio, fechaFin, tipoPago, true, dateSession, user, tpvId);
    }

    @Transactional
    public Object[] getTotalNueva(
        String fechaInicio,
        String fechaFin,
        String userUID,
        TipoPago tipoPago,
        boolean taquilla,
        boolean dateSession,
        String tpvId
    ) throws ParseException {
        Usuario user = usuariosDAO.getUserById(userUID);

        return getTotalNueva(fechaInicio, fechaFin, tipoPago, taquilla, dateSession, user, tpvId);
    }

    private Object[] getTotalNueva(
        String fechaInicio,
        String fechaFin,
        TipoPago tipoPago,
        boolean taquilla,
        boolean dateSession,
        Usuario user,
        String tpvId
    ) throws ParseException {
        String sql = "select sum(b.precio), count(b.precio) "
            + "from par_butacas b, par_compras c, par_sesiones s, par_eventos e, "
            + "par_salas sala, par_salas_usuarios su, par_usuarios u "
            + "where b.compra_id = c.id and s.id = c.sesion_id and e.id = s.evento_id " + (tpvId != null && !tpvId.equals("-1") ?
            " and e.tpv_id = " + tpvId + " " :
            " ") + "and sala.id = s.sala_id and u.usuario = '" + user.getUsuario()
            + "' and sala.id = su.sala_id and su.usuario_id = u.id " + sqlConditionsToSkipAnuladasIReservas(fechaInicio,
            fechaFin, false, dateSession) + "and c.taquilla = " + (taquilla ?
            dbHelper.trueString() :
            dbHelper.falseString()) + " " + "and c.tipo IS NOT NULL and lower(c.tipo) = '" + tipoPago.toString()
            .toLowerCase() + "' " + (taquilla && user.getRole().equals(Role.TAQUILLA) ?
            "and (c.par_usuario is NULL or c.par_usuario = " + user.getId() + ") " :
            " ");

        List<Object[]> result = entityManager.createNativeQuery(sql).getResultList();

        if (result.size() > 0)
            return result.get(0);
        else
            return null;
    }

    /******************************************************************************************************************************/

    @Transactional
    public void rellenaCodigoPagoPasarela(
        long idCompra,
        String codigoPago
    ) {
        CompraDTO compra = getCompraById(idCompra);
        compra.setCodigoPagoPasarela(codigoPago);

        entityManager.persist(compra);
    }

    @Transactional
    public BigDecimal getRecaudacionSesiones(
        List<Sesion> sesiones,
        boolean comision
    ) {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;

        List<Long> idsSesiones = Sesion.getIdsSesiones(sesiones);

        JPAQuery query = new JPAQuery(entityManager);

        BigDecimal total =
            query.from(qCompraDTO).join(qCompraDTO.parSesion, qSesionDTO).join(qCompraDTO.parButacas, qButacaDTO).where(
                qSesionDTO.id.in(idsSesiones).and(qCompraDTO.reserva.eq(false)).and(qCompraDTO.pagada.eq(true))
                    .and(qCompraDTO.anulada.eq(false)).and(qButacaDTO.anulada.eq(false))).distinct()
                .uniqueResult(comision ? qButacaDTO.precio.sum() : qButacaDTO.precioSinComision.sum());

        if (total == null)
            return BigDecimal.ZERO;
        else
            return total;
    }

    @Transactional
    public int getEspectadores(List<Sesion> sesiones) {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;

        List<Long> idsSesiones = Sesion.getIdsSesiones(sesiones);

        JPAQuery query = new JPAQuery(entityManager);

        Long butacas =
            query.from(qCompraDTO).join(qCompraDTO.parSesion, qSesionDTO).join(qCompraDTO.parButacas, qButacaDTO).where(
                qSesionDTO.id.in(idsSesiones).and(qCompraDTO.reserva.eq(false)).and(qCompraDTO.anulada.eq(false))
                    .and(qButacaDTO.anulada.eq(false))).uniqueResult(qButacaDTO.count());

        if (butacas == null)
            return 0;
        else
            return butacas.intValue();
    }

    @Transactional
    public List<CompraDTO> getComprasOfEvento(long eventoId) {
        QEventoDTO qEvento = QEventoDTO.eventoDTO;
        QSesionDTO qSesion = QSesionDTO.sesionDTO;
        QCompraDTO qCompra = QCompraDTO.compraDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qEvento, qSesion, qCompra).where(qEvento.id.eq(eventoId).and(
            qSesion.parEvento.id.eq(qEvento.id).and(qCompra.parSesion.id.eq(qSesion.id))
                .and(qCompra.anulada.isFalse()))).list(qCompra);
    }

    @Transactional
    public boolean hasComprasBySesion(long sesionId) {
        QCompraSinEagerDTO qCompraSinEagerDTO = QCompraSinEagerDTO.compraSinEagerDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qCompraSinEagerDTO)
            .where(qCompraSinEagerDTO.parSesion.id.eq(sesionId).and(qCompraSinEagerDTO.anulada.isFalse()).and(qCompraSinEagerDTO.caducada.isFalse()))
            .count() > 0;
    }

    @Transactional
    public long getEntradasEventoYEmail(
        long eventoId,
        String email
    ) {
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;
        QCompraDTO qCompra = QCompraDTO.compraDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qButacaDTO).join(qButacaDTO.parCompra, qCompra).join(qCompra.parSesion, qSesionDTO)
            .join(qSesionDTO.parEvento, qEventoDTO).where(
                qCompra.email.eq(email).and(qEventoDTO.id.eq(eventoId)).and(qCompra.anulada.isFalse())
                    .and(qCompra.caducada.isFalse()).and(qButacaDTO.anulada.isFalse())).count();
    }

    public InformeModelReport getResumenSesion(
        Long sesionId,
        String userUID
    ) {
        return getResumenSesion(sesionId, userUID, false);
    }

    public InformeModelReport getResumenSesion(
        Long sesionId,
        String userUID,
        boolean allCompras
    ) {
        QButacaDTO qButaca = QButacaDTO.butacaDTO;

        BooleanExpression whereVendidas = qButaca.parSesion.id.eq(sesionId).and(
            qButaca.anulada.eq(false).and(qButaca.parCompra.reserva.eq(false)).and(qButaca.parCompra.pagada.eq(true))
                .and(qButaca.parSesion.anulada.isNull().or(qButaca.parSesion.anulada.eq(false))));

        BooleanExpression whereCanceladasTaquilla = qButaca.parSesion.id.eq(sesionId)
            .and(qButaca.parSesion.anulada.isNull().or(qButaca.parSesion.anulada.eq(false))).and(
                qButaca.parCompra.reserva.eq(false).and(qButaca.parCompra.anulada.eq(true).or(qButaca.anulada.eq(true)))
                    .and(qButaca.parCompra.caducada.eq(false)));

        Usuario user = usuariosDAO.getUserById(userUID);
        if (!allCompras && user.getRole().equals(Role.TAQUILLA)) {
            BooleanExpression whereCompraIsFromUsuario =
                qButaca.parCompra.usuario.id.eq(user.getId()).or(qButaca.parCompra.usuario.isNull());
            whereVendidas = whereVendidas.and(whereCompraIsFromUsuario);
            whereCanceladasTaquilla = whereCanceladasTaquilla.and(whereCompraIsFromUsuario);
        }

        InformeModelReport r = new InformeModelReport();
        JPAQuery query = new JPAQuery(entityManager);
        Long vendidas = query.from(qButaca).where(whereVendidas).count();
        r.setNumeroEntradas(vendidas.intValue());

        query = new JPAQuery(entityManager);
        Long canceladasTaquilla = query.from(qButaca).where(whereCanceladasTaquilla).count();
        r.setCanceladasTaquilla(canceladasTaquilla.intValue());

        Sesion sesion = new Sesion(sesionId.intValue());
        r.setTotalSinComision(getRecaudacionSesiones(Arrays.asList(sesion), false));
        r.setTotal(getRecaudacionSesiones(Arrays.asList(sesion), true));

        return r;
    }

    @Transactional
    public String getReportClassByCompraUUID(
        String uuidCompra,
        String tipo
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QReportDTO qReportDTO = QReportDTO.reportDTO;
        QCineDTO qCineDTO = QCineDTO.cineDTO;

        return query.from(qCompraDTO).join(qCompraDTO.parSesion, qSesionDTO).join(qSesionDTO.parSala, qSalaDTO)
            .join(qSalaDTO.parCine, qCineDTO).join(qCineDTO.parReports, qReportDTO)
            .where(qCompraDTO.uuid.eq(uuidCompra).and(qReportDTO.tipo.toUpperCase().eq(tipo.toUpperCase())))
            .uniqueResult(qReportDTO.clase);
    }

    @Transactional
    public boolean existeCompraButaca(
        String uuidCompra,
        Long idButaca
    ) {
        QCompraDTO qCompraDTO = QCompraDTO.compraDTO;
        QButacaDTO qButacaDTO = QButacaDTO.butacaDTO;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qCompraDTO).join(qCompraDTO.parButacas, qButacaDTO)
            .where(qCompraDTO.uuid.like(uuidCompra).and(qButacaDTO.id.eq(idButaca)))
            .exists();
    }

    public List<String> getDataCSVFromSesion(
        Long sesionId,
        String userUID
    ) {
        SesionDTO sesion = sesionDAO.getSesion(sesionId, userUID);
        if (sesion != null) {
            JPAQuery query = new JPAQuery(entityManager);
            return query.from(qButacaCSVDTO).where(qButacaCSVDTO.sesionId.eq(sesionId)).list(qButacaCSVDTO.uuid);
        }
        else {
            return new ArrayList<>();
        }
    }

    public List<Tuple> getDataCSVEntradasFromSesion(
        Long sesionId,
        String userUID
    ) {
        SesionDTO sesion = sesionDAO.getSesion(sesionId, userUID);
        if (sesion != null) {
            JPAQuery query = new JPAQuery(entityManager);
            return query.from(qButacaCSVDTO).where(qButacaCSVDTO.sesionId.eq(sesionId))
                .list(qButacaCSVDTO.uuid, qButacaCSVDTO.fila, qButacaCSVDTO.numero, qButacaCSVDTO.nombre, qButacaCSVDTO.apellidos, qButacaCSVDTO.email, qButacaCSVDTO.presentada, qButacaCSVDTO.localizacionNombreEs, qButacaCSVDTO.localizacionNombreVa, qButacaCSVDTO.nombreNominal.coalesce(""));
        }
        else {
            return new ArrayList<>();
        }
    }

    public List<Tuple> getDataCSVDetailFromSesion(
        Long sesionId,
        String userUID
    ) {
        SesionDTO sesion = sesionDAO.getSesion(sesionId, userUID);
        if (sesion != null) {
            JPAQuery query = new JPAQuery(entityManager);
            return query.from(qButacaCSVDTO).where(qButacaCSVDTO.sesionId.eq(sesionId))
                .groupBy(qButacaCSVDTO.compraId, qButacaCSVDTO.nombre, qButacaCSVDTO.apellidos, qButacaCSVDTO.email)
                .list(qButacaCSVDTO.compraId, qButacaCSVDTO.nombre, qButacaCSVDTO.apellidos, qButacaCSVDTO.email,
                    qButacaCSVDTO.compraId.count());
        }
        else {
            return new ArrayList<>();
        }
    }

    public List<ComoNosConocisteDTO> getMotivosComoNosConociste(long cineId) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qComoNosConocisteDTO).where(qComoNosConocisteDTO.parCine.id.eq(cineId))
            .list(qComoNosConocisteDTO);
    }

    @Transactional
    public void actualizaButacasNombres(List<ButacaDTO> parButacas) {
        QButacaSinEagerDTO qButacaSinEagerDTO = QButacaSinEagerDTO.butacaSinEagerDTO;
        for (ButacaDTO parButaca : parButacas) {
            new JPAUpdateClause(entityManager, qButacaSinEagerDTO)
                .set(qButacaSinEagerDTO.nombreCompleto, parButaca.getNombreCompleto())
                .where(qButacaSinEagerDTO.id.eq(parButaca.getId())).execute();
        }
    }
}
