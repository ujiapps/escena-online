package es.uji.apps.par.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysema.query.Tuple;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.services.CodigoService;

@XmlRootElement
public class SalaHTML {
    public static Logger log = Logger.getLogger(SalaHTML.class);

    private String html;
    private String nombre;
    private String direccion;
    private Cine cine;
    private Document document;
    private Map<String, List<ButacaHTML>> map;

    public SalaHTML(
        String html,
        String nombre,
        String direccion,
        Cine cine
    ) {
        Assert.hasLength(html);
        Assert.hasLength(nombre);
        Assert.hasLength(direccion);
        Assert.notNull(cine);

        this.html = html;
        this.nombre = nombre;
        this.direccion = direccion;
        this.cine = cine;
        this.document = Jsoup.parse(html);
        this.map = getLocalizacionesFromHTML();
    }

    private Map<String, List<ButacaHTML>> getLocalizacionesFromHTML() {
        Map<String, List<ButacaHTML>> localizaciones = new HashMap<>();
        Elements elements = document.getElementsByClass("mapa");
        for (Element element : elements) {
            String[] idSplitted = element.id().split("-");
            String localizacionCodigo = idSplitted[0];
            String fila = idSplitted[1];
            String numero = idSplitted[2];
            List butacas = localizaciones.get(localizacionCodigo);
            if (butacas == null) {
                butacas = new ArrayList();
            }
            butacas.add(new ButacaHTML(localizacionCodigo, fila, numero));
            localizaciones.put(localizacionCodigo, butacas);
        }
        return localizaciones;
    }

    public List<Path> writeButacasJSON(Path butacasPath) throws IOException {
        List<Path> paths = new ArrayList<>();
        for (String localizacionCodigo : map.keySet()) {
            ObjectMapper jsonMapper = new ObjectMapper();
            java.nio.file.Path jsonPath = Paths.get(butacasPath.toString(), localizacionCodigo + ".json");
            Files.createFile(jsonPath);
            paths.add(Files.write(jsonPath, jsonMapper.writeValueAsString(map.get(localizacionCodigo)).getBytes()));
        }
        return paths;
    }

    public Path writeFragment(
        Path fragmentPath
    ) throws IOException {
        String nuevoFragment = String.format(""
            + "<div th:fragment=\"butacasFragment\" id=\"butacas\">\n"
            + "    <div th:replace=\"paranimf/butacasDecorator :: header\"></div>\n"
            + "    <div class=\"main\" style=\"margin-bottom: 30px;\">\n"
            + "        <div class=\"row\" id=\"butacasRoot\" style=\"min-width: 310px !important;\">\n"
            + "            <div id=\"divButacas\" class=\"twelve columns\" style=\"min-width: 0px !important; padding: 0px 0px 0px 0px !important;\">\n"
            + "                <input type=\"hidden\" name=\"idSesion\" th:value=\"${sesion.id}\" value=\"\" />\n"
            + "                <input type=\"hidden\" name=\"butacasSeleccionadas\" value=\"[]\" />\n"
            + "                <input type=\"hidden\" name=\"uuidCompra\" th:value=\"${uuidCompra}\" value=\"\" />\n"
            + "                <div>"
            + "                     %s"
            + "                     <div style=\"margin-left:auto;margin-right:auto; width:100%%;height: 60px; margin-bottom: 50px;\">\n"
            + "                         <div id=\"escenario\" class=\"entrada-plano localizacion_platea\" style=\"width: 100%% !important; height: 100%%; margin: 0px !important;\">\n"
            + "                             <div th:text=\"#{butacasFragment.escenario}\">ESCENARIO</div>\n"
            + "                         </div>"
            + "                     </div>"
            + "                </div>\n"
            + "            </div>\n"
            + "            <div th:replace=\"paranimf/butacasDecorator :: footer\"></div>\n"
            + "         </div>\n"
            + "     </div>\n"
            + "</div>\n", html);

        Files.createFile(fragmentPath);
        return Files.write(fragmentPath, nuevoFragment.getBytes());
    }

    public List<Localizacion> getLocalizaciones(
        String salaCodigo
    ) {
        List<Localizacion> result = new ArrayList<>();
        Elements textoLocalizaciones = document.getElementsByClass("localizacionTexto");
        for (Element textoLocalizacion : textoLocalizaciones) {
            String localizacionKey = textoLocalizacion.attr("text");
            long totalEntradas = map.get(localizacionKey).stream().count();

            Localizacion localizacion = new Localizacion();
            localizacion.setTotalEntradas(new Long(totalEntradas).intValue());
            localizacion.setNombreEs(textoLocalizacion.attr("text-es"));
            localizacion.setNombreVa(textoLocalizacion.attr("text-ca"));
            localizacion.setCodigo(localizacionKey);
            result.add(localizacion);
        }
        return result;
    }

    public Sala toSala(Tuple clasesEntrada) {
        Sala sala = new Sala();
        sala.setNombre(nombre);
        sala.setDireccion(direccion);
        sala.setCine(cine);
        sala.setAsientosNumerados(true);
        sala.setCodigo(CodigoService.getCodigoFromNombre(cine.getCodigo(), nombre, false));
        sala.setFormato("1");
        sala.setHtmlTemplateName(sala.getCodigo() + "Fragment");
        sala.setSubtitulo("1");
        sala.setTipo("1");
        if (clasesEntrada != null) {
            sala.setClaseEntradaTaquilla(clasesEntrada.get(0, String.class));
            sala.setClaseEntradaOnline(clasesEntrada.get(1, String.class));
        } else {
            sala.setClaseEntradaTaquilla("es.uji.apps.par.report.EntradaTaquillaComisionReport");
            sala.setClaseEntradaOnline("es.uji.apps.par.report.EntradaComisionReport");
        }

        return sala;
    }

    public long getCineId() {
        return cine.getId();
    }
}