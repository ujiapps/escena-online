package es.uji.apps.par.config;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import es.uji.apps.par.model.TipoInforme;

public class ConfigurationProperties implements ConfigurationSelector {
    private static final String URL_PUBLIC = "uji.par.urlPublic";
    private static final String URL_PUBLIC_LIMPIO = "uji.par.urlPublicLimpio";
    private static final String URL_ADMIN = "uji.par.urlAdmin";
    private static final String HTML_TITLE = "uji.par.htmltitle";
    private static final String COMO_LLEGAR = "uji.par.urlComoLlegar";
    private static final String URL_CONDICIONES_PRIVACIDAD = "uji.par.urlCondicionesPrivacidad";
    private static final String URL_CONDICIONES_CANCELACION = "uji.par.urlCondicionesCancelacion";
    private static final String MAIL_FROM = "uji.par.mail.from";
    private static final String URL_PIE_ENTRADA = "uji.par.urlPieEntrada";
    private static final String LOGO_REPORT = "uji.reports.logo";
    private static final String LOCATION_REPORT = "uji.reports.location";
    private static final String API_KEY = "api.key";
    private static final String LANGS_ALLOWED = "uji.par.langsAllowed";
    private static final String IDIOMA_POR_DEFECTO = "uji.par.defaultLang";
    private static final String CLASE_POSTVENTA = "uji.par.clasePostventa";
    private static final String TIPOS_INFORME = "uji.reports.tipos";
    private static final String TIPOS_INFORME_GENERALES = "uji.reports.tiposGenerales";
    private static final String TPV = "uji.tpv";
    private static final String MENU_CLIENTES = "uji.par.menuClientes";
    private static final String MENU_SALAS = "uji.par.menuSalas";
    private static final String MENU_INTEGRACIONES = "uji.par.menuIntegraciones";
    private static final String MENU_PERFIL = "uji.par.menuPerfil";
    private static final String MENU_VENTA_ANTICIPADA = "uji.par.menuVentaAnticipada";
    private static final String MENU_ICAA = "uji.par.menuICAA";
    private static final String SHOW_COMO_NOS_CONOCISTE = "uji.par.showComoNosConociste";
    private static final String MENU_ABONO = "uji.par.menuAbonos";
    private static final String HELP = "uji.par.help";
    private static final String COMPRA_TAQUILLA_DISABLED = "uji.par.compraTaquillaDisabled";
    private static final String CHECK_CONTRATACION_GENERAL_HABILIDADO = "uji.par.checkContratacionGeneral";
    private static final String PAY_MODES = "uji.par.paymodes";
    private static final String NO_NUMERADA_SECUENCIAL = "uji.par.noNumeradaSecuencial";

    private static final String SHOW_BUTACAS_QUE_HAN_ENTRADO_EN_DISTINTO_COLOR =
        "uji.par.showButacasQueHanEntradoEnDistintoColor";

    private Properties properties;

    @Autowired
    public ConfigurationProperties(ConfigurationInterface configurationInterface) throws IOException {
        properties = new Properties();
        properties.load(configurationInterface.getPathToFile());
    }

    private String getProperty(String propertyName) {
        return Configuration.getProperty(properties, propertyName);
    }

    private String getNoObligatoryProperty(String propertyName) {
        try {
            String property = getProperty(propertyName);
            return property;
        } catch (Exception e) {
            return null;
        }
    }

    public String getUrlPublic() {
        return getProperty(URL_PUBLIC);
    }

    public String getUrlPublicLimpio() {
        return getNoObligatoryProperty(URL_PUBLIC_LIMPIO);
    }

    public String getUrlAdmin() {
        return getProperty(URL_ADMIN);
    }

    public String getHtmlTitle() {
        return getProperty(HTML_TITLE);
    }

    public String getMailFrom() {
        return getProperty(MAIL_FROM);
    }

    public String getUrlComoLlegar() {
        return getProperty(COMO_LLEGAR);
    }

    public String getUrlCondicionesPrivacidad() {
        return getProperty(URL_CONDICIONES_PRIVACIDAD);
    }

    public String getUrlCondicionesCancelacion() {
        return getProperty(URL_CONDICIONES_CANCELACION);
    }

    public String getUrlPieEntrada() {
        return getProperty(URL_PIE_ENTRADA);
    }

    public String getLogoReport() {
        return getProperty(LOGO_REPORT);
    }

    @Override
    public String getNombreMunicipio() {
        return getNoObligatoryProperty(LOCATION_REPORT);
    }

    public String getApiKey() {
        return getProperty(API_KEY);
    }

    @Override
    public String getLangsAllowed() {
        String langsAllowed = getNoObligatoryProperty(LANGS_ALLOWED);

        if (langsAllowed != null && langsAllowed.length() > 0)
            return langsAllowed;
        return "[{'locale':'ca', 'alias': 'Valencià'}]";
    }

    @Override
    public boolean getLocalizacionEnValenciano() {
        String langsAllowed = getLangsAllowed();
        return (langsAllowed.toUpperCase().contains("VALENCI") || langsAllowed.toUpperCase().contains("CATAL"));
    }

    @Override
    public String getIdiomaPorDefecto() {
        String lang = getNoObligatoryProperty(IDIOMA_POR_DEFECTO);
        if (lang != null && lang.length() > 0) {
            return lang;
        } else {
            return "ca";
        }
    }

    @Override
    public boolean showButacasHanEntradoEnDistintoColor() {
        String showButacasQueHanEntradoEnDistintoColor =
            getNoObligatoryProperty(SHOW_BUTACAS_QUE_HAN_ENTRADO_EN_DISTINTO_COLOR);
        return (showButacasQueHanEntradoEnDistintoColor != null && showButacasQueHanEntradoEnDistintoColor.toLowerCase()
            .equals("true")) ? true : false;
    }

    @Override
    public boolean showIVA() {
        return true;
    }

    @Override
    public boolean showTotalesSinComision() {
        return false;
    }

    public String getClasePostventa() {
        return getProperty(CLASE_POSTVENTA);
    }

    public String getInformes() {
        return getProperty(TIPOS_INFORME);
    }

    public String getInformesGenerales() {
        String tiposInformeGenerales = getNoObligatoryProperty(TIPOS_INFORME_GENERALES);
        if (tiposInformeGenerales == null)
            tiposInformeGenerales = TipoInforme.getDefaultGenerales();
        return tiposInformeGenerales;
    }

    public boolean isMenuSalasEnabled() {
        String menuSalas = getNoObligatoryProperty(MENU_SALAS);
        if (menuSalas == null || !menuSalas.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public boolean isMenuIntegracionesEnabled() {
        String menuIntegraciones = getNoObligatoryProperty(MENU_INTEGRACIONES);
        if (menuIntegraciones == null || !menuIntegraciones.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public boolean isMenuPerfilEnabled() {
        String menuPerfil = getNoObligatoryProperty(MENU_PERFIL);
        if (menuPerfil == null || !menuPerfil.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public boolean hasHelp() {
        String help = getNoObligatoryProperty(HELP);
        if (help == null || !help.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public boolean isCompraTaquillaDisabled() {
        String isCompraTaquillaDisabled = getNoObligatoryProperty(COMPRA_TAQUILLA_DISABLED);
        if (isCompraTaquillaDisabled == null || !isCompraTaquillaDisabled.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public boolean isMenuClientesEnabled() {
        String menuAbono = getNoObligatoryProperty(MENU_CLIENTES);
        if (menuAbono == null || !menuAbono.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public boolean isMenuVentaAnticipadaEnabled() {
        String menuVentaAnticipada = getNoObligatoryProperty(MENU_VENTA_ANTICIPADA);
        if (menuVentaAnticipada == null || !menuVentaAnticipada.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public boolean isShowComoNosConocisteEnabled() {
        String showComoNosConociste = getNoObligatoryProperty(SHOW_COMO_NOS_CONOCISTE);
        if (showComoNosConociste == null || !showComoNosConociste.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public boolean isMenuAbonosEnabled() {
        String menuAbono = getNoObligatoryProperty(MENU_ABONO);
        if (menuAbono == null || !menuAbono.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public boolean isContratoGeneralEnabled() {
        String checkContratacionGeneralHabilitado = getNoObligatoryProperty(CHECK_CONTRATACION_GENERAL_HABILIDADO);
        return (checkContratacionGeneralHabilitado != null && checkContratacionGeneralHabilitado
            .equalsIgnoreCase("true"));
    }

    public Boolean isMenuTpvEnabled() {
        String tpv = getNoObligatoryProperty(TPV);
        if (tpv != null && tpv.length() > 0 && !tpv.equals("false")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isMenuICAAEnabled() {
        String menuICAA = getNoObligatoryProperty(MENU_ICAA);
        if (menuICAA == null || !menuICAA.equalsIgnoreCase("true"))
            return false;
        else
            return true;
    }

    public boolean hasNumeracionSecuencialEnabled() {
        String noNumeradaSecuencial = getNoObligatoryProperty(NO_NUMERADA_SECUENCIAL);
        if (noNumeradaSecuencial != null && noNumeradaSecuencial.length() > 0 && !noNumeradaSecuencial.equals("false")) {
            return true;
        } else {
            return false;
        }
    }

    public String getPayModes(Locale locale) {
        List<String> modes = Arrays.asList(getProperty(PAY_MODES).split(Configuration.PROPERTIES_SEPARATOR));
        return Configuration.getPayModes(modes, locale);
    }
}