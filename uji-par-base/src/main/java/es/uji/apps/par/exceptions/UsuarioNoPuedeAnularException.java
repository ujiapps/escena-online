package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class UsuarioNoPuedeAnularException extends GeneralPARException
{
    public UsuarioNoPuedeAnularException()
    {
        super(USUARIO_NO_PUEDE_ANULAR_CODE, USUARIO_NO_PUEDE_ANULAR);
    }
}
