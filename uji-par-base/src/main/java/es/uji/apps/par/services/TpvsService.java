package es.uji.apps.par.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.par.dao.TpvsDAO;
import es.uji.apps.par.db.TpvsDTO;
import es.uji.apps.par.model.Tpv;

@Service
public class TpvsService {

    @Autowired
    private TpvsDAO tpvsDAO;

    public List<Tpv> getTpvs(
        String userUID,
        boolean onlyVisibles,
        String sortParameter,
        int start,
        int limit
    ) {
        List<Tpv> tpvs = new ArrayList<>();
        List<TpvsDTO> tpvsDto = tpvsDAO.getTpvs(userUID, onlyVisibles, sortParameter, start, limit);

        for (TpvsDTO tpvDto : tpvsDto) {
            tpvs.add(Tpv.tpvDTOToTpv(tpvDto));
        }

        return tpvs;
    }

    public long countTpvsVisible(String userUID, Tpv tpv) {
        return tpvsDAO.countTpvsVisible(userUID, tpv.getId());
    }

    public Tpv update(Tpv tpv) {
        TpvsDTO tpvDTO = Tpv.tpvToTpvDTO(tpv);
        tpvsDAO.update(tpvDTO);
        return tpv;
    }
}