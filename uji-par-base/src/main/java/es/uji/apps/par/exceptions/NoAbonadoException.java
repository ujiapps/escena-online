package es.uji.apps.par.exceptions;

public class NoAbonadoException extends GeneralPARException {
    String abono;

    public NoAbonadoException(
        String email,
        String abono
    ) {
        super(NO_ABONADO_CODE, NO_ABONADO + ": " + email + " - " + abono);
        this.abono = abono;
    }

    public String getAbono() {
        return abono;
    }
}
