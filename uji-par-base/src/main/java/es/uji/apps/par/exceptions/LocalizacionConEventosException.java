package es.uji.apps.par.exceptions;

public class LocalizacionConEventosException extends GeneralPARException{

    public LocalizacionConEventosException() {
        super(LOCALIZACION_CON_EVENTOS_CODE, LOCALIZACION_CON_EVENTOS);
    }
}
