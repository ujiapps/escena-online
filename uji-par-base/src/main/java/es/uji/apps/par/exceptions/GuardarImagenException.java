package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class GuardarImagenException extends GeneralPARException
{
    public GuardarImagenException(String fileName)
    {
        super(ADE_ERROR_CODE, String.format("%s %s", ADE_ERROR, fileName));
    }
}
