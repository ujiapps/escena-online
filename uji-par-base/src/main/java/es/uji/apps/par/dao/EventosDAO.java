package es.uji.apps.par.dao;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.StringPath;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.config.ConfigurationSelector;
import es.uji.apps.par.database.DatabaseHelper;
import es.uji.apps.par.database.DatabaseHelperFactory;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.EventoMultisesionDTO;
import es.uji.apps.par.db.QCineDTO;
import es.uji.apps.par.db.QEventoDTO;
import es.uji.apps.par.db.QEventoMultisesionDTO;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSalasUsuarioDTO;
import es.uji.apps.par.db.QSesionDTO;
import es.uji.apps.par.db.QTipoEventoDTO;
import es.uji.apps.par.db.QTpvsDTO;
import es.uji.apps.par.db.QUsuarioDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TpvsDTO;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.ext.ExtGridFilter;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.EventoMultisesion;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.TipoEvento;
import es.uji.apps.par.model.Tpv;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.Utils;

@Repository
public class EventosDAO extends BaseDAO {
    private QEventoDTO qEventoDTO = QEventoDTO.eventoDTO;
    private QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
    private QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
    private QEventoMultisesionDTO qEventoMultisesionDTO = QEventoMultisesionDTO.eventoMultisesionDTO;

    private DatabaseHelper dbHelper;

    @Autowired
    private TpvsDAO tpvsDAO;

    @Autowired
    Configuration configuration;

    @Autowired
    ConfigurationSelector configurationSelector;

    @Autowired
    public EventosDAO(Configuration configuration) {
        dbHelper = DatabaseHelperFactory.newInstance(configuration);
    }

    @Transactional
    public List<Evento> getAbonos(
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId
    ) throws ParseException {
        return getEventosConPrimeraFechaCelebracion(true, sortParameter, start, limit, filter, userUID, cineId, null,
            null, true);
    }

    @Transactional
    public List<Evento> getEventos(
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId,
        String fecha
    ) throws ParseException {
        return getEventosConPrimeraFechaCelebracion(false, sortParameter, start, limit, filter, userUID, cineId, null,
            fecha, false);
    }

    @Transactional
    public List<Evento> getEventosActivos(
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId,
        String fecha
    ) throws ParseException {
        return getEventosConPrimeraFechaCelebracion(true, sortParameter, start, limit, filter, userUID, cineId, null,
            fecha, false);
    }

    @Transactional
    public List<Evento> getEventosActivosBySala(
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId,
        Long salaId,
        String fecha
    ) throws ParseException {
        return getEventosConPrimeraFechaCelebracion(true, sortParameter, start, limit, filter, userUID, cineId, salaId,
            fecha, false);
    }

    @Transactional
    public List<Evento> getEventosTaquilla(
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId
    ) {
        return getEventosConPrimeraFechaCelebracionTaquilla(false, sortParameter, start, limit, filter, userUID,
            cineId);
    }

    @Transactional
    public List<Evento> getEventosActivosTaquilla(
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId
    ) {
        return getEventosConPrimeraFechaCelebracionTaquilla(true, sortParameter, start, limit, filter, userUID, cineId);
    }

    @SuppressWarnings({"rawtypes"})
    protected OrderSpecifier<String> getSort(
        EntityPath entity,
        String sortParameter,
        EntityPath entityOpcional
    ) {
        if (hasSort(sortParameter) && !sortParameter.contains("fechaPrimeraSesion"))
            return super.getSort(entity, sortParameter);

        StringPath strPath = new StringPath(entityOpcional, "fechaCelebracion");
        return strPath.desc();
    }

    @Transactional
    public List<Evento> getEventosConSesionesRecientes(String userUID, int prevDays, boolean onlySingleSession) {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -prevDays);
        Timestamp semanaPrevia = new Timestamp(calendar.getTimeInMillis());

        BooleanExpression where =
            qSesionDTO.fechaCelebracion.gt(semanaPrevia).and(qSalasUsuarioDTO.parUsuario.usuario.eq(userUID));
        if (onlySingleSession) {
            where = where.and(qSesionDTO.multisesion.isFalse());
        }

        JPAQuery query = new JPAQuery(entityManager);
        List<EventoDTO> eventos = query.from(qEventoDTO).leftJoin(qEventoDTO.parSesiones, qSesionDTO).fetch()
            .leftJoin(qSesionDTO.parSala, qSalaDTO).leftJoin(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .where(where)
            .list(qEventoDTO);

        return toEventosConSesiones(eventos);
    }

    private List<Evento> toEventosConSesiones(List<EventoDTO> eventosDTO) {
        List<Evento> eventos = new ArrayList<Evento>();

        for (EventoDTO eventoDTO : eventosDTO) {
            Evento evento = Evento.eventoDTOtoEvento(eventoDTO);
            if (!eventos.stream().filter(e -> e.getId() == evento.getId()).findAny().isPresent()) {
                List<Sesion> sesiones = new ArrayList<Sesion>();

                if (eventoDTO.getParSesiones() != null) {
                    for (SesionDTO sesionDTO : eventoDTO.getParSesiones()) {
                        sesiones.add(Sesion.SesionDTOToSesionSinEvento(sesionDTO));
                    }
                }

                evento.setSesiones(sesiones);

                eventos.add(evento);
            }
        }

        return eventos;
    }

    private List<Evento> getEventosConPrimeraFechaCelebracion(
        boolean activos,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId,
        Long salaId,
        String fecha,
        boolean isAbono
    ) throws ParseException {

        String sql = "select distinct" + "  e.CARACTERISTICAS_ES," + "  e.CARACTERISTICAS_VA," + "  e.COMENTARIOS_ES,"
            + "  e.COMENTARIOS_VA," + "  e.COMPANYIA_ES," + "  e.COMPANYIA_VA," + "  e.DESCRIPCION_ES,"
            + "  e.DESCRIPCION_VA," + "  e.DURACION_ES," + "  e.DURACION_VA,"
            + "  t.ID                       as tipoId," + "  t.NOMBRE_ES                as parTiposEvento,"
            + "  t.NOMBRE_VA," + "  e.PREMIOS_ES," + "  e.PREMIOS_VA," + "  e.TITULO_ES                as tituloes,"
            + "  e.TITULO_VA                as titulova," + "  e.IMAGEN_SRC               as imagensrc,"
            + "  e.IMAGEN_CONTENT_TYPE," + "  e.ID," + "  e.ASIENTOS_NUMERADOS       as asientosnumerados,"
            + "  e.RETENCION_SGAE           as retencionsgae," + "  e.IVA_SGAE                 as ivasgae,"
            + "  e.PORCENTAJE_IVA           as porcentajeiva," + "  e.RSS_ID                   as rssid,"
            + "  (select min(s.FECHA_CELEBRACION)" + "   from PAR_SESIONES s"
            + "   where e.id = s.EVENTO_ID and (s.anulada is NULL or s.anulada=" + dbHelper.falseString() + ")) as fechaPrimeraSesion," + "  e.EXPEDIENTE," + "  e.COD_DISTRI,"
            + "  e.NOM_DISTRI," + "  e.NACIONALIDAD," + "  e.VO," + "  e.METRAJE," + "  e.SUBTITULOS,"
            + "  t.exportar_icaa," + "  e.formato," + "  (select count(*)" + "   from par_eventos_multisesion pem"
            + "   where pem.evento_id = e.id)," + "  tp.ID                      as tpv,"
            + "  tp.NOMBRE                  as tpvNombre," + "  e.PROMOTOR," + "  e.NIF_PROMOTOR,"
            + "  e.IMAGEN_PUBLI_SRC         as imagenPublisrc," + "  e.IMAGEN_PUBLI_CONTENT_TYPE,"
            + "  e.INTERPRETES_ES,"
            + "  e.INTERPRETES_VA, e.EMAIL_PROMOTOR, e.TLF_PROMOTOR, e.DIRECCION_PROMOTOR, e.PUBLICO, e.ISABONO, "
            + " e.ENTRADAS_NOMINALES, e.RESERVAS_PUBLICAS, e.ENTRADAS_LIMITADAS, e.ENTRADAS_POR_EMAIL, e.DURACION " + " from PAR_EVENTOS e" + "  join PAR_SESIONES s on e.id ="
            + " s.EVENTO_ID"
            + "  join PAR_SALAS sa on s.sala_id = sa.id" + "  join PAR_SALAS_USUARIOS sau on sa.id = sau.SALA_ID"
            + "  join PAR_USUARIOS u on u.id = sau.USUARIO_ID" + "  join PAR_TIPOS_EVENTO t on e.TIPO_EVENTO_ID = t.id"
            + "  join PAR_TPVS tp on e.TPV_ID = tp.id" + " where (e.isabono = " + (isAbono ?
            dbHelper.trueString() :
            dbHelper.falseString()) + " and " + (activos ? getWhereActivos(userUID) : getWhereTodos(userUID)) + ")" + (
            salaId != null ?
                " and s.sala_id=" + salaId + " " :
                " ") + (fecha != null ?
            " and s.FECHA_CELEBRACION >= " + dbHelper.toDate() + "('" + DateUtils
                .dateToSpanishStringWithHour(DateUtils.databaseWithSecondsToDate(fecha + " 00:00:00")) + "', '"
                + dbHelper.dateFormatter() + "') and s.FECHA_CELEBRACION <= " + dbHelper.toDate() + "('" + DateUtils
                .dateToSpanishStringWithHour(DateUtils.databaseWithSecondsToDate(fecha + " 23:59:59")) + "', '"
                + dbHelper.dateFormatter() + "') " :
            "");

        sql = addFilters(sql, filter);

        sql += " " + "UNION" + "" + " select distinct" + " e.CARACTERISTICAS_ES," + "  e.CARACTERISTICAS_VA,"
            + "  e.COMENTARIOS_ES," + "  e.COMENTARIOS_VA," + "  e.COMPANYIA_ES," + "  e.COMPANYIA_VA,"
            + "  e.DESCRIPCION_ES," + "  e.DESCRIPCION_VA," + "  e.DURACION_ES," + "  e.DURACION_VA,"
            + "  t.ID                       as tipoId," + "  t.NOMBRE_ES                as parTiposEvento,"
            + "  t.NOMBRE_VA," + "  e.PREMIOS_ES," + "  e.PREMIOS_VA," + "  e.TITULO_ES                as tituloes,"
            + "  e.TITULO_VA                as titulova," + "  e.IMAGEN_SRC               as imagensrc,"
            + "  e.IMAGEN_CONTENT_TYPE," + "  e.ID," + "  e.ASIENTOS_NUMERADOS       as asientosnumerados,"
            + "  e.RETENCION_SGAE           as retencionsgae," + "  e.IVA_SGAE                 as ivasgae,"
            + "  e.PORCENTAJE_IVA           as porcentajeiva," + "  e.RSS_ID                   as rssid,"
            + " CAST(NULL as timestamp)," + "  e.EXPEDIENTE," + "  e.COD_DISTRI," + "  e.NOM_DISTRI,"
            + "  e.NACIONALIDAD," + "  e.VO," + "  e.METRAJE," + "  e.SUBTITULOS," + "  t.exportar_icaa,"
            + "  e.formato," + "  0," + "  tp.ID                      as tpv,"
            + "  tp.NOMBRE                  as tpvNombre," + "  e.PROMOTOR," + "  e.NIF_PROMOTOR,"
            + "  e.IMAGEN_PUBLI_SRC         as imagenPublisrc," + "  e.IMAGEN_PUBLI_CONTENT_TYPE,"
            + "  e.INTERPRETES_ES,"
            + "  e.INTERPRETES_VA, e.EMAIL_PROMOTOR, e.TLF_PROMOTOR, e.DIRECCION_PROMOTOR, e.PUBLICO, e.ISABONO,"
            + " e.ENTRADAS_NOMINALES, e.RESERVAS_PUBLICAS, e.ENTRADAS_LIMITADAS, e.ENTRADAS_POR_EMAIL, e.DURACION " + " from PAR_EVENTOS e join par_cines c on e.cine_id = c"
            + ".id"
            + "  left join PAR_SESIONES s on e.id = s.EVENTO_ID"
            + "  join PAR_TIPOS_EVENTO t on e.TIPO_EVENTO_ID = t.id " + "  join PAR_TPVS tp on e.TPV_ID = tp.id "
            + "where s.id is null and c.id = " + cineId + " and e.isabono = " + (isAbono ?
            dbHelper.trueString() :
            dbHelper.falseString());

        sql = addFilters(sql, filter);

        sql = addSorter(sql, sortParameter);

        String sqlPaginado = paginaConsulta(start, limit, sql);

        Query query = entityManager.createNativeQuery(sqlPaginado);

        List<Object[]> result = query.getResultList();

        return listadoTuplasConFechaAListadoEventos(result);
    }

    private List<Evento> getEventosConPrimeraFechaCelebracionTaquilla(
        boolean activos,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId
    ) {

        String sql = "select distinct" + "  e.CARACTERISTICAS_ES," + "  e.CARACTERISTICAS_VA," + "  e.COMENTARIOS_ES,"
            + "  e.COMENTARIOS_VA," + "  e.COMPANYIA_ES," + "  e.COMPANYIA_VA," + "  e.DESCRIPCION_ES,"
            + "  e.DESCRIPCION_VA," + "  e.DURACION_ES," + "  e.DURACION_VA,"
            + "  t.ID                       as tipoId," + "  t.NOMBRE_ES                as parTiposEvento,"
            + "  t.NOMBRE_VA," + "  e.PREMIOS_ES," + "  e.PREMIOS_VA," + "  e.TITULO_ES                as tituloes,"
            + "  e.TITULO_VA                as titulova," + "  e.IMAGEN_SRC               as imagensrc,"
            + "  e.IMAGEN_CONTENT_TYPE," + "  e.ID," + "  e.ASIENTOS_NUMERADOS       as asientosnumerados,"
            + "  e.RETENCION_SGAE           as retencionsgae," + "  e.IVA_SGAE                 as ivasgae,"
            + "  e.PORCENTAJE_IVA           as porcentajeiva," + "  e.RSS_ID                   as rssid,"
            + "  (select min(s.FECHA_CELEBRACION)" + "   from PAR_SESIONES s"
            + "   where e.id = s.EVENTO_ID) as fechaPrimeraSesion," + "  e.EXPEDIENTE," + "  e.COD_DISTRI,"
            + "  e.NOM_DISTRI," + "  e.NACIONALIDAD," + "  e.VO," + "  e.METRAJE," + "  e.SUBTITULOS,"
            + "  t.exportar_icaa," + "  e.formato," + "  (select count(*)" + "   from par_eventos_multisesion pem"
            + "   where pem.evento_id = e.id)," + "  tp.ID                      as tpv,"
            + "  tp.NOMBRE                  as tpvNombre," + "  e.PROMOTOR," + "  e.NIF_PROMOTOR,"
            + "  e.IMAGEN_PUBLI_SRC         as imagenPublisrc," + "  e.IMAGEN_PUBLI_CONTENT_TYPE,"
            + "  e.INTERPRETES_ES,"
            + "  e.INTERPRETES_VA, e.EMAIL_PROMOTOR, e.TLF_PROMOTOR, e.DIRECCION_PROMOTOR, e.PUBLICO, e.ISABONO,"
            + " e.ENTRADAS_NOMINALES, e.RESERVAS_PUBLICAS, e.ENTRADAS_LIMITADAS, e.ENTRADAS_POR_EMAIL, e.DURACION " + " from PAR_EVENTOS e" + "  join PAR_SESIONES s on e.id ="
            + " s.EVENTO_ID"
            + "  join PAR_SALAS sa on s.sala_id = sa.id" + "  join PAR_SALAS_USUARIOS sau on sa.id = sau.SALA_ID"
            + "  join PAR_USUARIOS u on u.id = sau.USUARIO_ID" + "  join PAR_TIPOS_EVENTO t on e.TIPO_EVENTO_ID = t.id"
            + "  join PAR_TPVS tp on e.TPV_ID = tp.id" + " where (e.isabono = " + dbHelper.falseString() + " and " + (
            activos ?
                getWhereActivos(userUID) :
                getWhereTodos(userUID)) + ")";

        sql = addFilters(sql, filter);

        sql = addSorter(sql, sortParameter);

        String sqlPaginado = paginaConsulta(start, limit, sql);

        Query query = entityManager.createNativeQuery(sqlPaginado);

        List<Object[]> result = query.getResultList();

        return listadoTuplasConFechaAListadoEventos(result);
    }

    private String paginaConsulta(
        int start,
        int limit,
        String sql
    ) {
        return dbHelper.paginate(start, limit, sql);
    }

    private String getWhereTodos(String userUID) {
        return " u.usuario = '" + userUID + "' or e.CINE_ID IS NULL";
    }

    private String getWhereActivos(String userUID) {
        return " (s.ID IS NULL or (s.FECHA_CELEBRACION >= " + dbHelper.toDate() + "('" + DateUtils
            .dateToSpanishStringWithHour(configuration.dateConMargenTrasVenta()) + "','" + dbHelper.dateFormatter()
            + "'))) and (u.usuario = '" + userUID + "' or e.CINE_ID IS " + "NULL)";
    }

    private Evento objectArrayToEvento(
        Object[] array,
        boolean isMenuTpvEnabled
    ) {
        Evento evento = new Evento();

        evento.setCaracteristicasEs((String) array[0]);
        evento.setCaracteristicasVa((String) array[1]);

        evento.setComentariosEs((String) array[2]);
        evento.setComentariosVa((String) array[3]);

        evento.setCompanyiaEs((String) array[4]);
        evento.setCompanyiaVa((String) array[5]);

        evento.setDescripcionEs(Utils.sinUnicodes((String) array[6]));
        evento.setDescripcionVa(Utils.sinUnicodes((String) array[7]));

        evento.setDuracionEs((String) array[8]);
        evento.setDuracionVa((String) array[9]);

        if (array[10] != null) {

            TipoEvento tipoEvento = new TipoEvento();
            tipoEvento.setId(dbHelper.castId(array[10]));
            tipoEvento.setNombreEs((String) array[11]);
            tipoEvento.setNombreVa((String) array[12]);
            tipoEvento.setExportarICAA(dbHelper.castBoolean(array[33]));
            evento.setParTipoEvento(tipoEvento);
        }

        evento.setPremiosEs((String) array[13]);
        evento.setPremiosVa((String) array[14]);

        evento.setTituloEs((String) array[15]);
        evento.setTituloVa((String) array[16]);

        evento.setImagenSrc((String) array[17]);
        evento.setImagenContentType((String) array[18]);

        evento.setId(dbHelper.castId(array[19]));

        evento.setAsientosNumerados(dbHelper.castBoolean(array[20]));
        if (array[21] != null) {
            evento.setRetencionSGAE(dbHelper.castBigDecimal(array[21]));
        }
        if (array[22] != null) {
            evento.setIvaSGAE(dbHelper.castBigDecimal(array[22]));
        }
        if (array[23] != null) {
            evento.setPorcentajeIVA(dbHelper.castBigDecimal(array[23]));
        }

        evento.setRssId((String) array[24]);

        Timestamp ts = (Timestamp) array[25];
        if (ts != null)
            evento.setFechaPrimeraSesion(new Date(ts.getTime()));

        evento.setExpediente((String) array[26]);
        evento.setCodigoDistribuidora((String) array[27]);
        evento.setNombreDistribuidora((String) array[28]);
        evento.setNacionalidad((String) array[29]);
        evento.setVo((String) array[30]);
        evento.setMetraje((String) array[31]);
        evento.setSubtitulos((String) array[32]);
        evento.setFormato((String) array[34]);

        if (array[35] != null) {
            BigDecimal numeroEventosHijos = dbHelper.castBigDecimal(array[35]);
            evento.setMultisesion((numeroEventosHijos.compareTo(BigDecimal.ZERO) == 0) ? "" : "on");
        }

        if (array[36] != null) {
            Tpv tpv = new Tpv();
            tpv.setId(dbHelper.castId(array[36]));
            tpv.setNombre((String) array[37]);
            evento.setParTpv(tpv);
        }
        evento.setMultipleTpv(isMenuTpvEnabled);

        evento.setPromotor((String) array[38]);
        evento.setNifPromotor((String) array[39]);

        evento.setImagenPubliSrc((String) array[40]);
        evento.setImagenPubliContentType((String) array[41]);

        evento.setInterpretesEs((String) array[42]);
        evento.setInterpretesVa((String) array[43]);
        evento.setEmailPromotor((String) array[44]);
        evento.setTelefonoPromotor((String) array[45]);
        evento.setDireccionPromotor((String) array[46]);
        evento.setPublico(dbHelper.castBoolean(array[47]));
        evento.setAbono(dbHelper.castBoolean(array[48]));
        evento.setEntradasNominales(dbHelper.castBoolean(array[49]));
        evento.setReservasPublicas(dbHelper.castBoolean(array[50]));
        evento.setEntradasLimitadas(dbHelper.castBoolean(array[51]));
        evento.setEntradasPorEmail((Integer) array[52]);
        evento.setDuracion(((Integer) array[53]).intValue());

        return evento;
    }

    private JPAQuery getQueryEventosConSesion(
        ExtGridFilterList filter,
        String userUID
    ) {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;
        QTipoEventoDTO qTipoEventoDTO = QTipoEventoDTO.tipoEventoDTO;
        QTpvsDTO qTpvsDTO = QTpvsDTO.tpvsDTO;

        JPAQuery query = new JPAQuery(entityManager);

        BooleanBuilder where = new BooleanBuilder(qUsuarioDTO.usuario.eq(userUID));
        where.and(getFilterWhere(filter)).and(qEventoDTO.isabono.isFalse());

        return query.from(qEventoDTO).join(qEventoDTO.parSesiones, qSesionDTO).join(qSesionDTO.parSala, qSalaDTO)
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).join(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
            .join(qEventoDTO.parTiposEvento, qTipoEventoDTO).join(qEventoDTO.parTpv, qTpvsDTO).where(where).distinct();
    }

    private JPAQuery getQueryEventosSinSesionByCine(
        ExtGridFilterList filter,
        Long idCine
    ) {
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QCineDTO qCineDTO = QCineDTO.cineDTO;
        QTipoEventoDTO qTipoEventoDTO = QTipoEventoDTO.tipoEventoDTO;

        JPAQuery query = new JPAQuery(entityManager);

        BooleanBuilder where = new BooleanBuilder(qCineDTO.id.eq(idCine).and(qSesionDTO.id.isNull()));
        where.and(getFilterWhere(filter)).and(qEventoDTO.isabono.isFalse());

        return query.from(qEventoDTO).join(qEventoDTO.parCine, qCineDTO).leftJoin(qEventoDTO.parSesiones, qSesionDTO)
            .join(qEventoDTO.parTiposEvento, qTipoEventoDTO).where(where).distinct();
    }

    private BooleanBuilder getFilterWhere(
        ExtGridFilterList filter
    ) {
        BooleanBuilder where = new BooleanBuilder();
        if (filter != null) {
            final ExtGridFilter tituloVaFilter = filter.findFiltroByProperty("tituloVa");
            if (tituloVaFilter != null) {
                where.and(
                    qEventoDTO.tituloEs.upper().like("%" + tituloVaFilter.getValue().toString().toUpperCase() + "%"));
            }

            final ExtGridFilter tituloEsFilter = filter.findFiltroByProperty("tituloEs");
            if (tituloEsFilter != null) {
                where.and(
                    qEventoDTO.tituloEs.upper().like("%" + tituloEsFilter.getValue().toString().toUpperCase() + "%"));
            }
        }
        return where;
    }

    private JPAQuery getQueryEventosActivos(
        ExtGridFilterList filter,
        String userUID
    ) {
        QTipoEventoDTO qTipoEventoDTO = QTipoEventoDTO.tipoEventoDTO;
        QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;
        QSesionDTO qSesionDTO = QSesionDTO.sesionDTO;
        QTpvsDTO qTpvsDTO = QTpvsDTO.tpvsDTO;
        JPAQuery query = new JPAQuery(entityManager);

        Timestamp now = new Timestamp(configuration.dateConMargenTrasVenta().getTime());

        BooleanBuilder where = new BooleanBuilder(qUsuarioDTO.usuario.eq(userUID))
            .and(qEventoDTO.parTiposEvento.id.eq(qTipoEventoDTO.id).and(qSesionDTO.fechaCelebracion.after(now)));
        where.and(getFilterWhere(filter)).and(qEventoDTO.isabono.isFalse());

        return query.from(qEventoDTO).join(qEventoDTO.parSesiones, qSesionDTO).join(qSesionDTO.parSala, qSalaDTO)
            .join(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).join(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
            .join(qEventoDTO.parTiposEvento, qTipoEventoDTO).join(qEventoDTO.parTpv, qTpvsDTO).where(where).distinct();
    }

    private List<Evento> listadoTuplasConFechaAListadoEventos(List<Object[]> listEventoFechaSesion) {
        List<Evento> listadoEventos = new ArrayList<>();
        Boolean isMenuTpvEnabled = configurationSelector.isMenuTpvEnabled();
        for (Object[] eventoConFecha : listEventoFechaSesion) {
            Evento evento = objectArrayToEvento(eventoConFecha, isMenuTpvEnabled);
            listadoEventos.add(evento);
        }
        return listadoEventos;
    }

    @Transactional
    public long removeEvento(long id) {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qEventoDTO);
        return delete.where(qEventoDTO.id.eq(id)).execute();
    }

    @Transactional
    public Evento addEvento(
        Evento evento,
        String userUID
    ) {
        EventoDTO eventoDTO = rellenarParEventoDTOConParEvento(evento, new EventoDTO(), userUID);
        entityManager.persist(eventoDTO);
        updateEventosMultisesion(eventoDTO.getId(), evento.getEventosMultisesion());

        return new Evento(eventoDTO, false);
    }

    private EventoDTO rellenarParEventoDTOConParEvento(
        Evento evento,
        EventoDTO eventoDTO,
        String userUID
    ) {
        eventoDTO.setCaracteristicasEs(evento.getCaracteristicasEs());
        eventoDTO.setCaracteristicasVa(evento.getCaracteristicasVa());

        eventoDTO.setComentariosEs(evento.getComentariosEs());
        eventoDTO.setComentariosVa(evento.getComentariosVa());

        eventoDTO.setCompanyiaEs(evento.getCompanyiaEs());
        eventoDTO.setCompanyiaVa(evento.getCompanyiaVa());

        eventoDTO.setDescripcionEs(evento.getDescripcionEs());
        eventoDTO.setDescripcionVa(evento.getDescripcionVa());

        eventoDTO.setDuracionEs(evento.getDuracionEs());
        eventoDTO.setDuracionVa(evento.getDuracionVa());
        eventoDTO.setDuracion(evento.getDuracion());

        byte[] imagen = evento.getImagen();
        if (evento.getImagenUUID() != null || (imagen != null && imagen.length > 0)) {
            if (evento.getImagenUUID() != null) {
                eventoDTO.setImagenUUID(evento.getImagenUUID());
            } else {
                eventoDTO.setImagen(evento.getImagen());
            }
            eventoDTO.setImagenSrc(evento.getImagenSrc());
            eventoDTO.setImagenContentType(evento.getImagenContentType());
        }

        byte[] imagenPubli = evento.getImagenPubli();
        if (evento.getImagenPubliUUID() != null || (imagenPubli != null && imagenPubli.length > 0)) {
            if (evento.getImagenPubliUUID() != null) {
                eventoDTO.setImagenPubliUUID(evento.getImagenPubliUUID());
            } else {
                eventoDTO.setImagenPubli(evento.getImagenPubli());
            }
            eventoDTO.setImagenPubliSrc(evento.getImagenPubliSrc());
            eventoDTO.setImagenPubliContentType(evento.getImagenPubliContentType());
        }

        eventoDTO.setInterpretesEs(evento.getInterpretesEs());
        eventoDTO.setInterpretesVa(evento.getInterpretesVa());

        if (evento.getParTiposEvento() != null) {
            eventoDTO.setParTiposEvento(TipoEvento.tipoEventoToTipoEventoDTO(evento.getParTiposEvento()));
        }

        TpvsDTO parTpv;
        if (evento.getParTpv() != null) {
            parTpv = Tpv.tpvToTpvDTO(evento.getParTpv());
        } else {
            parTpv = tpvsDAO.getTpvDefault(userUID);
        }
        if (parTpv == null) {
            throw new CampoRequeridoException("TPV");
        }
        eventoDTO.setParTpv(parTpv);

        eventoDTO.setPromotor(evento.getPromotor());
        eventoDTO.setNifPromotor(evento.getNifPromotor());
        eventoDTO.setEmailPromotor(evento.getEmailPromotor());
        eventoDTO.setTelefonoPromotor(evento.getTelefonoPromotor());
        eventoDTO.setDireccionPromotor(evento.getDireccionPromotor());

        eventoDTO.setPremiosEs(evento.getPremiosEs());
        eventoDTO.setPremiosVa(evento.getPremiosVa());

        eventoDTO.setRssId(evento.getRssId());
        eventoDTO.setTituloEs(evento.getTituloEs());
        eventoDTO.setTituloVa(evento.getTituloVa());

        eventoDTO.setPorcentajeIva(evento.getPorcentajeIVA());
        eventoDTO.setAsientosNumerados(evento.getAsientosNumerados());
        eventoDTO.setEntradasNominales(evento.getEntradasNominales());
        eventoDTO.setRetencionSgae(evento.getRetencionSGAE());
        eventoDTO.setIvaSgae(evento.getIvaSGAE());

        eventoDTO.setExpediente(evento.getExpediente());
        eventoDTO.setFormato(evento.getFormato());
        eventoDTO.setCodigoDistribuidora(evento.getCodigoDistribuidora());
        eventoDTO.setNombreDistribuidora(evento.getNombreDistribuidora());
        eventoDTO.setNacionalidad(evento.getNacionalidad());
        eventoDTO.setVo(evento.getVo());
        eventoDTO.setMetraje(evento.getMetraje());
        eventoDTO.setSubtitulos(evento.getSubtitulos());

        if (evento.getId() != 0)
            eventoDTO.setId(evento.getId());

        if (evento.getCine() != null)
            eventoDTO.setParCine(new CineDTO(evento.getCine().getId()));

        eventoDTO.setPublico(evento.getPublico());
        eventoDTO.setReservasPublicas(evento.getReservasPublicas());
        eventoDTO.setIsabono(evento.getAbono());
        eventoDTO.setEntradasLimitadas(evento.getEntradasLimitadas());
        eventoDTO.setEntradasPorEmail(evento.getEntradasPorEmail());

        return eventoDTO;
    }

    @Transactional
    public Evento updateEvento(
        Evento evento,
        String userUID
    ) {
        if (evento.getParTpv() == null) {
            throw new CampoRequeridoException("TPV");
        }

        EventoDTO eventoDTO = getEventoById(evento.getId(), userUID);
        eventoDTO = rellenarParEventoDTOConParEvento(evento, eventoDTO, userUID);
        entityManager.persist(eventoDTO);

        updateEventosMultisesion(evento.getId(), evento.getEventosMultisesion());
        return evento;
    }

    private void updateEventosMultisesion(
        long idEvento,
        List<EventoMultisesion> eventosMultisesion
    ) {
        JPADeleteClause del = new JPADeleteClause(entityManager, qEventoMultisesionDTO);
        del.where(qEventoMultisesionDTO.parEvento.id.eq(idEvento)).execute();

        if (eventosMultisesion != null) {
            for (EventoMultisesion eventoHijo : eventosMultisesion) {
                EventoMultisesionDTO eventoMultisesionDTO = new EventoMultisesionDTO();
                eventoMultisesionDTO.setParEvento(new EventoDTO(idEvento));
                eventoMultisesionDTO.setParEventoHijo(new EventoDTO(eventoHijo.getId()));
                eventoMultisesionDTO.setVersionLinguistica(eventoHijo.getVersionLinguistica());
                entityManager.persist(eventoMultisesionDTO);
            }
        }
    }

    public Tuple getImagenUUID(long idEvento) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qEventoDTO).where(qEventoDTO.id.eq(idEvento))
            .uniqueResult(qEventoDTO.imagenUUID, qEventoDTO.imagenPubliUUID);
    }

    @Transactional
    public EventoDTO updateEventoDTO(EventoDTO eventoDTO) {
        return entityManager.merge(eventoDTO);
    }

    @Transactional
    public void deleteImagen(long eventoId) {
        JPAUpdateClause jpaUpdate = new JPAUpdateClause(entityManager, qEventoDTO);

        jpaUpdate.setNull(qEventoDTO.imagen).setNull(qEventoDTO.imagenContentType).setNull(qEventoDTO.imagenSrc)
            .setNull(qEventoDTO.imagenUUID).where(qEventoDTO.id.eq(eventoId)).execute();
    }

    @Transactional
    public void deleteImagenPubli(long eventoId) {
        JPAUpdateClause jpaUpdate = new JPAUpdateClause(entityManager, qEventoDTO);

        jpaUpdate.setNull(qEventoDTO.imagenPubli).setNull(qEventoDTO.imagenPubliContentType)
            .setNull(qEventoDTO.imagenPubliSrc).setNull(qEventoDTO.imagenPubliUUID).where(qEventoDTO.id.eq(eventoId))
            .execute();
    }

    @Transactional
    public EventoDTO getEventoById(
        long eventoId,
        String userUID
    ) {
        QCineDTO qCineDTO = QCineDTO.cineDTO;
        QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
        QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
        QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<EventoDTO> list =
            query.from(qEventoDTO).leftJoin(qEventoDTO.parCine, qCineDTO).leftJoin(qCineDTO.parSalas, qSalaDTO)
                .leftJoin(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).leftJoin(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
                .where((qUsuarioDTO.usuario.eq(userUID).or(qCineDTO.isNull())).and(qEventoDTO.id.eq(eventoId)))
                .list(qEventoDTO);

        return list != null && list.size() > 0 ? list.get(0) : null;
    }

    @Transactional
    public Tuple getImagenesEventoById(
        long eventoId
    ) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qEventoDTO).where(qEventoDTO.id.eq(eventoId))
            .uniqueResult(qEventoDTO.imagen, qEventoDTO.imagenUUID, qEventoDTO.imagenContentType,
                qEventoDTO.imagenPubli, qEventoDTO.imagenPubliUUID, qEventoDTO.imagenPubliContentType);
    }

    @Transactional
    public EventoDTO getEventoByRssId(
        String rssId,
        String userUID
    ) {

        QTipoEventoDTO qTipoEvento = QTipoEventoDTO.tipoEventoDTO;
        QCineDTO qCineDTO = QCineDTO.cineDTO;
        QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;

        JPAQuery query = new JPAQuery(entityManager);

        List<EventoDTO> list = query.from(qEventoDTO).leftJoin(qEventoDTO.parTiposEvento, qTipoEvento).fetch()
            .leftJoin(qEventoDTO.parCine, qCineDTO).leftJoin(qCineDTO.parSalas, qSalaDTO)
            .leftJoin(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO).leftJoin(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
            .where((qUsuarioDTO.usuario.eq(userUID).or(qCineDTO.isNull())).and(qEventoDTO.rssId.eq(rssId)))
            .list(qEventoDTO);

        return list != null && list.size() > 0 ? list.get(0) : null;
    }

    @Transactional
    public int getTotalEventosActivos(
        ExtGridFilterList filter,
        String userUID,
        Long idCine
    ) {
        return (int) getQueryEventosActivos(filter, userUID).count() + (int) getQueryEventosSinSesionByCine(filter,
            idCine).count();
    }

    @Transactional
    public int getTotalEventos(
        ExtGridFilterList filter,
        String userUID,
        Long idCine
    ) {
        return (int) getQueryEventosConSesion(filter, userUID).count() + (int) getQueryEventosSinSesionByCine(filter,
            idCine).count();
    }

    @Transactional
    public int getTotalEventosActivosTaquilla(
        ExtGridFilterList filter,
        String userUID
    ) {
        return (int) getQueryEventosActivos(filter, userUID).count();
    }

    @Transactional
    public int getTotalEventosTaquilla(
        ExtGridFilterList filter,
        String userUID,
        Long idCine
    ) {
        return (int) getQueryEventosConSesion(filter, userUID).count();
    }

    @Transactional
    public List<EventoDTO> getEventosActivosParaVentaOnline() {
        JPAQuery query = new JPAQuery(entityManager);
        QSesionDTO qSesionDTO = new QSesionDTO("qSesionDTO");
        Calendar cal = Calendar.getInstance();
        Timestamp currentTime = new Timestamp(cal.getTime().getTime());

        return query.from(qEventoDTO).join(qEventoDTO.parSesiones, qSesionDTO).where(qSesionDTO.canalInternet.eq(true)
            .and(qSesionDTO.fechaInicioVentaOnline.before(currentTime)
                .and(qSesionDTO.fechaFinVentaOnline.after(currentTime)))).list(qEventoDTO);
    }

    @Transactional
    public List<EventoDTO> getPeliculas() {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoEventoDTO tiposICAA = new QTipoEventoDTO("tiposICAA");

        return query.from(qEventoDTO).where(qEventoDTO.parTiposEvento.id
            .in(new JPASubQuery().from(tiposICAA).where(tiposICAA.exportarICAA.eq(true)).list(tiposICAA.id)))
            .orderBy(qEventoDTO.tituloEs.asc()).orderBy(qEventoDTO.tituloVa.asc()).list(qEventoDTO);
    }

    @Transactional
    public List<EventoDTO> getPeliculas(long eventoId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qEventoMultisesionDTO).where(qEventoMultisesionDTO.parEvento.id.eq(eventoId))
            .orderBy(qEventoMultisesionDTO.id.asc()).list(qEventoMultisesionDTO.parEventoHijo);
    }

    @Transactional
    public List<Tuple> getPeliculasMultisesion(long eventoId) {
        JPAQuery query = new JPAQuery((entityManager));
        QEventoDTO qEventoDTO1 = new QEventoDTO("qEventoDTO1");
        return query.from(qEventoMultisesionDTO).leftJoin(qEventoMultisesionDTO.parEventoHijo, qEventoDTO1)
            .where(qEventoMultisesionDTO.parEvento.id.eq(eventoId))
            .list(new QTuple(qEventoDTO1, qEventoMultisesionDTO.versionLinguistica));
    }


    private String addFilters(
        String sql,
        ExtGridFilterList filter
    ) {
        if (filter != null) {
            final ExtGridFilter tituloVaFilter = filter.findFiltroByProperty("tituloVa");
            if (tituloVaFilter != null) {
                sql += " and upper(e.TITULO_VA) like '%" + tituloVaFilter.getValue().toString().toUpperCase()
                    .replace("'", "''") + "%'";
            }

            final ExtGridFilter tituloEsFilter = filter.findFiltroByProperty("tituloEs");
            if (tituloEsFilter != null) {
                sql += " and upper(e.TITULO_ES) like '%" + tituloEsFilter.getValue().toString().toUpperCase()
                    .replace("'", "''") + "%'";
            }
        }
        return sql;
    }

    private String addSorter(
        String sql,
        String sortParameter
    ) {
        sql += " order by ";

        Type type = new TypeToken<List<Map<String, String>>>() {
        }.getType();
        List<Map<String, String>> order = new Gson().fromJson(sortParameter, type);

        sql += order.get(0).get("property") + " " + order.get(0).get("direction");

        return sql;
    }

    public boolean isEventoReservasPublicasBySesionId(Long sesionId) {
        QSesionDTO qSesionDTO = new QSesionDTO("qSesionDTO");

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qEventoDTO).leftJoin(qEventoDTO.parSesiones, qSesionDTO).where(qSesionDTO.id.eq(sesionId)).singleResult(qEventoDTO.reservasPublicas);
    }
}
