package es.uji.apps.par.config;

import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public interface ConfigurationSelector
{
	String getUrlPublic();
	String getUrlPublicLimpio();
	String getUrlAdmin();
	String getHtmlTitle();
	String getMailFrom();
	String getUrlComoLlegar();
	String getUrlCondicionesPrivacidad();
	String getUrlCondicionesCancelacion();
	String getUrlPieEntrada();
	String getLogoReport();
	String getClasePostventa();
	String getNombreMunicipio();
	String getApiKey();
	String getLangsAllowed();
	boolean getLocalizacionEnValenciano();
	String getIdiomaPorDefecto();
	boolean showButacasHanEntradoEnDistintoColor();
	boolean showIVA();
	boolean showTotalesSinComision();
	String getInformes();
	String getInformesGenerales();
	String getPayModes(Locale locale);
	boolean isMenuSalasEnabled();
	boolean isMenuIntegracionesEnabled();
	boolean isMenuPerfilEnabled();
	boolean isMenuVentaAnticipadaEnabled();
	boolean isMenuAbonosEnabled();
	boolean isShowComoNosConocisteEnabled();
	boolean isCompraTaquillaDisabled();
	boolean isMenuClientesEnabled();
	boolean isMenuICAAEnabled();
	Boolean isMenuTpvEnabled();
	boolean isContratoGeneralEnabled();
	boolean hasHelp();
    boolean hasNumeracionSecuencialEnabled();
}
