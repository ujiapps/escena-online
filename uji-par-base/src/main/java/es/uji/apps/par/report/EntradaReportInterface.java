package es.uji.apps.par.report;

public interface EntradaReportInterface {
	void generaPaginaButaca(EntradaModelReport entrada, String urlPublic);
	void setFecha(String fecha);
	void setHora(String hora);
	void setHoraApertura(String horaApertura);
}
