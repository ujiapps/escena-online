package es.uji.apps.par.builders;

import java.text.ParseException;

import es.uji.commons.web.template.model.Pagina;

public interface PublicPageBuilderInterface {
	Pagina buildPublicPageInfo(String urlBase, String url, String idioma, String htmlTitle) throws ParseException;
}
