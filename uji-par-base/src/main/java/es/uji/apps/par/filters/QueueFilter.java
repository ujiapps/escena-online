package es.uji.apps.par.filters;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.io.filefilter.AndFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.services.QueueService;

public class QueueFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(QueueFilter.class);

    public static final String TMP_DIR = System.getProperty("java.io.tmpdir");
    public static final String SPRING_PREFIX = "session_spring_";
    public static final String TMP_PREFIX = "session_tmp_";
    public static final String QUEU_ID_ATTR = "uuid";

    public static final Pattern QUEUE_PATTERN = Pattern.compile("^(/par-public/rest/evento(?!.*(imagen))"
        + ".*|/par-public/rest/entrada/[0-9]+)$");

    @Autowired
    private QueueService queueService;

    @Autowired
    protected Configuration configuration;

    public void init(FilterConfig filterConfig) {

    }

    public void destroy() {
    }

    public void doFilter(
        ServletRequest servletRequest,
        ServletResponse servletResponse,
        FilterChain filterChain
    ) throws IOException, ServletException {
        initService(servletRequest);

        String domains = configuration.getQueueDomains();
        if (domains != null) {
            HttpServletRequest sRequest = (HttpServletRequest) servletRequest;
            if (QUEUE_PATTERN.matcher(sRequest.getRequestURI()).matches()) {
                if (domains != null && Arrays.asList(domains.split(",")).contains(servletRequest.getServerName())) {
                    Cookie[] cookies = sRequest.getCookies();
                    Optional<Cookie> parPublicSessionCookie =
                        cookies != null ? Arrays.stream(cookies).filter(c -> c.getName().toLowerCase().equals("par_public_session"))
                            .findAny() : null;
                    if (cookies == null || !parPublicSessionCookie.isPresent() || !existCookieAsSessionFile(parPublicSessionCookie)) {
                        String uuid = servletRequest.getParameter(QUEU_ID_ATTR);
                        if (uuid == null) {
                            uuid = UUID.randomUUID().toString();
                        }

                        File file = new File(Paths.get(TMP_DIR, TMP_PREFIX + uuid).toString());
                        try {
                            if (file.exists()) {
                                file.setLastModified(new Date().getTime());
                            } else {
                                file.createNewFile();
                            }

                            BasicFileAttributes attributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
                            int users = getOlder(file.getParentFile(), new Date(attributes.creationTime().toMillis()));
                            Integer queueLimit = configuration.getQueueLimit(400);
                            log.info(String.format("==================> Límite de la cola actual: %s", queueLimit));
                            if (users < queueLimit) {
                                servletRequest.setAttribute(QUEU_ID_ATTR, uuid);
                            } else {
                                HttpServletResponse sResponse = (HttpServletResponse) servletResponse;
                                sResponse.sendRedirect(String.format("/statics/espera.html?url=%s%%3F%s%%3D%s", ((HttpServletRequest) servletRequest).getRequestURI(), QUEU_ID_ATTR, uuid));
                                return;
                            }
                        } catch (Exception e) {

                        }
                    }
                }
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private boolean existCookieAsSessionFile(Optional<Cookie> cookie) {
        if (cookie.isPresent()) {
            Path newFilePath = Paths.get(TMP_DIR, SPRING_PREFIX + cookie.get().getValue());
            return newFilePath != null && Files.exists(newFilePath);
        }
        return false;
    }

    private int getOlder(
        File dir,
        Date date
    ) {
        IOFileFilter filter = new IOFileFilter() {
            @Override
            public boolean accept(File file) {
                try {
                    BasicFileAttributes attributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
                    return new Date(attributes.creationTime().toMillis()).before(date);
                } catch (IOException e) {
                    return false;
                }
            }

            @Override
            public boolean accept(
                File file,
                String s
            ) {
                return accept(file);
            }
        };

        if (dir.isDirectory()) {
            Collection<File> files = FileUtils.listFiles(dir,
                new AndFileFilter(new PrefixFileFilter(Arrays.asList(SPRING_PREFIX, TMP_PREFIX)),
                    filter), null);
            return files.size();
        }
        return 0;
    }

    public void deleteOlder(
    ) {
        if (configuration.getQueueDomains() != null) {
            deleteOlderTempInactive();
            deleteOlderSpringInactive();
        }
    }

    private void deleteOlderSpringInactive() {
        Collection<File> files =
            FileUtils.listFiles(new File(TMP_DIR), new PrefixFileFilter(SPRING_PREFIX), null);
        List<String> activeSessions = queueService.getActiveSessions();
        List<File> filesToDelete =
            files.stream().filter(f -> !activeSessions.contains(f.getName().substring(SPRING_PREFIX.length())))
                .collect(Collectors.toList());
        for (File file : filesToDelete) {
            boolean success = FileUtils.deleteQuietly(file);
            if (!success) {
                log.warn("No se ha podido eliminar el archivo: " + file.getAbsolutePath());
            }
        }
    }

    private void deleteOlderTempInactive() {
        int whenMinutes = -1;
        Date targetTime = new Date();
        targetTime = DateUtils.addMinutes(targetTime, whenMinutes);

        Collection<File> files = FileUtils.listFiles(new File(TMP_DIR),
            new AndFileFilter(new PrefixFileFilter(TMP_PREFIX, IOCase.INSENSITIVE), new AgeFileFilter(targetTime)),
            null);
        for (File file : files) {
            boolean success = FileUtils.deleteQuietly(file);
            if (!success) {
                log.warn("No se ha podido eliminar el archivo: " + file.getAbsolutePath());
            }
        }
    }

    private void initService(ServletRequest servletRequest) {
        if (configuration == null) {
            ServletContext servletContext = servletRequest.getServletContext();
            WebApplicationContext webApplicationContext =
                WebApplicationContextUtils.getWebApplicationContext(servletContext);
            configuration = (Configuration) webApplicationContext.getBean("Configuration");
        }
    }
}