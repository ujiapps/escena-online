package es.uji.apps.par.exceptions;

public class EmailCompraNoCoincideException extends Throwable {
    public EmailCompraNoCoincideException(
        String uuidCompra,
        String emailExpected,
        String email
    ) {
        super(String.format("La compra %s que está intentando anular está registrada con el email %s y se ha recibido el email %s", uuidCompra, emailExpected, email));
    }
}
