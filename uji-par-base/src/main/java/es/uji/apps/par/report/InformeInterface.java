package es.uji.apps.par.report;

import java.io.OutputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

import es.uji.apps.fopreports.serialization.ReportSerializationException;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.exceptions.SinIvaException;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.InformeSesion;

public interface InformeInterface
{
	InformeInterface create(Locale locale, Configuration configuration, String logoReport, boolean showIVA, boolean showTotalesSinComision, String location);
	void serialize(OutputStream output) throws ReportSerializationException;
	void genera(String inicio, String fin, List<InformeModelReport> compras, InformeTotalesModelReport informeTotalesModelReport, String usuario, String sortBy);
	void genera(String inicio, String fin, List<InformeModelReport> compras, List<InformeAbonoReport> abonos, String cargoInformeEfectivo,
    		String firmanteInformeEfectivo, String sortBy) throws SinIvaException;
	void genera(String titulo, String inicio, String fin, List<InformeModelReport> compras, List<InformeAbonoReport> abonos, String cargoInformeEfectivo,
			String firmanteInformeEfectivo, String sortBy) throws SinIvaException;
	void genera(String cargo, String firmante, List<InformeSesion> informesSesion, Cine cine, boolean printSesion, boolean printDesgloses, String fechaInicio, String fechaFin, String sortBy) throws SinIvaException;
	void genera(String inicio, String fin, List<InformeModelReport> compras, String sortBy) throws SinIvaException;
    void genera(long sesionId, String userUID) throws SinIvaException;
	void genera(String fechaInicio, String fechaFin, String userUID) throws ParseException;
	void genera(Cine cine, String fechaInicio, String fechaFin, List list) throws ParseException;
}
