package es.uji.apps.par.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.db.AbonadoDTO;
import es.uji.apps.par.db.TarifaDTO;

@XmlRootElement
public class Abonado {

    private long id;
    private String nombre;
    private String apellidos;
    private String direccion;
    private String poblacion;
    private String cp;
    private String provincia;
    private String telefono;
    private String email;
    private boolean infoPeriodica;
    private Abono abono;
    private Float importe;
    private String idDevolucion;
    private String uuid;
    private int abonos;

    public Abonado()
    {
    }

    public static AbonadoDTO toAbonadoDTO(Abonado abonado) {
        AbonadoDTO abonadoDTO = new AbonadoDTO();

        abonadoDTO.setNombre(abonado.getNombre());
        abonadoDTO.setApellidos(abonado.getApellidos());
        abonadoDTO.setDireccion(abonado.getDireccion());
        abonadoDTO.setPoblacion(abonado.getPoblacion());
        abonadoDTO.setCp(abonado.getCp());
        abonadoDTO.setProvincia(abonado.getProvincia());
        abonadoDTO.setTelefono(abonado.getTelefono());
        abonadoDTO.setEmail(abonado.getEmail());
        abonadoDTO.setInfoPeriodica(abonado.isInfoPeriodica());
        abonadoDTO.setFecha(new Timestamp(new Date().getTime()));
        abonadoDTO.setTarifa(new TarifaDTO(abonado.getAbono().getId()));
        abonadoDTO.setImporte(new BigDecimal(abonado.getImporte()));
        abonadoDTO.setCodigoPagoPasarela(abonado.getIdDevolucion());
        abonadoDTO.setUuid(abonado.getUuid());

        return abonadoDTO;
    }

    public Abonado(AbonadoDTO abonadoDTO) {
        this.id = abonadoDTO.getId();
        this.nombre = abonadoDTO.getNombre();
        this.apellidos = abonadoDTO.getApellidos();
        this.direccion = abonadoDTO.getDireccion();
        this.poblacion = abonadoDTO.getPoblacion();
        this.cp = abonadoDTO.getCp();
        this.provincia = abonadoDTO.getProvincia();
        this.telefono = abonadoDTO.getTelefono();
        this.email = abonadoDTO.getEmail();
        if (abonadoDTO.getInfoPeriodica() != null) {
            this.infoPeriodica = abonadoDTO.getInfoPeriodica();
        }
        this.abono = new Abono(abonadoDTO.getTarifa());
        this.importe = abonadoDTO.getImporte().floatValue();
        this.idDevolucion = abonadoDTO.getCodigoPagoPasarela();
        this.uuid = abonadoDTO.getUuid();
        this.abonos = abonadoDTO.getAbonos();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isInfoPeriodica() {
        return infoPeriodica;
    }

    public void setInfoPeriodica(boolean infoPeriodica) {
        this.infoPeriodica = infoPeriodica;
    }

    public Abono getAbono() {
        return abono;
    }

    public void setAbono(Abono abono) {
        this.abono = abono;
    }

    public Float getImporte() {
        return importe;
    }

    public void setImporte(Float importe) {
        this.importe = importe;
    }

    public String getIdDevolucion() {
        return idDevolucion;
    }

    public void setIdDevolucion(String idDevolucion) {
        this.idDevolucion = idDevolucion;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getAbonos() {
        return abonos;
    }

    public void setAbonos(int abonos) {
        this.abonos = abonos;
    }
}
