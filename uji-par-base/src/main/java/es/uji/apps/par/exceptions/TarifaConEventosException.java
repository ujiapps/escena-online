package es.uji.apps.par.exceptions;

public class TarifaConEventosException extends GeneralPARException {

    public TarifaConEventosException() {
        super(TARIFA_CON_EVENTOS_CODE, TARIFA_CON_EVENTOS);
    }
}
