package es.uji.apps.par.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.db.ReportDTO;

@XmlRootElement
public class Report implements Serializable {
    private static final long serialVersionUID = 1L;

    private long id;
    private Cine parCine;
    private String clase;
    private String tipo;

    public Report(
        Long cineId,
        String clase,
        String tipo
    ) {
        Cine cine = new Cine();
        cine.setId(cineId);
        this.parCine = cine;
        this.clase = clase;
        this.tipo = tipo;
    }

    public static ReportDTO reportToReportDTO(Report report) {
        ReportDTO reportDTO = new ReportDTO();

        reportDTO.setId(report.getId());
        reportDTO.setParCine(Cine.cineToCineDTO(report.getParCine()));
        reportDTO.setClase(report.getClase());
        reportDTO.setTipo(report.getTipo());

        return reportDTO;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Cine getParCine() {
        return parCine;
    }

    public void setParCine(Cine parCine) {
        this.parCine = parCine;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}