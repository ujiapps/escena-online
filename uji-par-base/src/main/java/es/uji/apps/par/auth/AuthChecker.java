package es.uji.apps.par.auth;

import javax.servlet.http.HttpServletRequest;

public class AuthChecker
{
    public static void canWrite(HttpServletRequest request) throws RuntimeException
    {
        Boolean readonly = (Boolean) request.getSession().getAttribute(Authenticator.READONLY_ATTRIBUTE);
        
        if (readonly != null && readonly.booleanValue())
        {
            throw new RuntimeException("Usuario sólo lectura ha intentado guardar");
        }

        Boolean taquilla = (Boolean) request.getSession().getAttribute(Authenticator.TAQUILLA_ATTRIBUTE);

        if (taquilla != null && taquilla.booleanValue())
        {
            throw new RuntimeException("Usuario sólo habilitado para la venta en taquilla ha intentado guardar");
        }
    }

    public static void canBuy(HttpServletRequest request) throws RuntimeException
    {
        Boolean readonly = (Boolean) request.getSession().getAttribute(Authenticator.READONLY_ATTRIBUTE);

        if (readonly != null && readonly.booleanValue())
        {
            throw new RuntimeException("Usuario sólo lectura ha intentado registrar ventas");
        }
    }

	public static String getUserUID(HttpServletRequest request)
	{
		return (String) request.getSession().getAttribute(Authenticator.USER_ATTRIBUTE);
	}
}
