package es.uji.apps.par.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.model.SorterInforme;
import es.uji.apps.par.model.TipoInforme;

@Service
public class InformesService {
    @Autowired
    Configuration configuration;

    public List<SorterInforme> getSortersInformesByTipo(
        String tiposInforme,
        String language,
        String typeId
    ) {
        return getTiposInforme(tiposInforme, language).stream().filter(tipoInforme -> tipoInforme.getId().equals(typeId))
            .filter(tipoInforme -> tipoInforme.getSorters() != null).map(TipoInforme::getSorters).findFirst()
            .orElse(SorterInforme.getDefaultSorters(language));
    }

    private List<TipoInforme> getTiposInforme(
        String tiposInforme,
        String language
    ) {
        Type listType = new TypeToken<ArrayList<TipoInforme>>() {
        }.getType();
        List<TipoInforme> tiposInformeDisponibles = new Gson().fromJson(tiposInforme, listType);

        if (language != null) {
            for (TipoInforme tipoInformeDisponible : tiposInformeDisponibles) {
                if (language.equals("es"))
                    tipoInformeDisponible.setNombre(tipoInformeDisponible.getNombreES());
                else
                    tipoInformeDisponible.setNombre(tipoInformeDisponible.getNombreCA());

                if (tipoInformeDisponible.getSorters() != null) {
                    for (SorterInforme sorterInforme : tipoInformeDisponible.getSorters()) {
                        if (language.equals("es"))
                            sorterInforme.setNombre(sorterInforme.getNombreES());
                        else
                            sorterInforme.setNombre(sorterInforme.getNombreCA());
                    }
                }
            }
        }

        return tiposInformeDisponibles;
    }

    public List<TipoInforme> getTiposInformeGenerales(
        String tiposInforme,
        String language
    ) {
        return getTiposInforme(tiposInforme, language).stream().filter(ti -> !ti.isSessionType()).collect(Collectors.toList());
    }

    public List<TipoInforme> getTiposInformeGeneralesSesion(
        String tiposInforme,
        String language
    ) {
        return getTiposInforme(tiposInforme, language).stream().filter(ti -> ti.isSessionType()).collect(Collectors.toList());
    }
}
