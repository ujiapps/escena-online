package es.uji.apps.par.exceptions;


public class EventoNominalException extends GeneralPARException
{
    public EventoNominalException()
    {
        super(EMAIL_VERIFICACION_CODE, EMAIL_VERIFICACION);
    }
}
