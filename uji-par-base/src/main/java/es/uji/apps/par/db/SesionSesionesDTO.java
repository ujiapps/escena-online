package es.uji.apps.par.db;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PAR_SESIONES_SESIONES")
public class SesionSesionesDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "PAR_SESIONES_SESIONES_ID_GENERATOR", sequenceName = "HIBERNATE_SEQUENCE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAR_SESIONES_SESIONES_ID_GENERATOR")
    private long id;

    @ManyToOne
    @JoinColumn(name = "SESION_MULTIPLE_ID")
    private SesionDTO sesionMultiple;

    @ManyToOne
    @JoinColumn(name = "SESION_ID")
    private SesionDTO sesion;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SesionDTO getSesionMultiple() {
        return sesionMultiple;
    }

    public void setSesionMultiple(SesionDTO sesionMultiple) {
        this.sesionMultiple = sesionMultiple;
    }

    public SesionDTO getSesion() {
        return sesion;
    }

    public void setSesion(SesionDTO sesion) {
        this.sesion = sesion;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SesionSesionesDTO) {
            SesionSesionesDTO toCompare = (SesionSesionesDTO) o;
            return this.id == toCompare.id;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(id).intValue();
    }
}