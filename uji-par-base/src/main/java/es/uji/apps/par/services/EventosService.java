package es.uji.apps.par.services;

import com.mysema.query.Tuple;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.dao.ComprasDAO;
import es.uji.apps.par.dao.EventosDAO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.EventoConCompras;
import es.uji.apps.par.exceptions.EventoNoEncontradoException;
import es.uji.apps.par.exceptions.FormatoImagenException;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.EventoMultisesion;
import es.uji.apps.par.model.EventoParaSync;
import es.uji.apps.par.model.IImagenesEvento;
import es.uji.apps.par.model.ImagenesEvento;
import es.uji.apps.par.utils.Environment;

@Service
public class EventosService {
    private static final Logger log = LoggerFactory.getLogger(EventosService.class);

    @Autowired
    private EventosDAO eventosDAO;

    @Autowired
    private ComprasDAO comprasDAO;

    @Autowired
    Configuration configuration;

    @Autowired
    SalasService salasService;

    @Autowired
    ImageService imageService;

    @Autowired
    LangService langService;

    public List<Evento> getEventosConSesionesRecientes(String userUID, int prevDays, boolean onlySingleSession) {
        return eventosDAO.getEventosConSesionesRecientes(userUID, prevDays, onlySingleSession);
    }

    public List<Evento> getAbonos(
        String sort,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId
    ) throws ParseException {
        return eventosDAO.getAbonos(sort, start, limit, filter, userUID, cineId);
    }

    public List<Evento> getEventos(
        String sort,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId,
        String fecha
    ) throws ParseException {
        return getEventos(false, sort, start, limit, filter, userUID, cineId, fecha);
    }

    public List<Evento> getEventosActivos(
        String sort,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId,
        String fecha
    ) throws ParseException {
        return getEventos(true, sort, start, limit, filter, userUID, cineId, fecha);
    }

    public List<Evento> getEventosActivosBySala(
        String sort,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId,
        Long salaId,
        String fecha
    ) throws ParseException {
        return eventosDAO.getEventosActivosBySala(sort, start, limit, filter, userUID, cineId, salaId, fecha);
    }

    public List<Evento> getEventosTaquilla(
        String sort,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId
    ) {
        return getEventosTaquilla(false, sort, start, limit, filter, userUID, cineId);
    }

    public List<Evento> getEventosActivosTaquilla(
        String sort,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId
    ) {
        return getEventosTaquilla(true, sort, start, limit, filter, userUID, cineId);
    }

    private List<Evento> getEventos(
        boolean activos,
        String sort,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId,
        String fecha
    ) throws ParseException {
        if (activos)
            return eventosDAO.getEventosActivos(sort, start, limit, filter, userUID, cineId, fecha);
        else
            return eventosDAO.getEventos(sort, start, limit, filter, userUID, cineId, fecha);
    }

    private List<Evento> getEventosTaquilla(
        boolean activos,
        String sort,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID,
        Long cineId
    ) {
        if (activos)
            return eventosDAO.getEventosActivosTaquilla(sort, start, limit, filter, userUID, cineId);
        else
            return eventosDAO.getEventosTaquilla(sort, start, limit, filter, userUID, cineId);
    }

    @Transactional
    public void removeEvento(Long eventoId) {
        if (hasEventoCompras(Long.valueOf(eventoId).intValue())) {
            throw new EventoConCompras(eventoId);
        } else {
            removeImagenes(eventoId);
            eventosDAO.removeEvento(eventoId);
        }
    }

    private boolean hasEventoCompras(Integer eventoId) {
        return comprasDAO.getComprasOfEvento(eventoId).size() > 0;
    }

    @Transactional
    public Evento addEvento(
        Evento evento,
        String userId
    ) throws CampoRequeridoException, IOException {
        checkRequiredFields(evento);
        checkAndSaveImage(evento);
        return eventosDAO.addEvento(evento, userId);
    }

    private void checkAndSaveImage(Evento evento) throws IOException {
        byte[] imagen = evento.getImagen();
        if (imagen != null && imagen.length > 0) {
            if (imageService.isImageValidFormat(imagen)) {
                imagen = imageService.getImageWithoutAlpha(imagen);
                String imagenSrc = evento.getImagenSrc();
                String imagenContentType = evento.getImagenContentType();
                if (imageService.hasADEConfigured() && imagenContentType != null) {
                    String reference = imageService.insertImageToAdeWS(imagen, imagenSrc, imagenContentType);
                    evento.setImagenUUID(reference);
                } else {
                    evento.setImagen(imagen);
                }
            } else {
                throw new FormatoImagenException();
            }
        }

        byte[] imagenPubli = evento.getImagenPubli();
        if (imagenPubli != null && imagenPubli.length > 0) {
            if (imageService.isImageValidFormat(imagenPubli)) {
                imagenPubli = imageService.getImageWithoutAlpha(imagenPubli);
                String imagenPubliSrc = evento.getImagenPubliSrc();
                String imagenPubliContentType = evento.getImagenPubliContentType();
                if (imageService.hasADEConfigured() && imagenPubliContentType != null) {
                    String reference =
                        imageService.insertImageToAdeWS(imagenPubli, imagenPubliSrc, imagenPubliContentType);
                    evento.setImagenPubliUUID(reference);
                } else {
                    evento.setImagenPubli(imagenPubli);
                }
            } else {
                throw new FormatoImagenException();
            }
        }
    }

    private void checkRequiredFields(Evento evento) throws CampoRequeridoException {
        if (langService.isLangAllowed("es") && (evento.getTituloEs() == null || evento.getTituloEs().isEmpty()))
            throw new CampoRequeridoException("Título");
        if (langService.isLangAllowed("ca") && (evento.getTituloVa() == null || evento.getTituloVa().isEmpty()))
            throw new CampoRequeridoException("Títol");
        if (evento.getParTiposEvento() == null)
            throw new CampoRequeridoException("Tipo de evento");
    }

    public void updateEvento(
        Evento evento,
        String userUID
    ) throws CampoRequeridoException, EventoConCompras, IOException {
        checkRequiredFields(evento);

        if (hasEventoCompras(Long.valueOf(evento.getId()).intValue()) && modificaAsientosNumeradosOEntradasNominales(evento, userUID))
            throw new EventoConCompras(evento.getId());
        else {
            checkAndSaveImage(evento);
            eventosDAO.updateEvento(evento, userUID);
        }
    }

    private boolean modificaAsientosNumeradosOEntradasNominales(
        Evento evento,
        String userUID
    ) {
        EventoDTO eventoDTO = eventosDAO.getEventoById(evento.getId(), userUID);
        return eventoDTO.getAsientosNumerados() != evento.getAsientosNumerados() || eventoDTO.getEntradasNominales() != evento.getEntradasNominales();
    }

    public ImagenesEvento getImagenesEvento(Long eventoId) {
        Tuple imagenesEventoById = eventosDAO.getImagenesEventoById(eventoId);
        if (imagenesEventoById != null) {
            ImagenesEvento imagenesEvento = new ImagenesEvento();
            imagenesEvento.setImagen(imagenesEventoById.get(0, byte[].class));
            imagenesEvento.setImagenUUID(imagenesEventoById.get(1, String.class));
            imagenesEvento.setImagenContentType(imagenesEventoById.get(2, String.class));
            imagenesEvento.setImagenPubli(imagenesEventoById.get(3, byte[].class));
            imagenesEvento.setImagenPubliUUID(imagenesEventoById.get(4, String.class));
            imagenesEvento.setImagenPubliContentType(imagenesEventoById.get(5, String.class));

            return (ImagenesEvento) getEventoConImagen(imagenesEvento);
        } else {
            throw new EventoNoEncontradoException(eventoId.toString());
        }
    }

    public Evento getEvento(
        Long eventoId,
        String userUID
    ) throws EventoNoEncontradoException {
        EventoDTO eventoDTO = eventosDAO.getEventoById(eventoId.longValue(), userUID);
        if (eventoDTO != null) {
            Evento evento = new Evento(eventoDTO, true);
            return (Evento) getEventoConImagen(evento);
        }

        throw new EventoNoEncontradoException(eventoId.toString());
    }

    public Evento getEventoByRssId(
        String contenidoId,
        String userUID
    ) throws EventoNoEncontradoException {
        EventoDTO eventoDTO = eventosDAO.getEventoByRssId(contenidoId, userUID);

        if (eventoDTO == null)
            throw new EventoNoEncontradoException(contenidoId);
        else {
            Evento evento = new Evento(eventoDTO, true);
            return (Evento) getEventoConImagen(evento);
        }
    }

    private IImagenesEvento getEventoConImagen(IImagenesEvento evento) {
        if (evento.getImagenUUID() != null) {
            byte[] fitxerFromAdeWS = imageService.getImageFromAdeWS(evento.getImagenUUID());
            evento.setImagen(fitxerFromAdeWS);
        }

        if (evento.getImagenPubliUUID() != null) {
            byte[] fitxerFromAdeWS = imageService.getImageFromAdeWS(evento.getImagenPubliUUID());
            evento.setImagenPubli(fitxerFromAdeWS);
        }

        return evento;
    }

    @Transactional
    public void removeImagenes(Long eventoId) {
        Tuple imagenesEvento = eventosDAO.getImagenUUID(eventoId);

        if (imagenesEvento != null) {
            removeImagen(eventoId, imagenesEvento);
            removeImagenPubli(eventoId, imagenesEvento);
        } else {
            throw new EventoNoEncontradoException(eventoId.toString());
        }
    }

    public void removeImagen(Long eventoId) {
        Tuple imagenesEvento = eventosDAO.getImagenUUID(eventoId);
        removeImagen(eventoId, imagenesEvento);
    }

    public void removeImagenPubli(Long eventoId) {
        Tuple imagenesEvento = eventosDAO.getImagenUUID(eventoId);
        removeImagenPubli(eventoId, imagenesEvento);
    }

    private void removeImagenPubli(
        Long eventoId,
        Tuple imagenesEvento
    ) {
        eventosDAO.deleteImagenPubli(eventoId);
        String imagenPubliUUID = imagenesEvento.get(1, String.class);
        if (imagenPubliUUID != null) {
            imageService.removeImageFromAdeWS(imagenPubliUUID);
        }
    }

    private void removeImagen(
        Long eventoId,
        Tuple imagenesEvento
    ) {
        eventosDAO.deleteImagen(eventoId);
        String imagenUUID = imagenesEvento.get(0, String.class);
        if (imagenUUID != null) {
            imageService.removeImageFromAdeWS(imagenUUID);
        }
    }

    public int getTotalEventosActivos(
        ExtGridFilterList filter,
        String userUID,
        Long idCine
    ) {
        return eventosDAO.getTotalEventosActivos(filter, userUID, idCine);
    }

    public int getTotalEventos(
        ExtGridFilterList filter,
        String userUID,
        Long idCine
    ) {
        return eventosDAO.getTotalEventos(filter, userUID, idCine);
    }

    public int getTotalEventosActivosTaquilla(
        ExtGridFilterList filter,
        String userUID
    ) {
        return eventosDAO.getTotalEventosActivosTaquilla(filter, userUID);
    }

    public int getTotalEventosTaquilla(
        ExtGridFilterList filter,
        String userUID,
        Long idCine
    ) {
        return eventosDAO.getTotalEventosTaquilla(filter, userUID, idCine);
    }

    public List<EventoParaSync> getEventosActivosParaVentaOnline(String urlPublic) {
        List<EventoDTO> eventosDTO = eventosDAO.getEventosActivosParaVentaOnline();
        List<EventoParaSync> eventosParaSync = new ArrayList<EventoParaSync>();
        String urlPrefix = urlPublic + "/rest/evento/";
        String urlSuffix = "?lang=##IDIOMA##";

        for (EventoDTO eventoDTO : eventosDTO) {
            EventoParaSync eventoParaSync =
                new EventoParaSync(eventoDTO.getRssId(), urlPrefix + eventoDTO.getRssId() + urlSuffix);
            eventosParaSync.add(eventoParaSync);
        }
        return eventosParaSync;
    }

    @Cacheable(value = "imagenCache", sync = true)
    public byte[] getImagenSustitutivaSiExiste() throws IOException {
        String path = configuration.getPathImagenSustitutiva();
        if (path != null) {
            FileInputStream fis = new FileInputStream(Environment.getParHome() + "/imagenes/" + path);
            return IOUtils.toByteArray(fis);
        } else
            return null;
    }

    @Cacheable(value = "imagenPubliCache", sync = true)
    public byte[] getImagenPubliSustitutivaSiExiste() throws IOException {
        String path = configuration.getPathImagenPubliSustitutiva();
        if (path != null) {
            FileInputStream fis = new FileInputStream(Environment.getParHome() + "/imagenes/" + path);
            return IOUtils.toByteArray(fis);
        } else
            return null;
    }

    public String getImagenSustitutivaContentType() {
        return configuration.getImagenSustitutivaContentType();
    }

    public String getImagenPubliSustitutivaContentType() {
        return configuration.getImagenPubliSustitutivaContentType();
    }

    public List<Evento> getPeliculas() {
        List<EventoDTO> listPeliculasDTO = eventosDAO.getPeliculas();
        return getEventos(listPeliculasDTO);
    }

    private List<Evento> getEventos(List<EventoDTO> listPeliculasDTO) {
        List<Evento> listPeliculas = new ArrayList<Evento>();
        for (EventoDTO pelicula : listPeliculasDTO) {
            Evento evento = Evento.eventoDTOtoEvento(pelicula);
            listPeliculas.add(evento);
        }
        return listPeliculas;
    }

    private List<EventoMultisesion> getEventosMultisesion(List<Tuple> list) {
        List<EventoMultisesion> listPeliculas = new ArrayList<EventoMultisesion>();
        for (Tuple pelicula : list) {
            EventoDTO eventoDTO = pelicula.get(0, EventoDTO.class);
            String versionLinguistica = pelicula.get(1, String.class);
            EventoMultisesion evento = EventoMultisesion
                .tupleToEventoMultisesion(eventoDTO.getId(), eventoDTO.getTituloEs(), eventoDTO.getTituloVa(),
                    versionLinguistica);
            listPeliculas.add(evento);
        }
        return listPeliculas;
    }

    public List<EventoMultisesion> getPeliculas(long eventoId) {
        List<Tuple> listPeliculasDTO = eventosDAO.getPeliculasMultisesion(eventoId);
        return getEventosMultisesion(listPeliculasDTO);
    }

    public boolean isEventoReservasPublicasBySesionId(Long sesionId) {
        return eventosDAO.isEventoReservasPublicasBySesionId(sesionId);
    }
}
