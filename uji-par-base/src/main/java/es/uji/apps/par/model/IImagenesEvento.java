package es.uji.apps.par.model;

public interface IImagenesEvento {
    byte[] getImagen();

    String getImagenUUID();

    String getImagenContentType();

    byte[] getImagenPubli();

    String getImagenPubliUUID();

    String getImagenPubliContentType();

    void setImagen(byte[] fitxerFromAdeWS);

    void setImagenPubli(byte[] fitxerFromAdeWS);
}
