package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class UsuarioObligatorioException extends GeneralPARException
{
    public UsuarioObligatorioException()
    {
        super(USUARIO_OBLIGATORIO_CODE, USUARIO_OBLIGATORIO);
    }
}
