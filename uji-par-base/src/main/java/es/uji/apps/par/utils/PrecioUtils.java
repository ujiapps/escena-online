package es.uji.apps.par.utils;

import java.math.BigDecimal;

import es.uji.apps.par.model.GastosGestion;

public class PrecioUtils {
    public static String monedaToCents(BigDecimal cantidad) {
        BigDecimal importeCentimos = cantidad.setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
        return Integer.toString(importeCentimos.intValue());
    }

    public static BigDecimal getPrecioConComision(
        BigDecimal precio,
        BigDecimal comision
    ) {
        return precio.add(precio.multiply(comision)).setScale(2, BigDecimal.ROUND_UP);
    }

    public static GastosGestion getGastosGestion(BigDecimal precio, BigDecimal precioSinComision) {
        GastosGestion gastosGestion = new GastosGestion();

        gastosGestion.setGastosGestion(
            precio.subtract(precioSinComision).setScale(2, BigDecimal.ROUND_HALF_UP));
        gastosGestion.setGastosGestionSinIVA(
            gastosGestion.getGastosGestion().divide(new BigDecimal(1.21), 2, BigDecimal.ROUND_HALF_UP));
        gastosGestion.setIVAgastosGestion(
            gastosGestion.getGastosGestion().subtract(gastosGestion.getGastosGestionSinIVA())
                .setScale(2, BigDecimal.ROUND_HALF_UP));

        return gastosGestion;
    }
}
