package es.uji.apps.par.exceptions;

@SuppressWarnings("serial")
public class LimiteEntradasGratisSuperadoException extends GeneralPARException
{
    public LimiteEntradasGratisSuperadoException(long compraId)
    {
        super(LIMITE_ENTRADAS_GRATIS_SUPERADO_ERROR_CODE, "Compra: " + compraId);
    }

	public LimiteEntradasGratisSuperadoException()
	{
		super(LIMITE_ENTRADAS_GRATIS_SUPERADO_ERROR_CODE);
	}
}
