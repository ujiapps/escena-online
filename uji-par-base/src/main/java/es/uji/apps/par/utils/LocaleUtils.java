package es.uji.apps.par.utils;

import java.util.Arrays;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import es.uji.apps.par.config.ConfigurationSelector;
import es.uji.apps.par.i18n.ResourceProperties;

public class LocaleUtils {
    public static final String LANG = "language";

    public static void setLocale(String lang, HttpServletRequest currentRequest) {
        if (lang != null && lang.length() > 0 && (lang.equals("es") || lang.equals("ca"))) {
            HttpSession session = currentRequest.getSession();
            session.setAttribute(LANG, lang);
        }
    }

    public static java.util.Locale getLocale(ConfigurationSelector configurationSelector, HttpServletRequest currentRequest) {
        HttpSession session = currentRequest.getSession();
        String lang = (String) session.getAttribute(LANG);

        return getLocale(lang, configurationSelector, currentRequest);
    }

    public static java.util.Locale getLocale(String lang, ConfigurationSelector configurationSelector, HttpServletRequest currentRequest) {
        String idiomaFinal = configurationSelector.getIdiomaPorDefecto();
        if (lang != null && lang.length() > 0) {
            HttpSession session = currentRequest.getSession();
            session.setAttribute(LANG, lang);
            idiomaFinal = lang;
        } else if (currentRequest != null) {
            if (currentRequest.getCookies() != null) {
                for (Cookie cookie : currentRequest.getCookies()) {
                    if (cookie != null && "uji-lang".equals(cookie.getName())) {
                        String idiomaCookie = cookie.getValue();

                        if (esIdiomaValido(idiomaCookie)) {
                            idiomaFinal = idiomaCookie;
                            break;
                        }
                    }
                }
            }

            String idiomaParametro = currentRequest.getParameter("idioma");
            if (idiomaParametro != null) {
                if (esIdiomaValido(idiomaParametro)) {
                    idiomaFinal = idiomaParametro;
                }
            }
        }

        return new java.util.Locale(idiomaFinal, "ES");
    }

    private static boolean esIdiomaValido(String idioma) {
        return Arrays.asList(ResourceProperties.LENGUAJES).contains(idioma);
    }
}