package es.uji.apps.par.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import es.uji.apps.par.dao.AbonadosDAO;
import es.uji.apps.par.model.Abonado;

@Service
public class AbonadosService {

    @Autowired
    private AbonadosDAO abonadosDAO;

    @Autowired
    private ComprasService comprasService;

    public List<Abonado> getAbonadosByAbonoId(
        Long tarifaId,
        String sortParameter,
        int start,
        int limit
    ) {
        return abonadosDAO.getAbonadosByAbonoId(tarifaId, sortParameter, start, limit);
    }

    public int getTotalAbonadosByAbonoId(Long abonoId) {
        return abonadosDAO.getTotalAbonadosByAbonoId(abonoId);
    }

    @Transactional
    public void removeAbonado(
        Long abonadoId,
        String userUID
    ) {
        comprasService.anularCompraReserva(abonadoId, userUID);
    }

    @Transactional
    public int getNumeroAbonosByAbonoIdAndEmail(long tarifaId, String email) {
        return abonadosDAO.getNumeroAbonosByAbonoIdAndEmail(tarifaId, email);
    }
}
