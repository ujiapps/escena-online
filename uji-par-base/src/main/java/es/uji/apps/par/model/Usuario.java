package es.uji.apps.par.model;

import java.sql.Timestamp;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.auth.Role;
import es.uji.apps.par.db.UsuarioDTO;

@XmlRootElement
public class Usuario {
    private long id;
    private String nombre;
    private String mail;
    private String url;
    private String usuario;
    private String password;
    private Role role;
    private String tokenValidacion;
    private Timestamp expiracionToken;
    private boolean emailVerificado;
    private boolean puedeAnular;

    public Usuario() {
    }

    public Usuario(String usuario, String nombre, String mail, String password, String url) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.mail = mail;
        this.password = password;
        this.url = url;
    }

    public Usuario(UsuarioDTO usuarioDTO) {
        this.id = usuarioDTO.getId();
        this.nombre = usuarioDTO.getNombre();
        this.mail = usuarioDTO.getMail();
        this.usuario = usuarioDTO.getUsuario();
        this.url = usuarioDTO.getUrl();
        this.password = usuarioDTO.getPassword();
        this.role = usuarioDTO.getRole();
        this.emailVerificado = usuarioDTO.getEmailVerificado();
        this.tokenValidacion = usuarioDTO.getTokenValidacion();
        this.puedeAnular = usuarioDTO.getPuedeAnular();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getTokenValidacion() {
        return tokenValidacion;
    }

    public void setTokenValidacion(String tokenValidacion) {
        this.tokenValidacion = tokenValidacion;
    }

    public Timestamp getExpiracionToken() {
        return expiracionToken;
    }

    public void setExpiracionToken(Timestamp expiracionToken) {
        this.expiracionToken = expiracionToken;
    }

    public boolean getEmailVerificado() {
        return emailVerificado;
    }

    public void setEmailVerificado(boolean emailVerificado) {
        this.emailVerificado = emailVerificado;
    }

    public boolean getPuedeAnular() {
        return puedeAnular;
    }

    public void setPuedeAnular(boolean puedeAnular) {
        this.puedeAnular = puedeAnular;
    }

    public static UsuarioDTO toDTO(Usuario usuarioTaquilla) {
        UsuarioDTO usuarioDTO = new UsuarioDTO(usuarioTaquilla.getId());
        usuarioDTO.setNombre(usuarioTaquilla.getNombre());
        usuarioDTO.setUsuario(usuarioTaquilla.getUsuario());
        return usuarioDTO;
    }
}