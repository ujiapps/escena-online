package es.uji.apps.par.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DisponiblesLocalizacion
{
    private String localizacion;
    private int totales;
    private int ocupadas;
    private int disponibles;
    private long sesionId;

    public DisponiblesLocalizacion()
    {
    }

    public DisponiblesLocalizacion(String localizacion, int totales, int ocupadas)
    {
        this.localizacion = localizacion;
        this.totales = totales;
        this.ocupadas = ocupadas;
        this.disponibles = totales - ocupadas;
    }

    public DisponiblesLocalizacion(String localizacion, int totales, int ocupadas, long sesionId)
    {
        this(localizacion, totales, ocupadas);
        this.sesionId = sesionId;
    }

    public String getLocalizacion()
    {
        return localizacion;
    }

    public void setLocalizacion(String localizacion)
    {
        this.localizacion = localizacion;
    }

    public int getDisponibles()
    {
        return disponibles;
    }

    public void setDisponibles(int disponibles)
    {
        this.disponibles = disponibles;
    }

    public int getTotales() {
        return totales;
    }

    public void setTotales(int totales) {
        this.totales = totales;
    }

    public int getOcupadas() {
        return ocupadas;
    }

    public void setOcupadas(int ocupadas) {
        this.ocupadas = ocupadas;
    }

    public long getSesionId() {
        return sesionId;
    }

    public void setSesionId(long sesionId) {
        this.sesionId = sesionId;
    }
}