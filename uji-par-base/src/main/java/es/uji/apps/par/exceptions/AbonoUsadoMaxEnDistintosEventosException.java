package es.uji.apps.par.exceptions;

@SuppressWarnings("serial")
public class AbonoUsadoMaxEnDistintosEventosException extends GeneralPARException {
    String abono;

    public AbonoUsadoMaxEnDistintosEventosException(
        String email,
        String abono
    ) {
        super(ABONO_MAXIMOS_USOS_DISTINTOS_EVENTOS_CODE, email + " - " + abono);
        this.abono = abono;
    }

    public String getAbono() {
        return abono;
    }
}
