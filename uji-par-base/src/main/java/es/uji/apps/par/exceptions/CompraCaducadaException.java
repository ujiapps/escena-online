package es.uji.apps.par.exceptions;


public class CompraCaducadaException extends GeneralPARException
{
    public CompraCaducadaException()
    {
        super(COMPRA_CADUCADA_CODE, COMPRA_CADUCADA);
    }
}
