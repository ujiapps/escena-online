package es.uji.apps.par.model;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.db.TpvsDTO;
import es.uji.apps.par.utils.ExtjsTypeUtils;

@XmlRootElement
public class Tpv {
    private long id;
    private String nombre;
    private String visible;

    public Tpv()
    {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public static TpvsDTO tpvToTpvDTO(Tpv parTpv) {
        TpvsDTO tpvDTO = new TpvsDTO();

        tpvDTO.setId(parTpv.getId());
        tpvDTO.setNombre(parTpv.getNombre());
        tpvDTO.setVisible(ExtjsTypeUtils.stringToBoolean(parTpv.getVisible()));

        return tpvDTO;
    }

    public static Tpv tpvDTOToTpv(TpvsDTO tpvDTO) {
        Tpv tpv = new Tpv();
        tpv.setId(tpvDTO.getId());
        tpv.setNombre(tpvDTO.getNombre());
        tpv.setVisible(ExtjsTypeUtils.booleanToString(tpvDTO.getVisible()));

        return tpv;
    }
}
