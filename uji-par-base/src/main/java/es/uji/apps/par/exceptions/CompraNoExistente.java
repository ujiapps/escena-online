package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class CompraNoExistente extends GeneralPARException
{
    public CompraNoExistente()
    {
        super(COMPRA_NO_EXISTENTE_CODE);
    }
}
