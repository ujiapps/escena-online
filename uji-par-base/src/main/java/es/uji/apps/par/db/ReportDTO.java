package es.uji.apps.par.db;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="PAR_REPORTS")
public class ReportDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "PAR_REPORTS_ID_GENERATOR", sequenceName = "HIBERNATE_SEQUENCE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAR_REPORTS_ID_GENERATOR")
	private long id;

	@ManyToOne
	@JoinColumn(name="CINE_ID")
	private CineDTO parCine;

	private String tipo;

	private String clase;

	public ReportDTO() {
	}

	public ReportDTO(long cineId, String tipoEntrada, String clase) {
		this.parCine = new CineDTO(cineId);
		this.tipo = tipoEntrada;
		this.clase = clase;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CineDTO getParCine() {
		return parCine;
	}

	public void setParCine(CineDTO parCine) {
		this.parCine = parCine;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}
}
