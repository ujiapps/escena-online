package es.uji.apps.par.auth;

public enum Role {
    ADMIN, READONLY, TAQUILLA;
}
