package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class UsuarioNoExisteException extends GeneralPARException
{
    public UsuarioNoExisteException(String userUID)
    {
        super(USUARIO_NO_EXISTE_CODE, USUARIO_NO_EXISTE + ": " + userUID);
    }
}
