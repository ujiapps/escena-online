package es.uji.apps.par.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static Date spanishStringToDate(String spanishDate) {
        final SimpleDateFormat FORMAT_DAY = new SimpleDateFormat("dd/MM/yyyy");

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);

        if (spanishDate == null || spanishDate.isEmpty()) {
            return null;
        }

        if (isTimestamp(spanishDate)) {
            cal.setTimeInMillis(Long.valueOf(spanishDate));
        } else {
            try {
                cal.setTime(FORMAT_DAY.parse(spanishDate));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        return cal.getTime();
    }

    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    private static boolean isTimestamp(String spanishDate) {
        if (spanishDate.contains("/"))
            return false;
        else
            return true;
    }

    public static Timestamp dateToTimestampSafe(Date fecha) {
        if (fecha == null)
            return null;
        else
            return new Timestamp(fecha.getTime());
    }

    public static String getHourAndMinutesWithLeadingZeros(Date date) {
        SimpleDateFormat date_format = new SimpleDateFormat("HH:mm");
        return date_format.format(date);
    }

    public static Date addTimeToDate(
        Date startDate,
        String hour
    ) {
        if (startDate == null) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);

        if (hour != null && !hour.equals("")) {
            String[] arrHoraMinutos = hour.split(":");

            int hora = Integer.parseInt(arrHoraMinutos[0]);
            int minutos = Integer.parseInt(arrHoraMinutos[1]);

            cal.set(Calendar.HOUR_OF_DAY, hora);
            cal.set(Calendar.MINUTE, minutos);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
        }

        return cal.getTime();
    }

    public static String dateToSpanishString(Date fecha) {
        final SimpleDateFormat FORMAT_DAY = new SimpleDateFormat("dd/MM/yyyy");

        if (fecha == null) {
            throw new NullPointerException();
        }

        return FORMAT_DAY.format(fecha);
    }

    public static String dateToHourString(Date fecha) {
        final SimpleDateFormat FORMAT_HOUR = new SimpleDateFormat("HH:mm");

        if (fecha == null) {
            throw new NullPointerException();
        }

        return FORMAT_HOUR.format(fecha);
    }

    public static long millisecondsToSeconds(long time) {
        return time / 1000;
    }

    public static String dateToSpanishStringWithHour(Date fecha) {
        final SimpleDateFormat FORMAT_DAY_HOUR = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        if (fecha == null) {
            throw new NullPointerException();
        }

        return FORMAT_DAY_HOUR.format(fecha);
    }

    public static String dateToSpanishStringWithHourAndSeconds(Date fecha) {
        final SimpleDateFormat FORMAT_DAY_HOUR = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        if (fecha == null) {
            throw new NullPointerException();
        }

        return FORMAT_DAY_HOUR.format(fecha);
    }

    public static String dateToStringForFileNames(Date fecha) {
        final SimpleDateFormat FORMAT_FILE_DAY_HOUR = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

        if (fecha == null) {
            throw new NullPointerException();
        }

        return FORMAT_FILE_DAY_HOUR.format(fecha);
    }

    public static Date spanishStringWithHourstoDate(String spanishDate) throws ParseException {
        final SimpleDateFormat FORMAT_DAY_HOUR = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return FORMAT_DAY_HOUR.parse(spanishDate);
    }

    public static Date databaseWithSecondsToDate(String dateString) throws ParseException {
        final SimpleDateFormat DATABASE_WITH_SECONDS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return DATABASE_WITH_SECONDS.parse(dateString);
    }

    public static Date databaseStringToDate(String databaseDate) throws ParseException {
        final SimpleDateFormat DATABASE_DAY = new SimpleDateFormat("yyyy-MM-dd");
        return DATABASE_DAY.parse(databaseDate);
    }

    public static String formatDdmmyy(Date fecha) {
        final SimpleDateFormat FORMAT_DDMMYY = new SimpleDateFormat("ddMMyy");
        return FORMAT_DDMMYY.format(fecha);
    }

    public static String getNumeroSemana() {
        Calendar cal = Calendar.getInstance();
        int week = cal.get(Calendar.WEEK_OF_YEAR);
        return String.format("%03d", week);
    }

    public static Date getDateWithHora(
        Date fecha,
        String hora
    ) {
        if (fecha == null) {
            fecha = new Date();
        }

        if (hora == null) {
            hora = String.format("%02d:%02d", LocalDateTime.now().getHour(), LocalDateTime.now().getMinute());
        }
        String[] horaSeparada = hora.split(":");

        LocalDate fechaLD = fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDateTime fechaCompraConHora = LocalDateTime
            .of(fechaLD.getYear(), fechaLD.getMonth(), fechaLD.getDayOfMonth(), Integer.parseInt(horaSeparada[0]),
                Integer.parseInt(horaSeparada[1]));

        return Date.from(fechaCompraConHora.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static boolean isSameDay(
        Date d1,
        Date d2
    ) {
        Date date1 = setTimeToMidnight(d1);
        Date date2 = setTimeToMidnight(d2);

        return date1.equals(date2);
    }

    public static Date setTimeToMidnight(Date date) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime( date );
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static Date getDayOfWeek(
        String date,
        int dayOfWeek
    ) {
        return getDayOfWeek(DateUtils.spanishStringToDate(date), dayOfWeek);
    }

    public static Date getDayOfWeek(
        Date date,
        int dayOfWeek
    ) {
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setTime(date);
        c.set(Calendar.DAY_OF_WEEK, dayOfWeek);
        return c.getTime();
    }
}
