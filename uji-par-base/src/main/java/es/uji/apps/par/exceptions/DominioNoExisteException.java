package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class DominioNoExisteException extends GeneralPARException
{
    public DominioNoExisteException(String dominio)
    {
        super(DOMINIO_NO_EXISTE_CODE, DOMINIO_NO_EXISTE + ": " + dominio);
    }
}
