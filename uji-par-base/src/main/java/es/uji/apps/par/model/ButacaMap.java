package es.uji.apps.par.model;

import org.apache.log4j.Logger;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.db.ButacasPlantillaReservasDTO;

@XmlRootElement
public class ButacaMap
{
    public static Logger log = Logger.getLogger(ButacaMap.class);

    private String fila;
    private String numero;
    private String localizacion;
    private String texto;
    private String x;
    private String y;
    private String tipo;

    public static ButacaMap plantillaButacaReservaDTOToButacaMap(
        ButacasPlantillaReservasDTO butacasPlantillaReservasDTO,
        String language) {
        ButacaMap butaca = new ButacaMap();
        butaca.setFila(butacasPlantillaReservasDTO.getFila());
        butaca.setNumero(butacasPlantillaReservasDTO.getNumero());
        butaca.setLocalizacion(butacasPlantillaReservasDTO.getParLocalizacion().getCodigo());
        String nombreLocalizacion = butacasPlantillaReservasDTO.getParLocalizacion().getNombreEs();
        if(language.equalsIgnoreCase("ca")) {
            nombreLocalizacion = butacasPlantillaReservasDTO.getParLocalizacion().getNombreVa();
        }
        butaca.setTexto(nombreLocalizacion);

        return butaca;
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}