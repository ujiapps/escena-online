package es.uji.apps.par;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class ApplicationConfig {
    private static final Logger log = LoggerFactory.getLogger(ApplicationConfig.class);

    private String jdbcUrl;
    private String dbUser;
    private String dbPassword;
    private boolean insecure;

    public ApplicationConfig() {
    }

    public ApplicationConfig(
        String jdbcUrl,
        String dbUser,
        String dbPassword,
        boolean insecure
    ) {
        this.jdbcUrl = jdbcUrl;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
        this.insecure = insecure;
    }

    public void initialize() {
        databaseMigrate();
        if (insecure) {
            disableSSLCheckValidation();
        }
    }

    private void databaseMigrate() {
        String dbType = jdbcUrl.split(":")[1];

        LogFactory.setLogCreator(clazz -> new Log() {
            @Override
            public void debug(String message) {
                log.debug(message);
            }

            @Override
            public void info(String message) {
                log.info(message);
            }

            @Override
            public void warn(String message) {
                log.warn(message);
            }

            @Override
            public void error(String message) {
                log.error(message);
            }

            @Override
            public void error(
                String message,
                Exception e
            ) {
                log.error(message, e);
            }
        });

        Flyway flyway = new Flyway();
        flyway.setLocations("classpath:database/" + dbType);
        flyway.setDataSource(jdbcUrl, dbUser, dbPassword);
        flyway.baseline();
        flyway.repair();
        flyway.migrate();
    }

    private void disableSSLCheckValidation() {
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(
                X509Certificate[] certs,
                String authType
            ) {
            }

            public void checkServerTrusted(
                X509Certificate[] certs,
                String authType
            ) {
            }
        }};

        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            log.error("Error al deshabilitar la validación SSL", e);
        }
    }
}
