package es.uji.apps.par.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.dao.MailDAO;
import es.uji.apps.par.db.MailDTO;
import es.uji.apps.par.model.Usuario;

@Service
public class JavaHtmlMailService implements MailInterface {
    private static final Logger log = LoggerFactory.getLogger(JavaHtmlMailService.class);

    @Autowired
    MailDAO mailDao;

    @Autowired
    Configuration configuration;

    public JavaHtmlMailService() {

    }

    public void anyadeEnvio(
        String from,
        String to,
        String titulo,
        String texto,
        String uuid,
        String urlPublic,
        String urlPieEntrada
    ) {
        mailDao.insertaMail(from, to, titulo, texto, uuid, urlPublic, urlPieEntrada);
    }

    private Address[] getMailAddressList(String path) throws AddressException {
        if (path == null || path.equals("")) {
            return null;
        }

        String[] addrList = path.split(",");
        Address[] result = new Address[addrList.length];

        for (int i = 0; i < addrList.length; i++) {
            result[i] = new InternetAddress(addrList[i].trim());
        }

        return result;
    }

    private Message createMailMessage(
        String de,
        String para,
        String titulo
    ) throws MessagingException {
        Properties props = new Properties();

        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", configuration.getMailHost());
        props.put("mail.smtp.port", configuration.getMailPort());
        props.put("mail.smtp.socketFactory.port", configuration.getMailPort());
        props.put("mail.smtp.socketFactory.fallback", "false");
        props.put("mail.smtp.quitwait", "false");
        props.put("mail.smtp.auth", "true");
        if (configuration.isMailSSL()) {
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }

        if (configuration.isTLS()) {
            props.put("mail.smtp.starttls.enable", "true");
        }

        Authenticator auth = null;
        if (configuration.getMailPassword() != null && configuration.getMailUsername() != null) {
            auth = new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(configuration.getMailUsername(), configuration.getMailPassword());
                }
            };
        }
        Session session = Session.getInstance(props, auth);
        Message message = new MimeMessage(session);

        message.addHeader("Auto-Submitted", "auto-generated");
        message.setHeader("Content-Type", "text/plain; charset=UTF-8");
        message.setSubject(titulo);
        message.setSentDate(new Date());

        message.setFrom(new InternetAddress(de));
        message.setReplyTo(getMailAddressList(para));
        message.setRecipients(Message.RecipientType.TO, getMailAddressList(para));
        if (!configuration.isDebug()) {
            message.setRecipients(Message.RecipientType.BCC, getMailAddressList("nicolas.manero@cuatroochenta.com"));
        }

        return message;
    }

    public void enviaMail(
        String de,
        String para,
        String titulo,
        String texto
    ) throws MessagingException {
        Message message = createMailMessage(de, para, titulo);
        MimeMultipart multipart = createMailBodyMessage(texto);
        message.setContent(multipart, "text/html; charset=UTF-8");
        send(message);
    }

    private void enviaMailMultipart(
        String de,
        String para,
        String titulo,
        String texto,
        String uuid,
        String urlPublic,
        String urlPieEntrada,
        EntradasService entradasService,
        UsersService usersService
    ) throws MessagingException {
        try {
            if (uuid == null) {
                log.error("UUID nulo en el método enviaMailMultipart");
                throw new NullPointerException();
            }

            Message message = createMailMessage(de, para, titulo);
            Multipart multipart = createMailBodyMessage(texto);
            message.setContent(multipart, "text/html; charset=UTF-8");

            URI uri = new URI(urlPublic);
            Usuario user = usersService.getUserByServerName(uri.getHost());
            ByteArrayOutputStream attach = new ByteArrayOutputStream();
            entradasService.generaEntrada(uuid, attach, user.getUsuario(), urlPublic, urlPieEntrada);
            if (attach != null && attach.size() > 0) {
                byte[] pdf = attach.toByteArray();
                MimeBodyPart attachPart = new MimeBodyPart();
                attachPart.setFileName("entrada.pdf");
                attachPart.setContent(pdf, "application/pdf");
                multipart.addBodyPart(attachPart);
            }
            attach.close();

            if (mailDao.getMailByUUID(uuid).getFechaEnviado() == null) {
                send(message);
                mailDao.marcaEnviado(uuid);
            }
        } catch (NullPointerException e) {
            log.error("Error enviando mail multipart (elemento nulo)", e);
            throw new MessagingException();
        } catch (Exception me) {
            if (!para.contains("4tic.com")) {
                try {
                    log.warn("Error enviando mail multipart, se procede al envío de mail sin adjunto", me);
                    enviaMailTexto(de, para, titulo, texto, uuid);
                }
                 catch (Exception e) {
                    log.error("Error enviando mail", me);
                    throw new MessagingException();
                }
            }
        }
    }

    public void enviaMailTexto(
        String de,
        String para,
        String titulo,
        String texto,
        String uuid
    ) throws MessagingException {
            if (mailDao.getMailByUUID(uuid).getFechaEnviado() == null) {
                enviaMail(de, para, titulo, texto);
                mailDao.marcaEnviado(uuid);
            }
    }

    private void send(Message message) throws MessagingException {
        Instant start = Instant.now();
        if (configuration.isDebug()) {
            String tmpdir = System.getProperty("java.io.tmpdir");
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy_hhmmss_SSS");
            try {
                message.writeTo(new FileOutputStream(String.format("%s/email_%s.eml", tmpdir, sdf.format(new Date()))));
            } catch (IOException e) {
                log.warn("No se ha podido guardar el mail en formato EML", e);
            }
        }
        else {
            Transport.send(message);
        }
        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        log.info(String.format("Tiempo ejecutando Transport.send: %s seg.", timeElapsed / 1000));
    }

    private MimeMultipart createMailBodyMessage(String texto) throws MessagingException {
        MimeMultipart multipart = new MimeMultipart();

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(texto, "text/html; charset=UTF-8");
        multipart.addBodyPart(messageBodyPart);

        return multipart;
    }

    public synchronized void enviaPendientes(
        MailDAO mailDAO,
        EntradasService entradasService,
        UsersService usersService,
        Configuration configuration
    ) throws MessagingException {
        this.mailDao = mailDAO;
        this.configuration = configuration;

        log.info("** - Enviando mails pendientes desde JavaHtmlService...");
        List<MailDTO> mails = mailDAO.getMailsPendientes();
        for (MailDTO mail : mails) {
            enviaMailMultipart(mail.getDe(), mail.getPara(), mail.getTitulo(), mail.getTexto(), mail.getUuid(),
                mail.getUrlPublic(), mail.getUrlPieEntrada(), entradasService, usersService);
        }

        log.info("** - Enviando alertas pendientes desde JavaHtmlService...");
        List<MailDTO> alertas = mailDAO.getAlertasPendientes();
        for (MailDTO alerta : alertas) {
            enviaMailTexto(alerta.getDe(), alerta.getPara(), alerta.getTitulo(), alerta.getTexto(), alerta.getUuid());
        }
    }
}
