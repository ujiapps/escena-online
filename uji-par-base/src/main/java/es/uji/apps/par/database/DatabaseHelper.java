package es.uji.apps.par.database;

import java.math.BigDecimal;

public interface DatabaseHelper {
    String paginate(
        int start,
        int limit,
        String sql
    );

    Integer castId(Object id);

    Boolean castBoolean(Object value);

    BigDecimal castBigDecimal(Object value);

    String caseString(
        String condicion,
        String[] condicionesValores
    );

    String trueString();

    String falseString();

    String trunc(
        String campo,
        String formato
    );

    int booleanToNumber(Object valor);

    String toInteger(String columna);

    String toDate();

    String isNotEmptyString(String columna);

    String dateFormatter();

    String dateHourSecondsFormatter();
}