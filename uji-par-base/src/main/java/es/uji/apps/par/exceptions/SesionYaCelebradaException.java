package es.uji.apps.par.exceptions;

@SuppressWarnings("serial")
public class SesionYaCelebradaException extends GeneralPARException {

	public SesionYaCelebradaException(long sesionId) {
		super(SESION_YA_CELEBRADA_CODE, SESION_YA_CELEBRADA + String.format("sesionId = %d", sesionId));
	}
}
