package es.uji.apps.par.services;

import com.google.common.base.Strings;

import com.sun.jersey.core.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.uji.apps.par.dao.MailDAO;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.mails.MailEntrada;
import es.uji.apps.par.model.ButacaPassbook;
import es.uji.apps.par.services.rest.BaseResource;
import es.uji.apps.par.utils.DateUtils;
import es.uji.commons.web.template.HTMLTemplate;

@Service
public class MailComposerService {
    public final static String COVID_AVISO_BENIN = "Magia por Benín Duodécima Edición: Estimados espectadores, Magia por Benín vuelve en abril de 2022 a los escenarios de Madrid. Os rogamos que intentéis acudir al espectáculo a 30 minutos antes del comienzo del show y que hagáis cola respetando la distancia de seguridad en la Sala de la Máquina de la Escuela de Industriales de la Politécnica de Madrid (c/ José Gutiérrez Abascal, 2, 28006, Madrid) hasta la apertura de puertas, que se hará aproximadamente 15 minutos antes del espectáculo.\n\n";
    public final static String COVID_AVISO_BENIN2 = "Muchas gracias. La Organización\n";

    @Autowired
    private MailDAO mailDAO;

    public boolean isEmailInBlacklist(String email) {
        return !Strings.isNullOrEmpty(email) && mailDAO.isEmailInBlacklist(email);
    }

    public boolean registraMail(
        CompraDTO compra,
        String email,
        String uuid,
        Locale locale
    ) {
        if (!isEmailInBlacklist(email) && mailDAO.getMailByUUID(compra.getUuid()) == null) {
            CineDTO cine = compra.getParSesion().getParSala().getParCine();
            List<ButacaDTO> butacasDTO = compra.getParButacas();

            String urlEntradas = String.format("%s/rest/compra/%s/pdf", cine.getUrlPublic(), uuid);
            String titulo = String.format("%s %s", ResourceProperties.getProperty(locale, "mail.entradas.titulo"), cine.getNombre());
            String texto = ResourceProperties.getProperty(locale, "mail.entradas.texto", urlEntradas);
            try {
                MailEntrada mailEntrada = new MailEntrada();
                Map<String, Object> properties = new HashMap<>();
                properties.put("titulo", titulo);
                properties.put("localizacionLabel", ResourceProperties.getProperty(locale,"mail.entradas.localizacion"));
                properties.put("cabecera", ResourceProperties.getProperty(locale, "mail.entradas.cabecera.html"));
                properties.put("mensaje", ResourceProperties.getProperty(locale, "mail.entradas.mensaje.html"));
                properties.put("info", ResourceProperties.getProperty(locale, "mail.entradas.info.html"));
                properties.put("eventoTxt", ResourceProperties.getProperty(locale, "mail.entradas.evento.html"));
                properties.put("evento", compra.getParSesion().getParEvento().getTituloEs());
                properties.put("salaTxt", ResourceProperties.getProperty(locale, "mail.entradas.sala.html"));
                properties.put("sala", compra.getParSesion().getParSala().getNombre());
                properties.put("fechaTxt", ResourceProperties.getProperty(locale, "mail.entradas.fecha.html"));
                properties.put("fecha", DateUtils.dateToSpanishString(compra.getParSesion().getFechaCelebracion()));
                properties.put("enlace", ResourceProperties.getCustomProperty(locale, cine.getCodigo(),"mail.entradas.enlace.html"));
                properties.put("horaTxt", ResourceProperties.getProperty(locale, "mail.entradas.hora.html"));
                properties.put("hora", compra.getParSesion().getHoraApertura());
                properties.put("horaSesionTxt", ResourceProperties.getProperty(locale, "mail.entradas.horaSesion.html"));
                properties.put("horaSesion", DateUtils.dateToHourString(compra.getParSesion().getFechaCelebracion()));
                properties.put("imprimir", ResourceProperties.getProperty(locale, "mail.entradas.imprimir.html"));
                properties.put("precabecera", ResourceProperties.getProperty(locale, "mail.entradas.precabecera.html"));
                properties.put("src", urlEntradas);

                String nombrePlantilla = Constantes.PLANTILLAS_DIR + "mail";
                EventoDTO evento = compra.getParSesion().getParEvento();
                if (evento.getIsabono() != null && evento.getIsabono()) {
                    properties.put("mensaje", ResourceProperties.getProperty(locale, "mail.abonos.mensaje.html"));
                    properties.put("imprimir", ResourceProperties.getProperty(locale, "mail.abonos.imprimir.html"));
                    properties.put("precabecera", ResourceProperties.getProperty(locale, "mail.abonos.precabecera.html"));

                    nombrePlantilla += "_abono";
                } else if (isPassbookActivado(cine)) {
                    nombrePlantilla += "_passbook";
                    addPassbookProperties(compra, butacasDTO, locale, properties);
                }

                HTMLTemplate template = new HTMLTemplate(nombrePlantilla, locale, BaseResource.APP);
                texto = mailEntrada.compose(template, properties);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mailDAO.insertaMail(cine.getMailFrom(), email, titulo, texto, uuid, cine.getUrlPublic(), cine.getUrlPieEntrada());
            return true;
        }
        else {
            return false;
        }
    }

    private boolean isPassbookActivado(CineDTO cineDTO) {
        return cineDTO.getPassbookActivado() != null && cineDTO.getPassbookActivado();
    }

    private void addPassbookProperties(
        CompraDTO compra,
        List<ButacaDTO> butacasDTO,
        Locale locale,
        Map<String, Object> properties
    ) {
        CineDTO cine = compra.getParSesion().getParEvento().getParCine();

        List<ButacaPassbook> butacaPassbooks = new ArrayList<>();
        for (ButacaDTO butaca : butacasDTO) {
            ButacaPassbook butacaPassbook = new ButacaPassbook(butaca, locale, compra.getUuid(), cine.getUrlPublic());
            if (cine.isAnulableDesdeEnlace() && butaca.getPrecio().compareTo(BigDecimal.ZERO) == 0) {
                butacaPassbook.setEnlaceCancelacion(compra.getEmail());
            }
            butacaPassbooks.add(butacaPassbook);
        }
        if (cine.isAnulableDesdeEnlace() && butacasDTO.stream().map(ButacaDTO::getPrecio).reduce(BigDecimal.ZERO, BigDecimal::add).compareTo(BigDecimal.ZERO) == 0) {
            String enlaceCancelacionCompra = String.format("%s/rest/compra/%s/%s/anular", cine.getUrlPublic(),  compra.getUuid(), new String(Base64.encode(compra.getEmail())));
            properties.put("enlaceCancelacionCompra", enlaceCancelacionCompra);
        }
        properties.put("butacas", butacaPassbooks);
        properties.put("filaLabel", ResourceProperties.getProperty(locale, "entrada.filaS"));
        properties.put("butacaLabel", ResourceProperties.getProperty(locale, "entrada.butacaSimpleS"));
        properties.put("botonPassbook", ResourceProperties.getProperty(locale, "mail.entradas.botonPassbook.html"));
        properties.put("botonCancelar", ResourceProperties.getProperty(locale, "mail.entradas.botonCancelar.html"));
        properties.put("botonCancelacionCompra", ResourceProperties.getProperty(locale, "mail.entradas.botonCancelarCompra.html"));
        String textoPassbook = ResourceProperties.getProperty(locale, "mail.entradas.textoPassbook.html");
        if (compra.getParSesion().getParSala().getParCine().getUrlPublic().contains("magiaporbenin.escenaonline.es")) {
            textoPassbook = String.format("<p>%s</p><p>%s</p><p>%s</p>", COVID_AVISO_BENIN, COVID_AVISO_BENIN2, textoPassbook);
        }
        properties.put("textoPassbook", textoPassbook);
    }
}
