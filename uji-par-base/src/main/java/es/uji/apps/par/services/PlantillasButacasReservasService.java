package es.uji.apps.par.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.par.dao.PlantillasReservasDAO;
import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.model.ButacaMap;

@Service
public class PlantillasButacasReservasService {

    @Autowired
    private PlantillasReservasDAO plantillasReservasDAO;


    public List<Butaca> getButacasOfPlantilla(
        Long plantillaId,
        String sort,
        int start,
        int limit,
        String language
    ) {
        return plantillasReservasDAO.getButacasOfPlantilla(plantillaId, sort, start, limit).stream()
            .map(b -> Butaca.plantillaButacaReservaDTOToButaca(b, language)).collect(Collectors.toList());
    }

    public List<ButacaMap> getButacasMapOfPlantilla(
        Long plantillaId,
        String sort,
        int start,
        int limit,
        String language
    ) {
        return plantillasReservasDAO.getButacasOfPlantilla(plantillaId, sort, start, limit).stream()
            .map(b -> ButacaMap.plantillaButacaReservaDTOToButacaMap(b, language)).collect(Collectors.toList());
    }

    public int getTotalButacasOfPlantilla(Long plantillaId) {
        return (int) plantillasReservasDAO.getTotalButacasOfPlantilla(plantillaId);
    }

    public void actualizaButacasPlantilla(
        Long plantillaId,
        List<Butaca> butacasSeleccionadas
    ) {
        plantillasReservasDAO.removeButacasFromPlantilla(plantillaId);
        plantillasReservasDAO.guardaButacasByPlantilla(plantillaId,
            butacasSeleccionadas.stream().filter(b -> b.getCantidad() == null).collect(Collectors.toList()));
    }
}
