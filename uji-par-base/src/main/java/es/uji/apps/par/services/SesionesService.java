package es.uji.apps.par.services;

import com.google.common.base.Strings;

import com.mysema.query.Tuple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import es.uji.apps.par.dao.ComprasDAO;
import es.uji.apps.par.dao.EventosDAO;
import es.uji.apps.par.dao.LocalizacionesDAO;
import es.uji.apps.par.dao.SesionesDAO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.PreciosSesionDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.EdicionSesionAnuladaException;
import es.uji.apps.par.exceptions.EventoConCompras;
import es.uji.apps.par.exceptions.FechasInvalidasException;
import es.uji.apps.par.exceptions.IncidenciaNotFoundException;
import es.uji.apps.par.exceptions.PrecioRepetidoException;
import es.uji.apps.par.exceptions.SalaConSesionesMismaHoraException;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.model.Ocupacion;
import es.uji.apps.par.model.PreciosEditablesSesion;
import es.uji.apps.par.model.PreciosPlantilla;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.Tarifa;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.Pair;

@Service
public class SesionesService {
    @Autowired
    private SesionesDAO sesionDAO;

    @Autowired
    private EventosDAO eventosDAO;

    @Autowired
    private ComprasDAO comprasDAO;

    @Autowired
    private LocalizacionesDAO localizacionesDAO;

    @Autowired
    private PreciosPlantillaService preciosPlantillaService;

    public List<Sesion> getSesiones(
        Long eventoId,
        String sortParameter,
        int start,
        int limit,
        String userUID
    ) {
        return getSesiones(eventoId, false, sortParameter, start, limit, userUID);
    }

    public Ocupacion getSesionConVendida(
        long id,
        String userId
    ) {
        SesionDTO sesion = sesionDAO.getSesion(id, userId);
        return getSesionesConVendidasDateEnSegundos(sesion.getParEvento().getId(), null, 0, Integer.MAX_VALUE, null,
            userId).stream().filter(s -> s.getId() == id).map(Ocupacion::new).findFirst().orElse(null);
    }

    private List<Sesion> getSesionesConVendidas(
        Long eventoId,
        boolean activas,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID
    ) {
        List<Sesion> listaSesiones = new ArrayList<>();

        List<Tuple> sesiones;
        if (activas) {
            sesiones =
                sesionDAO.getSesionesActivasConButacasVendidas(eventoId, sortParameter, start, limit, filter, userUID);
        } else {
            sesiones = sesionDAO.getSesionesConButacasVendidas(eventoId, sortParameter, start, limit, filter, userUID);
        }

        for (Tuple fila : sesiones) {

            SesionDTO sesionDTO = fila.get(0, SesionDTO.class);
            Long butacasVendidas = fila.get(1, Long.class);
            Long butacasReservadas = fila.get(2, Long.class);
            Long butacasTotales = fila.get(3, Long.class);

            Sesion sesion = new Sesion(sesionDTO);
            sesion.setButacasVendidas(butacasVendidas);
            sesion.setButacasReservadas(butacasReservadas);
            sesion.setButacasDisponibles(butacasTotales - butacasReservadas - butacasVendidas);

            listaSesiones.add(sesion);
        }

        return listaSesiones;
    }

    public List<Sesion> getSesionesPorSala(
        Long eventoId,
        Long salaId,
        String sortParameter,
        String userUID
    ) {

        List<Sesion> listaSesiones = new ArrayList<Sesion>();
        List<SesionDTO> sesiones = sesionDAO.getSesionesPorSala(eventoId, salaId, sortParameter, userUID);

        for (SesionDTO sesionDB : sesiones) {
            listaSesiones.add(new Sesion(sesionDB));
        }
        return listaSesiones;
    }

    public List<Sesion> getSesiones(
        Long eventoId,
        String userUID
    ) {
        return getSesiones(eventoId, false, "", 0, Integer.MAX_VALUE, userUID);
    }

    public List<Sesion> getSesionesActivas(
        Long eventoId,
        String sortParameter,
        int start,
        int limit,
        String userUID
    ) {
        return getSesiones(eventoId, true, sortParameter, start, limit, userUID);
    }

    private List<Sesion> getSesiones(
        Long eventoId,
        boolean activos,
        String sortParameter,
        int start,
        int limit,
        String userUID
    ) {
        List<Sesion> listaSesiones = new ArrayList<Sesion>();

        List<SesionDTO> sesiones;

        if (activos)
            sesiones = sesionDAO.getSesionesActivas(eventoId, sortParameter, start, limit, userUID);
        else
            sesiones = sesionDAO.getSesiones(eventoId, sortParameter, start, limit, userUID);

        for (SesionDTO sesionDB : sesiones) {
            listaSesiones.add(new Sesion(sesionDB));
        }
        return listaSesiones;
    }

    public List<Sesion> getSesionesPorRssId(
        String rssId,
        String userUID
    ) {
        List<Sesion> listaSesiones = new ArrayList<Sesion>();
        List<SesionDTO> sesiones = sesionDAO.getSesionesPorRssId(rssId, userUID);

        for (SesionDTO sesionDB : sesiones) {
            listaSesiones.add(new Sesion(sesionDB));
        }
        return listaSesiones;
    }

    public List<Sesion> getSesionesConVendidasDateEnSegundos(
        Long eventoId,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID
    ) {
        return getSesionesConVendidasDateEnSegundos(eventoId, false, sortParameter, start, limit, filter, userUID);
    }

    public List<Sesion> getSesionesActivasConVendidasDateEnSegundos(
        Long eventoId,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID
    ) {
        return getSesionesConVendidasDateEnSegundos(eventoId, true, sortParameter, start, limit, filter, userUID);
    }

    public List<Sesion> getSesionesConVendidasDateEnSegundos(
        Long eventoId,
        boolean activas,
        String sortParameter,
        int start,
        int limit,
        ExtGridFilterList filter,
        String userUID
    ) {
        List<Sesion> sesiones = getSesionesConVendidas(eventoId, activas, sortParameter, start, limit, filter, userUID);

        for (Sesion sesion : sesiones) {
            if (sesion.isMultisesion()) {
                List<SesionDTO> multiplesSesionesBySesionId = sesionDAO.getMultiplesSesionesBySesionId(sesion.getId());
                sesion.setFechaCelebracionLabel(multiplesSesionesBySesionId.stream()
                    .map(s -> DateUtils.dateToSpanishString(s.getFechaCelebracion()))
                    .collect(Collectors.joining(", ")));
            } else {
                sesion.setFechaCelebracionLabel(DateUtils.dateToSpanishStringWithHour(sesion.getFechaCelebracion()));
            }
            sesion.setFechaCelebracionWithDate(new Date(sesion.getFechaCelebracion().getTime() / 1000));

            if (sesion.getFechaInicioVentaOnline() != null)
                sesion.setFechaInicioVentaOnlineWithDate(new Date(sesion.getFechaInicioVentaOnline().getTime() / 1000));
            if (sesion.getFechaFinVentaOnline() != null)
                sesion.setFechaFinVentaOnlineWithDate(new Date(sesion.getFechaFinVentaOnline().getTime() / 1000));
            if (sesion.getFechaFinVentaAnticipada() != null)
                sesion.setFechaFinVentaAnticipadaWithDate(
                    new Date(sesion.getFechaFinVentaAnticipada().getTime() / 1000));
        }

        return sesiones;
    }

    public void anulaSesion(Integer id) {
        if (comprasDAO.hasComprasBySesion(id)) {
            throw new EventoConCompras(id);
        } else {
            sesionDAO.anulaSesion(id);
        }
    }

    @Transactional(rollbackForClassName = {"CampoRequeridoException", "FechasInvalidasException",
        "IncidenciaNotFoundException"})
    public Sesion addSesion(
        long eventoId,
        Sesion sesion,
        boolean anulaConMismaHoraYSala,
        String userUID
    ) throws CampoRequeridoException, FechasInvalidasException, IncidenciaNotFoundException {
        sesion.setEvento(Evento.eventoDTOtoEvento(eventosDAO.getEventoById(eventoId, userUID)));
        sesion.setPreciosSesion(createPreciosSesion(sesion));
        checkSesionAndDates(sesion);
        creaSesionConAnulacionDeSesionesMismaHora(sesion, anulaConMismaHoraYSala, userUID);
        return sesion;
    }

    @Transactional(rollbackForClassName = {"CampoRequeridoException", "FechasInvalidasException",
        "IncidenciaNotFoundException"})
    public void addSesionesPeriodicas(
        long eventoId,
        Sesion sesion,
        boolean anulaConMismaHoraYSala,
        String userUID
    ) {
        sesion.setEvento(Evento.eventoDTOtoEvento(eventosDAO.getEventoById(eventoId, userUID)));
        sesion.setPreciosSesion(createPreciosSesion(sesion));

        LocalDate fechaInicio = sesion.getFechaInicio().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate fechaFin = sesion.getFechaFin().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        while (!fechaInicio.isAfter(fechaFin)) {
            if (haySesionEsteDia(sesion, fechaInicio)) {
                Date fechaCelebracion = Date.from(fechaInicio.atStartOfDay(ZoneId.systemDefault()).toInstant());
                sesion.setFechaCelebracionWithDate(fechaCelebracion);

                if (sesion.getCanalInternet()) {
                    setFechasByDia(sesion, fechaInicio);
                }

                checkSesionAndDates(sesion);
                creaSesionConAnulacionDeSesionesMismaHora(sesion, anulaConMismaHoraYSala, userUID);
            }

            fechaInicio = fechaInicio.plusDays(1);
        }
    }

    @Transactional
    public Sesion creaSesionConAnulacionDeSesionesMismaHora(
        Sesion sesion,
        boolean anulaConMismaHoraYSala,
        String userUID
    ) {
        SesionDTO sesionDTO = sesionDAO.persistSesion(Sesion.SesionToSesionDTO(sesion));
        anulaSesionesConLaMismaHoraYSala(sesionDTO, anulaConMismaHoraYSala, userUID);
        return Sesion.SesionDTOToSesion(sesionDTO);
    }

    private boolean haySesionEsteDia(
        Sesion sesion,
        LocalDate fechaInicio
    ) {
        return sesion.getDias().contains(Long.valueOf(fechaInicio.getDayOfWeek().getValue()));
    }

    private void setFechasByDia(
        Sesion sesion,
        LocalDate fechaInicio
    ) {
        String horaCelebracionString = sesion.getHoraCelebracion();
        String[] horaCelebracionStringSeparada = horaCelebracionString.split(":");
        int hora = Integer.parseInt(horaCelebracionStringSeparada[0]);
        int minutos = Integer.parseInt(horaCelebracionStringSeparada[1]);
        LocalDateTime fechaHoraSesion =
            LocalDateTime.of(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDayOfMonth(), hora, minutos);
        LocalDateTime fechaHoraCierreOnline = fechaHoraSesion.minusHours(sesion.getHorasCierreOnline());
        Date fechaCierreOnline = Date.from(fechaHoraCierreOnline.atZone(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm");
        sesion.setFechaFinVentaOnline(sdf.format(fechaCierreOnline));
        sesion.setHoraFinVentaOnline(sdfHora.format(fechaCierreOnline));
    }

    private List<PreciosEditablesSesion> createPreciosSesion(Sesion sesion) {
        List<PreciosEditablesSesion> listaPreciosSesion = new ArrayList<>();
        if (sesion.getPreciosSesion() != null) {
            for (PreciosEditablesSesion preciosSesion : sesion.getPreciosSesion()) {
                long preciosConMismaTarifaYLocalizacion = listaPreciosSesion.stream().filter(
                    ps -> ps.getTarifa().getId() == preciosSesion.getTarifa().getId()
                        && ps.getLocalizacion().getId() == preciosSesion.getLocalizacion().getId()).count();

                if (preciosConMismaTarifaYLocalizacion > 0) {
                    throw new PrecioRepetidoException();
                }

                preciosSesion.setLocalizacion(Localizacion.localizacionDTOtoLocalizacion(
                    localizacionesDAO.getLocalizacionById(preciosSesion.getLocalizacion().getId())));
                preciosSesion.setSesion(sesion);
                listaPreciosSesion.add(preciosSesion);
            }
            return listaPreciosSesion;
        }

        return null;
    }

    private void addPreciosSesion(SesionDTO sesionDTO) {
        List<PreciosSesionDTO> listaPreciosSesion = new ArrayList<>();
        if (sesionDTO.getParPreciosSesions() != null) {
            for (PreciosSesionDTO precioSesionDTO : sesionDTO.getParPreciosSesions()) {
                long preciosConMismaTarifaYLocalizacion = listaPreciosSesion.stream().filter(
                    ps -> ps.getParTarifa().getId() == precioSesionDTO.getParTarifa().getId()
                        && ps.getParLocalizacione().getId() == precioSesionDTO.getParLocalizacione().getId()).count();

                if (preciosConMismaTarifaYLocalizacion > 0) {
                    throw new PrecioRepetidoException();
                }

                listaPreciosSesion.add(precioSesionDTO);
                precioSesionDTO.setParSesione(sesionDTO);
                sesionDAO.addPrecioSesion(precioSesionDTO);
            }
        }
    }

    private Evento createParEventoWithId(long eventoId) {
        Evento evento = new Evento();
        evento.setId(eventoId);
        return evento;
    }

    private void checkSesionAndDates(Sesion sesion) throws CampoRequeridoException, FechasInvalidasException {
        checkRequiredFields(sesion);

        Date fechaCelebracion = DateUtils.addTimeToDate(sesion.getFechaCelebracion(), sesion.getHoraCelebracion());

        if (sesion.getCanalInternet()) {
            Date fechaInicioVentaOnline =
                DateUtils.addTimeToDate(sesion.getFechaInicioVentaOnline(), sesion.getHoraInicioVentaOnline());
            Date fechaFinVentaOnline =
                DateUtils.addTimeToDate(sesion.getFechaFinVentaOnline(), sesion.getHoraFinVentaOnline());

            checkIfDatesAreValid(fechaInicioVentaOnline, fechaFinVentaOnline, fechaCelebracion);
        }
    }

    @Transactional(rollbackForClassName = {"CampoRequeridoException", "FechasInvalidasException",
        "IncidenciaNotFoundException"})
    public void updateSesion(
        long eventoId,
        Sesion sesion,
        boolean anulaConMismaHoraYSala,
        boolean cambiaFechaConCompras,
        String userUID
    ) throws CampoRequeridoException, FechasInvalidasException, IncidenciaNotFoundException {
        checkSesionAndDates(sesion);
        sesion.setEvento(createParEventoWithId(eventoId));

        sesionDAO.deleteExistingPreciosSesion(sesion.getId());
        SesionDTO sesionDTO = sesionDAO.getSesion(sesion.getId(), userUID);
        if (isSesionAnulada(sesionDTO)) {
            throw new EdicionSesionAnuladaException();
        } else {
            sesionDTO = sesionDAO.updateSesion(sesionDTO, sesion, comprasDAO.hasComprasBySesion(sesion.getId()),
                cambiaFechaConCompras);
            anulaSesionesConLaMismaHoraYSala(sesionDTO, anulaConMismaHoraYSala, userUID);
            addPreciosSesion(Sesion.SesionToSesionDTO(sesion));
        }
    }

    private void checkIfDatesAreValid(
        Date fechaInicio,
        Date fechaFin,
        Date fechaCelebracion
    ) throws FechasInvalidasException {
        if (DateUtils.millisecondsToSeconds(fechaFin.getTime()) < DateUtils.millisecondsToSeconds(
            fechaInicio.getTime()))
            throw new FechasInvalidasException(FechasInvalidasException.FECHA_INICIO_VENTA_POSTERIOR_FECHA_FIN_VENTA,
                fechaInicio, fechaFin);

        if (DateUtils.millisecondsToSeconds(fechaFin.getTime()) > DateUtils.millisecondsToSeconds(
            fechaCelebracion.getTime()))
            throw new FechasInvalidasException(FechasInvalidasException.FECHA_FIN_VENTA_POSTERIOR_FECHA_CELEBRACION,
                fechaCelebracion, fechaFin);
    }

    private void checkRequiredFields(Sesion sesion) throws CampoRequeridoException {
        if (sesion.getFechaCelebracion() == null)
            throw new CampoRequeridoException("Fecha de celebración");
        if (sesion.getHoraCelebracion() == null)
            throw new CampoRequeridoException("Hora de celebración");

        if (sesion.getCanalInternet()) {
            if (sesion.getFechaInicio() == null) {
                if (sesion.getFechaInicioVentaOnline() == null)
                    throw new CampoRequeridoException("Fecha de inicio de la venta online");
                if (sesion.getFechaFinVentaOnline() == null)
                    throw new CampoRequeridoException("Fecha de fin de la venta online");
                if (sesion.getHoraInicioVentaOnline() == null)
                    throw new CampoRequeridoException("Hora de inicio de la venta online");
                if (sesion.getHoraFinVentaOnline() == null)
                    throw new CampoRequeridoException("Hora de fin de la venta online");
            } else {
                if (sesion.getFechaInicioVentaOnline() == null)
                    throw new CampoRequeridoException("Fecha de inicio de la venta online");
                if (sesion.getHoraInicioVentaOnline() == null)
                    throw new CampoRequeridoException("Hora de inicio de la venta online");
                if (sesion.getHorasCierreOnline() == null)
                    throw new CampoRequeridoException("Número de horas para el cierre de venta online");
            }
        }

        if (sesion.getDisponibleVentaAnticipada()) {
            if (sesion.getFechaFinVentaAnticipada() == null) {
                throw new CampoRequeridoException("Fecha de fin de la venta anticipada");
            }
            if (sesion.getHoraFinVentaAnticipada() == null) {
                throw new CampoRequeridoException("Hora de fin de la venta anticipada");
            }
        }
    }

    private boolean mostrarTarifa(
        TarifaDTO tarifa,
        boolean mostrarTarifasInternas
    ) {
        if ((tarifa.getIsPublica() == null && !mostrarTarifasInternas) || (tarifa.getIsPublica() != null
            && !tarifa.getIsPublica() && !mostrarTarifasInternas))
            return false;
        else
            return true;
    }

    public List<PreciosEditablesSesion> getPreciosEditablesSesion(
        Long sesionId,
        String sortParameter,
        int start,
        int limit,
        boolean mostrarTarifasInternas,
        String userUID
    ) {
        List<PreciosEditablesSesion> listaPreciosSesion = new ArrayList<PreciosEditablesSesion>();

        Sesion sesion = getSesion(sesionId, userUID);

        if (sesion.getPlantillaPrecios().getId() == -1) {
            for (PreciosSesionDTO precioSesionDB : sesionDAO.getPreciosSesion(sesionId, sortParameter, start, limit)) {
                if (mostrarTarifa(precioSesionDB.getParTarifa(), mostrarTarifasInternas))
                    listaPreciosSesion.add(new PreciosEditablesSesion(precioSesionDB));
            }
        } else {
            List<PreciosPlantilla> preciosPlantilla =
                preciosPlantillaService.getPreciosOfPlantilla(sesion.getPlantillaPrecios().getId(), sortParameter,
                    start, limit);

            for (PreciosPlantilla precioPlantilla : preciosPlantilla) {
                if (!precioPlantilla.getTarifa().getIsPublica().equals("on") && !mostrarTarifasInternas)
                    continue;

                PreciosEditablesSesion preciosEditablesSesion = new PreciosEditablesSesion(precioPlantilla);
                preciosEditablesSesion.setSesion(sesion);
                listaPreciosSesion.add(preciosEditablesSesion);
            }
        }

        return listaPreciosSesion;
    }

    public List<PreciosEditablesSesion> getPreciosEditablesSesion(
        Long sesionId,
        String userUID
    ) {
        return getPreciosEditablesSesion(sesionId, "", 0, Integer.MAX_VALUE, true, userUID);
    }

    public List<PreciosEditablesSesion> getPreciosEditablesSesionPublicos(
        Long sesionId,
        String userUID
    ) {
        return getPreciosEditablesSesion(sesionId, "", 0, Integer.MAX_VALUE, false, userUID);
    }

    public Sesion getSesion(
        long id,
        String userUID
    ) {
        return new Sesion(sesionDAO.getSesion(id, userUID));
    }

    public int getTotalSesionesActivas(
        Long eventoId,
        ExtGridFilterList filter,
        String userUID
    ) {
        return sesionDAO.getTotalSesionesActivas(eventoId, filter, userUID);
    }

    public int getTotalSesiones(
        Long eventoId,
        ExtGridFilterList filter,
        String userUID
    ) {
        return sesionDAO.getTotalSesiones(eventoId, filter, userUID);
    }

    public int getTotalPreciosSesion(Long sesionId) {
        return sesionDAO.getTotalPreciosSesion(sesionId);
    }

    private List<Sesion> getSesionesPorFechas(
        List<SesionDTO> sesionesDTO,
        boolean conEnvios
    ) {
        List<Sesion> listaSesiones = new ArrayList<Sesion>();

        for (SesionDTO sesionDTO : sesionesDTO) {
            Sesion sesion = Sesion.SesionDTOToSesion(sesionDTO);
            if (sesion.isMultisesion()) {
                List<SesionDTO> multiplesSesionesBySesionId = sesionDAO.getMultiplesSesionesBySesionId(sesion.getId());
                sesion.setFechaCelebracionLabel(multiplesSesionesBySesionId.stream()
                    .map(s -> DateUtils.dateToSpanishString(s.getFechaCelebracion()))
                    .collect(Collectors.joining(", ")));
            } else {
                sesion.setFechaCelebracionLabel(DateUtils.dateToSpanishStringWithHour(sesion.getFechaCelebracion()));
            }
            sesion.setFechaCelebracionWithDate(new Date(sesion.getFechaCelebracion().getTime() / 1000));

            if (conEnvios) {
                if (sesionDTO.getParEnviosSesion().size() > 0) {
                    sesion.setFechaGeneracionFichero(new Date(
                        sesionDTO.getParEnviosSesion().get(0).getParEnvio().getFechaGeneracionFichero().getTime()
                            / 1000));

                    if (sesionDTO.getParEnviosSesion().get(0).getParEnvio().getFechaEnvioFichero() != null)
                        sesion.setFechaEnvioFichero(new Date(
                            sesionDTO.getParEnviosSesion().get(0).getParEnvio().getFechaEnvioFichero().getTime()
                                / 1000));
                    sesion.setTipoEnvio(sesionDTO.getParEnviosSesion().get(0).getTipoEnvio());
                    sesion.setIdEnvioFichero(sesionDTO.getParEnviosSesion().get(0).getParEnvio().getId());
                    sesion.setIncidenciaId(sesionDTO.getIncidenciaId());
                }
            }
            listaSesiones.add(sesion);
        }
        return listaSesiones;
    }

    public List<Sesion> getSesionesCinePorFechas(
        String fechaInicio,
        String fechaFin,
        boolean icaa,
        String sort,
        String userUID
    ) {
        Date dtInicio = DateUtils.spanishStringToDate(fechaInicio);
        Date dtFin = DateUtils.spanishStringToDate(fechaFin);
        dtFin = DateUtils.addTimeToDate(dtFin, "23:59");
        List<SesionDTO> sesionesDTO = sesionDAO.getSesionesCinePorFechas(dtInicio, dtFin, icaa, sort, userUID);
        return getSesionesPorFechas(sesionesDTO, false);
    }

    public List<Sesion> getSesionesICAAPorFechas(
        String fechaInicio,
        String fechaFin,
        String sort,
        String userUID
    ) {
        Date dtInicio = DateUtils.spanishStringToDate(fechaInicio);
        Date dtFin = DateUtils.spanishStringToDate(fechaFin);
        dtFin = DateUtils.addTimeToDate(dtFin, "23:59");
        List<SesionDTO> sesionesDTO = sesionDAO.getSesionesICAAPorFechas(dtInicio, dtFin, sort, userUID);
        return getSesionesPorFechas(sesionesDTO, true);
    }

    public List<Sesion> getSesionesPorFechas(
        String fechaInicio,
        String fechaFin,
        String sort,
        String userUID
    ) {
        Date dtInicio = DateUtils.spanishStringToDate(fechaInicio);
        Date dtFin = DateUtils.spanishStringToDate(fechaFin);
        dtFin = DateUtils.addTimeToDate(dtFin, "23:59");
        List<SesionDTO> sesionesDTO = sesionDAO.getSesionesPorFechas(dtInicio, dtFin, sort, userUID);
        return getSesionesPorFechas(sesionesDTO, false);
    }

    public List<Tarifa> getTarifasConPrecioSinPlantilla(long sesionId) {
        return _getTarifasConPrecioSinPlantilla(sesionId, null, true);
    }

    public List<Tarifa> getTarifasConPrecioSinPlantillaDeLocalizacion(
        long sesionId,
        long localizacionId
    ) {
        return _getTarifasConPrecioSinPlantilla(sesionId, localizacionId, true);
    }

    public List<Tarifa> getTarifasConPrecioConPlantilla(long sesionId) {
        return _getTarifasConPrecioConPlantilla(sesionId, null, true);
    }

    public List<Tarifa> getTarifasConPrecioConPlantillaDeLocalizacion(
        long sesionId,
        long localizacionId
    ) {
        return _getTarifasConPrecioConPlantilla(sesionId, localizacionId, true);
    }

    public List<Tarifa> getTarifasPublicasConPrecioConPlantilla(long sesionId) {
        return _getTarifasConPrecioConPlantilla(sesionId, null, false);
    }

    public List<Tarifa> getTarifasPublicasConPrecioSinPlantilla(long sesionId) {
        return _getTarifasConPrecioSinPlantilla(sesionId, null, false);
    }

    private List<Tarifa> _getTarifasConPrecioConPlantilla(
        Long sesionId,
        Long localizacionId,
        boolean tambienInternas
    ) {
        List<TarifaDTO> tarifasDTO = sesionDAO.getTarifasPreciosPlantilla(sesionId, localizacionId);
        List<Tarifa> tarifas = new ArrayList<Tarifa>();

        for (TarifaDTO tarifaDTO : tarifasDTO) {
            if (!mostrarTarifa(tarifaDTO, tambienInternas))
                continue;
            Tarifa tarifa = Tarifa.tarifaDTOToTarifa(tarifaDTO);
            tarifas.add(tarifa);
        }
        return tarifas;
    }

    private List<Tarifa> _getTarifasConPrecioSinPlantilla(
        long sesionId,
        Long localizacionId,
        boolean tambienInternas
    ) {
        List<TarifaDTO> tarifasDTO = sesionDAO.getTarifasPreciosSesion(sesionId, localizacionId);
        List<Tarifa> tarifas = new ArrayList<Tarifa>();

        for (TarifaDTO tarifaDTO : tarifasDTO) {
            if (!mostrarTarifa(tarifaDTO, tambienInternas))
                continue;
            Tarifa tarifa = Tarifa.tarifaDTOToTarifa(tarifaDTO);
            tarifas.add(tarifa);
        }
        return tarifas;
    }

    public void setIncidencia(
        long sesionId,
        int incidenciaId
    ) {
        sesionDAO.setIncidencia(sesionId, incidenciaId);
    }

    public Pair getNumeroSesionesMismaHoraYSala(
        Long sesionId,
        long salaId,
        Date fechaCelebracion,
        String userUID
    ) {
        return sesionDAO.getCantidadSesionesMismaFechaYLocalizacion(DateUtils.dateToTimestampSafe(fechaCelebracion),
            salaId, sesionId, userUID);
    }

    public Pair getNumeroSesionesMismaHoraYSalaPeriodica(
        Long sesionId,
        long salaId,
        Sesion sesion,
        String userUID
    ) {
        LocalDate fechaInicio = sesion.getFechaInicio().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate fechaFin = sesion.getFechaFin().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        Pair numeroSesionesMismaHoraYSala = new Pair(0l, 0l);
        while (fechaInicio.isBefore(fechaFin)) {
            if (haySesionEsteDia(sesion, fechaInicio)) {
                String[] horaCelebracionSeparada = sesion.getHoraCelebracion().split(":");
                int hora = Integer.parseInt(horaCelebracionSeparada[0]);
                int minutos = Integer.parseInt(horaCelebracionSeparada[1]);
                LocalDateTime fechaHoraCelebracion =
                    LocalDateTime.of(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDayOfMonth(), hora,
                        minutos);
                Date fechaCelebracion = Date.from(fechaHoraCelebracion.atZone(ZoneId.systemDefault()).toInstant());
                sesion.setFechaCelebracionWithDate(fechaCelebracion);

                Pair numeroSesionesMismaHoraYSala1 =
                    getNumeroSesionesMismaHoraYSala(sesionId, salaId, sesion.getFechaCelebracion(), userUID);
                numeroSesionesMismaHoraYSala.setFirst(
                    numeroSesionesMismaHoraYSala.getFirst() + numeroSesionesMismaHoraYSala1.getFirst());
                numeroSesionesMismaHoraYSala.setSecond(
                    numeroSesionesMismaHoraYSala.getSecond() + numeroSesionesMismaHoraYSala1.getSecond());
            }
            fechaInicio = fechaInicio.plusDays(1);
        }
        return numeroSesionesMismaHoraYSala;
    }

    private void anulaSesionesConLaMismaHoraYSala(
        SesionDTO sesionDTO,
        boolean anulaConMismaHoraYSala,
        String userUID
    ) {
        Sesion sesion = Sesion.SesionDTOToSesion(sesionDTO);
        Timestamp fechaCelebracion = DateUtils.dateToTimestampSafe(
            DateUtils.addTimeToDate(sesion.getFechaCelebracion(), sesion.getHoraCelebracion()));
        EventoDTO evento = eventosDAO.getEventoById(sesion.getEvento().getId(), userUID);
        List<SesionDTO> sesionesMismaHoraYSala =
            sesionDAO.getSesionesMismaFechaYLocalizacion(fechaCelebracion, evento.getDuracion(),
                sesion.getSala().getId());
        for (SesionDTO sesionMismaHoraYSala : sesionesMismaHoraYSala) {
            if (sonDistintaSesion(sesionMismaHoraYSala.getId(), sesion.getId()) && !isSesionAnulada(
                sesionMismaHoraYSala)) {
                if (anulaConMismaHoraYSala) {
                    if (comprasDAO.hasComprasBySesion(sesionMismaHoraYSala.getId())) {
                        throw new EventoConCompras(sesionMismaHoraYSala.getId());
                    }

                    long totalAnuladas = sesionDAO.getButacasAnuladasTotal(sesionMismaHoraYSala.getId());
                    boolean hasVentasDegradadas =
                        sesionDAO.hasVentasDegradadas(sesionMismaHoraYSala.getId(), fechaCelebracion);
                    boolean isSesionReprogramada =
                        sesionDAO.isSesionReprogramada(fechaCelebracion, sesionMismaHoraYSala.getParSala().getId(),
                            sesionMismaHoraYSala.getId(), userUID);
                    int tipoIncidenciaId =
                        sesionDAO.getTipoIncidenciaSesion(totalAnuladas, hasVentasDegradadas, isSesionReprogramada);
                    setIncidencia(sesionMismaHoraYSala.getId(), tipoIncidenciaId);

                    sesionDAO.anulaSesion(sesionMismaHoraYSala.getId());
                } else {
                    throw new SalaConSesionesMismaHoraException();
                }
            }
        }
    }

    public boolean isSesionAnulada(SesionDTO sesionDTO) {
        if (sesionDTO.getCanalInternet() != null && !sesionDTO.getCanalInternet() && sesionDTO.getAnulada() != null
            && sesionDTO.getAnulada())
            return true;
        return false;
    }

    private boolean sonDistintaSesion(
        long sesionId1,
        long sesionId2
    ) {
        return (sesionId1 != sesionId2);
    }

    public List<Timestamp> getFechasMultiplesSesionesBySesionId(long sesionId) {
        return sesionDAO.getMultiplesSesionesBySesionId(sesionId).stream().map(SesionDTO::getFechaCelebracion)
            .collect(Collectors.toList());
    }

    public String getFechaSesion(
        Sesion sesion,
        boolean withTime,
        Locale locale
    ) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat(withTime ? "dd MMMM HH.mm" : "dd MMMM", locale);
        String diaSemana;
        if (hasCustomDescripcionSesion(sesion, locale)) {
            return locale.getLanguage().equals("ca") ? sesion.getDescripcionCa() : sesion.getDescripcionEs();
        } else {
            if (sesion.isMultisesion()) {
                List<Timestamp> fechasMultiplesSesionesBySesionId =
                    getFechasMultiplesSesionesBySesionId(sesion.getId());
                String texto = "";
                for (int i = 0; i < fechasMultiplesSesionesBySesionId.size(); i++) {
                    Timestamp timestamp = fechasMultiplesSesionesBySesionId.get(i);
                    cal.setTime(timestamp);
                    diaSemana =
                        ResourceProperties.getProperty(locale, "dia.abreviado." + cal.get(Calendar.DAY_OF_WEEK));
                    texto += (Strings.isNullOrEmpty(texto) ?
                        "" :
                        getUnionChar(locale, i == fechasMultiplesSesionesBySesionId.size() - 1)) + diaSemana + " "
                        + format.format(cal.getTime());
                }
                return texto;
            } else {
                cal.setTime(sesion.getFechaCelebracion());
                diaSemana = ResourceProperties.getProperty(locale, "dia.abreviado." + cal.get(Calendar.DAY_OF_WEEK));
                return diaSemana + " " + format.format(cal.getTime());
            }
        }
    }

    public static String getUnionChar(
        Locale locale,
        boolean lastUnion
    ) {
        return lastUnion ? String.format(" %s ", ResourceProperties.getProperty(locale, "and")) : ", ";
    }

    private boolean hasCustomDescripcionSesion(
        Sesion sesion,
        Locale locale
    ) {
        return locale.getLanguage().equals("ca") && !Strings.isNullOrEmpty(sesion.getDescripcionCa())
            || !locale.getLanguage().equals("ca") && !Strings.isNullOrEmpty(sesion.getDescripcionEs());
    }
}
