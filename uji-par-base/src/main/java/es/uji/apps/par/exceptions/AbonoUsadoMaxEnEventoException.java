package es.uji.apps.par.exceptions;

@SuppressWarnings("serial")
public class AbonoUsadoMaxEnEventoException extends GeneralPARException {
    String abono;

    public AbonoUsadoMaxEnEventoException(
        String email,
        String abono
    ) {
        super(ABONO_USADO_MAX_EN_EVENTO_CODE, email + " - " + abono);
        this.abono = abono;
    }

    public String getAbono() {
        return abono;
    }
}
