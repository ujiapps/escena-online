package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class AbonadoYaExisteException extends GeneralPARException
{
    public AbonadoYaExisteException(String message)
    {
        super(ABONADO_EXISTE_CODE, ABONADO_EXISTE + ": " + message);
    }
}
