package es.uji.apps.par.exceptions;

@SuppressWarnings("serial")
public class MultiplesButacaAbonoCompraException extends GeneralPARException {

    public MultiplesButacaAbonoCompraException() {
        super(MULTIPLE_BUTACA_ABONO_COMPRA_CODE);
    }
}
