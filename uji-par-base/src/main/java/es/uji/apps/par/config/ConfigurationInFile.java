package es.uji.apps.par.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import es.uji.apps.par.utils.Environment;

public class ConfigurationInFile implements ConfigurationInterface {
	@Override
	public InputStream getPathToFile() throws FileNotFoundException {
		return new FileInputStream(Environment.getParHome() + "/app.properties");
	}
}
