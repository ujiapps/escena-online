package es.uji.apps.par.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.exceptions.GeneralPARException;
import es.uji.apps.par.exceptions.RegistroSerializaException;

@XmlRootElement
public class Sala
{
    private long id;
    private String codigo;
    private String nombre;
    private String direccion;
    private String tipo;
    private String formato;
    private String subtitulo;
    private String htmlTemplateName;
    private Cine cine;
    private boolean asientosNumerados;
    private String urlComoLlegar;
    private String claseEntradaTaquilla;
    private String claseEntradaOnline;
    private boolean aforoPorCompras;

    public Sala()
    {
    }

    public Sala(long id)
    {
        this.id = id;
    }

    public Sala(String nombre)
    {
        this.nombre = nombre;
    }

    public Sala(SalaDTO salaDTO)
    {
        this.setId(salaDTO.getId());
        this.setCodigo(salaDTO.getCodigo());
        this.setNombre(salaDTO.getNombre());
        this.setDireccion(salaDTO.getDireccion());
        this.setTipo(salaDTO.getTipo());
        this.setFormato(salaDTO.getFormato());
        this.setSubtitulo(salaDTO.getSubtitulo());
        this.setHtmlTemplateName(salaDTO.getHtmlTemplateName());
        this.setAsientosNumerados(salaDTO.getAsientosNumerados() != null && salaDTO.getAsientosNumerados());
        this.setUrlComoLlegar(salaDTO.getUrlComoLlegar());
        this.setClaseEntradaTaquilla(salaDTO.getClaseEntradaTaquilla());
        this.setClaseEntradaOnline(salaDTO.getClaseEntradaOnline());
        this.setAforoPorCompras(salaDTO.isAforoPorCompras());

        if (salaDTO.getParCine() != null)
            this.setCine(Cine.cineDTOToCine(salaDTO.getParCine(), false));
    }

    public static Sala salaDTOtoSala(SalaDTO salaDTO)
    {
        return new Sala(salaDTO);
    }

    public static SalaDTO salaToSalaDTO(Sala sala)
    {
        SalaDTO salaDTO = new SalaDTO();

        salaDTO.setId(sala.getId());
        salaDTO.setCodigo(sala.getCodigo());
        salaDTO.setNombre(sala.getNombre());
        salaDTO.setDireccion(sala.getDireccion());
        salaDTO.setTipo(sala.getTipo());
        salaDTO.setFormato(sala.getFormato());
        salaDTO.setSubtitulo(sala.getSubtitulo());
        salaDTO.setHtmlTemplateName(sala.getHtmlTemplateName());
        salaDTO.setAsientosNumerados(sala.isAsientosNumerados());
        salaDTO.setUrlComoLlegar(sala.getUrlComoLlegar());
        salaDTO.setClaseEntradaTaquilla(sala.getClaseEntradaTaquilla());
        salaDTO.setClaseEntradaOnline(sala.getClaseEntradaOnline());
        salaDTO.setAforoPorCompras(sala.isAforoPorCompras());

        if (sala.getCine() != null)
            salaDTO.setParCine(Cine.cineToCineDTO(sala.getCine()));

        return salaDTO;
    }

    public static List<Sala> salasDTOtoSalas(List<SalaDTO> salasDTO)
    {
        ArrayList<Sala> salas = new ArrayList<Sala>();
        ArrayList<Long> idSalas = new ArrayList<>();

        for (SalaDTO salaDTO : salasDTO) {
            if (!idSalas.contains(salaDTO.getId())) {
                idSalas.add(salaDTO.getId());
                salas.add(Sala.salaDTOtoSala(salaDTO));
            }
        }

        return salas;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getFormato()
    {
        return formato;
    }

    public void setFormato(String formato)
    {
        this.formato = formato;
    }

    public String getSubtitulo()
    {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo)
    {
        this.subtitulo = subtitulo;
    }

    public Cine getCine()
    {
        return cine;
    }

    public void setCine(Cine cine)
    {
        this.cine = cine;
    }

    public String getHtmlTemplateName() {
        return htmlTemplateName;
    }

    public void setHtmlTemplateName(String htmlTemplateName) {
        this.htmlTemplateName = htmlTemplateName;
    }

    public boolean isAsientosNumerados()
    {
        return asientosNumerados;
    }

    public void setAsientosNumerados(boolean asientosNumerados)
    {
        this.asientosNumerados = asientosNumerados;
    }

    public static void checkValidity(String nombre, String codigo) throws RegistroSerializaException {
        if (nombre == null)
            throw new RegistroSerializaException(GeneralPARException.NOMBRE_SALA_NULO_CODE);
        
        Sala.checkValidity(codigo);
    }
    
    public static void checkValidity(String codigo) throws RegistroSerializaException {
    	if (codigo == null)
            throw new RegistroSerializaException(GeneralPARException.SALA_NULA_CODE);
    	
    	 if (codigo.length() > 6)
             throw new RegistroSerializaException(GeneralPARException.CODIGO_SALA_LARGO_CODE);
                     
    }

    public String getUrlComoLlegar() {
        return urlComoLlegar;
    }

    public void setUrlComoLlegar(String urlComoLlegar) {
        this.urlComoLlegar = urlComoLlegar;
    }

    public String getClaseEntradaTaquilla() {
        return claseEntradaTaquilla;
    }

    public void setClaseEntradaTaquilla(String claseEntradaTaquilla) {
        this.claseEntradaTaquilla = claseEntradaTaquilla;
    }

    public String getClaseEntradaOnline() {
        return claseEntradaOnline;
    }

    public void setClaseEntradaOnline(String claseEntradaOnline) {
        this.claseEntradaOnline = claseEntradaOnline;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isAforoPorCompras() {
        return aforoPorCompras;
    }

    public void setAforoPorCompras(boolean aforoPorCompras) {
        this.aforoPorCompras = aforoPorCompras;
    }
}