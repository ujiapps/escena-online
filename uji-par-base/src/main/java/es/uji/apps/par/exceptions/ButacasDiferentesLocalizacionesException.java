package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class ButacasDiferentesLocalizacionesException extends GeneralPARException
{
    public ButacasDiferentesLocalizacionesException()
    {
        super(BUTACAS_DIFERENTES_LOCALIZACIONES_CODE, BUTACAS_DIFERENTES_LOCALIZACIONES);
    }
}
