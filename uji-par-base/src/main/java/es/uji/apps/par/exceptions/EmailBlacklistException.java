package es.uji.apps.par.exceptions;


@SuppressWarnings("serial")
public class EmailBlacklistException extends GeneralPARException
{
    public EmailBlacklistException()
    {
        super(EMAIL_BLACKLIST_CODE);
    }
}
