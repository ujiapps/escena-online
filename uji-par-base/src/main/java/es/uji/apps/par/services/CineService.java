package es.uji.apps.par.services;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import es.uji.apps.par.dao.CinesDAO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.exceptions.FormatoImagenException;
import es.uji.apps.par.model.Cine;

@Service
public class CineService {
    @Autowired
    ImageService imageService;

    @Autowired
    private CinesDAO cinesDAO;

    @Transactional
    public Cine addCine(Cine cine) {
        checkAndSaveImage(cine);

        cine.setApiKey(RandomStringUtils.randomAlphanumeric(20));
        cine.setIva(new BigDecimal(10));
        cine.setShowIVA(true);
        cine.setLimiteEntradasGratuitasPorCompra(10);

        cine.setLogoReport("-");
        cine.setUrlComoLlegar("-");
        cine.setUrlPieEntrada("-");
        cine.setUrlPrivacidad("-");

        return cinesDAO.addCine(cine);
    }

    private void checkAndSaveImage(Cine cine) {
        byte[] logo = cine.getLogo();
        if (logo != null && logo.length > 0) {
            if (imageService.isImageValidFormat(logo)) {
                String logoSrc = cine.getLogoSrc();
                String logoContentType = cine.getLogoContentType();
                if (imageService.hasADEConfigured() && logoContentType != null) {
                    String reference = imageService.insertImageToAdeWS(logo, logoSrc, logoContentType);
                    cine.setLogoUUID(reference);
                } else {
                    cine.setLogo(logo);
                }
            } else {
                throw new FormatoImagenException();
            }
        }

        byte[] banner = cine.getBanner();
        if (banner != null && banner.length > 0) {
            if (imageService.isImageValidFormat(banner)) {
                String bannerSrc = cine.getBannerSrc();
                String bannerContentType = cine.getBannerContentType();
                if (imageService.hasADEConfigured() && bannerContentType != null) {
                    String reference = imageService.insertImageToAdeWS(banner, bannerSrc, bannerContentType);
                    cine.setBannerUUID(reference);
                } else {
                    cine.setBanner(banner);
                }
            } else {
                throw new FormatoImagenException();
            }
        }
    }

    public Cine getCineConImagen(CineDTO cineDTO) {
        Cine cine = Cine.cineDTOToCine(cineDTO, true);
        if (cine.getLogoUUID() != null) {
            byte[] fitxerFromAdeWS = imageService.getImageFromAdeWS(cine.getLogoUUID());
            cine.setLogo(fitxerFromAdeWS);
        }

        if (cine.getBannerUUID() != null) {
            byte[] fitxerFromAdeWS = imageService.getImageFromAdeWS(cine.getBannerUUID());
            cine.setBanner(fitxerFromAdeWS);
        }

        return cine;
    }

    @Transactional
    public void updateDatosRegistro(Cine cine) {
        cinesDAO.updateDatosRegistro(cine);
    }

    @Transactional
    public void updateCine(Cine cine) {
        checkAndSaveImage(cine);

        cinesDAO.updateCine(cine);
    }
}
