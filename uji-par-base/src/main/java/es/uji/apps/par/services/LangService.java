package es.uji.apps.par.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.par.config.ConfigurationSelector;

@Service
public class LangService {
    private static final Logger log = LoggerFactory.getLogger(LangService.class);

    @Autowired
    ConfigurationSelector configurationSelector;

    public boolean isLangAllowed(String lang) {
        String langsAllowed = configurationSelector.getLangsAllowed();
        return langsAllowed != null && configurationSelector.getLangsAllowed().replaceAll(" ", "").replaceAll("\"", "'")
            .contains(String.format("'locale':'%s'", lang));
    }
}
