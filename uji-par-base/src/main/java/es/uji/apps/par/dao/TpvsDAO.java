package es.uji.apps.par.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.db.QSalaDTO;
import es.uji.apps.par.db.QSalasUsuarioDTO;
import es.uji.apps.par.db.QTpvsDTO;
import es.uji.apps.par.db.QTpvsSalasDTO;
import es.uji.apps.par.db.QUsuarioDTO;
import es.uji.apps.par.db.TpvsDTO;

@Repository
public class TpvsDAO extends BaseDAO {
    @Autowired
    Configuration configuration;

    private QTpvsDTO qTpvsDTO = QTpvsDTO.tpvsDTO;
    private QTpvsSalasDTO qTpvsSalasDTO = QTpvsSalasDTO.tpvsSalasDTO;
    private QSalasUsuarioDTO qSalasUsuarioDTO = QSalasUsuarioDTO.salasUsuarioDTO;
    private QSalaDTO qSalaDTO = QSalaDTO.salaDTO;
    private QUsuarioDTO qUsuarioDTO = QUsuarioDTO.usuarioDTO;

    @Autowired
    public TpvsDAO(Configuration configuration) {
        if (this.configuration == null)
            this.configuration = configuration;
    }

    @Transactional
    public TpvsDTO getTpvDefault(String userUID) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qTpvsDTO).leftJoin(qTpvsDTO.parTpvsSalas, qTpvsSalasDTO)
            .leftJoin(qTpvsSalasDTO.sala, qSalaDTO).leftJoin(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .leftJoin(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
            .where(qUsuarioDTO.usuario.eq(userUID).and(qTpvsDTO.visible.isTrue())).orderBy(qTpvsDTO.id.asc()).limit(1)
            .uniqueResult(qTpvsDTO);
    }

    @Transactional
    public List<TpvsDTO> getTpvs(
        String userUID,
        boolean onlyVisibles,
        String sortParameter,
        int start,
        int limit
    ) {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where = qUsuarioDTO.usuario.eq(userUID);
        if (onlyVisibles) {
            where = where.and(qTpvsDTO.visible.isTrue());
        }

        return query.from(qTpvsDTO).leftJoin(qTpvsDTO.parTpvsSalas, qTpvsSalasDTO)
            .leftJoin(qTpvsSalasDTO.sala, qSalaDTO).leftJoin(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .leftJoin(qSalasUsuarioDTO.parUsuario, qUsuarioDTO).where(where).orderBy(getSort(qTpvsDTO, sortParameter))
            .offset(start).limit(limit).distinct().list(qTpvsDTO);
    }

    @Transactional
    public void update(TpvsDTO tpv) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qTpvsDTO);
        update.set(qTpvsDTO.visible, tpv.getVisible()).
            where(qTpvsDTO.id.eq(tpv.getId())).execute();
    }

    public long countTpvsVisible(
        String userUID,
        long id
    ) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qTpvsDTO).leftJoin(qTpvsDTO.parTpvsSalas, qTpvsSalasDTO)
            .leftJoin(qTpvsSalasDTO.sala, qSalaDTO).leftJoin(qSalaDTO.parSalasUsuario, qSalasUsuarioDTO)
            .leftJoin(qSalasUsuarioDTO.parUsuario, qUsuarioDTO)
            .where(qUsuarioDTO.usuario.eq(userUID).and(qTpvsDTO.id.ne(id)).and(qTpvsDTO.visible.isTrue())).distinct()
            .count();
    }
}
