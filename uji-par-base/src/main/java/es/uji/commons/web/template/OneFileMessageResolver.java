//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package es.uji.commons.web.template;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.thymeleaf.Arguments;
import org.thymeleaf.messageresolver.AbstractMessageResolver;
import org.thymeleaf.messageresolver.MessageResolution;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

public class OneFileMessageResolver extends AbstractMessageResolver {
    private static Logger log = Logger.getLogger(OneFileMessageResolver.class);
    protected Pair<String, Properties> generalConfigurationRegistry = null;
    protected Map<String, Pair<String, Properties>> configurationRegistry = new HashMap();
    protected String application;

    public OneFileMessageResolver(String application) {
        this.application = application;
    }

    public MessageResolution resolveMessage(Arguments arguments, String key, Object[] messageParameters) {
        Locale locale = arguments.getContext().getLocale();
        String language = locale.getLanguage();

        this.initGeneralPropertiesFileIfNeeded();
        this.initPropertiesFileIfNeeded(language);

        Properties configuration = this.configurationRegistry.get(locale.getLanguage()).getRight();
        if (!configuration.containsKey(key)) {
            configuration = this.generalConfigurationRegistry.getRight();
        }

        return messageParameters != null && messageParameters.length > 0 ?
            new MessageResolution(MessageFormat.format(configuration.getProperty(key), messageParameters)) :
            new MessageResolution(configuration.getProperty(key));
    }

    protected void initGeneralPropertiesFileIfNeeded() {
        try {
            String file = MessageFormat.format("/etc/uji/{0}/i18n/i18n.properties", this.application);
            if (this.generalConfigurationRegistry == null) {
                this.initGeneralPropertiesFile(file);
            }
            else {
                String md5Saved = this.generalConfigurationRegistry.getLeft();
                String md5 = getMD5FromFile(file);
                if (md5 == null || md5Saved == null || !md5.equals(md5Saved)) {
                    this.initGeneralPropertiesFile(file);
                }
            }
        } catch (Exception var3) {
            log.error(var3);
            throw new RuntimeException(var3);
        }
    }

    protected void initGeneralPropertiesFile(String file) {
        try {
            Properties diskConfiguration = new Properties();
            diskConfiguration.load(new FileReader(file));
            this.generalConfigurationRegistry = new ImmutablePair<>(getMD5FromFile(file), diskConfiguration);
        } catch (Exception var3) {
            log.error(var3);
            throw new RuntimeException(var3);
        }
    }

    protected void initPropertiesFileIfNeeded(String language) {
        try {
            String file = MessageFormat.format("/etc/uji/{0}/i18n/i18n_{1}.properties", this.application, language);
            if (!this.configurationRegistry.containsKey(language)) {
                this.initPropertiesFileForLanguage(file, language);
            }
            else {
                String md5Saved = this.configurationRegistry.get(language).getLeft();
                String md5 = getMD5FromFile(file);
                if (md5 == null || md5Saved == null || !md5.equals(md5Saved)) {
                    this.initPropertiesFileForLanguage(file, language);
                }
            }
        } catch (Exception var3) {
            log.error(var3);
            throw new RuntimeException(var3);
        }
    }

    private String getMD5FromFile(String file) throws IOException {
        try (InputStream is = Files.newInputStream(Paths.get(file))) {
            String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(is);
            return md5;
        }
    }

    protected void initPropertiesFileForLanguage(String file, String language) {
        try {
            Properties diskConfiguration = new Properties();
            diskConfiguration.load(new FileReader(file));
            this.configurationRegistry.put(language, new ImmutablePair<>(getMD5FromFile(file), diskConfiguration));
        } catch (Exception var3) {
            log.error(var3);
            throw new RuntimeException(var3);
        }
    }
}
