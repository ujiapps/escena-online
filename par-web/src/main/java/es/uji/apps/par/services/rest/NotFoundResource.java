package es.uji.apps.par.services.rest;

import java.net.URI;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("notfound")
public class NotFoundResource extends BaseResource {

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getNotFound()
    {
        return Response.seeOther(URI.create(configuration.getNotFoundURL())).build();
    }
}
