package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.model.Ocupacion;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.ButacasService;
import es.uji.apps.par.services.LocalizacionesService;
import es.uji.apps.par.services.SesionesService;
import es.uji.apps.par.services.UsersService;
import es.uji.apps.par.utils.LocaleUtils;

@Path("sesion")
public class ButacasResource extends BaseResource
{
    @InjectParam
    private ButacasService butacasService;

    @InjectParam
    UsersService usersService;

    @InjectParam
    private SesionesService sesionesService;

    @InjectParam
    private LocalizacionesService localizacionesService;

    @GET
    @Path("{idSesion}/butacas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getButacasNoAnuladas(@PathParam("idSesion") Long idSesion) throws InterruptedException
    {
        if (!correctApiKey(currentRequest))
        {
            return apiAccessDenied();
        }

        String language = LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage();
        
        List<Butaca> butacas = butacasService.getButacasNoAnuladas(idSesion, language);

        return Response.ok().entity(butacas).build();
    }

    @GET
    @Path("{idSesion}/ocupacion")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSesion(@PathParam("idSesion") Long idSesion) throws InterruptedException
    {
        if (!correctApiKey(currentRequest))
        {
            return apiAccessDenied();
        }
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
        Ocupacion ocupacion = sesionesService.getSesionConVendida(idSesion, user.getUsuario());

        return Response.ok().entity(ocupacion).build();
    }

    @GET
    @Path("{idSesion}/localizaciones")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLocalizaciones(@PathParam("idSesion") Long idSesion) throws InterruptedException
    {
        if (!correctApiKey(currentRequest))
        {
            return apiAccessDenied();
        }

        List<Localizacion> localizaciones = localizacionesService.getLocalizacionesSesion(idSesion);

        return Response.ok().entity(localizaciones).build();
    }

    @POST
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateEntradasPresentadas(@PathParam("id") Long sesionId, List<Butaca> butacas)
    {
        if (!correctApiKey(currentRequest))
        {
            return apiAccessDenied();
        }

        butacasService.updatePresentadas(butacas);
        
        return Response.ok().build();
    }

    @POST
    @Path("{id}/online")
    @Produces(MediaType.APPLICATION_JSON)
    public Response presentarButaca(@PathParam("id") Long sesionId, Butaca butaca)
    {
        if (!correctApiKey(currentRequest))
        {
            return apiAccessDenied();
        }

        long update = butacasService.presentarButaca(butaca);
        RestResponse response = new RestResponse();
        if (update > 0) {
            response.setSuccess(true);
            return Response.ok(response).build();
        }
        else {
            response.setSuccess(false);
            return Response.ok(response).build();
        }
    }

}
