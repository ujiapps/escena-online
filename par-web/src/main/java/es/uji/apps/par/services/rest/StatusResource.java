package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("status")
public class StatusResource extends BaseResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStatus() {
        Client client = Client.create();
        WebResource webResource = client.resource(configurationSelector.getUrlAdmin());
        ClientResponse response =
            webResource.path("rest/status").type(MediaType.APPLICATION_FORM_URLENCODED).get(ClientResponse.class);

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            return Response.ok().build();
        } else {
            return Response.serverError().build();
        }
    }
}
