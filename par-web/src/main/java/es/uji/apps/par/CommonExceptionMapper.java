package es.uji.apps.par;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import es.uji.apps.par.config.ConfigurationSelector;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.utils.LocaleUtils;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.GrupoMenu;
import es.uji.commons.web.template.model.ItemMenu;
import es.uji.commons.web.template.model.Menu;
import es.uji.commons.web.template.model.Pagina;

import static es.uji.apps.par.services.rest.BaseResource.APP;

@Provider
public class CommonExceptionMapper implements ExceptionMapper<Exception> {
    private static final Logger log = LoggerFactory.getLogger(CommonExceptionMapper.class);

    @Context
    HttpServletRequest currentRequest;

    @Autowired
    ConfigurationSelector configurationSelector;

    @Override
    public Response toResponse(Exception exception) {
        log.error(exception.getMessage(), exception);
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();

        Template template = new HTMLTemplate(Constantes.PLANTILLAS_DIR + "error", locale, APP);
        Pagina pagina = null;

        try {
            pagina = new Pagina(configurationSelector.getUrlPublic(), configurationSelector.getUrlPublic(), language, configurationSelector.getHtmlTitle());
            pagina.setTitulo(configurationSelector.getHtmlTitle());
            pagina.setSubTitulo("");
        } catch (ParseException e) {

        }
        template.put("idioma", language);
        template.put("baseUrl", configurationSelector.getUrlPublic());
        template.put("pagina", pagina);

        Menu menu = new Menu();

        GrupoMenu grupo = new GrupoMenu("Comunicació");
        grupo.addItem(new ItemMenu("Noticies", "http://www.uji.es/"));
        grupo.addItem(new ItemMenu("Investigació", "http://www.uji.es/"));
        menu.addGrupo(grupo);
        pagina.setMenu(menu);

        return Response.ok(template).build();
    }
}