package es.uji.apps.par.services.rest;

import com.google.common.base.Strings;

import com.mysema.commons.lang.Pair;
import com.sun.jersey.api.core.InjectParam;

import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.builders.PublicPageBuilderInterface;
import es.uji.apps.par.database.DatabaseHelper;
import es.uji.apps.par.database.DatabaseHelperFactory;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.exceptions.EventoNoEncontradoException;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.DisponiblesLocalizacion;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.EventoParaSync;
import es.uji.apps.par.model.ImagenesEvento;
import es.uji.apps.par.model.PreciosSesion;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.ButacasService;
import es.uji.apps.par.services.ComprasService;
import es.uji.apps.par.services.CondicionesService;
import es.uji.apps.par.services.EntradasService;
import es.uji.apps.par.services.EventosService;
import es.uji.apps.par.services.PreciosService;
import es.uji.apps.par.services.SalasService;
import es.uji.apps.par.services.SesionesService;
import es.uji.apps.par.services.UsersService;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.LocaleUtils;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;

@Path("evento")
public class EventosResource extends BaseResource {

    @InjectParam
    private EventosService eventosService;

    @InjectParam
    SalasService salasService;

    @InjectParam
    private SesionesService sesionesService;

    @InjectParam
    private PreciosService preciosService;

    @InjectParam
    private ButacasService butacasService;

    @InjectParam
    private UsersService usersService;

    @InjectParam
    private PublicPageBuilderInterface publicPageBuilderInterface;

    @InjectParam
    private ComprasService comprasService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventos(
        @QueryParam("key") String key,
        @QueryParam("prevDays") @DefaultValue("7") int prevDays
    ) {
        if (!correctApiKey(currentRequest)) {
            return apiAccessDenied();
        }

        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
        boolean onlySingleSessions = !Strings.isNullOrEmpty(key);
        List<Evento> eventos =
            eventosService.getEventosConSesionesRecientes(user.getUsuario(), prevDays, onlySingleSessions);

        imagenesANull(eventos);

        return Response.ok().entity(new RestResponse(true, eventos, 0)).build();
    }

    @GET
    @Path("activos/online")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventosActivosParaVentaOnline() {
        if (!correctApiKey(currentRequest)) {
            return apiAccessDenied();
        }

        List<EventoParaSync> eventos;

        eventos = eventosService.getEventosActivosParaVentaOnline(configurationSelector.getUrlPublic());
        return Response.ok().entity(eventos).build();
    }

    private void imagenesANull(List<Evento> eventos) {
        for (Evento evento : eventos) {
            evento.setImagen(null);
            evento.setImagenPubli(null);
        }
    }

    @GET
    @Path("listado/")
    @Produces(MediaType.TEXT_HTML)
    public Template getListadoEventos(@QueryParam("fecha") String fecha) throws Exception {
        currentRequest.getSession().setAttribute(MENU_ABONO, false);

        return getListadoEventosBySala(null, fecha);
    }

    @GET
    @Path("listado/abonos")
    @Produces(MediaType.TEXT_HTML)
    public Template getListadoEventosAbonos() throws Exception {
        currentRequest.getSession().setAttribute(MENU_ABONO, true);

        try {
            Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
            Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
            List<Evento> abonos =
                eventosService.getAbonos("[{\"property\":\"fechaPrimeraSesion\",\"direction\":\"ASC\"}]", 0,
                    Integer.MAX_VALUE, null, user.getUsuario(), cine.getId());

            return getTemplateAbonos(
                abonos.stream().filter(abono -> abono.getFechaPrimeraSesion() != null).collect(Collectors.toList()),
                null, comprasService.isMobile(currentRequest), user.getUsuario());
        } catch (EventoNoEncontradoException e) {
            return getTemplateEventoNoEncontrado(comprasService.isMobile(currentRequest));
        }
    }

    @GET
    @Path("listado/{salaId}")
    @Produces(MediaType.TEXT_HTML)
    public Template getListadoEventosBySala(
        @PathParam("salaId") Long salaId,
        @QueryParam("fecha") String fecha
    ) throws Exception {
        try {
            Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
            Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
            List<Evento> eventosActivos;
            if (fecha != null && fecha.trim().length() == 0) {
                fecha = null;
            }
            if (salaId != null) {
                eventosActivos = eventosService.getEventosActivosBySala(
                    "[{\"property\":\"fechaPrimeraSesion\",\"direction\":\"ASC\"}]", 0, Integer.MAX_VALUE, null,
                    user.getUsuario(), cine.getId(), salaId, fecha);
            } else {
                eventosActivos =
                    eventosService.getEventosActivos("[{\"property\":\"fechaPrimeraSesion\",\"direction\":\"ASC\"}]", 0,
                        Integer.MAX_VALUE, null, user.getUsuario(), cine.getId(), fecha);
            }

            return getTemplateEventos(eventosActivos.stream()
                .filter(evento -> evento.getFechaPrimeraSesion() != null && evento.getPublico() == true)
                .collect(Collectors.toList()), salaId, comprasService.isMobile(currentRequest), fecha);
        } catch (EventoNoEncontradoException e) {
            return getTemplateEventoNoEncontrado(comprasService.isMobile(currentRequest));
        }
    }

    @GET
    @Path("{contenidoId}")
    @Produces(MediaType.TEXT_HTML)
    public Response getEvento(@PathParam("contenidoId") String contenidoId) throws Exception {
        try {
            Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
            Evento evento = eventosService.getEventoByRssId(contenidoId, user.getUsuario());

            return getTemplateEvento(evento, user.getUsuario(), comprasService.isMobile(currentRequest));
        } catch (EventoNoEncontradoException e) {
            return Response.ok(getTemplateEventoNoEncontrado(comprasService.isMobile(currentRequest))).build();
        }
    }

    @GET
    @Path("id/{id}")
    @Produces(MediaType.TEXT_HTML)
    public Response getEventoById(@PathParam("id") Long id) throws Exception {
        try {
            Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
            Evento evento = eventosService.getEvento(id, user.getUsuario());

            return getTemplateEvento(evento, user.getUsuario(), comprasService.isMobile(currentRequest));
        } catch (EventoNoEncontradoException e) {
            return Response.ok(getTemplateEventoNoEncontrado(comprasService.isMobile(currentRequest))).build();
        }
    }

    private Template getTemplateEventoNoEncontrado(boolean mobile) throws ParseException {
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());

        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();
        HTMLTemplate template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/eventoNoEncontrado", locale, APP);

        String url = currentRequest.getRequestURL().toString();

        template.put("pagina", publicPageBuilderInterface.buildPublicPageInfo(getBaseUrlPublic(), url, language,
            configurationSelector.getHtmlTitle()));
        template.put("baseUrl", getBaseUrlPublic());
        template.put("lang", language);
        cine.serializeTemplate(template, mobile);

        return template;
    }

    private Template getTemplateBaseEventos(
        Template template,
        List<Evento> eventos,
        Long salaId,
        String language
    ) throws ParseException {
        borrarEntradasSeleccionadasConAnterioridad();

        List<Sala> salas =
            salasService.getSalas(usersService.getUserByServerName(currentRequest.getServerName()).getUsuario());
        template.put("salas", salas);
        template.put("salaId", salaId);

        String url = currentRequest.getRequestURL().toString();
        template.put("pagina", publicPageBuilderInterface.buildPublicPageInfo(getBaseUrlPublic(), url, language,
            configurationSelector.getHtmlTitle()));
        template.put("baseUrl", getBaseUrlPublic());
        template.put("eventos", eventos);
        template.put("lang", language);

        return template;
    }

    private Template getTemplateEventos(
        List<Evento> eventos,
        Long salaId,
        boolean mobile,
        String fecha
    ) throws ParseException {
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();

        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());

        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/eventosListado", locale, APP);
        cine.serializeTemplate(template, mobile);
        template.put("fecha", fecha);

        return getTemplateBaseEventos(template, eventos, salaId, language);
    }

    private Template getTemplateAbonos(
        List<Evento> eventos,
        Long salaId,
        boolean mobile,
        String userUID
    ) throws ParseException {
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();

        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());

        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/abonosListado", locale, APP);
        cine.serializeTemplate(template, mobile);

        List<Pair> abonosConPrecio = new ArrayList<>();
        for (Evento evento : eventos) {
            PreciosSesion precio = preciosService.getPrecioAbono(evento.getId(), userUID);
            abonosConPrecio.add(new Pair(evento, precio));
        }

        template.put("abonos", abonosConPrecio);

        return getTemplateBaseEventos(template, eventos, salaId, language);
    }

    private Response getTemplateEvento(
        Evento evento,
        String userUID,
        boolean mobile
    ) throws ParseException, URISyntaxException {
        List<Sesion> sesiones = sesionesService.getSesionesActivas(evento.getId(),
            "[{\"property\":\"fechaCelebracion\", \"direction\": \"ASC\"}]", 0, Integer.MAX_VALUE, userUID);

        if (sesiones.size() > 0) {
            if (evento.getAbono() != null && evento.getAbono()) {
                return Response.seeOther(
                    new URI(configurationSelector.getUrlPublic() + "/rest/entrada/" + sesiones.get(0).getId())).build();
            }

            evento.setSesiones(sesiones);

            Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
            String language = locale.getLanguage();

            borrarEntradasSeleccionadasConAnterioridad();

            Template template = new HTMLTemplate(
                Constantes.PLANTILLAS_DIR + evento.getSesiones().get(0).getSala().getCine().getCodigo()
                    + "/eventoDetalle", locale, APP);

            String tipoEvento, titulo, companyia, duracion, caracteristicas, premios, descripcion;
            if (language.equals("ca")) {
                tipoEvento = evento.getParTiposEvento().getNombreVa();
                titulo = evento.getTituloVa();
                companyia = evento.getCompanyiaVa();
                duracion = evento.getDuracionVa();
                caracteristicas = evento.getCaracteristicasVa();
                premios = evento.getPremiosVa();
                descripcion = evento.getDescripcionVa();
            } else {
                tipoEvento = evento.getParTiposEvento().getNombreEs();
                titulo = evento.getTituloEs();
                companyia = evento.getCompanyiaEs();
                duracion = evento.getDuracionEs();
                caracteristicas = evento.getCaracteristicasEs();
                premios = evento.getPremiosEs();
                descripcion = evento.getDescripcionEs();
            }

            String url = currentRequest.getRequestURL().toString();

            template.put("pagina", publicPageBuilderInterface.buildPublicPageInfo(getBaseUrlPublic(), url, language,
                configurationSelector.getHtmlTitle()));
            template.put("baseUrl", getBaseUrlPublic());

            template.put("tipoEvento", tipoEvento);
            template.put("titulo", titulo);
            template.put("companyia", companyia);
            template.put("duracion", duracion);
            template.put("caracteristicas", caracteristicas);
            template.put("premios", premios);
            template.put("descripcion", descripcion);

            template.put("evento", evento);
            template.put("sesiones", sesiones);
            template.put("sesionesPlantilla", getSesionesPlantilla(sesiones, userUID));
            template.put("lang", language);

            Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
            cine.serializeTemplate(template, mobile);

            return Response.ok(template).build();
        } else {
            return Response.ok(getTemplateEventoNoEncontrado(mobile)).build();
        }
    }

    private void borrarEntradasSeleccionadasConAnterioridad() {
        currentRequest.getSession().removeAttribute(EntradasService.BUTACAS_COMPRA);
    }

    private List<Map<String, Object>> getSesionesPlantilla(
        List<Sesion> sesiones,
        String userUID
    ) {
        DatabaseHelper databaseHelper = DatabaseHelperFactory.newInstance(configuration);
        List<Map<String, Object>> sesionesPlantilla = new ArrayList<Map<String, Object>>();
        Calendar cal = Calendar.getInstance();
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);

        List<Long> sesionIds = sesiones.stream().map(Sesion::getId).collect(Collectors.toList());
        List<DisponiblesLocalizacion> disponiblesNoNumerada = butacasService.getDisponibles(sesionIds);
        for (Sesion sesion : sesiones) {
            if (sesion.getFechaCelebracion().before(cal.getTime()))
                continue;

            Map<String, Object> datos = new HashMap<>();


            if (sesiones.size() < 10) {
                List<PreciosSesion> preciosSesion = preciosService.getPreciosSesion(sesion.getId(), userUID);
                datos.put("precios", getTextoPrecios(preciosSesion));
                if (configurationSelector.isMenuAbonosEnabled()) {
                    if (preciosSesion.stream().map(PreciosSesion::getTarifa)
                        .filter(tarifa -> tarifa.getIsAbono().equals("on")).count() > 0) {
                        datos.put("abonosCanjeables", databaseHelper.booleanToNumber(true));
                    }
                }
            }

            Integer disponibles = disponiblesNoNumerada.stream().filter(dN -> dN.getSesionId() == sesion.getId())
                .map(DisponiblesLocalizacion::getDisponibles).reduce(0, Integer::sum);
            datos.put("numeroDisponibles", disponibles);
            datos.put("disponibles", " " + ResourceProperties.getProperty(locale, "venta.disponibles", disponibles));
            datos.put("texto", sesionesService.getFechaSesion(sesion, true, locale));
            datos.put("textoSinHora", sesionesService.getFechaSesion(sesion, false, locale));
            datos.put("id", sesion.getId());
            datos.put("sala", sesion.getSala().getNombre());
            datos.put("salaDireccion", sesion.getSala().getDireccion());
            datos.put("enPlazoVentaInternet", sesion.getEnPlazoVentaInternet());
            datos.put("canalInternet", databaseHelper.booleanToNumber(sesion.getCanalInternet()));
            datos.put("fechaInicioVentaOnline", sesion.getFechaInicioVentaOnline());

            if (sesion.getFechaInicioVentaOnline() != null && sesion.getFechaFinVentaOnline() != null)
                datos.put("textoFechasInternet", ResourceProperties.getProperty(locale, "venta.plazoInternet",
                    DateUtils.dateToSpanishStringWithHour(sesion.getFechaInicioVentaOnline()),
                    DateUtils.dateToSpanishStringWithHour(sesion.getFechaFinVentaOnline())));

            if (sesion.getEnPlazoVentaInternet())
                datos.put("clase", "contieneBoton");


            sesionesPlantilla.add(datos);
        }

        return sesionesPlantilla;
    }

    private String getTextoPrecios(List<PreciosSesion> preciosSesion) {
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String texto = "";
        for (int i = 0; i < preciosSesion.size(); i++) {
            PreciosSesion precio = preciosSesion.get(i);
            if (!Strings.isNullOrEmpty(precio.getTarifa().getIsPublica()) && precio.getTarifa().getIsPublica()
                .equals("on")) {
                texto += (Strings.isNullOrEmpty(texto) ? "" : SesionesService.getUnionChar(locale, i == preciosSesion.size() - 1))
                    + String.format("%s: %s€", precio.getTarifa().getNombre(), precio.getPrecio());
            }
        }

        return texto;
    }

    class Imagen {
        byte[] imagen;
        String contentType;

        public Imagen(
            byte[] imagen,
            String contentType
        ) {
            this.imagen = imagen;
            this.contentType = contentType;
        }

        public byte[] getImagen() {
            return imagen;
        }

        public String getContentType() {
            return contentType;
        }
    }

    private Imagen getImagen(
        Long eventoId,
        boolean imagenPubli
    ) throws IOException {
        ImagenesEvento imagenesEvento = eventosService.getImagenesEvento(eventoId);

        byte[] imagen;
        String contentType;
        if (imagenPubli) {
            imagen = (imagenesEvento.getImagenPubli() != null) ?
                imagenesEvento.getImagenPubli() :
                eventosService.getImagenPubliSustitutivaSiExiste();
            contentType = (imagenesEvento.getImagenPubliContentType() != null) ?
                imagenesEvento.getImagenPubliContentType() :
                eventosService.getImagenPubliSustitutivaContentType();
        } else {
            imagen = (imagenesEvento.getImagen() != null) ?
                imagenesEvento.getImagen() :
                eventosService.getImagenSustitutivaSiExiste();
            contentType = (imagenesEvento.getImagenContentType() != null) ?
                imagenesEvento.getImagenContentType() :
                eventosService.getImagenSustitutivaContentType();
        }

        return new Imagen(imagen, contentType);
    }

    private Response getOutputStreamImagenResponse(EventosResource.Imagen imagen) {
        if (imagen.getImagen() != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream(imagen.getImagen().length);
            bos.write(imagen.getImagen(), 0, imagen.getImagen().length);
            return Response.ok(bos.toByteArray()).type(imagen.getContentType()).build();
        } else {
            return Response.status(404).build();
        }
    }

    @GET
    @Path("{id}/imagen")
    public Response getImagenEvento(@PathParam("id") Long eventoId) throws IOException {
        try {
            Imagen imagen = getImagen(eventoId, false);

            return Response.ok(imagen.getImagen()).type(imagen.getContentType()).build();
        } catch (EventoNoEncontradoException e) {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("{id}/imagenPubli")
    public Response getImagenPubliEvento(@PathParam("id") Long eventoId) throws IOException {
        try {
            Imagen imagenPubli = getImagen(eventoId, true);

            return Response.ok(imagenPubli.getImagen()).type(imagenPubli.getContentType()).build();
        } catch (EventoNoEncontradoException e) {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("{id}/imagenEntrada")
    public Response getImagenEntrada(@PathParam("id") Long eventoId) throws IOException {
        try {
            Imagen imagen = getImagen(eventoId, false);
            return getOutputStreamImagenResponse(imagen);
        } catch (EventoNoEncontradoException e) {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("{id}/imagenPubliEntrada")
    public Response getImagenPubliEntrada(@PathParam("id") Long eventoId) throws IOException {
        try {
            Imagen imagenPubli = getImagen(eventoId, true);
            return getOutputStreamImagenResponse(imagenPubli);
        } catch (EventoNoEncontradoException e) {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("condicionesprivacidad")
    @Produces(MediaType.TEXT_HTML)
    public Template getCondicionesPrivacidad() throws ParseException {
        return getTemplate("condicionesPrivacidad", comprasService.isMobile(currentRequest));
    }

    @GET
    @Path("condicionescancelacion")
    @Produces(MediaType.TEXT_HTML)
    public Template getCondicionesCancelacion() throws ParseException {
        return getTemplate("condicionesCancelacion", comprasService.isMobile(currentRequest));
    }

    @GET
    @Path("politicacookies")
    @Produces(MediaType.TEXT_HTML)
    public Template getPoliticaCookies() throws ParseException {
        return getTemplate("politicaCookies", comprasService.isMobile(currentRequest));
    }

    @GET
    @Path("avisolegal")
    @Produces(MediaType.TEXT_HTML)
    public Template getAvisoLegal() throws ParseException {
        return getTemplate("avisoLegal", comprasService.isMobile(currentRequest));
    }

    @GET
    @Path("condicionesgenerales")
    @Produces(MediaType.TEXT_HTML)
    public Template getCondicionesGenerales() throws ParseException {
        Template template = getTemplate("condicionesGenerales", comprasService.isMobile(currentRequest));

        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        String condiciones = CondicionesService.getCondiciones(cine.getCodigo(), locale);

        template.put("condiciones", condiciones.replaceAll("\n", "<br/>"));

        return template;
    }

    private Template getTemplate(
        String templateName,
        boolean mobile
    ) throws ParseException {
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();

        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/" + templateName, locale, APP);

        String url = currentRequest.getRequestURL().toString();

        template.put("pagina", publicPageBuilderInterface.buildPublicPageInfo(getBaseUrlPublic(), url, language,
            configurationSelector.getHtmlTitle()));
        template.put("baseUrl", getBaseUrlPublic());
        template.put("lang", language);
        cine.serializeTemplate(template, mobile);

        return template;
    }
}
