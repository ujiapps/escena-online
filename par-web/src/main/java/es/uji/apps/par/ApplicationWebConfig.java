package es.uji.apps.par;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.filters.QueueFilter;
import es.uji.apps.par.services.QueueService;

public class ApplicationWebConfig {
    private static final Logger log = LoggerFactory.getLogger(ApplicationWebConfig.class);

    @Autowired
    private QueueService queueService;

    @Autowired
    protected Configuration configuration;

    public ApplicationWebConfig() {
    }

    public void initialize() {
        if (configuration.getQueueDomains() != null) {
            List<String> activeSessions = queueService.getActiveSessions();
            for (String activeSession : activeSessions) {
                try {
                    new File(Paths.get(QueueFilter.TMP_DIR, QueueFilter.SPRING_PREFIX + activeSession).toString()).createNewFile();
                } catch (IOException e) {
                    log.warn(String.format("Error creando el archivo de sesión %s", activeSession));
                }
            }
        }
    }
}
