package es.uji.apps.par.services.rest;

import com.google.common.base.Strings;

import com.mysema.commons.lang.Pair;
import com.sun.jersey.api.core.InjectParam;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import es.uji.apps.par.builders.PublicPageBuilderInterface;
import es.uji.apps.par.butacas.EstadoButacasRequest;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.TpvsDTO;
import es.uji.apps.par.exceptions.AbonoUsadoMaxEnDistintosEventosException;
import es.uji.apps.par.exceptions.AbonoUsadoMaxEnEventoException;
import es.uji.apps.par.exceptions.ButacaOcupadaException;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.CompraButacaDescuentoNoDisponible;
import es.uji.apps.par.exceptions.CompraButacaNoExistente;
import es.uji.apps.par.exceptions.CompraCaducadaException;
import es.uji.apps.par.exceptions.CompraInvitacionPorInternetException;
import es.uji.apps.par.exceptions.CompraSinButacasException;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.exceptions.EmailBlacklistException;
import es.uji.apps.par.exceptions.EmailVerificacionException;
import es.uji.apps.par.exceptions.EventoNominalException;
import es.uji.apps.par.exceptions.FueraDePlazoVentaInternetException;
import es.uji.apps.par.exceptions.LimiteEntradasGratisSuperadoException;
import es.uji.apps.par.exceptions.LimiteEntradasPorEmailSuperadoException;
import es.uji.apps.par.exceptions.NoAbonadoException;
import es.uji.apps.par.exceptions.NoHayButacasLibresException;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.model.ButacaMap;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Compra;
import es.uji.apps.par.model.DisponiblesLocalizacion;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.GastosGestion;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.model.Plantilla;
import es.uji.apps.par.model.PreciosSesion;
import es.uji.apps.par.model.ResultadoCompra;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.SignatureTPV;
import es.uji.apps.par.model.Tarifa;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.ButacasService;
import es.uji.apps.par.services.ComprasService;
import es.uji.apps.par.services.EntradasService;
import es.uji.apps.par.services.LocalizacionesService;
import es.uji.apps.par.services.PlantillasButacasReservasService;
import es.uji.apps.par.services.PlantillasReservasService;
import es.uji.apps.par.services.PreciosService;
import es.uji.apps.par.services.SalasService;
import es.uji.apps.par.services.SesionesService;
import es.uji.apps.par.services.UsersService;
import es.uji.apps.par.tpv.HmacSha256TPVInterface;
import es.uji.apps.par.tpv.IdTPVInterface;
import es.uji.apps.par.tpv.SHA1TPVInterface;
import es.uji.apps.par.tpv.TpvInterface;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.LocaleUtils;
import es.uji.apps.par.utils.PrecioUtils;
import es.uji.apps.par.utils.ReportUtils;
import es.uji.apps.par.utils.Utils;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;

@Path("entrada")
public class EntradasResource extends BaseResource {
    private static final Logger log = LoggerFactory.getLogger(EntradasResource.class);

    @InjectParam
    private PlantillasReservasService plantillasReservasService;

    @InjectParam
    private PlantillasButacasReservasService plantillasButacasReservasService;

    @InjectParam
    private SesionesService sesionesService;

    @InjectParam
    private PreciosService preciosService;

    @InjectParam
    private ButacasService butacasService;

    @InjectParam
    private SalasService salasService;

    @InjectParam
    private ComprasService comprasService;

    @InjectParam
    private LocalizacionesService localizacionesService;

    @InjectParam
    private UsersService usersService;

    @InjectParam
    private TpvInterface tpvInterface;

    @InjectParam
    private HmacSha256TPVInterface hmacSha256TPVInterface;

    @InjectParam
    private SHA1TPVInterface sha1TPVInterface;

    @InjectParam
    private IdTPVInterface idTPVInterface;

    @InjectParam
    private PublicPageBuilderInterface publicPageBuilderInterface;

    @Context
    HttpServletResponse currentResponse;

    @GET
    @Path("{id}")
    @Produces(MediaType.TEXT_HTML)
    public Response datosEntrada(
        @PathParam("id") Long sesionId,
        @QueryParam("abonos") @DefaultValue("false") boolean abonos
    ) throws Exception {
        boolean mobile = comprasService.isMobile(currentRequest);
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
        Sesion sesion = sesionesService.getSesion(sesionId, user.getUsuario());
        if (sesion.getCanalInternet() && (sesion.getAnulada() == null || sesion.getAnulada() == false)) {
            currentRequest.getSession().setAttribute(EntradasService.ID_SESION, sesionId);

            Evento evento = sesion.getEvento();
            if (Utils.isAsientosNumerados(evento)) {
                return paginaSeleccionEntradasNumeradas(sesion, null, abonos, user.getUsuario(), mobile);
            } else {
                return paginaSeleccionEntradasNoNumeradas(sesion, null, abonos, user.getUsuario(), mobile);
            }
        } else
            return paginaFueraDePlazo(sesionId, user.getUsuario(), mobile);
    }

    private Response paginaSeleccionEntradasNumeradas(
        Sesion sesion,
        String error,
        boolean abonos,
        String userUID,
        boolean mobile
    ) throws Exception {
        long sesionId = sesion.getId();
        String urlBase = getBaseUrlPublic();
        String url = currentRequest.getRequestURL().toString();

        if (!sesion.getEnPlazoVentaInternet())
            return paginaFueraDePlazo(sesionId, userUID, mobile);

        Evento evento = sesion.getEvento();

        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();
        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + sesion.getSala().getCine().getCodigo() + "/seleccionEntrada",
                locale, APP);
        template.put("evento", evento);
        template.put("sesion", sesion);
        template.put("idioma", language);
        template.put("baseUrl", getBaseUrlPublic());
        template.put("fecha", DateUtils.dateToSpanishString(sesion.getFechaCelebracion()));
        template.put("hora", sesion.getHoraCelebracion());
        template.put("limiteEntradas", comprasService.getLimiteEntradas(evento));
        template.put("pagina", publicPageBuilderInterface.buildPublicPageInfo(urlBase, url, language,
            configurationSelector.getHtmlTitle()));
        template.put("tipoEventoEs", sesion.getEvento().getParTiposEvento().getNombreEs());
        template.put("butacasFragment",
            Constantes.PLANTILLAS_DIR + sesion.getSala().getCine().getCodigo() + "/" + sesion.getSala()
                .getHtmlTemplateName());
        Calendar cal = Calendar.getInstance();
        template.put("millis", cal.getTime().getTime());

        String butacasSesion = (String) currentRequest.getSession().getAttribute(EntradasService.BUTACAS_COMPRA);
        if (butacasSesion == null) {
            template.put("butacasSesion", "[]");
        } else {
            template.put("butacasSesion", butacasSesion);
        }

        String uuidCompra = (String) currentRequest.getSession().getAttribute(EntradasService.UUID_COMPRA);
        if (uuidCompra != null) {
            template.put("uuidCompra", uuidCompra);
        }

        if (language.equals("ca")) {
            template.put("titulo", evento.getTituloVa());
            template.put("tipoEvento", evento.getParTiposEvento().getNombreVa());
        } else {
            template.put("titulo", evento.getTituloEs());
            template.put("tipoEvento", evento.getParTiposEvento().getNombreEs());
        }

        Map<String, Map<Long, PreciosSesion>> preciosSesionLocalizacion;
        if (abonos) {
            preciosSesionLocalizacion = preciosService.getPreciosSesionAbonosPublicosPorLocalizacion(sesionId, userUID);
        } else {
            preciosSesionLocalizacion = preciosService.getPreciosSesionNormalPublicosPorLocalizacion(sesionId, userUID);
        }

        template.put("preciosSesionLocalizacion", preciosSesionLocalizacion);
        List<Localizacion> localizacionesSesion = localizacionesService.getLocalizacionesSesion(sesionId);
        template.put("localizaciones", localizacionesSesion);

        Map<String, String> butacasFragment = new HashMap<>();
        for (Localizacion localizacion : localizacionesSesion) {
            String nombreLocalizacion =
                language.equalsIgnoreCase("es") ? localizacion.getNombreEs() : localizacion.getNombreVa();
            butacasFragment.put(localizacion.getCodigo(), nombreLocalizacion);
        }
        template.put("localizacionNombre", butacasFragment);

        template.put("reservasPublicas", evento.getReservasPublicas());
        template.put("estilosOcupadas", butacasService.estilosButacasOcupadas(sesionId, localizacionesSesion, false));
        List<DisponiblesLocalizacion> disponibles = butacasService.getDisponibles(sesionId);
        List<DisponiblesLocalizacion> disponiblesConPrecios = disponibles.stream().filter(
                localizacionDisponible -> preciosSesionLocalizacion.containsKey(localizacionDisponible.getLocalizacion()))
            .collect(Collectors.toList());
        template.put("disponiblesNoNumerada", disponiblesConPrecios);

        template.put("gastosGestion", Float.parseFloat(configuration.getGastosGestion()));
        template.put("lang", language);
        if (error != null && !error.equals("")) {
            template.put("error", error);
        }
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        cine.serializeTemplate(template, mobile);

        ResponseBuilder builder = Response.ok(template);

        return Utils.noCache(builder).build();
    }

    private Response paginaSeleccionEntradasNoNumeradas(
        Sesion sesion,
        String error,
        boolean abonos,
        String userUID,
        boolean mobile
    ) throws Exception {
        String urlBase = getBaseUrlPublic();
        long sesionId = sesion.getId();
        if (!sesion.getEnPlazoVentaInternet())
            return paginaFueraDePlazo(sesionId, userUID, mobile);

        Evento evento = sesion.getEvento();

        String uuidCompra = (String) currentRequest.getSession().getAttribute(EntradasService.UUID_COMPRA);
        List<PreciosSesion> preciosSesion = preciosService.getPreciosSesionPublicos(sesionId, userUID);
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();
        Template template = new HTMLTemplate(
            Constantes.PLANTILLAS_DIR + sesion.getSala().getCine().getCodigo() + "/seleccionEntradaNoNumerada", locale,
            APP);
        template.put("evento", evento);
        template.put("sesion", sesion);
        template.put("texto", sesionesService.getFechaSesion(sesion, true, locale));
        template.put("idioma", language);
        template.put("baseUrl", getBaseUrlPublic());
        template.put("limiteEntradas", comprasService.getLimiteEntradas(evento));
        template.put("fecha", DateUtils.dateToSpanishString(sesion.getFechaCelebracion()));
        template.put("hora", sesion.getHoraCelebracion());
        template.put("pagina", publicPageBuilderInterface.buildPublicPageInfo(urlBase, urlBase, language.toString(),
            configurationSelector.getHtmlTitle()));
        Calendar cal = Calendar.getInstance();
        template.put("millis", cal.getTime().getTime());
        List<Tarifa> tarifas = new ArrayList<Tarifa>();

        if (sesion.getPlantillaPrecios() != null && sesion.getPlantillaPrecios().getId() != -1)
            tarifas = sesionesService.getTarifasPublicasConPrecioConPlantilla(sesionId);
        else
            tarifas = sesionesService.getTarifasPublicasConPrecioSinPlantilla(sesionId);

        template.put("tarifas", tarifas);
        if (error != null && !error.equals("")) {
            template.put("error", error);
        }

        if (uuidCompra != null) {
            template.put("uuidCompra", uuidCompra);
        }

        if (language.equals("ca")) {
            template.put("titulo", evento.getTituloVa());
        } else {
            template.put("titulo", evento.getTituloEs());
        }


        template.put("preciosSesion", preciosSesion);

        Map<String, Map<Long, PreciosSesion>> preciosSesionLocalizacion;
        if (abonos) {
            preciosSesionLocalizacion = preciosService.getPreciosSesionAbonosPublicosPorLocalizacion(sesionId, userUID);
        } else {
            preciosSesionLocalizacion = preciosService.getPreciosSesionNormalPublicosPorLocalizacion(sesionId, userUID);
        }
        template.put("preciosSesionLocalizacion", preciosSesionLocalizacion);

        List<DisponiblesLocalizacion> disponibles = butacasService.getDisponibles(sesionId);
        List<DisponiblesLocalizacion> disponiblesConPrecios = disponibles.stream().filter(
                localizacionDisponible -> preciosSesionLocalizacion.containsKey(localizacionDisponible.getLocalizacion()))
            .collect(Collectors.toList());

        Map<String, Integer> disponiblesLocalizacion = new HashMap<>();
        for (DisponiblesLocalizacion disponiblesConPrecio : disponiblesConPrecios) {
            disponiblesLocalizacion.put(disponiblesConPrecio.getLocalizacion(), disponiblesConPrecio.getDisponibles());
        }
        template.put("disponiblesLocalizacion", disponiblesLocalizacion);

        List<Localizacion> localizacionesSesion = localizacionesService.getLocalizacionesSesion(sesionId);
        localizacionesSesion = localizacionesSesion.stream()
            .filter(localizacionDisponible -> preciosSesionLocalizacion.containsKey(localizacionDisponible.getCodigo()))
            .collect(Collectors.toList());
        template.put("localizaciones", localizacionesSesion);

        template.put("gastosGestion", Float.parseFloat(configuration.getGastosGestion()));
        template.put("lang", language);
        template.put("showIVA", configurationSelector.showIVA());
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        cine.serializeTemplate(template, mobile);

        ResponseBuilder builder = Response.ok(template);

        return Utils.noCache(builder).build();
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Response compraEntradaHtml(
        @PathParam("id") Long sesionId,
        @FormParam("butacasSeleccionadas") String butacasSeleccionadasJSON,
        @FormParam("platea1Normal") String platea1Normal,
        @FormParam("platea1Descuento") String platea1Descuento,
        @FormParam("platea2Normal") String platea2Normal,
        @FormParam("platea2Descuento") String platea2Descuento,
        @FormParam("uuidCompra") String uuidCompra,
        @FormParam("b_t") String strButacas,
        @QueryParam("abonos") @DefaultValue("false") boolean abonos
    ) throws Exception {
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
        Sesion sesion = sesionesService.getSesion(sesionId, user.getUsuario());
        String language = LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage();
        boolean mobile = comprasService.isMobile(currentRequest);

        if (Utils.isAsientosNumerados(sesion.getEvento())) {
            currentRequest.getSession().setAttribute(EntradasService.BUTACAS_COMPRA, butacasSeleccionadasJSON);
            List<Butaca> butacasSeleccionadas = Butaca.parseaJSON(butacasSeleccionadasJSON);
            butacasService.cargarInformacionButacas(butacasSeleccionadas, language);
            List<Butaca> butacas = Butaca.getNumeradas(butacasSeleccionadas);
            List<Butaca> butacasNoNumeradas = Butaca.getNoNumeradas(butacasSeleccionadas);
            for (Butaca butacaNoNumerada : butacasNoNumeradas) {
                for (int i = 0; i < butacaNoNumerada.getCantidad(); i++) {
                    Butaca butaca = new Butaca(butacaNoNumerada.getLocalizacion(), butacaNoNumerada.getTipo());
                    butaca.setTexto(butacaNoNumerada.getTexto());
                    butacas.add(butaca);
                }
            }
            return compraEntradaNumeradaHtml(sesion, butacas, uuidCompra, abonos, user.getUsuario(), mobile);
        } else {
            butacasSeleccionadasJSON = String.format("[%s]", strButacas);
            currentRequest.getSession().setAttribute(EntradasService.BUTACAS_COMPRA, butacasSeleccionadasJSON);
            List<Butaca> butacas = Butaca.parseaJSON(butacasSeleccionadasJSON);
            butacasService.cargarInformacionButacas(butacas, language);
            return compraEntradaNoNumeradaHtml(sesion, butacas, uuidCompra, abonos, user.getUsuario(), mobile);
        }
    }

    private Response compraEntradaNumeradaHtml(
        Sesion sesion,
        List<Butaca> butacasSeleccionadas,
        String uuidCompra,
        boolean abonos,
        String userUID,
        boolean mobile
    ) throws Exception {
        Cine cine = usersService.getUserCineByUserUID(userUID);
        ResultadoCompra resultadoCompra;
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        butacasService.cargarInformacionButacas(butacasSeleccionadas, locale.getLanguage());
        try {
            Map<String, List<Butaca>> butacasExistentes = new HashMap<String, List<Butaca>>();
            for (Butaca butacaSeleccionada : butacasSeleccionadas) {
                if (butacaSeleccionada.getFila() != null && butacaSeleccionada.getNumero() != null) {
                    String localizacion = butacaSeleccionada.getLocalizacion();
                    if (!butacasExistentes.containsKey(localizacion)) {
                        byte[] encoded =
                            Files.readAllBytes(Paths.get(configuration.getPathJson() + localizacion + ".json"));
                        butacasExistentes.put(localizacion, Butaca.parseaJSON(new String(encoded, "UTF-8")));
                    }

                    if (!existeButaca(butacasExistentes.get(localizacion), butacaSeleccionada)) {
                        throw new CompraButacaNoExistente();
                    }
                }
            }

            resultadoCompra =
                comprasService.realizaCompraInternet(sesion.getId(), butacasSeleccionadas, uuidCompra, userUID);
        } catch (FueraDePlazoVentaInternetException e) {
            return paginaFueraDePlazo(sesion.getId(), userUID, mobile);
        } catch (ButacaOcupadaException e) {
            String error = ResourceProperties.getProperty(locale, "error.seleccionEntradas.ocupadas");
            return paginaSeleccionEntradasNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (CompraSinButacasException e) {
            String error = ResourceProperties.getProperty(locale, "error.seleccionEntradas.noSeleccionadas");
            return paginaSeleccionEntradasNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (CompraInvitacionPorInternetException e) {
            String error = ResourceProperties.getProperty(locale, "error.seleccionEntradas.invitacionPorInternet");
            return paginaSeleccionEntradasNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (CompraButacaDescuentoNoDisponible e) {
            String error =
                ResourceProperties.getProperty(locale, "error.seleccionEntradas.compraDescuentoNoDisponible");
            return paginaSeleccionEntradasNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (CompraButacaNoExistente e) {
            String error = ResourceProperties.getProperty(locale, "error.seleccionEntradas.compraButacaNoExistente");
            return paginaSeleccionEntradasNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (LimiteEntradasPorEmailSuperadoException e) {
            String error =
                ResourceProperties.getProperty(locale, "error.seleccionEntradas.limiteEntradasGratis", e.getLimite());
            return paginaSeleccionEntradasNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (LimiteEntradasGratisSuperadoException e) {
            String error = ResourceProperties.getProperty(locale, "error.seleccionEntradas.limiteEntradasGratis",
                cine.getLimiteEntradasGratuitasPorCompra());
            return paginaSeleccionEntradasNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (NoHayButacasLibresException e) {
            String error = "";
            try {
                error = ResourceProperties.getProperty(locale, "error.noHayButacasParaLocalizacion") + " "
                    + e.getLocalizacion();
            } catch (Exception ex) {
                error = ResourceProperties.getProperty(locale, "error.noHayButacasParaLocalizacion");
            }

            return paginaSeleccionEntradasNumeradas(sesion, error, abonos, userUID, mobile);
        }

        if (resultadoCompra.getCorrecta()) {
            currentRequest.getSession().setAttribute(EntradasService.UUID_COMPRA, resultadoCompra.getUuid());

            currentResponse.sendRedirect(
                getBaseUrlPublicLimpio() + "/rest/entrada/" + resultadoCompra.getUuid() + "/datosComprador");
            return null;
        } else {
            currentRequest.getSession().removeAttribute(EntradasService.BUTACAS_COMPRA);
            return paginaSeleccionEntradasNumeradas(sesion, null, abonos, userUID, mobile);
        }
    }

    private boolean existeButaca(
        List<Butaca> butacas,
        Butaca butacaSeleccionada
    ) {
        for (Butaca butaca : butacas) {
            if (butacaSeleccionada.getFila().equals(butaca.getFila()) && butacaSeleccionada.getNumero()
                .equals(butaca.getNumero())) {
                return true;
            }
        }
        return false;
    }

    private Response compraEntradaNoNumeradaHtml(
        Sesion sesion,
        List<Butaca> butacasSeleccionadas,
        String uuidCompra,
        boolean abonos,
        String userUID,
        boolean mobile
    ) throws Exception {
        long sesionId = sesion.getId();
        Cine cine = usersService.getUserCineByUserUID(userUID);
        ResultadoCompra resultadoCompra;
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        try {
            resultadoCompra = comprasService.realizaCompraInternet(sesionId, butacasSeleccionadas, uuidCompra, userUID);
        } catch (FueraDePlazoVentaInternetException e) {
            return paginaFueraDePlazo(sesionId, userUID, mobile);
        } catch (ButacaOcupadaException e) {
            String error = ResourceProperties.getProperty(locale, "error.seleccionEntradas.ocupadas");
            return paginaSeleccionEntradasNoNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (CompraSinButacasException e) {
            String error = ResourceProperties.getProperty(locale, "error.seleccionEntradas.noSeleccionadas");
            return paginaSeleccionEntradasNoNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (CompraButacaDescuentoNoDisponible e) {
            String error =
                ResourceProperties.getProperty(locale, "error.seleccionEntradas.compraDescuentoNoDisponible");
            return paginaSeleccionEntradasNoNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (NoHayButacasLibresException e) {
            String error = "";
            try {
                error = ResourceProperties.getProperty(locale, "error.noHayButacasParaLocalizacion") + " "
                    + e.getLocalizacion();
            } catch (Exception ex) {
                error = ResourceProperties.getProperty(locale, "error.noHayButacasParaLocalizacion");
            }

            return paginaSeleccionEntradasNoNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (LimiteEntradasGratisSuperadoException e) {
            String error = ResourceProperties.getProperty(locale, "error.seleccionEntradas.limiteEntradasGratis",
                cine.getLimiteEntradasGratuitasPorCompra());
            return paginaSeleccionEntradasNoNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (LimiteEntradasPorEmailSuperadoException e) {
            String error =
                ResourceProperties.getProperty(locale, "error.seleccionEntradas.limiteEntradasGratis", e.getLimite());
            return paginaSeleccionEntradasNoNumeradas(sesion, error, abonos, userUID, mobile);
        } catch (Exception e) {
            String error = ResourceProperties.getProperty(locale, "error.errorGeneral");
            return paginaSeleccionEntradasNoNumeradas(sesion, error, abonos, userUID, mobile);
        }

        if (resultadoCompra.getCorrecta()) {
            currentRequest.getSession().setAttribute(EntradasService.UUID_COMPRA, resultadoCompra.getUuid());

            currentResponse.sendRedirect(
                getBaseUrlPublicLimpio() + "/rest/entrada/" + resultadoCompra.getUuid() + "/datosComprador");
            return null;
        } else {
            return paginaSeleccionEntradasNoNumeradas(sesion, "", abonos, userUID, mobile);
        }
    }

    private Template getDatosCompradorTemplate(
        CompraDTO compra,
        String language,
        Locale locale,
        String uuidCompra,
        String nombre,
        String apellidos,
        String direccion,
        String poblacion,
        String cp,
        String provincia,
        String telefono,
        String email,
        String infoPeriodica,
        String condicionesPrivacidad,
        String condicionesContratacion,
        String condicionesCancelacion,
        boolean mobile,
        String emailParam,
        String error
    ) throws ParseException {
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/datosComprador", locale, APP);
        String urlBase = getBaseUrlPublic();
        String url = currentRequest.getRequestURL().toString();
        template.put("pagina", publicPageBuilderInterface.buildPublicPageInfo(urlBase, url, language.toString(),
            configurationSelector.getHtmlTitle()));
        template.put("baseUrl", getBaseUrlPublic());

        template.put("idioma", language);
        template.put("lang", language);

        template.put("uuidCompra", uuidCompra);

        template.put("nombre", nombre);
        template.put("apellidos", apellidos);
        template.put("direccion", direccion);
        template.put("poblacion", poblacion);
        template.put("cp", cp);
        template.put("provincia", provincia);
        template.put("telefono", telefono);
        template.put("email", email);
        template.put("condicionesPrivacidad", condicionesPrivacidad);
        template.put("condicionesContratacion", condicionesContratacion);
        template.put("condicionesCancelacion", condicionesCancelacion);

        template.put("urlCondicionesPrivacidad", configurationSelector.getUrlCondicionesPrivacidad());
        template.put("urlCondicionesCancelacion", configurationSelector.getUrlCondicionesCancelacion());

        GastosGestion gastosGestion = PrecioUtils.getGastosGestion(compra.getImporte(), compra.getImporteSinComision());
        template.put("total", ReportUtils.formatEuros(compra.getImporte()));
        template.put("totalSinComision", ReportUtils.formatEuros(compra.getImporteSinComision()));
        template.put("gastosGestion", ReportUtils.formatEuros(gastosGestion.getGastosGestion()));
        template.put("gastosGestionSinIVA", ReportUtils.formatEuros(gastosGestion.getGastosGestionSinIVA()));
        template.put("IVAGastosGestion", ReportUtils.formatEuros(gastosGestion.getIVAgastosGestion()));
        if (configurationSelector.isShowComoNosConocisteEnabled()) {
            template.put("motivos", comprasService.getMotivosComoNosConociste(cine.getId()));
        }

        if (compra != null) {
            if (compra.getParSesion() != null && compra.getParSesion().getParEvento() != null) {
                EventoDTO parEvento = compra.getParSesion().getParEvento();
                TpvsDTO parTpv = parEvento.getParTpv();
                if (parTpv != null) {
                    template.put("tpvNombre", parTpv.getNombre());
                    template.put("tpvCif", parTpv.getCif());
                    template.put("tpvDireccion", parTpv.getDireccion());
                }

                if (language.equals("ca"))
                    template.put("tipoEvento", parEvento.getParTiposEvento().getNombreVa());
                else
                    template.put("tipoEvento", parEvento.getParTiposEvento().getNombreEs());

                if (parEvento.getEntradasNominales() != null && parEvento.getEntradasNominales()) {
                    List<ButacaDTO> butacas = compra.getParButacas();
                    List<Long> sesionesIds = butacasService.getSesionesButacas(butacas);
                    template.put("butacas", butacasService.getButacas(
                        butacas.stream().filter(b -> b.getParSesion().getId() == sesionesIds.get(0))
                            .collect(Collectors.toList()), language));
                }

                template.put("eventoId", parEvento.getId());
            }

            if (infoPeriodica == null || infoPeriodica.equals(""))
                infoPeriodica = "no";

            template.put("infoPeriodica", infoPeriodica);
        } else {
            error = ResourceProperties.getProperty(locale, "error.compraCaducada");
        }

        if (error != null && !error.equals("")) {
            template.put("error", error);
        }

        cine.serializeTemplate(template, mobile);

        if (!Strings.isNullOrEmpty(emailParam)) {
            template.put("emailParam", emailParam);
        }

        return template;
    }

    @GET
    @Path("{uuidCompra}/datosComprador")
    @Produces(MediaType.TEXT_HTML)
    public Response rellenaDatosComprador(
        @PathParam("uuidCompra") String uuidCompra
    ) throws Exception {
        CompraDTO compra = comprasService.getCompraByUuid(uuidCompra);

        if (compra != null) {
            Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
            String language = locale.getLanguage();

            Template template =
                getDatosCompradorTemplate(compra, language, locale, uuidCompra, null, null, null, null, null, null,
                    null, null, null, null, null, null, comprasService.isMobile(currentRequest),
                    comprasService.getSessionEmail(currentRequest), null);
            return Response.ok(template).build();
        } else {
            return Response.seeOther(new URI(configurationSelector.getUrlPublic() + "/..")).build();
        }
    }

    @POST
    @Path("{uuidCompra}/datosComprador")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Response guardaDatosComprador(
        @PathParam("uuidCompra") String uuidCompra,
        @FormParam("nombre") String nombre,
        @FormParam("apellidos") String apellidos,
        @FormParam("direccion") String direccion,
        @FormParam("poblacion") String poblacion,
        @FormParam("cp") String cp,
        @FormParam("provincia") String provincia,
        @FormParam("telefono") String telefono,
        @FormParam("email") String email,
        @FormParam("emailVerificacion") String emailVerificacion,
        @FormParam("comoNosConociste") Integer comoNosConocisteId,
        @FormParam("infoPeriodica") String infoPeriodica,
        @FormParam("condicionesPrivacidad") String condicionesPrivacidad,
        @FormParam("condicionesContratacion") String condicionesContratacion,
        @FormParam("condicionesCancelacion") String condicionesCancelacion,
        @FormParam("nombresEntradas") List<String> nombresEntradas
    ) throws Exception {
        boolean mobile = comprasService.isMobile(currentRequest);
        String emailParam = comprasService.getSessionEmail(currentRequest);
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();

        CompraDTO compra = comprasService.getCompraByUuid(uuidCompra);
        if (compra != null && !compra.getPagada()) {
            String error = null;
            try {
                compra = comprasService.rellenaDatosComprador(uuidCompra, nombre, apellidos, direccion, poblacion, cp,
                    provincia, telefono, email, emailVerificacion, comoNosConocisteId, infoPeriodica,
                    condicionesPrivacidad, configurationSelector.isContratoGeneralEnabled(), condicionesContratacion,
                    configurationSelector.getUrlCondicionesCancelacion(), condicionesCancelacion, nombresEntradas);
            } catch (NoAbonadoException e) {
                error = ResourceProperties.getProperty(locale, "error.datosComprador.abonado");
            } catch (AbonoUsadoMaxEnEventoException e) {
                error = String.format("%s",
                    ResourceProperties.getProperty(locale, "error.datosComprador.abonoUsadoEvento"));
            } catch (AbonoUsadoMaxEnDistintosEventosException e) {
                error = ResourceProperties.getProperty(locale, "error.datosComprador.abonoMaximosUsos");
            } catch (CampoRequeridoException e) {
                error =
                    ResourceProperties.getProperty(locale, String.format("error.datosComprador.%s", e.getMessage()));
            } catch (EmailVerificacionException e) {
                error = ResourceProperties.getProperty(locale, "error.datosComprador.emailVerificacion");
            } catch (EmailBlacklistException e) {
                error = ResourceProperties.getProperty(locale, "error.datosComprador.emailInvalido");
            } catch (CompraCaducadaException e) {
                error = ResourceProperties.getProperty(locale, "error.datosComprador.compraCaducada");
            } catch (EventoNominalException e) {
                error = ResourceProperties.getProperty(locale, "error.datosComprador.nominales");
            } catch (LimiteEntradasPorEmailSuperadoException e) {
                error =
                    String.format(ResourceProperties.getProperty(locale, "error.datosComprador.limiteEntradasPorEmail"),
                        e.getLimite());
            }

            Template template;
            if (!Strings.isNullOrEmpty(error)) {
                template = getDatosCompradorTemplate(compra, language, locale, uuidCompra, nombre, apellidos, direccion,
                    poblacion, cp, provincia, telefono, email, infoPeriodica, condicionesPrivacidad,
                    condicionesContratacion, condicionesCancelacion, mobile, emailParam, error);
                return Response.ok(template).build();
            } else {
                template = getPagoTemplate(email, mobile, emailParam, compra, locale, language);

                return Response.ok(template).build();
            }
        } else {
            return Response.seeOther(new URI(configurationSelector.getUrlPublic() + "/..")).build();
        }
    }

    private Template getPagoTemplate(
        @FormParam("email") String email,
        boolean mobile,
        String emailParam,
        CompraDTO compra,
        Locale locale,
        String language
    ) throws Exception {
        Template template;
        String url = currentRequest.getRequestURL().toString();
        if (configuration.isDebug()) {
            comprasService.eliminaCompraDeSesion(currentRequest);
            template = tpvInterface.testTPV(compra.getId(), url, locale, mobile);
        } else if (compra.getImporte().compareTo(BigDecimal.ZERO) == 0) {
            comprasService.eliminaCompraDeSesion(currentRequest);
            template = tpvInterface.compraGratuita(compra.getId(), url, locale, mobile);
        } else {
            TpvsDTO parTpv = compra.getParSesion().getParEvento().getParTpv();
            String tpvSignatureMethod = parTpv.getSignatureMethod();
            if (tpvSignatureMethod != null && tpvSignatureMethod.equals(SignatureTPV.HMAC_SHA256_V1.toString())) {
                template = getSha2Template(locale, parTpv, compra, email, language);
            } else if (tpvSignatureMethod != null && (tpvSignatureMethod.equals(SignatureTPV.CECA_SHA1.toString())
                || tpvSignatureMethod.equals(SignatureTPV.CECA_SHA2.toString()))) {
                template = getCecaShaTemplate(locale, parTpv, compra, language, tpvSignatureMethod);
            } else if (tpvSignatureMethod != null && tpvSignatureMethod.equals(SignatureTPV.PAYNOPAIN.toString())) {
                template = getPayNoPainTemplate(locale, parTpv, compra);
            } else {
                template = getSha1Template(locale, parTpv, compra, email, language, mobile);
            }

            String urlPago = parTpv.getUrl();
            if (urlPago != null)
                template.put("urlPago", urlPago);
        }
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        cine.serializeTemplate(template, mobile);

        if (!Strings.isNullOrEmpty(emailParam)) {
            template.put("emailParam", emailParam);
        }
        return template;
    }

    private Template getSha2Template(
        Locale locale,
        TpvsDTO parTpv,
        CompraDTO compra,
        String email,
        String language
    ) throws Exception {

        String identificador = idTPVInterface.getFormattedId(compra.getId());
        String urlOk = getBaseUrlPublic() + "/rest/tpv/oksha2";
        String urlKo = getBaseUrlPublic() + "/rest/tpv/kosha2";

        Template template = new HTMLTemplate(Constantes.PLANTILLAS_DIR + "tpv_sha2", locale, APP);
        Pair<String, String> parametrosYFirma =
            hmacSha256TPVInterface.getParametrosYFirma(PrecioUtils.monedaToCents(compra.getImporte()),
                parTpv.getOrderPrefix() + identificador, parTpv.getCode(), parTpv.getCurrency(),
                parTpv.getTransactionCode(), parTpv.getTerminal(), parTpv.getWsdlUrl(), urlOk, urlKo, email,
                language.equals("ca") ? parTpv.getLangCaCode() : parTpv.getLangEsCode(), identificador,
                StringUtils.stripAccents(compra.getParSesion().getParEvento().getTituloVa().toUpperCase()),
                parTpv.getNombre(), parTpv.getSecret());

        template.put("params", parametrosYFirma.getFirst());
        template.put("signature", parametrosYFirma.getSecond());
        return template;
    }

    private Template getCecaShaTemplate(
        Locale locale,
        TpvsDTO parTpv,
        CompraDTO compra,
        String language,
        String signatureTPV
    ) {
        String Clave_encriptacion = parTpv.getSecret();

        String compraId = idTPVInterface.getFormattedId(compra.getId());

        String AcquirerBIN = parTpv.getOrderPrefix();
        String MerchantID = parTpv.getCode();
        String TerminalID = parTpv.getTerminal();

        String Sign_Param = Utils.sha1(Clave_encriptacion + compraId + AcquirerBIN + MerchantID + TerminalID);
        String params = "?order=" + compraId + "&uuid=" + Sign_Param;
        String URL_OK = getBaseUrlPublic() + "/rest/tpv/ceca/ok" + params;
        String URL_NOK = getBaseUrlPublic() + "/rest/tpv/ceca/ko" + params;

        String Num_operacion = compraId;
        String Importe = PrecioUtils.monedaToCents(compra.getImporte());
        String TipoMoneda = parTpv.getCurrency();
        String Exponente = parTpv.getTransactionCode();
        String Pago_soportado = "SSL";
        String Idioma = language.equals("ca") ? parTpv.getLangCaCode() : parTpv.getLangEsCode();
        String Descripcion = StringUtils.stripAccents(compra.getParSesion().getParEvento().getTituloVa().toUpperCase());

        String url = parTpv.getWsdlUrl();

        String Firma;
        String Cifrado;
        if (signatureTPV.equals(SignatureTPV.CECA_SHA2.toString())) {
            Cifrado = "SHA2";
            Firma = Utils.sha2(
                Clave_encriptacion + MerchantID + AcquirerBIN + TerminalID + Num_operacion + Importe + TipoMoneda
                    + Exponente + Cifrado + URL_OK + URL_NOK);
        } else {
            Cifrado = "SHA1";
            Firma = Utils.sha1(
                Clave_encriptacion + MerchantID + AcquirerBIN + TerminalID + Num_operacion + Importe + TipoMoneda
                    + Exponente + Cifrado + URL_OK + URL_NOK);
        }

        Template template = new HTMLTemplate(Constantes.PLANTILLAS_DIR + "tpv_ceca", locale, APP);
        template.put("AcquirerBIN", AcquirerBIN);
        template.put("MerchantID", MerchantID);
        template.put("TerminalID", TerminalID);
        template.put("URL_OK", URL_OK);
        template.put("URL_NOK", URL_NOK);
        template.put("Firma", Firma);
        template.put("Cifrado", Cifrado);
        template.put("Num_operacion", Num_operacion);
        template.put("Importe", Importe);
        template.put("TipoMoneda", TipoMoneda);
        template.put("Exponente", Exponente);
        template.put("Pago_soportado", Pago_soportado);
        template.put("Idioma", Idioma);
        template.put("Descripcion", Descripcion);

        template.put("urlPago", url);

        return template;
    }

    private Template getPayNoPainTemplate(
        Locale locale,
        TpvsDTO parTpv,
        CompraDTO compra
    ) {
        String compraId = idTPVInterface.getFormattedId(compra.getId());
        String firma = Utils.sha1(parTpv.getSecret() + parTpv.getCode() + parTpv.getOrderPrefix() + compraId);

        Template template = new HTMLTemplate(Constantes.PLANTILLAS_DIR + "tpv_paynopain", locale, APP);
        template.put("compraId", compraId);
        template.put("firma", firma);

        return template;
    }

    private Template getSha1Template(
        Locale locale,
        TpvsDTO parTpv,
        CompraDTO compra,
        String email,
        String language,
        boolean mobile
    ) {
        String importe = PrecioUtils.monedaToCents(compra.getImporte());
        String identificador = idTPVInterface.getFormattedId(compra.getId());
        String order = parTpv.getOrderPrefix() + identificador;
        String tpvCode = parTpv.getCode();
        String tpvCurrency = parTpv.getCurrency();
        String tpvTransaction = parTpv.getTransactionCode();
        String tpvTerminal = parTpv.getTerminal();
        String tpvNombre = parTpv.getNombre();
        String secret = parTpv.getSecret();
        String url = parTpv.getWsdlUrl();
        String urlOk = getBaseUrlPublic() + "/rest/tpv/ok";
        String urlKo = getBaseUrlPublic() + "/rest/tpv/ko";
        String concepto = StringUtils.stripAccents(compra.getParSesion().getParEvento().getTituloVa().toUpperCase());

        Template template = new HTMLTemplate(Constantes.PLANTILLAS_DIR + "tpv", locale, APP);
        template.put("idioma", language);
        template.put("lang", language);
        template.put("baseUrl", getBaseUrlPublic());
        template.put("identificador", identificador);
        template.put("concepto", concepto);
        template.put("importe", importe);
        template.put("correo", email);
        template.put("url", url);
        template.put("hash", Utils.sha1(identificador + importe + email + url + secret));
        template.put("order", order);
        template.put("urlOk", urlOk);
        template.put("urlKo", urlKo);

        if (language.equals("ca"))
            template.put("langCode", parTpv.getLangCaCode());
        else
            template.put("langCode", parTpv.getLangEsCode());

        if (tpvCode != null && tpvCurrency != null && tpvTransaction != null && tpvTerminal != null
            && tpvNombre != null) {
            String date = new SimpleDateFormat("YYMMddHHmmss").format(new Date());
            template.put("date", date);
            template.put("currency", tpvCurrency);
            template.put("code", tpvCode);
            template.put("terminal", tpvTerminal);
            template.put("transaction", tpvTransaction);
            template.put("nombre", tpvNombre);

            String shaEnvio =
                sha1TPVInterface.getFirma(importe, parTpv.getOrderPrefix(), identificador, tpvCode, tpvCurrency,
                    tpvTransaction, url, secret, date);

            template.put("hashcajamar", shaEnvio);
        }

        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        cine.serializeTemplate(template, mobile);

        return template;
    }

    private Response paginaFueraDePlazo(
        Long sesionId,
        String userUID,
        boolean mobile
    ) throws Exception {
        Sesion sesion = sesionesService.getSesion(sesionId, userUID);

        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();

        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + sesion.getSala().getCine().getCodigo() + "/compraFinalizada",
                locale, APP);
        String urlBase = getBaseUrlPublic();
        String url = currentRequest.getRequestURL().toString();
        template.put("pagina", publicPageBuilderInterface.buildPublicPageInfo(urlBase, url, language.toString(),
            configurationSelector.getHtmlTitle()));
        template.put("baseUrl", getBaseUrlPublic());

        template.put("idioma", language);
        template.put("lang", language);
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        cine.serializeTemplate(template, mobile);

        return Response.ok(template).build();
    }

    @POST
    @Path("{id}/ocupadas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Butaca> estadoButaca(
        @PathParam("id") Integer idSesion,
        EstadoButacasRequest params
    ) {
        return butacasService.estanOcupadas(Long.valueOf(idSesion), params.getButacas(), params.getUuidCompra());
    }

    @GET
    @Path("{id}/compra/{fila}/{butaca}/{localizacion}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Compra getCompra(
        @PathParam("id") long idSesion,
        @PathParam("fila") String fila,
        @PathParam("butaca") String butaca,
        @PathParam("localizacion") String localizacion
    ) {
        return butacasService.getCompra(idSesion, localizacion, fila, butaca);
    }

    @GET
    @Path("{id}/precios")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPreciosSesion(@PathParam("id") Long sesionId) {
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());

        return Response.ok().entity(new RestResponse(true, preciosService.getPreciosSesion(sesionId, user.getUsuario()),
            sesionesService.getTotalPreciosSesion(sesionId))).build();
    }

    @GET
    @Path("{id}/plantillareservas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlantillasReservasSesion(@PathParam("id") Long sesionId) {
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());

        List<Plantilla> plantillasReservas =
            plantillasReservasService.getBySesion(sesionId, null, 0, Integer.MAX_VALUE, user.getUsuario());
        return Response.ok().entity(new RestResponse(true, plantillasReservas, plantillasReservas.size())).build();
    }

    @GET
    @Path("plantillareservas/{plantillaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getButacasPlantillasReservas(@PathParam("plantillaId") Long plantillaId) {
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();

        List<ButacaMap> butacas =
            plantillasButacasReservasService.getButacasMapOfPlantilla(plantillaId, null, 0, Integer.MAX_VALUE,
                language);
        return Response.ok().entity(new RestResponse(true, butacas, butacas.size())).build();
    }

    @GET
    @Path("butacasFragment/{id}")
    @Produces(MediaType.TEXT_HTML)
    public Response butacasFragment(
        @PathParam("id") long sesionId,
        @QueryParam("reserva") String reserva,
        @QueryParam("if") String isAdmin
    ) {
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
        String userUID = user.getUsuario();

        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();

        Sesion sesion = sesionesService.getSesion(sesionId, userUID);
        HTMLTemplate template = new HTMLTemplate(
            Constantes.PLANTILLAS_DIR + sesion.getSala().getCine().getCodigo() + "/" + sesion.getSala()
                .getHtmlTemplateName(), locale, APP);

        Evento evento = sesion.getEvento();

        template.put("baseUrl", getBaseUrlPublic());
        template.put("idioma", language);
        template.put("lang", language);
        template.put("payModes", configurationSelector.getPayModes(locale));
        template.put("evento", evento);
        template.put("sesion", sesion);
        template.put("fecha", DateUtils.dateToSpanishString(sesion.getFechaCelebracion()));
        template.put("hora", sesion.getHoraCelebracion());
        template.put("ocultaComprar", "true");
        template.put("gastosGestion", 0.0);
        template.put("modoReserva", reserva != null && reserva.equals("true"));
        template.put("estilopublico", "false");
        template.put("muestraReservadas", true);
        template.put("modoAdmin", true);
        template.put("tipoEventoEs", sesion.getEvento().getParTiposEvento().getNombreEs());
        Calendar cal = Calendar.getInstance();
        template.put("millis", cal.getTime().getTime());
        List<Tarifa> tarifas = new ArrayList<Tarifa>();

        List<Localizacion> localizacionesSesion = localizacionesService.getLocalizacionesSesion(sesionId);
        template.put("localizaciones", localizacionesSesion);
        Map<String, String> butacasFragment = new HashMap<>();
        for (Localizacion localizacion : localizacionesSesion) {
            String nombreLocalizacion =
                language.equalsIgnoreCase("es") ? localizacion.getNombreEs() : localizacion.getNombreVa();
            butacasFragment.put(localizacion.getCodigo(), nombreLocalizacion);
        }
        template.put("localizacionNombre", butacasFragment);
        template.put("reservasPublicas", evento.getReservasPublicas());
        template.put("estilosOcupadas",
            butacasService.estilosButacasOcupadas(sesionId, localizacionesSesion, isAdmin.equals("true")));

        if (sesion.getPlantillaPrecios() != null && sesion.getPlantillaPrecios().getId() != -1)
            tarifas = sesionesService.getTarifasConPrecioConPlantilla(sesionId);
        else
            tarifas = sesionesService.getTarifasConPrecioSinPlantilla(sesionId);

        template.put("tarifas", tarifas);

        if (language.equals("ca")) {
            template.put("titulo", sesion.getEvento().getTituloVa());
        } else {
            template.put("titulo", sesion.getEvento().getTituloEs());
        }

        template.put("butacasSesion", "[]");

        Map<String, Map<Long, PreciosSesion>> preciosSesionLocalizacion =
            preciosService.getPreciosSesionNormalPorLocalizacion(sesion.getId(), userUID);
        template.put("preciosSesionLocalizacion", preciosSesionLocalizacion);

        List<DisponiblesLocalizacion> disponibles = butacasService.getDisponibles(sesion.getId());
        List<DisponiblesLocalizacion> disponiblesConPrecios = disponibles.stream().filter(
                localizacionDisponible -> preciosSesionLocalizacion.containsKey(localizacionDisponible.getLocalizacion()))
            .collect(Collectors.toList());
        template.put("disponiblesNoNumerada", disponiblesConPrecios);

        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        cine.serializeTemplate(template, comprasService.isMobile(currentRequest));

        return Response.ok().entity(template).header("Content-Type", "text/html; charset=utf-8").build();
    }

    @GET
    @Path("butacasFragment/reservas/{id}")
    @Produces(MediaType.TEXT_HTML)
    public Response butacasFragment(
        @PathParam("id") long plantillaId
    ) {
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();

        Plantilla plantilla = plantillasReservasService.getById(plantillaId);

        HTMLTemplate template = new HTMLTemplate(
            Constantes.PLANTILLAS_DIR + plantilla.getSala().getCine().getCodigo() + "/" + plantilla.getSala()
                .getHtmlTemplateName(), locale, APP);

        template.put("baseUrl", getBaseUrlPublic());
        template.put("idioma", language);
        template.put("lang", language);
        template.put("payModes", configurationSelector.getPayModes(locale));
        template.put("ocultaComprar", "true");
        template.put("gastosGestion", 0.0);
        template.put("modoPlantilla", true);
        template.put("modoReserva", true);
        template.put("estilopublico", "false");
        template.put("muestraReservadas", true);
        template.put("modoAdmin", true);
        Calendar cal = Calendar.getInstance();
        template.put("millis", cal.getTime().getTime());

        List<Localizacion> localizacionesSesion =
            localizacionesService.get(null, 0, Integer.MAX_VALUE, (int) plantilla.getSala().getId());
        template.put("localizaciones", localizacionesSesion);
        Map<String, String> butacasFragment = new HashMap<>();
        for (Localizacion localizacion : localizacionesSesion) {
            String nombreLocalizacion =
                language.equalsIgnoreCase("es") ? localizacion.getNombreEs() : localizacion.getNombreVa();
            butacasFragment.put(localizacion.getCodigo(), nombreLocalizacion);
        }
        template.put("localizacionNombre", butacasFragment);
        template.put("reservasPublicas", false);
        template.put("estilosOcupadas", new HashMap<>());

        template.put("sesion", new Sesion(0));
        template.put("butacasSesion", Butaca.toJSON(
            plantillasButacasReservasService.getButacasOfPlantilla(plantillaId, null, 0, Integer.MAX_VALUE, language)));

        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        cine.serializeTemplate(template, comprasService.isMobile(currentRequest));

        return Response.ok().entity(template).header("Content-Type", "text/html; charset=utf-8").build();
    }

    @GET
    @Path("butacasFragment/salas/{id}")
    @Produces(MediaType.TEXT_HTML)
    public Response butacasSalaFragment(
        @PathParam("id") long salaId
    ) {
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();

        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
        String userUID = user.getUsuario();
        Sala sala = salasService.getSalas(userUID).stream().filter(s -> s.getId() == salaId).findAny().orElse(null);

        HTMLTemplate template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + sala.getCine().getCodigo() + "/" + sala.getHtmlTemplateName(),
                locale, APP);

        template.put("baseUrl", getBaseUrlPublic());
        template.put("idioma", language);
        template.put("lang", language);
        template.put("payModes", configurationSelector.getPayModes(locale));
        template.put("ocultaComprar", "true");
        template.put("gastosGestion", 0.0);
        template.put("modoPlantilla", true);
        template.put("modoReserva", true);
        template.put("estilopublico", "false");
        template.put("muestraReservadas", true);
        template.put("modoAdmin", true);
        Calendar cal = Calendar.getInstance();
        template.put("millis", cal.getTime().getTime());

        List<Localizacion> localizacionesSesion =
            localizacionesService.get(null, 0, Integer.MAX_VALUE, (int) sala.getId());
        template.put("localizaciones", localizacionesSesion);
        Map<String, String> butacasFragment = new HashMap<>();
        for (Localizacion localizacion : localizacionesSesion) {
            String nombreLocalizacion =
                language.equalsIgnoreCase("es") ? localizacion.getNombreEs() : localizacion.getNombreVa();
            butacasFragment.put(localizacion.getCodigo(), nombreLocalizacion);
        }
        template.put("localizacionNombre", butacasFragment);
        template.put("reservasPublicas", false);
        template.put("estilosOcupadas", new HashMap<>());
        template.put("sesion", new Sesion(0));
        template.put("butacasSesion", Butaca.toJSON(Collections.emptyList()));

        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());
        cine.serializeTemplate(template, comprasService.isMobile(currentRequest));

        return Response.ok().entity(template).header("Content-Type", "text/html; charset=utf-8").build();
    }
}
