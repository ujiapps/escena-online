package es.uji.apps.par.services.rest;

import com.google.zxing.WriterException;

import com.sun.jersey.api.core.InjectParam;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import es.uji.apps.par.services.BarcodeService;
import es.uji.apps.par.utils.Utils;

@Path("barcode")
public class BarcodeResource extends BaseResource {
    @InjectParam
    private BarcodeService barcodeService;

    @Context
    HttpServletResponse currentResponse;

    @GET
    @Produces("image/png")
    public Response datosEntradaWithQueryParam(
        @QueryParam("text") String text,
        @QueryParam("size") String size
    ) throws Exception {
        return getQR(text, size);
    }

    @GET
    @Path("{text}")
    @Produces("image/png")
    public Response datosEntrada(
        @PathParam("text") String text,
        @QueryParam("size") String size
    ) throws Exception {
        return getQR(text, size);
    }

    private Response getQR(
        String text,
        String size
    ) throws WriterException, IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        barcodeService.generaBarcodeQr(text, size, bos);

        ResponseBuilder builder = Response.ok(bos.toByteArray());

        return Utils.noCache(builder).build();
    }
}
