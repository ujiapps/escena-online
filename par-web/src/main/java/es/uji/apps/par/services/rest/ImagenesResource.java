package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import es.uji.apps.par.exceptions.EventoNoEncontradoException;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.services.UsersService;

@Path("imagenes")
public class ImagenesResource extends BaseResource
{
    @InjectParam
    private UsersService usersService;

    @Context
    ServletContext context;

    @GET
    @Path("cine/logo")
    public Response getCineLogo() {
        try {
            Cine cine = usersService.getUserCineConImagenByServerName(currentRequest.getServerName());

            return Response.ok(cine.getLogo()).type(cine.getLogoContentType()).build();
        } catch (EventoNoEncontradoException e) {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("cine/banner")
    public Response getCineBanner() {
        try {
            Cine cine = usersService.getUserCineConImagenByServerName(currentRequest.getServerName());

            return Response.ok(cine.getBanner()).type(cine.getBannerContentType()).build();
        } catch (EventoNoEncontradoException e) {
            return Response.noContent().build();
        }
    }
}