package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import es.uji.apps.par.builders.PublicPageBuilderInterface;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.exceptions.TiempoMinioRequeridoParaAnularDesdeEnlace;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.ComprasService;
import es.uji.apps.par.services.EntradasService;
import es.uji.apps.par.services.PassbookService;
import es.uji.apps.par.services.UsersService;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.LocaleUtils;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;

@Path("compra")
public class ComprasResource extends BaseResource
{
    private static final Logger log = LoggerFactory.getLogger(ComprasResource.class);
    @InjectParam
    EntradasService entradasService;

    @InjectParam
    UsersService usersService;

    @Context
    HttpServletResponse currentResponse;

    @InjectParam
    ComprasService comprasService;

    @InjectParam
    PassbookService passbookService;

    @InjectParam
    private PublicPageBuilderInterface publicPageBuilderInterface;

    @GET
    @Path("{id}/pdf")
    @Produces("application/pdf")
    public Response datosEntrada(@PathParam("id") String uuidCompra) throws Exception
    {
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        entradasService.generaEntrada(uuidCompra, bos, user.getUsuario(), configurationSelector.getUrlPublic(), configurationSelector.getUrlPieEntrada());

        Response response = Response.ok(bos.toByteArray())
                .header("Cache-Control", "no-cache, no-store, must-revalidate")
                .header("Pragma", "no-cache")
                .header("Expires", "0")
                .header("Content-Disposition","attachment; filename =\"entrada_" + uuidCompra + ".pdf\"")
                .build();

        return response;
    }

    @GET
    @Path("{uuidCompra}/{idButaca}/passbook")
    public Response getPassbook(
        @PathParam("uuidCompra") String uuidCompra,
        @PathParam("idButaca") Long idButaca
    ) throws Exception
    {
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        boolean existeCompraYButacaYSeCorresponden = comprasService.existeCompraButaca(uuidCompra, idButaca);
        if (existeCompraYButacaYSeCorresponden) {
            passbookService.generaEntradaPassbook(uuidCompra, idButaca, bos, user.getUsuario(), configurationSelector.getUrlPublic(), configurationSelector.getUrlPieEntrada());

            Response response =
                Response.ok(bos.toByteArray()).header("Cache-Control", "no-cache, no-store, must-revalidate")
                    .header("Pragma", "no-cache").header("Expires", "0")
                    .header("Content-Type", "application/vnd.apple.pkpass")
                    .header("Content-Disposition", "attachment; filename =\"passbook_" + uuidCompra + ".pkpass\"").build();

            return response;
        } else {
            log.warn(String.format("Compra no existente con uuid %s-%s", uuidCompra, idButaca));
            return Response.status(404).build();
        }
    }

    @GET
    @Path("{uuidCompra}/{emailBase64}/anular")
    public Response verAnularCompra(
        @PathParam("uuidCompra") String uuidCompra,
        @PathParam("emailBase64") String emailBase64
    ) throws ParseException {
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());

        CompraDTO compra = comprasService.getCompraByUuid(uuidCompra);
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();
        String urlBase = getBaseUrlPublic();
        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/anular",
                locale, APP);
        template.put("evento", language.equals("ca") ? compra.getParSesion().getParEvento().getTituloVa() : compra.getParSesion().getParEvento().getTituloEs());
        template.put("fecha", DateUtils.dateToSpanishString(compra.getParSesion().getFechaCelebracion()));
        template.put("url", currentRequest.getRequestURL());
        template.put("pagina", publicPageBuilderInterface
            .buildPublicPageInfo(urlBase, urlBase, language, configurationSelector.getHtmlTitle()));
        template.put("baseUrl", getBaseUrlPublic());
        template.put("lang", language);

        template.put("title", ResourceProperties.getProperty(locale, "mail.entradas.botonCancelarCompra.html"));
        template.put("compra", true);
        return Response.ok(template).build();
    }

    @POST
    @Path("{uuidCompra}/{emailBase64}/anular")
    public Response anularCompra(
        @PathParam("uuidCompra") String uuidCompra,
        @PathParam("emailBase64") String emailBase64
    ) throws ParseException {
        Assert.notNull(uuidCompra);
        Assert.notNull(emailBase64);

        String email = new String(Base64.decode(emailBase64));
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());

        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();
        String urlBase = getBaseUrlPublic();
        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/anular",
                locale, APP);
        template.put("pagina", publicPageBuilderInterface
            .buildPublicPageInfo(urlBase, urlBase, language, configurationSelector.getHtmlTitle()));
        template.put("baseUrl", getBaseUrlPublic());
        template.put("lang", language);

        template.put("title", ResourceProperties.getProperty(locale, "mail.entradas.botonCancelarCompra.html"));
        try {
            comprasService.anularCompraDesdeMail(uuidCompra, email);
            template.put("resultado", String.format(ResourceProperties.getProperty(locale, "mail.entradas.botonCancelarCompra.ok"), cine.getEmail()));
        }
        catch (TiempoMinioRequeridoParaAnularDesdeEnlace e) {
            log.warn(e.getMessage(), e);
            template.put("resultado", String.format(ResourceProperties.getProperty(locale, "mail.entradas.botonCancelarCompra.ko"), cine.getEmail()));
        }
        catch (Throwable e) {
            log.error(e.getMessage(), e);
            template.put("resultado", String.format(ResourceProperties.getProperty(locale, "mail.entradas.botonCancelarCompra.ko"), cine.getEmail()));
        }
        return Response.ok(template).build();
    }

    @GET
    @Path("{uuidCompra}/butaca/{idButaca}/{emailBase64}/anular")
    public Response verAnularEntrada(
        @PathParam("uuidCompra") String uuidCompra,
        @PathParam("idButaca") long idButaca,
        @PathParam("emailBase64") String emailBase64
    ) throws ParseException {
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());

        CompraDTO compra = comprasService.getCompraByUuid(uuidCompra);
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();
        String urlBase = getBaseUrlPublic();
        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/anular",
                locale, APP);
        template.put("evento", language.equals("ca") ? compra.getParSesion().getParEvento().getTituloVa() : compra.getParSesion().getParEvento().getTituloEs());
        template.put("fecha", DateUtils.dateToSpanishString(compra.getParSesion().getFechaCelebracion()));
        template.put("url", currentRequest.getRequestURL());
        template.put("pagina", publicPageBuilderInterface
            .buildPublicPageInfo(urlBase, urlBase, language, configurationSelector.getHtmlTitle()));
        template.put("baseUrl", getBaseUrlPublic());
        template.put("lang", language);

        template.put("title", ResourceProperties.getProperty(locale, "mail.entradas.botonCancelar.html"));
        template.put("entrada", true);
        return Response.ok(template).build();
    }

    @POST
    @Path("{uuidCompra}/butaca/{idButaca}/{emailBase64}/anular")
    public Response anularEntrada(
        @PathParam("uuidCompra") String uuidCompra,
        @PathParam("idButaca") long idButaca,
        @PathParam("emailBase64") String emailBase64
    ) throws ParseException {
        Assert.notNull(uuidCompra);
        Assert.notNull(idButaca);
        Assert.notNull(emailBase64);

        String email = new String(Base64.decode(emailBase64));
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());

        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        String language = locale.getLanguage();
        String urlBase = getBaseUrlPublic();
        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/anular",
                locale, APP);
        template.put("pagina", publicPageBuilderInterface
            .buildPublicPageInfo(urlBase, urlBase, language, configurationSelector.getHtmlTitle()));
        template.put("baseUrl", getBaseUrlPublic());
        template.put("lang", language);

        template.put("title", ResourceProperties.getProperty(locale, "mail.entradas.botonCancelar.html"));
        try {
            comprasService.anularButacaDesdeMail(uuidCompra, idButaca, email);
            template.put("resultado", String.format(ResourceProperties.getProperty(locale, "mail.entradas.botonCancelar.ok"), cine.getEmail()));
        }
        catch (TiempoMinioRequeridoParaAnularDesdeEnlace e) {
            log.warn(e.getMessage(), e);
            template.put("resultado", String.format(ResourceProperties.getProperty(locale, "mail.entradas.botonCancelarCompra.ko"), cine.getEmail()));
        }
        catch (Throwable e) {
            log.error(e.getMessage(), e);
            template.put("resultado", String.format(ResourceProperties.getProperty(locale, "mail.entradas.botonCancelar.ko"), cine.getEmail()));
        }
        return Response.ok(template).build();
    }
}
