package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response.Status;

@Transactional
public class EventosResourceTest extends BaseResourceTest {
    final String EVENTO_EXISTENTE = "MADAMA BUTTERFLY";

    @Test
    public void getListado() {
        ClientResponse response =
            resource.path("evento/listado").get(ClientResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(EVENTO_EXISTENTE));
    }

    @Test
    public void getListadoSalaNoExistente() {
        ClientResponse response =
            resource.path("evento/listado/9999").get(ClientResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertFalse(html.contains(EVENTO_EXISTENTE));
    }

    @Test
    public void getListadoSalaExistente() {
        ClientResponse response =
            resource.path("evento/listado/1").get(ClientResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(EVENTO_EXISTENTE));
    }
}
