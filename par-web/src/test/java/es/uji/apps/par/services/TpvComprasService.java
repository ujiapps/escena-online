package es.uji.apps.par.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Locale;

import es.uji.apps.par.builders.PublicPageBuilderInterface;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.config.ConfigurationSelector;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.exceptions.LimiteEntradasGratisSuperadoException;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.services.rest.BaseResource;
import es.uji.apps.par.tpv.TpvInterface;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;

@Service
public class TpvComprasService implements TpvInterface {
    private static final Logger log = LoggerFactory.getLogger(TpvComprasService.class);

    @Autowired
    private ComprasService compras;

    @Autowired
    private MailComposerService mailComposerService;

    @Autowired
    private PublicPageBuilderInterface publicPageBuilderInterface;

    @Autowired
    Configuration configuration;

    @Autowired
    ConfigurationSelector configurationSelector;

    public boolean registraCompra(
        String recibo,
        CompraDTO compra,
        Locale locale
    ) {
        return registraCompraEstado(recibo, null, compra, locale);
    }

    public boolean registraCompraEstado(
        String recibo,
        String estado,
        CompraDTO compra,
        Locale locale
    ) {
        log.info("CADUCADA " + compra.getCaducada());
        long id = compra.getId();
        if (compra.getCaducada()) {
            log.warn("Compra caducada");
            compras.rellenaCodigoPagoPasarela(id, recibo);
            return false;
        } else if (estado == null || isEstadoOk(estado)) {
            if (!compra.getPagada()) {
                log.info("Compra NO caducada: " + id);
                compras.marcaPagadaPasarela(id, recibo);
                log.info("Marcada como pagada " + compra.getUuid() + " - " + compra.getEmail());
                mailComposerService.registraMail(compra, compra.getEmail(), compra.getUuid(), locale);
            } else {
                log.info("Compra ya marcada como pagada: " + id);
            }

            return true;
        } else {
            return false;
        }
    }

    private boolean isEstadoOk(String estado) {
        int intEstado = Integer.valueOf(estado);
        return (intEstado >= 0 && intEstado <= 99) || intEstado == 900;
    }

    public HTMLTemplate paginaError(
        CompraDTO compra,
        String url,
        Locale locale,
        boolean mobile
    ) throws Exception {
        String language = locale.getLanguage();
        CineDTO cine = compra.getParSesion().getParSala().getParCine();

        HTMLTemplate template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/compraIncorrecta", locale,
                BaseResource.APP);

        template.put("pagina", publicPageBuilderInterface
            .buildPublicPageInfo(cine.getUrlPublic(), url, language.toString(), cine.getNombre()));
        template.put("baseUrl", cine.getUrlPublic());

        template.put("urlReintentar", cine.getUrlPublic() + "/rest/entrada/" + compra.getParSesion().getId());
        template.put("lang", language);
        Cine.cineDTOToCine(cine, false).serializeTemplate(template, mobile);

        return template;
    }

    public Template paginaExito(
        CompraDTO compra,
        String url,
        Locale locale,
        boolean mobile
    ) throws Exception {
        String language = locale.getLanguage();
        CineDTO cine = compra.getParSesion().getParSala().getParCine();

        Template template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR + cine.getCodigo() + "/compraValida", locale, BaseResource.APP);

        template.put("pagina",
            publicPageBuilderInterface.buildPublicPageInfo(cine.getUrlPublic(), url, language, cine.getNombre()));
        template.put("baseUrl", cine.getUrlPublic());

        template.put("referencia", compra.getCodigoPagoPasarela());
        template.put("email", compra.getEmail());
        template.put("url", cine.getUrlPublic() + "/rest/compra/" + compra.getUuid() + "/pdf");
        String urlComoLlegar = compra.getParSesion().getParSala().getUrlComoLlegar();
        if (urlComoLlegar == null) {
            urlComoLlegar = cine.getUrlComoLlegar();
        }
        template.put("urlComoLlegar", urlComoLlegar);
        template.put("lang", language);
        template.put("evento", compra.getParSesion().getParEvento());
        template.put("eventoId", compra.getParSesion().getParEvento().getId());
        Cine.cineDTOToCine(cine, false).serializeTemplate(template, mobile);

        return template;
    }

    public Template getTemplateResultadoOk(
        boolean validSignature,
        CompraDTO compra,
        Locale locale,
        String url,
        boolean mobile
    ) throws Exception {
        Template template;
        if (validSignature) {
            if (compra.getCaducada()) {
                template = paginaError(compra, url, locale, mobile);
                template.put("descripcionError",
                    ResourceProperties.getProperty(locale, "error.datosComprador.compraCaducadaTrasPagar"));
            } else {
                template = paginaExito(compra, url, locale, mobile);
            }
        } else {
            template = paginaError(compra, url, locale, mobile);
        }

        return template;
    }

    @Override
    synchronized public Template testTPV(
        long idCompra,
        String url,
        Locale locale,
        boolean mobile
    ) throws Exception {
        log.info("Identificador: " + idCompra);
        CompraDTO compra = compras.getCompraById(idCompra);
        compra.setCodigoPagoPasarela("RECIBO_TEST");

        return compraEstadoOK(compra, url, locale, mobile);
    }

    @Override
    synchronized public Template compraGratuita(
        long idCompra,
        String url,
        Locale locale,
        boolean mobile
    ) throws Exception {
        log.info("Identificador: " + idCompra);
        CompraDTO compra = compras.getCompraById(idCompra);
        compra.setCodigoPagoPasarela(String.valueOf(idCompra));

        CineDTO cine = compra.getParSesion().getParEvento().getParCine();
        if (compra.getParSesion().getParEvento().getEntradasPorEmail() != null || compra.getParButacas().size() <= cine
            .getLimiteEntradasGratuitasPorCompra()) {
            return compraEstadoOK(compra, url, locale, mobile);
        } else {
            throw new LimiteEntradasGratisSuperadoException(idCompra);
        }
    }

    private Template compraEstadoOK(
        CompraDTO compra,
        String url,
        Locale locale,
        boolean mobile
    ) throws Exception {
        Template template;
        if ((compra.getPagada() != null && compra.getPagada()) || registraCompraEstado(compra.getCodigoPagoPasarela(),
            "0", compra, locale)) {
            template = paginaExito(compra, url, locale, mobile);
        } else {
            template = paginaError(compra, url, locale, mobile);
            template.put("descripcionError",
                ResourceProperties.getProperty(locale, "error.datosComprador.compraCaducadaTrasPagar"));
        }
        return template;
    }

    public CompraDTO getCompraOCompraBorrada(String order) {
        CompraDTO compra = compras.getCompraById(Long.parseLong(order));
        if (compra == null) {
            compra = compras.getCompraBorradaById(Long.parseLong(order));
        }
        return compra;
    }
}
