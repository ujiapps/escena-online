package es.uji.apps.par.tpvmodel;

import org.springframework.stereotype.Component;

import es.uji.apps.par.tpv.IdTPVInterface;

@Component
public class IdTpv implements IdTPVInterface {
    public String getFormattedId(long id) {
        return String.valueOf(id);
    }
}
