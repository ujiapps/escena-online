package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;

import es.uji.apps.par.i18n.ResourceProperties;

@Transactional
public class EntradasResourceTest extends BaseResourceTest {
    final String COMPRA_UUID = "uuid_compra_nomal";
    final String COMPRA_UUID_PAGADA = "uuid_compra_nomal_pagada";
    final String COMPRA_ABONO_UUID = "uuid_compra_abono";

    @Test
    public void getDatosComprador() {
        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).get(ClientResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void guardaDatosCompradorSinNombre() {
        MultivaluedMap formData = new MultivaluedMapImpl();

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(
            ResourceProperties.getProperty(new Locale("es"), String.format("error.datosComprador.nombre"))));
    }

    @Test
    public void guardaDatosCompradorSinApellidos() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(
            ResourceProperties.getProperty(new Locale("es"), String.format("error.datosComprador.apellidos"))));
    }

    @Test
    public void guardaDatosCompradorSinEmail() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");
        formData.add("apellidos", "Gragera Camino");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(
            ResourceProperties.getProperty(new Locale("es"), String.format("error.datosComprador.email"))));
    }

    @Test
    public void guardaDatosCompradorSinEmailVerificacion() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");
        formData.add("apellidos", "Gragera Camino");
        formData.add("email", "sergio@4tic.com");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(
            ResourceProperties.getProperty(new Locale("es"), String.format("error.datosComprador.emailVerificacion"))));
    }

    @Test
    public void guardaDatosCompradorConDiferenteEmailVerificacion() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");
        formData.add("apellidos", "Gragera Camino");
        formData.add("email", "sergio@4tic.com");
        formData.add("emailVerificacion", "sergiogragera@4tic.com");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(
            ResourceProperties.getProperty(new Locale("es"), String.format("error.datosComprador.emailVerificacion"))));
    }

    @Test
    public void guardaDatosCompradorSinFormatoEmail() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");
        formData.add("apellidos", "Gragera Camino");
        formData.add("email", "sergio");
        formData.add("emailVerificacion", "sergio");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(
            ResourceProperties.getProperty(new Locale("es"), String.format("error.datosComprador.emailIncorrecto"))));
    }

    @Test
    public void guardaDatosCompradorSinAceptarCondiciones() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");
        formData.add("apellidos", "Gragera Camino");
        formData.add("email", "sergio@4tic.com");
        formData.add("emailVerificacion", "sergio@4tic.com");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(ResourceProperties
            .getProperty(new Locale("es"), String.format("error.datosComprador.condicionesPrivacidad"))));
    }

    @Test
    public void guardaDatosCompradorSinAceptarCondicionesCancelacion() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");
        formData.add("apellidos", "Gragera Camino");
        formData.add("email", "sergio@4tic.com");
        formData.add("emailVerificacion", "sergio@4tic.com");
        formData.add("condicionesPrivacidad", "on");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(ResourceProperties
            .getProperty(new Locale("es"), String.format("error.datosComprador.condicionesCancelacion"))));
    }

    @Test
    public void guardaDatosCompradorCompraAbonoSinEmailAbonado() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");
        formData.add("apellidos", "Gragera Camino");
        formData.add("email", "sergiogragera@gmail.com");
        formData.add("emailVerificacion", "sergiogragera@gmail.com");
        formData.add("condicionesPrivacidad", "on");
        formData.add("condicionesCancelacion", "on");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_ABONO_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains(
            ResourceProperties.getProperty(new Locale("es"), String.format("error.datosComprador.abonado"))));
    }

    @Test
    public void guardaDatosCompradorCompraAbono() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");
        formData.add("apellidos", "Gragera Camino");
        formData.add("email", "sergio@4tic.com");
        formData.add("emailVerificacion", "sergio@4tic.com");
        formData.add("condicionesPrivacidad", "on");
        formData.add("condicionesCancelacion", "on");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_ABONO_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains("<span>RECIBO_TEST</span>"));
    }

    @Test
    public void guardaDatosCompradorCompraPagada() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");
        formData.add("apellidos", "Gragera Camino");
        formData.add("email", "otro@4tic.com");
        formData.add("emailVerificacion", "otro@4tic.com");
        formData.add("condicionesPrivacidad", "on");
        formData.add("condicionesCancelacion", "on");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID_PAGADA))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.SEE_OTHER.getStatusCode(), response.getStatus());
    }

    @Test
    public void guardaDatosCompradorCompra() {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("nombre", "Sergio");
        formData.add("apellidos", "Gragera Camino");
        formData.add("email", "otro@4tic.com");
        formData.add("emailVerificacion", "otro@4tic.com");
        formData.add("condicionesPrivacidad", "on");
        formData.add("condicionesCancelacion", "on");

        ClientResponse response = resource.path(String.format("entrada/%s/datosComprador", COMPRA_UUID))
            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        String html = response.getEntity(String.class);
        Assert.assertTrue(html.contains("<span>RECIBO_TEST</span>"));
    }
}
