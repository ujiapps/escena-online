package es.uji.apps.par.services.rest;

import org.springframework.stereotype.Component;

import java.text.ParseException;

import es.uji.apps.par.builders.PublicPageBuilderInterface;
import es.uji.commons.web.template.model.Pagina;

@Component
public class PublicPageBuilder implements PublicPageBuilderInterface 
{
	public Pagina buildPublicPageInfo(String urlBase, String url, String idioma, String htmlTitle) throws ParseException
	{
		Pagina pagina = new Pagina(urlBase, url, idioma, htmlTitle);
		pagina.setSubTitulo("");
		return pagina;
	}
}
