package es.uji.apps.par.services.rest;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import com.sun.jersey.test.framework.spi.container.TestContainerFactory;
import com.sun.jersey.test.framework.spi.container.grizzly.web.GrizzlyWebTestContainerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import es.uji.apps.par.config.Configuration;

@ContextConfiguration(locations = {"/applicationContext-db-test.xml"})
public class BaseResourceTest extends JerseyTest {
    protected WebResource resource;

    @Autowired
    Configuration configuration;

    public BaseResourceTest() {
        super(new WebAppDescriptor.Builder(
            "com.fasterxml.jackson.jaxrs.json; es.uji.apps.par.services.rest; es.uji.apps.par;es.uji.commons.rest;")
            .contextParam("contextConfigLocation", "classpath:applicationContext-db-test.xml")
            .contextParam("webAppRootKey", "paranimf-fw-uji.root").contextListenerClass(ContextLoaderListener.class)
            .clientConfig(clientConfiguration()).requestListenerClass(RequestContextListener.class)
            .servletClass(SpringServlet.class).build());

        this.client().addFilter(new LoggingFilter());

        Logger rootLogger = LogManager.getLogManager().getLogger("");
        rootLogger.setLevel(Level.SEVERE);
        for (Handler h : rootLogger.getHandlers()) {
            h.setLevel(Level.SEVERE);
        }

        this.resource = resource();
    }

    protected static ClientConfig clientConfiguration() {
        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(JacksonJaxbJsonProvider.class);
        return config;
    }

    @Override
    protected TestContainerFactory getTestContainerFactory() {
        return new GrizzlyWebTestContainerFactory();
    }

    @Override
    protected int getPort(int defaultPort) {
        return 19998;
    }
}
