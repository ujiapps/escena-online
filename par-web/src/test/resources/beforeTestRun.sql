INSERT INTO PAR_USUARIOS (ID, NOMBRE, USUARIO, MAIL, URL)
VALUES (1, 'Sergio', 'sergio', 'sergio@4tic.com', 'localhost');

INSERT INTO PAR_CINES (id, codigo, nombre, cif, direccion, cod_municipio, nom_municipio, cp, empresa, cod_registro,
                       tfno, iva, url_public, url_privacidad, url_cancelacion, url_como_llegar, mail_from,
                       url_pie_entrada, logo_report, api_key, butacasentradoendistintocolor, langs, default_lang,
                       show_iva, passbook_activado, limite_entradas_gratis, comision, email, registro_mercantil, logo,
                       logo_content_type, logo_src, logo_uuid, banner, banner_content_type, banner_src, banner_uuid,
                       clase_postventa, informes, informes_generales, menu_tpv, menu_clientes, menu_integraciones,
                       menu_salas, menu_perfil, menu_ayuda, menu_taquilla, check_contratacion, formas_pago,
                       menu_venta_anticipada, menu_abonos, show_como_nos_conociste, show_logo_entrada, no_numeradas_secuencial,
                       is_anulable_desde_enlace, tiempo_anulable_desde_enlace, codigo_icaa, menu_icaa)
VALUES (1, 'huesca', 'Palacio de Congresos', 'A-22338495', 'Avda. de los Danzantes, s/n', '1', 'Huesca (España)',
        '22005', 'Ayuntamiento Huesca', '1', '974292191', 0, 'https://dev.palaciodecongresoshuesca.4tic.com/par-public',
        'https://dev.palaciocongresoshuesca.es/aviso-legal', 'https://dev.palaciocongresoshuesca.es/cancelacion',
        'http://www.palaciocongresoshuesca.es/auditorio', 'no_reply@4tic.com', 'http://example.com/example.jpg',
        'pchu_logo.jpg', '123456789', false, null, null, true, true, 10, 0, null, null, null, 'image/png',
        'palacio_logo.png', 'B1RY1RF8U5V9F4QQUE3MSBQZ545N62L0', null, 'image/jpeg', 'palacio_banner.jpg',
        '1OQYMK82BLDR737W3XFQGXG268MZBBWT', null, null, null, true, true, true, true, true, true, true, false,
        'metalico,tarjetaOffline,transferencia,credito', true, true, false, true, false, true, 60, '123', false);

INSERT INTO PAR_SALAS (ID, CODIGO, NOMBRE, TIPO, FORMATO, SUBTITULO, CINE_ID,
                       HTML_TEMPLATE_NAME, ASIENTOS_NUMERADOS, IS_AFORO_POR_COMPRAS)
VALUES (1, '', '', '', '', '', 1, '', 1, false);

INSERT INTO par_localizaciones (id, codigo, nombre_es, total_entradas, nombre_va, sala_id, iniciales)
VALUES (1, 'anfiteatro_huesca', 'Anfiteatro', 205, 'Amfiteatre', 1, 'HAA');

INSERT INTO PAR_SALAS_USUARIOS (id, USUARIO_ID, SALA_ID)
VALUES (1, 1, 1);
INSERT INTO PAR_TPVS (id, nombre, code, currency, terminal, transaction_code, order_prefix, lang_ca_code, lang_es_code,
                      url, wsdl_url, secret, cif, signature_method, visible)
VALUES (1, 'Palacio de Congresos', '43242', '978', '00000001', '2', '0000554027', '2', '1',
        'https://pgw.ceca.es/cgi-bin/tpv', '-', '321312', 'A-3213', 'CECA_SHA1', true);
INSERT INTO PAR_TPVS_SALAS (id, tpv_id, sala_id)
VALUES (1, 1, 1);
INSERT INTO PAR_TIPOS_EVENTO (id, nombre_es, nombre_va, exportar_icaa, cine_id)
VALUES (1, 'Teatro', 'Teatre', FALSE, 1);

INSERT INTO par_plantillas (id, nombre, sala_id)
VALUES (-1, 'Sin plantilla', 1);

INSERT INTO par_tarifas (id, nombre, ispublica, defecto, cine_id, isabono)
VALUES (1, 'general', true, true, 1, true);

INSERT INTO par_eventos (id, titulo_es, descripcion_es, companyia_es, interpretes_es, duracion_es, premios_es,
                         caracteristicas_es, comentarios_es, tipo_evento_id, imagen_content_type, imagen_src,
                         titulo_va, descripcion_va, companyia_va, interpretes_va, duracion_va, premios_va,
                         caracteristicas_va, comentarios_va, porcentaje_iva, iva_sgae, retencion_sgae, rss_id,
                         expediente, cod_distri, nom_distri, nacionalidad, vo, metraje, asientos_numerados,
                         imagen, subtitulos, formato, tpv_id, cine_id, promotor, nif_promotor, imagen_publi,
                         imagen_publi_content_type, imagen_publi_src, imagen_uuid, imagen_publi_uuid,
                         email_promotor, tlf_promotor, direccion_promotor, publico, duracion)
VALUES (1, 'MADAMA BUTTERFLY  ', 'Uno de los principales objetivos de la ORA es acercar la ópera de verdad al público aragonés, y después de representar “La Traviata”, “La Bohème” y “Carmen”, con tanto éxito de crítica y público, vuelven a traer uno de los grandes monumentos operísticos.

La producción de la ORA acercará a Puccini con su contrastada pasión y calor. La ópera, centrada en la protagonista geisha, dibuja la heroína más desgarradora, consiguiendo ser uno de los personajes más humanos, sensibles y más fascinantes. Además, la obra es una condena a la engreída y prepotente civilización occidental, de la que señala su cinismo e hipocresía en enorme contraste con la cultura oriental.',
        '', '', '', '', '', '', 1, 'image/jpeg', 'Madama Buttefly Huesca.jpg', '', '', '', '', '', '', '', '', 10,
        null, null, '', '', '', '', '', '', '', true, null, '', '', 1, 1, 'GESMAR SERVICIOS CULTURALES, S. L.',
        'B-99469298', null, 'image/jpeg', 'Pub hazte Amigo del Palacio.jpg', 'GZD5FNECPSKY97OAPZ9W6HVDOJ0F9ZXB',
        'ZZ27T6K1UFTBAUNT9S6E7TK0G4NMDLUZ', '', '', '', true, 0);

INSERT INTO par_sesiones (id, evento_id, fecha_celebracion, fecha_inicio_venta_online, fecha_fin_venta_online,
                          hora_apertura, canal_internet, canal_taquilla, plantilla_id, nombre, sala_id, ver_ling,
                          rss_id, incidencia_id, anulada, fecha_fin_venta_anticipada, multisesion)
VALUES (1, 1, '2059-06-16 19:00:00.000000', '2018-11-08 17:00:00.000000', '2059-06-16 17:30:00.000000',
        '18:30', true, false, -1, '', 1, '', '', 9, false, null, false);

INSERT INTO par_compras (id, sesion_id, nombre, apellidos, direccion, poblacion, cp, provincia, tfno, email,
                         info_periodica, fecha, taquilla, importe, codigo_pago_tarjeta, pagada, uuid,
                         codigo_pago_pasarela, reserva, desde, hasta, observaciones_reserva, anulada,
                         recibo_pinpad, caducada, referencia_pago, tipo, porcentaje_iva,
                         par_usuario, comision, importe_sin_comision)
VALUES (1, 1, 'José', 'Pujol cdasc', 'Av. Mtnez. Velasco', 'Huesca', '22004', 'Huesca',
        '090909090', 'aaa@cocsdacsdac.net', true, '2018-11-10 11:43:45.281000', false, 102, null, false,
        'uuid_compra_abono', '8468c4864cs846', false, null, null, null, false, null,
        false, null, 'TARJETA', 10, null, 0, 102);

INSERT INTO par_butacas (id, sesion_id, localizacion_id, compra_id, fila, numero, tipo, precio, anulada,
                         presentada, id_entrada, precio_sin_comision, fecha_anulada, par_usuario_anula)
VALUES (1, 1, 1, 1, '23', '5', '2', 10, false, null, null, 10, null, null);

INSERT INTO par_compras (id, sesion_id, nombre, apellidos, direccion, poblacion, cp, provincia, tfno, email,
                         info_periodica, fecha, taquilla, importe, codigo_pago_tarjeta, pagada, uuid,
                         codigo_pago_pasarela, reserva, desde, hasta, observaciones_reserva, anulada,
                         recibo_pinpad, caducada, referencia_pago, tipo, porcentaje_iva,
                         par_usuario, comision, importe_sin_comision)
VALUES (2, 1, 'Otro', 'Apellido cdasc', 'Av. Mtnez. Calle', 'Huesca', '22004', 'Huesca',
        '090909090', 'aaaa@casdcdas.net', true, '2018-11-10 11:43:45.281000', false, 102, null, false,
        'uuid_compra_nomal', '8468c4864cs846', false, null, null, null, false, null,
        false, null, 'TARJETA', 10, null, 0, 102);

INSERT INTO par_tarifas (id, nombre, ispublica, defecto, cine_id, isabono)
VALUES (2, 'abono', true, false, 1, true);

INSERT INTO par_eventos (id, titulo_es, descripcion_es, companyia_es, interpretes_es, duracion_es, premios_es,
                         caracteristicas_es, comentarios_es, tipo_evento_id, imagen_content_type, imagen_src,
                         titulo_va, descripcion_va, companyia_va, interpretes_va, duracion_va, premios_va,
                         caracteristicas_va, comentarios_va, porcentaje_iva, iva_sgae, retencion_sgae, rss_id,
                         expediente, cod_distri, nom_distri, nacionalidad, vo, metraje, asientos_numerados,
                         imagen, subtitulos, formato, tpv_id, cine_id, promotor, nif_promotor, imagen_publi,
                         imagen_publi_content_type, imagen_publi_src, imagen_uuid, imagen_publi_uuid,
                         email_promotor, tlf_promotor, direccion_promotor, publico, isabono, duracion)
VALUES (2, 'abono', '', '', '', '', '', '', '', 1, null, null, 'abono', '', '', '', '', '', '', '', 10,
        null, null, '', '', '', '', '', '', '', true, null, '', '', 1, 1, '',
        null, null, null, null, null,
        null, '', '', '', true, true, 0);

INSERT INTO par_sesiones (id, evento_id, fecha_celebracion, fecha_inicio_venta_online, fecha_fin_venta_online,
                          hora_apertura, canal_internet, canal_taquilla, plantilla_id, nombre, sala_id, ver_ling,
                          rss_id, incidencia_id, anulada, fecha_fin_venta_anticipada, multisesion)
VALUES (2, 2, '2059-12-31 00:00:00.000000', '2018-01-01 00:00:00.000000', '2059-12-31 00:00:00.000000',
        '00:00', true, false, -1, '', 1, '', '', 9, false, null, false);

INSERT INTO par_compras (id, sesion_id, nombre, apellidos, direccion, poblacion, cp, provincia, tfno, email,
                         info_periodica, fecha, taquilla, importe, codigo_pago_tarjeta, pagada, uuid,
                         codigo_pago_pasarela, reserva, desde, hasta, observaciones_reserva, anulada,
                         recibo_pinpad, caducada, referencia_pago, tipo, porcentaje_iva,
                         par_usuario, comision, importe_sin_comision)
VALUES (3, 2, 'sergio', 'gragera', 'franca', 'Vilareal', '12540', 'Castellón',
        '2312312', 'sergio@4tic.com', false, '2019-03-11 17:42:34.302000', false, 102, null, true,
        'uuid_compra_normal2', '8468c4864cs847', false, null, null, null, false, null,
        false, null, 'TARJETA', 10, null, 0, 102);

INSERT INTO par_butacas (id, sesion_id, localizacion_id, compra_id, fila, numero, tipo, precio, anulada,
                         presentada, id_entrada, precio_sin_comision, fecha_anulada, par_usuario_anula)
VALUES (2, 2, 1, 3, null, null, '2', 10, false, null, null, 10, null, null);

INSERT INTO par_como_nos_conociste (id, cine_id, motivo)
VALUES (1, 1, 'Buscando por internet');

INSERT INTO par_compras (id, sesion_id, nombre, apellidos, direccion, poblacion, cp, provincia, tfno, email,
                         info_periodica, fecha, taquilla, importe, codigo_pago_tarjeta, pagada, uuid,
                         codigo_pago_pasarela, reserva, desde, hasta, observaciones_reserva, anulada,
                         recibo_pinpad, caducada, referencia_pago, tipo, porcentaje_iva,
                         par_usuario, comision, importe_sin_comision)
VALUES (4, 1, 'Otro', 'Apellido cdasc', 'Av. Mtnez. Calle', 'Huesca', '22004', 'Huesca',
        '090909090', 'aaaa@casdcdas.net', true, '2018-11-10 12:12:45.281000', false, 102, null, true,
        'uuid_compra_nomal_pagada', 'cdsc324234cds', false, null, null, null, false, null,
        false, null, 'TARJETA', 10, null, 0, 102);