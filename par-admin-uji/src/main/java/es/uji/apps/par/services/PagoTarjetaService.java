package es.uji.apps.par.services;

import com.google.common.base.Strings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.uji.apps.par.dao.AbonadosDAO;
import es.uji.apps.par.dao.ComprasDAO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.pinpad.EstadoPinpad;
import es.uji.apps.par.pinpad.ResultadoPagoPinpad;

@Service
public class PagoTarjetaService {
    private static final Logger log = LoggerFactory.getLogger(PagoTarjetaService.class);
    private static final String PAGO_OK = "0";
    private Map<Long, EstadoPinpad> pagosPendientes = new HashMap<Long, EstadoPinpad>();

    @Autowired
    private Pinpad pinpad;

    @Autowired
    private ComprasDAO compras;

    @Autowired
    private AbonadosDAO abonados;

    @Autowired
    private MailComposerService mailComposerService;

    public ResultadoPagoPinpad realizaPago(
        long idCompra,
        String concepto
    ) {
        CompraDTO compra = compras.getCompraById(idCompra);
        ResultadoPagoPinpad resultado = pinpad.realizaPago(Long.toString(idCompra), compra.getImporte(), concepto);

        if (resultado.getError())
            compras.borrarCompraNoPagada(idCompra);
        else
            log.info("pago realizado para la compra idCompra:" + idCompra + ", empezamos la comprobacion de estado");

        return resultado;
    }

    private boolean pagoCorrecto(EstadoPinpad estado) {
        return estado.getCodigoAccion().equals(PAGO_OK);
    }

    private boolean tieneCodigoAccion(EstadoPinpad estado) {
        return estado != null && estado.getCodigoAccion() != null && !estado.getCodigoAccion().equals("");
    }

    public EstadoPinpad consultaEstadoPago(Long idCompra) {
        EstadoPinpad estado = pinpad.getEstadoPinpad(Long.toString(idCompra));
        if (tieneCodigoAccion(estado)) {
            if (pagoCorrecto(estado)) {
                actualizaCompraComoPagada(idCompra, estado);
            } else if (estado.getError()) {
                compras.borrarCompraNoPagada(idCompra);
            }
        }
        return estado;
    }

    public void actualizarEstadoPagoPendientesTaquilla() {
        List<CompraDTO> comprasPendientesDePago = compras.getComprasPendientesDeConfirmarPago(0);
        for (CompraDTO compraDTO : comprasPendientesDePago) {
            long idCompra = compraDTO.getId();
            EstadoPinpad estado = pinpad.getEstadoPinpad(Long.toString(idCompra));
            if (tieneCodigoAccion(estado) && pagoCorrecto(estado)) {
                actualizaCompraComoPagadaOnline(idCompra, estado);
                if (!Strings.isNullOrEmpty(compraDTO.getEmail())) {
                    mailComposerService
                        .registraMail(compraDTO, compraDTO.getEmail(), compraDTO.getUuid(), new Locale("ca", "ES"));
                }
            }
        }
    }

    private void actualizaCompraComoPagada(
        Long idCompra,
        EstadoPinpad estado
    ) {
        log.info("guardarRecibo: idCompra:" + idCompra);
        compras.guardarCodigoPagoTarjeta(idCompra, estado.getRecibo());
        log.info("marcarPagada: idCompra:" + idCompra);
        compras.marcarPagadaConRecibo(idCompra, estado.getRecibo());
    }

    private void actualizaCompraComoPagadaOnline(
        Long idCompra,
        EstadoPinpad estado
    ) {
        log.info("marcarPagadaPasarela: idCompra:" + idCompra);
        compras.marcarPagadaPasarela(idCompra, estado.getRecibo());
    }


    public void borraCompraPendiente(Long idCompra) {
        compras.borrarCompraNoPagada(idCompra);
    }
}
