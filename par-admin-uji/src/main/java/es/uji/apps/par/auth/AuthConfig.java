package es.uji.apps.par.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AuthConfig
{
    @Value("${saml.metadata.username:uid}")
    public String userNameAttribute;
}