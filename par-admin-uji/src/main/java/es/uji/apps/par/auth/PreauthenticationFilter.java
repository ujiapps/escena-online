package es.uji.apps.par.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;

import es.uji.apps.par.services.UsersService;

public class PreauthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {
    private static Logger log = LoggerFactory.getLogger(PreauthenticationFilter.class);

    private UsersService usersService;

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest httpServletRequest) {
        if (hasCorrectTokens(httpServletRequest)) {
            //return "";
        }
        return null;
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest httpServletRequest) {
        if (hasCorrectTokens(httpServletRequest)) {
            try {
                usersService.getUserById(httpServletRequest.getHeader("X-UJI-AuthUser"));
            } catch (UsernameNotFoundException e) {
                log.error("Error al obtener la persona con id: " + httpServletRequest.getHeader("X-UJI-AuthUser"), e);
            }
        }
        return null;
    }

    private boolean hasCorrectTokens(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getHeader("X-UJI-AuthToken") != null
            && httpServletRequest.getHeader("X-UJI-AuthUser") != null;
    }

    public UsersService getUserService() {
        return usersService;
    }

    public class PreAuthenticationFilter {
    }


    public void setUsersService(UsersService userService) {
        this.usersService = userService;
    }
}
