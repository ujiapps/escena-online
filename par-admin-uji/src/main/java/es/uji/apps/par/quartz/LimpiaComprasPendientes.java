package es.uji.apps.par.quartz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.par.exceptions.IncidenciaNotFoundException;
import es.uji.apps.par.services.ComprasService;
import es.uji.apps.par.services.PagoTarjetaService;

@Service
public class LimpiaComprasPendientes
{
    @Autowired
    PagoTarjetaService pagoTarjeta;

    @Autowired
    ComprasService comprasService;

    public void ejecuta() throws IncidenciaNotFoundException {
        pagoTarjeta.actualizarEstadoPagoPendientesTaquilla();
        comprasService.eliminaPendientes();
    }
}
