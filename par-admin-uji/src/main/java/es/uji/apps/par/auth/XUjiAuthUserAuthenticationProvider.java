package es.uji.apps.par.auth;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import es.uji.apps.par.services.UsersService;

public class XUjiAuthUserAuthenticationProvider implements AuthenticationProvider
{
    private UsersService usersService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException
    {
        return authentication;
    }

    @Override
    public boolean supports(Class<?> authentication)
    {
        return authentication.equals(PreAuthenticatedAuthenticationToken.class);
    }

    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }
}
