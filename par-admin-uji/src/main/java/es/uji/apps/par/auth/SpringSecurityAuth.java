package es.uji.apps.par.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.UsersService;

import static es.uji.apps.par.auth.Authenticator.ERROR_LOGIN;
import static es.uji.apps.par.auth.Authenticator.READONLY_ATTRIBUTE;
import static es.uji.apps.par.auth.Authenticator.TAQUILLA_ATTRIBUTE;
import static es.uji.apps.par.auth.Authenticator.USER_ATTRIBUTE;

public class SpringSecurityAuth implements Filter {
    @Autowired
    UsersService usersService;

    private FilterConfig filterConfig = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(
        ServletRequest request,
        ServletResponse response,
        FilterChain filterChain
    ) throws IOException, ServletException {
        HttpServletRequest clientRequest = (HttpServletRequest) request;
        HttpServletResponse clientResponse = (HttpServletResponse) response;

        String url = clientRequest.getRequestURI();
        String headerAuthToken = clientRequest.getHeader("X-UJI-AuthToken");
        if (isValidForDoFilter(clientRequest, url, headerAuthToken)) {
            filterChain.doFilter(request, response);
            return;
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            clientResponse.sendRedirect("forbidden.jsp");
            return;
        }

        SAMLCredential credential = (SAMLCredential) authentication.getCredentials();
        AuthConfig authConfig = getAuthConfig();
        String userName = credential.getAttributeAsString(authConfig.userNameAttribute);
        registerUserInHttpSession(clientRequest, usersService.getUserById(userName));
        filterChain.doFilter(request, response);
    }

    private boolean isValidForDoFilter(
        HttpServletRequest clientRequest,
        String url,
        String headerAuthToken
    ) {
        return isExcludedUrl(url) || isCorrectExternalAPICall(headerAuthToken) || sessionAlreadyRegistered(
            clientRequest);
    }

    private boolean sessionAlreadyRegistered(HttpServletRequest clientRequest) {
        return clientRequest.getSession().getAttribute(Authenticator.USER_ATTRIBUTE) != null;
    }

    private void registerUserInHttpSession(
        HttpServletRequest clientRequest,
        Usuario usuario
    ) {
        HttpSession serverSession = clientRequest.getSession();
        if (usuario.getRole() != null) {
            serverSession.setAttribute(USER_ATTRIBUTE, usuario.getUsuario());
            serverSession.setAttribute(ERROR_LOGIN, false);
            if (usuario.getRole().equals(Role.READONLY)) {
                serverSession.setAttribute(READONLY_ATTRIBUTE, true);
            } else if (usuario.getRole().equals(Role.TAQUILLA)) {
                serverSession.setAttribute(TAQUILLA_ATTRIBUTE, true);
            }
        }
    }

    private AuthConfig getAuthConfig() {
        WebApplicationContext context =
            WebApplicationContextUtils.getWebApplicationContext(filterConfig.getServletContext());
        return context.getBean(AuthConfig.class);
    }

    private boolean isCorrectExternalAPICall(
        String headerAuthToken
    ) {
        if (headerAuthToken != null) {
            String token = filterConfig.getInitParameter("authToken");
            return headerAuthToken.equals(token);
        }

        return false;
    }

    @Override
    public void destroy() {
    }

    private boolean isExcludedUrl(String url) {
        String excludedUrls = filterConfig.getInitParameter("excludedUrls");
        Pattern pattern = Pattern.compile(excludedUrls);
        return pattern.matcher(url).matches();
    }
}
