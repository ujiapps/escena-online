UPDATE UJI_PARANIMF.PAR_CINES
SET LANGS                  = '[{"locale":"ca", "alias":"Valencià"}, {"locale":"es", "alias":"Castellano"}]',
    DEFAULT_LANG           = 'ca',
    SHOW_IVA               = 1,
    PASSBOOK_ACTIVADO      = 0,
    LIMITE_ENTRADAS_GRATIS = 10,
    COMISION               = 0,
    MENU_TPV               = 0,
    MENU_CLIENTES          = 1,
    MENU_INTEGRACIONES     = 1,
    MENU_SALAS             = 0,
    MENU_PERFIL            = 0,
    MENU_AYUDA             = 1,
    MENU_TAQUILLA          = 1,
    CHECK_CONTRATACION     = 0,
    FORMAS_PAGO            = 'metalico,tarjetaOffline,transferencia,credito'
WHERE ID = 1;

INSERT INTO UJI_PARANIMF.PAR_REPORTS ("id", CINE_ID, CLASE, TIPO)
VALUES (1, 1, 'es.uji.apps.par.report.InformeSesionNoAnuladasReport', 'pdfSesionNoAnuladas');
INSERT INTO UJI_PARANIMF.PAR_REPORTS ("id", CINE_ID, CLASE, TIPO)
VALUES (2, 1, 'es.uji.apps.par.report.InformeEventosReport', 'pdfSGAE');
INSERT INTO UJI_PARANIMF.PAR_REPORTS ("id", CINE_ID, CLASE, TIPO)
VALUES (3, 1, 'es.uji.apps.par.report.InformeTaquillaReport', 'pdfTaquilla');
INSERT INTO UJI_PARANIMF.PAR_REPORTS ("id", CINE_ID, CLASE, TIPO)
VALUES (4, 1, 'es.uji.apps.par.report.InformeTaquillaTpvSubtotalesReport', 'pdfTpv');
INSERT INTO UJI_PARANIMF.PAR_REPORTS ("id", CINE_ID, CLASE, TIPO)
VALUES (5, 1, 'es.uji.apps.par.report.InformeMiTaquillaReport', 'pdfMiTaquilla');
INSERT INTO UJI_PARANIMF.PAR_REPORTS ("id", CINE_ID, CLASE, TIPO)
VALUES (6, 1, 'es.uji.apps.par.report.InformeEfectivoReport', 'pdfEfectiu');
INSERT INTO UJI_PARANIMF.PAR_REPORTS ("id", CINE_ID, CLASE, TIPO)
VALUES (7, 1, 'es.uji.apps.par.report.InformeIncidenciasReport', 'pdfIncidencias');
INSERT INTO UJI_PARANIMF.PAR_REPORTS ("id", CINE_ID, CLASE, TIPO)
VALUES (8, 1, 'es.uji.apps.par.report.InformeSesionReport', 'pdfSesion');

UPDATE UJI_PARANIMF.PAR_SALAS
SET CLASE_ENTRADA_TAQUILLA = 'es.uji.apps.par.report.EntradaTaquillaReport',
    CLASE_ENTRADA_ONLINE   = 'es.uji.apps.par.report.EntradaReport'
WHERE ID = 1;

DELETE
FROM UJI_PARANIMF.PAR_SALAS
WHERE ID = 2;

UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 11;
UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 1;
UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 2;
UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 3;
UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 4;
UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 5;
UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 6;
UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 7;
UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 8;
UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 9;
UPDATE UJI_PARANIMF.PAR_USUARIOS
SET ROLE = 'ADMIN'
WHERE ID = 10;

