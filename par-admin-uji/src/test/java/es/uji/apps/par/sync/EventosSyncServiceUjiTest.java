package es.uji.apps.par.sync;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.PlantillaBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.TarifaBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.builders.TpvBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.dao.EventosDAO;
import es.uji.apps.par.dao.SesionesDAO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.services.EventosSyncService;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager")
@ContextConfiguration(locations = {"/applicationContext-db-test.xml", "/applicationContext-uji-test.xml",})
public class EventosSyncServiceUjiTest extends SyncBaseTest {
    private static final String SORT = "[{\"property\":\"tituloVa\", \"direction\":\"ASC\"}]";

    private static final String RSS_TIPO_GRADUACION = "uji/rss-graduacion.xml";
    private static final String RSS_TIPO_GRADUACION_UPDATE = "uji/rss-graduacion-update.xml";

    @Autowired
    EventosSyncService syncService;

    @Autowired
    EventosDAO eventosDAO;

    @Autowired
    SesionesDAO sesionesDAO;

    @PersistenceContext
    protected EntityManager entityManager;

    CineDTO cine;
    UsuarioDTO usuario;

    @Before
    public void setup() {
        syncService.setTipo("uji");

        cine = new CineBuilder("Cine 1")
            .build(entityManager);

        new TipoEventoBuilder("tipo", "tipo", false, cine)
            .build(entityManager);

        SalaDTO sala = new SalaBuilder("Sala 1", cine)
            .build(entityManager);

        new TpvBuilder("ejemplo 2").withSala(sala).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala)
            .build(entityManager);

        new TarifaBuilder("GENERAL").withPublic().build(cine, entityManager);

        usuario = new UsuarioBuilder("User 1", "user1@test.com", "user1")
            .withSala(sala)
            .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    @Transactional
    public void testSyncNuevosItemsGraduacion() throws Exception {
        syncService.sync(loadFromClasspath(RSS_TIPO_GRADUACION), usuario.getUsuario());

        List<Evento> eventos = eventosDAO.getEventosActivos(SORT, 0, Integer.MAX_VALUE, null, usuario.getUsuario(),
            cine.getId(), null);

        assertEquals("Número de eventos nuevos", 2, eventos.size());
    }

    @Test
    @Transactional
    public void testSyncUpdateItemsGraduacion() throws Exception {
        syncService.sync(loadFromClasspath(RSS_TIPO_GRADUACION), usuario.getUsuario());
        List<Evento> eventos = eventosDAO.getEventosActivos(SORT, 0, Integer.MAX_VALUE, null, usuario.getUsuario(), cine.getId(), null);
        assertEquals("Número de eventos nuevos", 2, eventos.size());

        syncService.sync(loadFromClasspath(RSS_TIPO_GRADUACION_UPDATE), usuario.getUsuario());
        List<SesionDTO> sesiones = sesionesDAO.getSesionesPorRssId("2508948", usuario.getUsuario());

        assertEquals("Número de sesiones", 1, sesiones.size());
        assertEquals("Fecha primera sesión", sesiones.get(0).getFechaCelebracion().toString(), "2050-10-16 19:00:00.0");
        assertEquals("Hora apertura", sesiones.get(0).getHoraApertura(), "18:00");
    }
}
