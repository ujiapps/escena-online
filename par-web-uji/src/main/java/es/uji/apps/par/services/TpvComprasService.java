package es.uji.apps.par.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Locale;

import es.uji.apps.par.builders.PublicPageBuilderInterface;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.config.ConfigurationSelector;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.services.rest.BaseResource;
import es.uji.apps.par.tpv.TpvInterface;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;

@Service
public class TpvComprasService implements TpvInterface {
    @Autowired
    private ComprasService compras;

    @Autowired
    private MailComposerService mailComposerService;

    @Autowired
    private PublicPageBuilderInterface publicPageBuilderInterface;

    @Autowired
    Configuration configuration;

    @Autowired
    ConfigurationSelector configurationSelector;

    public Template compraOk(
        CompraDTO compraDTO,
        String recibo,
        String url,
        Locale locale
    ) throws Exception {
        Template template;
        if (!compraDTO.getPagada()) {
            compras.marcaPagadaPasarela(compraDTO.getId(), recibo);
            mailComposerService.registraMail(compraDTO, compraDTO.getEmail(), compraDTO.getUuid(), locale);
        }
        template = paginaExito(compraDTO, recibo, url, locale);
        return template;
    }

    public Template compraCaducada(
        CompraDTO compraDTO,
        String recibo,
        String url,
        Locale locale
    ) throws Exception {
        Template template;
        compras.rellenaCodigoPagoPasarela(compraDTO.getId(), recibo);
        template = paginaError(compraDTO, url, locale);
        template.put("descripcionError",
            ResourceProperties.getProperty(locale, "error.datosComprador.compraCaducadaTrasPagar"));
        return template;
    }

    private Template checkCompra(
        CompraDTO compraDTO,
        String recibo,
        String estado,
        String url,
        Locale locale
    ) throws Exception {
        Template template;

        if (compraDTO.getCaducada()) {
            template = compraCaducada(compraDTO, recibo, url, locale);
        } else if (estado != null && estado.equals("OK")) {
            template = compraOk(compraDTO, recibo, url, locale);
        } else {
            template = paginaError(compraDTO, url, locale);
        }
        return template;
    }

    private Template paginaExito(
        CompraDTO compra,
        String recibo,
        String url,
        Locale locale
    ) throws Exception {
        String language = locale.getLanguage();
        CineDTO cine = compra.getParSesion().getParSala().getParCine();
        
        Template template = new HTMLTemplate(Constantes.PLANTILLAS_DIR  + cine.getCodigo() + "/compraValida", locale, BaseResource.APP);

        template.put("pagina", publicPageBuilderInterface
            .buildPublicPageInfo(configurationSelector.getUrlPublic(), url, language.toString(),
                configurationSelector.getHtmlTitle()));
        template.put("baseUrl", configurationSelector.getUrlPublic());

        template.put("referencia", recibo);
        template.put("email", compra.getEmail());
        template.put("url", configurationSelector.getUrlPublic() + "/rest/compra/" + compra.getUuid() + "/pdf");
        template.put("urlComoLlegar", configurationSelector.getUrlComoLlegar());
        template.put("lang", language);
        template.put("evento", compra.getParSesion().getParEvento());

        return template;
    }

    public HTMLTemplate paginaError(
        CompraDTO compra,
        String url,
        Locale locale
    ) throws Exception {
        String language = locale.getLanguage();
        CineDTO cine = compra.getParSesion().getParSala().getParCine();

        HTMLTemplate template =
            new HTMLTemplate(Constantes.PLANTILLAS_DIR  + cine.getCodigo() + "/compraIncorrecta", locale, BaseResource.APP);

        template.put("pagina", publicPageBuilderInterface
            .buildPublicPageInfo(configurationSelector.getUrlPublic(), url, language.toString(),
                configurationSelector.getHtmlTitle()));
        template.put("baseUrl", configurationSelector.getUrlPublic());

        template.put("urlReintentar",
            configurationSelector.getUrlPublic() + "/rest/entrada/" + compra.getParSesion().getId());
        template.put("lang", language);

        return template;
    }

    @Override
    synchronized public Template testTPV(
        long identificadorCompra,
        String url,
        Locale locale,
        boolean mobile
    ) throws Exception {
        CompraDTO compra = compras.getCompraById(identificadorCompra);
        return checkCompra(compra, "RECIBO_TEST", "OK", url, locale);
    }

    @Override
    synchronized public Template compraGratuita(
        long identificadorCompra,
        String url,
        Locale locale,
        boolean mobile
    ) throws Exception {
        CompraDTO compra = compras.getCompraById(identificadorCompra);
        return checkCompra(compra, "COMPRA_GRATUITA_" + identificadorCompra, "OK", url, locale);
    }
}
