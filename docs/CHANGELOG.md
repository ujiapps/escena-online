# Changelog

Todos los cambios significativos en este proyecto estarán documentados en este archivo.

El formato se basa en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
y este proyecto se versiona respetando la especificación de [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.40.3] - 2021-12-20
### Fixed
- La fecha de primera sesión del evento no tiene en cuenta sesiones anuladas

## [2.40.2] - 2021-11-29
### Fixed
- No permite la introducción de imágenes con transparencia evitando errores de impresión PDF

## [2.40.0] - 2021-09-21
### Added
- Añade la multisesión para eventos numerados.

## [2.39.0] - 2021-09-14
### Added
- La app recibe información sobre entradas vendidas, disponibles y reservadas de la sesión.
### Fixed
- Los códigos de sala y localización se normalizan para que solo contengan caracateres alfanumércios.

## [2.38.3] - 2021-08-25
### Fixed
- Corrige errores menores como no poder seleccionar salas sin localizaciones o datos nulos que aparecían en los informes.

## [2.38.2] - 2021-07-30
### Fixed
- Corrige error por el que no se mostraban informes de sesión

## [2.38.1] - 2021-07-28
### Fixed
- Permite la inclusión de textos personalizados al mostrar las diferentes sesiones de un evento.

## [2.38.0] - 2021-07-19
### Added
- Adaptaciones realizadas para la homologación ICAA.

## [2.37.1] - 2021-07-07
### Fixed
- Se corrige el _timeout_ de cliente al marcar la compra de taquilla como pagada para que no la deje pendiente de pago.

## [2.37.0] - 2021-06-24
### Added
- Se permite la introducción de códigos alfanuméricos para definir la URL del evento que esta
sea una URL semántica para mejorar la indexación en los buscadores.

## [2.36.0] - 2021-06-21
### Added
- El apartado de integraciones muestra un código QR con el que se permite configurar la aplicación móvil
  de manera rápida y sencilla escaneando el código.
### Fixed
- Corrige el mensaje de error cuando se supera el límite de compras en eventos no numerados.

## [2.35.4] - 2021-06-16
### Fixed
- Corrige control de aforo en la última compra.

## [2.35.3] - 2021-06-09
### Fixed
- Se muestran los datos del promotor del evento en la entrada _Print-at-home_.

## [2.35.2] - 2021-05-31
### Fixed
- Informes adaptados a la venta de sesiones de varios días.

## [2.35.1] - 2021-05-26
### Added
- Nueva plantilla para entradas online y taquilla en A4.

## [2.35.0] - 2021-05-24
### Added
- Permite indicar aforo de la sala por compras, sin contar el número de entradas de cada compra.
### Fixed
- El _encoding_ de los _emails_ se mostraba mal en algunos clientes de correo ya que falta indicar el _Charset **UTF-8**_ 
  utilizado en la codificación de caracteres.

## [2.34.1] - 2021-05-17
### Fixed
- Muestra de nuevo el número de butacas vendidas y reservadas en el menú Taquilla.

## [2.34.0] - 2021-05-10
### Added 
- Permite la compra única de múltiples sesiones de un evento usado en festivales de duración indeterminada
  donde las entradas pueden ser para uno, dos o varios días.

## [2.33.2] - 2021-05-03
### Fixed
- El email enviado desde la compra de taquilla tiene el formato enriquezido con botones de anulación y `passbook` si están habilitados.

## [2.33.1] - 2021-04-29
### Fixed
- Se verifica que cuando hay cola habilitada y un usuario accede con cookie, esta aún existe y si no vuelve al principio de la cola.

## [2.33.0] - 2021-04-22
### Added
- Añade la columna con el número de entradas disponibles en las vistas de `Taquilla` y de `Compras y reservas`.

## [2.32.2] - 2021-04-20
### Fixed
- Se optimiza la generación de informes de sesión consiguiendo evitar el error por `timeout` excedido. 

## [2.32.1] - 2021-04-12
### Fixed
- Se optimiza la actualización de la sesión de un evento consiguiendo evitar el error por `timeout` excedido.
  
## [2.32.0] - 2021-03-25
### Added
- Se añade la posibilidad de presentar un enlace en el email de compra para anular las entradas
  gratuitas (campo `is_anulable_desde_enlace` de la tabla `par_cines`) siempre que el tiempo restante hasta la sesión sea superior al estipulado en la 
  configuración (campo `tiempo_anulable_desde_enlace` de la tabla `par_cines`). 
  
## Fixed
- Se corrige la funcionalidad de aplicar plantillas de reservas que provocaba error cuando no había compras ni reservas previas en la sesión.
