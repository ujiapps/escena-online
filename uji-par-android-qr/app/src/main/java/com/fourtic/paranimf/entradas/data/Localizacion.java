package com.fourtic.paranimf.entradas.data;

import com.google.gson.annotations.SerializedName;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Localizacion {
    @DatabaseField(columnName = "id", id = true)
    private transient Integer id;

    @SerializedName("codigo")
    @DatabaseField(columnName = "codigo")
    private String codigo;

    @SerializedName("nombreEs")
    @DatabaseField(columnName = "nombre")
    private String nombre;

    @DatabaseField(columnName = "seleccionada")
    private boolean seleccionada;

    @DatabaseField(foreign = true)
    private transient Sesion sesion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isSeleccionada() {
        return seleccionada;
    }

    public void setSeleccionada(boolean seleccionada) {
        this.seleccionada = seleccionada;
    }

    public Sesion getSesion() {
        return sesion;
    }

    public void setSesion(Sesion sesion) {
        this.sesion = sesion;
    }
}
