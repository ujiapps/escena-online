package com.fourtic.paranimf.entradas.rest;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.fourtic.paranimf.entradas.R;
import com.fourtic.paranimf.entradas.activity.SettingsActivity;
import com.fourtic.paranimf.entradas.data.Butaca;
import com.fourtic.paranimf.entradas.data.Evento;
import com.fourtic.paranimf.entradas.data.Localizacion;
import com.fourtic.paranimf.entradas.data.ResponseEventos;
import com.fourtic.paranimf.entradas.exception.EntradaPresentadaException;
import com.fourtic.paranimf.entradas.socket.MySSLSocketFactory;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class RestService {
	private static final String PORT_SEPARATOR = ":";

	private static String BASE_SECURE_URL = "/par-public/rest";

	private AsyncHttpClient client;
	private Gson gson;
	private Context context;
	private String url;
	private String apiKey;
	private int prevDays;

	@Inject
	public RestService(Application application) {
		setURLFromPreferences(application);
		setAPIKeyFromPreferences(application);
		setPrevDaysFromPreferences(application);
		this.context = application;
		this.client = new AsyncHttpClient();
		this.gson = new Gson();

		client.setTimeout(30000);
		initCookieStore(context);
		initSsl();
	}

	public void setURLFromPreferences(Context context) {
		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(context);
		String host = sharedPref.getString(SettingsActivity.PREF_HOST, "").trim();
		String port = sharedPref.getString(SettingsActivity.PREF_PORT, "").trim();

		if (host.endsWith("/"))
			host = host.substring(0, host.length() - 1);

		if (port != null && port.length() > 0)
			host += PORT_SEPARATOR + port;

		this.url = host;
	}

	public void setAPIKeyFromPreferences(Context context) {
		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(context);
		this.apiKey = sharedPref.getString(SettingsActivity.PREF_APIKEY, "").trim();
	}

	public void setPrevDaysFromPreferences(Context context) {
		SharedPreferences sharedPref = PreferenceManager
			.getDefaultSharedPreferences(context);
		this.prevDays = Integer.valueOf(sharedPref.getString(SettingsActivity.PREF_PREV_DAYS, "7"));
	}

	private void initSsl() {
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore
					.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			client.setSSLSocketFactory(sf);
		} catch (Exception e) {
			throw new AssertionError(e);
		}
	}

	private void initCookieStore(Context context) {
		PersistentCookieStore myCookieStore = new PersistentCookieStore(context);
		client.setCookieStore(myCookieStore);
	}

	private String getUrl() {
		return this.url;
	}

	private String getApiKey() {
		return "?key=" + this.apiKey;
	}

	private String getPrevDaysParam() {
		return "&prevDays=" + this.prevDays;
	}

	private Map<String, Object> createMap(Object... args) {
		Map<String, Object> result = new HashMap<String, Object>();

		for (int i = 0; i < args.length; i += 2) {
			result.put((String) args[i], args[i + 1]);
		}

		return result;
	}

	private HttpEntity mapToEntity(Map<String, Object> map)
			throws UnsupportedEncodingException {
		return new StringEntity(gson.toJson(map), "UTF-8");
	}

	/*
	 * public void authenticate(String username, String password, final
	 * ResultCallback<Void> responseHandler) { try { Map<String, Object> data =
	 * createMap("email", username, "contrasenya", password);
	 * 
	 * postJSON(BASE_SECURE_URL + "/loginTablet", data, new
	 * AsyncHttpResponseHandler(context, true) {
	 * 
	 * @Override public void onSuccess(int status, String response, boolean
	 * fromCache) { responseHandler.onSuccess(null); };
	 * 
	 * @Override public void onFailure(Throwable error, String errorBody) {
	 * responseHandler.onError(error, getErrorMessage(errorBody)); } }); } catch
	 * (Exception e) { responseHandler.onError(e, ""); } }
	 */

	protected String getErrorMessage(String errorBody) {
		try {
			return parseErrorBody(errorBody).getError();
		} catch (Exception e) {
			return "";
		}
	}

	protected RestError parseErrorBody(String errorBody) {
		Type collectionType = new TypeToken<RestError>() {
		}.getType();

		return gson.fromJson(errorBody, collectionType);
	}

	protected ResponseEventos parseEventos(String json) {
		Type collectionType = new TypeToken<ResponseEventos>() {
		}.getType();

		return gson.fromJson(json, collectionType);
	}

	protected List<Butaca> parseButacas(String json) {
		Type collectionType = new TypeToken<List<Butaca>>() {
		}.getType();

		return gson.fromJson(json, collectionType);
	}

	protected List<Localizacion> parseLocalizaciones(String json) {
		Type collectionType = new TypeToken<List<Localizacion>>() {
		}.getType();

		return gson.fromJson(json, collectionType);
	}

	public void getEventos(final ResultCallback<List<Evento>> responseHandler) {
		get(getUrl() + BASE_SECURE_URL + "/evento" + getApiKey() + "&" + getPrevDaysParam(),
				new AsyncHttpResponseHandler() {
					@Override
					public void onSuccess(String result) {
						try {
							ResponseEventos response = parseEventos(result);
							responseHandler.onSuccess(response.getEventos());
						} catch (Exception e) {
							responseHandler.onError(e,
									"Error recuperando eventos");
						}
					}

					@Override
					public void onFailure(Throwable error, String errorBody) {
						responseHandler.onError(error,
								getErrorMessage(errorBody));
					}
				});
	}

	public void getButacas(int idSesion,
			final ResultCallback<List<Butaca>> responseHandler) {
		get(getUrl() + BASE_SECURE_URL + "/sesion/" + idSesion + "/butacas"
				+ getApiKey(), new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String result) {
				responseHandler.onSuccess(parseButacas(result));
			}

			@Override
			public void onFailure(Throwable error, String errorBody) {
				responseHandler.onError(error, getErrorMessage(errorBody));
			}
		});
	}

	public void getLocalizaciones(int idSesion,
		final ResultCallback<List<Localizacion>> responseHandler) {
		get(getUrl() + BASE_SECURE_URL + "/sesion/" + idSesion + "/localizaciones"
			+ getApiKey(), new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String result) {
				responseHandler.onSuccess(parseLocalizaciones(result));
			}

			@Override
			public void onFailure(Throwable error, String errorBody) {
				responseHandler.onError(error, getErrorMessage(errorBody));
			}
		});
	}

	public void updatePresentadas(int idSesion, List<Butaca> butacas,
			final ResultCallback<Void> responseHandler) {
		String url = getUrl() + BASE_SECURE_URL + "/sesion/" + idSesion
				+ getApiKey();

		HttpEntity entity = null;
		try {
			String json = gson.toJson(butacas);
			entity = new StringEntity(json, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			responseHandler.onError(e, "Error toJson en POST");
		}

		client.post(context, url, defaultHeaders(), entity, "application/json",
				new AsyncHttpResponseHandler() {
					@Override
					public void onSuccess(int arg0, String response) {
						responseHandler.onSuccess(null);
					}

					@Override
					public void onFailure(Throwable throwable, String body) {
						responseHandler.onError(throwable,
								getErrorMessage(body));
					}
				});
	}

    public void updateOnlinePresentada(int idSesion, Butaca butaca,
                                  final ResultCallback<Void> responseHandler) {
        String url = getUrl() + BASE_SECURE_URL + "/sesion/" + idSesion
                + "/online" +  getApiKey();

        HttpEntity entity = null;
        try {
            String json = gson.toJson(butaca);
            entity = new StringEntity(json, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            responseHandler.onError(e, "Error toJson en POST");
        }

        client.setTimeout(5000);
        client.post(context, url, defaultHeaders(), entity, "application/json",
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int arg0, String response) {
                        client.setTimeout(30000);
                        try {
                            RestResponse restResponse = gson.fromJson(response, RestResponse.class);
                            if (restResponse.getSuccess()) {
                                responseHandler.onSuccess(null);
                            }
                            else {
                                responseHandler.onError(new EntradaPresentadaException(), context.getString(R.string.ya_presentada));
                            }
                        } catch (JsonSyntaxException e) {
                            responseHandler.onError(e, "Error fromJson al recibir la respuesta del POST");
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, String body) {
                        client.setTimeout(30000);
                        responseHandler.onError(throwable, getErrorMessage(body));
                    }
                });
    }

	private void get(final String url,
			final AsyncHttpResponseHandler responseHandler) {
		client.get(context, url, defaultHeaders(), null,
				new AsyncHttpResponseHandler() {
					@Override
					public void onSuccess(int status, String result) {
						responseHandler.onSuccess(status, result);
					}

					@Override
					public void onFailure(Throwable throwable, String message) {
						responseHandler.onFailure(throwable, message);
					}
				});
	}

	private Header[] defaultHeaders() {
		return new Header[] { new BasicHeader("X-Requested-With",
				"XMLHttpRequest") };
	}

	public interface ResultCallback<T> {
		void onSuccess(T successData);

		void onError(Throwable throwable, String errorMessage);
	}
}
