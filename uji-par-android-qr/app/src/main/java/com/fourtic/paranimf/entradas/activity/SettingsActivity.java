package com.fourtic.paranimf.entradas.activity;

import com.fourtic.paranimf.entradas.services.ScanService;
import com.google.inject.Inject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;

import com.fourtic.paranimf.entradas.R;
import com.fourtic.paranimf.entradas.activity.base.BasePreferenceActivity;
import com.fourtic.paranimf.entradas.rest.RestService;
import com.fourtic.paranimf.entradas.services.OptionsService;

public class SettingsActivity extends BasePreferenceActivity implements
        OnSharedPreferenceChangeListener, OnScanResultListener {
    public static final String PREF_QR_SCANN = "qr_scann";
    public static final String PREF_HOST = "pref_host";
    public static final String PREF_PORT = "pref_port";
    public static final String PREF_APIKEY = "pref_apikey";
    public static final String PREF_EXT_SCAN = "pref_ext_scan";
    public static final String PREF_WAIT_VALIDATION = "pref_wait_validation";
    public static final String PREF_PREV_DAYS = "pref_prev_days";

    @Inject
    private RestService rest;

    @Inject
    private OptionsService options;

    @Inject
    private ScanService scanService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        setTitle(R.string.action_settings);

        init();
    }

    private void init() {
        for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); i++) {
            initSummary(getPreferenceScreen().getPreference(i));
        }

        Preference button = findPreference(PREF_QR_SCANN);
        button.setOnPreferenceClickListener(preference -> {
            scanService.abreActividadEscanear();
            return true;
        });
    }

    @Override
    protected void onActivityResult(
            int requestCode,
            int resultCode,
            Intent data
    ) {
        scanService.onActivityResult(requestCode, resultCode, data, this);
    }

    public void processScanResult(String scanningResult) {
        String[] scanValues = scanningResult.split("@@@");
        if (scanValues.length == 3) {
            SharedPreferences sharedPref = PreferenceManager
                    .getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(PREF_HOST, scanValues[0]);
            editor.putString(PREF_PORT, scanValues[1]);
            editor.putString(PREF_APIKEY, scanValues[2]);
            editor.apply();

            onSharedPreferenceChanged(sharedPref, PREF_HOST);
            setPreferenceScreen(null);
            addPreferencesFromResource(R.xml.preferences);
            init();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        rest.setURLFromPreferences(this);
        rest.setAPIKeyFromPreferences(this);
        rest.setPrevDaysFromPreferences(this);
        options.setLectorExternoFromPreferences(this);
        options.setWaitValidationFromPreferences(this);

        updatePrefSummary(findPreference(key));
    }

    private void initSummary(Preference p) {
        if (p instanceof PreferenceCategory) {
            PreferenceCategory pCat = (PreferenceCategory) p;
            for (int i = 0; i < pCat.getPreferenceCount(); i++) {
                initSummary(pCat.getPreference(i));
            }
        } else {
            updatePrefSummary(p);
        }
    }

    private void updatePrefSummary(Preference p) {
        if (p instanceof EditTextPreference) {
            EditTextPreference editTextPref = (EditTextPreference) p;
            String text = editTextPref.getText();
            if (text != null && text.length() > 0)
                p.setSummary(editTextPref.getText());
            else {
                int resourceId = getResources().getIdentifier("@string/" + p.getKey() + "_summ", "strings", this.getPackageName());
                if (resourceId > 0)
                    p.setSummary(getResources().getString(resourceId));
            }
        }
    }
}
