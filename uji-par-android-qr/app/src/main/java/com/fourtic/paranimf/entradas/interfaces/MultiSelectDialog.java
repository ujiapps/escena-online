package com.fourtic.paranimf.entradas.interfaces;

public interface MultiSelectDialog {
    void onDialogResult(
        int code,
        boolean[] selectedItems
    );
}
