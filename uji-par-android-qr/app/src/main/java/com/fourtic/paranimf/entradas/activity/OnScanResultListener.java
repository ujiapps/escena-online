package com.fourtic.paranimf.entradas.activity;

public interface OnScanResultListener {
    void processScanResult(String scanningResult);
}
