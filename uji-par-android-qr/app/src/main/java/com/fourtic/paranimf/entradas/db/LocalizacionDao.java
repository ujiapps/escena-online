package com.fourtic.paranimf.entradas.db;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import android.util.Log;

import com.fourtic.paranimf.entradas.constants.Constants;
import com.fourtic.paranimf.entradas.data.Localizacion;
import com.fourtic.paranimf.entradas.data.Sesion;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@Singleton
public class LocalizacionDao
{
    DbHelperService dbHelper;

    private Dao<Localizacion, Integer> dao;

    @Inject
    private SesionDao sesionDao;

    @Inject
    public LocalizacionDao(DbHelperService dbHelper)
    {
        this(dbHelper.getHelper());
        this.dbHelper = dbHelper;
    }

    public LocalizacionDao(DBHelper dbHelper)
    {
        try
        {
            dao = dbHelper.getLocalizacionDao();
        }
        catch (SQLException e)
        {
            Log.e(Constants.TAG, "Error iniciando dao Butaca", e);
        }
    }

    public List<Localizacion> getLocalizaciones(int sesionId) throws SQLException
    {
        List<Localizacion> butacas = dao.queryForEq("sesion_id", sesionId);
        
        return butacas;
    }

    public List<String> getCodigoLocalizacionesSeleccionadas(int sesionId) throws SQLException
    {
        QueryBuilder<Localizacion, Integer> builder = dao.queryBuilder();
        builder.where().eq("sesion_id", sesionId).and().eq("seleccionada", true);

        List<Localizacion> localizaciones = builder.query();
        List<String> codigoLocalizaciones = new ArrayList<String>();
        for (Localizacion localizacion : localizaciones) {
            codigoLocalizaciones.add(localizacion.getCodigo());
        }

        return codigoLocalizaciones;
    }

    public void actualizaLocalizaciones(final int sesionId, final List<Localizacion> localizaciones) throws SQLException
    {
        TransactionManager.callInTransaction(dao.getConnectionSource(), (Callable<Void>) () -> {
            guardaLocalizaciones(sesionId, localizaciones);

            return null;
        });
    }

    private void guardaLocalizaciones(final int sesionId, final List<Localizacion> localizaciones) throws SQLException
    {
        Sesion sesion = sesionDao.getPorId(sesionId);

        DeleteBuilder<Localizacion, Integer> builder = dao.deleteBuilder();
        builder.where().eq("sesion_id", sesionId);
        builder.delete();

        for (Localizacion localizacion : localizaciones)
        {
            insertaLocalizacion(sesion, localizacion);
        }
    }

    private void insertaLocalizacion(Sesion sesion, Localizacion localizacion) throws SQLException
    {
        localizacion.setSesion(sesion);

        dao.create(localizacion);
    }
}
