package com.fourtic.paranimf.entradas.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.fourtic.paranimf.entradas.R;
import com.fourtic.paranimf.entradas.interfaces.MultiSelectDialog;

public class LocalizacionesDialogFragment extends DialogFragment {
    public static final String ITEMS = "items";
    public static final String SELECTED_ITEMS = "selectedItems";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final CharSequence[] items = getArguments().getCharSequenceArray(ITEMS);
        final boolean[] selectedItems = getArguments().getBooleanArray(SELECTED_ITEMS);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMultiChoiceItems(items, selectedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(
                DialogInterface dialog,
                int which,
                boolean isChecked
            ) {
                selectedItems[which] = isChecked;
            }
        }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(
                DialogInterface dialog,
                int id
            ) {
                notifyToTarget(Activity.RESULT_OK, selectedItems);
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(
                DialogInterface dialog,
                int id
            ) {
                notifyToTarget(Activity.RESULT_CANCELED, selectedItems);
            }
        });
        ;

        return builder.create();
    }

    private void notifyToTarget(
        int code,
        boolean[] selectedItems
    ) {
        ((MultiSelectDialog) getActivity()).onDialogResult(code, selectedItems);
    }

}
