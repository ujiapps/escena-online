package com.fourtic.paranimf.entradas.services;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.fourtic.paranimf.entradas.R;
import com.fourtic.paranimf.entradas.activity.LectorExternoActivity;
import com.fourtic.paranimf.entradas.activity.OnScanResultListener;
import com.fourtic.paranimf.entradas.activity.SettingsActivity;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.zxing.integration.android.IntentIntegrator;

public class ScanService {
	private static final String BARCODE_SCANNER_PACKAGE = "com.google.zxing.client.android";
	public static final int EXTERNAL_SCANNER_REQUEST = 1;
	public static final int BARCODE_SCANNER_REQUEST = 49374;

	Activity context;

	@Inject
	public ScanService(Activity context) {
		this.context = context;
	}

	public void abreActividadEscanear() {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.context);
		boolean hasExtScan = sharedPref.getBoolean(SettingsActivity.PREF_EXT_SCAN, false);
		if (hasExtScan) {
			Intent intent = new Intent(this.context, LectorExternoActivity.class);
			this.context.startActivityForResult(intent, EXTERNAL_SCANNER_REQUEST);
		} else {
			if (aplicacionInstalada(BARCODE_SCANNER_PACKAGE)) {
				IntentIntegrator scanIntegrator = new IntentIntegrator(this.context);
				scanIntegrator.initiateScan();
			} else {
				Toast.makeText(this.context, R.string.instalar_barcode_scanner, Toast.LENGTH_LONG).show();
				abrirGooglePlay(BARCODE_SCANNER_PACKAGE);
			}
		}
	}

	private void abrirGooglePlay(String appPackage) {
		try {
			this.context.startActivity(
					new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackage)));
		} catch (android.content.ActivityNotFoundException anfe) {
			this.context.startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://play.google.com/store/apps/details?id=" + appPackage)));
		}
	}

	private boolean aplicacionInstalada(String uri) {
		PackageManager pm = this.context.getPackageManager();
		boolean app_installed;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	public void onActivityResult(
			int requestCode,
			int resultCode,
			Intent data,
			OnScanResultListener scanResult
	) {
		if ((requestCode == ScanService.EXTERNAL_SCANNER_REQUEST || requestCode == ScanService.BARCODE_SCANNER_REQUEST)
				&& resultCode == Activity.RESULT_OK) {
			try {
				String scanningResult;
				if (requestCode == ScanService.BARCODE_SCANNER_REQUEST) {
					scanningResult =
							IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
									.getContents();
				} else {
					scanningResult = data.getAction();
				}

				if (scanningResult != null) {
					scanResult.processScanResult(scanningResult);
				} else {
					Toast toast = Toast.makeText(this.context, "No scan data received!",
							Toast.LENGTH_SHORT);
					toast.show();
				}
			} catch (Exception e) {
				Toast toast = Toast.makeText(this.context, "No scan data received!",
						Toast.LENGTH_SHORT);
				toast.show();
			}
		}
	}
}
