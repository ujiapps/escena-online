package com.fourtic.paranimf.entradas.db;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import android.util.Log;

import com.fourtic.paranimf.entradas.constants.Constants;
import com.fourtic.paranimf.entradas.data.Butaca;
import com.fourtic.paranimf.entradas.data.Sesion;
import com.fourtic.paranimf.entradas.exception.ButacaDeOtraSesionException;
import com.fourtic.paranimf.entradas.exception.ButacaNoEncontradaException;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

@Singleton
public class ButacaDao {
    DbHelperService dbHelper;

    private Dao<Butaca, Integer> butacasDao;

    @Inject
    private LocalizacionDao localizacionDao;

    @Inject
    private SesionDao sesionDao;

    @Inject
    public ButacaDao(DbHelperService dbHelper) {
        this.dbHelper = dbHelper;

        try {
            butacasDao = dbHelper.getHelper().getButacaDao();
        } catch (SQLException e) {
            Log.e(Constants.TAG, "Error iniciando dao Butaca", e);
        }
    }

    public List<Butaca> getButacas(int sesionId) throws SQLException {
        Where<Butaca, Integer> where = butacasDao.queryBuilder().where();
        where.eq("sesion_id", sesionId);

        filtraBuilderByLocalizacion(sesionId, where);

        List<Butaca> butacas = where.query();
        actualizaFechaPresentadaEpoch(butacas);

        return butacas;
    }

    private void filtraBuilderByLocalizacion(
        int sesionId,
        Where<Butaca, Integer> where
    ) throws SQLException {
        List<String> codigosLocalizacionesSeleccionadas = localizacionDao.getCodigoLocalizacionesSeleccionadas(sesionId);
        if (codigosLocalizacionesSeleccionadas.size() > 0) {
            where.and()
                .in("localizacion", codigosLocalizacionesSeleccionadas);
        }
    }

    public List<Butaca> getButacasModificadas(int sesionId) throws SQLException {
        QueryBuilder<Butaca, Integer> builder = butacasDao.queryBuilder();
        builder.where().eq("sesion_id", sesionId).and().eq("modificada", true);

        List<Butaca> butacas = builder.query();

        actualizaFechaPresentadaEpoch(butacas);

        return butacas;
    }

    public boolean hayButacasModificadas(int sesionId) throws SQLException {
        return getButacasModificadas(sesionId).size() > 0;
    }

    // Cuando se interactua con REST se usa el campo fechaPresentadaEpoch, en cambio para guardar en base de datos se usa el fechaPresentada
    private void actualizaFechaPresentadaEpoch(List<Butaca> butacas) {
        for (Butaca butaca : butacas) {
            if (butaca.getFechaPresentada() != null)
                butaca.setFechaPresentadaEpoch(butaca.getFechaPresentada().getTime());
        }
    }

    public long getNumeroButacas(int sesionId) throws SQLException {
        Where<Butaca, Integer> where = butacasDao.queryBuilder().where();

        where.eq("sesion_id", sesionId);
        filtraBuilderByLocalizacion(sesionId, where);

        return where.countOf();
    }

    public long getNumeroButacasPresentadas(int sesionId) throws SQLException {
        Where<Butaca, Integer> where = butacasDao.queryBuilder().where();

        where.eq("sesion_id", sesionId).and().isNotNull("presentada");
        filtraBuilderByLocalizacion(sesionId, where);

        return where.countOf();
    }

    public long getNumeroButacasModificadas(int sesionId) throws SQLException {
        Where<Butaca, Integer> where = butacasDao.queryBuilder().where();

        where.eq("sesion_id", sesionId).and().eq("modificada", true);
        filtraBuilderByLocalizacion(sesionId, where);

        return where.countOf();
    }

    public Butaca getButacaPorUuid(
        int sesionId,
        String uuid,
        boolean localizacionesFiltradas
    ) throws SQLException, ButacaNoEncontradaException, ButacaDeOtraSesionException {
        Where<Butaca, Integer> where = butacasDao.queryBuilder().where();
        where.eq("uuid", uuid);
        if (localizacionesFiltradas) {
            filtraBuilderByLocalizacion(sesionId, where);
        }
        List<Butaca> butacas = where.query();

        if (butacas.size() == 0) {
            throw new ButacaNoEncontradaException();
        } else {
            Butaca butaca = butacas.get(0);

            if (butaca.getSesion().getId() != sesionId) {
                throw new ButacaDeOtraSesionException();
            } else {
                return butacas.get(0);
            }
        }
    }

    public Butaca getButacaPorUuid(
        int sesionId,
        String uuid
    ) throws SQLException, ButacaNoEncontradaException, ButacaDeOtraSesionException {
        return getButacaPorUuid(sesionId, uuid, true);
    }

    public void actualizaButacas(
        final int sesionId,
        final List<Butaca> butacas
    ) throws SQLException {
        TransactionManager.callInTransaction(this.butacasDao.getConnectionSource(), new Callable<Void>() {
            public Void call() throws Exception {
                guardaButacas(sesionId, butacas);
                sesionDao.actualizaFechaSincronizacion(sesionId, new Date());

                return null;
            }
        });
    }

    private void guardaButacas(
        final int sesionId,
        final List<Butaca> butacas
    ) throws SQLException {
        Sesion sesion = sesionDao.getPorId(sesionId);

        DeleteBuilder<Butaca, Integer> builder = this.butacasDao.deleteBuilder();
        builder.where().eq("sesion_id", sesionId);
        builder.delete();

        for (Butaca butaca : butacas) {
            insertaButaca(sesion, butaca);
        }
    }

    private void insertaButaca(
        Sesion sesion,
        Butaca butaca
    ) throws SQLException {
        butaca.setSesion(sesion);

        if (butaca.getFechaPresentadaEpoch() != 0)
            butaca.setFechaPresentada(new Date(butaca.getFechaPresentadaEpoch()));

        butaca.setModificada(false);

        butacasDao.create(butaca);
    }

    public void actualizaFechaPresentada(
        String uuid,
        Date date
    ) throws SQLException {
        UpdateBuilder<Butaca, Integer> builder = butacasDao.updateBuilder();
        builder.where().eq("uuid", uuid);
        builder.updateColumnValue("presentada", date);
        builder.updateColumnValue("modificada", true);
        builder.update();
    }

    public List<Butaca> getButacasNoPresentadasPorUuid(
        int sesionId,
        String uuid
    ) throws SQLException {
        Where<Butaca, Integer> where = butacasDao.queryBuilder().where();
        where.eq("sesion_id", sesionId).and().isNull("presentada").and()
            .like("uuid", "%-%-%-%-%-%" + uuid + "%");
        filtraBuilderByLocalizacion(sesionId, where);

        return where.query();
    }
}
