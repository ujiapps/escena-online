package com.fourtic.paranimf.entradas.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.fourtic.paranimf.entradas.activity.SettingsActivity;

@Singleton
public class OptionsService {
	@Inject
	public OptionsService(Application application) {
		setLectorExternoFromPreferences(application);
	}

	public void setLectorExternoFromPreferences(Context context) {
		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(context);
		sharedPref.getBoolean(SettingsActivity.PREF_EXT_SCAN, false);
	}

	public void setWaitValidationFromPreferences(Context context) {
		SharedPreferences sharedPref = PreferenceManager
			.getDefaultSharedPreferences(context);
		sharedPref.getInt(SettingsActivity.PREF_WAIT_VALIDATION, 1000);
	}
}
