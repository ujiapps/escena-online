package es.uji.apps.par.services.reports;

import org.h2.tools.Server;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.fopreports.serialization.ReportSerializationException;
import es.uji.apps.par.auth.Role;
import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.EventoBuilder;
import es.uji.apps.par.builders.LocalizacionBuilder;
import es.uji.apps.par.builders.PlantillaBuilder;
import es.uji.apps.par.builders.ReportBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.SesionBuilder;
import es.uji.apps.par.builders.TarifaBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.builders.TpvBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.dao.ComprasDAO;
import es.uji.apps.par.dao.SesionesDAO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.db.TpvsDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.report.EntradaReportFactory;
import es.uji.apps.par.report.InformeInterface;
import es.uji.apps.par.services.EntradasService;
import es.uji.apps.par.services.ExcelService;
import es.uji.apps.par.services.ReportService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext-db-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class ReportTest {

    @Autowired
    SesionesDAO sesionesDAO;

    @Autowired
    ComprasDAO comprasDAO;

    @Autowired
    ReportService reportService;

    @Autowired
    EntradasService entradasService;

    @PersistenceContext
    protected EntityManager entityManager;

    TipoEventoDTO tipoEvento;
    CineDTO cine;
    SesionDTO sesion;
    UsuarioDTO usuario;
    TarifaDTO tarifa;
    LocalizacionDTO localizacion;
    TpvsDTO tpv;
    final String RSS_ID = "100";

    @Before
    public void setUp() {
        cine = new CineBuilder("Cine 1").build(entityManager);

        tipoEvento = new TipoEventoBuilder("tipo", "tipo", false, cine).build(entityManager);

        SalaDTO sala = new SalaBuilder("Sala 1", cine).build(entityManager);

        tpv = new TpvBuilder("ejemplo").withSala(sala).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala).build(entityManager);

        EventoDTO evento =
            new EventoBuilder("Evento 1", "Esdeveniment 1", cine, tipoEvento).withTpv(tpv).withRssId(RSS_ID).build(entityManager);

        tarifa = new TarifaBuilder("Tarifa 1").build(cine, entityManager);

        localizacion = new LocalizacionBuilder("Localizacion", "LOC", new BigDecimal(200))
            .withSala(sala).build(entityManager);

        usuario = new UsuarioBuilder("User 1", "user1@test.com", "user1").withSala(sala).build(entityManager);

        sesion = new SesionBuilder("Sesión 1", sala, evento, "12:00")
            .withCompraTaquilla("Juan", BigDecimal.valueOf(24.0), 4, tarifa, localizacion, "1234-1", usuario)
            .withCompraTaquilla("Pepe", BigDecimal.valueOf(24.0), 4, tarifa, localizacion, "1234-2", usuario)
            .withCompraTaquillaTPV("Antonio", BigDecimal.valueOf(12.0), null, 3, tarifa, localizacion, "1234-3", "0000123",
                usuario).withCompra("Manuel", BigDecimal.valueOf(12.0), 2, tarifa, localizacion, "1234-3")
            .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void testNombreClaseInformePDFTaquilla() throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeTaquillaReport",
            EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA, cine).build(entityManager);

        InformeInterface informe = reportService
            .generaYRellenaInformePDFTaquilla("2016-01-01", "2016-02-01", new Locale("es"), usuario.getUsuario(),
                "logo", true, true, "ciudad", null, false, "1");
        Assert.assertNotNull(informe);
        Assert.assertEquals("es.uji.apps.par.report.InformeTaquillaReport", informe.getClass().getName());
    }

    @Test(expected = RuntimeException.class)
    public void testNombreClaseInformePDFTaquillaCuandoReportClassNoExiste()
        throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeTaquillaReportPersonalizado",
            EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA, cine).build(entityManager);

        reportService
            .generaYRellenaInformePDFTaquilla("2016-01-01", "2016-02-01", new Locale("es"), usuario.getUsuario(),
                "logo", true, true, "ciudad", null, false, "1");
    }

    @Test(expected = RuntimeException.class)
    public void testNombreClaseInformePDFTaquillaWhenUserNotExists()
        throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeTaquillaReport",
            EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA, cine).build(entityManager);

        reportService
            .generaYRellenaInformePDFTaquilla("2016-01-01", "2016-02-01", new Locale("es"), "NOTEXISTS", "logo", true,
                true, "ciudad", null, false, "1");
    }

    @Test
    public void testNombreClaseInformePDFEfectivo() throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeEfectivoReport",
            EntradaReportFactory.TIPO_INFORME_PDF_EFECTIVO, cine).build(entityManager);

        InformeInterface informe = reportService
            .generaYRellenaInformePDFEfectivo("2016-01-01", "2016-02-01", new Locale("es"), usuario.getUsuario(),
                "logo", true, true, "ciudad", null, "1");
        Assert.assertNotNull(informe);
        Assert.assertEquals("es.uji.apps.par.report.InformeEfectivoReport", informe.getClass().getName());
    }

    @Test(expected = RuntimeException.class)
    public void testNombreClaseInformePDFEfectivoCuandoReportClassNoExiste()
        throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeEfectivoReportPersonalizado",
            EntradaReportFactory.TIPO_INFORME_PDF_EFECTIVO, cine).build(entityManager);

        reportService
            .generaYRellenaInformePDFEfectivo("2016-01-01", "2016-02-01", new Locale("es"), usuario.getUsuario(),
                "logo", true, true, "ciudad", null, "1");
    }

    @Test(expected = RuntimeException.class)
    public void testNombreClaseInformePDFEfectivoWhenUserNotExists()
        throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeEfectivoReport",
            EntradaReportFactory.TIPO_INFORME_PDF_EFECTIVO, cine).build(entityManager);

        reportService
            .generaYRellenaInformePDFEfectivo("2016-01-01", "2016-02-01", new Locale("es"), "NOTEXISTS", "logo", true,
                true, "ciudad", null, "1");
    }

    @Test
    public void testNombreClaseInformePDFTaquillaTpvSubtotales() throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeTaquillaTpvSubtotalesReport",
            EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA_TPV_SUBTOTALES, cine).build(entityManager);

        InformeInterface informe = reportService
            .generaYRellenaInformePDFTaquillaTPVSubtotales("2016-01-01", "2016-02-01", new Locale("es"),
                usuario.getUsuario(), "logo", true, true, "ciudad", null, "1");
        Assert.assertNotNull(informe);
        Assert.assertEquals("es.uji.apps.par.report.InformeTaquillaTpvSubtotalesReport", informe.getClass().getName());
    }

    @Test(expected = RuntimeException.class)
    public void testNombreClaseInformePDFTaquillaTpvSubtotalesCuandoReportClassNoExiste()
        throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeTaquillaTpvSubtotalesReportPersonalizado",
            EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA_TPV_SUBTOTALES, cine).build(entityManager);

        reportService.generaYRellenaInformePDFTaquillaTPVSubtotales("2016-01-01", "2016-02-01", new Locale("es"),
            usuario.getUsuario(), "logo", true, true, "ciudad", null, "1");
    }

    @Test(expected = RuntimeException.class)
    public void testNombreClaseInformePDFTaquillaTpvSubtotalesWhenUserNotExists()
        throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeTaquillaTpvSubtotalesReport",
            EntradaReportFactory.TIPO_INFORME_PDF_TAQUILLA_TPV_SUBTOTALES, cine).build(entityManager);

        reportService
            .generaYRellenaInformePDFTaquillaTPVSubtotales("2016-01-01", "2016-02-01", new Locale("es"), "NOTEXISTS",
                "logo", true, true, "ciudad", null, "1");
    }

    @Test
    public void testNombreClaseInformePDFEventos() throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeEventosReport", EntradaReportFactory.TIPO_INFORME_PDF_EVENTOS,
            cine).build(entityManager);

        try {
            Server.createWebServer().start();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        InformeInterface informe = reportService
            .generaYRellenaInformePDFEventos("2014-01-01", "2050-02-01", new Locale("es"), usuario.getUsuario(), "logo",
                true, true, "ciudad", null, String.valueOf(tpv.getId()));
        Assert.assertNotNull(informe);
        Assert.assertEquals("es.uji.apps.par.report.InformeEventosReport", informe.getClass().getName());
    }

    @Test(expected = RuntimeException.class)
    public void testNombreClaseInformePDFEventosCuandoReportClassNoExiste()
        throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeEventosReportPersonalizado",
            EntradaReportFactory.TIPO_INFORME_PDF_EVENTOS, cine).build(entityManager);

        reportService
            .generaYRellenaInformePDFEventos("2016-01-01", "2016-02-01", new Locale("es"), usuario.getUsuario(), "logo",
                true, true, "ciudad", null, "1");
    }

    @Test(expected = RuntimeException.class)
    public void testNombreClaseInformePDFEventosWhenUserNotExists()
        throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeEventosReport", EntradaReportFactory.TIPO_INFORME_PDF_EVENTOS,
            cine).build(entityManager);

        reportService
            .generaYRellenaInformePDFEventos("2016-01-01", "2016-02-01", new Locale("es"), "NOTEXISTS", "logo", true,
                true, "ciudad", null, "1");
    }

    @Test
    public void testNombreClaseInformePDFSesiones() throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeSesionReport", EntradaReportFactory.TIPO_INFORME_PDF_SESIONES,
            cine).build(entityManager);

        InformeInterface informe = reportService
            .generaYRellenaPDFSesiones(Arrays.asList(new Sesion(sesion)), new Locale("es"), usuario.getUsuario(),
                "logo", true, true, "ciudad", true, null, false, null, null);
        Assert.assertNotNull(informe);
        Assert.assertEquals("es.uji.apps.par.report.InformeSesionReport", informe.getClass().getName());
    }

    @Test(expected = RuntimeException.class)
    public void testNombreClaseInformePDFSesionesCuandoReportClassNoExiste()
        throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeSesionReportPersonalizado",
            EntradaReportFactory.TIPO_INFORME_PDF_SESIONES, cine).build(entityManager);

        reportService
            .generaYRellenaPDFSesiones(Arrays.asList(new Sesion(sesion)), new Locale("es"), usuario.getUsuario(),
                "logo", true, true, "ciudad", true, null, false, null, null);
    }

    @Test(expected = RuntimeException.class)
    public void testNombreClaseInformePDFSesionesWhenUserNotExists()
        throws ReportSerializationException, ParseException {
        new ReportBuilder("es.uji.apps.par.report.InformeSesionReport", EntradaReportFactory.TIPO_INFORME_PDF_SESIONES,
            cine).build(entityManager);

        reportService
            .generaYRellenaPDFSesiones(Arrays.asList(new Sesion(sesion)), new Locale("es"), "NOTEXISTS", "logo", true,
                true, "ciudad", true, null, false, null, null);
    }

    @Test
    public void testInformeExcelTaquillaSoloMuestraDatosDeLasSalasDelUsuario() throws ParseException {
        CompraDTO compra = sesion.getParCompras().get(0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine).build(entityManager);

        TpvsDTO tpv2 = new TpvBuilder("ejemplo 2").withSala(sala2).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala2).build(entityManager);

        EventoDTO evento2 =
            new EventoBuilder("Evento 2", "Esdeveniment 2", cine, tipoEvento).withTpv(tpv2).withRssId(RSS_ID).build(entityManager);

        UsuarioDTO usuario2 =
            new UsuarioBuilder("User 2", "user2@test.com", "user2").withRole(Role.TAQUILLA).withSala(sala2)
                .build(entityManager);

        SesionDTO sesion2 = new SesionBuilder("Sesión 2", sala2, evento2, "13:00")
            .withCompraTaquilla("Juan", BigDecimal.valueOf(24.0), 2, tarifa, localizacion, "1234-2-1", usuario)
            .withCompraTaquilla("Dani", BigDecimal.valueOf(36.0), 3, tarifa, localizacion, "1234-2-1", usuario2)
            .build(entityManager);

        ExcelService excelService = reportService
            .getExcelServiceVentas(sdf.format(cal.getTime()), sdf.format(compra.getFecha()), new Locale("es"),
                usuario.getUsuario(), false, String.valueOf(tpv.getId()));
        Assert.assertEquals(2, excelService.getTotalFilas());
        Object horaSesion = excelService.getCellValue("B2");
        Assert.assertEquals("12:00", horaSesion.toString().split(" ")[1]);
        Object butacasSesion = excelService.getCellValue("E2");
        Assert.assertEquals(11.0, butacasSesion);

        compra = sesion2.getParCompras().get(0);
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);

        ExcelService excelServiceSesion2 = reportService
            .getExcelServiceVentas(sdf.format(cal.getTime()), sdf.format(compra.getFecha()), new Locale("es"),
                usuario2.getUsuario(), false, String.valueOf(tpv2.getId()));
        Assert.assertEquals(2, excelServiceSesion2.getTotalFilas());
        Object horaSesion2 = excelServiceSesion2.getCellValue("B2");
        Assert.assertEquals("13:00", horaSesion2.toString().split(" ")[1]);
        Object cantidadDeButacasSesion2 = excelServiceSesion2.getCellValue("E2");
        Assert.assertEquals(3.0, cantidadDeButacasSesion2);
    }

    @Test
    public void testInformeExcelVentasSoloMuestraDatosDeLasSalasDelUsuario() throws IOException, ParseException {
        CompraDTO compra = sesion.getParCompras().get(0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine).build(entityManager);

        TpvsDTO tpv2 = new TpvBuilder("ejemplo 2").withSala(sala2).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala2).build(entityManager);

        EventoDTO evento2 =
            new EventoBuilder("Evento 2", "Esdeveniment 2", cine, tipoEvento).withTpv(tpv2).withRssId(RSS_ID).build(entityManager);

        SesionDTO sesion2 = new SesionBuilder("Sesión 2", sala2, evento2, "13:00")
            .withCompra("Juan", BigDecimal.valueOf(24.0), 2, tarifa, localizacion, "1234-2-1").build(entityManager);

        UsuarioDTO usuario2 =
            new UsuarioBuilder("User 2", "user2@test.com", "user2").withSala(sala2).build(entityManager);

        ExcelService excelServiceConTresCompras = reportService
            .getExcelServiceVentas(sdf.format(cal.getTime()), sdf.format(compra.getFecha()), new Locale("es"),
                usuario.getUsuario(), true, String.valueOf(tpv.getId()));
        Assert.assertEquals(2, excelServiceConTresCompras.getTotalFilas());
        Object fechaSesion1 = excelServiceConTresCompras.getCellValue("B2");
        Assert.assertEquals("12:00", fechaSesion1.toString().split(" ")[1]);
        Object butacasComprasSesion1 = excelServiceConTresCompras.getCellValue("E2");
        Assert.assertEquals(13.0, butacasComprasSesion1);

        compra = sesion2.getParCompras().get(0);
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);

        ExcelService excelServiceSesion2 = reportService
            .getExcelServiceVentas(sdf.format(cal.getTime()), sdf.format(compra.getFecha()), new Locale("es"),
                usuario2.getUsuario(), true, String.valueOf(tpv2.getId()));
        Assert.assertEquals(2, excelServiceSesion2.getTotalFilas());
        Object fechaSesion2 = excelServiceSesion2.getCellValue("B2");
        Assert.assertEquals("13:00", fechaSesion2.toString().split(" ")[1]);
        Object butacasSesion2 = excelServiceSesion2.getCellValue("E2");
        Assert.assertEquals(2.0, butacasSesion2);
    }

    @Test
    public void testInformeExcelEventosSoloMuestraDatosDeLasSalasDelUsuario() throws IOException, ParseException {
        SalaDTO sala2 = new SalaBuilder("Sala 2", cine).build(entityManager);

        TpvsDTO tpv = new TpvBuilder("ejemplo 2").withSala(sala2).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala2).build(entityManager);

        EventoDTO evento2 =
            new EventoBuilder("Evento 2", "Esdeveniment 2", cine, tipoEvento).withTpv(tpv).withRssId(RSS_ID)
                .build(entityManager);

        SesionDTO sesion2 = new SesionBuilder("Sesión 2", sala2, evento2, "13:00")
            .withCompra("Juan", BigDecimal.valueOf(24.0), 2, tarifa, localizacion, "1234-2-1").build(entityManager);

        UsuarioDTO usuario2 =
            new UsuarioBuilder("User 2", "user2@test.com", "user2").withSala(sala2).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        CompraDTO compra = sesion2.getParCompras().get(0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        ExcelService excelServiceConUnaCompra = reportService
            .getExcelServiceEventos(sdf.format(cal.getTime()), sdf.format(compra.getFecha()), new Locale("es"),
                usuario2.getUsuario(), String.valueOf(tpv.getId()));
        Assert.assertEquals(2, excelServiceConUnaCompra.getTotalFilas());
        Object butacasSesion2 = excelServiceConUnaCompra.getCellValue("D2");
        Assert.assertEquals(2.0, butacasSesion2);
    }

    @Test
    public void testInformePDFTaquillaDondeTotalTaquillaTPVSoloMuestranDatosDeLasSalasDelUsuario()
        throws IOException, ParseException {
        CompraDTO compra = sesion.getParCompras().get(0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fechaInicio = sdf.format(cal.getTime());
        String fechaFin = sdf.format(compra.getFecha());

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine).build(entityManager);

        new TpvBuilder("ejemplo 2").withSala(sala2).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala2).build(entityManager);

        EventoDTO evento2 =
            new EventoBuilder("Evento 2", "Esdeveniment 2", cine, tipoEvento).withRssId(RSS_ID).build(entityManager);

        UsuarioDTO usuario2 =
            new UsuarioBuilder("User 2", "user1@test.com", "user2").withRole(Role.TAQUILLA).withSala(sala2)
                .build(entityManager);

        new SesionBuilder("Sesión 2", sala2, evento2, "13:00")
            .withCompraTaquillaTPV("Juan", BigDecimal.valueOf(14.0), null, 1, tarifa, localizacion, "1234-2-1", "00000123",
                usuario)
            .withCompraTaquillaTPV("Dani", BigDecimal.valueOf(24.0), null,2, tarifa, localizacion, "1234-2-2", "00000124",
                usuario2).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        Object[] totalTaquillaTPVUsuario1 = comprasDAO.getTotalTaquillaTpv(fechaInicio, fechaFin, usuario.getUsuario());
        Assert.assertEquals("12.00", totalTaquillaTPVUsuario1[0].toString());
        Assert.assertEquals("3", totalTaquillaTPVUsuario1[1].toString());

        Object[] totalTaquillaTPVUsuario2 =
            comprasDAO.getTotalTaquillaTpv(fechaInicio, fechaFin, usuario2.getUsuario());
        Assert.assertEquals("24.00", totalTaquillaTPVUsuario2[0].toString());
        Assert.assertEquals("2", totalTaquillaTPVUsuario2[1].toString());
    }

    @Test
    public void testInformePDFTaquillaDondeTotalTaquillaEfectivoSoloMuestranDatosDeLasSalasDelUsuario()
        throws ParseException {
        CompraDTO compra = sesion.getParCompras().get(0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fechaInicio = sdf.format(cal.getTime());
        String fechaFin = sdf.format(compra.getFecha());

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine).build(entityManager);

        new TpvBuilder("ejemplo 2").withSala(sala2).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala2).build(entityManager);

        EventoDTO evento2 =
            new EventoBuilder("Evento 2", "Esdeveniment 2", cine, tipoEvento).withRssId(RSS_ID).build(entityManager);

        UsuarioDTO usuario2 =
            new UsuarioBuilder("User 2", "user1@test.com", "user2").withRole(Role.TAQUILLA).withSala(sala2)
                .build(entityManager);

        new SesionBuilder("Sesión 2", sala2, evento2, "13:00")
            .withCompraTaquilla("Juan1", BigDecimal.valueOf(14.0), 1, tarifa, localizacion, "1234-2-1", usuario)
            .withCompraTaquillaTPV("Pedro1", BigDecimal.valueOf(14.0), null,1, tarifa, localizacion, "1234-2-2", "000001",
                usuario)
            .withCompraTaquilla("Juan2", BigDecimal.valueOf(14.0), 1, tarifa, localizacion, "1234-2-3", usuario2)
            .withCompraTaquillaTPV("Pedro2", BigDecimal.valueOf(14.0), null, 1, tarifa, localizacion, "1234-2-4", "000001",
                usuario2).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        Object[] totalTaquillaTPVUsuario1 =
            comprasDAO.getTotalTaquillaEfectivo(fechaInicio, fechaFin, usuario.getUsuario());
        Assert.assertEquals("48.00", totalTaquillaTPVUsuario1[0].toString());
        Assert.assertEquals("8", totalTaquillaTPVUsuario1[1].toString());

        Object[] totalTaquillaTPVUsuario2 =
            comprasDAO.getTotalTaquillaEfectivo(fechaInicio, fechaFin, usuario2.getUsuario());
        Assert.assertEquals("14.00", totalTaquillaTPVUsuario2[0].toString());
        Assert.assertEquals("1", totalTaquillaTPVUsuario2[1].toString());
    }

    @Test
    public void testInformePDFTaquillaDondeTotalOnlineSoloMuestranDatosDeLasSalasDelUsuario()
        throws IOException, ParseException {
        CompraDTO compra = sesion.getParCompras().get(0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fechaInicio = sdf.format(cal.getTime());
        String fechaFin = sdf.format(compra.getFecha());

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine).build(entityManager);

        new TpvBuilder("ejemplo 2").withSala(sala2).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala2).build(entityManager);

        EventoDTO evento2 =
            new EventoBuilder("Evento 2", "Esdeveniment 2", cine, tipoEvento).withRssId(RSS_ID).build(entityManager);

        new SesionBuilder("Sesión 2", sala2, evento2, "13:00")
            .withCompraTaquilla("Juan", BigDecimal.valueOf(14.0), 1, tarifa, localizacion, "1234-2-1", usuario)
            .withCompra("Pedro", BigDecimal.valueOf(14.0), 2, tarifa, localizacion, "1234-2-2").build(entityManager);

        UsuarioDTO usuario2 =
            new UsuarioBuilder("User 2", "user1@test.com", "user2").withSala(sala2).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        Object[] totalTaquillaTPVUsuario1 = comprasDAO.getTotalOnline(fechaInicio, fechaFin, usuario.getUsuario());
        Assert.assertEquals("12.00", totalTaquillaTPVUsuario1[0].toString());
        Assert.assertEquals("2", totalTaquillaTPVUsuario1[1].toString());

        Object[] totalTaquillaTPVUsuario2 = comprasDAO.getTotalOnline(fechaInicio, fechaFin, usuario2.getUsuario());
        Assert.assertEquals("14.00", totalTaquillaTPVUsuario2[0].toString());
        Assert.assertEquals("2", totalTaquillaTPVUsuario2[1].toString());
    }

    @Test
    public void testInformePDFEfectivoDondeComprasSoloMuestranDatosDeLasSalasDelUsuario()
        throws IOException, ParseException {
        CompraDTO compra = sesion.getParCompras().get(0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fechaInicio = sdf.format(cal.getTime());
        String fechaFin = sdf.format(compra.getFecha());

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine).build(entityManager);

        TpvsDTO tpv2 = new TpvBuilder("ejemplo 2").withSala(sala2).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala2).build(entityManager);

        EventoDTO evento2 =
            new EventoBuilder("Evento 2", "Esdeveniment 2", cine, tipoEvento).withTpv(tpv2).withRssId(RSS_ID).build(entityManager);

        UsuarioDTO usuario2 =
            new UsuarioBuilder("User 2", "user1@test.com", "user2").withRole(Role.TAQUILLA).withSala(sala2)
                .build(entityManager);

        new SesionBuilder("Sesión 2", sala2, evento2, "13:00")
            .withCompraTaquilla("Juan", BigDecimal.valueOf(14.0), 3, tarifa, localizacion, "1234-2-1", usuario)
            .withCompraTaquilla("Dani", BigDecimal.valueOf(24.0), 4, tarifa, localizacion, "1234-2-3", usuario2)
            .withCompra("Pedro", BigDecimal.valueOf(14.0), 2, tarifa, localizacion, "1234-2-2").build(entityManager);

        entityManager.flush();
        entityManager.clear();

        List<Object[]> comprasEfectivo =
            comprasDAO.getComprasEfectivo(fechaInicio, fechaFin, usuario.getUsuario(), String.valueOf(tpv.getId()));
        String totalButacas = comprasEfectivo.get(0)[3].toString();
        Assert.assertEquals("8", totalButacas);

        List<Object[]> comprasEfectivo2 =
            comprasDAO.getComprasEfectivo(fechaInicio, fechaFin, usuario2.getUsuario(), String.valueOf(tpv2.getId()));
        String totalButacasSesion2 = comprasEfectivo2.get(0)[3].toString();
        Assert.assertEquals("4", totalButacasSesion2);
    }

    @Test
    public void testInformePdfTpvSubtotalesDondeComprasSoloMuestranDatosDeLasSalasDelUsuario()
        throws IOException, ParseException {
        CompraDTO compra = sesion.getParCompras().get(0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fechaInicio = sdf.format(cal.getTime());
        String fechaFin = sdf.format(compra.getFecha());

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine).build(entityManager);

        TpvsDTO tpv2 = new TpvBuilder("ejemplo 2").withSala(sala2).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala2).build(entityManager);

        EventoDTO evento2 =
            new EventoBuilder("Evento 2", "Esdeveniment 2", cine, tipoEvento).withTpv(tpv2).withRssId(RSS_ID).build(entityManager);

        new SesionBuilder("Sesión 2", sala2, evento2, "13:00")
            .withCompraTaquilla("Juan", BigDecimal.valueOf(14.0), 3, tarifa, localizacion, "1234-2-1", usuario)
            .withCompra("Pedro", BigDecimal.valueOf(14.0), 2, tarifa, localizacion, "1234-2-2")
            .withCompraTaquillaTPV("Antonio G.", BigDecimal.valueOf(22.0), new Timestamp(cal.getTime().getTime()), 3, tarifa, localizacion, "1234-2-3",
                "0000123", usuario)
            .withCompraTaquillaTPV("Luís", BigDecimal.valueOf(12.0), compra.getFecha(), 3, tarifa, localizacion, "1234-2-4", "0000124",
                usuario).build(entityManager);

        UsuarioDTO usuario2 =
            new UsuarioBuilder("User 2", "user1@test.com", "user2").withSala(sala2).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        List<Object[]> comprasTpv = comprasDAO.getComprasTpv(fechaInicio, fechaFin, usuario.getUsuario(), String.valueOf(tpv.getId()));
        Assert.assertEquals(1, comprasTpv.size());
        List<Object[]> comprasTpv2 = comprasDAO.getComprasTpv(fechaInicio, fechaFin, usuario2.getUsuario(), String.valueOf(tpv2.getId()));
        Assert.assertEquals(2, comprasTpv2.size());
    }

    @Test
    public void testFiltraSalasParaInforme() throws IOException {
        CompraDTO compra = sesion.getParCompras().get(0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(compra.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        SalaDTO sala2 = new SalaBuilder("Sala 2", cine).build(entityManager);

        new TpvBuilder("ejemplo 2").withSala(sala2).build(entityManager);

        new PlantillaBuilder("Plantilla 1", sala2).build(entityManager);

        EventoDTO evento2 =
            new EventoBuilder("Evento 2", "Esdeveniment 2", cine, tipoEvento).withRssId(RSS_ID).build(entityManager);

        new SesionBuilder("Sesión 2", sala2, evento2, "13:00").build(entityManager);

        UsuarioDTO usuario2 =
            new UsuarioBuilder("User 2", "user1@test.com", "user2").withSala(sala2).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        List<SesionDTO> sesiones1 = sesionesDAO.getSesionesCinePorFechas(null, null, false, null, usuario.getUsuario());
        Assert.assertEquals(1, sesiones1.size());
        Assert.assertEquals("12:00", sdf.format(sesiones1.get(0).getFechaCelebracion()));

        List<SesionDTO> sesiones2 = sesionesDAO.getSesionesCinePorFechas(null, null, false, null, usuario2.getUsuario());
        Assert.assertEquals(1, sesiones2.size());
        Assert.assertEquals("13:00", sdf.format(sesiones2.get(0).getFechaCelebracion()));
    }
}
