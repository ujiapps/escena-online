package es.uji.apps.par.services.reports;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.par.builders.CineBuilder;
import es.uji.apps.par.builders.EventoBuilder;
import es.uji.apps.par.builders.LocalizacionBuilder;
import es.uji.apps.par.builders.PlantillaBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.SesionBuilder;
import es.uji.apps.par.builders.TarifaBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.builders.TpvBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.dao.ComprasDAO;
import es.uji.apps.par.db.CineDTO;
import es.uji.apps.par.db.CompraDTO;
import es.uji.apps.par.db.EventoDTO;
import es.uji.apps.par.db.LocalizacionDTO;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.db.SesionDTO;
import es.uji.apps.par.db.TarifaDTO;
import es.uji.apps.par.db.TipoEventoDTO;
import es.uji.apps.par.db.UsuarioDTO;
import es.uji.apps.par.report.EntradaReportOnlineInterface;
import es.uji.apps.par.report.EntradaReportTaquillaInterface;
import es.uji.apps.par.services.EntradasService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-db-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class EntradasReportsTest {

	@Autowired
	EntradasService entradasService;

	@Autowired
	ComprasDAO comprasDAO;

	@PersistenceContext
	protected EntityManager entityManager;

	CineDTO cine;
	SesionDTO sesion;
	SesionDTO sesionActoGraduacion;
	UsuarioDTO usuario;
	final String RSS_ID = "100";

	@Before
	public void setUp()
	{
		cine = new CineBuilder("Cine 1")
			.build(entityManager);

		TipoEventoDTO tipoEvento = new TipoEventoBuilder("tipo", "tipo", false, cine)
			.build(entityManager);

		TipoEventoDTO tipoEventoActoGraduacion = new TipoEventoBuilder("ActoGraduacion", "ActoGraduacion", false, cine)
			.build(entityManager);

		SalaDTO sala = new SalaBuilder("Sala 1", cine)
            .withClaseEntradaTaquilla("es.uji.apps.par.report.EntradaTaquillaReport")
            .withClaseEntradaOnline("es.uji.apps.par.report.EntradaReport")
			.build(entityManager);

		new TpvBuilder("ejemplo")
			.withSala(sala)
			.build(entityManager);

		new PlantillaBuilder("Plantilla 1", sala)
			.build(entityManager);

		EventoDTO evento = new EventoBuilder("Evento 1", "Esdeveniment 1", cine, tipoEvento)
			.withRssId(RSS_ID)
			.build(entityManager);

		EventoDTO eventoActoGraduacion = new EventoBuilder("Evento 2", "Esdeveniment 2", cine, tipoEventoActoGraduacion)
			.build(entityManager);

		TarifaDTO tarifa = new TarifaBuilder("Tarifa 1").build(cine, entityManager);

		LocalizacionDTO localizacion = new LocalizacionBuilder("Localizacion", "LOC", new BigDecimal(200))
			.withSala(sala)
			.build(entityManager);

		sesion = new SesionBuilder("Sesi�n 1", sala, evento)
			.withCompra("Juan", BigDecimal.valueOf(25.0), 4, tarifa, localizacion, "1234-1")
			.build(entityManager);

		sesionActoGraduacion = new SesionBuilder("Sesi�n 1", sala, eventoActoGraduacion)
			.withCompra("Paco", BigDecimal.valueOf(25.0), 4, tarifa, localizacion, "1234-2")
			.build(entityManager);

		usuario = new UsuarioBuilder("User 1", "user1@test.com", "user1")
			.withSala(sala)
			.build(entityManager);

		entityManager.flush();
		entityManager.clear();
	}

	@Test
	public void testNombreClaseEntradaTaquilla() throws SAXException, IOException {
		CompraDTO compra = comprasDAO.getCompraByUuid(sesion.getParCompras().get(0).getUuid());
		EntradaReportTaquillaInterface
            entrada = entradasService.generaEntradaTaquillaYRellena(compra, usuario.getUsuario(), "https");
		Assert.assertNotNull(entrada);
		Assert.assertEquals("es.uji.apps.par.report.EntradaTaquillaReport", entrada.getClass().getName());
	}

	@Test
	public void testNombreClaseEntradaOnline() {
		CompraDTO compra = comprasDAO.getCompraByUuid(sesion.getParCompras().get(0).getUuid());
		EntradaReportOnlineInterface entrada = entradasService.generaEntradaOnlineYRellena(compra, usuario.getUsuario(), "https", "urlPieEntrada");
		Assert.assertNotNull(entrada);
		Assert.assertEquals("es.uji.apps.par.report.EntradaReport", entrada.getClass().getName());
	}

	@Test
	public void testNombreClaseEntradaOnlineActoGraduacion() {
		CompraDTO compra = comprasDAO.getCompraByUuid(sesionActoGraduacion.getParCompras().get(0).getUuid());
		EntradaReportOnlineInterface entrada = entradasService.generaEntradaOnlineYRellena(compra, usuario.getUsuario(), "https", "urlPieEntrada");
		Assert.assertNotNull(entrada);
		Assert.assertEquals("es.uji.apps.par.report.EntradaActoGraduacionReport", entrada.getClass().getName());
	}
}
