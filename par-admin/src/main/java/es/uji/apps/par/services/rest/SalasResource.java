package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.exceptions.GeneralPARException;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.SalaHTML;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.EventosService;
import es.uji.apps.par.services.SalasService;
import es.uji.apps.par.services.UsersService;

@Path("sala")
public class SalasResource extends BaseResource {
    @InjectParam
    private SalasService salasService;

    @InjectParam
    private EventosService eventosService;

    @InjectParam
    private UsersService usersService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@QueryParam("filter") ExtGridFilterList filter) {
        String userUID = AuthChecker.getUserUID(currentRequest);
        List<Sala> salas = salasService.getSalas(userUID, filter);

        return Response.ok().entity(new RestResponse(true, salas, salas.size())).build();
    }

    @GET
    @Path("evento/{eventoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAvailableByEvent(@PathParam("eventoId") Long eventoId) {
        String userUID = AuthChecker.getUserUID(currentRequest);
        List<Sala> salas = salasService.getSalasDisponiblesParaEvento(userUID, eventoId);

        return Response.ok().entity(new RestResponse(true, salas, salas.size())).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addSala(Sala sala) {
        String userUID = AuthChecker.getUserUID(currentRequest);
        salasService.addSala(sala, userUID);
        return Response.created(URI.create("")).build();
    }

    @PUT
    @Path("{idSala}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response editSala(
        @PathParam("idSala") Long idSala,
        Sala sala
    ) {
        salasService.editSala(sala);
        return Response.ok().build();
    }

    @DELETE
    @Path("{idSala}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response removeSala(@PathParam("idSala") Long idSala) {
        salasService.deleteSala(idSala);
        return Response.ok().build();
    }

    @POST
    @Path("numerada")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addSalaNumerada(
        @FormParam("html") String html,
        @FormParam("nombre") String nombre,
        @FormParam("direccion") String direccion,
        @FormParam("ansibleFilesPath") String ansibleFilesPath
    ) throws GeneralPARException, IOException {
        AuthChecker.canWrite(currentRequest);
        Usuario usuario = usersService.getUserByServerName(currentRequest.getServerName());
        Cine cine = usersService.getUserCineByServerName(currentRequest.getServerName());

        SalaHTML sala = new SalaHTML(html, nombre, direccion, cine);
        return Response.ok(salasService.createFromHtml(sala, ansibleFilesPath, usuario)).build();
    }
}
