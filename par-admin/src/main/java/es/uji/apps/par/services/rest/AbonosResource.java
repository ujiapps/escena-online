package es.uji.apps.par.services.rest;

import com.google.common.base.Strings;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataParam;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.exceptions.TamanyoImagenException;
import es.uji.apps.par.model.Abono;
import es.uji.apps.par.services.AbonadosService;
import es.uji.apps.par.services.AbonosService;
import es.uji.apps.par.services.UsersService;

import static es.uji.apps.par.filters.UploadFilter.MAX_BINARY_SIZE;

@Path("abono")
public class AbonosResource extends BaseResource {
    @InjectParam
    private AbonosService abonosService;

    @InjectParam
    private AbonadosService abonadosService;

    @InjectParam
    private UsersService usersService;

    @Context
    HttpServletRequest currentRequest;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAbonos(
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        return Response.ok().entity(new RestResponse(true, abonosService.getAll(sort, start, limit, userUID),
            (int) abonosService.getTotal(userUID))).build();
    }

    @GET
    @Path("{id}/abonados")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAbonados(
        @PathParam("id") Long tarifaId,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        return Response.ok().entity(
            new RestResponse(true, abonadosService.getAbonadosByAbonoId(tarifaId, sort, start, limit),
                abonadosService.getTotalAbonadosByAbonoId(tarifaId))).build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"text/html"})
    public String addAbono(
        @FormDataParam("nombre") String nombre,
        @FormDataParam("maxEventos") Long maxEventos,
        @FormDataParam("descripcionEs") String descripcionEs,
        @FormDataParam("descripcionVa") String descripcionVa,
        @FormDataParam("dataBinary") InputStream dataBinary,
        @FormDataParam("dataBinary") FormDataContentDisposition dataBinaryDetail,
        @FormDataParam("dataBinary") FormDataBodyPart imagenBodyPart,
        @FormDataParam("precio") String precio,
        @FormDataParam("precioAnticipado") String precioAnticipado,
        @FormDataParam("tpv") Integer tpvId,
        @FormDataParam("localizacion") Integer localizacionId,
        @FormDataParam("canalInternet") String canalInternet,
        @FormDataParam("fechaInicioVentaOnline") String fechaInicioVentaOnline,
        @FormDataParam("fechaFinVentaOnline") String fechaFinVentaOnline,
        @FormDataParam("fechaFinVentaAnticipada") String fechaFinVentaAnticipada,
        @FormDataParam("horaInicioVentaOnline") String horaInicioVentaOnline,
        @FormDataParam("horaFinVentaOnline") String horaFinVentaOnline,
        @FormDataParam("horaFinVentaAnticipada") String horaFinVentaAnticipada
    ) throws IOException {
        AuthChecker.canWrite(currentRequest);

        Abono abono =
            createAbono(nombre, maxEventos, descripcionEs, descripcionVa, dataBinary, dataBinaryDetail, imagenBodyPart,
                precio, precioAnticipado, tpvId, localizacionId, canalInternet, fechaInicioVentaOnline,
                fechaFinVentaOnline, fechaFinVentaAnticipada, horaInicioVentaOnline, horaFinVentaOnline,
                horaFinVentaAnticipada);

        String userUID = AuthChecker.getUserUID(currentRequest);
        abonosService.add(abono, userUID);

        return "{success: 'true'}";
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"text/html"})
    public String updateAbono(
        @FormDataParam("nombre") String nombre,
        @FormDataParam("maxEventos") Long maxEventos,
        @FormDataParam("descripcionEs") String descripcionEs,
        @FormDataParam("descripcionVa") String descripcionVa,
        @FormDataParam("dataBinary") InputStream dataBinary,
        @FormDataParam("dataBinary") FormDataContentDisposition dataBinaryDetail,
        @FormDataParam("dataBinary") FormDataBodyPart imagenBodyPart,
        @FormDataParam("precio") String precio,
        @FormDataParam("precioAnticipado") String precioAnticipado,
        @FormDataParam("tpv") Integer tpvId,
        @FormDataParam("localizacion") Integer localizacionId,
        @FormDataParam("canalInternet") String canalInternet,
        @FormDataParam("fechaInicioVentaOnline") String fechaInicioVentaOnline,
        @FormDataParam("fechaFinVentaOnline") String fechaFinVentaOnline,
        @FormDataParam("fechaFinVentaAnticipada") String fechaFinVentaAnticipada,
        @FormDataParam("horaInicioVentaOnline") String horaInicioVentaOnline,
        @FormDataParam("horaFinVentaOnline") String horaFinVentaOnline,
        @FormDataParam("horaFinVentaAnticipada") String horaFinVentaAnticipada,
        @PathParam("id") long id
    ) throws IOException {
        AuthChecker.canWrite(currentRequest);

        Abono abono =
            createAbono(nombre, maxEventos, descripcionEs, descripcionVa, dataBinary, dataBinaryDetail, imagenBodyPart,
                precio, precioAnticipado, tpvId, localizacionId, canalInternet, fechaInicioVentaOnline,
                fechaFinVentaOnline, fechaFinVentaAnticipada, horaInicioVentaOnline, horaFinVentaOnline,
                horaFinVentaAnticipada);
        abono.setId(id);

        String userUID = AuthChecker.getUserUID(currentRequest);
        abonosService.update(abono, userUID);

        return "{success: 'true'}";
    }

    private Abono createAbono(
        String nombre,
        Long maxEventos,
        String descripcionEs,
        String descripcionVa,
        InputStream dataBinary,
        FormDataContentDisposition dataBinaryDetail,
        FormDataBodyPart imagenBodyPart,
        String precio,
        String precioAnticipado,
        Integer tpvId,
        Integer localizacionId,
        String canalInternet,
        String fechaInicioVentaOnline,
        String fechaFinVentaOnline,
        String fechaFinVentaAnticipada,
        String horaInicioVentaOnline,
        String horaFinVentaOnline,
        String horaFinVentaAnticipada
    ) throws IOException {
        String nombreArchivo = (dataBinaryDetail != null) ? dataBinaryDetail.getFileName() : "";
        String mediaType = (imagenBodyPart != null) ? imagenBodyPart.getMediaType().toString() : "";
        byte[] byteDataBinary = dataBinary != null ? IOUtils.toByteArray(dataBinary) : new byte[0];

        if (byteDataBinary.length > MAX_BINARY_SIZE) {
            throw new TamanyoImagenException();
        }

        Abono abono =
            new Abono(nombre, maxEventos, descripcionEs, descripcionVa, byteDataBinary, nombreArchivo, mediaType,
                !Strings.isNullOrEmpty(precio) ? new BigDecimal(precio.replace(",", ".")) : null,
                !Strings.isNullOrEmpty(precioAnticipado) ? new BigDecimal(precioAnticipado.replace(",", ".")) : null,
                tpvId, localizacionId, canalInternet, fechaInicioVentaOnline, fechaFinVentaOnline,
                fechaFinVentaAnticipada, horaInicioVentaOnline, horaFinVentaOnline, horaFinVentaAnticipada);

        return abono;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeAbono(@PathParam("id") Long id) {
        AuthChecker.canWrite(currentRequest);

        abonosService.remove(id);

        return Response.ok().entity(new RestResponse(true)).build();
    }

    @DELETE
    @Path("{idAbono}/abonados/{idAbonado}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeAbonado(
        @PathParam("idAbono") Long idAbono,
        @PathParam("idAbonado") Long idAbonado
    ) {
        AuthChecker.canWrite(currentRequest);
        String userUID = AuthChecker.getUserUID(currentRequest);

        abonosService.removeAbonado(idAbonado, userUID);
        return Response.ok().entity(new RestResponse(true)).build();
    }
}