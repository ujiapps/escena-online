package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.fopreports.serialization.ReportSerializationException;
import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.exceptions.SinIvaException;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.services.ReportService;
import es.uji.apps.par.services.SesionesService;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.LocaleUtils;

@Path("report")
public class ReportResource extends BaseResource {
    private static final Logger log = LoggerFactory.getLogger(ReportResource.class);

    @InjectParam
    private ReportService reportService;

    @InjectParam
    private SesionesService sesionesService;

    @Context
    HttpServletRequest currentRequest;

    @POST
    @Path("taquilla/{fechaInicio}/{fechaFin}")
    @Produces("application/vnd.ms-excel")
    public Response generateExcelTaquilla(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("tpv") String tpv
    ) throws IOException, ParseException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = reportService
            .getExcelTaquilla(fechaInicio, fechaFin, LocaleUtils.getLocale(configurationSelector, currentRequest),
                userUID, tpv);
        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeTaquilla " + fechaInicio + "-" + fechaFin + ".xls\"").build();
    }

    @POST
    @Path("ventas/{fechaInicio}/{fechaFin}")
    @Produces("application/vnd.ms-excel")
    public Response generateExcelVentas(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("tpv") String tpv
    ) throws IOException, ParseException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = reportService
            .getExcelVentas(fechaInicio, fechaFin, LocaleUtils.getLocale(configurationSelector, currentRequest),
                userUID, tpv);
        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeVentas " + fechaInicio + "-" + fechaFin + ".xls\"").build();
    }

    @POST
    @Path("eventos/{fechaInicio}/{fechaFin}")
    @Produces("application/vnd.ms-excel")
    public Response generateExcelEventos(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("tpv") String tpv
    ) throws IOException, ParseException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = reportService
            .getExcelEventos(fechaInicio, fechaFin, LocaleUtils.getLocale(configurationSelector, currentRequest),
                userUID, tpv);
        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeEventos " + fechaInicio + "-" + fechaFin + ".xls\"").build();
    }

    @POST
    @Path("taquilla/{fechaInicio}/{fechaFin}/pdf")
    @Produces("application/pdf")
    public Response generatePdfTaquillaAgrupado(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException, IOException {

        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        reportService.getTaquillaCompras(fechaInicio, fechaFin, ostream,
            LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
            configurationSelector.getLogoReport(), configurationSelector.showIVA(),
            configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), sort, false,
            tpv, false);

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeTaquilla " + fechaInicio + "-" + fechaFin + ".pdf\"")
            .type("application/pdf").build();
    }

    @POST
    @Path("taquilla/{fechaInicio}/{fechaFin}/pdf/compras")
    @Produces("application/pdf")
    public Response generatePdfTaquillaCompra(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException, IOException {

        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        reportService.getTaquillaCompras(fechaInicio, fechaFin, ostream,
            LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
            configurationSelector.getLogoReport(), configurationSelector.showIVA(),
            configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), sort, true, tpv,
            false);

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeTaquillaCompras " + fechaInicio + "-" + fechaFin + ".pdf\"")
            .type("application/pdf").build();
    }

    @POST
    @Path("taquilla/{fechaInicio}/{fechaFin}/excel/compras")
    @Produces("application/vnd.ms-excel")
    public Response generateExcelTaquillaCompra(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException, IOException {

        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        reportService.getTaquillaCompras(fechaInicio, fechaFin, ostream,
            LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
            configurationSelector.getLogoReport(), configurationSelector.showIVA(),
            configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), sort, true, tpv,
            true);

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeTaquillaCompras " + fechaInicio + "-" + fechaFin + ".xls\"").build();
    }

    @POST
    @Path("mitaquilla/{fechaInicio}/{fechaFin}/pdf")
    @Produces("application/pdf")
    public Response generatePdfMiTaquilla(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        reportService.getPdfMiTaquilla(fechaInicio, fechaFin, ostream,
            LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
            configurationSelector.getLogoReport(), configurationSelector.showIVA(),
            configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), sort, false,
            tpv);

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeMiTaquilla " + fechaInicio + "-" + fechaFin + ".pdf\"")
            .type("application/pdf").build();
    }

    @POST
    @Path("mitaquillasesion/{fechaInicio}/{fechaFin}/pdf")
    @Produces("application/pdf")
    public Response generatePdfMiTaquillaSesion(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        reportService.getPdfMiTaquilla(fechaInicio, fechaFin, ostream,
            LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
            configurationSelector.getLogoReport(), configurationSelector.showIVA(),
            configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), sort, true,
            tpv);

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeMiTaquillaSesion " + fechaInicio + "-" + fechaFin + ".pdf\"")
            .type("application/pdf").build();
    }

    @POST
    @Path("sesion/{idSesion}/pdf/online")
    @Produces("application/pdf")
    public Response generatePdfSesionOnline(
        @PathParam("idSesion") long idSesion,
        @QueryParam("sort") String sort
    ) throws ReportSerializationException, SinIvaException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        Sesion sesion = sesionesService.getSesion(idSesion, userUID);
        String fileName =
            "informeSesionOnline-" + DateUtils.dateToSpanishStringWithHour(sesion.getFechaCelebracion()) + ".pdf";

        reportService
            .getPdfSesionOnline(idSesion, ostream, LocaleUtils.getLocale(configurationSelector, currentRequest),
                userUID, configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), sort);

        return Response.ok(ostream.toByteArray())
            .header("Content-Disposition", "attachment; filename =\"" + fileName + "\"").type("application/pdf")
            .build();
    }

    @POST
    @Path("sesion/{idSesion}/pdf")
    @Produces("application/pdf")
    public Response generatePdfSesion(
        @PathParam("idSesion") long idSesion,
        @QueryParam("sort") String sort
    ) throws ReportSerializationException, SinIvaException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        Sesion sesion = sesionesService.getSesion(idSesion, userUID);
        String fileName =
            "informeSesion " + DateUtils.dateToSpanishStringWithHour(sesion.getFechaCelebracion()) + ".pdf";

        reportService
            .getPdfSesion(idSesion, ostream, LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
                configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), true, sort);

        return Response.ok(ostream.toByteArray())
            .header("Content-Disposition", "attachment; filename =\"" + fileName + "\"").type("application/pdf")
            .build();
    }

    @POST
    @Path("evento/{idEvento}/pdf")
    @Produces("application/pdf")
    public Response generatePdfEvento(
        @PathParam("idEvento") long idEvento,
        @QueryParam("sort") String sort
    ) throws ReportSerializationException, SinIvaException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        List<Sesion> sesiones = sesionesService.getSesiones(idEvento, userUID);
        Collections.sort(sesiones, Comparator.comparing(Sesion::getFechaCelebracion));
        String fileName = "informeEvento-" + idEvento + ".pdf";

        reportService
            .getPdfSesiones(sesiones, ostream, LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
                configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), true, sort, false, null, null);

        return Response.ok(ostream.toByteArray())
            .header("Content-Disposition", "attachment; filename =\"" + fileName + "\"").type("application/pdf")
            .build();
    }

    @POST
    @Path("sesion/{idSesion}/pdf/noanuladas")
    @Produces("application/pdf")
    public Response generatePdfSesionAnuladas(
        @PathParam("idSesion") long idSesion,
        @QueryParam("sort") String sort
    ) throws ReportSerializationException, SinIvaException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        Sesion sesion = sesionesService.getSesion(idSesion, userUID);
        String fileName =
            "informeSesion " + DateUtils.dateToSpanishStringWithHour(sesion.getFechaCelebracion()) + ".pdf";

        reportService
            .getPdfSesion(idSesion, ostream, LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
                configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), false,
                sort);

        return Response.ok(ostream.toByteArray())
            .header("Content-Disposition", "attachment; filename =\"" + fileName + "\"").type("application/pdf")
            .build();
    }

    @POST
    @Path("evento/{idEvento}/pdf/noanuladas")
    @Produces("application/pdf")
    public Response generatePdfEventoAnuladas(
        @PathParam("idEvento") long idEvento,
        @QueryParam("sort") String sort
    ) throws ReportSerializationException, SinIvaException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        List<Sesion> sesiones = sesionesService.getSesiones(idEvento, userUID);
        Collections.sort(sesiones, Comparator.comparing(Sesion::getFechaCelebracion));
        String fileName = "informeEvento-" + idEvento + ".pdf";

        reportService
            .getPdfSesiones(sesiones, ostream, LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
                configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), false,
                sort, false, null, null);

        return Response.ok(ostream.toByteArray())
            .header("Content-Disposition", "attachment; filename =\"" + fileName + "\"").type("application/pdf")
            .build();
    }

    @POST
    @Path("evento/{idEvento}/sesion/{idSesion}/pdf/{tipo}")
    @Produces("application/pdf")
    public Response generatePdf(
        @PathParam("idEvento") long idEvento,
        @PathParam("idSesion") long idSesion,
        @PathParam("tipo") String tipo
    ) throws ReportSerializationException, SinIvaException {
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        String userUID = AuthChecker.getUserUID(currentRequest);

        Sesion sesion = sesionesService.getSesion(idSesion, userUID);
        String fileName = tipo + "_" + DateUtils.dateToSpanishStringWithHour(sesion.getFechaCelebracion()) + ".pdf";

        reportService
            .getPdf(idSesion, ostream, tipo, LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
                configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio());

        return Response.ok(ostream.toByteArray())
            .header("Content-Disposition", "attachment; filename =\"" + fileName + "\"").type("application/pdf")
            .build();
    }

    @POST
    @Path("{fechaInicio}/{fechaFin}/{tipo}")
    @Produces("application/pdf")
    public Response generatePdfPorFechas(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @PathParam("tipo") String tipo
    ) throws ReportSerializationException, ParseException, SinIvaException {
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        String fileName = tipo + "_" + fechaInicio + "-" + fechaFin + ".pdf";
        String userUID = AuthChecker.getUserUID(currentRequest);
        reportService.getPdfPorFechas(fechaInicio, fechaFin, tipo, ostream,
            LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
            configurationSelector.getLogoReport(), configurationSelector.showIVA(),
            configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio());

        return Response.ok(ostream.toByteArray())
            .header("Content-Disposition", "attachment; filename =\"" + fileName + "\"").type("application/pdf")
            .build();
    }

    @POST
    @Path("taquilla/{fechaInicio}/{fechaFin}/efectivo/pdf")
    @Produces("application/pdf")
    public Response generatePdfEfectivo(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException {
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        try {
            String userUID = AuthChecker.getUserUID(currentRequest);
            reportService.getPdfEfectivo(fechaInicio, fechaFin, ostream,
                LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
                configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), sort, tpv);
        } catch (SinIvaException e) {
            log.error("Error", e);
            String errorMsj = String.format("ERROR: Cal introduir l'IVA de l'event \"%s\"", e.getEvento());
            return Response.serverError().type(MediaType.TEXT_PLAIN).entity(errorMsj).build();
        }

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeTaquilla " + fechaInicio + "-" + fechaFin + ".pdf\"")
            .type("application/pdf").build();
    }

    @POST
    @Path("taquilla/{fechaInicio}/{fechaFin}/tpv/pdf")
    @Produces("application/pdf")
    public Response generatePdfTpv(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException {
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        try {
            String userUID = AuthChecker.getUserUID(currentRequest);
            reportService.getPdfTpvSubtotales(fechaInicio, fechaFin, ostream,
                LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
                configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), sort, tpv);
        } catch (SinIvaException e) {
            log.error("Error", e);

            String errorMsj = String.format("ERROR: Cal introduir l'IVA de l'event \"%s\"", e.getEvento());
            return Response.serverError().type(MediaType.TEXT_PLAIN).entity(errorMsj).build();
        }

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeTPVTaquilla " + fechaInicio + "-" + fechaFin + ".pdf\"")
            .type("application/pdf").build();
    }

    @POST
    @Path("taquilla/{fechaInicio}/{fechaFin}/tpv/online/pdf")
    @Produces("application/pdf")
    public Response generatePdfTpvOnline(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException {
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        try {
            String userUID = AuthChecker.getUserUID(currentRequest);
            reportService.getPdfTpvOnlineSubtotales(fechaInicio, fechaFin, ostream,
                LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
                configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), sort, tpv);
        } catch (SinIvaException e) {
            log.error("Error", e);

            String errorMsj = String.format("ERROR: Cal introduir l'IVA de l'event \"%s\"", e.getEvento());
            return Response.serverError().type(MediaType.TEXT_PLAIN).entity(errorMsj).build();
        }

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeTPVOnline " + fechaInicio + "-" + fechaFin + ".pdf\"")
            .type("application/pdf").build();
    }

    @POST
    @Path("taquilla/{fechaInicio}/{fechaFin}/eventos/pdf")
    @Produces("application/pdf")
    public Response generatePdfEventos(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException {
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        try {
            String userUID = AuthChecker.getUserUID(currentRequest);
            reportService.getPdfEventos(fechaInicio, fechaFin, ostream,
                LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
                configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), sort, tpv);
        } catch (SinIvaException e) {
            log.error("Error", e);

            String errorMsj = String.format("ERROR: Cal introduir l'IVA de l'event \"%s\"", e.getEvento());
            return Response.serverError().type(MediaType.TEXT_PLAIN).entity(errorMsj).build();
        }

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeEventos " + fechaInicio + "-" + fechaFin + ".pdf\"").type("application/pdf")
            .build();
    }

    @POST
    @Path("incidencias/{fechaInicio}/{fechaFin}/pdf")
    @Produces("application/pdf")
    public Response generatePdfIncidencias(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException, IOException {

        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        reportService.generaYRellenaInformePDFIncidencias(fechaInicio, fechaFin,
            LocaleUtils.getLocale(configurationSelector, currentRequest), configurationSelector.getLogoReport(),
            configurationSelector.getNombreMunicipio(), userUID, ostream);

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeIncidencias " + fechaInicio + "-" + fechaFin + ".pdf\"")
            .type("application/pdf").build();
    }

    @POST
    @Path("exhibicion/{fechaInicio}/{fechaFin}/pdf")
    @Produces("application/pdf")
    public Response generatePdfExhibicion(
        @PathParam("fechaInicio") String fechaInicio,
        @PathParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("tpv") String tpv
    ) throws ReportSerializationException, ParseException, IOException {

        String userUID = AuthChecker.getUserUID(currentRequest);
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        String fechaInicioString = DateUtils.dateToSpanishString(DateUtils.databaseStringToDate(fechaInicio));
        String fechaFinString = DateUtils.dateToSpanishString(DateUtils.databaseStringToDate(fechaFin));
        List<Sesion> sesiones = sesionesService.getSesionesCinePorFechas(fechaInicioString, fechaFinString, true,null, userUID);
        Collections.sort(sesiones, Comparator.comparing(Sesion::getFechaCelebracion));

        reportService
            .getPdfSesiones(sesiones, ostream, LocaleUtils.getLocale(configurationSelector, currentRequest), userUID,
                configurationSelector.getLogoReport(), configurationSelector.showIVA(),
                configurationSelector.showTotalesSinComision(), configurationSelector.getNombreMunicipio(), false,
                sort, true, fechaInicioString, fechaFinString);

        return Response.ok(ostream.toByteArray()).header("Content-Disposition",
            "attachment; filename =\"informeExhibicion " + fechaInicio + "-" + fechaFin + ".pdf\"")
            .type("application/pdf").build();
    }

    @GET
    @Path("sesiones")
    @Produces(MediaType.APPLICATION_JSON)
    public Response filtraSesiones(
        @QueryParam("fechaInicio") String fechaInicio,
        @QueryParam("fechaFin") String fechaFin,
        @QueryParam("sort") @DefaultValue("[{\"property\":\"nombre\",\"direction\":\"ASC\"}]") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);
        List<Sesion> sesiones = sesionesService.getSesionesCinePorFechas(fechaInicio, fechaFin, false, sort, userUID);
        return Response.ok().entity(new RestResponse(true, sesiones, sesiones.size())).build();
    }
}
