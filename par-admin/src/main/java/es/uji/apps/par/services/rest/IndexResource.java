package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.auth.Authenticator;
import es.uji.apps.par.auth.Role;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.services.InformesService;
import es.uji.apps.par.services.UsersService;
import es.uji.apps.par.utils.LocaleUtils;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;

@Path("index")
public class IndexResource extends BaseResource {
    @InjectParam
    private UsersService usersService;

    @InjectParam
    private InformesService informesService;

    @Context
    HttpServletRequest currentRequest;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template index(@QueryParam("lang") String lang) throws Exception {
        if (lang != null) {
            LocaleUtils.setLocale(lang, currentRequest);
        }

        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        Template template = new HTMLTemplate(Constantes.PLANTILLAS_DIR + "admin", locale, APP);

        String userUID = AuthChecker.getUserUID(currentRequest);
        template.put("user", usersService.getUserById(userUID).getNombre());
        template.put("urlPublic", configurationSelector.getUrlPublic());
        template.put("payModes", configurationSelector.getPayModes(locale));
        template.put("lang", locale.getLanguage());
        template.put("langsAllowed", configurationSelector.getLangsAllowed());

        Boolean readonly = (Boolean) currentRequest.getSession().getAttribute(Authenticator.READONLY_ATTRIBUTE);
        Boolean taquilla = (Boolean) currentRequest.getSession().getAttribute(Authenticator.TAQUILLA_ATTRIBUTE);
        Role role = Role.ADMIN;
        if (readonly != null && readonly) {
            role = Role.READONLY;
        } else if (taquilla != null && taquilla) {
            role = Role.TAQUILLA;
        }

        boolean hasSalasNumeradas = usersService.hasSalasNumeradas(userUID);

        template.put("readOnlyUser", role == Role.READONLY);
        template.put("taquillaUser", role == Role.TAQUILLA);
        template.put("role", role);
        template.put("menuHtml", getMenu(role, hasSalasNumeradas));
        template.put("controllers", getControllers(role, hasSalasNumeradas));
        template.put("screens", getScreens(role, hasSalasNumeradas));
        template.put("views", getViews(role, hasSalasNumeradas));
        template.put("multipleTpv", configurationSelector.isMenuTpvEnabled());
        template.put("icaa", configurationSelector.isMenuICAAEnabled());
        template.put("allowMultisesion", configurationSelector.isMenuICAAEnabled());
        template.put("clientes", configurationSelector.isMenuClientesEnabled());
        template.put("salas", configurationSelector.isMenuSalasEnabled());
        template.put("integraciones", configurationSelector.isMenuIntegracionesEnabled());
        template.put("ventaAnticipada", configurationSelector.isMenuVentaAnticipadaEnabled());
        template.put("showComoNosConociste", configurationSelector.isShowComoNosConocisteEnabled());
        template.put("perfil", isAdminByToken(currentRequest) || configurationSelector.isMenuPerfilEnabled());
        template.put("help", configurationSelector.hasHelp());
        template.put("compraTaquillaDisabled", configurationSelector.isCompraTaquillaDisabled());
        template.put("informesGeneralSesion", informesService
            .getTiposInformeGeneralesSesion(configurationSelector.getInformesGenerales(),
                LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage()).size() > 0);

        return template;
    }

    @GET
    @Path("public/properties")
    @Produces(MediaType.APPLICATION_JSON)
    public Response index() throws Exception {
        List<String> properties = new ArrayList<>();
        properties.add(configurationSelector.getUrlPublic());
        properties.add(configurationSelector.getUrlPublicLimpio());
        properties.add(configurationSelector.getHtmlTitle());
        properties.add(configurationSelector.getUrlCondicionesPrivacidad());
        properties.add(configurationSelector.getUrlCondicionesCancelacion());
        properties.add(configurationSelector.getUrlComoLlegar());
        properties.add(configurationSelector.getUrlPieEntrada());
        properties.add(configurationSelector.getMailFrom());
        properties.add(configurationSelector.getLogoReport());

        return Response.ok().entity(new RestResponse(true, properties, properties.size())).build();
    }

    private String getMenu(
        Role role,
        boolean hasSalasNumeradas
    ) {
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);

        String menuHtml = "<div class=\"nav\"><div class=\"paranimf_logo\"></div><ul>";
        if (role != Role.TAQUILLA) {
            menuHtml += "<li id=\"menuTiposEventos\">"
                + "            <a href=\"javascript:muestraMenu('TiposEventos')\"><img src=\"../resources/images/menu/tiposeventos.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.tipoEvento") + "</span></a>" + "        </li>"
                + "        <li id=\"menuEventos\">"
                + "            <a href=\"javascript:muestraMenu('Eventos')\"><img src=\"../resources/images/menu/eventos.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.eventoSesiones") + "</span></a>" + "        </li>"
                + "        <li id=\"menuPlantillasPrecios\">"
                + "            <a href=\"javascript:muestraMenu('PlantillasPrecios')\"><img src=\"."
                + "./resources/images/menu/plantillasprecio.png\" width=\"24\"/><span>" + ResourceProperties
                .getProperty(locale, "menu.plantillaPrecios") + "</span></a></li>";


            if (hasSalasNumeradas && role == Role.ADMIN) {
                menuHtml += "<li id=\"menuPlantillasReservas\">"
                    + "            <a href=\"javascript:muestraMenu('PlantillasReservas')\"><img src=\"../resources/images/menu/plantillasprecio.png\" width=\"24\"/><span>"
                    + ResourceProperties.getProperty(locale, "menu.plantillasReservas") + "</span></a>"
                    + "        </li>    ";
            }

            menuHtml += "<li id=\"menuTarifas\">"
                + "            <a href=\"javascript:muestraMenu('Tarifas')\"><img src=\"../resources/images/menu/tarifas.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.tarifas") + "</span></a>" + "        </li>";

            menuHtml += "<li id=\"menuSalasNumeradas\">"
                + "            <a href=\"javascript:muestraMenu('SalasNumeradas')\"><img src=\"../resources/images/menu/salas.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.salaNumerada") + "</span></a>" + "        </li>";
            if (configurationSelector.isMenuSalasEnabled()) {
                menuHtml += "<li id=\"menuSalas\">"
                    + "            <a href=\"javascript:muestraMenu('Salas')\"><img src=\"../resources/images/menu/salas.png\" width=\"24\"/><span>"
                    + ResourceProperties.getProperty(locale, "menu.sala") + "</span></a>" + "        </li>";
            }
        }

        if (role != Role.READONLY) {
            menuHtml += "        <li id=\"menuTaquilla\">"
                + "            <a href=\"javascript:muestraMenu('Taquilla')\"><img src=\"../resources/images/menu/taquilla.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.taquilla") + "</span></a>" + "        </li>";
        }

        menuHtml += "        <li id=\"menuComprasReservas\">"
            + "            <a href=\"javascript:muestraMenu('ComprasReservas')\"><img src=\"../resources/images/menu/compras.png\" width=\"24\"/><span>"
            + ResourceProperties.getProperty(locale, "menu.comprasReservas") + "</span></a>" + "        </li>";

        if (configurationSelector.isMenuAbonosEnabled()) {
            menuHtml += "        <li id=\"menuAbonos\">"
                + "            <a href=\"javascript:muestraMenu('Abonos')\"><img src=\"../resources/images/menu/abonos.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.abonos") + "</span></a>" + "        </li>";
        }

        menuHtml += "        <li id=\"menuInformes\">"
            + "            <a href=\"javascript:muestraMenu('Informes')\"><img src=\"../resources/images/menu/informes.png\" width=\"24\"/><span>"
            + ResourceProperties.getProperty(locale, "menu.informes") + "</span></a>" + "        </li>    ";

        menuHtml += "        <li id=\"menuInformes\">"
            + "            <a href=\"javascript:muestraMenu('InformesSesion')\"><img src=\"../resources/images/menu/informes.png\" width=\"24\"/><span>"
            + ResourceProperties.getProperty(locale, "menu.informesSesion") + "</span></a>" + "        </li>    ";

        if (configurationSelector.isMenuClientesEnabled() && role != Role.TAQUILLA) {
            menuHtml += "        <li id=\"menuClientes\">"
                + "            <a href=\"javascript:muestraMenu('Clientes')\"><img src=\"../resources/images/menu/clientes.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.clientes") + "</span></a>" + "        </li>    ";
        }

        if (configurationSelector.isMenuICAAEnabled() && role != Role.TAQUILLA) {
            menuHtml += "        <li id=\"menuGenerarFicheros\">"
                + "            <a href=\"javascript:muestraMenu('GenerarFicheros')\"><img src=\"../resources/images/menu/icaa.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.icaa") + "</span></a>" + "        </li>";
        }

        if (isAdminByToken(currentRequest) || (configurationSelector.isMenuPerfilEnabled() && role == Role.ADMIN)) {
            menuHtml += "        <li id=\"menuDatos\">"
                + "            <a href=\"javascript:muestraMenu('DatosRegistro')\"><img src=\"../resources/images/menu/datosregistro.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.datosRegistro") + "</span></a>" + "        </li>";
        }

        if (configurationSelector.isMenuIntegracionesEnabled() && role == Role.ADMIN) {
            menuHtml += "      <li id=\"menuIntegraciones\">"
                + "            <a href=\"javascript:muestraMenu('Integraciones')\"><img src=\"../resources/images/menu/integraciones.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.integraciones") + "</span></a>" + "        </li>";
        }

        if (configurationSelector.isMenuTpvEnabled() && role == Role.ADMIN) {
            menuHtml += "        <li id=\"menuTpvs\">"
                + "            <a href=\"javascript:muestraMenu('Tpvs')\"><img src=\"../resources/images/menu/tpvs.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.tpvs") + "</span></a>" + "        </li>    ";
        }

        if (role == Role.ADMIN) {
            menuHtml += "        <li id=\"menuSoporte\">"
                + "            <a href=\"javascript:muestraMenu('Soporte')\"><img src=\"../resources/images/menu/soporte.png\" width=\"24\"/><span>"
                + ResourceProperties.getProperty(locale, "menu.soporte") + "</span></a>" + "        </li>    ";
        }

        menuHtml += "    </ul></div>";

        return menuHtml;
    }

    private String getControllers(
        Role role,
        boolean hasSalasNumeradas
    ) {
        String controllers = "['Dashboard', 'ComprasReservas', 'Informes', 'InformesSesion'";

        if (role != Role.TAQUILLA) {
            controllers += ", 'Usuarios', 'TiposEventos', 'Eventos', 'PlantillasPrecios', 'Tarifas'";

            controllers += ", 'SalasNumeradas'";
            if (configurationSelector.isMenuSalasEnabled()) {
                controllers += ", 'Salas'";
            }
        }

        if (configurationSelector.isMenuClientesEnabled() && role != Role.TAQUILLA) {
            controllers += ", 'Clientes'";
        }

        if (role != Role.READONLY) {
            controllers += ", 'Taquilla'";
        }

        if (configurationSelector.isMenuAbonosEnabled()) {
            controllers += ", 'Abonos'";
        }

        if (configurationSelector.isMenuICAAEnabled() && role != Role.TAQUILLA) {
            controllers += ", 'GenerarFicheros'";
        }

        if (isAdminByToken(currentRequest) || (configurationSelector.isMenuPerfilEnabled() && role == Role.ADMIN)) {
            controllers += ", 'DatosRegistro'";
        }

        if (configurationSelector.isMenuIntegracionesEnabled() && role == Role.ADMIN) {
            controllers += ", 'Integraciones'";
        }

        if (configurationSelector.isMenuTpvEnabled() && role == Role.ADMIN) {
            controllers += ", 'Tpvs'";
        }

        if (hasSalasNumeradas && role == Role.ADMIN) {
            controllers += ", 'PlantillasReservas'";
        }

        if (role == Role.ADMIN) {
            controllers += ", 'Soporte'";
        }

        controllers += "]";

        return controllers;
    }

    private String getScreens(
        Role role,
        boolean hasSalasNumeradas
    ) {
        String screens = "[{'Dashboard': 0, 'ComprasReservas': 1, 'Informes': 2, 'InformesSesion': 3";

        int i = 4;
        if (role != Role.TAQUILLA) {
            screens += ", 'Usuarios': 4, 'TiposEventos': 5, 'Eventos': 6, 'PlantillasPrecios': 7, 'Tarifas': 8";

            i = 9;
            screens += ", 'SalasNumeradas': " + i;
            i++;
            if (configurationSelector.isMenuSalasEnabled()) {
                screens += ", 'Salas': " + i;
                i++;
            }
        }

        if (configurationSelector.isMenuClientesEnabled() && role != Role.TAQUILLA) {
            screens += ", 'Clientes': " + i;
            i++;
        }

        if (role != Role.READONLY) {
            screens += ", 'Taquilla': " + i;
            i++;
        }

        if (configurationSelector.isMenuAbonosEnabled()) {
            screens += ", 'Abonos': " + i;
            i++;
        }

        if (configurationSelector.isMenuICAAEnabled() && role != Role.TAQUILLA) {
            screens += ", 'GenerarFicheros': " + i;
            i++;
        }

        if (isAdminByToken(currentRequest) || (configurationSelector.isMenuPerfilEnabled() && role == Role.ADMIN)) {
            screens += ", 'DatosRegistro': " + i;
            i++;
        }

        if (configurationSelector.isMenuIntegracionesEnabled() && role == Role.ADMIN) {
            screens += ", 'Integraciones': " + i;
            i++;
        }

        if (configurationSelector.isMenuTpvEnabled() && role == Role.ADMIN) {
            screens += ", 'Tpvs': " + i;
            i++;
        }

        if (hasSalasNumeradas && role == Role.ADMIN) {
            screens += ", 'PlantillasReservas': " + i;
            i++;
        }

        if (role == Role.ADMIN) {
            screens += ", 'Soporte': " + i;
            i++;
        }

        screens += "}]";

        return screens;
    }

    private String getViews(
        Role role,
        boolean hasSalasNumeradas
    ) {
        String views =
            "[{border: false, xtype: 'dashboard'}, {xtype: 'panelComprasReservas'}, {xtype: 'panelInformes'}, {xtype: 'panelInformesSesion'}";

        if (role != Role.TAQUILLA) {
            views +=
                ", {xtype: 'gridUsuarios'}, {xtype: 'gridTiposEventos'}, {xtype: 'panelEventos'}, {xtype: 'panelPlantillas'}, {xtype: 'panelTarifas'}";

            views += ", {xtype: 'panelSalasNumeradas'}";
            if (configurationSelector.isMenuSalasEnabled()) {
                views += ", {xtype: 'panelSalas'}";
            }
        }

        if (configurationSelector.isMenuClientesEnabled() && role != Role.TAQUILLA) {
            views += ", {xtype: 'gridClientes'}";
        }

        if (role != Role.READONLY) {
            views += ", {xtype: 'panelTaquilla'}";
        }

        if (configurationSelector.isMenuAbonosEnabled()) {
            views += ", {xtype: 'panelAbonos'}";
        }

        if (configurationSelector.isMenuICAAEnabled() && role != Role.TAQUILLA) {
            views += ", {xtype: 'panelSesionesFicheros'}";
        }

        if (isAdminByToken(currentRequest) || (configurationSelector.isMenuPerfilEnabled() && role == Role.ADMIN)) {
            views += ", {xtype: 'panelDatosRegistro'}";
        }

        if (configurationSelector.isMenuIntegracionesEnabled() && role == Role.ADMIN) {
            views += ", {xtype: 'panelIntegraciones'}";
        }

        if (configurationSelector.isMenuTpvEnabled() && role == Role.ADMIN) {
            views += ", {xtype: 'panelTpvs'}";
        }

        if (hasSalasNumeradas && role == Role.ADMIN) {
            views += ", {xtype: 'panelPlantillasReservas'}";
        }

        if (role == Role.ADMIN) {
            views += ", {xtype: 'panelSoporte'}";
        }

        views += "]";

        return views;
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    public Template indexPost() throws Exception {
        return index(null);
    }
}
