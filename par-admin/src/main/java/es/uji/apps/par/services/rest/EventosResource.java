package es.uji.apps.par.services.rest;


import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataParam;

import org.apache.commons.io.IOUtils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.exceptions.EventoNoEncontradoException;
import es.uji.apps.par.exceptions.GeneralPARException;
import es.uji.apps.par.exceptions.ImagenConAlphaException;
import es.uji.apps.par.exceptions.TamanyoImagenException;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Evento;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.services.CineService;
import es.uji.apps.par.services.EventosService;
import es.uji.apps.par.services.LocalizacionesService;
import es.uji.apps.par.services.SesionesService;
import es.uji.apps.par.services.UsersService;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.ExtjsTypeUtils;
import es.uji.apps.par.utils.Pair;

import static es.uji.apps.par.filters.UploadFilter.MAX_BINARY_SIZE;

@Path("evento")
public class EventosResource {

    @InjectParam
    private EventosService eventosService;

    @InjectParam
    private SesionesService sesionesService;

    @InjectParam
    private UsersService usersService;

    @InjectParam
    private LocalizacionesService localizacionesService;

    @InjectParam
    private CineService cineService;

    @Context
    HttpServletRequest currentRequest;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(
        @QueryParam("activos") boolean activos,
        @QueryParam("sort") @DefaultValue("[{\"property\":\"tituloVa\",\"direction\":\"ASC\"}]") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit,
        @QueryParam("filter") ExtGridFilterList filter
    ) throws ParseException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        Cine userCineByServerName = usersService.getUserCineByServerName(currentRequest.getServerName());
        List<Evento> eventos;
        int total = 0;

        if (activos) {
            eventos =
                eventosService.getEventosActivos(sort, start, limit, filter, userUID, userCineByServerName.getId(), null);
            total = eventosService.getTotalEventosActivos(filter, userUID, userCineByServerName.getId());
        } else {
            eventos = eventosService.getEventos(sort, start, limit, filter, userUID, userCineByServerName.getId(), null);
            total = eventosService.getTotalEventos(filter, userUID, userCineByServerName.getId());
        }

        return Response.ok().entity(new RestResponse(true, eventos, total)).build();
    }

    @GET
    @Path("taquilla")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventosConSesionesParaTaquilla(
        @QueryParam("activos") boolean activos,
        @QueryParam("sort") @DefaultValue("[{\"property\":\"tituloVa\",\"direction\":\"ASC\"}]") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit,
        @QueryParam("filter") ExtGridFilterList filter
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);
        Cine userCineByServerName = usersService.getUserCineByServerName(currentRequest.getServerName());
        List<Evento> eventos;
        int total = 0;

        if (activos) {
            eventos = eventosService
                .getEventosActivosTaquilla(sort, start, limit, filter, userUID, userCineByServerName.getId());
            total = eventosService.getTotalEventosActivosTaquilla(filter, userUID);
        } else {
            eventos =
                eventosService.getEventosTaquilla(sort, start, limit, filter, userUID, userCineByServerName.getId());
            total = eventosService.getTotalEventosTaquilla(filter, userUID, userCineByServerName.getId());
        }

        return Response.ok().entity(new RestResponse(true, eventos, total)).build();
    }

    @GET
    @Path("{id}/imagen")
    public Response getImagenEvento(@PathParam("id") Long eventoId) {
        try {
            String userUID = AuthChecker.getUserUID(currentRequest);
            Evento evento = eventosService.getEvento(eventoId, userUID);

            return Response.ok(evento.getImagen()).type(evento.getImagenContentType()).build();
        } catch (EventoNoEncontradoException e) {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("{id}/imagenPubli")
    public Response getImagenPubliEvento(@PathParam("id") Long eventoId) {
        try {
            String userUID = AuthChecker.getUserUID(currentRequest);
            Evento evento = eventosService.getEvento(eventoId, userUID);

            return Response.ok(evento.getImagenPubli()).type(evento.getImagenPubliContentType()).build();
        } catch (EventoNoEncontradoException e) {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("{id}/sesiones")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSesiones(
        @PathParam("id") Long eventoId,
        @QueryParam("activos") boolean activos,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit,
        @QueryParam("filter") ExtGridFilterList filter
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);
        List<Sesion> sesiones = new ArrayList<>();
        int total = 0;

        if (userUID != null) {
            if (activos) {
                sesiones = sesionesService
                    .getSesionesActivasConVendidasDateEnSegundos(eventoId, sort, start, limit, filter, userUID);
                total = sesionesService.getTotalSesionesActivas(eventoId, filter, userUID);
            } else {
                sesiones =
                    sesionesService.getSesionesConVendidasDateEnSegundos(eventoId, sort, start, limit, filter, userUID);
                total = sesionesService.getTotalSesiones(eventoId, filter, userUID);
            }
        }

        return Response.ok().entity(new RestResponse(true, sesiones, total)).build();
    }

    @GET
    @Path("{id}/sesiones/sala/{idSala}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSesionesPorSala(
        @PathParam("id") Long eventoId,
        @PathParam("idSala") Long salaId,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        return Response.ok().entity(sesionesService.getSesionesPorSala(eventoId, salaId, sort, userUID)).build();
    }

    @GET
    @Path("sesionesficheros")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSesionesCinePorFechas(
        @QueryParam("fechaInicio") String fechaInicio,
        @QueryParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        List<Sesion> sesiones = sesionesService.getSesionesICAAPorFechas(fechaInicio, fechaFin, sort, userUID);
        return Response.ok().entity(new RestResponse(true, sesiones, sesiones.size())).build();
    }

    @GET
    @Path("sesionesficheros/todo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSesionesPorFechas(
        @QueryParam("fechaInicio") String fechaInicio,
        @QueryParam("fechaFin") String fechaFin,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        List<Sesion> sesiones = sesionesService.getSesionesPorFechas(fechaInicio, fechaFin, sort, userUID);
        return Response.ok().entity(new RestResponse(true, sesiones, sesiones.size())).build();
    }

    @PUT
    @Path("sesionesficheros/{sesionId}/incidencia/{incidenciaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response setIncidencia(
        @PathParam("sesionId") long sesionId,
        @PathParam("incidenciaId") int incidenciaId
    ) {
        sesionesService.setIncidencia(sesionId, incidenciaId);

        return Response.ok().entity(new RestResponse(true)).build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"text/html"})
    public String addEvento(
        @FormDataParam("tituloEs") String tituloEs,
        @FormDataParam("descripcionEs") String descripcionEs,
        @FormDataParam("companyiaEs") String companyiaEs,
        @FormDataParam("interpretesEs") String interpretesEs,
        @FormDataParam("duracionEs") String duracionEs,
        @FormDataParam("dataBinary") InputStream dataBinary,
        @FormDataParam("dataBinary") FormDataContentDisposition dataBinaryDetail,
        @FormDataParam("dataBinary") FormDataBodyPart imagenBodyPart,
        @FormDataParam("dataBinaryPubli") InputStream dataBinaryPubli,
        @FormDataParam("dataBinaryPubli") FormDataContentDisposition dataBinaryDetailPubli,
        @FormDataParam("dataBinaryPubli") FormDataBodyPart imagenBodyPartPubli,
        @FormDataParam("premiosEs") String premiosEs,
        @FormDataParam("caracteristicasEs") String caracteristicasEs,
        @FormDataParam("comentariosEs") String comentariosEs,
        @FormDataParam("tipoEvento") Integer tipoEventoId,
        @FormDataParam("tpv") Integer tpvId,
        @FormDataParam("tituloVa") String tituloVa,
        @FormDataParam("descripcionVa") String descripcionVa,
        @FormDataParam("companyiaVa") String companyiaVa,
        @FormDataParam("interpretesVa") String interpretesVa,
        @FormDataParam("duracionVa") String duracionVa,
        @FormDataParam("premiosVa") String premiosVa,
        @FormDataParam("caracteristicasVa") String caracteristicasVa,
        @FormDataParam("comentariosVa") String comentariosVa,
        @FormDataParam("duracion") int duracion,
        @FormDataParam("porcentajeIVA") @DefaultValue("0") BigDecimal porcentajeIVA,
        @FormDataParam("retencionSGAE") BigDecimal retencionSGAE,
        @FormDataParam("ivaSGAE") BigDecimal ivaSGAE,
        @FormDataParam("asientosNumerados") Boolean asientosNumerados,
        @FormDataParam("entradasNominales") Boolean entradasNominales,
        @FormDataParam("rssId") String rssId,
        @FormDataParam("expediente") String expediente,
        @FormDataParam("codigoDistribuidora") String codigoDistribuidora,
        @FormDataParam("nombreDistribuidora") String nombreDistribuidora,
        @FormDataParam("nacionalidad") String nacionalidad,
        @FormDataParam("vo") String vo,
        @FormDataParam("metraje") String metraje,
        @FormDataParam("subtitulos") String subtitulos,
        @FormDataParam("multisesion") String checkMultisesion,
        @FormDataParam("jsonEventosMultisesion") String jsonEventosMultisesion,
        @FormDataParam("formato") String formato,
        @FormDataParam("promotor") String promotor,
        @FormDataParam("nifPromotor") String nifPromotor,
        @FormDataParam("emailPromotor") String emailPromotor,
        @FormDataParam("telefonoPromotor") String telefonoPromotor,
        @FormDataParam("direccionPromotor") String direccionPromotor,
        @FormDataParam("publico") String publico,
        @FormDataParam("reservasPublicas") String reservasPublicas,
        @FormDataParam("entradasLimitadas") String entradasLimitadas,
        @FormDataParam("entradasPorEmail") Integer entradasPorEmail
    ) throws GeneralPARException, IOException {
        createEvento(tituloEs, descripcionEs, companyiaEs, interpretesEs, duracionEs, dataBinary, dataBinaryDetail,
            imagenBodyPart, dataBinaryPubli, dataBinaryDetailPubli, imagenBodyPartPubli, premiosEs, caracteristicasEs,
            comentariosEs, tipoEventoId, tpvId, tituloVa, descripcionVa, companyiaVa, interpretesVa, duracionVa,
            premiosVa, caracteristicasVa, comentariosVa, duracion, porcentajeIVA, retencionSGAE, ivaSGAE, asientosNumerados,
            entradasNominales, rssId, expediente, codigoDistribuidora, nombreDistribuidora, nacionalidad, vo, metraje,
            subtitulos, checkMultisesion, jsonEventosMultisesion, formato, promotor, nifPromotor, emailPromotor,
            telefonoPromotor, direccionPromotor, publico, reservasPublicas, entradasLimitadas, entradasPorEmail);

        return "{success: 'true'}";
    }

    @Path("create")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createEvento(
        @FormDataParam("tituloEs") String tituloEs,
        @FormDataParam("descripcionEs") String descripcionEs,
        @FormDataParam("companyiaEs") String companyiaEs,
        @FormDataParam("interpretesEs") String interpretesEs,
        @FormDataParam("duracionEs") String duracionEs,
        @FormDataParam("dataBinary") InputStream dataBinary,
        @FormDataParam("dataBinary") FormDataContentDisposition dataBinaryDetail,
        @FormDataParam("dataBinary") FormDataBodyPart imagenBodyPart,
        @FormDataParam("dataBinaryPubli") InputStream dataBinaryPubli,
        @FormDataParam("dataBinaryPubli") FormDataContentDisposition dataBinaryDetailPubli,
        @FormDataParam("dataBinaryPubli") FormDataBodyPart imagenBodyPartPubli,
        @FormDataParam("premiosEs") String premiosEs,
        @FormDataParam("caracteristicasEs") String caracteristicasEs,
        @FormDataParam("comentariosEs") String comentariosEs,
        @FormDataParam("tipoEvento") Integer tipoEventoId,
        @FormDataParam("tpv") Integer tpvId,
        @FormDataParam("tituloVa") String tituloVa,
        @FormDataParam("descripcionVa") String descripcionVa,
        @FormDataParam("companyiaVa") String companyiaVa,
        @FormDataParam("interpretesVa") String interpretesVa,
        @FormDataParam("duracionVa") String duracionVa,
        @FormDataParam("premiosVa") String premiosVa,
        @FormDataParam("caracteristicasVa") String caracteristicasVa,
        @FormDataParam("comentariosVa") String comentariosVa,
        @FormDataParam("duracion") int duracion,
        @FormDataParam("porcentajeIVA") @DefaultValue("0") BigDecimal porcentajeIVA,
        @FormDataParam("retencionSGAE") BigDecimal retencionSGAE,
        @FormDataParam("ivaSGAE") BigDecimal ivaSGAE,
        @FormDataParam("asientosNumerados") Boolean asientosNumerados,
        @FormDataParam("entradasNominales") Boolean entradasNominales,
        @FormDataParam("rssId") String rssId,
        @FormDataParam("expediente") String expediente,
        @FormDataParam("codigoDistribuidora") String codigoDistribuidora,
        @FormDataParam("nombreDistribuidora") String nombreDistribuidora,
        @FormDataParam("nacionalidad") String nacionalidad,
        @FormDataParam("vo") String vo,
        @FormDataParam("metraje") String metraje,
        @FormDataParam("subtitulos") String subtitulos,
        @FormDataParam("multisesion") String checkMultisesion,
        @FormDataParam("jsonEventosMultisesion") String jsonEventosMultisesion,
        @FormDataParam("formato") String formato,
        @FormDataParam("promotor") String promotor,
        @FormDataParam("nifPromotor") String nifPromotor,
        @FormDataParam("emailPromotor") String emailPromotor,
        @FormDataParam("telefonoPromotor") String telefonoPromotor,
        @FormDataParam("direccionPromotor") String direccionPromotor,
        @FormDataParam("publico") String publico,
        @FormDataParam("reservasPublicas") String reservasPublicas,
        @FormDataParam("entradasLimitadas") String entradasLimitadas,
        @FormDataParam("entradasPorEmail") Integer entradasPorEmail
    ) throws GeneralPARException, IOException {
        String nombreArchivo = (dataBinaryDetail != null) ? dataBinaryDetail.getFileName() : "";
        String mediaType = (imagenBodyPart != null) ? imagenBodyPart.getMediaType().toString() : "";

        String nombreArchivoPubli = (dataBinaryDetailPubli != null) ? dataBinaryDetailPubli.getFileName() : "";
        String mediaTypePubli = (imagenBodyPartPubli != null) ? imagenBodyPartPubli.getMediaType().toString() : "";

        byte[] byteDataBinary = dataBinary != null ? IOUtils.toByteArray(dataBinary) : new byte[0];
        byte[] byteDataBinaryPubli = dataBinaryPubli != null ? IOUtils.toByteArray(dataBinaryPubli) : new byte[0];

        if (byteDataBinary.length > MAX_BINARY_SIZE || byteDataBinaryPubli.length > MAX_BINARY_SIZE) {
            throw new TamanyoImagenException();
        }

        if (isImageWithAlpha(dataBinary) || isImageWithAlpha(dataBinaryPubli)) {
            throw new ImagenConAlphaException();
        }

        String userUID = AuthChecker.getUserUID(currentRequest);
        Cine cine = usersService.getUserCineByUserUID(userUID);

        Evento evento = new Evento(rssId, tituloEs, descripcionEs, companyiaEs, interpretesEs, duracionEs, premiosEs,
            caracteristicasEs, comentariosEs, tituloVa, descripcionVa, companyiaVa, interpretesVa, duracionVa,
            premiosVa, caracteristicasVa, comentariosVa, byteDataBinary, nombreArchivo, mediaType, byteDataBinaryPubli,
            nombreArchivoPubli, mediaTypePubli, tipoEventoId, tpvId, duracion, porcentajeIVA, retencionSGAE, ivaSGAE,
            asientosNumerados, entradasNominales, expediente, codigoDistribuidora, nombreDistribuidora, nacionalidad,
            vo, metraje, subtitulos, formato, cine, promotor, nifPromotor, emailPromotor, telefonoPromotor,
            direccionPromotor, ExtjsTypeUtils.stringToBoolean(publico),
            ExtjsTypeUtils.stringToBoolean(reservasPublicas), ExtjsTypeUtils.stringToBoolean(entradasLimitadas), entradasPorEmail);

        if (checkMultisesion != null && checkMultisesion.equalsIgnoreCase("on"))
            evento.setEventosMultisesion(jsonEventosMultisesion);

        evento = eventosService.addEvento(evento, userUID);

        return Response.created(URI.create("")).entity(new RestResponse(true, Arrays.asList(evento), 1)).build();
    }

    private boolean isImageWithAlpha(InputStream dataBinary) throws IOException {
        if (dataBinary != null) {
            BufferedImage img = ImageIO.read(dataBinary);
            return img != null && img.getColorModel() != null && img.getColorModel().hasAlpha();
        }
        return false;
    }

    @POST
    @Path("{id}/sesiones")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addSesion(
        @PathParam("id") Integer eventoId,
        @QueryParam("createmultiples") boolean createMultiples,
        @QueryParam("anulaConMismaHoraYSala") @DefaultValue("false") boolean anulaConMismaHoraYSala,
        Sesion sesion
    ) throws GeneralPARException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        if (createMultiples) {
            sesionesService.addSesionesPeriodicas(eventoId, sesion, anulaConMismaHoraYSala, userUID);
        } else {
            sesionesService.addSesion(eventoId, sesion, anulaConMismaHoraYSala, userUID);
        }
        //TODO -> crear URL
        return Response.created(URI.create("")).entity(new RestResponse(true)).build();
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
        @PathParam("id") Integer id,
        @FormDataParam("tituloEs") String tituloEs,
        @FormDataParam("descripcionEs") String descripcionEs,
        @FormDataParam("companyiaEs") String companyiaEs,
        @FormDataParam("interpretesEs") String interpretesEs,
        @FormDataParam("duracionEs") String duracionEs,
        @FormDataParam("dataBinary") InputStream dataBinary,
        @FormDataParam("dataBinary") FormDataContentDisposition dataBinaryDetail,
        @FormDataParam("dataBinary") FormDataBodyPart imagenBodyPart,
        @FormDataParam("dataBinaryPubli") InputStream dataBinaryPubli,
        @FormDataParam("dataBinaryPubli") FormDataContentDisposition dataBinaryDetailPubli,
        @FormDataParam("dataBinaryPubli") FormDataBodyPart imagenBodyPartPubli,
        @FormDataParam("premiosEs") String premiosEs,
        @FormDataParam("caracteristicasEs") String caracteristicasEs,
        @FormDataParam("comentariosEs") String comentariosEs,
        @FormDataParam("tipoEvento") Integer tipoEventoId,
        @FormDataParam("tpv") Integer tpvId,
        @FormDataParam("tituloVa") String tituloVa,
        @FormDataParam("descripcionVa") String descripcionVa,
        @FormDataParam("companyiaVa") String companyiaVa,
        @FormDataParam("interpretesVa") String interpretesVa,
        @FormDataParam("duracionVa") String duracionVa,
        @FormDataParam("premiosVa") String premiosVa,
        @FormDataParam("caracteristicasVa") String caracteristicasVa,
        @FormDataParam("comentariosVa") String comentariosVa,
        @FormDataParam("duracion") int duracion,
        @FormDataParam("porcentajeIVA") @DefaultValue("0") BigDecimal porcentajeIVA,
        @FormDataParam("retencionSGAE") BigDecimal retencionSGAE,
        @FormDataParam("ivaSGAE") BigDecimal ivaSGAE,
        @FormDataParam("asientosNumerados") Boolean asientosNumerados,
        @FormDataParam("entradasNominales") Boolean entradasNominales,
        @FormDataParam("rssId") String rssId,
        @FormDataParam("expediente") String expediente,
        @FormDataParam("codigoDistribuidora") String codigoDistribuidora,
        @FormDataParam("nombreDistribuidora") String nombreDistribuidora,
        @FormDataParam("nacionalidad") String nacionalidad,
        @FormDataParam("vo") String vo,
        @FormDataParam("metraje") String metraje,
        @FormDataParam("subtitulos") String subtitulos,
        @FormDataParam("multisesion") String checkMultisesion,
        @FormDataParam("jsonEventosMultisesion") String jsonEventosMultisesion,
        @FormDataParam("formato") String formato,
        @FormDataParam("promotor") String promotor,
        @FormDataParam("nifPromotor") String nifPromotor,
        @FormDataParam("emailPromotor") String emailPromotor,
        @FormDataParam("telefonoPromotor") String telefonoPromotor,
        @FormDataParam("direccionPromotor") String direccionPromotor,
        @FormDataParam("publico") String publico,
        @FormDataParam("reservasPublicas") String reservasPublicas,
        @FormDataParam("entradasLimitadas") String entradasLimitadas,
        @FormDataParam("entradasPorEmail") Integer entradasPorEmail
    ) throws GeneralPARException, IOException {
        AuthChecker.canWrite(currentRequest);
        String userUID = AuthChecker.getUserUID(currentRequest);

        String nombreArchivo = (dataBinaryDetail != null) ? dataBinaryDetail.getFileName() : "";
        String mediaType = (imagenBodyPart != null) ? imagenBodyPart.getMediaType().toString() : "";

        String nombreArchivoPubli = (dataBinaryDetailPubli != null) ? dataBinaryDetailPubli.getFileName() : "";
        String mediaTypePubli = (imagenBodyPartPubli != null) ? imagenBodyPartPubli.getMediaType().toString() : "";

        Cine cine = usersService.getUserCineByUserUID(userUID);

        byte[] byteDataBinary = dataBinary != null ? IOUtils.toByteArray(dataBinary) : new byte[0];
        byte[] byteDataBinaryPubli = dataBinaryPubli != null ? IOUtils.toByteArray(dataBinaryPubli) : new byte[0];

        if (byteDataBinary.length > MAX_BINARY_SIZE || byteDataBinaryPubli.length > MAX_BINARY_SIZE) {
            throw new TamanyoImagenException();
        }

        if (isImageWithAlpha(dataBinary) || isImageWithAlpha(dataBinaryPubli)) {
            throw new ImagenConAlphaException();
        }

        Evento evento = new Evento(rssId, tituloEs, descripcionEs, companyiaEs, interpretesEs, duracionEs, premiosEs,
            caracteristicasEs, comentariosEs, tituloVa, descripcionVa, companyiaVa, interpretesVa, duracionVa,
            premiosVa, caracteristicasVa, comentariosVa, byteDataBinary, nombreArchivo, mediaType, byteDataBinaryPubli,
            nombreArchivoPubli, mediaTypePubli, tipoEventoId, tpvId, duracion, porcentajeIVA, retencionSGAE, ivaSGAE,
            asientosNumerados, entradasNominales, expediente, codigoDistribuidora, nombreDistribuidora, nacionalidad,
            vo, metraje, subtitulos, formato, cine, promotor, nifPromotor, emailPromotor, telefonoPromotor,
            direccionPromotor, ExtjsTypeUtils.stringToBoolean(publico),
            ExtjsTypeUtils.stringToBoolean(reservasPublicas), ExtjsTypeUtils.stringToBoolean(entradasLimitadas), entradasPorEmail);

        if (checkMultisesion != null && checkMultisesion.equalsIgnoreCase("on"))
            evento.setEventosMultisesion(jsonEventosMultisesion);

        evento.setId(id);
        eventosService.updateEvento(evento, userUID);

        // no devolvemos el evento porque al enviar la imagen colgaba el navegador durante un tiempo
        return Response.ok().entity(new RestResponse(true/* , Collections.singletonList(evento) */)).build();
    }

    @PUT
    @Path("{id}/sesiones/{sesionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
        @PathParam("id") Integer eventoId,
        @PathParam("sesionId") Integer sesionId,
        @QueryParam("anulaConMismaHoraYSala") @DefaultValue("false") boolean anulaConMismaHoraYSala,
        @QueryParam("cambiaFechaConCompras") @DefaultValue("false") boolean cambiaFechaConCompras,
        Sesion sesion
    ) throws GeneralPARException {
        AuthChecker.canWrite(currentRequest);
        String userUID = AuthChecker.getUserUID(currentRequest);

        sesion.setId(sesionId);
        sesionesService.updateSesion(eventoId, sesion, anulaConMismaHoraYSala, cambiaFechaConCompras, userUID);
        return Response.ok().entity(new RestResponse(true, Arrays.asList(sesion), 1)).build();
    }

    @DELETE
    @Path("{id}/sesiones/{sesionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response anulaSesion(
        @PathParam("id") Integer eventoId,
        @PathParam("sesionId") Integer sesionId
    ) {
        AuthChecker.canWrite(currentRequest);

        sesionesService.anulaSesion(sesionId);
        return Response.ok().entity(new RestResponse(true)).build();
    }

    @DELETE
    @Path("{id}/imagen")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeImagen(@PathParam("id") Long eventoId) {
        AuthChecker.canWrite(currentRequest);

        eventosService.removeImagen(eventoId);
        return Response.ok().entity(new RestResponse(true)).build();
    }

    @DELETE
    @Path("{id}/imagenPubli")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeImagenPubli(@PathParam("id") Long eventoId) {
        AuthChecker.canWrite(currentRequest);

        eventosService.removeImagenPubli(eventoId);
        return Response.ok().entity(new RestResponse(true)).build();
    }

    @GET
    @Path("sesion/{sesionId}/localizacion")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLocalizacionesSesion(@PathParam("sesionId") Long sesionId) {
        return Response.ok().entity(localizacionesService.getLocalizacionesSesion(sesionId)).build();
    }

    @GET
    @Path("peliculas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPeliculas() {
        return Response.ok().entity(eventosService.getPeliculas()).build();
    }

    @GET
    @Path("{id}/peliculas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPeliculasEvento(@PathParam("id") long eventoId) {
        return Response.ok().entity(eventosService.getPeliculas(eventoId)).build();
    }

    @GET
    @Path("{eventoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNumeroSesionesMismaHoraYSala(
        @QueryParam("sesionid") Long sesionId,
        @QueryParam("sala") long salaId,
        @QueryParam("fecha") String fechaCelebracion
    ) throws ParseException {
        String userUID = AuthChecker.getUserUID(currentRequest);

        return Response.ok().entity(sesionesService
            .getNumeroSesionesMismaHoraYSala(sesionId, salaId, DateUtils.spanishStringWithHourstoDate(fechaCelebracion),
                userUID)).build();
    }

    @POST
    @Path("{eventoId}/sesionesensala")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNumeroSesionesMismaHoraYSalaPeriodica(
        @QueryParam("sesionid") Long sesionId,
        @QueryParam("sala") long salaId,
        Sesion sesion
    ) throws ParseException {
        String userUID = AuthChecker.getUserUID(currentRequest);

        Pair pair = sesionesService.getNumeroSesionesMismaHoraYSalaPeriodica(sesionId, salaId, sesion, userUID);
        return Response.ok().entity(pair).build();
    }
}
