package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.exceptions.ButacaOcupadaAlActivarException;
import es.uji.apps.par.exceptions.ButacaOcupadaException;
import es.uji.apps.par.exceptions.CompraSinButacasException;
import es.uji.apps.par.exceptions.Constantes;
import es.uji.apps.par.exceptions.IncidenciaNotFoundException;
import es.uji.apps.par.exceptions.NoHayButacasLibresException;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.model.CompraRequest;
import es.uji.apps.par.model.DisponiblesLocalizacion;
import es.uji.apps.par.model.ResultadoCompra;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.ButacasService;
import es.uji.apps.par.services.ComprasService;
import es.uji.apps.par.services.EntradasService;
import es.uji.apps.par.services.SesionesService;
import es.uji.apps.par.services.UsersService;
import es.uji.apps.par.utils.DateUtils;
import es.uji.apps.par.utils.LocaleUtils;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;

@Path("compra")
public class CompraResource extends BaseResource {
    @InjectParam
    private ComprasService comprasService;

    @InjectParam
    private SesionesService sesionesService;

    @InjectParam
    private ButacasService butacasService;

    @InjectParam
    private EntradasService entradasService;

    @InjectParam
    private UsersService usersService;

    @Context
    HttpServletResponse currentResponse;

    @POST
    @Path("{idCompra}/pagada")
    @Consumes(MediaType.APPLICATION_JSON)
    public void marcaPagada(
        @PathParam("idCompra") Long idCompra,
        @QueryParam("tipopago") String tipoPago,
        @QueryParam("fecha") Long fecha,
        @QueryParam("referencia") String referenciaDePago,
        @QueryParam("email") String email,
        @QueryParam("nombre") String nombre,
        @QueryParam("apellidos") String apellidos
    ) {
        AuthChecker.canBuy(currentRequest);

        if (!ComprasService.isTipoPagoTarjeta(tipoPago)) {
            referenciaDePago = "";
        }

        String language = LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage();

        comprasService
            .marcarPagadaConReferenciaDePago(idCompra, tipoPago, referenciaDePago, email, nombre, apellidos, language);
        if (fecha != null) {
            comprasService.cambiarFecha(idCompra, new Date(fecha));
        }
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCompras(
        @PathParam("id") Long sesionId,
        @QueryParam("showAnuladas") int showAnuladas,
        @QueryParam("showOnline") int showOnline,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit,
        @QueryParam("filter") ExtGridFilterList filter,
        @QueryParam("search") @DefaultValue("") String search
    ) {
        return Response.ok().entity(new RestResponse(true, comprasService
            .getComprasBySesionFechaSegundos(sesionId, showAnuladas, showOnline, search, sort, start, limit, filter),
            comprasService.getTotalComprasBySesion(sesionId, showAnuladas, showOnline, search, filter))).build();
    }

    @PUT
    @Path("{idSesion}/{idCompraReserva}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response anularCompraOReserva(
        @PathParam("idSesion") Long sesionId,
        @PathParam("idCompraReserva") Long idCompraReserva
    ) throws IncidenciaNotFoundException {
        AuthChecker.canBuy(currentRequest);
        String userUID = AuthChecker.getUserUID(currentRequest);

        comprasService.anularCompraReserva(idCompraReserva, userUID);
        return Response.ok().build();
    }

    @PUT
    @Path("{idSesion}/desanuladas/{idCompraReserva}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response desanularCompraOReserva(
        @PathParam("idSesion") Long sesionId,
        @PathParam("idCompraReserva") Long idCompraReserva
    ) {
        AuthChecker.canBuy(currentRequest);
        String userUID = AuthChecker.getUserUID(currentRequest);

        try {
            comprasService.desanularCompraReserva(idCompraReserva, userUID);
            return Response.ok().build();
        } catch (ButacaOcupadaAlActivarException e) {
            String language = LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage();

            return errorResponse("error.butacaOcupadaAlActivar", e.getTaquilla() ? "taquilla" : "online",
                e.getComprador(),
                language.equalsIgnoreCase("ca") ? e.getLocalizacion().getNombreVa() : e.getLocalizacion().getNombreEs(),
                e.getFila(), e.getNumero());
        }
    }

    @PUT
    @Path("{idSesion}/pago/{idCompra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarFormaPago(
        @PathParam("idSesion") Long sesionId,
        @PathParam("idCompra") Long idCompraReserva,
        @QueryParam("recibo") String recibo,
        @QueryParam("tipopago") String tipoPago,
        @QueryParam("fechacompra") Date fechaCompra,
        @QueryParam("horacompra") String horaCompra,
        @QueryParam("email") String email,
        @QueryParam("nombre") String nombre,
        @QueryParam("apellidos") String apellidos
    ) {
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());

        AuthChecker.canBuy(currentRequest);

        if (!ComprasService.isTipoPagoTarjeta(tipoPago)) {
            recibo = "";
        }

        comprasService.actualizarFormaPago(sesionId, idCompraReserva, recibo, tipoPago,
            DateUtils.getDateWithHora(fechaCompra, horaCompra), email, nombre, apellidos, user.getUsuario());
        return Response.ok().build();
    }

    @PUT
    @Path("{idSesion}/passaracompra/{idCompra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response passarACompra(
        @PathParam("idSesion") Long sesionId,
        @PathParam("idCompra") Long idCompraReserva,
        @QueryParam("recibo") String recibo,
        @QueryParam("tipopago") String tipoPago,
        @QueryParam("fechacompra") Date fechaCompra,
        @QueryParam("horacompra") String horaCompra,
        @QueryParam("email") String email,
        @QueryParam("nombre") String nombre,
        @QueryParam("apellidos") String apellidos
    ) {
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());
        AuthChecker.canBuy(currentRequest);

        if (!ComprasService.isTipoPagoTarjeta(tipoPago)) {
            recibo = "";
        }

        comprasService.passarACompra(sesionId, idCompraReserva, recibo, tipoPago,
            DateUtils.getDateWithHora(fechaCompra, horaCompra), email, nombre, apellidos, user.getUsuario());
        return Response.ok().build();
    }

    @PUT
    @Path("{idSesion}/butacapassaracompra/{idCompra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response passarButacasACompra(
        @PathParam("idSesion") Long sesionId,
        @PathParam("idCompra") Long idCompraReserva,
        @QueryParam("recibo") String recibo,
        @QueryParam("tipopago") String tipoPago,
        @QueryParam("fechacompra") Date fechaCompra,
        @QueryParam("horacompra") String horaCompra,
        @QueryParam("email") String email,
        @QueryParam("nombre") String nombre,
        @QueryParam("apellidos") String apellidos,
        List<Long> idsButacas
    ) {
        AuthChecker.canBuy(currentRequest);
        String userUID = AuthChecker.getUserUID(currentRequest);

        if (!ComprasService.isTipoPagoTarjeta(tipoPago)) {
            recibo = "";
        }

        String language = LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage();

        comprasService.passarButacasACompra(sesionId, idCompraReserva, recibo, tipoPago,
            DateUtils.getDateWithHora(fechaCompra, horaCompra), email, nombre, apellidos, idsButacas, language,
            userUID);
        return Response.ok().build();
    }

    @PUT
    @Path("{idSesion}/{idCompraReserva}/{idButaca}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response anularButaca(
        @PathParam("idSesion") Long sesionId,
        @PathParam("idCompraReserva") Long idCompraReserva,
        @PathParam("idButaca") Long idButaca
    ) throws IncidenciaNotFoundException {
        AuthChecker.canBuy(currentRequest);
        String userUID = AuthChecker.getUserUID(currentRequest);

        comprasService.anularButacas(Arrays.asList(idButaca), userUID);
        return Response.ok().build();
    }

    @PUT
    @Path("{idSesion}/butacas/anuladas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response anularButacas(
        @PathParam("idSesion") Long sesionId,
        List<Long> idsButacas
    ) throws IncidenciaNotFoundException {
        AuthChecker.canBuy(currentRequest);
        String userUID = AuthChecker.getUserUID(currentRequest);

        comprasService.anularButacas(idsButacas, userUID);
        return Response.ok().build();
    }

    @POST
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response compraEntrada(
        @PathParam("id") Long sesionId,
        CompraRequest compraRequest
    ) throws IncidenciaNotFoundException {
        AuthChecker.canBuy(currentRequest);
        String userUID = AuthChecker.getUserUID(currentRequest);
        String language = LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage();
        try {
            butacasService.cargarInformacionButacas(compraRequest.getButacasSeleccionadas(), language);
            ResultadoCompra resultadoCompra = comprasService.registraCompraTaquilla(sesionId, compraRequest, userUID);
            return Response.ok(resultadoCompra).build();
        } catch (NoHayButacasLibresException e) {
            return errorResponse("error.noHayButacas", e.getLocalizacion());
        } catch (ButacaOcupadaException e) {
            return errorResponse("error.butacaOcupada", e.getLocalizacion(), e.getFila(), e.getNumero());
        } catch (CompraSinButacasException e) {
            return errorResponse("error.compraSinButacas");
        }
    }

    @GET
    @Path("{id}/precios")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPreciosSesion(
        @PathParam("id") Long sesionId,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        return Response.ok().entity(new RestResponse(true,
            sesionesService.getPreciosEditablesSesion(sesionId, sort, start, limit, true, userUID),
            sesionesService.getTotalPreciosSesion(sesionId))).build();
    }

    @GET
    @Path("{id}/disponibles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOcupacionesNoNumerada(@PathParam("id") Long sesionId) {
        List<DisponiblesLocalizacion> listadoOcupacionesNoNumeradas = butacasService.getDisponibles(sesionId);
        return Response.ok()
            .entity(new RestResponse(true, listadoOcupacionesNoNumeradas, listadoOcupacionesNoNumeradas.size()))
            .build();
    }

    @POST
    @Path("{id}/importe")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getImportesButacas(
        @PathParam("id") Long sesionId,
        CompraRequest compraRequest
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);
        BigDecimal importe =
            comprasService.calculaImporteButacas(sesionId, compraRequest.getButacasSeleccionadas(), false, userUID);
        return Response.ok().entity(importe.setScale(2, BigDecimal.ROUND_UP).toString()).build();
    }

    @GET
    @Path("{idCompra}/butacas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getButacasCompra(
        @PathParam("idCompra") Long idCompra,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("filter") ExtGridFilterList filter,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        String language = LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage();

        return Response.ok().entity(
            new RestResponse(true, butacasService.getButacasCompra(idCompra, sort, start, limit, filter, language),
                butacasService.getTotalButacasCompra(idCompra, filter))).build();
    }

    @GET
    @Path("{id}/print/{pdfType}")
    @Produces(MediaType.TEXT_HTML)
    public Template imprimeEntrada(
        @PathParam("id") String uuidCompra,
        @PathParam("pdfType") String pdfType
    ) {
        Template template = new HTMLTemplate(Constantes.PLANTILLAS_DIR + "print",
            LocaleUtils.getLocale(configurationSelector, currentRequest), APP);

        template.put("urlAdmin", configurationSelector.getUrlAdmin());
        template.put("uuid", uuidCompra);
        template.put("pdfType", pdfType);

        return template;
    }

    @GET
    @Path("{id}/pdf")
    @Produces("application/pdf")
    public Response generaEntradaPrintAtHome(@PathParam("id") String uuidCompra) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        String userUID = AuthChecker.getUserUID(currentRequest);
        entradasService.generaEntrada(uuidCompra, bos, userUID, configurationSelector.getUrlPublic(),
            configurationSelector.getUrlPieEntrada());

        Response response =
            Response.ok(bos.toByteArray()).header("Cache-Control", "no-cache, no-store, must-revalidate")
                .header("Pragma", "no-cache").header("Expires", "0").build();

        return response;
    }

    @GET
    @Path("{id}/pdftaquilla")
    @Produces("application/pdf")
    public Response generaEntradaTaquilla(@PathParam("id") String uuidCompra) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        String userUID = AuthChecker.getUserUID(currentRequest);
        entradasService.generaEntradaTaquilla(uuidCompra, bos, userUID, configurationSelector.getUrlPublic());

        Response response =
            Response.ok(bos.toByteArray()).header("Cache-Control", "no-cache, no-store, must-revalidate")
                .header("Pragma", "no-cache").header("Expires", "0").build();

        return response;
    }

    @HEAD
    @Path("{id}/pdftaquilla")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generaEntradaTaquillaHeader(@PathParam("id") String uuidCompra) {
        if (entradasService.compraHasButacas(uuidCompra)) {
            return Response.ok().build();
        } else {
            return Response.serverError().build();
        }
    }

    @GET
    @Path("sesion/{sesionId}/csv")
    @Produces("text/csv")
    public Response exportComprasCSV(
        @PathParam("sesionId") Long sesionId
    ) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        String userUID = AuthChecker.getUserUID(currentRequest);
        comprasService.exportCSV(bos, sesionId, userUID);

        return Response.ok(bos.toByteArray()).header("Content-Disposition", "attachment; filename =\"entradas.csv\"")
            .build();
    }

    @GET
    @Path("sesion/{sesionId}/csv/detail/entradas")
    @Produces("text/csv")
    public Response exportEntradasCSV(
        @PathParam("sesionId") Long sesionId
    ) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        String userUID = AuthChecker.getUserUID(currentRequest);
        comprasService.exportCSVEntradas(bos, sesionId, userUID);

        return Response.ok(bos.toByteArray()).header("Content-Disposition", "attachment; filename =\"entradas.csv\"")
            .build();
    }

    @GET
    @Path("sesion/{sesionId}/csv/detail")
    @Produces("text/csv")
    public Response exportComprasCSVDetail(
        @PathParam("sesionId") Long sesionId
    ) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        String userUID = AuthChecker.getUserUID(currentRequest);
        comprasService.exportCSVDetail(bos, sesionId, userUID);

        return Response.ok(bos.toByteArray()).header("Content-Disposition", "attachment; filename =\"compras.csv\"")
            .build();
    }
}
