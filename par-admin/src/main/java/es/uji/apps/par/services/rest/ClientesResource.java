package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.ext.ExtGridFilterList;
import es.uji.apps.par.services.ClientesService;

@Path("clientes")
public class ClientesResource extends BaseResource {
    @InjectParam
    private ClientesService clientesService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit,
        @QueryParam("filter") ExtGridFilterList filter
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        return Response.ok().entity(
            new RestResponse(true, clientesService.getClientes(sort, start, limit, filter, userUID),
                clientesService.getTotalClientes(filter, userUID))).build();
    }

    @GET
    @Path("mails")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMails(
        @QueryParam("sort") String sort,
        @QueryParam("filter") ExtGridFilterList filter
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        return Response.ok().entity(new RestResponse(true, clientesService.getMails(sort, filter, userUID), 0)).build();
    }

    @DELETE
    @Path("{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteClient(@PathParam("email") String email) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        return Response.ok().entity(new RestResponse(clientesService.removeInfoPeriodica(email, userUID))).build();
    }
}