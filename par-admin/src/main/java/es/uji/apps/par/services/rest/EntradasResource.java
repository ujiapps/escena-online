package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.services.AbonosService;
import es.uji.apps.par.services.ButacasService;
import es.uji.apps.par.services.LocalizacionesService;
import es.uji.apps.par.services.PreciosService;

@Path("entrada")
public class EntradasResource extends BaseResource {

    @InjectParam
    private AbonosService abonosService;

    @InjectParam
    private PreciosService preciosService;

    @InjectParam
    private ButacasService butacasService;

    @InjectParam
    private LocalizacionesService localizacionesService;

    @Context
    HttpServletRequest currentRequest;

    @GET
    @Path("{id}/precios")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPreciosSesion(@PathParam("id") Long sesionId) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        return Response.ok().entity(
            new RestResponse(true, preciosService.getPreciosSesion(sesionId, userUID),
                preciosService.getTotalPreciosSesion(sesionId))).build();
    }
}