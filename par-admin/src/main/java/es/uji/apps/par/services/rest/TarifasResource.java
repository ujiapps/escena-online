package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.db.ButacaDTO;
import es.uji.apps.par.exceptions.GeneralPARException;
import es.uji.apps.par.model.Tarifa;
import es.uji.apps.par.services.ButacasService;
import es.uji.apps.par.services.SesionesService;
import es.uji.apps.par.services.TarifasService;
import es.uji.apps.par.services.UsersService;

@Path("tarifa")
public class TarifasResource
{
    @InjectParam
    private UsersService usersService;

    @InjectParam
    private TarifasService tarifasService;

    @InjectParam
    private ButacasService butacasService;

    @InjectParam
    private SesionesService sesionesService;

    @Context
    HttpServletRequest currentRequest;

    @GET
    @Path("editable")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEditables(@QueryParam("sort") @DefaultValue("[{\"property\":\"nombre\",\"direction\":\"ASC\"}]") String sort,
        @QueryParam("start") int start, @QueryParam("limit") @DefaultValue("1000") int limit)
    {
        String userUID = AuthChecker.getUserUID(currentRequest);
        List<Tarifa> tarifas = tarifasService.getEditables(sort, start, limit, userUID);
        return Response.ok().entity(new RestResponse(true, tarifas, (int) tarifasService.getTotalEditables(userUID))).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@QueryParam("sort") @DefaultValue("[{\"property\":\"nombre\",\"direction\":\"ASC\"}]") String sort, 
    		@QueryParam("start") int start, @QueryParam("limit") @DefaultValue("1000") int limit)
    {
        String userUID = AuthChecker.getUserUID(currentRequest);
        List<Tarifa> tarifas = tarifasService.getAll(sort, start, limit, userUID);
        return Response.ok().entity(new RestResponse(true, tarifas, (int) tarifasService.getTotal(userUID))).build();
    }

    @GET
    @Path("{butacaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getButacaTarifa(@PathParam("butacaId") Long butacaId)
    {
        ButacaDTO butaca = butacasService.getButaca(butacaId);

        List<Tarifa> tarifas = new ArrayList<>();
        tarifas.addAll(sesionesService.getTarifasConPrecioConPlantillaDeLocalizacion(butaca.getParSesion().getId(), butaca.getParLocalizacion().getId()));
        tarifas.addAll(sesionesService.getTarifasConPrecioSinPlantillaDeLocalizacion(butaca.getParSesion().getId(), butaca.getParLocalizacion().getId()));

        return Response.ok().entity(new RestResponse(true, tarifas, tarifas.size())).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addTarifa(Tarifa tarifa) throws GeneralPARException {
        String userUID = AuthChecker.getUserUID(currentRequest);

        tarifa = tarifasService.add(tarifa, userUID);

        return Response.created(URI.create(""))
                .entity(new RestResponse(true, Arrays.asList(tarifa), 1)).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Tarifa tarifa) throws GeneralPARException {
        AuthChecker.canWrite(currentRequest);
        String userUID = AuthChecker.getUserUID(currentRequest);

        tarifa = tarifasService.update(tarifa, userUID);

        return Response.ok()
                .entity(new RestResponse(true, Collections.singletonList(tarifa), 1)).build();
    }
    
    @DELETE
    @Path("editable/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response remove(@PathParam("id") Integer id)
    {
    	AuthChecker.canWrite(currentRequest);
        tarifasService.remove(id);
        return Response.ok().entity(new RestResponse(true)).build();
    }
}
