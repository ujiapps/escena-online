package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.exceptions.TPVVisibleMinimoException;
import es.uji.apps.par.i18n.ResourceProperties;
import es.uji.apps.par.model.Tpv;
import es.uji.apps.par.services.TpvsService;
import es.uji.apps.par.utils.ExtjsTypeUtils;
import es.uji.apps.par.utils.LocaleUtils;

@Path("tpv")
public class TpvsResource extends BaseResource {
    @InjectParam
    private TpvsService tpvsService;

    @Context
    HttpServletRequest currentRequest;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(
        @QueryParam("visibles") @DefaultValue("false") boolean onlyVisibles,
        @QueryParam("allOption") @DefaultValue("false") boolean allOption,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        List<Tpv> tpvs = tpvsService.getTpvs(userUID, onlyVisibles, sort, start, limit);
        if (allOption) {
            Tpv tpv = new Tpv();
            tpv.setId(-1);
            tpv.setNombre(ResourceProperties.getProperty( LocaleUtils.getLocale(configurationSelector, currentRequest), "all"));
            tpvs.add(0, tpv);
        }

        return Response.ok().entity(new RestResponse(true, tpvs, tpvs.size())).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Tpv tpv) {
        AuthChecker.canWrite(currentRequest);
        Boolean visible = ExtjsTypeUtils.stringToBoolean(tpv.getVisible());

        if (!visible) {
            String userUID = AuthChecker.getUserUID(currentRequest);
            long tpvVisibles = tpvsService.countTpvsVisible(userUID, tpv);
            if (tpvVisibles > 0) {
                tpv = tpvsService.update(tpv);
            } else {
                throw new TPVVisibleMinimoException();
            }
        } else {
            tpv = tpvsService.update(tpv);
        }

        return Response.ok().entity(new RestResponse(true, Collections.singletonList(tpv), 1)).build();
    }
}
