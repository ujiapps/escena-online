package es.uji.apps.par.auth;

import com.google.common.base.Strings;

import org.jasypt.util.password.BasicPasswordEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.UsersService;

public class DataBaseAuthenticator implements Authenticator {
    private static final Logger log = LoggerFactory.getLogger(DataBaseAuthenticator.class);

    private static final String LOGIN_PARAM = "login";
    private static final String PASSWORD_PARAM = "password";

    private BasicPasswordEncryptor encryptor;

    public DataBaseAuthenticator() {
        encryptor = new BasicPasswordEncryptor();
    }

    Configuration configuration;
    UsersService usersService;

    @Override
    public int authenticate(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Usuario usuario;
        if (urlValidacion(request)) {
            usuario = autenticaConToken(request);
            if (usuario != null) {
                return setValoresEnSesion(usuario.getUsuario(), session, usuario);
            } else {
                return AUTH_FAILED;
            }
        } else {
            usuario = getUsuario(request);
            if (usuario != null && validaPassword(request, usuario)) {
                return setValoresEnSesion(request.getParameter(LOGIN_PARAM), session, usuario);

            } else {
                if (request.getParameter(LOGIN_PARAM) != null)
                    session.setAttribute(ERROR_LOGIN, true);
                return AUTH_FAILED;
            }
        }
    }

    private int setValoresEnSesion(
        String login,
        HttpSession session,
        Usuario usuario
    ) {
        if (usuario.getRole() != null) {
            session.setAttribute(USER_ATTRIBUTE, login);
            session.setAttribute(ERROR_LOGIN, false);
            if (usuario.getRole().equals(Role.READONLY)) {
                session.setAttribute(READONLY_ATTRIBUTE, true);
            } else if (usuario.getRole().equals(Role.TAQUILLA)) {
                session.setAttribute(TAQUILLA_ATTRIBUTE, true);
            }
            return AUTH_OK;
        } else {
            return AUTH_FAILED;
        }
    }

    private Usuario autenticaConToken(HttpServletRequest request) {
        String url = request.getRequestURL().toString();
        String tokenValidacion = url.substring(url.lastIndexOf("/") + 1, url.length());

        Usuario usuario = usersService.getUserByTokenValidacion(tokenValidacion);
        if (isTokenValidoAhora(usuario)) {
            if (hasUsuarioAccessToServerName(request, usuario))
                return usuario;
        }

        return null;
    }

    private boolean hasUsuarioAccessToServerName(
        HttpServletRequest request,
        Usuario usuario
    ) {
        if (usuario != null) {
            Cine cineFromServerName = usersService.getUserCineByServerName(request.getServerName());
            Cine cineByUserUID = usersService.getUserCineByUserUID(usuario.getUsuario());
            if (cineFromServerName.getId() == cineByUserUID.getId()) {
                return true;
            }
            else {
                log.error(String.format("El usuario %s está accediento por un dominio (%s) que no le pertenece", usuario.getUsuario(), request.getServerName()));
            }
        }

        return false;
    }

    private boolean isTokenValidoAhora(Usuario usuario) {
        return usuario.getExpiracionToken().after(new Timestamp(System.currentTimeMillis()));
    }

    private boolean urlValidacion(HttpServletRequest request) {
        return request.getRequestURL().toString().contains("/validacion");
    }

    @Override
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void setUserService(UsersService usersService) {
        this.usersService = usersService;
    }

    private Usuario getUsuario(HttpServletRequest request) {
        String loginFromRequest = request.getParameter(LOGIN_PARAM);
        String passwordFromRequest = request.getParameter(PASSWORD_PARAM);
        if (!Strings.isNullOrEmpty(loginFromRequest) && !Strings.isNullOrEmpty(passwordFromRequest)) {
            Usuario usuario = usersService.getUserById(loginFromRequest);
            if (hasUsuarioAccessToServerName(request, usuario))
                return usuario;
        }
        return null;
    }

    private boolean validaPassword(
        HttpServletRequest request,
        Usuario usuario
    ) {
        String passwordFromRequest = request.getParameter(PASSWORD_PARAM);
        if (!Strings.isNullOrEmpty(passwordFromRequest)) {
            return encryptor.checkPassword(passwordFromRequest, usuario.getPassword());
        }
        return false;
    }
}
