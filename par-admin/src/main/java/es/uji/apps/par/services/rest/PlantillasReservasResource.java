package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.net.URI;
import java.util.Arrays;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.exceptions.GeneralPARException;
import es.uji.apps.par.model.Plantilla;
import es.uji.apps.par.model.ReservaRequest;
import es.uji.apps.par.services.PlantillasButacasReservasService;
import es.uji.apps.par.services.PlantillasReservasService;
import es.uji.apps.par.utils.LocaleUtils;

@Path("plantillareservas")
public class PlantillasReservasResource extends BaseResource {

    @InjectParam
    private PlantillasReservasService plantillasReservasService;

    @InjectParam
    private PlantillasButacasReservasService plantillasButacasReservasService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlantillas(
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        String userUID = AuthChecker.getUserUID(currentRequest);

        return Response.ok().entity(new RestResponse(true, plantillasReservasService.get(sort, start, limit, userUID),
            plantillasReservasService.getTotalPlantillaReservas(userUID))).build();
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response remove(@PathParam("id") String id) {
        AuthChecker.canWrite(currentRequest);

        plantillasReservasService.remove(Integer.parseInt(id));
        return Response.ok().entity(new RestResponse(true)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Plantilla plantillaPrecios) throws GeneralPARException {
        AuthChecker.canWrite(currentRequest);

        Plantilla newPlantillaPrecios = plantillasReservasService.add(plantillaPrecios);
        return Response.created(URI.create("")).entity(new RestResponse(true, Arrays.asList(newPlantillaPrecios), 1))
            .build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
        @PathParam("id") String id,
        Plantilla plantillaPrecios
    ) throws GeneralPARException {
        AuthChecker.canWrite(currentRequest);

        plantillaPrecios.setId(Long.valueOf(id));
        plantillasReservasService.update(plantillaPrecios);
        return Response.ok().entity(new RestResponse(true, Arrays.asList(plantillaPrecios), 1)).build();
    }

    @GET
    @Path("{id}/butacas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPrecios(
        @PathParam("id") String id,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit
    ) {
        String language = LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage();

        return Response.ok().entity(new RestResponse(true,
            plantillasButacasReservasService.getButacasOfPlantilla(Long.valueOf(id), sort, start, limit, language),
            plantillasButacasReservasService.getTotalButacasOfPlantilla(Long.valueOf(id)))).build();
    }

    @POST
    @Path("{plantillaId}/butacas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addPrecio(
        @PathParam("plantillaId") String plantillaId,
        ReservaRequest reservaRequest
    ) throws GeneralPARException {
        AuthChecker.canWrite(currentRequest);

        plantillasButacasReservasService
            .actualizaButacasPlantilla(Long.valueOf(plantillaId), reservaRequest.getButacasSeleccionadas());
        return Response.ok().build();
    }
}
