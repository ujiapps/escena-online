package es.uji.apps.par.services.rest.crm;

import com.sun.jersey.api.core.InjectParam;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.enums.TipoPago;
import es.uji.apps.par.exceptions.ButacaOcupadaException;
import es.uji.apps.par.exceptions.CompraSinButacasException;
import es.uji.apps.par.exceptions.NoHayButacasLibresException;
import es.uji.apps.par.exceptions.ResponseMessage;
import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.model.CompraYUso;
import es.uji.apps.par.model.DisponiblesLocalizacion;
import es.uji.apps.par.model.ResultadoCompra;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.TipoEvento;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.report.EntradaReportFactory;
import es.uji.apps.par.services.ButacasService;
import es.uji.apps.par.services.ComprasService;
import es.uji.apps.par.services.EntradasService;
import es.uji.apps.par.services.SesionesService;
import es.uji.apps.par.services.UsersService;
import es.uji.apps.par.services.rest.BaseResource;

@Path("crm")
public class CrmResource extends BaseResource {

    @InjectParam
    private ComprasService comprasService;

    @InjectParam
    private SesionesService sesionesService;

    @InjectParam
    private ButacasService butacasService;

    @InjectParam
    private UsersService usersService;

    @Context
    HttpServletRequest currentRequest;

    @POST
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response compraEntrada(@PathParam("id") String rssId, @QueryParam("entradas") int numeroEntradas, @QueryParam("email") String email, @QueryParam("nombre") String nombre, @QueryParam("apellidos") String apellidos)
    {
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());

        if (email == null) {
            return errorResponse("error.datosComprador.email");
        }
        else if (nombre == null) {
            return errorResponse("error.datosComprador.nombre");
        }
        else if (apellidos == null) {
            return errorResponse("error.datosComprador.apellidos");
        }
        else if (numeroEntradas == 0) {
            return errorResponse("error.compraSinButacas");
        }

        List<Sesion> sesiones = sesionesService.getSesionesPorRssId(rssId, user.getUsuario());
        if (sesiones != null && sesiones.size() == 1) {
            Sesion sesion = sesiones.get(0);

            TipoEvento parTiposEvento = sesion.getEvento().getParTiposEvento();
            if (parTiposEvento != null && parTiposEvento.getNombreEs() != null) {
                try {
                    EntradaReportFactory.newInstanceByClassName("es.uji.apps.par.report." + EntradasService.BEAN_REPORT_PREFIX +
							parTiposEvento.getNombreEs() + EntradasService.BEAN_REPORT_SUFFIX);
                } catch(Exception e) {
                    return errorResponse("error.tipoNoPermitido");
                }

                if (sesion.getFechaCelebracion().before(new Date())) {
                    return errorResponse("error.sesionPasada");
                }

                List<Butaca> butacasSeleccionadas = new ArrayList<Butaca>();
                int quedanPorReservar = numeroEntradas;
                List<DisponiblesLocalizacion> disponiblesNoNumerada = butacasService.getDisponibles(sesion.getId());
                for (DisponiblesLocalizacion disponiblesLocalizacion : disponiblesNoNumerada) {
                    for (int i = 0; i < disponiblesLocalizacion.getDisponibles() && quedanPorReservar > 0; i++) {
                        if (quedanPorReservar > 0) {
                            Butaca butaca = new Butaca();
                            butaca.setLocalizacion(disponiblesLocalizacion.getLocalizacion());
                            butaca.setPrecio(BigDecimal.ZERO);
                            butaca.setPrecioSinComision(BigDecimal.ZERO);
                            butaca.setTipo("1");
                            butacasSeleccionadas.add(butaca);
                            quedanPorReservar--;
                        }
                    }

                    if (quedanPorReservar == 0)
                        break;
                }

                if (quedanPorReservar > 0) {
                    return errorResponse("error.noHayButacasParaLocalizacion");
                }

                try {
                    ResultadoCompra resultadoCompra = comprasService.registraCompra(sesion.getId(), butacasSeleccionadas, true, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, email, nombre, apellidos, user.getUsuario());

                    if (resultadoCompra.getCorrecta()) {
                        comprasService.marcaPagada(resultadoCompra.getId(), TipoPago.METALICO);
                        String urlPdf = configurationSelector.getUrlPublic() + "/rest/compra/" + resultadoCompra.getUuid() + "/pdf";
                        return Response.ok(new ResponseMessage(true, urlPdf)).build();
                    }
                    else {
                        return errorResponse("error.ws");
                    }
                } catch (NoHayButacasLibresException e) {
                    return errorResponse("error.noHayButacas",  e.getLocalizacion());
                } catch (ButacaOcupadaException e) {
                    return errorResponse("error.butacaOcupada", e.getLocalizacion(), e.getFila(), e.getNumero());
                } catch (CompraSinButacasException e) {
                    return errorResponse("error.compraSinButacas");
                }
            }
            else {
                return errorResponse("error.tipoNoEncontrado");
            }

        }
        else {
            return errorResponse("error.multiplesSesiones");
        }
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response muestraCompras(@PathParam("id") String rssId)
    {
        Usuario user = usersService.getUserByServerName(currentRequest.getServerName());

        List<Sesion> sesiones = sesionesService.getSesionesPorRssId(rssId, user.getUsuario());
        if (sesiones != null && sesiones.size() == 1) {
            Sesion sesion = sesiones.get(0);

            TipoEvento parTiposEvento = sesion.getEvento().getParTiposEvento();
            if (parTiposEvento != null && parTiposEvento.getNombreEs() != null) {
                try {
                    EntradaReportFactory.newInstanceByClassName("es.uji.apps.par.report." + EntradasService.BEAN_REPORT_PREFIX +
							parTiposEvento.getNombreEs() + EntradasService.BEAN_REPORT_SUFFIX);
                } catch (Exception e) {
                    return errorResponse("error.tipoNoPermitido");
                }

                List<CompraYUso> comprasYPresentadas = comprasService.getComprasYPresentadas(sesion.getId());
                return Response.ok(comprasYPresentadas).build();
            }
            else {
                return errorResponse("error.tipoNoEncontrado");
            }
        }
        else {
            return errorResponse("error.multiplesSesiones");
        }
    }
}
