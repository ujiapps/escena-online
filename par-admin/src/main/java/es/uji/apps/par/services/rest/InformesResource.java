package es.uji.apps.par.services.rest;


import com.sun.jersey.api.core.InjectParam;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.model.SorterInforme;
import es.uji.apps.par.model.TipoInforme;
import es.uji.apps.par.services.InformesService;
import es.uji.apps.par.utils.LocaleUtils;

@Path("informe")
public class InformesResource extends BaseResource
{
    @InjectParam
    private InformesService informesService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll()
    {
        List<TipoInforme> tiposInforme = informesService.getTiposInformeGenerales(configurationSelector.getInformes(), LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage());
        tiposInforme.addAll(informesService.getTiposInformeGeneralesSesion(configurationSelector.getInformes(), LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage()));
                
        return Response.ok().entity(new RestResponse(true, tiposInforme, tiposInforme.size())).build();
    }

    @GET
    @Path("generales")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGenerales()
    {
        List<TipoInforme> tiposInformeGenerales = informesService
            .getTiposInformeGenerales(configurationSelector.getInformesGenerales(),
                LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage());

        return Response.ok().entity(new RestResponse(true, tiposInformeGenerales, tiposInformeGenerales.size())).build();
    }

    @GET
    @Path("generales/sesion")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGeneralesSesion()
    {
        List<TipoInforme> tiposInformeGeneralesSesion = informesService
            .getTiposInformeGeneralesSesion(configurationSelector.getInformesGenerales(),
                LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage());

        return Response.ok().entity(new RestResponse(true, tiposInformeGeneralesSesion, tiposInformeGeneralesSesion.size())).build();
    }



    @GET
    @Path("sorters/{typeId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSorters(@PathParam("typeId") String typeId)
    {
        List<SorterInforme> sorterInformes = informesService.getSortersInformesByTipo(configurationSelector.getInformesGenerales(), LocaleUtils.getLocale(configurationSelector, currentRequest).getLanguage(), typeId);

        return Response.ok().entity(new RestResponse(true, sorterInformes, sorterInformes.size())).build();
    }
}
