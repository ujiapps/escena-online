package es.uji.apps.par.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosRegistro {

    private Long id;
    private String cif;
    private String empresa;
    private String cp;
    private String direccion;
    private String codMunicipio;
    private String municipio;
    private String telefono;
    private String email;
    private String password;
    private String url;
    private Boolean emailVerificado;


    public DatosRegistro() {

    }

    public DatosRegistro(
        Usuario usuario,
        Cine cine
    ) {
        this.id = cine.getId();
        this.cif = cine.getCif();
        this.empresa = cine.getEmpresa();
        this.cp = cine.getCp();
        this.direccion = cine.getDireccion();
        this.codMunicipio = cine.getCodigoMunicipio();
        this.municipio = cine.getNombreMunicipio();
        this.telefono = cine.getTelefono();
        this.email = usuario.getMail();
        this.url = usuario.getUrl();
        this.emailVerificado = usuario.getEmailVerificado();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodMunicipio() {
        return codMunicipio;
    }

    public void setCodMunicipio(String codMunicipio) {
        this.codMunicipio = codMunicipio;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getEmailVerificado() {
        return emailVerificado;
    }

    public void setEmailVerificado(Boolean emailVerificado) {
        this.emailVerificado = emailVerificado;
    }

    public Cine actualizaCamposRegistro(Cine cine) {
        cine.setCif(this.getCif());
        cine.setEmpresa(this.getEmpresa());
        cine.setCp(this.getCp());
        cine.setCodigoMunicipio(this.getCodMunicipio());
        cine.setNombreMunicipio(this.getMunicipio());
        cine.setTelefono(this.getTelefono());
        cine.setDireccion(this.getDireccion());

        return cine;
    }
}