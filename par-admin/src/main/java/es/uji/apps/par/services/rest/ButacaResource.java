package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.exceptions.ButacaOcupadaException;
import es.uji.apps.par.model.Butaca;
import es.uji.apps.par.services.ButacasService;
import es.uji.apps.par.utils.LocaleUtils;

@Path("butacas")
public class ButacaResource extends BaseResource {
    @InjectParam
    private ButacasService butacasService;

    @Context
    HttpServletResponse currentResponse;

    @GET
    @Path("{butacaId}/libres/{tarifaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getButacasLibres(
        @PathParam("butacaId") Long butacaId,
        @PathParam("tarifaId") String tarifaId,
        @QueryParam("sort") String sort,
        @QueryParam("start") int start,
        @QueryParam("limit") @DefaultValue("1000") int limit,
        @QueryParam("search") @DefaultValue("") String search
    ) throws IOException {
        List<Butaca> butacasDisponibles = butacasService
            .getButacasDisponibles(butacaId, tarifaId, LocaleUtils.getLocale(configurationSelector, currentRequest));

        return Response.ok().entity(new RestResponse(true, butacasDisponibles, butacasDisponibles.size())).build();
    }

    @POST
    @Path("{butacaId}/cambia/{fila}/{numero}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response cambiaButaca(
        @PathParam("butacaId") Long butacaId,
        @PathParam("fila") String fila,
        @PathParam("numero") String numero
    ) {
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        try {
            butacasService.cambiaFilaNumero(butacaId, fila, numero, locale.getLanguage());

            return Response.ok(butacaId).build();
        } catch (ButacaOcupadaException e) {
            return errorResponse("error.butacaOcupada", e.getLocalizacion(), e.getFila(),
                e.getNumero());
        }
    }

    @POST
    @Path("cambiatipo/{tipo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response cambiaButaca(
        @PathParam("tipo") String tipo,
        List<Long> idsButacas
        ) {
        if (idsButacas.size() > 0) {
            String userUID = AuthChecker.getUserUID(currentRequest);
            butacasService.cambiaTipo(idsButacas, tipo, userUID);
        }

        return Response.ok().build();
    }
}
