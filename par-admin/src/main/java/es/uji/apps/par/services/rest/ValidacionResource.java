package es.uji.apps.par.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.services.UsersService;

@Path("validacion")
public class ValidacionResource extends BaseResource {

    @InjectParam
    UsersService usersService;

    @GET
    @Path("{token}")
    public Response validacionToken(@PathParam("token") String tokenValidacion) throws URISyntaxException {
        String userUID = AuthChecker.getUserUID(currentRequest);
        usersService.validarToken(userUID, tokenValidacion);
        return Response.seeOther(new URI("/index")).build();
    }
}
