package es.uji.apps.par.services.rest;

import com.google.common.base.Strings;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataParam;

import org.apache.commons.io.IOUtils;
import org.jasypt.util.password.BasicPasswordEncryptor;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Locale;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.par.auth.AuthChecker;
import es.uji.apps.par.exceptions.GeneralPARException;
import es.uji.apps.par.exceptions.TamanyoImagenException;
import es.uji.apps.par.model.Cine;
import es.uji.apps.par.model.DatosIntegracion;
import es.uji.apps.par.model.DatosRegistro;
import es.uji.apps.par.model.Usuario;
import es.uji.apps.par.services.CineService;
import es.uji.apps.par.services.UsersService;
import es.uji.apps.par.services.VendedoresService;
import es.uji.apps.par.utils.LocaleUtils;
import es.uji.apps.par.utils.Utils;

import static es.uji.apps.par.filters.UploadFilter.MAX_BINARY_SIZE;

@Path("registro")
public class DatosRegistroResource extends BaseResource {

    @InjectParam
    private UsersService usersService;

    @InjectParam
    private CineService cineService;

    @InjectParam
    private VendedoresService vendedoresService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDatosRegistro() {
        String userUID = AuthChecker.getUserUID(currentRequest);
        Usuario userById = usersService.getUserById(userUID);
        Cine cine = usersService.getUserCineByUserUID(userUID);
        return Response.ok(new DatosRegistro(userById, cine)).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateDatosRegistro(DatosRegistro datosRegistro) {
        String userUID = AuthChecker.getUserUID(currentRequest);
        Cine cine = usersService.getUserCineByUserUID(userUID);

        String password = datosRegistro.getPassword();
        if (password != null && !password.trim().isEmpty()) {
            BasicPasswordEncryptor encryptor = new BasicPasswordEncryptor();
            usersService.updatePassword(userUID, encryptor.encryptPassword(password.trim()));
        }

        cineService.updateDatosRegistro(datosRegistro.actualizaCamposRegistro(cine));

        return Response.ok().entity(new RestResponse(true)).build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateImagen(
        @FormDataParam("logo") InputStream logoData,
        @FormDataParam("logo") FormDataContentDisposition logoDetail,
        @FormDataParam("logo") FormDataBodyPart logoBodyPart,
        @FormDataParam("banner") InputStream bannerData,
        @FormDataParam("banner") FormDataContentDisposition bannerDetail,
        @FormDataParam("banner") FormDataBodyPart bannerBodyPart
    ) throws GeneralPARException, IOException {
        if (logoDetail != null && !Strings.isNullOrEmpty(logoDetail.getFileName()) || bannerDetail != null && !Strings
            .isNullOrEmpty(bannerDetail.getFileName())) {
            String userUID = AuthChecker.getUserUID(currentRequest);
            Cine cine = usersService.getUserCineByUserUID(userUID);

            cine.setLogoSrc(logoDetail != null ? logoDetail.getFileName() : "");
            cine.setLogoContentType(logoBodyPart != null ? logoBodyPart.getMediaType().toString() : "");
            cine.setLogo(logoData != null ? IOUtils.toByteArray(logoData) : new byte[0]);

            cine.setBannerSrc(bannerDetail != null ? bannerDetail.getFileName() : "");
            cine.setBannerContentType(bannerBodyPart != null ? bannerBodyPart.getMediaType().toString() : "");
            cine.setBanner(bannerData != null ? IOUtils.toByteArray(bannerData) : new byte[0]);

            if (cine.getLogo().length > MAX_BINARY_SIZE || cine.getBanner().length > MAX_BINARY_SIZE) {
                throw new TamanyoImagenException();
            }

            cineService.updateCine(cine);
        }

        return Response.ok().entity(new RestResponse(true)).build();
    }

    @PUT
    @Path("resend")
    @Produces(MediaType.APPLICATION_JSON)
    public Response reenviarEmailVerificacion() {
        String userUID = AuthChecker.getUserUID(currentRequest);
        Locale locale = LocaleUtils.getLocale(configurationSelector, currentRequest);
        Cine cine = usersService.getUserCineByUserUID(userUID);
        Usuario usuario = usersService.getUserById(userUID);
        vendedoresService.enviarEmailValidacion(cine.getNombre(), usuario, configurationSelector.getUrlAdmin(), locale);
        return Response.ok().build();
    }

    @GET
    @Path("integracion")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDatosIntegracion() {
        String userUID = AuthChecker.getUserUID(currentRequest);
        Cine cine = usersService.getUserCineByUserUID(userUID);

        DatosIntegracion datosIntegracion = new DatosIntegracion();
        URI uri = URI.create(cine.getUrlPublic());
        datosIntegracion.setUrlConexion(String.format("%s://%s", uri.getScheme(), uri.getHost()));
        datosIntegracion.setPort(Utils.getPortFromURI(uri));
        datosIntegracion.setApiKey(cine.getApiKey());

        return Response.ok(datosIntegracion).build();
    }
}
