Ext.define('Paranimf.controller.InformesSesion', {
    extend: 'Ext.app.Controller',

    views: ['EditBaseGrid', 'informes.PanelInformesSesion', 'informes.GridSesionesInformes'],
    stores: ['SesionesInformes', 'TipoInformes'],
    models: ['Sesion', 'Informe'],

    refs: [{
        ref: 'fechaInicio',
        selector: 'panelInformesSesion datefield[name=fechaInicio]'
    }, {
        ref: 'fechaFin',
        selector: 'panelInformesSesion datefield[name=fechaFin]'
    }, {
        ref: 'gridSesionesInformes',
        selector: 'gridSesionesInformes'
    }, {
        ref: 'panelInformesCombo',
        selector: 'panelInformesSesion combo[name=comboInformesSesiones]'
    }],

    init: function () {
        this.control({
            'panelInformesSesion button[action=filtrarSessions]': {
                click: this.filtraSesiones
            },

            'panelInformesSesion button[action=generarInforme]': {
                click: this.generarInforme
            }
        });
    },

    generarInformeSesion: function (anuladas) {
        var idsSelected = this.getGridSesionesInformes().getSelectedColumnValues("id");
        if (idsSelected && idsSelected.length == 1) {
            this.generatePdfSesion(idsSelected[0], anuladas);
        } else
            alert(UI.i18n.message.selectRow);
    },

    generarInformeSesionOnline: function () {
        var idsSelected = this.getGridSesionesInformes().getSelectedColumnValues("id");
        if (idsSelected && idsSelected.length == 1) {
            this.generatePdfSesionOnline(idsSelected[0]);
        } else
            alert(UI.i18n.message.selectRow);
    },

    generarInformeEvento: function (anuladas) {
        var idsSelected = this.getGridSesionesInformes().getSelectedColumnValues("evento");
        if (idsSelected && idsSelected.length == 1) {
            this.generatePdfEvento(idsSelected[0].id, anuladas);
        } else
            alert(UI.i18n.message.selectRow);
    },

    generarInformeTipo: function (idSelected) {
        var sesionSelected = this.getGridSesionesInformes().getSelectedColumnValues("id");
        var eventoSelected = this.getGridSesionesInformes().getSelectedColumnValues("evento");
        if (sesionSelected && sesionSelected.length == 1 && eventoSelected && eventoSelected.length == 1) {
            this.generatePdf(eventoSelected[0].id, sesionSelected[0], idSelected);
        } else
            alert(UI.i18n.message.selectRow);
    },

    generarInforme: function () {
        var idSelected = this.getPanelInformesCombo().getValue();
        if (idSelected) {
            if (idSelected === 'informeSesion') {
                this.generarInformeSesion(true);
            } else if (idSelected === 'informeSesionNoAnuladas') {
                this.generarInformeSesion(false);
            } else if (idSelected === 'informeSesionOnline') {
                this.generarInformeSesionOnline();
            } else if (idSelected === 'informeEventos') {
                this.generarInformeEvento(true);
            } else if (idSelected === 'informeEventosNoAnuladas') {
                this.generarInformeEvento(false);
            } else {
                this.generarInformeTipo(idSelected);
            }
        } else
            alert(UI.i18n.message.selectInforme);
    },

    filtraSesiones: function () {
        if (!this.sonFechasValidas(this.getFechaInicio().value, this.getFechaFin().value))
            return;

        var storeSesiones = this.getGridSesionesInformes().getStore();
        storeSesiones.getProxy().url = urlPrefix + 'evento/sesionesficheros/todo';
        storeSesiones.getProxy().extraParams = {
            'fechaInicio': this.getFechaInicio().rawValue,
            'fechaFin': this.getFechaFin().rawValue
        };
        storeSesiones.load();
    },

    generatePdfSesion: function (idSesion, anuladas) {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", urlPrefix + 'report/sesion/' + idSesion + '/pdf' + (anuladas ? '' : '/noanuladas'));
        form.setAttribute("target", "_blank");

        document.body.appendChild(form);
        form.submit();
    },

    generatePdfSesionOnline: function (idSesion) {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", urlPrefix + 'report/sesion/' + idSesion + '/pdf/online');
        form.setAttribute("target", "_blank");

        document.body.appendChild(form);
        form.submit();
    },

    generatePdfEvento: function (idEvento, anuladas) {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", urlPrefix + 'report/evento/' + idEvento + '/pdf' + (anuladas ? '' : '/noanuladas'));
        form.setAttribute("target", "_blank");

        document.body.appendChild(form);
        form.submit();
    },

    generatePdf: function (idEvento, idSesion, idSelected) {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", urlPrefix + 'report/evento/' + idEvento + '/sesion/' + idSesion + '/pdf/' + idSelected);
        form.setAttribute("target", "_blank");

        document.body.appendChild(form);
        form.submit();
    },

    sonFechasValidas: function (fechaInicio, fechaFin) {
        if (!fechaInicio || fechaInicio == '' || !fechaFin || fechaFin == '') {
            alert(UI.i18n.error.fechasObligatorias);
            return false;
        }
        return true;
    }
});