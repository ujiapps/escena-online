Ext.define('Paranimf.controller.Informes', {
    extend: 'Ext.app.Controller',

    views: ['informes.PanelInformes'],
    stores: ['TiposInformesGenerales', 'SorterInformes', 'TiposInformesGeneralesSesion', 'SorterInformesSesion', 'TpvsInformes'],
    models: ['Informe', 'Sorter', 'Tpv'],

    refs: [{
        ref: 'fechaInicio',
        selector: 'panelInformes datefield[name=fechaInicio]'
    }, {
        ref: 'fechaFin',
        selector: 'panelInformes datefield[name=fechaFin]'
    }, {
        ref: 'comboInformesGenerales',
        selector: 'panelInformes combo[name=comboInformesGenerales]'
    }, {
        ref: 'comboSorterInformes',
        selector: 'panelInformes combo[name=comboSorterInformes]'
    }, {
        ref: 'fechaInicioSesion',
        selector: 'panelInformes datefield[name=fechaInicioSesion]'
    }, {
        ref: 'fechaFinSesion',
        selector: 'panelInformes datefield[name=fechaFinSesion]'
    }, {
        ref: 'comboInformesGeneralesSesion',
        selector: 'panelInformes combo[name=comboInformesGeneralesSesion]'
    }, {
        ref: 'comboSorterInformesSesion',
        selector: 'panelInformes combo[name=comboSorterInformesSesion]'
    }, {
        ref: 'comboTpv',
        selector: 'panelInformes combobox[name=tpv]'
    }, {
        ref: 'comboTpvSesiones',
        selector: 'panelInformes combobox[name=tpvSesiones]'
    }],

    init: function () {
        this.control({
            'panelInformes': {
                beforerender: this.loadTpvCombo
            },

            'panelInformes button[action=generarInformeGeneral]': {
                click: this.generarInformeGeneral
            },

            'panelInformes combo[name=comboInformesGenerales]': {
                change: this.changeTypeInforme
            },

            'panelInformes button[action=generarInformeGeneralSesion]': {
                click: this.generarInformeGeneralSesion
            },

            'panelInformes combo[name=comboInformesGeneralesSesion]': {
                change: this.changeTypeInformeSesion
            }
        });
    },

    loadTpvCombo: function () {
        this.getComboTpv().store.load();
        if (typeof multipleTpv !== "undefined") {
            if (null != multipleTpv && multipleTpv) {
                this.getComboTpv().show();
                this.getComboTpvSesiones().show();
            }
        }
        this.getComboTpv().setValue(-1);
        this.getComboTpvSesiones().setValue(-1);
    },

    changeTypeInforme: function (combo, newValue, oldValue, opts) {
        return this.changeTypeInformeBase(combo, newValue, this.getComboSorterInformes().getStore());
    },

    changeTypeInformeSesion: function (combo, newValue, oldValue, opts) {
        return this.changeTypeInformeBase(combo, newValue, this.getComboSorterInformesSesion().getStore());
    },

    changeTypeInformeBase: function (combo, newValue, store) {
        if (newValue != -1) {
            store.getProxy().url = urlPrefix + 'informe/sorters/' + newValue;
            store.load();
        }
    },

    generarInformeGeneral: function () {
        return this.generarInformeGeneralBase(this.getFechaInicio().value, this.getFechaFin().value, this.getComboInformesGenerales(), this.getComboSorterInformes(), this.getComboTpv());
    },

    generarInformeGeneralSesion: function () {
        return this.generarInformeGeneralBase(this.getFechaInicioSesion().value, this.getFechaFinSesion().value, this.getComboInformesGeneralesSesion(), this.getComboSorterInformesSesion(), this.getComboTpvSesiones());
    },

    generarInformeGeneralBase: function (fechaInicio, fechaFin, combo, comboSort, comboTpv) {
        if (!this.sonFechasValidas(fechaInicio, fechaFin))
            return;

        if (combo.value) {
            var rec = combo.store.getById(combo.value);
            var suffix = (rec.data.suffix != undefined && rec.data.suffix != '') ? rec.data.suffix : '';
            var prefix = (rec.data.prefix != undefined && rec.data.prefix != '') ? rec.data.prefix : '';
            var url = urlPrefix + 'report/' + prefix + this.getStrFecha(fechaInicio) + '/' +
                this.getStrFecha(fechaFin) + suffix + "?sort=" + comboSort.value + "&tpv=" + comboTpv.value;
            this.generateInformeGeneral(url);
        } else
            console.log("no entra");
    },

    generateInformeGeneral: function (url) {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", url);
        form.setAttribute("target", "_blank");

        document.body.appendChild(form);
        form.submit();
    },

    sonFechasValidas: function (fechaInicio, fechaFin) {
        if (!fechaInicio || fechaInicio == '' || !fechaFin || fechaFin == '') {
            alert(UI.i18n.error.fechasObligatorias);
            return false;
        }
        return true;
    },

    getStrFecha: function (fecha) {
        var dt = fecha;
        var mes = dt.getMonth() + 1;
        mes = (mes > 9) ? mes : '0' + mes;
        var dia = (dt.getDate() > 9) ? dt.getDate() : '0' + dt.getDate();
        return dt.getFullYear() + '-' + mes + '-' + dia;
    }
});