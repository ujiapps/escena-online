Ext.define('Paranimf.controller.Abonos', {
    extend: 'Ext.app.Controller',

    views: ['EditModalWindow', 'EditBaseForm', 'EditBaseGrid', 'abono.GridAbonos', 'abono.FormAbonos', 'abono.PanelAbonos', 'abono.GridAbonados'],
    stores: ['Abonos', 'Abonados', 'TpvsVisibles', 'LocalizacionesSalasNoNumeradas', 'LocalizacionesSalasNoNumeradas', 'TpvsVisibles', 'Horas24'],
    models: ['Abono', 'Abonado', 'Tpv', 'Localizacion', 'HoraMinuto'],

    refs: [{
        ref: 'gridAbonos',
        selector: 'gridAbonos'
    }, {
        ref: 'gridAbonados',
        selector: 'gridAbonados'
    }, {
        ref: 'formAbonos',
        selector: 'formAbonos'
    }, {
        ref: 'panelAbonos',
        selector: 'panelAbonos'
    }, {
        ref: 'idFormAbonos',
        selector: 'formAbonos textfield[name=id]'
    }, {
        ref: 'precio',
        selector: 'formAbonos numericfield[name=precio]'
    }, {
        ref: 'localizacion',
        selector: 'formAbonos combobox[name=localizacion]'
    }, {
        ref: 'precioAnticipado',
        selector: 'formAbonos numericfield[name=precioAnticipado]'
    }, {
        ref: 'checkboxVentaOnline',
        selector: 'formAbonos checkbox[name=canalInternet]'
    }, {
        ref: 'fechaInicioVentaOnline',
        selector: 'formAbonos datefield[name=fechaInicioVentaOnline]'
    }, {
        ref: 'fechaFinVentaOnline',
        selector: 'formAbonos datefield[name=fechaFinVentaOnline]'
    }, {
        ref: 'horaInicioVentaOnline',
        selector: 'formAbonos timefield[name=horaInicioVentaOnline]'
    }, {
        ref: 'horaFinVentaOnline',
        selector: 'formAbonos timefield[name=horaFinVentaOnline]'
    }, {
        ref: 'checkboxVentaAnticipada',
        selector: 'formAbonos checkbox[name=disponibleVentaAnticipada]'
    }, {
        ref: 'fechaFinVentaAnticipada',
        selector: 'formAbonos datefield[name=fechaFinVentaAnticipada]'
    }, {
        ref: 'horaFinVentaAnticipada',
        selector: 'formAbonos timefield[name=horaFinVentaAnticipada]'
    }, {
        ref: 'comboLocalizaciones',
        selector: 'formAbonos combobox[name=localizacion]'
    }, {
        ref: 'ventaAnticipada',
        selector: 'formAbonos fieldset[name=ventaAnticipada]'
    }, {
        ref: 'disponibleVentaAnticipada',
        selector: 'formAbonos checkboxfield[name=disponibleVentaAnticipada]'
    }
    ],

    init: function () {
        this.control({

            'gridAbonos button[action=add]': {
                click: this.addAbono
            },

            'gridAbonos button[action=edit]': {
                click: this.editAbono
            },

            'gridAbonos button[action=del]': {
                click: this.removeAbono
            },

            'gridAbonos': {
                selectionchange: this.loadAbonados
            },

            'gridAbonados button[action=del]': {
                click: this.removeAbonado
            },

            'panelAbonos': {
                beforeactivate: this.recargaStore
            },

            'formAbonos button[action=save]': {
                click: this.saveAbonoFormData
            },

            'formAbonos': {
                added: this.cargaStores
            },

            'formAbonos checkbox[name=canalInternet]': {
                change: this.enableDisableCanalInternet
            },

            'formAbonos checkbox[name=disponibleVentaAnticipada]': {
                change: this.enableDisableVentaAnticipada
            }
        });
    },

    cargaStores: function (comp, opts) {
        this.getComboLocalizaciones().store.proxy.url = urlPrefix + 'localizacion/nonumeradas';
        this.getComboLocalizaciones().store.load();
    },

    addAbono: function (button, event, opts) {
        this.getGridAbonos().showAddAbonoWindow();
    },

    editAbono: function (button, event, opts) {
        this.getGridAbonos().edit('formAbonos');
    },

    removeAbono: function (button, event, opts) {
        var abonado = this.getGridAbonados().getStore().data.length;
        if (abonado > 0) {
            alert(UI.i18n.error.tieneAbonados);
        } else {
            this.getGridAbonos().remove();
        }
    },

    removeAbonado: function (button, event, opts) {
        this.getGridAbonados().remove();
    },

    enableDisableCanalInternet: function (obj, newValue, oldValue) {
        this.getPrecio().setDisabled(!newValue);
        this.getLocalizacion().setDisabled(!newValue);
        this.getFechaInicioVentaOnline().setDisabled(!newValue);
        this.getHoraInicioVentaOnline().setDisabled(!newValue);
        this.getFechaFinVentaOnline().setDisabled(!newValue);
        this.getHoraFinVentaOnline().setDisabled(!newValue);
        this.getVentaAnticipada().setDisabled(!newValue);
        if (newValue) {
            this.getPrecio().show();
            this.getLocalizacion().show();
            this.getFechaInicioVentaOnline().show();
            this.getHoraInicioVentaOnline().show();
            this.getFechaFinVentaOnline().show();
            this.getHoraFinVentaOnline().show();
        } else {
            this.getDisponibleVentaAnticipada().setValue(newValue);
            this.getPrecio().hide();
            this.getLocalizacion().hide();
            this.getFechaInicioVentaOnline().hide();
            this.getHoraInicioVentaOnline().hide();
            this.getFechaFinVentaOnline().hide();
            this.getHoraFinVentaOnline().hide();
        }
    },

    enableDisableVentaAnticipada: function (obj, newValue, oldValue) {
        this.getFechaFinVentaAnticipada().setDisabled(!newValue);
        this.getHoraFinVentaAnticipada().setDisabled(!newValue);
        this.getPrecioAnticipado().setDisabled(!newValue);
        if (newValue) {
            this.getFechaFinVentaAnticipada().show();
            this.getHoraFinVentaAnticipada().show();
            this.getPrecioAnticipado().show();
        } else {
            this.getFechaFinVentaAnticipada().hide();
            this.getHoraFinVentaAnticipada().hide();
            this.getPrecioAnticipado().hide();
        }
    },

    saveAbonoFormData: function (button, event, opts) {
        var grid = this.getGridAbonos();
        var form = this.getFormAbonos();

        form.saveFormData(grid, urlPrefix + 'abono', undefined, 'multipart/form-data');
    },

    recargaStore: function (comp, opts) {
        this.getGridAbonos().recargaStore();
    },

    loadAbonados: function (selectionModel, record) {
        if (record[0]) {
            var storeAbonados = this.getGridAbonados().getStore();
            var abonoId = record[0].get("id");

            storeAbonados.getProxy().url = urlPrefix + 'abono/' + abonoId + '/abonados';
            storeAbonados.load();
        }
    }
});