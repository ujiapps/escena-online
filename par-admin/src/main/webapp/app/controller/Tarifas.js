Ext.define('Paranimf.controller.Tarifas', {
    extend: 'Ext.app.Controller',

    views: ['EditModalWindow', 'EditBaseForm', 'EditBaseGrid', 'tarifa.GridTarifas', 'tarifa.FormTarifas', 'tarifa.PanelTarifas'],
    stores: ['Tarifas', 'TarifasCombo'],
    models: ['Tarifa'],

    refs: [{
        ref: 'gridTarifas',
        selector: 'gridTarifas'
    }, {
        ref: 'formTarifas',
        selector: 'formTarifas'
    }],

    init: function () {
        this.control({

            'panelTarifas': {
                afterrender: this.recargaStore
            },

            'gridTarifas button[action=add]': {
                click: this.addTarifa
            },

            'gridTarifas button[action=edit]': {
                click: this.editTarifa
            },

            'gridTarifas button[action=del]': {
                click: this.removeTarifa
            },

            'gridTarifas': {
                beforeactivate: this.recargaStore
            },

            'formTarifas button[action=save]': {
                click: this.saveTarifaFormData
            }
        });
    },

    recargaStore: function (comp, opts) {
        console.log("RECARGA STORE TARIFAS");
        this.getGridTarifas().recargaStore();
    },

    addTarifa: function (button, event, opts) {
        this.getGridTarifas().showAddTarifaWindow();
    },

    editTarifa: function (button, event, opts) {
        this.getGridTarifas().edit('formTarifas');
    },

    removeTarifa: function (button, event, opts) {
        this.getGridTarifas().remove();
    },

    saveTarifaFormData: function (button, event, opts) {
        var grid = this.getGridTarifas();
        var form = this.getFormTarifas();
        form.saveFormData(grid, urlPrefix + 'tarifa');
    }
});