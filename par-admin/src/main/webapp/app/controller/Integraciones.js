Ext.define('Paranimf.controller.Integraciones', {
    extend: 'Ext.app.Controller',

    views: ['integraciones.PanelIntegraciones', 'integraciones.FormIntegraciones'],
    stores: [],
    models: ['DatosIntegracion'],

    refs: [{
        ref: 'formIntegraciones',
        selector: 'formIntegraciones'
    }, {
        ref: 'apiKey',
        selector: 'formIntegraciones textfield[name=apiKey]'
    }, {
        ref: 'urlConexion',
        selector: 'formIntegraciones textfield[name=urlConexion]'
    }, {
        ref: 'port',
        selector: 'formIntegraciones textfield[name=port]'
    }, {
        ref: 'btnDownload',
        selector: 'formIntegraciones button[action=download]'
    }, {
        ref: 'qr',
        selector: 'formIntegraciones container[name=qr]'
    }],

    init: function () {
        this.control({
            'formIntegraciones': {
                beforerender: this.onLoad
            },

            'formIntegraciones button[action=download]': {
                click: this.download
            }
        })
    },

    onLoad: function () {
        var qrImage = this.getQr();
        var formIntegraciones = this.getFormIntegraciones();
        formIntegraciones.setLoading(UI.i18n.message.loading);
        Ext.Ajax.request({
            url: urlPrefix + 'registro/integracion',
            method: 'GET',
            success: function (response) {
                var respuesta = Ext.JSON.decode(response.responseText, true);
                var datosIntegracion = Ext.create('Paranimf.model.DatosIntegracion', respuesta);
                formIntegraciones.loadRecord(datosIntegracion);
                qrImage.update('<img src="/par-public/rest/barcode?text=' + encodeURIComponent(datosIntegracion.data.qr) + '&size=150"/>');
                formIntegraciones.setLoading(false);
            }, failure: function (response) {
                formIntegraciones.setLoading(false);
            }
        });
    },

    download: function () {
        window.open('/par/resources/downloads/entradas.apk');
    }
});