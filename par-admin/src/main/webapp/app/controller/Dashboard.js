Ext.define('Paranimf.controller.Dashboard', {
    extend: 'Ext.app.Controller',

    views: ['dashboard.Dashboard'],

    init: function () {
        this.control({
            'viewport > panel[region=center] > panel[region=north] button[action=logout]': {
                click: function (button, opts) {
                    window.location = urlLogout;
                }
            },

            'viewport > panel[region=center] > panel[region=north] button[action=help]': {
                click: function (button, opts) {
                    var hash = window.location.hash;
                    hash = hash.substring(1, hash.length);
                    hopscotch.startTour(window['tour' + hash]);
                }
            }
        });
    }
});
