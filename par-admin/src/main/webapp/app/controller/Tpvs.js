Ext.define('Paranimf.controller.Tpvs', {
    extend: 'Ext.app.Controller',

    views: ['EditModalWindow', 'EditBaseForm', 'EditBaseGrid', 'tpv.GridTpvs', 'tpv.FormTpvs', 'tpv.PanelTpvs', 'tpv.FormTutorialTpv'],
    stores: ['Tpvs'],
    models: ['Tpv'],

    refs: [{
        ref: 'gridTpvs',
        selector: 'gridTpvs'
    }, {
        ref: 'formTpvs',
        selector: 'formTpvs'
    }, {
        ref: 'formTutorialTpv',
        selector: 'formTutorialTpv'
    },
        {
            ref: 'formTutorialTpvCards',
            selector: '#formTutorialTpvCards'
        }],

    init: function () {
        this.control({

            'panelTpvs': {
                afterrender: this.recargaStore
            },

            'gridTpvs button[action=add]': {
                click: this.showTutorialTpv
            },

            'gridTpvs button[action=edit]': {
                click: this.editTpv
            },

            'gridTpvs': {
                itemdblclick: this.editTpv,
                beforeactivate: this.recargaStore
            },

            'formTpvs button[action=save]': {
                click: this.saveTpvsFormData
            },

            'formTutorialTpv #tutorialAnterior': {
                click: this.tutorialAnterior
            },

            'formTutorialTpv #tutorialSiguiente': {
                click: this.tutorialSiguiente
            },

            'formTutorialTpv #tutorialCancelar': {
                click: this.cerrarTutorial
            },
        });
    },

    showTutorialTpv() {
        this.getGridTpvs().showTutorialWindow();
    },

    recargaStore: function (comp, opts) {
        this.getGridTpvs().recargaStore();
    },

    editTpv: function (button, event, opts) {
        this.getGridTpvs().edit('formTpvs');
    },

    saveTpvsFormData: function (button, event, opts) {
        var grid = this.getGridTpvs();
        var form = this.getFormTpvs();
        form.saveFormData(grid, urlPrefix + 'tpv');
    },

    cerrarTutorial: function () {
        this.getFormTutorialTpv().up('window').close();
    },

    tutorialAnterior: function () {
        var card = this.getFormTutorialTpvCards().getLayout();
        if (card.getPrev())
            card.prev();
    },

    tutorialSiguiente: function () {
        var card = this.getFormTutorialTpvCards().getLayout();
        if (card.getNext())
            card.next();
    }
});