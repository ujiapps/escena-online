Ext.define('Paranimf.controller.DatosRegistro', {
    extend: 'Ext.app.Controller',

    views: ['EditModalWindow', 'EditBaseForm', 'cine.PanelDatosRegistro', 'cine.FormDatosRegistro'],
    stores: [],
    models: [],

    refs: [{
        ref: 'panelDatosRegistro',
        selector: 'panelDatosRegistro'
    }, {
        ref: 'formDatosRegistro',
        selector: 'formDatosRegistro'
    }, {
        ref: 'passwordRepeat',
        selector: 'formDatosRegistro textfield[name=passwordRepeat]'
    }, {
        ref: 'password',
        selector: 'formDatosRegistro textfield[name=password]'
    }, {
        ref: 'subdominio',
        selector: 'formDatosRegistro label[name=subdominio]'
    }, {
        ref: 'emailTextField',
        selector: 'formDatosRegistro textfield[name=email]'
    }, {
        ref: 'btnEnviarMailVerificar',
        selector: 'formDatosRegistro button[action=resend]'
    }],

    init: function () {
        this.control({
            'panelDatosRegistro': {
                beforeactivate: this.onLoad
            },

            'formDatosRegistro textfield[name=passwordRepeat]': {
                change: this.onConfirmChange
            },

            'formDatosRegistro button[action=save]': {
                click: this.saveDatosRegistro
            },

            'formDatosRegistro button[action=resend]': {
                click: this.resendEmailVerificacion
            }
        })
    },

    onConfirmChange: function (object, value) {
        var inputPassword = this.getPassword();
        if (value !== inputPassword.value) {
            inputPassword.markInvalid(UI.i18n.error.passwordsNoCoinciden);
            inputPassword.setActiveError(UI.i18n.error.passwordsNoCoinciden);
        } else {
            inputPassword.clearInvalid();
        }
    },

    onLoad: function () {
        var formulario = this.getFormDatosRegistro();
        formulario.setLoading(UI.i18n.message.loading);
        var me = this;
        Ext.Ajax.request({
            url: urlPrefix + 'registro',
            method: 'GET',
            success: function (response) {
                var respuesta = Ext.JSON.decode(response.responseText, true);
                var cine = Ext.create('Paranimf.model.Cine', respuesta);
                formulario.loadRecord(cine);
                if (!cine.get('emailVerificado')) {
                    me.getEmailTextField().markInvalid(UI.i18n.error.emailNoVerificado);
                    me.getBtnEnviarMailVerificar().show();
                }
                formulario.setLoading(false);
            }, failure: function (response) {
                formulario.setLoading(false);
            }
        });
    },

    saveDatosRegistro: function (button, event, opts) {
        var formulario = this.getFormDatosRegistro();
        formulario.setLoading(UI.i18n.message.loading);
        var values = formulario.getValues();
        var password = values.password;
        var passwordC = values.passwordRepeat;
        if (password == passwordC && formulario.isValid()) {
            formulario.submit({
                method: 'POST',
                url: urlPrefix + 'registro',
                headers: {'Content-Type': 'multipart/form-data'},
                params: encodeURIComponent(Ext.JSON.encode(values)),
                success: function () {
                    Ext.Ajax.request({
                        url: urlPrefix + 'registro',
                        method: 'PUT',
                        jsonData: values,
                        success: function (response) {
                            formulario.setLoading(false);
                        }, failure: function (response) {
                            formulario.setLoading(false);
                        }
                    });
                }, failure: function (form, action) {
                    if (action != undefined && action.response != undefined && action.response.responseText != undefined) {
                        var respuesta = Ext.JSON.decode(action.response.responseText, true);
                        var key = "UI.i18n.error.error" + respuesta.codi;
                        var msg = eval(key);

                        if (msg != undefined)
                            alert(msg);
                        else
                            alert(UI.i18n.error.formSave);
                    }
                    formulario.setLoading(false);
                }
            });
        }
    },

    resendEmailVerificacion: function (button, event, opts) {
        Ext.Ajax.request({
            url: urlPrefix + 'registro/resend',
            method: 'PUT',
            success: function (response) {
                alert(UI.i18n.message.emailVerificacionEnviado);
            }, failure: function (response) {
            }
        });
    }
});