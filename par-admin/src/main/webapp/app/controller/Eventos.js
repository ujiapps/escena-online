Ext.define('Paranimf.controller.Eventos', {
    extend: 'Ext.app.Controller',

    views: ['EditModalWindow', 'EditBaseForm', 'EditBaseGrid', 'evento.GridEventos', 'evento.GridEventosMultisesion', 'evento.FormEventos', 'evento.PanelEventos', 'evento.GridSesiones', 'evento.FormSesiones', 'evento.GridPreciosSesion', 'evento.FormPreciosSesion', 'evento.FormPeliculaMultisesion'],
    stores: ['Eventos', 'EventosMultisesion', 'TiposEventosSinPaginar', 'Sesiones', 'PlantillasPrecios', 'PreciosSesion', 'Salas', 'SalasEvento', 'Nacionalidades', 'Peliculas', 'TpvsVisibles', 'Dias', 'Horas24'],
    models: ['Evento', 'Sesion', 'PrecioSesion', 'Sala', 'HoraMinuto', 'Tpv'],

    refs: [{
        ref: 'gridEventos',
        selector: 'gridEventos'
    }, {
        ref: 'gridEventosMultisesion',
        selector: 'gridEventosMultisesion'
    }, {
        ref: 'formEventos',
        selector: 'formEventos'
    }, {
        ref: 'panelEventos',
        selector: 'panelEventos'
    }, {
        ref: 'gridSesiones',
        selector: 'gridSesiones'
    }, {
        ref: 'formSesiones',
        selector: 'formSesiones'
    }, {
        ref: 'botonDeleteImagen',
        selector: 'formEventos button[action=deleteImage]'
    }, {
        ref: 'botonDeleteImagenPubli',
        selector: 'formEventos button[action=deleteImagePubli]'
    }, {
        ref: 'checkMultisesion',
        selector: 'formEventos checkbox[name=multisesion]'
    }, {
        ref: 'icaaMultisesionGridFieldset',
        selector: 'formEventos fieldset[name=icaaGrid]'
    }, {
        ref: 'icaaFieldset',
        selector: 'formEventos fieldset[name=icaa]'
    }, {
        ref: 'comboTpv',
        selector: 'formEventos combobox[name=tpv]'
    },
        {
            ref: 'entradasLimitadas',
            selector: 'formEventos checkbox[name=entradasLimitadas]'
        },{
            ref: 'entradasPorEmail',
            selector: 'formEventos numberfield[name=entradasPorEmail]'
        }, {
        ref: 'descripcionEs',
        selector: 'formEventos htmleditor[name=descripcionEs]'
    }, {
        ref: 'descripcionVa',
        selector: 'formEventos htmleditor[name=descripcionVa]'
    }, {
        ref: 'gridPreciosSesion',
        selector: 'gridPreciosSesion'
    }, {
        ref: 'columnPrecioAnticipado',
        selector: 'gridPreciosSesion gridcolumn[dataIndex=precioAnticipado]'
    }, {
        ref: 'formPreciosSesion',
        selector: 'formPreciosSesion'
    }, {
        ref: 'sesionCine',
        selector: 'formSesiones fieldset[name=sesionCine]'
    }, {
        ref: 'comboSala',
        selector: 'formSesiones combobox[name=sala]'
    }, {
        ref: 'comboLocalizaciones',
        selector: 'formPreciosSesion combobox[name=localizacion_id]'
    }, {
        ref: 'comboTarifas',
        selector: 'formPreciosSesion combobox[name=tarifa_id]'
    }, {
        ref: 'fieldPrecioAnticipado',
        selector: 'formPreciosSesion numericfield[name=precioAnticipado]'
    }, {
        ref: 'checkboxVentaAnticipada',
        selector: 'formSesiones checkbox[name=disponibleVentaAnticipada]'
    }, {
        ref: 'checkboxVentaOnline',
        selector: 'formSesiones checkbox[name=canalInternet]'
    }, {
        ref: 'fechaInicioVentaOnline',
        selector: 'formSesiones datefield[name=fechaInicioVentaOnline]'
    }, {
        ref: 'fechaFinVentaOnline',
        selector: 'formSesiones datefield[name=fechaFinVentaOnline]'
    }, {
        ref: 'horaInicioVentaOnline',
        selector: 'formSesiones timefield[name=horaInicioVentaOnline]'
    }, {
        ref: 'horaFinVentaOnline',
        selector: 'formSesiones timefield[name=horaFinVentaOnline]'
    }, {
        ref: 'fieldsetVentaOnline',
        selector: 'formSesiones fieldset[name=ventaOnline]'
    }, {
        ref: 'fechaFinVentaAnticipada',
        selector: 'formSesiones datefield[name=fechaFinVentaAnticipada]'
    }, {
        ref: 'horaFinVentaAnticipada',
        selector: 'formSesiones timefield[name=horaFinVentaAnticipada]'
    }, {
        ref: 'fechaCelebracion',
        selector: 'formSesiones datefield[name=fechaCelebracion]'
    }, {
        ref: 'horaCelebracion',
        selector: 'formSesiones timefield[name=horaCelebracion]'
    }, {
        ref: 'comboPlantillaPrecios',
        selector: 'formSesiones combobox[name=plantillaPrecios]'
    }, {
        ref: 'comboVersionLinguistica',
        selector: 'formSesiones combobox[name=versionLinguistica]'
    }, {
        ref: 'comboVersionLinguisticaMultisesion',
        selector: 'formPeliculaMultisesion combobox[name=versionLinguistica]'
    }, {
        ref: 'comboPeliculas',
        selector: 'formPeliculaMultisesion combobox[name=peliculas]'
    }, {
        ref: 'formPeliculaMultisesion',
        selector: 'formPeliculaMultisesion'
    }, {
        ref: 'jsonEventosMultisesion',
        selector: 'formEventos hiddenfield[name=jsonEventosMultisesion]'
    }, {
        ref: 'checkMostrarTodosEventos',
        selector: 'gridEventos checkbox[name=mostrarTodos]'
    }, {
        ref: 'checkMostrarTodasSesiones',
        selector: 'gridSesiones checkbox[name=mostrarTodas]'
    }, {
        ref: 'checkMultiplesSesiones',
        selector: 'formSesiones checkbox[name=multiplesSesiones]'
    }, {
        ref: 'inputDateFechaCelebracion',
        selector: 'formSesiones datefield[name=fechaCelebracion]'
    }, {
        ref: 'inputDateFechaInicio',
        selector: 'formSesiones datefield[name=fechaInicio]'
    }, {
        ref: 'inputDateFechaFin',
        selector: 'formSesiones datefield[name=fechaFin]'
    }, {
        ref: 'comboDias',
        selector: 'formSesiones combobox[name=dias]'
    }, {
        ref: 'comboHorasCierreOnline',
        selector: 'formSesiones combobox[name=horasCierreOnline]'
    }],

    init: function () {
        this.control({

            'gridEventos button[action=add]': {
                click: this.addEvento
            },

            'gridEventos button[action=edit]': {
                click: this.editEvento
            },

            'panelEventos': {
                beforeactivate: this.recargaStore
            },

            'formEventos button[action=save]': {
                click: this.saveEventoFormData
            },

            'formEventos button[action=deleteImage]': {
                click: this.deleteImage
            },

            'formEventos button[action=deleteImagePubli]': {
                click: this.deleteImagePubli
            },

            'formEventos': {
                beforerender: this.showImagenIfExists,
                afterrender: this.preparaEvento
            },

            'formEventos checkbox[name=multisesion]': {
                change: this.showMultisesion
            },

            'gridEventos': {
                selectionchange: this.loadSesiones
            },

            'gridSesiones': {
                itemdblclick: this.editSesion
            },

            'gridSesiones button[action=add]': {
                click: this.addSesion
            },

            'gridSesiones button[action=edit]': {
                click: this.editSesion
            },

            'gridSesiones button[action=del]': {
                click: this.anulaSesion
            },

            'formSesiones': {
                afterrender: this.preparaStoresSesiones
            },

            'formSesiones button[action=save]': {
                click: this.saveSesionFormData
            },

            'formSesiones combobox[name=plantillaPrecios]': {
                change: this.cambiaPlantilla
            },

            'formSesiones combobox[name=sala]': {
                change: this.cambiaSala
            },

            'gridPreciosSesion button[action=add]': {
                click: this.addPrecioSesion
            },

            'gridPreciosSesion button[action=del]': {
                click: this.deletePrecioSesion
            },

            'gridPreciosSesion button[action=edit]': {
                click: this.editPrecioSesion
            },

            'formPreciosSesion': {
                added: this.cargaStores
            },

            'formPreciosSesion button[action=save]': {
                click: this.savePrecioSesion
            },

            'formSesiones checkbox[name=canalInternet]': {
                change: this.enableDisableCanalInternet
            },

            'formSesiones checkbox[name=disponibleVentaAnticipada]': {
                change: this.enableDisableVentaAnticipada
            },

            'formEventos gridEventosMultisesion button[action=add]': {
                click: this.showAddEventoMultisesion
            },

            'formEventos gridEventosMultisesion button[action=del]': {
                click: this.deleteEventoMultisesion
            },

            'formEventos checkbox[name=entradasLimitadas]': {
                change: this.showHideLimiteEntradas
            },

            'formPeliculaMultisesion button[action=save]': {
                click: this.addPeliculaMultisesion
            },

            'gridEventos checkbox[name=mostrarTodos]': {
                change: this.showHideTodosEventos
            },

            'gridSesiones checkbox[name=mostrarTodas]': {
                change: this.showHideTodasSesiones
            },

            'editmodalwindow button[action=help]': {
                click: this.mostrarAyuda
            },

            'formSesiones checkbox[name=multiplesSesiones]': {
                change: this.showHideFormMultiplesSesiones

            }
        });
    },

    showHideTodosEventos: function () {
        this.getGridEventos().store.proxy.url = urlPrefix + 'evento';
        this.getGridEventos().store.proxy.extraParams = {
            activos: (!this.getCheckMostrarTodosEventos().checked).toString()
        };
        this.recargaStore();
    },

    showHideTodasSesiones: function () {
        var idEvento = this.getGridEventos().getSelectedRecord().data["id"];
        if (idEvento != undefined) {
            this.getGridSesiones().store.proxy.url = urlPrefix + 'evento/' + idEvento + '/sesiones';
            this.getGridSesiones().store.proxy.extraParams = {
                activos: (!this.getCheckMostrarTodasSesiones().checked).toString()
            };
            this.getGridSesiones().deseleccionar();
            this.getGridSesiones().store.loadPage(1);
        }
    },

    actualizaHoraApertura: function (obj, newValue, oldValue) {
        if (newValue != undefined && newValue != '') {
            var d = new Date(newValue.getTime() - 30 * 60000);
            Ext.ComponentQuery.query('formSesiones timefield[name=horaApertura]')[0].setValue(d);
        }
    },

    enableDisableCanalInternet: function (obj, newValue, oldValue) {
        var isMultipleSesion = this.getCheckMultiplesSesiones().value;

        this.getFechaInicioVentaOnline().setDisabled(!newValue);
        this.getHoraInicioVentaOnline().setDisabled(!newValue);
        this.getFechaFinVentaOnline().setDisabled(isMultipleSesion || !newValue);
        this.getHoraFinVentaOnline().setDisabled(isMultipleSesion || !newValue);
        this.getComboHorasCierreOnline().setDisabled(!isMultipleSesion || !newValue);
        if (newValue) {
            this.getFechaInicioVentaOnline().show();
            this.getHoraInicioVentaOnline().show();
            if (isMultipleSesion) {
                this.getComboHorasCierreOnline().show();
            } else {
                this.getFechaFinVentaOnline().show();
                this.getHoraFinVentaOnline().show();
            }
        } else {
            this.getFechaInicioVentaOnline().hide();
            this.getHoraInicioVentaOnline().hide();
            if (isMultipleSesion) {
                this.getComboHorasCierreOnline().hide();
            } else {
                this.getFechaFinVentaOnline().hide();
                this.getHoraFinVentaOnline().hide();
            }
        }
    },

    enableDisableVentaAnticipada: function (obj, newValue, oldValue) {
        this.getFechaFinVentaAnticipada().setDisabled(!newValue);
        this.getHoraFinVentaAnticipada().setDisabled(!newValue);
        if (newValue) {
            this.getFechaFinVentaAnticipada().show();
            this.getHoraFinVentaAnticipada().show();
            this.getColumnPrecioAnticipado().show();
        } else {
            this.getFechaFinVentaAnticipada().hide();
            this.getHoraFinVentaAnticipada().hide();
            this.getColumnPrecioAnticipado().hide();
        }
    },

    deletePrecioSesion: function (button, event, opts) {
        if (button.up('form').getForm().findField('plantillaPrecios').value == -1) {
            this.getGridPreciosSesion().remove();
        }
    },

    editPrecioSesion: function (button, event, opts) {
        if (this.getComboSala().value != undefined && this.getComboSala().value != '' && this.getComboSala().value != 0) {
            this.getGridPreciosSesion().edit('formPreciosSesion', undefined, 0.4);
            this.getFieldPrecioAnticipado().setVisible(this.getCheckboxVentaAnticipada().value);
        } else {
            alert(UI.i18n.error.salaObligatoria);
        }
    },

    cambiaPlantilla: function (combo, newValue, oldValue, opts) {
        if (this.getComboSala().value != undefined && this.getComboSala().value != '' && this.getComboSala().value != 0) {
            if (newValue == -1) {
                this.getGridPreciosSesion().mostrarToolbar();
                this.getGridPreciosSesion().vaciar();

                if (oldValue === undefined) {
                    this.cargarPreciosSesion();
                }
            } else if (combo.isValid() && combo.value != null) {
                this.getGridPreciosSesion().ocultarToolbar();
                this.cargarPreciosPlantilla(newValue);
            }
        }
    },

    cambiaSala: function (combo, newValue, oldValue, opts) {
        this.getGridPreciosSesion().vaciar();
        if (combo.isValid() && combo.value != null) {
            if (newValue != undefined && newValue != '' && newValue != 0) {
                this.getComboPlantillaPrecios().store.proxy.url = urlPrefix + 'plantillaprecios/sala/' + newValue;
            } else {
                this.getComboPlantillaPrecios().store.proxy.url = urlPrefix + 'plantillaprecios';
            }

            var self = this;
            this.getComboPlantillaPrecios().store.load(function (records, operation, success) {
                var plantillaId = undefined;
                if (oldValue === undefined && self.getGridSesiones().getSelectedColumnId() != undefined) {
                    var selectedRecord = self.getGridSesiones().getSelectedRecord();
                    plantillaId = selectedRecord.data.plantillaPrecios;
                    self.getComboPlantillaPrecios().setValue(plantillaId);
                    if (plantillaId == -1) {
                        self.cargarPreciosSesion();
                    }
                    else {
                        self.cargarPreciosPlantilla(plantillaId);
                    }
                } else {
                    self.getComboPlantillaPrecios().setValue(-1);
                }
            });
        }
    },

    cargarPreciosPlantilla: function (plantillaId) {
        var gridPreciosSesion = this.getGridPreciosSesion();
        Ext.Ajax.request({
            url: urlPrefix + 'plantillaprecios/' + plantillaId + '/precios',
            method: 'GET',
            success: function (response) {
                var respuesta = Ext.JSON.decode(response.responseText, true);
                gridPreciosSesion.store.loadRawData(respuesta.data);
            }, failure: function (response) {
                alert(UI.i18n.error.loadingPrecios);
            }
        });
    },

    cargarPreciosSesion: function () {
        var gridPreciosSesion = this.getGridPreciosSesion();

        if (this.getGridEventos().hasRowSelected() && this.getGridSesiones().hasRowSelected()) {
            var idEvento = this.getGridEventos().getSelectedRecord().data["id"];
            var idSesion = this.getGridSesiones().getSelectedRecord().data["id"];

            if (idEvento != undefined && idSesion != undefined) {
                gridPreciosSesion.store.proxy.url = urlPrefix + 'compra/' + idSesion + '/precios';
                gridPreciosSesion.store.load(function (records, operation, success) {
                    if (!success)
                        alert(UI.i18n.error.loadingPrecios);
                })
            }
        }
    },

    cargaStores: function (comp, opts) {
        this.getComboLocalizaciones().store.loadData([], false);
        this.getComboLocalizaciones().store.proxy.url = urlPrefix + 'localizacion?sala=' + this.getComboSala().value;
        this.getFormPreciosSesion().cargaComboStore('localizacion_id');
        this.getFormPreciosSesion().recargaComboStore('tarifa_id');
    },

    addPrecioSesion: function (button, event, opts) {
        if (this.getComboSala().value != undefined && this.getComboSala().value != '' && this.getComboSala().value != 0) {
            this.getGridPreciosSesion().deseleccionar();
            this.getGridPreciosSesion().createPercentageModalWindow('formPreciosSesion', 0.4).show();
            this.getFieldPrecioAnticipado().setVisible(this.getCheckboxVentaAnticipada().value);
        } else
            alert(UI.i18n.error.salaObligatoria);
    },

    existeRecordConLocalizacionYTarifaElegida: function (indexFilaSeleccionada, localizacionSeleccionada, tarifaSeleccionada) {
        var encontrado = false;
        for (var i = 0; i < this.getGridPreciosSesion().store.count(); i++) {
            var record = this.getGridPreciosSesion().store.getAt(i);
            if (record.data.localizacion == localizacionSeleccionada && record.data.tarifa == tarifaSeleccionada)
                encontrado = true;
        }
        return encontrado;
    },

    savePrecioSesion: function (button, event, opts) {
        var indiceFilaSeleccionada = this.getGridPreciosSesion().getIndiceFilaSeleccionada();
        var localizacionSeleccionada = this.getComboLocalizaciones().value;
        var tarifaSeleccionada = this.getComboTarifas().value;

        if (!this.existeRecordConLocalizacionYTarifaElegida(indiceFilaSeleccionada, localizacionSeleccionada, tarifaSeleccionada)) {
            var precioSesion = new Ext.create('Paranimf.model.PrecioSesion', {
                plantillaPrecios: -1,
                precio: button.up('form').getForm().findField('precio').value,
                precioAnticipado: button.up('form').getForm().findField('precioAnticipado').value,
                localizacion_id: localizacionSeleccionada,
                "parLocalizacione.nombreVa": this.getComboLocalizaciones().rawValue,
                "parLocalizacione.nombreEs": this.getComboLocalizaciones().rawValue,
                tarifa_id: tarifaSeleccionada,
                "parTarifa.nombre": this.getComboTarifas().rawValue
            });

            if (indiceFilaSeleccionada != -1) {
                var recordSeleccionado = this.getGridPreciosSesion().store.getAt(indiceFilaSeleccionada);
                recordSeleccionado.set('precio', precioSesion.data.precio);
                recordSeleccionado.set('precioAnticipado', precioSesion.data.precioAnticipado);
                recordSeleccionado.set('localizacion_id', precioSesion.data.localizacion_id);
                recordSeleccionado.set('parLocalizacione.nombreVa', precioSesion.data["parLocalizacione.nombreVa"]);
                recordSeleccionado.set('tarifa_id', precioSesion.data.tarifa_id);
                recordSeleccionado.set('parTarifa.nombre', precioSesion.data["parTarifa.nombre"]);
                recordSeleccionado.commit(true);
            } else
                this.getGridPreciosSesion().store.add(precioSesion);

            this.getGridPreciosSesion().getView().refresh();
            button.up('window').close();
        } else {
            alert(UI.i18n.message.registroConMismaLocalizacionExiste)
        }
    },

    preparaStoresSesiones: function (comp, opts) {
        this.getGridPreciosSesion().getStore().removeAll(true);
        this.preparaStoreSalas(this.getGridEventos().getSelectedRecord().data.id);

        if (!this.getGridEventos().getSelectedRecord().data.parTiposEvento.exportarICAA) {
            this.getSesionCine().hide();
            this.getComboVersionLinguistica().allowBlank = true;
            this.getComboVersionLinguistica().setFieldLabel(UI.i18n.field.versionLinguistica);
        } else {
            this.getSesionCine().show();
            this.getComboVersionLinguistica().allowBlank = false;
            this.getComboVersionLinguistica().setFieldLabel(UI.i18n.field.versionLinguistica + ' <span class="req" style="color:red">*</span>');

            if (this.getGridEventos().getSelectedRecord().data.multisesion) {
                this.getComboVersionLinguistica().allowBlank = true;
                this.getComboVersionLinguistica().hide();
            }
        }
        if (this.getFormSesiones().getRecord() != null) {
            this.getCheckMultiplesSesiones().hide();
            this.getCheckMultiplesSesiones().disable();
        }

        if (this.getComboPlantillaPrecios().value === -1) {
            this.getGridPreciosSesion().mostrarToolbar();
        }
        else {
            this.getGridPreciosSesion().ocultarToolbar();
        }
    },

    preparaEvento: function (comp, opts) {
        if (typeof multipleTpv !== "undefined") {
            if (null != multipleTpv && multipleTpv) {
                this.getComboTpv().store.load();
                this.getComboTpv().show();
                this.getComboTpv().allowBlank = false;
                this.getComboTpv().setFieldLabel(UI.i18n.field.tpv + ' <span class="req" style="color:red">*</span>');
            }
        }
    },

    preparaStoreSalas: function (eventoId) {
        var salaId = undefined;
        if (this.getGridSesiones().getSelectedColumnId() != undefined) {
            var selectedRecord = this.getGridSesiones().getSelectedRecord();
            salaId = selectedRecord.data.sala;
        }
        this.getFormSesiones().recargaComboStore('sala', salaId, urlPrefix + 'sala/evento/' + eventoId);
    },

    showImagenIfExists: function (comp, opts) {
        var record = comp.getRecord();

        if (record != undefined && record.data["imagenSrc"]) {
            var idEvento = record.data["id"];

            var imagen = comp.down("#imagenInsertada");
            imagen.html = '<a href="' + urlPrefix + 'evento/' + idEvento + '/imagen" target="blank">' + UI.i18n.field.imagenInsertada + '</a>';

            var imagenPubli = comp.down("#imagenPubliInsertada");
            imagenPubli.html = '<a href="' + urlPrefix + 'evento/' + idEvento + '/imagenPubli" target="blank">' + UI.i18n.field.imagenPubliInsertada + '</a>';

            if (!readOnlyUser) {
                this.getBotonDeleteImagen().show();
                this.getBotonDeleteImagenPubli().show();
            }

        } else {
            this.getBotonDeleteImagen().hide();
            this.getBotonDeleteImagenPubli().hide();
        }
    },

    showMultisesion: function (comp, newValue, oldValue, eOpts) {
        if (!allowMultisesion)
            newValue = false;

        if (newValue) {
            this.getIcaaFieldset().hide();
            this.getIcaaMultisesionGridFieldset().show();
        } else {
            this.getGridEventosMultisesion().store.removeAll();
            this.getIcaaMultisesionGridFieldset().hide();
            if (icaa != undefined && icaa) {
                this.getIcaaFieldset().show();
            }
        }
    },

    deleteImage: function (button, event, opts) {
        if (confirm(UI.i18n.message.sureDeleteImage)) {
            var record = button.up('form').getRecord();
            var idEvento = record.data["id"];
            var grid = this.getGridEventos();

            Ext.Ajax.request({
                url: urlPrefix + 'evento/' + idEvento + '/imagen',
                method: 'DELETE',
                success: function (response) {
                    alert(UI.i18n.message.deletedImage);

                    button.up('window').close();
                    grid.store.load();
                }, failure: function (response) {
                    alert(UI.i18n.error.deletedImage);
                }
            });
        }
    },

    deleteImagePubli: function (button, event, opts) {
        if (confirm(UI.i18n.message.sureDeleteImagePubli)) {
            var record = button.up('form').getRecord();
            var idEvento = record.data["id"];
            var grid = this.getGridEventos();

            Ext.Ajax.request({
                url: urlPrefix + 'evento/' + idEvento + '/imagenPubli',
                method: 'DELETE',
                success: function (response) {
                    alert(UI.i18n.message.deletedImagePubli);

                    button.up('window').close();
                    grid.store.load();
                }, failure: function (response) {
                    alert(UI.i18n.error.deletedImagePubli);
                }
            });
        }
    },

    recargaStore: function (comp, opts) {
        this.getGridEventos().deseleccionar();
        this.getGridSesiones().store.proxy.url = urlPrefix + 'evento/-1/sesiones';
        this.getGridSesiones().store.loadPage(1);
        this.getGridEventos().store.loadPage(1);
    },

    addEvento: function (button, event, opts) {
        this.getGridEventos().showAddEventoWindow();
        this.showMultisesion(null, false);
    },

    editEvento: function (button, event, opts) {
        var isMultisesion = false;
        if (allowMultisesion) {
            if (this.getGridEventos().hasRowSelected()) {
                var rec = this.getGridEventos().getSelectedRecord();
                if (rec && rec.data && rec.data.multisesion)
                    isMultisesion = (rec.data.multisesion != undefined && rec.data.multisesion == 'on') ? true : false;
            }
        }
        this.getGridEventos().edit('formEventos', undefined, undefined, 0.8);

        if (this.getGridEventos().hasRowSelected()) {
            var rec = this.getGridEventos().getSelectedRecord();
            if (rec && rec.data) {
                this.getDescripcionEs().setValue(rec.data.descripcionEs);
                this.getDescripcionVa().setValue(rec.data.descripcionVa);
            }
        }
        this.showMultisesion(null, isMultisesion);

        if (isMultisesion)
            this.recargaGridMultisesion(this.getGridEventos().getSelectedColumnId());
    },

    recargaGridMultisesion: function (idEventoPadre) {
        var url = urlPrefix + 'evento/' + idEventoPadre + '/peliculas';
        this.getGridEventosMultisesion().store.removeAll();
        this.getGridEventosMultisesion().store.proxy.url = url;
        this.getGridEventosMultisesion().store.load();
    },

    saveEventoFormData: function (button, event, opts) {
        var limit = 15000;
        if ((this.getDescripcionEs().getValue() && this.getDescripcionEs().getValue().length > limit) || (this.getDescripcionVa().getValue() && this.getDescripcionVa().getValue().length > limit)) {
            alert(UI.i18n.message.maxDescription);
        }
        else {
            var grid = this.getGridEventos();
            var form = this.getFormEventos();
            this.getJsonEventosMultisesion().setValue(this.getGridEventosMultisesion().toJSON());
            form.saveFormData(grid, urlPrefix + 'evento', undefined, 'multipart/form-data');
        }
    },

    loadSesiones: function (selectionModel, record) {
        if (record[0]) {
            this.showHideTodasSesiones();
        }
    },

    addSesion: function (button, event, opts) {
        if (this.getGridEventos().getSelectedColumnId()) {
            this.getGridSesiones().deseleccionar();
            this.getGridSesiones().showAddSesionWindow();
            this.getHoraCelebracion().addListener('change', this.actualizaHoraApertura);
        } else
            alert(UI.i18n.message.event);
    },

    saveSesionFormData: function (button, event, opts) {
        var form = this.getFormSesiones();
        var eventoId = this.getGridEventos().getSelectedColumnId();

        if (form.isValid()) {
            var sala = this.getFormSesiones().getForm().findField('sala');

            if (this.getCheckboxVentaOnline().value) {
                var dataInicialVentaOnline = this.getFechaInicioVentaOnline().value;
                var h = this.getHoraInicioVentaOnline().rawValue.split(":");
                dataInicialVentaOnline.setHours(h[0], h[1]);

                var valorSesionId = this.getFormSesiones().getForm().findField('id').value;
                var sesionId = '&sesionid=' + valorSesionId;

                if (this.getCheckMultiplesSesiones().getValue() == false) {
                    var dataSessio = this.getFechaCelebracion().value;
                    var fechasCorrectas = this.comprobarFechasVentaOnlineSinPeriodicas(dataInicialVentaOnline, h, dataSessio);
                    if (!fechasCorrectas) {
                        return;
                    }

                    var strDataSessio = Ext.Date.format(dataSessio, 'd/m/Y H:i');
                    this.comprobarSesiones(eventoId, sala, strDataSessio, sesionId, this.guardarSesion, this);
                } else {
                    var fechasCorrectas = this.comprobarFechasVentaOnlinePeriodicas(dataInicialVentaOnline, h);
                    if (!fechasCorrectas) {
                        return;
                    }

                    this.comprobarSesionesPeriodicas(eventoId, sala, sesionId, h, this.guardarSesion, this);
                }
            } else {
                this.guardarSesion(this, eventoId)
            }
        }
    },

    comprobarFechasVentaOnlineSinPeriodicas: function (dataInicialVentaOnline, h, dataSessio) {
        var dataFinalVentaOnline = this.getFechaFinVentaOnline().getValue();
        h = this.getHoraFinVentaOnline().rawValue.split(":");
        var fechaFinVentaOnlineConHoras = new Date(dataFinalVentaOnline.getTime());
        fechaFinVentaOnlineConHoras.setHours(h[0], h[1]);

        if (dataInicialVentaOnline > fechaFinVentaOnlineConHoras) {
            alert(UI.i18n.error.dataIniciPosterior);
            return false;
        }
        ;

        h = this.getHoraCelebracion().rawValue.split(":");
        var fechaSesionConHoras = new Date(dataSessio.getTime());
        fechaSesionConHoras.setHours(h[0], h[1]);

        if (fechaSesionConHoras < fechaFinVentaOnlineConHoras) {
            alert(UI.i18n.error.dataEventAnterior);
            return false;
        }

        return true;
    },

    comprobarFechasVentaOnlinePeriodicas: function (dataIniciVentaOnline, h) {
        var horaCelebracion = this.getHoraCelebracion().getRawValue(),
            fechaInicioPeriodicidad = this.getInputDateFechaInicio().getValue(),
            fechaFinPeriodicidad = this.getInputDateFechaFin().getValue(),
            fechaInicioVentaOnline = this.getFechaInicioVentaOnline().getValue(),
            horaInicioVentaOnline = this.getHoraInicioVentaOnline().getRawValue(),
            dias = this.getComboDias().getValue(),
            fechaPrimeraSesion = null;

        while (fechaInicioPeriodicidad < fechaFinPeriodicidad && fechaPrimeraSesion == null) {

            if (dias.includes(this.getSpanishDay(fechaInicioPeriodicidad))) {
                fechaPrimeraSesion = fechaInicioPeriodicidad;
            }


            if (fechaPrimeraSesion == null) {
                var d = new Date(fechaInicioPeriodicidad);
                d.setDate(fechaInicioPeriodicidad.getDate() + 1);
                fechaInicioPeriodicidad = d;
            }
        }

        if (fechaPrimeraSesion == null) {
            alert(UI.i18n.error.noHaySesionesEnEstasFechas);
            return false;
        }

        var fechaPrimeraSesionConHora = new Date(fechaPrimeraSesion.getTime());
        var h = horaCelebracion.split(":");
        fechaPrimeraSesionConHora.setHours(h[0], h[1]);

        var fechaInicioVentaOnlineConHora = new Date(fechaInicioVentaOnline.getTime());
        var hInicioVentaOnline = horaInicioVentaOnline.split(":");
        fechaInicioVentaOnlineConHora.setHours(hInicioVentaOnline[0], hInicioVentaOnline[1]);

        var fechaFinVentaOnlineConHora = new Date(fechaPrimeraSesionConHora.getTime());
        fechaFinVentaOnlineConHora.setHours(fechaPrimeraSesionConHora.getHours() - 2);

        if (fechaInicioVentaOnlineConHora > fechaFinVentaOnlineConHora) {
            alert(UI.i18n.error.inicioVentaOnlineMayourQueFinVentaOnlinePrimeraSesion);
            return false;
        }

        return true;
    },

    getSpanishDay: function (date) {
        if (date.getDay() == 0) {
            return 7;
        }
        return date.getDay();
    },

    comprobarSesionesPeriodicas: function (eventoId, sala, sesionId, h, callback, scope) {
        var sesionesSolapadas = false,
            form = this.getFormSesiones();

        Ext.Ajax.request({
            url: urlPrefix + 'evento/' + eventoId + '/sesionesensala?sala=' + sala.getValue() + sesionId,
            method: 'POST',
            jsonData: form.getValues(),
            scope: scope,
            success: function (response) {
                var numeroSesionesMismaHora = Ext.JSON.decode(response.responseText, true);

                if (numeroSesionesMismaHora.first > 0) {
                    sesionesSolapadas = true;
                    alert(UI.i18n.message.existeSesion);
                }
                if (!sesionesSolapadas) {
                    callback(scope, eventoId);
                }
            }
        });
    },

    comprobarSesiones: function (eventoId, sala, strDataSessio, sesionId, callback, scope) {
        Ext.Ajax.request({
            url: urlPrefix + 'evento/' + eventoId + '?sala=' + sala.getValue() + '&fecha=' + strDataSessio + sesionId,
            method: 'GET',
            scope: scope,
            success: function (response) {
                var numeroSesionesMismaHora = Ext.JSON.decode(response.responseText, true);
                var guardar = true;

                if (numeroSesionesMismaHora.first > 0) {
                    if (numeroSesionesMismaHora.second > 0) {
                        guardar = false;
                        if (valorSesionId)
                            alert(UI.i18n.message.recordatorioDevolverEntradas);
                        else
                            alert(UI.i18n.message.existeSesionEnMismaSalaYHoraConVentasOReservas);
                    } else {
                        if (!confirm(UI.i18n.message.confirmReprogramacio))
                            guardar = false;
                    }
                }

                if (guardar) {
                    callback(scope, eventoId);
                }
            }, failure: function (response) {
                alert(UI.i18n.error.comprobandoSesionesMismaHora);
            }
        });
    },

    guardarSesion: function (scope, eventoId, anulaConMismaHoraYSala, cambiaFechaConCompras) {
        var form = scope.getFormSesiones();
        var grid = scope.getGridSesiones();
        var sala = form.getForm().findField('sala');

        if (scope.getComboPlantillaPrecios().value == -1)
            form.getForm().findField('preciosSesion').setValue(scope.getGridPreciosSesion().toJSON());
        else
            form.getForm().findField('preciosSesion').setValue(undefined);

        if (sala.getValue() == '')
            sala.setValue(null);

        var url = 'evento/' + eventoId + '/sesiones';
        var queryParams = '';
        if (scope.getCheckMultiplesSesiones().getValue() == true || anulaConMismaHoraYSala == true) {
            if (scope.getCheckMultiplesSesiones().getValue() == true) {
                queryParams += 'createmultiples=true';
            }
            if (anulaConMismaHoraYSala == true) {
                queryParams += (queryParams.length > 1 ? '&' : '') + 'anulaConMismaHoraYSala=true';
            }
        }

        if (cambiaFechaConCompras === true) {
            queryParams += (queryParams.length > 1 ? '&' : '') + 'cambiaFechaConCompras=true';
        }

        form.saveFormData(grid, urlPrefix + url, undefined, undefined, function (response) {
            var error = Ext.JSON.decode(response.responseText, true);
            if (error != null && error.codi != null) {
                if (error.codi == 515) {
                    alert(UI.i18n.error.sesionCompras);
                }
                else if (error.codi == 583) {
                    if (confirm(UI.i18n.message.sesionCompras)) {
                        scope.guardarSesion(scope, eventoId, anulaConMismaHoraYSala, true);
                    }
                } else if (error.codi == 565) {
                    if (confirm(UI.i18n.message.existeSesionEnMismaSalaYHora)) {
                        scope.guardarSesion(scope, eventoId, true, cambiaFechaConCompras);
                    }
                } else {
                    alert(UI.i18n.error.formSave);
                }
            } else
                alert(UI.i18n.error.formSave);
        }, queryParams);
    },

    anulaSesion: function (button, event, opts) {
        this.getGridSesiones().remove(undefined, UI.i18n.message.sureDelete);
    },

    editSesion: function (button, event, opts) {
        if (this.getGridEventos().hasRowSelected() && this.getGridSesiones().hasRowSelected()) {
            var rec = this.getGridSesiones().getSelectedRecord();
            if (!rec.data.anulada) {
                this.getGridSesiones().edit('formSesiones', undefined, undefined, 0.8);
                this.getHoraCelebracion().addListener('change', this.actualizaHoraApertura);
            }
            else
                alert(UI.i18n.error.editSesionAnulada);
        } else
            alert(UI.i18n.error.eventSelected);
    },

    deleteEventoMultisesion: function () {
        this.getGridEventosMultisesion().remove();
    },

    showAddEventoMultisesion: function () {
        this.getGridEventosMultisesion().showAddPelicula();
    },

    addPeliculaMultisesion: function () {
        if (this.getComboPeliculas().value != undefined) {
            if (this.getFormPeliculaMultisesion().isValid()) {
                if (!this.getGridEventosMultisesion().containsId(this.getComboPeliculas().value)) {
                    var record = {
                        id: this.getComboPeliculas().value,
                        tituloEs: this.getComboPeliculas().rawValue,
                        versionLinguistica: this.getComboVersionLinguisticaMultisesion().value
                    };
                    this.getGridEventosMultisesion().addItemToStore(record);
                    this.getFormPeliculaMultisesion().up("window").close();
                } else
                    alert(UI.i18n.error.peliculaJaAfegida);
            }
        }
    },

    mostrarAyuda: function () {
        hopscotch.startTour(tourEventosPopUp);
    },

    showHideLimiteEntradas: function (control, newValue, oldValue) {
        this.getEntradasPorEmail().setDisabled(!newValue);
        this.getEntradasPorEmail().setVisible(newValue);
    },

    showHideFormMultiplesSesiones: function (control, newValue, oldValue) {
        var isVentaOnlineEnabled = this.getCheckboxVentaOnline().value;

        var inputDateFechaCelebracion = this.getInputDateFechaCelebracion(),
            inputDateFechaInicio = this.getInputDateFechaInicio(),
            inputDateFechaFin = this.getInputDateFechaFin(),
            combodias = this.getComboDias(),
            comboHorasCierreOnline = this.getComboHorasCierreOnline(),
            fechaFinVentaOnline = this.getFechaFinVentaOnline(),
            horaFinVentaOnline = this.getHoraFinVentaOnline();

        inputDateFechaCelebracion.setDisabled(newValue);
        inputDateFechaInicio.setDisabled(!newValue);
        inputDateFechaFin.setDisabled(!newValue);
        combodias.setDisabled(!newValue);
        comboHorasCierreOnline.setDisabled(!newValue || !isVentaOnlineEnabled);
        fechaFinVentaOnline.setDisabled(newValue || !isVentaOnlineEnabled);
        horaFinVentaOnline.setDisabled(newValue || !isVentaOnlineEnabled);

        if (newValue == true) {
            inputDateFechaCelebracion.hide();
            inputDateFechaInicio.show();
            inputDateFechaFin.show();
            if (isVentaOnlineEnabled == true) {
                combodias.show();
                comboHorasCierreOnline.show();
                fechaFinVentaOnline.hide();
                horaFinVentaOnline.hide();
            }
        } else {
            inputDateFechaCelebracion.show();
            inputDateFechaInicio.hide();
            inputDateFechaFin.hide();
            if (isVentaOnlineEnabled == true) {
                combodias.hide();
                comboHorasCierreOnline.hide();
                fechaFinVentaOnline.show();
                horaFinVentaOnline.show();
            }
        }
    }
});