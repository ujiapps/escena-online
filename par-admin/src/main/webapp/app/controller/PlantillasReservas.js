Ext.define('Paranimf.controller.PlantillasReservas', {
    extend: 'Ext.app.Controller',

    views: ['EditModalWindow', 'EditBaseForm', 'EditBaseGrid', 'plantillareservas.PanelPlantillasReservas', 'plantillareservas.GridPlantillasReservas', 'plantillareservas.FormPlantillasReservas', 'plantillareservas.FormButacas'],
    stores: ['SalasNumeradas', 'PlantillasReservas'],
    models: ['PlantillaPrecios', 'Butaca'],

    refs: [{
        ref: 'gridPlantillasReservas',
        selector: 'gridPlantillasReservas'
    }, {
        ref: 'formPlantillas',
        selector: 'formPlantillasReservas'
    }, {
        ref: 'panelPlantillas',
        selector: 'panelPlantillasReservas'
    }, {
        ref: 'formButacas',
        selector: 'formButacas'
    }],

    init: function () {
        this.control({

            'gridPlantillasReservas button[action=add]': {
                click: this.addPlantilla
            },

            'gridPlantillasReservas button[action=edit]': {
                click: this.editPlantilla
            },

            'gridPlantillasReservas button[action=del]': {
                click: this.removePlantilla
            },

            'panelPlantillasReservas': {
                beforeactivate: this.recargaStore
            },

            'formPlantillasReservas button[action=save]': {
                click: this.savePlantillaFormData
            },

            'gridPlantillasReservas': {
                itemdblclick: this.editPlantilla
            },

            'gridPlantillasReservas button[action=editButacas]': {
                click: this.editButacas
            },

            'formButacas button[action=save]': {
                click: this.saveButacas
            }
        });
    },

    recargaStore: function (comp, opts) {
        console.log("RECARGA STORE PLANTILLASRESERVAS");
        this.getGridPlantillasReservas().getStore().load();
    },

    addPlantilla: function (button, event, opts) {
        this.getGridPlantillasReservas().showAddPlantillaWindow();
    },

    editPlantilla: function (button, event, opts) {
        this.getGridPlantillasReservas().edit('formPlantillasReservas');
    },

    removePlantilla: function (button, event, opts) {
        this.getGridPlantillasReservas().remove();
    },

    savePlantillaFormData: function (button, event, opts) {
        var grid = this.getGridPlantillasReservas();
        var form = this.getFormPlantillas();
        form.saveFormData(grid, urlPrefix + 'plantillareservas');
    },

    editButacas: function (button, event, opts) {
        var grid = this.getGridPlantillasReservas();
        grid.edit('formButacas', undefined, 0.7, 0.7);

        var plantillaId = grid.getSelectedColumnId();
        var url = urlPublic + "/rest/entrada/butacasFragment/reservas/" + plantillaId;
        Ext.getDom('iframeButacasReservas').src = url;
    },

    saveButacas: function () {
        var me = this;

        me.getFormButacas().setLoading(UI.i18n.message.saving);
        // Llamamos al iframe de butacas para que nos pase las butacas seleccionadas
        pm({
            target: window.frames[window.frames.length - 1],
            type: 'butacas',
            data: {},
            success: function (butacas) {
                var plantillaId = me.getGridPlantillasReservas().getSelectedColumnId();
                Ext.Ajax.request({
                    url: urlPrefix + 'plantillareservas/' + plantillaId + '/butacas',
                    method: 'POST',
                    jsonData: {
                        butacasSeleccionadas: butacas
                    },
                    success: function (response) {
                        me.getFormButacas().setLoading(false);
                        me.getFormButacas().up('window').close();
                    }, failure: function (response) {
                        me.getFormButacas().setLoading(false);
                        alert(UI.i18n.error.formSave);
                    }
                });

            }
        });
    }
});