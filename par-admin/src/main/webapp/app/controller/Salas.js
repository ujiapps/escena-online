Ext.define('Paranimf.controller.Salas', {
    extend: 'Ext.app.Controller',

    views: ['EditModalWindow', 'EditBaseForm', 'EditBaseGrid', 'sala.PanelSalas', 'sala.GridSalas', 'sala.FormSalas', 'sala.GridLocalizaciones', 'sala.FormLocalizaciones'],
    stores: ['SalasNoNumeradas', 'LocalizacionesSalasNoNumeradas'],
    models: ['Sala', 'Localizacion'],

    refs: [{
        ref: 'gridSalas',
        selector: 'gridSalas'
    },
        {
            ref: 'gridLocalizaciones',
            selector: 'gridLocalizaciones'
        },
        {
            ref: 'formSalas',
            selector: 'formSalas'
        },
        {
            ref: 'formLocalizaciones',
            selector: 'formLocalizaciones'
        }
    ],

    init: function () {
        this.control({
            'gridSalas button[action=add]': {
                click: this.addSala
            },

            'gridSalas': {
                selectionchange: this.loadLocalizaciones
            },

            'gridSalas button[action=edit]': {
                click: this.editSala
            },

            'formSalas button[action=save]': {
                click: this.saveSalaFormData
            },

            'gridSalas button[action=del]': {
                click: this.removeSala
            },

            'gridLocalizaciones button[action=add]': {
                click: this.addLocalizacion
            },

            'gridLocalizaciones button[action=edit]': {
                click: this.editLocalizacion
            },

            'formLocalizaciones button[action=save]': {
                click: this.saveLocalizacionFormData
            },

            'gridLocalizaciones button[action=del]': {
                click: this.removeLocalizaciones
            }
        })
    },

    addSala: function (button, event, opts) {
        this.getGridSalas().showAddSalaWindow();
    },

    addLocalizacion: function (button, event, opts) {
        var idSala = this.getGridSalas().getSelectedRecord().data["id"];
        if (idSala != undefined) {
            this.getGridLocalizaciones().store.proxy.url = urlPrefix + 'localizacion/?sala=' + idSala;
        }
        this.getGridLocalizaciones().showAddLocalizacionesWindow();
    },

    loadLocalizaciones: function (selectionModel, record) {
        if (record[0]) {
            this.showHideTodasLocalizaciones();
        }
    },

    showHideTodasLocalizaciones: function () {
        var idSala = this.getGridSalas().getSelectedRecord().data["id"];
        if (idSala != undefined) {
            this.getGridLocalizaciones().store.proxy.url = urlPrefix + 'localizacion/?sala=' + idSala;
            this.getGridLocalizaciones().deseleccionar();
            this.getGridLocalizaciones().store.loadPage(1);
        }
    },

    saveSalaFormData: function (button, event, opts) {
        var grid = this.getGridSalas();
        var form = this.getFormSalas();
        form.saveFormData(grid, urlPrefix + 'sala');
    },

    saveLocalizacionFormData: function (button, event, opts) {
        var grid = this.getGridLocalizaciones();
        var form = this.getFormLocalizaciones();
        var gridSalas = this.getGridSalas();
        var record = gridSalas.getSelectedRecord();
        var idSala = record.data["id"];
        var url = "";
        if (form.getValues()['id']) {
            url = urlPrefix + 'localizacion'
        } else {
            url = urlPrefix + 'localizacion/?sala=' + idSala;
        }
        form.saveFormData(grid, url);
    },

    removeSala: function (button, event, opts) {
        var gridSalas = this.getGridSalas();
        var gridLocalizaciones = this.getGridLocalizaciones();
        gridSalas.store.proxy.url = urlPrefix + 'sala';

        var records = gridSalas.getSelectionModel().getSelection();
        if (records.length == 0) {
            alert(UI.i18n.message.selectRow);
            if (callback)
                callback(false);
        } else {
            if (confirm(UI.i18n.message.sureDelete)) {
                var st = gridSalas.store;
                if (!gridSalas.store.getProxy().hasListener('exception')) {
                    gridSalas.store.getProxy().on('exception', function (proxy, resp, operation) {
                        var respuesta = Ext.JSON.decode(resp.responseText, true);
                        var key = "UI.i18n.error.error" + respuesta.codi;
                        var msg = eval(key);

                        if (msg != undefined)
                            alert(msg);
                        st.rejectChanges();
                    });
                }
                gridSalas.store.remove(records);
                gridSalas.getStore().sync();
                gridLocalizaciones.getStore().removeAll();
            }
        }
    },

    editSala: function (button, event, opts) {
        this.getGridSalas().edit('formSalas', undefined, undefined, undefined);
    },

    editLocalizacion: function (button, event, opts) {
        this.getGridLocalizaciones().edit('formLocalizaciones', undefined, undefined, undefined);
    },

    removeLocalizaciones: function (button, event, opts) {
        var gridLocalizaciones = this.getGridLocalizaciones();
        gridLocalizaciones.store.proxy.url = urlPrefix + 'localizacion';
        gridLocalizaciones.remove(function (success) {
            if (success) {
                gridLocalizaciones.getStore().sync();
            }
        });
    }
});