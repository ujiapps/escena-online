Ext.define('Paranimf.controller.Clientes', {
    extend: 'Ext.app.Controller',

    views: ['EditModalWindow', 'EditBaseGrid', 'EditBaseForm', 'cliente.GridClientes', 'cliente.FormMails'],
    stores: ['Clientes'],
    models: ['Cliente'],

    refs: [{
        ref: 'gridClientes',
        selector: 'gridClientes'
    }, {
        ref: 'formMails',
        selector: 'formMails'
    }, {
        ref: 'mailsText',
        selector: 'formMails textarea[name=mails]'
    }, {
        ref: 'mailsPuntoComaText',
        selector: 'formMails textarea[name=mailsPuntoComa]'
    }],

    init: function () {
        this.control({

            'gridClientes button[action=copyEmails]': {
                click: this.copyEmails
            },

            'gridClientes button[action=delClient]': {
                click: this.delClient
            },

            'gridClientes': {
                beforeactivate: this.recargaStore
            },

            'formMails': {
                afterrender: this.getMails
            }
        });
    },

    recargaStore: function (comp, opts) {
        console.log("RECARGA STORE CLIENTES");
        this.getGridClientes().recargaStore();
    },

    copyEmails: function (button, event, opts) {
        this.getGridClientes().createPercentageModalWindow('formMails').show();
    },

    delClient: function () {
        if (!this.getGridClientes().hasRowSelected())
            alert(UI.i18n.message.selectRow);
        else {
            var me = this;
            Ext.Ajax.request({
                url: urlPrefix + 'clientes/' + this.getGridClientes().getSelectedRecord().data["email"],
                method: 'DELETE',
                success: function (response) {
                    me.getGridClientes().store.load();
                }, failure: function (response) {
                    me.getGridClientes().store.load();
                }
            });
        }
    },

    getMails: function () {
        var me = this;
        Ext.Ajax.request({
            url: urlPrefix + 'clientes/mails?filter=' + this.getGridClientes().store.proxy.encodeFilters(this.getGridClientes().store.filters.items) + '&sort=' + this.getGridClientes().store.proxy.encodeSorters(this.getGridClientes().store.sorters.items),
            method: 'GET',
            success: function (response) {
                var respuesta = Ext.JSON.decode(response.responseText, true);
                me.getMailsText().setValue(respuesta.data.join(","));
                me.getMailsPuntoComaText().setValue(respuesta.data.join(";"));
            }, failure: function (response) {
                me.getFormMails().up("window").close();
                alert(UI.i18n.error.loadingPrecios);
            }
        });
    }
});