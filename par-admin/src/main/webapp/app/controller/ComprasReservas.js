Ext.define('Paranimf.controller.ComprasReservas', {
    extend: 'Ext.app.Controller',

    views: ['EditModalWindow', 'EditBaseForm', 'EditBaseGrid', 'compra.PanelComprasReservas', 'compra.GridEventosComprasReservas', 'compra.GridSesionesComprasReservas', 'compra.PanelCompras', 'compra.FormCambiaButaca', 'compra.FormCambiaTarifa', 'compra.GridCompras', 'compra.GridDetalleCompras', 'compra.FormFormasDePago'],
    stores: ['Compras', 'EventosCompra', 'SesionesCompra', 'ButacasCompra', 'ButacasLibres', 'TarifasLocalizacionCombo'],
    models: ['Compra', 'Evento', 'Sesion', 'Butaca'],

    refs: [
        {
            ref: 'gridCompras',
            selector: 'gridCompras'
        },
        {
            ref: 'gridDetalleCompras',
            selector: 'gridDetalleCompras'
        },
        {
            ref: 'panelCompras',
            selector: 'panelCompras'
        },
        {
            ref: 'botonVerCompras',
            selector: 'formComprar #verCompras'
        },
        {
            ref: 'botonExportarCSV',
            selector: 'formComprar #exportarCSV'
        },
        {
            ref: 'gridEventosComprasReservas',
            selector: 'gridEventosComprasReservas'
        },
        {
            ref: 'gridSesionesComprasReservas',
            selector: 'gridSesionesComprasReservas'
        }, {
            ref: 'formCambiaButaca',
            selector: 'formCambiaButaca'
        }, {
            ref: 'idCambiaButaca',
            selector: 'formCambiaButaca textfield[name=id]'
        }, {
            ref: 'comboButacasLibres',
            selector: 'formCambiaButaca combo[name=butaca]'
        }, {
            ref: 'formCambiaTarifa',
            selector: 'formCambiaTarifa'
        }, {
            ref: 'comboTarifas',
            selector: 'formCambiaTarifa combo[name=tarifa]'
        }, {
            ref: 'checkboxAnuladas',
            selector: 'gridCompras checkbox[action=showAnuladas]'
        }, {
            ref: 'checkboxOnline',
            selector: 'gridCompras checkbox[action=showOnline]'
        }, {
            ref: 'pagingToolbar',
            selector: 'gridCompras pagingtoolbar'
        }, {
            ref: 'btAnular',
            selector: 'gridCompras [action=anular]'
        }, {
            ref: 'btDesanular',
            selector: 'gridCompras [action=desanular]'
        }, {
            ref: 'btCambiarFormaPago',
            selector: 'gridCompras [action=cambiarFormaPago]'
        }, {
            ref: 'btPassToCompra',
            selector: 'gridCompras [action=passToCompra]'
        }, {
            ref: 'btAnularDetalle',
            selector: 'gridDetalleCompras [action=anular]'
        }, {
            ref: 'btCambiarButacaDetalle',
            selector: 'gridDetalleCompras [action=anular]'
        }, {
            ref: 'pagingToolbarDetalle',
            selector: 'gridDetalleCompras pagingtoolbar'
        }, {
            ref: 'formFormasDePago',
            selector: 'formFormasDePago'
        }, {
            ref: 'comboTipoPago',
            selector: 'formFormasDePago combobox[name=tipoPago]'
        }, {
            ref: 'txtReciboPago',
            selector: 'formFormasDePago textfield[name=referenciaDePago]'
        }, {
            ref: 'txtEmail',
            selector: 'formFormasDePago textfield[name=email]'
        }, {
            ref: 'txtNombre',
            selector: 'formFormasDePago textfield[name=nombre]'
        }, {
            ref: 'txtApellidos',
            selector: 'formFormasDePago textfield[name=apellidos]'
        }, {
            ref: 'fechaCompra',
            selector: 'formFormasDePago datefield[name=fechaCompra]'
        }, {
            ref: 'horaCompra',
            selector: 'formFormasDePago textfield[name=horaCompra]'
        }, {
            ref: 'checkMostrarTodosEventos',
            selector: 'gridEventosComprasReservas checkbox[name=mostrarTodos]'
        }, {
            ref: 'checkMostrarTodasSesiones',
            selector: 'gridSesionesComprasReservas checkbox[name=mostrarTodas]'
        }
    ],

    init: function () {
        this.control({
            'panelComprasReservas': {
                beforeactivate: this.loadEventos
            },

            'gridEventosComprasReservas': {
                selectionchange: this.loadSesiones
            },

            'gridSesionesComprasReservas': {
                itemdblclick: this.verCompras
            },

            'gridSesionesComprasReservas [action=verCompras]': {
                click: this.verCompras
            },

            'gridSesionesComprasReservas [action=exportarCSV]': {
                click: this.exportarCSV
            },

            'gridSesionesComprasReservas [action=exportarCSVDetail]': {
                click: this.exportarCSVDetail
            },

            'gridSesionesComprasReservas [action=exportarCSVComprasDetail]': {
                click: this.exportarCSVComprasDetail
            },

            'panelCompras [action=close]': {
                click: this.cerrarVerCompras
            },

            'panelCompras': {
                afterrender: this.loadCompras,
                beforerender: this.hideColumns
            },

            'gridCompras [action=anular]': {
                click: this.anularCompraReserva
            },

            'gridCompras [action=desanular]': {
                click: this.desanularCompraReserva
            },

            'gridCompras [action=cambiarFormaPago]': {
                click: this.cambiarFormaPago
            },

            'gridCompras [action=passToCompra]': {
                click: this.passarReservaACompra
            },

            'gridCompras': {
                selectionchange: this.loadButacas
            },

            'gridCompras checkbox[action=showAnuladas]': {
                change: this.showAnuladas
            },

            'gridCompras checkbox[action=showOnline]': {
                change: this.showOnline
            },

            'gridCompras pagingtoolbar': {
                beforechange: this.doRefresh
            },

            'gridDetalleCompras [action=cambiar]': {
                click: this.cambiaButaca
            },

            'gridDetalleCompras [action=cambiarTarifa]': {
                click: this.cambiaTarifa
            },

            'gridDetalleCompras [action=anular]': {
                click: this.anularButacas
            },

            'gridDetalleCompras [action=passButacaToCompra]': {
                click: this.passarButacaReservaACompra
            },

            'formCambiaButaca': {
                afterrender: this.butacasDisponibles
            },

            'formCambiaButaca [action=save]': {
                click: this.saveCambiaButaca
            },

            'formCambiaTarifa': {
                afterrender: this.tarifasDisponibles
            },

            'formCambiaTarifa [action=save]': {
                click: this.saveCambiaTarifaButaca
            },

            'formFormasDePago combobox[name=tipoPago]': {
                change: this.changeFormaPago
            },

            'editmodalwindow [action=help]': {
                click: this.mostrarAyuda
            },

            'formFormasDePago': {
                afterrender: this.onLoadFormaPago
            },

            'gridEventosComprasReservas checkbox[name=mostrarTodos]': {
                change: this.showHideTodosEventos
            },

            'gridSesionesComprasReservas checkbox[name=mostrarTodas]': {
                change: this.showHideTodasSesiones
            }
        });
    },

    showHideTodosEventos: function () {
            var url = (this.getCheckMostrarTodosEventos().checked) ? urlPrefix + 'evento/taquilla' : urlPrefix + 'evento/taquilla?activos=true';
            this.getGridEventosComprasReservas().store.proxy.url = url;
            this.recargaStore();
    },

    showHideTodasSesiones: function () {
        var idEvento = this.getGridEventosComprasReservas().getSelectedRecord().data["id"];
        if (idEvento != undefined) {
            this.getGridSesionesComprasReservas().store.proxy.url = urlPrefix + 'evento/' + idEvento + '/sesiones';
            this.getGridSesionesComprasReservas().store.proxy.extraParams = {
                activos: (!this.getCheckMostrarTodasSesiones().checked).toString()
            };
            this.getGridSesionesComprasReservas().store.filter('multisesion', false);
            this.getGridSesionesComprasReservas().deseleccionar();
            this.getGridSesionesComprasReservas().store.loadPage(1);
        }
    },

    recargaStore: function (comp, opts) {
        this.getGridEventosComprasReservas().deseleccionar();
        this.getGridSesionesComprasReservas().store.proxy.url = urlPrefix + 'evento/-1/sesiones';
        this.getGridSesionesComprasReservas().store.loadPage(1);
        this.getGridEventosComprasReservas().store.loadPage(1);
    },

    hideColumns: function () {
        this.getGridCompras().columns[0].setVisible(false);
        this.getGridCompras().columns[1].setVisible(false);
        this.getGridCompras().columns[5].setVisible(false);
        this.getGridCompras().columns[6].setVisible(false);
        this.getGridCompras().columns[7].setVisible(false);
        this.getGridCompras().columns[8].setVisible(false);
        this.getGridCompras().columns[9].setVisible(false);
        this.getGridCompras().columns[10].setVisible(false);
        this.getGridCompras().columns[11].setVisible(false);
        this.getGridCompras().columns[12].setVisible(false);
        this.getGridCompras().columns[21].setVisible(false);
    },

    doRefresh: function () {
        this.setStoreCompras();
    },

    showAnuladas: function (checkbox, newValue, oldValue) {
        this.setStoreCompras();
        this.getPagingToolbar().moveFirst();
    },

    showOnline: function (checkbox, newValue, oldValue) {
        this.setStoreCompras();
        this.getPagingToolbar().moveFirst();
    },

    anularCompraReserva: function () {
        if (!this.getGridCompras().hasRowSelected())
            alert(UI.i18n.message.selectRow);
        else {
            if (confirm(UI.i18n.message.sureAnular)) {
                var idSesion = this.getGridSesionesComprasReservas().getSelectedColumnId();
                var idCompra = this.getGridCompras().getSelectedColumnId();
                var me = this;
                this.getGridCompras().setLoading(UI.i18n.message.loading);

                Ext.Ajax.request({
                    url: urlPrefix + 'compra/' + idSesion + '/' + idCompra,
                    method: 'PUT',
                    success: function (response) {
                        me.setStoreCompras();
                        me.getGridCompras().setLoading(false);
                        me.getGridCompras().deseleccionar();
                        me.getGridCompras().getStore().load();
                    }, failure: function (response) {

                        me.getGridCompras().setLoading(false);

                        var respuesta = Ext.JSON.decode(response.responseText, true);
                        var msg = UI.i18n.error["error" + respuesta.codi];

                        if (msg != undefined)
                            alert(msg);
                        else
                            alert(UI.i18n.error.anularCompraReserva);
                    }
                });
            }
        }
    },

    desanularCompraReserva: function () {
        if (!this.getGridCompras().hasRowSelected())
            alert(UI.i18n.message.selectRow);
        else {
            if (confirm(UI.i18n.message.sureDesanular)) {
                var idSesion = this.getGridSesionesComprasReservas().getSelectedColumnId();
                var idCompra = this.getGridCompras().getSelectedColumnId();
                var me = this;
                this.getGridCompras().setLoading(UI.i18n.message.loading);

                Ext.Ajax.request({
                    url: urlPrefix + 'compra/' + idSesion + '/desanuladas/' + idCompra,
                    method: 'PUT',
                    success: function (response) {
                        me.setStoreCompras();
                        me.getGridCompras().setLoading(false);
                        me.getGridCompras().deseleccionar();
                        me.getGridCompras().getStore().load();
                    }, failure: function (response) {

                        me.getGridCompras().setLoading(false);

                        var resp = Ext.JSON.decode(response.responseText, true);

                        if (resp['message'])
                            alert(resp['message']);
                        else
                            alert(UI.i18n.error.desanularCompraReserva);
                    }
                });
            }
        }
    },

    cambiarFormaPago: function () {
        if (!this.getGridCompras().hasRowSelected())
            alert(UI.i18n.message.selectRow);
        else {
            var selectedRecord = this.getGridCompras().getSelectedRecord();

            if (!selectedRecord.data.reserva && !selectedRecord.data.anulada && selectedRecord.data.taquilla) {
                var self = this;
                this.getGridDetalleCompras().showFormasDePagoWindow(function () {
                    self.doCambiarFormaPago();
                });
                this.getComboTipoPago().setValue(selectedRecord.data.tipo);
                this.getTxtReciboPago().setValue(selectedRecord.data.idDevolucion);
                this.getTxtEmail().setValue(selectedRecord.data.email);
                this.getTxtNombre().setValue(selectedRecord.data.nombre);
                this.getTxtApellidos().setValue(selectedRecord.data.apellidos)
                var date = selectedRecord.data.fecha;
                this.getFechaCompra().setValue(date);
                var horas = date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
                var minutos = date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes();
                this.getHoraCompra().setValue(horas + ":" + minutos);
            } else {
                alert(UI.i18n.message.compraTaquilla);
            }
        }
    },

    passarReservaACompra: function () {
        if (!this.getGridCompras().hasRowSelected())
            alert(UI.i18n.message.selectRow);
        else {
            var selectedRecord = this.getGridCompras().getSelectedRecord();

            if (selectedRecord.data.reserva == true) {
                var self = this;
                this.getGridDetalleCompras().showFormasDePagoWindow(function () {
                    self.doPassarReservaACompra()
                });
            }
        }
    },

    passarButacaReservaACompra: function () {
        if (!this.getGridCompras().hasRowSelected() || !this.getGridDetalleCompras().hasRowSelected())
            alert(UI.i18n.message.selectRow);
        else {
            var selectedRecord = this.getGridCompras().getSelectedRecord();

            if (selectedRecord.data.reserva == true) {
                var self = this;
                this.getGridDetalleCompras().showFormasDePagoWindow(function () {
                    var idsButacas = self.getGridDetalleCompras().getSelectedColumnIds();
                    self.doPassarReservaACompra(idsButacas)
                });
            }
        }
    },

    changeFormaPago: function (combo, val, oldVal) {
        this.getTxtReciboPago().allowBlank = (val !== 'tarjetaOffline' && val !== 'tarjeta');
        this.getTxtReciboPago().validate();
        this.getTxtEmail().allowBlank = (val !== 'tarjeta');
        this.getTxtEmail().validate();
        this.getTxtNombre().allowBlank = (val !== 'tarjeta');
        this.getTxtNombre().validate();
        this.getTxtApellidos().allowBlank = (val !== 'tarjeta');
        this.getTxtApellidos().validate();
        if (val === 'tarjetaOffline' || val === 'tarjeta') {
            this.getTxtReciboPago().show();
        } else {
            this.getTxtReciboPago().hide();
        }
    },

    doCambiarFormaPago: function () {
        var tipoPago = this.getComboTipoPago().value;
        var txtRecibo = this.getTxtReciboPago().value;
        var fechaCompra = this.getFechaCompra().getSubmitValue();
        var horaCompra = this.getHoraCompra().getValue();

        if (tipoPago != undefined && tipoPago !== 0 && tipoPago !== '') {
            if (this.getFormFormasDePago().isValid()) {
                txtRecibo = (tipoPago == 'tarjetaOffline' || tipoPago == 'tarjeta') ? txtRecibo : '';
                var email = this.getTxtEmail().value;
                var nombre = this.getTxtNombre().value;
                var apellidos = this.getTxtApellidos().value;
                var selectedRecord = this.getGridCompras().getSelectedRecord();

                if (!selectedRecord.data.reserva && selectedRecord.data.taquilla) {

                    if (confirm(UI.i18n.message.sureCambiarFormaPago)) {
                        var idSesion = this.getGridSesionesComprasReservas().getSelectedColumnId();
                        var idCompra = this.getGridCompras().getSelectedColumnId();
                        var me = this;
                        this.getGridCompras().setLoading(UI.i18n.message.loading);

                        Ext.Ajax.request({
                            url: urlPrefix + 'compra/' + idSesion + '/pago/' + idCompra
                                + '?tipopago=' + tipoPago
                                + '&recibo=' + txtRecibo
                                + '&fechacompra=' + fechaCompra
                                + '&horacompra=' + horaCompra
                                + (email != undefined ? "&email=" + email : "")
                                + (nombre != undefined ? "&nombre=" + nombre : "")
                                + (apellidos != undefined ? "&apellidos=" + apellidos : ""),
                            method: 'PUT',
                            success: function (response) {
                                me.setStoreCompras();
                                me.getGridCompras().setLoading(false);
                                me.getGridCompras().deseleccionar();
                                me.getGridCompras().getStore().load();

                                me.getFormFormasDePago().up('window').close();
                            }, failure: function (response) {
                                me.getGridCompras().setLoading(false);
                                var resp = Ext.JSON.decode(response.responseText, true);

                                if (resp['message'])
                                    alert(resp['message']);
                                else
                                    alert(UI.i18n.error.cambiarFormaPago);
                            }
                        });
                    }
                } else
                    alert(UI.i18n.error.compraNoEsTaquilla);
            }
        }
    },

    doPassarReservaACompra: function (idsButacas) {
        var tipoPago = this.getComboTipoPago().value;
        var txtRecibo = this.getTxtReciboPago().value;
        var fechaCompra = this.getFechaCompra().getSubmitValue();
        var horaCompra = this.getHoraCompra().getValue();

        if (tipoPago != undefined && tipoPago !== 0 && tipoPago !== '') {
            if (this.getFormFormasDePago().isValid()) {
                txtRecibo = (tipoPago == 'tarjetaOffline' || tipoPago == 'tarjeta') ? txtRecibo : '';
                var email = this.getTxtEmail().value;
                var nombre = this.getTxtNombre().value;
                var apellidos = this.getTxtApellidos().value;
                var selectedRecord = this.getGridCompras().getSelectedRecord();

                if (selectedRecord.data.reserva == true) {

                    if (confirm(UI.i18n.message.surePassarACompra)) {
                        var idSesion = this.getGridSesionesComprasReservas().getSelectedColumnId();
                        var idCompra = this.getGridCompras().getSelectedColumnId();
                        var me = this;
                        this.getGridCompras().setLoading(UI.i18n.message.loading);

                        var urlPath = "passaracompra";
                        if (idsButacas) {
                            this.getGridDetalleCompras().setLoading(UI.i18n.message.loading);
                            urlPath = "butacapassaracompra";
                        }

                        Ext.Ajax.request({
                            url: urlPrefix + 'compra/' + idSesion + '/' + urlPath + '/' + idCompra
                                + '?tipopago=' + tipoPago
                                + '&recibo=' + txtRecibo
                                + '&fechacompra=' + fechaCompra
                                + '&horacompra=' + horaCompra
                                + (email != undefined ? "&email=" + email : "")
                                + (nombre != undefined ? "&nombre=" + nombre : "")
                                + (apellidos != undefined ? "&apellidos=" + apellidos : ""),
                            jsonData: idsButacas,
                            method: 'PUT',
                            success: function (response) {
                                me.setStoreCompras();
                                me.getGridCompras().setLoading(false);
                                me.getGridCompras().deseleccionar();
                                me.getGridCompras().getStore().load();

                                if (idsButacas) {
                                    me.getGridDetalleCompras().setLoading(false);
                                    me.getGridDetalleCompras().deseleccionar();
                                    me.getGridDetalleCompras().getStore().removeAll();
                                }

                                me.getFormFormasDePago().up('window').close();
                            }, failure: function (response) {
                                if (idsButacas) {
                                    me.getGridDetalleCompras().setLoading(false);
                                }
                                me.getGridCompras().setLoading(false);
                                var resp = Ext.JSON.decode(response.responseText, true);

                                if (resp['message'])
                                    alert(resp['message']);
                                else
                                    alert(UI.i18n.error.passarReservaCompra);
                            }
                        });
                    }
                } else
                    alert(UI.i18n.error.compraNoReserva);
            }
        }
    },

    cambiaButaca: function () {
        var sesion = this.getGridSesionesComprasReservas().getSelectedRecord();
        var today = new Date();
        if (sesion.data.fechaCelebracion > today) {
            if (!this.getGridDetalleCompras().hasRowSelected()) {
                alert(UI.i18n.message.selectRow);
            } else {
                var record = this.getGridDetalleCompras().getSelectedRecord();
                if (record.data.fila && record.data.numero) {
                    if (confirm(UI.i18n.message.cambiarButacaSoloLocalizacion)) {
                        this.getGridDetalleCompras().edit('formCambiaButaca');
                    }
                } else {
                    alert(UI.i18n.message.noNumerada);
                }
            }
        } else {
            alert(UI.i18n.message.sesionCelebrada);
        }
    },

    cambiaTarifa: function () {
        if (!this.getGridCompras().hasRowSelected())
            alert(UI.i18n.message.selectRow);
        else {
            var sesion = this.getGridSesionesComprasReservas().getSelectedRecord();
            var today = new Date();
            if (sesion.data.fechaCelebracion > today) {
                var compra = this.getGridCompras().getSelectedRecord();
                if (compra.data.reserva === true) {
                    if (!this.getGridDetalleCompras().hasRowSelected()) {
                        alert(UI.i18n.message.selectRow);
                    } else {
                        var localizacionesDiferentes = false;
                        var localizaciones = this.getGridDetalleCompras().getSelectedColumnValues("localizacion");
                        var oldLocalizacion = "";
                        for (l in localizaciones) {
                            if (oldLocalizacion == "") {
                                oldLocalizacion = localizaciones[l];
                            }

                            if (localizaciones[l] != oldLocalizacion) {
                                localizacionesDiferentes = true;
                                break;
                            }
                        }

                        if (!localizacionesDiferentes) {
                            this.getGridDetalleCompras().edit('formCambiaTarifa');
                        } else {
                            alert(UI.i18n.message.localizacionesDiferentes);
                        }
                    }
                } else {
                    alert(UI.i18n.message.soloReservas);
                }
            } else {
                alert(UI.i18n.message.sesionCelebrada);
            }
        }
    },

    butacasDisponibles: function () {
        var record = this.getGridDetalleCompras().getSelectedRecord();
        if (record) {
            this.getComboButacasLibres().getStore().getProxy().url = urlPrefix + 'butacas/' + record.data.id + '/libres/' + record.data.localizacion;
        }
    },

    saveCambiaButaca: function () {
        if (this.getComboButacasLibres().getValue()) {
            var valorCambioFilaButaca = this.getComboButacasLibres().getValue().split('#');
            if (valorCambioFilaButaca.length == 2) {
                var fila = valorCambioFilaButaca[0];
                var numero = valorCambioFilaButaca[1];
                var idCambiaButaca = this.getIdCambiaButaca().getValue();

                var me = this;

                if (idCambiaButaca && fila && numero) {
                    Ext.Ajax.request({
                        url: urlPrefix + 'butacas/' + idCambiaButaca + '/cambia/' + fila + '/' + numero,
                        method: 'POST',
                        success: function (response) {
                            me.getFormCambiaButaca().up("window").close();
                            me.getGridDetalleCompras().setLoading(false);
                            me.getGridDetalleCompras().deseleccionar();
                            me.getGridDetalleCompras().getStore().load();
                        }, failure: function (response) {
                            me.getGridDetalleCompras().setLoading(false);
                            alert(UI.i18n.error.cambiarButaca);
                        }
                    });
                }
            }
        }
    },

    tarifasDisponibles: function () {
        var record = this.getGridDetalleCompras().getSelectedRecord();
        if (record) {
            this.getComboTarifas().getStore().getProxy().url = urlPrefix + 'tarifa/' + record.data.id;
        }
    },

    saveCambiaTarifaButaca: function () {
        if (this.getComboTarifas().getValue()) {
            var tarifa = this.getComboTarifas().getValue();
            var me = this;

            if (tarifa) {
                Ext.Ajax.request({
                    url: urlPrefix + 'butacas/cambiatipo/' + tarifa,
                    jsonData: me.getGridDetalleCompras().getSelectedColumnIds(),
                    method: 'POST',
                    success: function (response) {
                        me.getFormCambiaTarifa().up("window").close();
                        me.getGridDetalleCompras().setLoading(false);
                        me.getGridDetalleCompras().deseleccionar();
                        me.getGridDetalleCompras().getStore().load();
                        me.getGridCompras().getStore().load();
                    }, failure: function (response) {
                        me.getGridDetalleCompras().setLoading(false);
                        alert(UI.i18n.error.cambiarButaca);
                    }
                });
            }
        }
    },

    anularButacas: function () {
        if (!this.getGridDetalleCompras().hasRowSelected())
            alert(UI.i18n.message.selectRow);
        else {
            if (confirm(UI.i18n.message.sureAnular)) {
                var idSesion = this.getGridSesionesComprasReservas().getSelectedColumnId();
                var idsButacas = this.getGridDetalleCompras().getSelectedColumnIds();

                var me = this;
                this.getGridDetalleCompras().setLoading(UI.i18n.message.loading);

                Ext.Ajax.request({
                    url: urlPrefix + 'compra/' + idSesion + '/butacas/anuladas',
                    jsonData: idsButacas,
                    method: 'PUT',
                    success: function (response) {
                        me.getGridDetalleCompras().setLoading(false);
                        me.getGridDetalleCompras().deseleccionar();
                        me.getGridDetalleCompras().getStore().load();
                        me.getGridCompras().getStore().load();
                    }, failure: function (response) {

                        me.getGridDetalleCompras().setLoading(false);

                        var respuesta = Ext.JSON.decode(response.responseText, true);
                        var msg = UI.i18n.error["error" + respuesta.codi];

                        if (msg != undefined)
                            alert(msg);
                        else
                            alert(UI.i18n.error.anularEntrada);
                    }
                });
            }
        }
    },

    muestraBotonesGrid: function (readOnlyUser) {
        if (readOnlyUser == undefined || !readOnlyUser) {
            this.getBtAnular().show();
            this.getBtAnularDetalle().show();
            this.getBtDesanular().show();
            if (!compraTaquillaDisabled)
                this.getBtPassToCompra().show();
        } else {
            this.getBtAnular().hide();
            this.getBtAnularDetalle().hide();
            this.getBtDesanular().hide();
            this.getBtPassToCompra().hide();
        }
    },

    verCompras: function (button, event, opts) {
        if (this.getGridEventosComprasReservas().hasRowSelected() && this.getGridSesionesComprasReservas().hasRowSelected()) {
            var evento = this.getGridEventosComprasReservas().getSelectedRecord();
            var sesion = this.getGridSesionesComprasReservas().getSelectedRecord();
            var title = UI.i18n.message.evento + ": " + (lang == 'ca' ? evento.data.tituloVa : evento.data.tituloEs) + " - " + UI.i18n.message.sessio + ": " + Ext.Date.format(sesion.data.fechaCelebracion, 'd/m/Y H:i');

            this.getGridSesionesComprasReservas().showVerComprasWindow(title);
            this.muestraBotonesGrid(readOnlyUser);
        } else
            alert(UI.i18n.message.selectRow);
    },

    exportarCSV: function (button, event, opts, detail, entradas) {
        if (this.getGridEventosComprasReservas().hasRowSelected() && this.getGridSesionesComprasReservas().hasRowSelected()) {
            var sesion = this.getGridSesionesComprasReservas().getSelectedRecord();
            if (detail) {
                if (entradas) {
                    window.open('/par/rest/compra/sesion/' + sesion.data.id + '/csv/detail/entradas');
                }
                else {
                    window.open('/par/rest/compra/sesion/' + sesion.data.id + '/csv/detail');
                }
            }
            else {
                window.open('/par/rest/compra/sesion/' + sesion.data.id + '/csv');
            }
        } else
            alert(UI.i18n.message.selectRow);
    },

    exportarCSVDetail: function (button, event, opts) {
        return this.exportarCSV(button, event, opts, true)
    },

    exportarCSVComprasDetail: function (button, event, opts) {
        return this.exportarCSV(button, event, opts, true, true)
    },

    loadEventos: function () {
        this.getGridEventosComprasReservas().getStore().load();
    },

    loadSesiones: function (selectionModel, record) {
        if (record[0]) {
            this.showHideTodasSesiones();
        }
    },

    setStoreCompras: function () {
        var storeCompras = this.getGridCompras().getStore();

        storeCompras.remoteFilter = false;
        storeCompras.clearFilter();
        storeCompras.remoteFilter = true;

        storeCompras.getProxy().url = urlPrefix + 'compra/' + this.getGridSesionesComprasReservas().getSelectedColumnId();
        storeCompras.getProxy().setExtraParam("showAnuladas", (this.getCheckboxAnuladas().value) ? 1 : 0);
        storeCompras.getProxy().setExtraParam("showOnline", (this.getCheckboxOnline().value) ? 1 : 0);
    },

    loadCompras: function () {
        this.getGridDetalleCompras().getStore().removeAll();
        this.getGridDetalleCompras().disable();
        this.setStoreCompras();
        this.getPagingToolbar().moveFirst();
        this.getGridCompras().store.load();
    },

    loadButacas: function (selectionModel, record) {
        if (record[0]) {
            var storeButacas = this.getGridDetalleCompras().getStore();
            var compraid = record[0].get('id');

            storeButacas.remoteFilter = false;
            storeButacas.clearFilter();
            var idSesion = this.getGridSesionesComprasReservas().getSelectedColumnId();
            storeButacas.filter('sesionId', idSesion);
            storeButacas.remoteFilter = true;

            storeButacas.getProxy().url = urlPrefix + 'compra/' + compraid + '/butacas';
            this.getGridDetalleCompras().enable();
            this.getPagingToolbarDetalle().moveFirst();
        } else {
            this.getGridDetalleCompras().getStore().removeAll();
            this.getGridDetalleCompras().disable();
        }
    },

    mostrarAyuda: function () {
        hopscotch.startTour(tourComprasReservasPopUp);
    },

    onLoadFormaPago: function () {
        var date = new Date();
        var horas = date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
        var minutos = date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes();
        this.getHoraCompra().setValue(horas + ":" + minutos);
    }
});
