Ext.define('Paranimf.controller.SalasNumeradas', {
    extend: 'Ext.app.Controller',

    views: ['EditModalWindow', 'EditBaseForm', 'EditBaseGrid', 'salasnumeradas.PanelSalasNumeradas', 'salasnumeradas.GridSalasNumeradas', 'salasnumeradas.FormButacasNumeradas'],
    stores: ['SalasNumeradas'],
    models: ['Butaca'],

    refs: [{
        ref: 'gridSalasNumeradas',
        selector: 'gridSalasNumeradas'
    }, {
        ref: 'formButacasNumeradas',
        selector: 'formButacasNumeradas'
    }],

    init: function () {
        this.control({

            'panelSalasNumeradas': {
                beforeactivate: this.recargaStore
            },

            'gridSalasNumeradas button[action=verButacas]': {
                click: this.verButacas
            }
        });
    },

    recargaStore: function (comp, opts) {
        console.log("RECARGA STORE SALAS NUMERADAS");
        this.getGridSalasNumeradas().getStore().load();
    },

    verButacas: function (button, event, opts) {
        var grid = this.getGridSalasNumeradas();
        grid.edit('formButacasNumeradas', undefined, 0.7, 0.7);

        var salaId = grid.getSelectedColumnId();
        var url = urlPublic + "/rest/entrada/butacasFragment/salas/" + salaId;
        Ext.getDom('iframeButacasReservas').src = url;
    }
});