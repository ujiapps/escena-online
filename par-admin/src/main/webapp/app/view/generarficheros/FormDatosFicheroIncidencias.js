Ext.define('Paranimf.view.generarficheros.FormDatosFicheroIncidencias', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formDatosFicheroIncidencias',

    defaults: {
        allowBlank: false,
        msgTarget: 'side',
        labelWidth: 210,
        anchor: '100%'
    },

    items: [{
        fieldLabel: UI.i18n.field.fechaPrimeraSesion,
        xtype: 'datefield',
        startDay: 1,
        name: 'fechaSemana',
        maxValue: new Date()
    }]
});