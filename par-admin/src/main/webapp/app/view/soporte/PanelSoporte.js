Ext.define('Paranimf.view.soporte.PanelSoporte', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelSoporte',
    frame: false,
    border: false,
    layout: 'border',
    bodyStyle:{"background-color":"white"},
    items: [{
        flex: 1,
        region: 'center',
        xtype: 'label',
        html: '<h4>' + UI.i18n.message.support + ' <a href="mailto:escenaonline@cuatroochenta.com">escenaonline@cuatroochenta.com</a></h4>',
    }]
});