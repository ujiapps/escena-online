Ext.define('Paranimf.view.tpv.FormTutorialTpv', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formTutorialTpv',
    layout: 'fit',

    defaults: {
        anchor: '100%',
        xtype: 'textfield',
        labelWidth: 190
    },

    buttons: undefined,

    bbar: [
        {
            id: 'tutorialAnterior',
            text: UI.i18n.button.anterior
        }, {
            id: 'tutorialSiguiente',
            text: UI.i18n.button.siguiente
        }, {
            id: 'tutorialCancelar',
            text: UI.i18n.button.close
        }],

    items: [{
        xtype: 'panel',
        id: 'formTutorialTpvCards',
        frame: false,
        layout: 'card',
        border: false,
        autoScroll: true,
        defaults: {
            border: false,
        },
        items: [{
            id: 'tutorial-tpv-indice',
            html: UI.i18n.html.indice
        }, {
            id: 'tutorial-tpv-redsys',
            html: UI.i18n.html.redsys
        }, {
            id: 'tutorial-tpv-ceca',
            html: UI.i18n.html.ceca
        }]
    }]
});
