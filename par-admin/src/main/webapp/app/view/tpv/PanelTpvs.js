Ext.define('Paranimf.view.tpv.PanelTpvs', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelTpvs',
    frame: false,
    border: false,
    layout: 'border',

    items: [{
        flex: 1,
        region: 'center',
        autoScroll: true,
        xtype: 'gridTpvs',
        collapsible: true
    }]
});