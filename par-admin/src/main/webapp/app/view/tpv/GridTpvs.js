Ext.define('Paranimf.view.tpv.GridTpvs', {
    extend: 'Paranimf.view.EditBaseGrid',

    requires: [
        'FilterField.filters.Filter',
        'FilterField.button.OperatorButton'
    ],

    plugins: {
        ptype: 'filterfield'
    },

    tbar: [{
        xtype: 'button',
        text: UI.i18n.button.add,
        action: 'add',
        cls: 'btn-add',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }, {
        xtype: 'button',
        text: (readOnlyUser == undefined || readOnlyUser == false) ? UI.i18n.button.edit : UI.i18n.button.ver,
        cls: 'btn-edit',
        action: 'edit'
    }],

    alias: 'widget.gridTpvs',
    store: 'Tpvs',
    stateId: 'gridTpvs',

    title: UI.i18n.gridTitle.tpvs,

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Tpvs',
        dock: 'bottom',
        displayInfo: true
    }],

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: 'nombre',
            text: UI.i18n.field.name,
            flex: 1
        }, {
            dataIndex: 'visible',
            text: UI.i18n.field.visible,
            flex: 1,
            renderer: function (value) {
                if (value == 'on')
                    return "Sí";
                else
                    return "No";
            }
        }];

        this.callParent(arguments);
    },

    showTutorialWindow: function () {
        this.createPercentageModalWindow('formTutorialTpv', 0.6, 0.6, UI.i18n.formTitle.tutorialTpv).show();
    }
});