Ext.define('Paranimf.view.tpv.FormTpvs', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formTpvs',

    url: urlPrefix + 'tpv',

    defaults: {
        allowBlank: false,
        msgTarget: 'side',
        labelWidth: 120,
        anchor: '100%',
        xtype: 'textfield',
        flex: 1
    },

    items: [{
        name: 'id',
        hidden: true,
        allowBlank: true
    }, {
        fieldLabel: UI.i18n.field.nameMulti,
        readOnly: true,
        name: 'nombre'
    }, {
        fieldLabel: UI.i18n.field.visible,
        xtype: 'checkbox',
        name: 'visible'
    }]
});