Ext.define('Paranimf.view.compra.FormFormasDePago', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formFormasDePago',

    defaults: {
        msgTarget: 'side',
        labelWidth: 150,
        anchor: '100%',
        flex: 1
    },

    items: [
        {
            fieldLabel: UI.i18n.field.tipoPago,
            name: 'tipoPago',
            xtype: 'combobox',
            displayField: 'name',
            valueField: 'value',
            queryMode: 'local',
            typeAhead: false,
            allowBlank: false,
            forceSelection: true,
            store: new Ext.data.SimpleStore({
                fields: ['value', 'name'],
                data: payModes
            })
        }, {
            name: 'referenciaDePago',
            xtype: 'textfield',
            allowBlank: false,
            hidden: true,
            fieldLabel: UI.i18n.field.referenciaDePago
        }, {
            name: 'email',
            xtype: 'textfield',
            allowBlank: false,
            fieldLabel: UI.i18n.field.email
        }, {
            name: 'nombre',
            xtype: 'textfield',
            allowBlank: false,
            fieldLabel: UI.i18n.field.name
        }, {
            name: 'apellidos',
            xtype: 'textfield',
            allowBlank: false,
            fieldLabel: UI.i18n.field.surname
        }, {
            fieldLabel: UI.i18n.field.fechaCompra,
            name: 'fechaCompra',
            xtype: 'datefield',
            submitFormat: 'Y/m/d',
            allowBlank: false,
            editable: false,
            startDay: 1,
            value: new Date()
        }, {
            name: 'horaCompra',
            xtype: 'textfield',
            emptyText: 'HH:MM',
            allowBlank: false,
            fieldLabel: UI.i18n.field.horaCompra,
            validator: function (value) {
                if (value.indexOf(":") != -1) {
                    var h = value.split(":");
                    if ((h.length == 2) && (h[1].length == 2)) {
                        if (h[0] >= 0 && h[0] < 24 && h[1] >= 0 && h[1] < 60) {
                            return true;
                        }
                    }
                }
                return false;
            }
        }]
});
