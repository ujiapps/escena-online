Ext.define('Paranimf.view.compra.GridCompras', {
    extend: 'Paranimf.view.EditBaseGrid',

    requires: [
        'FilterField.filters.Filter',
        'FilterField.button.OperatorButton'
    ],

    plugins: {
        ptype: 'filterfield'
    },

    alias: 'widget.gridCompras',
    store: 'Compras',
    title: UI.i18n.gridTitle.comprasReservasHechas,
    stateId: 'gridCompras',

    tbar: {
        enableOverflow: true,
        items: [{
            action: 'anular',
            cls: 'btn-anular-compra',
            text: UI.i18n.button.anular
        }, {
            action: 'desanular',
            cls: 'btn-desanular-compra',
            text: UI.i18n.button.desanular
        }, {
            action: 'cambiarFormaPago',
            cls: 'btn-cambia-formapago',
            text: UI.i18n.button.cambiarFormaPago,
            hidden: compraTaquillaDisabled
        }, {
            action: 'passToCompra',
            cls: 'btn-reserva-to-compra',
            text: UI.i18n.button.passToCompra,
            hidden: compraTaquillaDisabled
        }, {
            xtype: 'checkbox',
            action: 'showAnuladas',
            cls: 'btn-mostrar-anuladas',
            fieldLabel: UI.i18n.field.mostrarAnuladas,
            labelWidth: 110
        }, '-', {
            xtype: 'checkbox',
            checked: true,
            action: 'showOnline',
            cls: 'btn-mostrar-online',
            fieldLabel: UI.i18n.field.mostrarOnline,
            labelWidth: 90
        }]
    },

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Compras',
        dock: 'bottom',
        displayInfo: true
    }],

    viewConfig: {
        enableTextSelection: true,
        getRowClass: function (record) {
            if (record && record.data.anulada)
                return 'gridAnulada';
            else if (record && record.data.reserva)
                return 'gridReserva';
            else if (record && record.data.taquilla)
                return 'gridTaquilla';
            else
                return 'gridOnline';
        }
    },

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            text: UI.i18n.field.idIntern,
            filter: {
                xtype: 'textfield'
            }
        }, {
            dataIndex: 'uuid',
            text: UI.i18n.field.uuid,
            filter: {
                xtype: 'textfield'
            }
        }, {
            dataIndex: 'fecha',
            text: UI.i18n.field.date,
            format: 'd/m/Y H:i',
            xtype: 'datecolumn',
            filter: {
                xtype: 'datefield',
                editable: false,
                startDay: 1
            },
            flex: 5
        }, {
            hidden: true,
            dataIndex: 'desde',
            text: UI.i18n.field.inicioReserva,
            xtype: 'datecolumn',
            format: 'd/m/Y H:i',
            flex: 3
        }, {
            hidden: true,
            dataIndex: 'hasta',
            text: UI.i18n.field.finReserva,
            xtype: 'datecolumn',
            format: 'd/m/Y H:i',
            flex: 3
        }, {
            dataIndex: 'nombre',
            text: UI.i18n.field.nameMulti,
            flex: 5,
            filter: {
                xtype: 'textfield'
            }
        }, {
            dataIndex: 'apellidos',
            text: UI.i18n.field.surnameMulti,
            flex: 5,
            filter: {
                xtype: 'textfield'
            }
        }, {
            dataIndex: 'email',
            flex: 5,
            text: UI.i18n.field.email,
            filter: {
                xtype: 'textfield'
            }
        }, {
            dataIndex: 'telefono',
            flex: 2,
            text: UI.i18n.field.phone,
            filter: {
                xtype: 'textfield'
            }
        }, {
            dataIndex: 'direccion',
            flex: 2,
            text: UI.i18n.field.address,
            filter: {
                xtype: 'textfield'
            }
        }, {
            dataIndex: 'poblacion',
            flex: 2,
            text: UI.i18n.field.poblacion,
            filter: {
                xtype: 'textfield'
            }
        }, {
            dataIndex: 'cp',
            flex: 2,
            text: UI.i18n.field.codigoPostal,
            filter: {
                xtype: 'textfield'
            }
        }, {
            dataIndex: 'provincia',
            flex: 2,
            text: UI.i18n.field.provincia,
            filter: {
                xtype: 'textfield'
            }
        }, {
            dataIndex: 'infoPeriodica',
            flex: 2,
            text: UI.i18n.field.infoPeriodica,
            hidden: true,
            renderer: function (val, p) {
                return (val) ? '<img src="../resources/images/tick.png" alt="Sí" title="Sí" />' : '';
            }
        }, {
            align: 'center',
            dataIndex: 'importe',
            flex: 2,
            text: UI.i18n.field.importe,
            renderer: function (val) {
                return (val == 0) ? '' : val.toFixed(2) + '€';
            }
        }, {
            align: 'center',
            dataIndex: 'tipo',
            flex: 2,
            hidden: true,
            text: UI.i18n.formTitle.formasPago,
            sortable: false,
            renderer: function (val) {
                if (val) {
                    for (var i in payModes) {
                        if (payModes[i][0].toLowerCase() == val.toLowerCase()) {
                            return payModes[i][1];
                        }
                    }
                }
                return '';
            }
        }, {
            align: 'center',
            dataIndex: 'taquilla',
            flex: 2,
            text: UI.i18n.field.taquilla,
            renderer: function (val, p) {
                return (val) ? '<img src="../resources/images/tick.png" alt="Sí" title="Sí" />' : '';
            }
        }, {
            align: 'center',
            flex: 2,
            text: UI.i18n.field.online,
            renderer: function (val, p, rec) {
                return (!rec.data.taquilla) ? '<img src="../resources/images/tick.png" alt="Sí" title="Sí" />' : '';
            }
        }, {
            align: 'center',
            dataIndex: 'reserva',
            flex: 2,
            text: UI.i18n.field.reserva,
            renderer: function (val, p) {
                return (val) ? '<img src="../resources/images/tick.png" alt="Sí" title="Sí" />' : '';
            }
        }, {
            align: 'center',
            dataIndex: 'pagada',
            flex: 2,
            text: UI.i18n.field.pagada,
            renderer: function (val, p) {
                return (val) ? '<img src="../resources/images/tick.png" alt="Sí" title="Sí" />' : '<img src="../resources/images/cross.png" alt="No" title="No" />';
            }
        }, {
            align: 'center',
            dataIndex: 'caducada',
            flex: 2,
            text: UI.i18n.field.caducada,
            renderer: function (val, p) {
                return (val) ? '<img src="../resources/images/tick.png" alt="Sí" title="Sí" />' : '';
            }
        }, {
            dataIndex: 'observacionesReserva',
            text: UI.i18n.field.observacionesReserva,
            filter: {
                xtype: 'textfield'
            },
            flex: 2
        }, {
            flex: 2,
            align: 'center',
            text: UI.i18n.message.printTaquilla,
            renderer: function (val, p, rec) {
                if (!rec.data.reserva && !rec.data.anulada)
                    return '<a target="blank" href="' + urlPrefix + 'compra/' + rec.data.uuid + '/pdftaquilla">' + UI.i18n.message.pdf + '</a>';
                else
                    return '';
            }
        }, {
            flex: 2,
            align: 'center',
            text: UI.i18n.message.printAtHome,
            renderer: function (val, p, rec) {
                if (!rec.data.reserva && !rec.data.anulada)
                    return '<a target="blank" href="' + urlPrefix + 'compra/' + rec.data.uuid + '/pdf">' + UI.i18n.message.pdf + '</a>';
                else
                    return '';
            }
        }, {
            flex: 2,
            align: 'center',
            hidden: true,
            text: UI.i18n.message.printTaquilla,
            renderer: function (val, p, rec) {
                if (!rec.data.reserva && !rec.data.anulada)
                    return '<a target="" href="javascript:printJS({' +
                        'printable: \'/par/rest/compra/' + rec.data.uuid + '/pdftaquilla\',' +
                        'type: \'pdf\',' +
                        'showModal: true,' +
                        'modalMessage: UI.i18n.message.printing' +
                        '});">' + UI.i18n.message.print + '</a>';
                else
                    return '';
            }
        }, {
            dataIndex: 'idDevolucion',
            sortable: false,
            text: UI.i18n.field.idDevolucion,
            renderer: function (val, p) {
                if (val)
                    return val;
                else
                    return '';
            },
            hidden: false,
            filter: {
                xtype: 'textfield'
            },
            flex: 2
        }];

        this.callParent(arguments);
    },

    showAddCompraWindow: function () {
        this.createPercentageModalWindow('formCompras').show();
    },

    showFormasDePagoWindow: function (callback) {
        this.createPercentageModalWindow('formFormasDePago', undefined, undefined, UI.i18n.formTitle.formasPago, false, callback).show();
    }
});