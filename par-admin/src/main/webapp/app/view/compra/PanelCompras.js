Ext.define('Paranimf.view.compra.PanelCompras', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelCompras',
    layout: 'border',

    buttons: [{
        xtype: 'button',
        text: UI.i18n.button.close,
        handler: function () {
            this.up('window').close();
        }
    }],

    items: [{
        region: 'center',
        xtype: 'gridCompras',
        collapsible: true,
        minHeight: 100,
        split: true,
        header: true,
        flex: 1.7
    }, {
        region: 'south',
        xtype: 'gridDetalleCompras',
        minHeight: 100,
        collapsible: true,
        split: true,
        flex: 1
    }]
});