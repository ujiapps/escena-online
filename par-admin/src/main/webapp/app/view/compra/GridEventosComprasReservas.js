Ext.define('Paranimf.view.compra.GridEventosComprasReservas', {
    extend: 'Paranimf.view.EditBaseGrid',

    requires: [
        'FilterField.filters.Filter',
        'FilterField.button.OperatorButton'
    ],

    plugins: {
        ptype: 'filterfield'
    },

    tbar: [{
        xtype: 'checkbox',
        fieldLabel: UI.i18n.field.ventaRetrasada,
        name: 'mostrarTodos',
        labelWidth: 410,
        labelAlign: 'right'
    }],

    alias: 'widget.gridEventosComprasReservas',
    store: 'EventosCompra',
    autoScroll: true,
    stateId: 'gridEventosComprasReservas',

    title: UI.i18n.gridTitle.eventosCompras,

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'EventosCompra',
        dock: 'bottom',
        displayInfo: true
    }],

    initComponent: function () {
        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: 'asientosNumerados',
            hidden: true,
            text: UI.i18n.field.asientosNumerados,
            renderer: function (val) {
                if (val == 0)
                    return 'No';
                else
                    return 'Sí';
            }
        }, {
            flex: 2,
            dataIndex: 'fechaPrimeraSesion',
            text: UI.i18n.field.fechaPrimeraSesion,
            renderer: function (val) {
                if (val != '' && val != undefined) {
                    var dt = new Date(val);
                    return Ext.Date.format(dt, 'd/m/Y H:i');
                }
                return '';
            }
        }, {
            dataIndex: 'parTiposEvento',
            text: UI.i18n.field.type,
            flex: 2,
            renderer: function (val, p) {
                return lang && lang === 'ca' ? val["nombreVa"] : val["nombreEs"];
            }
        }, {
            dataIndex: lang && lang === 'ca' ? 'tituloVa' : 'tituloEs',
            text: lang && lang === 'ca' ? UI.i18n.field.title_va : UI.i18n.field.title,
            filter: {
                xtype: 'textfield'
            },
            flex: 5
        }];

        this.callParent(arguments);
    }
});