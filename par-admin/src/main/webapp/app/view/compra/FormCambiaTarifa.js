Ext.define('Paranimf.view.compra.FormCambiaTarifa', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formCambiaTarifa',

    url: urlPrefix + 'compra/edita/butaca',

    defaults: {
        msgTarget: 'side',
        labelWidth: 120,
        anchor: '100%',
        readOnly: true,
        xtype: 'textfield',
        flex: 1
    },

    items: [{
        allowBlank: false,
        readOnly: false,
        fieldLabel: UI.i18n.field.tarifa,
        name: 'tarifa',
        xtype: 'combobox',
        forceSelection: true,
        displayField: 'nombre',
        valueField: 'id',
        store: 'TarifasLocalizacionCombo',
        queryMode: 'remote',
        typeAhead: true
    }]
});