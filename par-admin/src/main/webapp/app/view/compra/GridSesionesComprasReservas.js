Ext.define('Paranimf.view.compra.GridSesionesComprasReservas', {
    extend: 'Paranimf.view.EditBaseGrid',

    requires: [
        'FilterField.filters.Filter',
        'FilterField.button.OperatorButton'
    ],

    plugins: {
        ptype: 'filterfield'
    },

    alias: 'widget.gridSesionesComprasReservas',
    store: 'SesionesCompra',

    title: UI.i18n.gridTitle.sesionesCompras,
    stateId: 'gridSesionesComprasReservas',

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'SesionesCompra',
        dock: 'bottom',
        displayInfo: true
    }],

    tbar: [{
        xtype: 'button',
        text: UI.i18n.button.verCompras,
        cls: 'btn-compras-reservas',
        action: 'verCompras'
    }, {
        xtype: 'button',
        text: UI.i18n.button.exportarCSVDetail,
        menu: {
            xtype: 'menu',
            items: [
                {
                    xtype: 'button',
                    text: UI.i18n.button.exportarCSVDetail,
                    cls: 'btn-export-csv',
                    action: 'exportarCSVDetail'
                }, {
                    xtype: 'button',
                    text: UI.i18n.button.exportarCSVCompraDetail,
                    cls: 'btn-export-csv',
                    action: 'exportarCSVComprasDetail'
                }, {
                    xtype: 'button',
                    text: UI.i18n.button.exportarCSV,
                    cls: 'btn-export-csv',
                    action: 'exportarCSV'
                }
            ]
        }
    }, {
        xtype: 'checkbox',
        fieldLabel: UI.i18n.field.sesionesAcabadas,
        name: 'mostrarTodas',
        cls: 'btn-mostrar-todas',
        labelWidth: 250,
        labelAlign: 'right'
    }],

    viewConfig: {
        enableTextSelection: true,
        getRowClass: function (record) {
            if (record && record.data.anulada)
                return 'gridAnulada'
        }
    },

    initComponent: function () {
        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            hidden: true,
            dataIndex: 'horaCelebracion',
            sortable: false,
            text: UI.i18n.field.sessionTime
        }, {
            dataIndex: 'fechaCelebracion',
            text: UI.i18n.field.eventDate,
            format: 'd/m/Y H:i',
            xtype: 'datecolumn',
            filter: {
                xtype: 'datefield',
                editable: false,
                startDay: 1
            },
            flex: 1
        }, {
            dataIndex: 'horaApertura',
            hidden: true,
            text: UI.i18n.field.opening,
            flex: 0.5
        }, {
            dataIndex: 'fechaInicioVentaOnline',
            text: UI.i18n.field.startOnlineSelling,
            xtype: 'datecolumn',
            flex: 1,
            renderer: function (val, p, rec) {
                return (!rec.data.canalInternet) ? '-' : Ext.util.Format.date(val, 'd/m/Y H:i');
            }
        }, {
            dataIndex: 'fechaFinVentaOnline',
            text: UI.i18n.field.endOnlineSelling,
            xtype: 'datecolumn',
            flex: 1,
            renderer: function (val, p, rec) {
                return (!rec.data.canalInternet) ? '-' : Ext.util.Format.date(val, 'd/m/Y H:i');
            }
        }, {
            dataIndex: 'butacasVendidas',
            text: UI.i18n.field.butacasVendidas,
            sortable: false,
            flex: 0.5
        }, {
            dataIndex: 'butacasReservadas',
            sortable: false,
            text: UI.i18n.field.butacasReservadas,
            flex: 0.5
        }, {
            dataIndex: 'butacasDisponibles',
            sortable: false,
            text: UI.i18n.field.butacasDisponibles,
            flex: 0.5,
            renderer: function (val, p, rec) {
                return rec.data.multisesion ? '-' : val;
            }
        }];

        this.callParent(arguments);

        //this.getDockedItems('toolbar[dock=top]')[0].hide();
    },

    showVerComprasWindow: function (title) {
        var window = this.createPercentageModalWindow('panelCompras', undefined, 0.95, title, false);
        window.header = {
            titlePosition: 0,
            items: [
                {
                    xtype: 'button',
                    text: UI.i18n.button.help,
                    action: 'help'
                }
            ]
        };
        window.show();
    }
});