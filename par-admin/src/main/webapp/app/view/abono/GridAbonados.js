Ext.define('Paranimf.view.abono.GridAbonados', {
    extend: 'Paranimf.view.EditBaseGrid',

    alias: 'widget.gridAbonados',
    store: 'Abonados',

    title: UI.i18n.gridTitle.abonados,
    stateId: 'gridAbonados',

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Abonados',
        dock: 'bottom',
        displayInfo: true
    }],

    tbar: [{
        xtype: 'button',
        text: UI.i18n.button.del,
        cls: 'btn-del-abono',
        action: 'del',
        hidden: taquillaUser || readOnlyUser
    }],

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: 'nombre',
            text: UI.i18n.field.nameMulti,
            flex: 1
        }, {
            dataIndex: 'apellidos',
            text: UI.i18n.field.surnameMulti,
            flex: 1
        }, {
            dataIndex: 'email',
            text: UI.i18n.field.email,
            flex: 1
        }, {
            align: 'center',
            dataIndex: 'importe',
            flex: 1,
            text: UI.i18n.field.importe,
            renderer: function (val) {
                return (val == 0) ? '' : val.toFixed(2) + '€';
            }
        }, {
            dataIndex: 'idDevolucion',
            sortable: false,
            text: UI.i18n.field.idDevolucion,
            renderer: function (val, p) {
                if (val)
                    return val;
                else
                    return '';
            },
            hidden: false,
            filter: {
                xtype: 'textfield'
            },
            flex: 1
        }, {
            dataIndex: 'uuid',
            text: UI.i18n.field.uuid,
            filter: {
                xtype: 'textfield'
            },
            flex: 2
        }, {
            flex: 2,
            align: 'center',
            text: UI.i18n.message.printAtHome,
            renderer: function (val, p, rec) {
                return '<a target="blank" href="' + urlPrefix + 'compra/' + rec.data.uuid + '/pdf">' + UI.i18n.message.print + '</a>';
            }
        }, {
            dataIndex: 'abonos',
            text: UI.i18n.field.abonos,
            flex: 1
        }];

        this.callParent(arguments);
    }
});