Ext.define('Paranimf.view.abono.GridAbonos', {
    extend: 'Paranimf.view.EditBaseGrid',

    alias: 'widget.gridAbonos',
    store: 'Abonos',

    title: UI.i18n.gridTitle.abonos,
    stateId: 'gridAbonos',

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Abonos',
        dock: 'bottom',
        displayInfo: true
    }],

    tbar: [{
        xtype: 'button',
        text: UI.i18n.button.add,
        action: 'add',
        cls: 'btn-add',
        hidden: taquillaUser || readOnlyUser
    }, {
        xtype: 'button',
        text: UI.i18n.button.edit,
        cls: 'btn-edit',
        action: 'edit',
        hidden: taquillaUser || readOnlyUser
    }, {
        xtype: 'button',
        text: UI.i18n.button.del,
        action: 'del',
        cls: 'btn-delete',
        hidden: taquillaUser || readOnlyUser
    }],

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern,
        }, {
            dataIndex: 'nombre',
            text: UI.i18n.field.nameMulti,
            flex: 2.5
        }, {
            dataIndex: 'maxEventos',
            text: UI.i18n.field.maxEventos,
            flex: 1
        }];

        this.callParent(arguments);
    },

    showAddAbonoWindow: function () {
        this.createPercentageModalWindow('formAbonos').show();
    }
});