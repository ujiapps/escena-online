Ext.define('Paranimf.view.abono.FormAbonos', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formAbonos',

    url: urlPrefix + 'abono',

    defaults: {
        allowBlank: false,
        msgTarget: 'side',
        labelWidth: 350,
        anchor: '100%',
        xtype: 'textfield',
        flex: 1
    },

    buttons: [{
        xtype: 'button',
        text: UI.i18n.button.save,
        action: 'save',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }, {
        xtype: 'button',
        text: UI.i18n.button.cancel,
        cls: 'btn-cancelar',
        handler: function () {
            this.up('window').close();
        }
    }, {
        xtype: 'button',
        text: UI.i18n.button.eliminarImagen,
        action: 'deleteImage',
        cls: 'btn-borrar-imagen',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }],

    items: [{
        name: 'id',
        hidden: true,
        allowBlank: true
    }, {
        fieldLabel: UI.i18n.field.nameMulti,
        name: 'nombre'
    }, {
        xtype: 'numberfield',
        anchor: '100%',
        spinUpEnabled: false,
        spinDownEnabled: false,
        minValue: 1,
        fieldLabel: UI.i18n.field.maxEventos,
        name: 'maxEventos',
        allowBlank: true
    },
        {
            xtype: 'fieldset',
            flex: 1,
            title: UI.i18n.field.descripcionMulti,
            defaults: {
                xtype: 'textarea',
                anchor: '100%'
            },
            items: [{
                fieldLabel: UI.i18n.field.description,
                name: 'descripcionEs',
                hidden: langsAllowed && !langsAllowed.some(item => item.locale === 'es' || item.locale === 'gl'),
                allowBlank: true
            }, {
                fieldLabel: UI.i18n.field.description_va,
                name: 'descripcionVa',
                hidden: langsAllowed && !langsAllowed.some(item => item.locale === 'ca'),
                allowBlank: true
            }]
        }, {
            fieldLabel: UI.i18n.field.tpv,
            hidden: true,
            name: 'tpv',
            labelWidth: 140,
            xtype: 'combobox',
            forceSelection: true,
            allowBlank: true,
            displayField: 'nombre',
            valueField: 'id',
            store: 'TpvsVisibles',
            queryMode: 'remote',
            typeAhead: true
        },
        {
            xtype: 'fieldset',
            title: UI.i18n.field.imagen,
            items: [{
                name: 'dataBinary',
                anchor: '100%',
                allowBlank: true,
                fieldLabel: UI.i18n.field.uploadImagen,
                labelWidth: 130,
                msgTarget: 'side',
                xtype: 'filefield',
                buttonText: '...'
            }, {
                xtype: 'label',
                id: 'imagenInsertada'
            }]
        }, {
            xtype: 'fieldset',
            name: 'ventaOnline',
            title: UI.i18n.field.ventaOnline,
            defaults: {
                anchor: '100%',
                labelWidth: 220,
                allowBlank: false
            },
            items: [{
                fieldLabel: UI.i18n.field.canalInternet,
                name: 'canalInternet',
                xtype: 'checkboxfield',
                inputValue: 1,
                uncheckedValue: 0,
                checked: true,
                allowBlank: true
            },
                {
                    fieldLabel: UI.i18n.field.startOnlineSelling,
                    name: 'fechaInicioVentaOnline',
                    xtype: 'datefield',
                    startDay: 1,
                    value: new Date()
                }, {
                    name: 'horaInicioVentaOnline',
                    xtype: 'timefield',
                    fieldLabel: UI.i18n.field.horaInicioVentaOnline,
                    minValue: '0:00 AM',
                    maxValue: '23:00 PM',
                    format: 'H:i',
                    increment: 30
                }, {
                    fieldLabel: UI.i18n.field.cierreVentaOnline,
                    name: 'horasCierreOnline',
                    xtype: 'combobox',
                    forceSelection: true,
                    displayField: 'label',
                    valueField: 'id',
                    store: 'Horas24',
                    hidden: true,
                    disabled: true,
                    queryMode: 'local',
                    typeAhead: true
                }, {
                    fieldLabel: UI.i18n.field.endOnlineSelling,
                    name: 'fechaFinVentaOnline',
                    xtype: 'datefield',
                    startDay: 1,
                    value: new Date()
                }, {
                    name: 'horaFinVentaOnline',
                    xtype: 'timefield',
                    fieldLabel: UI.i18n.field.horaFinVentaOnline,
                    minValue: '0:00 AM',
                    maxValue: '23:00 PM',
                    format: 'H:i',
                    increment: 30
                }, {
                    fieldLabel: UI.i18n.field.totalAbonos,
                    name: 'localizacion',
                    xtype: 'combobox',
                    forceSelection: true,
                    displayField: 'totalEntradas',
                    valueField: 'id',
                    store: 'LocalizacionesSalasNoNumeradas',
                    queryMode: 'local',
                    typeAhead: true
                }, {
                    fieldLabel: UI.i18n.field.precio,
                    name: 'precio',
                    minValue: 0,
                    xtype: 'numericfield',
                    decimalPrecision: 2,
                    alwaysDisplayDecimals: true
                }]
        }, {
            xtype: 'fieldset',
            hidden: !ventaAnticipada,
            name: 'ventaAnticipada',
            title: UI.i18n.field.ventaAnticipada,
            defaults: {
                anchor: '100%',
                labelWidth: 220,
                allowBlank: false
            },
            items: [{
                fieldLabel: UI.i18n.field.disponibleVentaAnticipada,
                name: 'disponibleVentaAnticipada',
                xtype: 'checkboxfield',
                inputValue: 1,
                uncheckedValue: 0,
                checked: false,
                allowBlank: true
            }, {
                fieldLabel: UI.i18n.field.fechaFinVentaAnticipada,
                name: 'fechaFinVentaAnticipada',
                xtype: 'datefield',
                disabled: true,
                hidden: true,
                startDay: 1
            }, {
                name: 'horaFinVentaAnticipada',
                xtype: 'timefield',
                fieldLabel: UI.i18n.field.horaFinVentaAnticipada,
                minValue: '0:00 AM',
                maxValue: '23:00 PM',
                format: 'H:i',
                disabled: true,
                hidden: true,
                increment: 30
            }, {
                fieldLabel: UI.i18n.field.precioAnticipado,
                hidden: true,
                disabled: true,
                name: 'precioAnticipado',
                minValue: 0,
                xtype: 'numericfield',
                decimalPrecision: 2,
                alwaysDisplayDecimals: true
            }]
        }]
});