Ext.define('Paranimf.view.tipoevento.FormTiposEventos', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formTiposEventos',

    url: urlPrefix + 'tiposeventos',

    defaults: {
        allowBlank: false,
        msgTarget: 'side',
        labelWidth: 90,
        anchor: '100%',
        xtype: 'textfield'
    },

    items: [{
        name: 'id',
        hidden: true,
        allowBlank: true
    }, {
        fieldLabel: UI.i18n.field.name,
        name: 'nombreEs',
        hidden: langsAllowed && !langsAllowed.some(item => item.locale === 'es' || item.locale === 'gl'),
        allowBlank: langsAllowed && !langsAllowed.some(item => item.locale === 'es' || item.locale === 'gl')
    }, {
        fieldLabel: UI.i18n.field.name_va,
        name: 'nombreVa',
        hidden: langsAllowed && !langsAllowed.some(item => item.locale === 'ca'),
        allowBlank: langsAllowed && !langsAllowed.some(item => item.locale === 'ca')
    }, {
        xtype: 'combobox',
        forceSelection: true,
        hidden: true,
        queryMode: 'local',
        name: 'exportarICAA',
        labelWidth: 280,
        valueField: 'id',
        displayField: 'label',
        fieldLabel: UI.i18n.field.exportarICAA,
        store: 'SiNo'
    }]
});