Ext.define('Paranimf.view.plantillareservas.FormPlantillasReservas', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formPlantillasReservas',

    url: urlPrefix + 'plantillareservas',

    defaults: {
        allowBlank: false,
        msgTarget: 'side',
        labelWidth: 90,
        anchor: '100%',
        xtype: 'textfield',
        flex: 1
    },

    items: [{
        name: 'id',
        hidden: true,
        allowBlank: true
    }, {
        fieldLabel: UI.i18n.field.nameMulti,
        name: 'nombre'
    }, {
        fieldLabel: UI.i18n.field.sala,
        name: 'sala',
        xtype: 'combobox',
        forceSelection: true,
        displayField: 'nombre',
        valueField: 'id',
        store: 'SalasNumeradas',
        queryMode: 'local',
        typeAhead: true
    }]
});