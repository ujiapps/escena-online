Ext.define('Paranimf.view.plantillareservas.PanelPlantillasReservas', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelPlantillasReservas',
    layout: 'border',
    border: false,

    items: [{
        flex: 1,
        region: 'center',
        autoScroll: true,
        xtype: 'gridPlantillasReservas'
    }]
});