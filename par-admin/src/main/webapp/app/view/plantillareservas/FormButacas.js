Ext.define('Paranimf.view.plantillareservas.FormButacas', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formButacas',
    layout: 'fit',

    url: urlPrefix + 'plantillareservas/butacas',

    defaults: {
        anchor: '100%',
        xtype: 'textfield',
        labelWidth: 190
    },

    items: [{
        xtype: 'panel',
        autoScroll: true,
        layout: 'card',
        border: false,
        frame: false,
        items: [{
            id: 'iframeButacasReservas',
            xtype: 'component',
            autoEl: {
                tag: 'iframe',
                src: ''
            }
        }]
    }]
});
