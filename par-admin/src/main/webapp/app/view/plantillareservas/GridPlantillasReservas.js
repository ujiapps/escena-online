Ext.define('Paranimf.view.plantillareservas.GridPlantillasReservas', {
    extend: 'Paranimf.view.EditBaseGrid',

    alias: 'widget.gridPlantillasReservas',
    store: 'PlantillasReservas',
    stateId: 'gridPlantillasReservas',

    title: UI.i18n.gridTitle.plantillasreservas,

    dockedItems: [{
        xtype: 'pagingtoolbar',
        dock: 'bottom',
        store: 'PlantillasReservas',
        displayInfo: true
    }],

    tbar: [{
        xtype: 'button',
        text: UI.i18n.button.add,
        action: 'add',
        cls: 'btn-add',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }, {
        xtype: 'button',
        text: (readOnlyUser == undefined || readOnlyUser == false) ? UI.i18n.button.edit : UI.i18n.button.ver,
        cls: 'btn-edit',
        action: 'edit'
    }, {
        xtype: 'button',
        text: UI.i18n.button.del,
        action: 'del',
        cls: 'btn-delete',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }, {
        xtype: 'button',
        text: UI.i18n.button.editButacas,
        action: 'editButacas',
        cls: 'btn-edit-butacas',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }],

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: 'sala',
            hidden: true,
            text: UI.i18n.field.idSala
        }, {
            dataIndex: 'nombre',
            text: UI.i18n.field.nameMulti,
            flex: 1
        }, {
            dataIndex: 'nombreSala',
            sortable: false,
            text: UI.i18n.field.sala,
            flex: 1
        }];

        this.callParent(arguments);
    },


    showAddPlantillaWindow: function () {
        this.createPercentageModalWindow('formPlantillasReservas').show();
    }
});