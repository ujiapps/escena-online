Ext.define('Paranimf.view.integraciones.PanelIntegraciones', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelIntegraciones',
    frame: false,
    border: false,
    layout: 'hbox',
    title: UI.i18n.formTitle.integraciones,

    items: [{
        flex: 1,
        autoScroll: true,
        xtype: 'formIntegraciones',
        split: true
    }]
});