Ext.define('Paranimf.view.integraciones.FormIntegraciones', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formIntegraciones',

    url: urlPrefix + '',

    defaults: {
        anchor: '100%'
    },

    buttons: [{
        xtype: 'button',
        text: UI.i18n.button.download,
        action: 'download'
    }],

    items: [{
        xtype: 'fieldset',
        flex: 1,
        layout: 'anchor',
        padding: '5px 3px 5px 5px',
        title: 'APP Android',
        cls: 'fieldset-app',
        defaults: {
            anchor: '100%',
            labelWidth: 250,
            xtype: 'textfield'
        },
        items: [{
            xtype: 'container',
            cls: 'lector-externo',
            html: '<p>' + UI.i18n.field.qrConfig + '</p>'
        }, {
            name: 'qr',
            xtype: 'container'
        }, {
            xtype: 'textfield',
            name: 'urlConexion',
            cls: 'url-conexion',
            readOnly: true,
            fieldLabel: UI.i18n.field.urlConexion
        }, {
            xtype: 'textfield',
            name: 'port',
            cls: 'port',
            readOnly: true,
            fieldLabel: UI.i18n.field.port
        }, {
            xtype: 'textfield',
            name: 'apiKey',
            readOnly: true,
            cls: 'api-key',
            fieldLabel: 'API key'
        }, {
            xtype: 'textfield',
            readOnly: true,
            cls: 'lector-externo',
            fieldLabel: UI.i18n.field.externalReader,
            value: UI.i18n.message.externalReader
        }]
    }]
});