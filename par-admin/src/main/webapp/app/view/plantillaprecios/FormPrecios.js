Ext.define('Paranimf.view.plantillaprecios.FormPrecios', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formPrecios',

    defaults: {
        allowBlank: false,
        msgTarget: 'side',
        labelWidth: 160,
        anchor: '100%',
        xtype: 'textfield'
    },

    items: [{
        name: 'id',
        hidden: true,
        allowBlank: true
    }, {
        fieldLabel: UI.i18n.field.plantillaprecios,
        name: 'plantillaPrecios',
        xtype: 'combobox',
        forceSelection: true,
        displayField: 'nombre',
        valueField: 'id',
        store: 'PlantillasPrecios',
        readOnly: true,
        queryMode: 'local',
        typeAhead: true
    }, {
        fieldLabel: UI.i18n.field.localizacion,
        name: 'localizacion',
        xtype: 'combobox',
        forceSelection: true,
        displayField: lang == 'ca' ? 'nombreVa' : 'nombreEs',
        valueField: 'id',
        store: 'Localizaciones',
        disabled: true,
        queryMode: 'local',
        typeAhead: true
    }, {
        fieldLabel: UI.i18n.field.tarifa,
        name: 'tarifa',
        xtype: 'combobox',
        forceSelection: true,
        displayField: 'nombre',
        valueField: 'id',
        store: 'TarifasCombo',
        disabled: true,
        queryMode: 'local',
        typeAhead: true
    }, {
        fieldLabel: UI.i18n.field.precio,
        name: 'precio',
        minValue: 0,
        xtype: 'numericfield',
        decimalPrecision: 2,
        alwaysDisplayDecimals: true
    }, {
        fieldLabel: UI.i18n.field.precioAnticipado,
        hidden: !ventaAnticipada,
        name: 'precioAnticipado',
        minValue: 0,
        allowBlank: true,
        xtype: 'numericfield',
        decimalPrecision: 2,
        alwaysDisplayDecimals: true
    }]
});