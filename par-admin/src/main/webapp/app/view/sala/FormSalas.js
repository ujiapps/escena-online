Ext.define('Paranimf.view.sala.FormSalas', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formSalas',
    fileUpload: true,
    enctype: 'multipart/form-data',

    url: urlPrefix + 'salas',

    defaults: {
        allowBlank: false,
        msgTarget: 'side',
        labelWidth: 90,
        anchor: '100%',
        xtype: 'textfield',
        flex: 1
    },

    buttons: [{
        xtype: 'button',
        text: UI.i18n.button.save,
        action: 'save',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }, {
        xtype: 'button',
        text: UI.i18n.button.cancel,
        handler: function () {
            this.up('window').close();
        }
    }],

    items: [
        {
            name: 'id',
            hidden: true,
            allowBlank: true
        },
        {
            name: 'aforoPorCompras',
            hidden: true,
            allowBlank: true
        },
        {
            fieldLabel: UI.i18n.field.name,
            name: 'nombre',
            allowBlank: false
        },
        {
            fieldLabel: UI.i18n.field.direccion,
            name: 'direccion',
            allowBlank: true
        }]
});
