Ext.define('Paranimf.view.sala.PanelSalas', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelSalas',
    frame: false,
    border: false,
    layout: 'border',

    items: [{
        flex: 1,
        region: 'north',
        autoScroll: true,
        xtype: 'gridSalas',
        split: true,
        collapsible: true
    }, {
        flex: 1,
        region: 'center',
        autoScroll: true,
        xtype: 'gridLocalizaciones',
        style: 'margin-top: 1em;'
    }]
});