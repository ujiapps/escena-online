Ext.define('Paranimf.view.sala.FormLocalizaciones', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formLocalizaciones',

    url: urlPrefix + 'localizaciones',

    defaults: {
        anchor: '100%',
        xtype: 'textfield',
        labelWidth: 190
    },

    items: [{
        name: 'id',
        hidden: true,
        allowBlank: true
    },
        {
            xtype: 'fieldset',
            flex: 1,
            title: UI.i18n.field.tituloMulti,
            defaults: {
                xtype: 'textfield',
                anchor: '100%'
            },
            items: [{
                fieldLabel: UI.i18n.field.name,
                name: 'nombreEs',
                hidden: langsAllowed && !langsAllowed.some(item => item.locale === 'es' || item.locale === 'gl'),
                allowBlank: langsAllowed && !langsAllowed.some(item => item.locale === 'es' || item.locale === 'gl'),
            }, {
                fieldLabel: UI.i18n.field.name_va,
                name: 'nombreVa',
                hidden: langsAllowed && !langsAllowed.some(item => item.locale === 'ca'),
                allowBlank: langsAllowed && !langsAllowed.some(item => item.locale === 'ca'),
            }]
        },
        {
            xtype: 'fieldset',
            flex: 1,
            title: UI.i18n.field.asientos,
            layout: 'hbox',
            defaults: {
                xtype: 'textfield',
                margins: '5px 5px 5px 5px'
            },
            items: [{
                fieldLabel: UI.i18n.field.totalEntradas,
                xtype: 'numberfield',
                labelWidth: 150,
                name: 'totalEntradas',
                allowBlank: false
            },
                {
                    fieldLabel: UI.i18n.field.asientosDiscapacitados,
                    xtype: 'checkbox',
                    labelWidth: 230,
                    name: 'discapacitados',
                    allowBlank: false
                }]
        }]
});