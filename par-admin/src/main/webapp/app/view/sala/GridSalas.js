Ext.define('Paranimf.view.sala.GridSalas', {
    extend: 'Paranimf.view.EditBaseGrid',

    requires: [
        'FilterField.filters.Filter',
        'FilterField.button.OperatorButton'
    ],

    plugins: {
        ptype: 'filterfield'
    },

    alias: 'widget.gridSalas',
    store: 'SalasNoNumeradas',
    stateId: 'gridSalas',

    title: UI.i18n.gridTitle.salasNoNumeradas,

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'SalasNoNumeradas',
        dock: 'bottom',
        displayInfo: true
    }],

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: 'nombre',
            text: lang && lang === 'ca' ? UI.i18n.field.name_va : UI.i18n.field.name,
            flex: 5
        }, {
            dataIndex: 'direccion',
            text: UI.i18n.field.direccion,
            flex: 5
        }];

        this.callParent(arguments);
    },

    showAddSalaWindow: function () {
        this.createPercentageModalWindow('formSalas', undefined, undefined).show();
    }
});