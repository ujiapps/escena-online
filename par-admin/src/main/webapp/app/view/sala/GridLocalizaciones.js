Ext.define('Paranimf.view.sala.GridLocalizaciones', {
    extend: 'Paranimf.view.EditBaseGrid',

    requires: [
        'FilterField.filters.Filter',
        'FilterField.button.OperatorButton'
    ],

    plugins: {
        ptype: 'filterfield'
    },

    alias: 'widget.gridLocalizaciones',
    store: 'LocalizacionesSalasNoNumeradas',

    title: UI.i18n.gridTitle.localizaciones,
    stateId: 'gridLocalizaciones',


    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'LocalizacionesSalasNoNumeradas',
        dock: 'bottom',
        displayInfo: true
    }],

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: lang && lang === 'ca' ? 'nombreVa' : 'nombreEs',
            text: lang && lang === 'ca' ? UI.i18n.field.name_va : UI.i18n.field.name,
            flex: 5
        }, {
            dataIndex: 'totalEntradas',
            text: UI.i18n.field.asientos,
            flex: 3
        }];

        this.callParent(arguments);
    },

    showAddLocalizacionesWindow: function () {
        this.createPercentageModalWindow('formLocalizaciones', undefined, undefined).show();
    }
});