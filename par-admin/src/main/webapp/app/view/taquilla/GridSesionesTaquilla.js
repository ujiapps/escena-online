Ext.define('Paranimf.view.taquilla.GridSesionesTaquilla', {
    extend: 'Paranimf.view.EditBaseGrid',

    requires: [
        'FilterField.filters.Filter',
        'FilterField.button.OperatorButton'
    ],

    plugins: {
        ptype: 'filterfield'
    },

    alias: 'widget.gridSesionesTaquilla',
    store: 'SesionesTaquilla',
    stateId: 'gridSesionesTaquilla',

    title: UI.i18n.gridTitle.sesionesCompras,

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'SesionesTaquilla',
        dock: 'bottom',
        displayInfo: true
    }],

    tbar: [{
        xtype: 'button',
        text: UI.i18n.button.venderTaquilla,
        cls: 'btn-comprar',
        action: 'comprar',
        hidden: compraTaquillaDisabled
    }, {
        xtype: 'button',
        text: UI.i18n.button.reservar,
        cls: 'btn-reservar',
        action: 'reservar'
    }, {
        xtype: 'checkbox',
        fieldLabel: UI.i18n.field.sesionesAcabadas,
        name: 'mostrarTodas',
        cls: 'btn-mostrar-todas',
        labelWidth: 250,
        labelAlign: 'right'
    }
    ],

    viewConfig: {
        enableTextSelection: true,
        getRowClass: function (record) {
            if (record && record.data.anulada)
                return 'gridAnulada'
        }
    },

    comprar: function () {
        console.log('COMPRAR ENTRADA');
    },

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: 'fechaCelebracion',
            text: UI.i18n.field.eventDate,
            filter: {
                xtype: 'datefield',
                editable: false,
                startDay: 1
            },
            flex: 1,
            renderer: function (val, p, rec) {
                return (rec.data.multisesion ? UI.i18n.field.multiDays + ': ' : '') + rec.data.fechaCelebracionLabel;
            }
        }, {
            dataIndex: 'horaApertura',
            text: UI.i18n.field.opening,
            flex: 0.5
        }, {
            dataIndex: 'fechaInicioVentaOnline',
            text: UI.i18n.field.startOnlineSelling,
            xtype: 'datecolumn',
            flex: 1,
            renderer: function (val, p, rec) {
                return (!rec.data.canalInternet) ? '-' : Ext.util.Format.date(val, 'd/m/Y H:i');
            }
        }, {
            dataIndex: 'fechaFinVentaOnline',
            text: UI.i18n.field.endOnlineSelling,
            xtype: 'datecolumn',
            flex: 1,
            renderer: function (val, p, rec) {
                return (!rec.data.canalInternet) ? '-' : Ext.util.Format.date(val, 'd/m/Y H:i');
            }
        }, {
            dataIndex: 'butacasVendidas',
            text: UI.i18n.field.butacasVendidas,
            sortable: false,
            flex: 0.5
        }, {
            dataIndex: 'butacasReservadas',
            sortable: false,
            text: UI.i18n.field.butacasReservadas,
            flex: 0.5
        }, {
            dataIndex: 'butacasDisponibles',
            sortable: false,
            text: UI.i18n.field.butacasDisponibles,
            flex: 0.5,
            renderer: function (val, p, rec) {
                return rec.data.multisesion ? '-' : val;
            }
        }];

        this.callParent(arguments);
    },

    showComprarWindow: function (idSesion, asientosNumerados, title, modoReserva) {
        //console.log("showComprarWindow: ", idSesion);
        var window = this.createPercentageModalWindow('formComprar', 0.95, 0.95, title).show();
        var cardLayout = Ext.getCmp('pasoSeleccionar').getLayout();

        if (asientosNumerados) {
            cardLayout.setActiveItem(0);
            var url = urlPublic + "/rest/entrada/butacasFragment/" + idSesion + "?if=true";

            if (modoReserva)
                url += '&reserva=true';

            Ext.getDom('iframeButacas').src = url;
        } else
            cardLayout.setActiveItem(1);
    }
});