Ext.define('Paranimf.view.cine.FormDatosRegistro', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formDatosRegistro',

    url: urlPrefix + '',

    defaults: {
        anchor: '100%',
        xtype: 'textfield',
        labelWidth: 190
    },

    buttons: [{
        xtype: 'button',
        text: UI.i18n.button.enviarMailVerificacion,
        action: 'resend',
        cls: 'btn-reenviar-email-verificacion',
        hidden: true
    }, {
        xtype: 'button',
        text: UI.i18n.button.save,
        action: 'save',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }],

    items: [{
        name: 'id',
        hidden: true,
        allowBlank: true
    },
        {
            xtype: 'fieldset',
            flex: 1,
            title: UI.i18n.field.datosFiscales,
            layout: 'column',
            defaults: {
                xtype: 'textfield',
                margin: '5px 5px 5px 5px'
            },
            items: [{
                fieldLabel: UI.i18n.field.cif,
                name: 'cif',
                allowBlank: false,
                columnWidth: 1 / 3
            }, {
                fieldLabel: UI.i18n.field.empresa,
                name: 'empresa',
                allowBlank: false,
                columnWidth: 2 / 3
            }]
        },

        {
            xtype: 'fieldset',
            flex: 1,
            title: UI.i18n.field.localizacion,
            defaults: {
                xtype: 'textfield',
                labelWidth: 160,
                width: 600
            },
            items: [{
                fieldLabel: UI.i18n.field.cp,
                name: 'cp',
                allowBlank: false
            }, {
                fieldLabel: UI.i18n.field.direccion,
                name: 'direccion',
                allowBlank: false
            }, {
                fieldLabel: UI.i18n.field.codMunicipio,
                name: 'codMunicipio',
                allowBlank: false
            }, {
                fieldLabel: UI.i18n.field.municipio,
                name: 'municipio',
                allowBlank: false
            }, {
                fieldLabel: UI.i18n.field.telefono,
                name: 'telefono'
            }]
        },


        {
            xtype: 'fieldset',
            title: UI.i18n.field.logo,
            items: [{
                name: 'logo',
                anchor: '100%',
                allowBlank: true,
                fieldLabel: UI.i18n.field.uploadLogo,
                labelWidth: 160,
                width: 600,
                msgTarget: 'side',
                xtype: 'filefield',
                buttonText: '...'
            }, {
                xtype: 'label',
                id: 'logoInsertado'
            }]
        },
        {
            xtype: 'fieldset',
            title: UI.i18n.field.imagenBanner,
            items: [{
                name: 'banner',
                anchor: '100%',
                allowBlank: true,
                fieldLabel: UI.i18n.field.uploadBanner,
                labelWidth: 160,
                width: 600,
                msgTarget: 'side',
                xtype: 'filefield',
                buttonText: '...'
            }, {
                xtype: 'label',
                id: 'bannerInsertado'
            }]
        },
        {
            xtype: 'fieldset',
            flex: 1,
            hidden: true,
            title: UI.i18n.field.acceso,
            defaults: {
                xtype: 'textfield',
                labelWidth: 160,
                width: 600
            },
            items: [{
                xtype: 'panel',
                flex: 1,
                border: false,
                layout: 'hbox',
                margin: '0px 0px 5px 0px',
                defaults: {
                    xtype: 'textfield'
                },
                items: [{
                    fieldLabel: UI.i18n.field.email,
                    name: 'email',
                    labelWidth: 160,
                    width: 600,
                    allowBlank: false,
                    disabled: true,
                    readOnly: true
                }]
            }, {
                fieldLabel: UI.i18n.field.password,
                name: 'password',
                inputType: 'password'
            }, {
                fieldLabel: UI.i18n.field.passwordRepeat,
                name: 'passwordRepeat',
                inputType: 'password'
            }]
        }]
});