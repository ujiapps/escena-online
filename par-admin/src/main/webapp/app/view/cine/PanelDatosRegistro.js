Ext.define('Paranimf.view.cine.PanelDatosRegistro', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelDatosRegistro',
    frame: false,
    border: false,
    layout: 'hbox',
    title: UI.i18n.gridTitle.datosRegistro,

    items: [{
        flex: 1,
        autoScroll: true,
        xtype: 'formDatosRegistro',
        split: true
    }]
});