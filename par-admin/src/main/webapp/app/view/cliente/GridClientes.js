Ext.define('Paranimf.view.cliente.GridClientes', {
    extend: 'Paranimf.view.EditBaseGrid',

    requires: [
        'FilterField.filters.Filter',
        'FilterField.button.OperatorButton'
    ],

    plugins: {
        ptype: 'filterfield'
    },

    alias: 'widget.gridClientes',
    store: 'Clientes',

    title: UI.i18n.gridTitle.client,
    stateId: 'gridClientes',

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Clientes',
        dock: 'bottom',
        displayInfo: true
    }],

    tbar: [{
        xtype: 'button',
        text: UI.i18n.button.emails,
        cls: 'btn-copiar-emails',
        action: 'copyEmails'
    }, {
        xtype: 'button',
        text: UI.i18n.button.del,
        cls: 'btn-del-client',
        action: 'delClient'
    }],

    viewConfig: {
        enableTextSelection: true
    },

    initComponent: function () {
        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: 'nombre',
            text: UI.i18n.field.name,
            filter: {
                xtype: 'textfield'
            },
            flex: 4
        }, {
            dataIndex: 'apellidos',
            text: UI.i18n.field.surnameMulti,
            filter: {
                xtype: 'textfield'
            },
            flex: 4
        }, {
            dataIndex: 'direccion',
            text: UI.i18n.field.address,
            filter: {
                xtype: 'textfield'
            },
            flex: 3
        }, {
            dataIndex: 'poblacion',
            text: UI.i18n.field.poblacion,
            filter: {
                xtype: 'textfield'
            },
            flex: 3
        }, {
            dataIndex: 'cp',
            text: UI.i18n.field.codigoPostal,
            filter: {
                xtype: 'textfield'
            },
            flex: 3
        }, {
            dataIndex: 'provincia',
            text: UI.i18n.field.provincia,
            filter: {
                xtype: 'textfield'
            },
            flex: 3
        }, {
            dataIndex: 'telefono',
            text: UI.i18n.field.phoneMobile,
            filter: {
                xtype: 'textfield'
            },
            flex: 3
        }, {
            dataIndex: 'email',
            text: UI.i18n.field.email,
            filter: {
                xtype: 'textfield'
            },
            flex: 4
        }, {
            dataIndex: 'fecha',
            text: UI.i18n.field.date,
            format: 'd/m/Y H:i',
            xtype: 'datecolumn',
            filter: {
                xtype: 'datefield',
                editable: false,
                startDay: 1
            },
            flex: 3
        }];

        if (showComoNosConociste) {
            this.columns.push({
                dataIndex: 'comoNosConociste',
                text: UI.i18n.field.comoNosConociste,
                filter: {
                    xtype: 'textfield'
                },
                flex: 6
            });
        }

        this.callParent(arguments);
    }
});