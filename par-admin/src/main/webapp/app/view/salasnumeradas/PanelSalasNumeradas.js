Ext.define('Paranimf.view.salasnumeradas.PanelSalasNumeradas', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelSalasNumeradas',
    layout: 'border',
    border: false,

    items: [{
        flex: 1,
        region: 'center',
        autoScroll: true,
        xtype: 'gridSalasNumeradas'
    }]
});