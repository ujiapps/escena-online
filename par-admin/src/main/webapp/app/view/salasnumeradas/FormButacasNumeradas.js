Ext.define('Paranimf.view.salasnumeradas.FormButacasNumeradas', {
    extend: 'Paranimf.view.EditBaseForm',
    alias: 'widget.formButacasNumeradas',
    layout: 'fit',

    buttons: [{
        xtype: 'button',
        text: UI.i18n.button.cancel,
        cls: 'btn-cancelar',
        action: 'cancel',
        handler: function () {
            this.up('window').close();
        }
    }],

    defaults: {
        anchor: '100%',
        xtype: 'textfield',
        labelWidth: 190
    },

    items: [{
        xtype: 'panel',
        autoScroll: true,
        layout: 'card',
        border: false,
        frame: false,
        items: [{
            id: 'iframeButacasReservas',
            xtype: 'component',
            autoEl: {
                tag: 'iframe',
                src: ''
            }
        }]
    }]
});
