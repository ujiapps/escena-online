Ext.define('Paranimf.view.salasnumeradas.GridSalasNumeradas', {
    extend: 'Paranimf.view.EditBaseGrid',

    alias: 'widget.gridSalasNumeradas',
    store: 'SalasNumeradas',
    stateId: 'gridSalasNumeradas',

    title: UI.i18n.gridTitle.salasNumeradas,

    dockedItems: [{
        xtype: 'pagingtoolbar',
        dock: 'bottom',
        store: 'SalasNumeradas',
        displayInfo: true
    }],

    tbar: [{
        xtype: 'button',
        text: UI.i18n.button.verButacas,
        action: 'verButacas',
        cls: 'btn-edit-butacas',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }],

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: 'nombre',
            text: UI.i18n.field.nameMulti,
            flex: 1
        }, {
            dataIndex: 'urlComoLlegar',
            text: UI.i18n.field.urlComoLlegar,
            flex: 1,
            renderer: function (val, record, p) {
                if (val != undefined) {
                    return '<a href="' + val + '" target="blank">' + val + '</a>'
                }
            }
        }];

        this.callParent(arguments);
    }
});