Ext.define('Paranimf.view.evento.GridSesiones', {
    extend: 'Paranimf.view.EditBaseGrid',

    requires: [
        'FilterField.filters.Filter',
        'FilterField.button.OperatorButton'
    ],

    plugins: {
        ptype: 'filterfield'
    },

    alias: 'widget.gridSesiones',
    store: 'Sesiones',

    title: UI.i18n.gridTitle.sesiones,
    stateId: 'gridSesiones',

    tbar: [{
        xtype: 'button',
        text: UI.i18n.button.add,
        action: 'add',
        cls: 'btn-add',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }, {
        xtype: 'button',
        cls: 'btn-edit',
        text: (readOnlyUser == undefined || readOnlyUser == false) ? UI.i18n.button.edit : UI.i18n.button.ver,
        action: 'edit'
    }, {
        xtype: 'button',
        cls: 'btn-delete',
        text: UI.i18n.button.anularSesion,
        action: 'del',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }, {
        xtype: 'checkbox',
        cls: 'btn-mostrar-todas',
        fieldLabel: UI.i18n.field.sesionesAcabadas,
        name: 'mostrarTodas',
        labelWidth: 250,
        labelAlign: 'right'
    }],

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Sesiones',
        dock: 'bottom',
        displayInfo: true
    }],

    viewConfig: {
        enableTextSelection: true,
        getRowClass: function (record) {
            if (record && record.data.anulada)
                return 'gridAnulada'
        }
    },

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: 'fechaCelebracion',
            text: UI.i18n.field.eventDate,
            filter: {
                xtype: 'datefield',
                editable: false,
                startDay: 1
            },
            flex: 1,
            renderer: function (val, p, rec) {
                return (rec.data.multisesion ? UI.i18n.field.multiDays + ': ' : '') + rec.data.fechaCelebracionLabel;
            }
        }, {
            dataIndex: 'horaApertura',
            text: UI.i18n.field.opening,
            hidden: true,
            flex: 1
        }, {
            dataIndex: 'fechaInicioVentaOnline',
            text: UI.i18n.field.startOnlineSelling,
            xtype: 'datecolumn',
            flex: 1,
            renderer: function (val, p, rec) {
                return (!rec.data.canalInternet) ? '-' : Ext.util.Format.date(val, 'd/m/Y H:i');
            }
        }, {
            dataIndex: 'fechaFinVentaOnline',
            text: UI.i18n.field.endOnlineSelling,
            xtype: 'datecolumn',
            flex: 1,
            renderer: function (val, p, rec) {
                return (!rec.data.canalInternet) ? '-' : Ext.util.Format.date(val, 'd/m/Y H:i');
            }
        }, {
            dataIndex: 'plantillaPrecios_nombre',
            text: UI.i18n.field.plantillaprecios,
            sortable: false,
            flex: 1
        }, {
            dataIndex: 'salaNombre',
            text: UI.i18n.field.sala,
            sortable: false,
            flex: 1
        }];

        this.callParent(arguments);
    },

    showAddSesionWindow: function () {
        this.createPercentageModalWindow('formSesiones', undefined, 0.8).show();
    }
});