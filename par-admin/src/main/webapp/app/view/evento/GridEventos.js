Ext.define('Paranimf.view.evento.GridEventos', {
    extend: 'Paranimf.view.EditBaseGrid',

    requires: [
        'FilterField.filters.Filter',
        'FilterField.button.OperatorButton'
    ],

    plugins: {
        ptype: 'filterfield'
    },

    alias: 'widget.gridEventos',
    store: 'Eventos',
    stateId: 'gridEventos',

    title: UI.i18n.gridTitle.eventos,

    tbar: [{
        xtype: 'button',
        text: UI.i18n.button.add,
        action: 'add',
        cls: 'btn-add',
        hidden: (readOnlyUser == undefined) ? false : readOnlyUser
    }, {
        xtype: 'button',
        cls: 'btn-edit',
        text: (readOnlyUser == undefined || readOnlyUser == false) ? UI.i18n.button.edit : UI.i18n.button.ver,
        action: 'edit'
    }, {
        xtype: 'checkbox',
        cls: 'btn-mostrar-todos',
        fieldLabel: UI.i18n.field.eventosAcabados,
        name: 'mostrarTodos',
        labelWidth: 180,
        labelAlign: 'right'
    }],

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Eventos',
        dock: 'bottom',
        displayInfo: true
    }],

    initComponent: function () {

        this.columns = [{
            dataIndex: 'id',
            hidden: true,
            text: UI.i18n.field.idIntern
        }, {
            dataIndex: 'asientosNumerados',
            hidden: true,
            text: UI.i18n.field.asientosNumerados,
            renderer: function (val) {
                if (val == 0)
                    return 'No';
                else
                    return 'Sí';
            }
        }, {
            flex: 2,
            dataIndex: 'fechaPrimeraSesion',
            text: UI.i18n.field.fechaPrimeraSesion,
            renderer: function (val) {
                if (val != '' && val != undefined) {
                    var dt = new Date(val);
                    return Ext.Date.format(dt, 'd/m/Y H:i');
                }
                return '';
            }
        }, {
            dataIndex: 'parTiposEvento',
            text: UI.i18n.field.type,
            flex: 2,
            renderer: function (val, p) {
                return lang && lang === 'ca' ? val["nombreVa"] : val["nombreEs"];
            }
        }, {
            dataIndex: lang && lang === 'ca' ? 'tituloVa' : 'tituloEs',
            text: lang && lang === 'ca' ? UI.i18n.field.title_va : UI.i18n.field.title,
            filter: {
                xtype: 'textfield'
            },
            flex: 5
        }, {
            dataIndex: 'imagenSrc',
            text: UI.i18n.field.imagen,
            flex: 3,
            renderer: function (val, record, p) {
                if (val != undefined) {
                    return '<a href="' + urlPrefix + 'evento/' + p.data.id + '/imagen" target="blank">' + UI.i18n.field.imagenInsertada + '</a>'
                }
            }
        }, {
            dataIndex: 'imagenPubliSrc',
            text: UI.i18n.field.imagenPubli,
            flex: 3,
            renderer: function (val, record, p) {
                if (val != undefined) {
                    return '<a href="' + urlPrefix + 'evento/' + p.data.id + '/imagenPubli" target="blank">' + UI.i18n.field.imagenPubliInsertada + '</a>'
                }
            }
        }, {
            dataIndex: 'rssId',
            text: UI.i18n.field.rssId,
            hidden: true
        }, {
            text: UI.i18n.field.urlPublica,
            dataIndex: 'id',
            flex: 4,
            renderer: function (val, record, p) {
                if (val != undefined) {
                    return '<a href="' + urlPublic + '/rest/evento/id/' + val + '" target="blank">' + urlPublic + '/rest/evento/id/' + val + '</a>'
                }
            }
        }];

        this.callParent(arguments);
    },

    showAddEventoWindow: function () {
        var window = this.createPercentageModalWindow('formEventos', undefined, 0.8);
        window.header = {
            titlePosition: 0,
            items: [
                {
                    xtype: 'button',
                    text: UI.i18n.button.help,
                    action: 'help'
                }
            ]
        };
        window.show();
    }
});