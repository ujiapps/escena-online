Ext.define('Paranimf.view.informes.PanelInformes', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelInformes',
    frame: false,
    border: false,
    layout: 'border',

    items: [
        {
            flex: 1,
            region: 'center',
            split: true,
            collapsible: true,
            defaults: {
                border: 0,
                xtype: 'panel',
                layout: 'hbox',
                margin: '5px',
            },
            xtype: 'panel',
            layout: 'vbox',
            title: UI.i18n.message.informesGeneralsCompras,
            items: [
                {
                    items: [{
                        xtype: 'datefield',
                        name: 'fechaInicio',
                        labelWidth: 150,
                        startDay: 1,
                        fieldLabel: UI.i18n.field.startDateCompra,
                        margin: '0px 10px 0px 0px'
                    }, {
                        xtype: 'datefield',
                        name: 'fechaFin',
                        labelWidth: 150,
                        startDay: 1,
                        fieldLabel: UI.i18n.field.endDateCompra,
                        margin: '0px 10px 0px 0px'
                    }]
                }, {
                    items: [{
                        fieldLabel: UI.i18n.field.tpv,
                        hidden: true,
                        name: 'tpv',
                        labelWidth: 150,
                        width: 500,
                        xtype: 'combobox',
                        forceSelection: true,
                        allowBlank: true,
                        displayField: 'nombre',
                        valueField: 'id',
                        store: 'TpvsInformes',
                        queryMode: 'remote',
                        typeAhead: true
                    }]
                }, {
                    items: [{
                        xtype: 'combo',
                        labelWidth: 150,
                        width: 500,
                        fieldLabel: UI.i18n.message.informes,
                        store: 'TiposInformesGenerales',
                        name: 'comboInformesGenerales',
                        queryMode: 'remote',
                        forceSelection: true,
                        displayField: 'nombre',
                        valueField: 'id'
                    }]
                }, {
                    items: [{
                        xtype: 'combo',
                        labelWidth: 150,
                        width: 500,
                        fieldLabel: UI.i18n.message.sortersInformes,
                        store: 'SorterInformes',
                        name: 'comboSorterInformes',
                        queryMode: 'local',
                        forceSelection: true,
                        displayField: 'nombre',
                        valueField: 'id'
                    }]
                }, {
                    items: [{
                        action: 'generarInformeGeneral',
                        xtype: 'button',
                        cls: 'btn-generar-informe-general',
                        text: UI.i18n.button.generarInforme
                    }]
                }]
        },
        {
            hidden: !informesGeneralSesion,
            flex:  1,
            region: 'south',
            defaults: {
                border: 0,
                xtype: 'panel',
                layout: 'hbox',
                margin: '5px',
            },
            xtype: 'panel',
            layout: 'vbox',
            title: UI.i18n.message.informesGenerals,
            items: [
                {
                    items: [{
                        xtype: 'datefield',
                        name: 'fechaInicioSesion',
                        labelWidth: 150,
                        startDay: 1,
                        fieldLabel: UI.i18n.field.startDateSesion,
                        margin: '0px 10px 0px 0px'
                    }, {
                        xtype: 'datefield',
                        name: 'fechaFinSesion',
                        labelWidth: 150,
                        startDay: 1,
                        fieldLabel: UI.i18n.field.endDateSesion,
                        margin: '0px 10px 0px 0px'
                    }]
                }, {
                    items: [{
                        fieldLabel: UI.i18n.field.tpv,
                        hidden: true,
                        name: 'tpvSesiones',
                        labelWidth: 150,
                        width: 500,
                        xtype: 'combobox',
                        forceSelection: true,
                        allowBlank: true,
                        displayField: 'nombre',
                        valueField: 'id',
                        store: 'TpvsInformes',
                        queryMode: 'remote',
                        typeAhead: true
                    }]
                }, {
                    items: [{
                        xtype: 'combo',
                        labelWidth: 150,
                        width: 500,
                        fieldLabel: UI.i18n.message.informes,
                        store: 'TiposInformesGeneralesSesion',
                        name: 'comboInformesGeneralesSesion',
                        queryMode: 'remote',
                        forceSelection: true,
                        displayField: 'nombre',
                        valueField: 'id'
                    }]
                }, {
                    items: [{
                        xtype: 'combo',
                        labelWidth: 150,
                        width: 500,
                        fieldLabel: UI.i18n.message.sortersInformes,
                        store: 'SorterInformesSesion',
                        name: 'comboSorterInformesSesion',
                        queryMode: 'local',
                        forceSelection: true,
                        displayField: 'nombre',
                        valueField: 'id'
                    }]
                }, {
                    items: [{
                        action: 'generarInformeGeneralSesion',
                        xtype: 'button',
                        cls: 'btn-generar-informe-sesion',
                        text: UI.i18n.button.generarInforme
                    }]
                }]
        }
    ]
});