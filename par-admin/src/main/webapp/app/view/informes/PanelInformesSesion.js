Ext.define('Paranimf.view.informes.PanelInformesSesion', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelInformesSesion',
    frame: false,
    border: false,
    layout: 'border',

    items: [{
        border: false,
        region: 'north',
        autoScroll: false,
        xtype: 'panel',
        defaults: {
            border: 0,
            xtype: 'panel',
            layout: 'hbox',
            margin: '0px 0px 10px 0px'
        },
        items: [{
            items: [{
                xtype: 'datefield',
                name: 'fechaInicio',
                startDay: 1,
                fieldLabel: UI.i18n.field.startDate,
                margin: '0px 10px 0px 0px'
            }, {
                xtype: 'datefield',
                name: 'fechaFin',
                startDay: 1,
                fieldLabel: UI.i18n.field.endDate,
                margin: '0px 10px 0px 0px'
            }, {
                xtype: 'button',
                action: 'filtrarSessions',
                text: UI.i18n.button.filtrarSessions
            }]
        }
        ]
    }, {
        region: 'center',
        xtype: 'gridSesionesInformes'
    }]
});