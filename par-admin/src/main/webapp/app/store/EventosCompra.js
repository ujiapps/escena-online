Ext.define('Paranimf.store.EventosCompra', {
    extend: 'Ext.data.Store',
    model: 'Paranimf.model.Evento',

    sorters: [{property: 'fechaPrimeraSesion', direction: 'DESC'}],
    autoLoad: false,
    autoSync: false,
    pageSize: 20,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: urlPrefix + 'evento/taquilla?activos=true',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    }
});