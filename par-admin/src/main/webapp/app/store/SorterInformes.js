Ext.define('Paranimf.store.SorterInformes',
    {
        extend: 'Ext.data.Store',
        model: 'Paranimf.model.Sorter',

        sorters: ['nombre'],
        autoLoad: false,
        pagesize: 1000,

        proxy: {
            type: 'rest',
            url: urlPrefix + 'informe/sorters',
            reader: {
                type: 'json',
                root: 'data',
                totalProperty: 'total'
            }
        }
    });