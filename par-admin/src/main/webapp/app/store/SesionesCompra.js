Ext.define('Paranimf.store.SesionesCompra', {
    extend: 'Ext.data.Store',
    model: 'Paranimf.model.Sesion',

    sorters: [{
        property: 'fechaCelebracion',
        direction: 'ASC'
    }],

    autoLoad: false,
    autoSync: false,
    pageSize: 20,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: urlPrefix + 'evento/-1/sesiones?activos=true',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    }
});