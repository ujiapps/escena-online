Ext.define('Paranimf.store.Clientes', {
    extend: 'Ext.data.Store',
    model: 'Paranimf.model.Cliente',

    sorters: [{
        property: 'fecha',
        direction: 'ASC'
    }],
    autoLoad: false,
    autoSync: true,
    pageSize: 25,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: urlPrefix + 'clientes',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        },
        writer: {
            type: 'json'
        }
    }
});