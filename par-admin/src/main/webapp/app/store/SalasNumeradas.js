Ext.define('Paranimf.store.SalasNumeradas', {
    extend: 'Ext.data.Store',
    model: 'Paranimf.model.Sala',

    sorters: ['nombre'],
    autoLoad: true,
    autoSync: false,
    pagesize: 1000,
    remoteFilter: true,
    filters: [{
        property: 'asientosNumerados',
        value: true
    }],

    proxy: {
        type: 'rest',
        url: urlPrefix + 'sala',
        extraParams: {
            'asientosNumerados': true
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        },
        writer: {
            type: 'json'
        }
    }
});