Ext.define('Paranimf.store.TpvsInformes', {
    extend: 'Ext.data.Store',
    model: 'Paranimf.model.Tpv',

    sorters: ['nombre'],
    autoLoad: false,
    autoSync: false,
    pagesize: 1000,

    proxy: {
        type: 'rest',
        url: urlPrefix + 'tpv?visibles=true&allOption=true',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        },
        writer: {
            type: 'json'
        }
    }
});