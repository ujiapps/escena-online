Ext.define('Paranimf.store.Dias',
    {
        extend: 'Ext.data.Store',
        model: 'Paranimf.model.HoraMinuto',
        autoLoad: true,
        data: [
            {'label': UI.i18n.storeWeekDay.mon, 'id': 1},
            {'label': UI.i18n.storeWeekDay.tues, 'id': 2},
            {'label': UI.i18n.storeWeekDay.wed, 'id': 3},
            {'label': UI.i18n.storeWeekDay.thurs, 'id': 4},
            {'label': UI.i18n.storeWeekDay.fri, 'id': 5},
            {'label': UI.i18n.storeWeekDay.sat, 'id': 6},
            {'label': UI.i18n.storeWeekDay.sun, 'id': 7}
        ]
    });