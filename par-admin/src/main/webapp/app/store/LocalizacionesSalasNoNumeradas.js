Ext.define('Paranimf.store.LocalizacionesSalasNoNumeradas', {
    extend: 'Ext.data.Store',
    model: 'Paranimf.model.Localizacion',

    sorters: ['nombreEs'],
    autoLoad: false,
    autoSync: false,
    pageSize: 10,
    remoteSort: true,

    proxy: {
        type: 'rest',
        url: urlPrefix + 'localizacion',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        },
        writer: {
            type: 'json'
        }
    }
});