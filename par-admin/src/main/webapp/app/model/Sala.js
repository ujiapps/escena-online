Ext.define('Paranimf.model.Sala', {
    extend: 'Ext.data.Model',

    fields: [
        {name: "id", type: "int"},
        {name: "nombre", type: "string"},
        {name: "direccion", type: "string"},
        {name: "urlComoLlegar", type: "string"},
        {name: "asientos", type: "int"},
        {name: "asientosDiscapacitados", type: "int"},
        {name: "asientosNumerados", type: "boolean"},
        {name: "aforoPorCompras", type: "boolean"}
    ]
});