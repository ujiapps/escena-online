Ext.define('Paranimf.model.Sorter', {
    extend: 'Ext.data.Model',

    fields: [
        'id',
        'nombre'
    ]
});