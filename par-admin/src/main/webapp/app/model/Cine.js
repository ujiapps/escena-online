Ext.define('Paranimf.model.Cine', {
    extend: 'Ext.data.Model',

    fields: [
        'id',
        'cif',
        'empresa',
        'cp',
        'direccion',
        'codMunicipio',
        'municipio',
        'telefono',
        'email',
        'password',
        'url',
        'emailVerificado'
    ]
});