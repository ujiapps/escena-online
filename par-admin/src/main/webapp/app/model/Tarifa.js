Ext.define('Paranimf.model.Tarifa', {
    extend: 'Ext.data.Model',

    fields: [
        'id',
        'nombre',
        'isPublica',
        'defecto'
    ]
});