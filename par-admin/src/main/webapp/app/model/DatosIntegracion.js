Ext.define('Paranimf.model.DatosIntegracion', {
    extend: 'Ext.data.Model',

    fields: [
        'urlConexion',
        'port',
        'apiKey',
        {
            name: 'qr', type: 'string', convert: function (val, row) {
                return row.data.urlConexion + '@@@' + row.data.port + '@@@' + row.data.apiKey;
            }
        }
    ]
});