Ext.define('Paranimf.model.Abono', {
    extend: 'Ext.data.Model',

    fields: [
        'id',
        'nombre',
        'maxEventos',
        'descripcionEs',
        'descripcionVa',
        'imagenContentType',
        'imagenSrc',
        {name: 'tpv', mapping: 'parTpv.id'},
        {name: 'evento', mapping: 'parEvento.id'},
        {name: 'localizacion', mapping: 'parLocalizacion.id'},
        'horaInicioVentaOnline',
        'horaFinVentaOnline',
        'horaFinVentaAnticipada',
        {name: 'fechaInicioVentaOnline', type: 'date', dateFormat: 'U'},
        {name: 'fechaFinVentaOnline', type: 'date', dateFormat: 'U'},
        {name: 'fechaFinVentaAnticipada', type: 'date', dateFormat: 'U'},
        'canalInternet',
        'precio',
        'precioAnticipado',
        {
            name: 'disponibleVentaAnticipada', type: 'boolean', convert: function (val, row) {
                return row.data.precioAnticipado != null;
            }
        }
    ]
});