var tourIntegraciones = {
    id: "Integraciones",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Integración con la app de control de acceso",
            content: "Para acceder a la configuración es necesaro pulsar sobre el icono de ajustes que se muestra con una llave inglesa.</br></br>Si se desea utilizar un lector de códigos QR externo es necesario activar esta opción en el menú de ajustes. En caso contrario se utilizará la propia cámara del dispositivo como lector.",
            target: "div[id^='formIntegraciones']  fieldset[class*='fieldset-app']",
            placement: "bottom"
        },
        {
            title: "API-KEY",
            content: "Introduce este código en la aplicación móvil de control de acceso en al apartado 'API key' para enlazar con el sistema de venta de entradas.",
            target: "div[id^='formIntegraciones'] input[name='apiKey']",
            placement: "bottom"
        }, {
            title: "URL",
            content: "Introduce esta URL en la aplicación móvil de control de acceso en el apartado 'URL de conexión' para enlazar con el sistema de venta de entradas.</br></br>Dejar en blanco el apartado de <b>puerto</b>.",
            target: "div[id^='formIntegraciones'] input[name='urlAppConexion']",
            placement: "bottom"
        }, {
            title: "Descargar app",
            content: "Pulsando sobre el enlace se descarga automáticamente la aplicación.",
            target: "div[id^='formIntegraciones'] label[class*='enlace-apk']",
            placement: "bottom"
        }
    ]
};