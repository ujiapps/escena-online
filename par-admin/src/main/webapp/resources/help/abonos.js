var tourAbonos = {
    id: "Abonos",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Añadir abono",
            content: "Permite añadir abonos que luego podremos asignar en las sesiones con un precio como si de una nueva tarifa se tratara. Además nos permite poner abonos a la venta en la web pública.",
            target: "div[id^='gridAbonos'] div[class*='btn-add']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Editar abono",
            content: "Abre un formulario para modificar el abono seleccionado.</br><b>Es necesario seleccionar al menos un registro de la tabla</b>.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='gridAbonos'] div[class*='btn-edit']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Eliminar abono",
            content: "Elimina el abono seleccionado.",
            target: "div[id^='gridAbonos'] div[class*='btn-del']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Eliminar abonados",
            content: "Elimina la compra del abonado quedando este email como no abonado del abono seleccionado",
            target: "div[id^='gridAbonados'] div[class*='btn-del']:not([style*='display: none'])",
            placement: "bottom"
        }
    ]
};