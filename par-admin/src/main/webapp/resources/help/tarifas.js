var tourTarifas = {
    id: "Tarifas",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Añadir tarifa",
            content: "Añade una tarifa a la aplicación.</br></br>Abre un formulario para introducir los datos necesarios para dar de alta una tarifa.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='gridTarifas'] div[class*='btn-add']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Editar tarifa",
            content: "Abre un formulario para modificar los datos necesarios para una tarifa.</br><b>Es necesario seleccionar al menos un registro de la tabla</b>.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='gridTarifas'] div[class*='btn-edit']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Eliminar tarifa",
            content: "Elimina una tarifa.</br><b>Es necesario seleccionar al menos un registro de la tabla</b>.",
            target: "div[id^='gridTarifas'] div[class*='btn-delete']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Tarifa pública",
            content: "Indica si la tarifa se podrá seleccionar al realizar una compra por un usuario desde la página web pública.</br></br>En caso negativo la tarifa sólo se podrá utilizar para realizar ventas desde taquilla.",
            target: "div[id^='panelTarifas'] div[id^='gridTarifas'] div[id^='gridcolumn']:nth-child(3)",
            placement: "left"
        }
    ]
};