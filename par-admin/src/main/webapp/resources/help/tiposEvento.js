var tourTiposEventos = {
    id: "TiposEventos",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Añadir tipo de evento",
            content: "Añadir un nuevo tipo de evento a la aplicación",
            target: "div[id^='gridTiposEventos'] div[class*='btn-add']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Editar tipo de evento",
            content: "Modificar el tipo evento seleccionado.</br><b>Es necesario seleccionar al menos un registro de la tabla</b>",
            target: "div[id^='gridTiposEventos'] div[class*='btn-edit']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Eliminar tipo de evento",
            content: "Eliminar el tipo de evento seleccionado.</br><b>Es necesario seleccionar al menos un registro de la tabla.</b></br><b>Si un tipo de evento contiene eventos no podrá ser eliminado.</b>",
            target: "div[id^='gridTiposEventos'] div[class*='btn-delete']:not([style*='display: none'])",
            placement: "bottom"
        }
    ]
};