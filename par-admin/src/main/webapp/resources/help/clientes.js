var tourClientes = {
    id: "Clientes",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Copiar e-mails de clientes",
            content: "Copia los e-mails de los clientes que han decidido recibir informaci&oacute;n.",
            target: "div[id^='gridClientes'] div[class*='btn-copiar-emails']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Eliminar cliente",
            content: "Deshabilita la opción de recibir información periódica para el cliente seleccionado",
            target: "div[id^='gridClientes'] div[class*='btn-del-client']:not([style*='display: none'])",
            placement: "bottom"
        }
    ]
};