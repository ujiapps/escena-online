var tourPlantillasReservas = {
    id: "PlantillasReservas",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Añadir plantilla",
            content: "Crea una plantilla con un grupo de butacas sobre una sala numerada que se podrá aplicar a una reserva de forma que no será necesario seleccionar las butacas manualmente para cada localización.</br></br>Abre un formulario para introducir los campos necesarios para dar de alta una nueva plantilla.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='gridPlantillasReservas'] div[class*='btn-add']:not([style*='display: none'])",
            placement: "bottom"
        }, {
            title: "Editar plantilla",
            content: "Abre un formulario para modificar los campos de una plantilla.</br><b>Es necesario seleccionar al menos un registro de la tabla</b>.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='gridPlantillasReservas'] div[class*='btn-edit']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Eliminar plantilla",
            content: "Eliminar la plantilla seleccionada.</br><b>Es necesario seleccionar al menos un registro de la tabla.</b>",
            target: "div[id^='gridPlantillasReservas'] div[class*='btn-delete']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Añadir fila detalle a la plantilla",
            content: "Muestra el patio de butacas con la selección actual de butacas para la plantilla.</br></br>Las butacas en color amarillo son las que se seleccionarán de manera automática cuando al reservar en una sesión apliquemos esta plantilla si no están ocupadas. Si están ocupadas el sistema seleccionará las restantes no ocupadas avisando de que alguna butaca no ha sido posible seleccionarla.",
            target: "div[id^='gridPlantillasReservas'] div[class*='btn-edit-butacas']:not([style*='display: none'])",
            placement: "bottom"
        }
    ]
};