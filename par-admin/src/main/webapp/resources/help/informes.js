var tourInformes = {
    id: "Informes",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Filtrar las sesiones por fecha",
            content: "Utilizando los campos previstos de fecha inicial y fecha final, se obtiene el listado de sesiones que han ocurrido entre esas fechas.",
            target: "div[id^='panelInformes'] input[name='fechaInicio']",
            placement: "bottom"
        },
        {
            title: "Tipos de informe",
            content: "<ul><li>Excel de los eventos: Muestra la recaudación de los eventos, diferenciando ONLINE y de TAQUILLA.</li><li>Excel de taquilla: Muestra la recaudación de cada evento filtrando solo compras de TAQUILLA.</li><li>Excel de taquilla agrupado por compras: Muestra cada compra individual y su tipo en un rango dado.</li><li>Excel de ventas: Muestra la recaudación total de cada evento.</li><li>PDF de TPVs online: Muestra las compras agrupadas por eventos indicando el número de entradasvendidas por evento, presentadas en la puerta y solo las que son online.</li><li>PDF de TPVs taquilla: Muestra las compras agrupadas por eventos indicando el número de entradasvendidas por evento, presentadas en la puerta y solo las que son de TPV taquilla(tipo de pago).</li><li>PDF de mi taquilla: Muestra la compras hechas desde taquilla con mi usuario agrupadas por evento.</li><li>PDF de taquilla: Muestra todas las compras agrupadas por eventos indicando el número de entradasvendidas por evento, presentadas en la puerta.</li><li>PDF de taquilla agrupado por compras: Muestra todas las compras sin agrupar indicando el número de entradas vendidaspor evento, presentadas en la puerta.</li><li>PDF de taquilla en efectivo: Muestra las compras agrupadas por eventos indicando el número de entradasvendidas por evento, presentadas en la puerta y solo las que son pagadas enmetálico (tipo de pago).</li></ul>",
            target: "div[id^='panelInformes'] input[name='comboInformesGenerales']",
            width: 600,
            placement: "bottom"
        },
        {
            title: "Consultar e imprimir informes",
            content: "Genera informe general para todas las sesiones y eventos que cumplan los requisitos de los campos anteriores.",
            target: "div[id^='panelInformes'] div[class*='btn-generar-informe-general']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Consultar e imprimir informes\" de sesión",
            content: "Obtener los mismos informes que la sección anterior pero sobre las sesiones realizadas entra las fechas seleccionadas (compras de cualquier fecha para esas sesiones).",
            target: "div[id^='panelInformes'] div[class*='btn-generar-informe-sesion']:not([style*='display: none'])",
            placement: "bottom"
        }
    ]
};