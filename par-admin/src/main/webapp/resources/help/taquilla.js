var tourTaquilla = {
    id: "Taquilla",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Filtrar por título",
            content: "Utilizando este campo se pueden filtrar los resultados por título",
            target: "div[id^='panelTaquilla'] div[id^='gridEventosTaquilla'] table[class*='filterControl'] input",
            placement: "bottom"
        },
        {
            title: "Realizar venta de entrada/s",
            content: "Para realizar una venta es necesario seleccionar un evento de la tabla superior y una sesión de la tabla inferior.</br></br>Al pulsar sobre este botón se abre la pantalla que permite seleccionar el número de entradas y el tipo de pago.",
            target: "div[id^='panelTaquilla'] div[id^='gridSesionesTaquilla'] div[class*='btn-comprar']:not([style*='display: none'])",
            placement: "top"
        },
        {
            title: "Realizar reserva de entrada/s",
            content: "Para realizar una reserva es necesario seleccionar un evento de la tabla superior y una sesión de la tabla inferior.</br></br>Al pulsar sobre este botón se abre la pantalla que permite seleccionar el número de entradas y las fechas que la reserva estará vigente.",
            target: "div[id^='panelTaquilla'] div[id^='gridSesionesTaquilla'] div[class*='btn-reservar']:not([style*='display: none'])",
            placement: "top"
        },
        {
            title: "Filtrar por fecha de celebración",
            content: "Utilizando este campo se pueden filtrar los resultados de las sesiones por fecha de celebración",
            target: "div[id^='panelTaquilla'] div[id^='gridSesionesTaquilla'] table[class*='filterControl'] input",
            placement: "top"
        }
    ]
};