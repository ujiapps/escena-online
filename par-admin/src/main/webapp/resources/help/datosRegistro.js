var tourDatosRegistro = {
    id: "DatosRegistro",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "CIF de la entidad que vende las entradas",
            content: "Este es el CIF que aparece en las entradas impresas. Es obligatorio rellenar este campo.",
            target: "div[id^='formDatosRegistro'] input[name='cif']",
            placement: "bottom"
        },
        {
            title: "Email",
            content: "Esta dirección de e-mail es la que sirve de acceso a la aplicación.</br></br>Si aparece marcada en rojo indica que el e-mail no ha sido verificado.",
            target: "div[id^='formDatosRegistro'] input[name='email']",
            placement: "bottom"
        },
        {
            title: "Reenviar e-mail de verificación",
            content: "Reenvía un e-mail de verificación. Al pulsar sobre el enlace se entra a la aplicación y se puede observar como el error de que el email no está verificado ha desaparecido.",
            target: "div[id^='formDatosRegistro'] div[class*='btn-reenviar-email-verificacion']:not([style*='display: none'])",
            placement: "top",
            width: 150
        }
    ]
};