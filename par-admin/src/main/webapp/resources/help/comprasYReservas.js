var tourComprasReservas = {
    id: "ComprasReservas",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Filtrar por título",
            content: "Utilizando este campo se pueden filtrar los resultados por título",
            target: "div[id^='panelComprasReservas'] div[id^='gridEventosComprasReservas'] table[class*='filterControl'] input",
            placement: "bottom"
        },
        {
            title: "Consultar compras y reservas",
            content: "Para gestionas las compras y reservas de un evento es necesario seleccionar una sesión.</br></br>",
            target: "div[id^='gridSesionesComprasReservas'] div[class*='btn-compras-reservas']",
            placement: "bottom"
        }
    ]
};

var tourComprasReservasPopUp = {
    id: "ComprasReservasPopUp",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Anular compra o reserva",
            content: "Anula la compra o reserva seleccionada.</br></br>Al anular la compra la fila aparecerá en color rojo. Siempre se pueden volver a reactivar las anulaciones.",
            target: "div[id^='panelCompras'] div[id^='gridCompras'] div[class*='btn-anular-compra']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Activar compra o reserva anulada",
            content: "Reactiva una compra o reserva que teníamos anulada.",
            target: "div[id^='panelCompras'] div[id^='gridCompras'] div[class*='btn-desanular-compra']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Pasar reserva a compra",
            content: "Convierte la reserva en una compra real de la aplicación. Con esta acción, esta compra se tendrá en cuenta en el resultado de ingresos del espectáculo, y el titular de la compra podrá acceder al evento presentando el documento.",
            target: "div[id^='panelCompras'] div[id^='gridCompras'] div[class*='btn-reserva-to-compra']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Mostrar anuladas",
            content: "Añade a la tabla las compras que han sido anuladas.</br></br><b>Por defecto no se muestran las compras anuladas</b>.",
            target: "div[id^='panelCompras'] div[id^='gridCompras'] table[class*='btn-mostrar-anuladas']",
            placement: "bottom"
        },
        {
            title: "Mostrar compras realizadas online",
            content: "Añade a la tabla las compras realizadas online mediante la página web pública.</br></br><b>Por defecto no se muestran las compras realizadas online</b>.",
            target: "div[id^='panelCompras'] div[id^='gridCompras'] table[class*='btn-mostrar-online']",
            placement: "bottom"
        },
        {
            title: "Anular una entrada individual",
            content: "Una compra puede estar formada por más de una entrada individual.</br></br>Esta opción anula una entrada individual dentro de una compra, dejando el resto de entradas de la misma sin alteraciones.",
            target: "div[id^='panelCompras'] div[id^='gridDetalleCompras'] div[class*='btn-anular-compra-butaca']:not([style*='display: none'])",
            placement: "top"
        },
        {
            title: "Cambiar asiento de una entrada individual",
            content: "Cambia el asiento de una butaca, en una sesión numerada.</br></br>Solo es posible cambiar la fila y asiento dentro de la misma localización en la que se realizó la compra o reserva.</b>.",
            target: "div[id^='panelCompras'] div[id^='gridDetalleCompras'] div[class*='btn-cambiar-compra-butaca']:not([style*='display: none'])",
            placement: "top"
        },
        {
            title: "Cambiar la tarifa de una reserva",
            content: "Cambia la tarifa de una reserva.</br></br>Solo es posible cambiar la tarifa de asiento dentro de la misma localización en la que se realizó la reserva.</b>.",
            target: "div[id^='panelCompras'] div[id^='gridDetalleCompras'] div[class*='btn-cambiar-tarifa-butaca']:not([style*='display: none'])",
            placement: "top"
        },
        {
            title: "Pasar a compra las butacas reservadas seleccionadas",
            content: "Convierte la reserva de la butaca en una compra real de la aplicación.</br></br>Para seleccionar más de una butaca utiliza <kbd>CTRL + click en las filas que se desean seleccionar</kbd>.",
            target: "div[id^='panelCompras'] div[id^='gridDetalleCompras'] div[class*='btn-reserva-to-compra-butaca']:not([style*='display: none'])",
            placement: "top"
        }
    ]
};
