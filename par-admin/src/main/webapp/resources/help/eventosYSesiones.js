var tourEventos = {
    id: "Eventos",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Filtrar por título",
            content: "Utilizando este campo se pueden filtrar los resultados por título",
            target: "div[id^='panelEventos'] div[id^='gridEventos'] table[class*='filterControl'] input",
            placement: "bottom"
        },
        {
            title: "Añadir evento",
            content: "Abre un formulario para introducir toda la información relativa al evento.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='panelEventos'] div[id^='gridEventos'] div[class*='btn-add']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Editar evento",
            content: "Abre un formulario para modificar el evento seleccionado.</br><b>Es necesario seleccionar al menos un registro de la tabla</b>.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='panelEventos'] div[class*='btn-edit']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Mostrar eventos acabados",
            content: "Muestra en la tabla los eventos pasados.",
            target: "div[id^='panelEventos'] table[class*='btn-mostrar-todos']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Filtrar por fecha de sesión",
            content: "Utilizando este campo se pueden filtrar los resultados por fecha de sesión",
            target: "div[id^='panelEventos'] div[id^='gridSesiones-'] table[class*='filterControl'] input",
            placement: "bottom"
        },
        {
            title: "Añadir sesión",
            content: "Abre un formulario para añadir una sesión al evento seleccionado en la tabla superior.</br></br><b>Es obligatorio haber seleccionado un registro de la tabla superior.</b>.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='gridSesiones-'] div[class*='btn-add']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Editar sesión",
            content: "Abre un formulario para modificar la información de la sesión seleccionada.</br><b>Es necesario seleccionar al menos un registro de la tabla</b>.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='gridSesiones-'] div[class*='btn-edit']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Eliminar sesión",
            content: "Eliminar la sesión seleccionada.</br><b>Es necesario seleccionar al menos un registro de la tabla.</b></br><b>Si una sesión contiene ventas no podrá ser eliminada.</b>",
            target: "div[id^='gridSesiones-'] div[class*='btn-delete']:not([style*='display: none'])",
            placement: "bottom"
        },
        {
            title: "Mostrar sesiones acabadas o canceladas",
            content: "Añade a la tabla las sesiones que han acabado o han sido canceladas.",
            target: "div[id^='gridSesiones-'] table[class*='btn-mostrar-todas']:not([style*='display: none'])",
            placement: "bottom"
        }
    ]
};



var tourEventosPopUp = {
    id: "EventosPopUp",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Título del evento",
            content: "Este campo hace referencia al título del evento que se visualizá tanto en la aplicación como en la web pública de venta de entradas.",
            target: "div[id^='formEventos'] input[name='tituloEs']",
            placement: "bottom"
        },{
            title: "Descripción",
            content: "Este campo hace referencia a la descripción del evento. Permite añadir formato HTML que luego se mostrará en la web pública de venta de entradas.",
            target: "div[id^='formEventos'] td[name^='htmleditor-',role='presentation']",
            placement: "bottom"
        },{
            title: "TPV",
            content: "Selecciona el tpv con el que se realizarán los cobros.",
            target: "div[id^='formEventos'] input[name='tpv']",
            placement: "top"
        },{
            title: "Tipo",
            content: "Selecciona el tipo de evento que se mostrará en la web pública de venta.",
            target: "div[id^='formEventos'] input[name='tipoEvento']",
            placement: "top"
        },{
            title: "Asientos numerados",
            content: "Define si el evento dispone de asientos numerados o no. Para poder elegir sesión numerada, la sala tiene que tener desarrollado el patio de butacas.",
            target: "div[id^='formEventos'] input[name='asientosNumerados']",
            placement: "top"
        },{
            title: "Entradas nominales",
            content: "Si selecciona que sí se obligará al comprador a introducir el nombre de cada uno de los asistentes al evento.",
            target: "div[id^='formEventos'] input[name='entradasNominales']",
            placement: "top"
        },{
            title: "IVA",
            content: "Define el iva que se aplica al evento. Es obligatorio, por lo que si está exento de IVA, indicar 0.",
            target: "div[id^='formEventos'] input[name='porcentajeIVA']",
            placement: "top"
        },{
            title: "Imagen del evento",
            content: "Define la imagen que se mostrará en la página web pública.</br></br><b>Restricciones:</b></br>Tamaño máximo: 1Mb.",
            target: "div[id^='formEventos'] input[name='dataBinary']",
            placement: "top"
        },{
            title: "Imagen de publicidad",
            content: "Define la imagen que se mostrará en la entrada a modo de publicidad.</br></br><b>Restricciones:</b></br>Tamaño máximo: 1Mb.",
            target: "div[id^='formEventos'] input[name='dataBinaryPubli']",
            placement: "top"
        },{
            title: "Borrar imagen",
            content: "Elimina la imagen que se utiliza en la página web pública.",
            target: "div[id^='formEventos'] div[class*='btn-borrar-imagen']:not([style*='display: none'])",
            placement: "top"
        },{
            title: "Borrar imagen de publicidad del evento",
            content: "Elimina la imagen que se utiliza en la entrada a modo de publicidad.",
            target: "div[id^='formEventos'] div[class*='btn-borrar-imagen-publi']:not([style*='display: none'])",
            placement: "top",
            width: 200
        }
    ]
};