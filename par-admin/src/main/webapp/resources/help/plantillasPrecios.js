var tourPlantillasPrecios = {
    id: "PlantillasPrecios",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Añadir plantilla",
            content: "Crea una plantilla de precios que se podrá aplicar a un evento de forma que no será necesario especificar los precios manualmente para cada localización y tarifa.</br></br>Abre un formulario para introducir los campos necesarios para dar de alta una nueva plantilla.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='gridPlantillas'] div[class*='btn-add']:not([style*='display: none'])",
            placement: "bottom"
        }, {
            title: "Editar plantilla",
            content: "Abre un formulario para modificar los campos de una plantilla.</br><b>Es necesario seleccionar al menos un registro de la tabla</b>.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='gridPlantillas'] div[class*='btn-edit']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Eliminar plantilla",
            content: "Eliminar la plantilla seleccionada.</br><b>Es necesario seleccionar al menos un registro de la tabla.</b></br></br><b>No se podrá eliminar la plantilla si alguna sesión la está utilizando.</b>",
            target: "div[id^='gridPlantillas'] div[class*='btn-delete']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Añadir fila detalle a la plantilla",
            content: "Añade una <b>localización</b>, una <b>tarifa</b> y un <b>precio</b> para generar una plantilla de precios.</br></br> Abre un formulario para introducir los campos necesarios para dar de alta una nueva fila de detalle para la plantilla seleccionada.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.</br><b>Es necesario seleccionar al menos un registro de la tabla superior</b>.",
            target: "div[id^='gridPrecios'] div[class*='btn-add']:not([style*='display: none'])",
            placement: "bottom"
        }, {
            title: "Editar fila detalle de la plantilla",
            content: "Abre un formulario para modificar los campos necesarios de una fila detalle de la plantilla seleccionada.</br><b>Es necesario seleccionar al menos un registro de la tabla superior y una fila de detalle</b>.",
            target: "div[id^='gridPrecios'] div[class*='btn-edit']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Eliminar fila detalle de la plantilla",
            content: "Eliminar la plantilla seleccionada.</br><b>Es necesario seleccionar al menos un registro de la tabla superior y una fila de detalle</b>.</br></br><b>No se podrá eliminar la plantilla si alguna sesión la está utilizando.</b>",
            target: "div[id^='gridPrecios'] div[class*='btn-delete']:not([style*='display: none'])",
            placement: "bottom"
        }
    ]
};