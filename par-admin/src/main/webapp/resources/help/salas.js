var tourSalas = {
    id: "salas",
    i18n: {
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Añadir sala no numerada",
            content: "Añade una sala no numerada a la aplicación.</br></br>Abre un formulario para introducir los datos necesarios para dar de alta una sala no numerada.",
            target: "div[id^='panelSalas'] div[id^='gridSalas'] div[class*='btn-add']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Editar sala no numerada",
            content: "Abre un formulario para modificar los datos necesarios para una sala.</br><b>Es necesario seleccionar al menos un registro de la tabla</b>.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='panelSalas'] div[id^='gridSalas'] div[class*='btn-edit']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Eliminar sala no numerada",
            content: "Elimina una sala no numerada a la aplicación.</br><b>Es necesario seleccionar al menos un registro de la tabla</b>.</br></br><b>No se podrá eliminar la sala si algúna sesión ya la tiene asignada.</b>",
            target: "div[id^='panelSalas'] div[id^='gridSalas'] div[class*='btn-delete']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Añadir localización",
            content: "Añade una localización a la sala seleccionada en la tabla superior. Ejemplos de localizaciones pueden ser: platea, anfiteatro, palco izquierdo...</br><b>Es necesario seleccionar al menos un registro de la tabla superior</b>.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='panelSalas'] div[id^='gridLocalizaciones'] div[class*='btn-add']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Editar localización",
            content: "Edita una localización a la sala seleccionada en la tabla superior. Ejemplos de localizaciones pueden ser: platea, anfiteatro, palco izquierdo...</br><b>Es necesario seleccionar al menos un registro de la tabla superior y una localización</b>.</br></br>Dentro del formulario los campos marcados con un * rojo son obligatorios.",
            target: "div[id^='panelSalas'] div[id^='gridLocalizaciones'] div[class*='btn-edit']:not([style*='display: none'])",
            placement: "bottom"
        },{
            title: "Eliminar localización",
            content: "Elimina una localización a la sala seleccionada en la tabla superior.</br><b>Es necesario seleccionar al menos un registro de la tabla superior y una localización</b>.</br></br><b>No se podrá eliminar la localización si alguna sesión ya la tiene asignada.</b>",
            target: "div[id^='panelSalas'] div[id^='gridLocalizaciones'] div[class*='btn-delete']:not([style*='display: none'])",
            placement: "bottom"
        }
    ]
};