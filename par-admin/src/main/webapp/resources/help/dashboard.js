var tourDashboard = {
    id: "dashboard",
    i18n:{
        nextBtn: "Siguiente",
        doneBtn: "Cerrar"
    },
    steps: [
        {
            title: "Tipos de eventos",
            content: "Desde esta secci&oacute;n se pueden definir los tipos de eventos que gestiona la aplicaci&oacute;n.</br></br> Por ejemplo:</br><b> humor, concierto, teatro</b>",
            target: "#menuTiposEventos",
            placement: "right"
        },{
            title: "Eventos y sesiones",
            content: "Desde esta secci&oacute;n se pueden definir los eventos que se vender&aacute;n a trav&eacute;s de la plataforma. </br></br>Para cada evento se pueden asignar diferentes sesiones.",
            target: "#menuEventos",
            placement: "right"
        },{
            title: "Plantillas de precios",
            content: "Desde esta secci&oacute;n se pueden definir plantillas de precios que poder aplicar a diferentes localizaciones. La plantilla de precios solamente tiene utilidad cuando hay muchos eventos con los mismos precios, de forma que, introduci&eacute;ndolos una vez, los aprovechamos para el resto.",
            target: "#menuPlantillasPrecios",
            placement: "right"
        },{
            title: "Tarifas",
            content: "Desde esta secci&oacute;n se pueden definir diferentes tarifas que se pueden aplicar a la venta de un evento. Algunos ejemplos pueden ser: tarifa general, tarifa para jubilados, para estudiantes, invitaciones...",
            target: "#menuTarifas",
            placement: "right"
        },{
            title: "Salas no numeradas",
            content: "Desde esta secci&oacute;n se pueden gestionar las salas NO numeradas de las que se dispone.</br></br> Para cada sala se pueden definir diferentes localizaciones con su respectivo aforo. Para salas numeradas, es necesario desarrollar el patio de butacas, con lo que puede contactar con 4TIC para su implementaci&oacute;n.",
            target: "#menuSalas",
            placement: "right"
        },{
            title: "Integraciones",
            content: "En esta secci&oacute;n se muestra informaci&oacute;n acerca de c&oacute;mo conectar la aplicaci&oacute;n m&oacute;vil para controlar los accesos al evento.",
            target: "#menuIntegraciones",
            placement: "right"
        },{
            title: "Taquilla",
            content: "Venta y reserva de entradas en taquilla. Esta secci&oacute;n permite vender o reservar entradas desde la taquilla.",
            target: "#menuTaquilla",
            placement: "right"
        },{
            title: "Compras y reservas",
            content: "Desde esta secci&oacute;n se pueden visualizar, anular y modificar, las compras y reservas de los eventos",
            target: "#menuComprasReservas",
            placement: "right"
        },{
            title: "Informes",
            content: "Desde esta secci&oacute;n se pueden obtener los informes de ventas, con filtros por fechas, por evento o por sesi&oacute;n.",
            target: "#menuInformes",
            placement: "right"
        },{
            title: "Clientes",
            content: "Desde esta secci&oacute;n se puede visualizar y obtener la lista de e-mails de clientes que han aceptado recibir informaci&oacute;n.</br></br>IMPORTANTE: S&oacute;lo se visualizan los clientes que en el proceso de compra hayan marcado la casilla, indicando que desean recibir informaci&oacute;n.",
            target: "#menuClientes",
            placement: "right"
        },{
            title: "Mis datos",
            content: "Desde esta secci&oacute;n se pueden visualizar y editar los datos personales, fiscales y de comportamiento que la aplicaci&oacute;n gestiona.",
            target: "#menuDatos",
            placement: "right"
        }
    ]
};