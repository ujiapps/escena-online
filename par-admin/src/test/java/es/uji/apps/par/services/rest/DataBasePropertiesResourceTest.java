package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response;

@Transactional
public class DataBasePropertiesResourceTest extends BaseResourceTest
{
    String WEB = "dev.palaciodecongresoshuesca.4tic.com/par-public";
    String HTML_TITLE = "Palacio de Congresos";
    String CONDICIONES_URL = "https://dev.palaciocongresoshuesca.es/aviso-legal";
    String CANCELACION_URL = "https://dev.palaciocongresoshuesca.es/cancelacion.html";
    String COMO_LLEGAR_URL = "http://www.palaciocongresoshuesca.es/auditorio";
    String PIE_ENTRADA_URL = "http://example.com/example.jpg";
    String MAIL_FROM = "no_reply@4tic.com";
    String LOGO_REPORT = "pchu_logo.jpg";
    String PUBLIC_URL = String.format("https://%s", WEB);
    String PUBLIC_URL_LIMPIO = PUBLIC_URL;

    @Test
    public void getPublicUrlFromDataBase()
    {
        ClientResponse response = resource.path("index/public/properties").get(ClientResponse.class);

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        Assert.assertNotNull(restResponse.getData());
        Assert.assertTrue(restResponse.getTotal() == 9);
        Assert.assertEquals(restResponse.getData().get(0), PUBLIC_URL);
        Assert.assertEquals(restResponse.getData().get(1), PUBLIC_URL_LIMPIO);
        Assert.assertEquals(restResponse.getData().get(2), HTML_TITLE);
        Assert.assertEquals(restResponse.getData().get(3), CONDICIONES_URL);
        Assert.assertEquals(restResponse.getData().get(4), CANCELACION_URL);
        Assert.assertEquals(restResponse.getData().get(5), COMO_LLEGAR_URL);
        Assert.assertEquals(restResponse.getData().get(6), PIE_ENTRADA_URL);
        Assert.assertEquals(restResponse.getData().get(7), MAIL_FROM);
        Assert.assertEquals(restResponse.getData().get(8), LOGO_REPORT);
    }
}
