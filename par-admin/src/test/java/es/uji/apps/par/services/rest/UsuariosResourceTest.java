package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response.Status;

import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.model.ResultatOperacio;
import es.uji.apps.par.model.Usuario;

public class UsuariosResourceTest extends BaseResourceTest
{
    public UsuariosResourceTest()
    {
        super();
        this.resource = resource().path("usuario");
    }

    @Test
    public void getUsers()
    {
        ClientResponse response = resource.get(ClientResponse.class);
        RestResponse serviceResponse = response.getEntity(RestResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        Assert.assertTrue(serviceResponse.getSuccess());
        Assert.assertNotNull(serviceResponse.getData());
    }

    @Test
    public void addUsuarioWithoutMail()
    {
        Usuario parUsuario = getUsuarioTest("addUsuarioWithoutMail");
        parUsuario.setMail(null);
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, parUsuario);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Mail",
                resultatOperacio.getDescripcio());
    }

    @Test
    public void addUsuarioWithoutNombre()
    {
        Usuario parUsuario = getUsuarioTest("addUsuarioWithoutNombre");
        parUsuario.setNombre(null);
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, parUsuario);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Nombre",
                resultatOperacio.getDescripcio());
    }

    @Test
    public void addUsuarioWithoutLogin()
    {
        Usuario parUsuario = getUsuarioTest("addUsuarioWithoutLogin");
        parUsuario.setUsuario(null);
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, parUsuario);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Usuario",
                resultatOperacio.getDescripcio());
    }

    @Test
    public void addUsuario()
    {
        Usuario parUsuario = getUsuarioTest("addUsuario");
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, parUsuario);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });
        Assert.assertTrue(restResponse.getSuccess());
        Assert.assertNotNull(getFieldFromRestResponse(restResponse, "id"));
        Assert.assertEquals(parUsuario.getNombre(),
                getFieldFromRestResponse(restResponse, "nombre"));
    }

    @Test
    public void updateUsuario()
    {
        Usuario parUsuario = getUsuarioTest("updateUsuario");
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, parUsuario);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        String id = getFieldFromRestResponse(restResponse, "id");
        Assert.assertNotNull(id);

        parUsuario.setNombre("updateUsuario2");
        response = resource.path(id).type("application/json").put(ClientResponse.class, parUsuario);
        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        Assert.assertEquals(parUsuario.getNombre(),
                getFieldFromRestResponse(restResponse, "nombre"));
    }

    @Test
    public void updateUsuarioAndRemoveMail()
    {
        Usuario parUsuario = getUsuarioTest("updateUsuarioAndRemoveMail");
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, parUsuario);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        String id = getFieldFromRestResponse(restResponse, "id");
        Assert.assertNotNull(id);

        parUsuario.setMail("");
        response = resource.path(id).type("application/json").put(ClientResponse.class, parUsuario);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio parResponseMessage = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Mail",
                parResponseMessage.getDescripcio());
    }
}