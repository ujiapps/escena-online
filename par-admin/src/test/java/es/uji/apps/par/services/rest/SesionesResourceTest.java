package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.multipart.FormDataMultiPart;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.exceptions.FechasInvalidasException;
import es.uji.apps.par.model.Plantilla;
import es.uji.apps.par.model.ResultatOperacio;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.TipoEvento;

public class SesionesResourceTest extends BaseResourceTest {
    String eventoId;

    private TipoEvento addTipoEvento() {
        TipoEvento parTipoEvento = new TipoEvento("prueba");
        ClientResponse response =
            resource.path("tipoevento").type("application/json").post(ClientResponse.class, parTipoEvento);
        RestResponse serviceResponse = response.getEntity(RestResponse.class);

        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        Assert.assertTrue(serviceResponse.getSuccess());
        Assert.assertNotNull(serviceResponse.getData());
        int id = Integer.valueOf(((HashMap) serviceResponse.getData().get(0)).get("id").toString());
        parTipoEvento.setId(id);
        return parTipoEvento;
    }

    private FormDataMultiPart preparaEvento(TipoEvento tipoEvento) {
        FormDataMultiPart f = new FormDataMultiPart();
        f.field("tituloEs", "titulo");
        f.field("tipoEvento", String.valueOf(tipoEvento.getId()));
        f.field("asientosNumerados", "1");

        return f;
    }

    private String addEvento(TipoEvento parTipoEvento) {
        FormDataMultiPart parEvento = preparaEvento(parTipoEvento);
        ClientResponse response = resource.path("evento/create").type(MediaType.MULTIPART_FORM_DATA_TYPE)
            .post(ClientResponse.class, parEvento);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());

        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>() {
        });
        Assert.assertTrue(restResponse.getSuccess());
        Assert.assertNotNull(getFieldFromRestResponse(restResponse, "id"));
        Assert.assertEquals(parEvento.getField("tituloEs").getValue(),
            getFieldFromRestResponse(restResponse, "tituloEs"));
        return getFieldFromRestResponse(restResponse, "id");
    }

    private Plantilla addPlantilla() {
        Plantilla plantilla = getPlantillaTest();
        ClientResponse response =
            resource.path("plantillaprecios").type("application/json").post(ClientResponse.class, plantilla);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>() {
        });
        Assert.assertTrue(restResponse.getSuccess());
        Assert.assertNotNull(getFieldFromRestResponse(restResponse, "id"));
        Assert.assertEquals(plantilla.getNombre(), getFieldFromRestResponse(restResponse, "nombre"));

        plantilla.setId(Long.valueOf(getFieldFromRestResponse(restResponse, "id")));
        return plantilla;
    }

    @Before
    public void createEvento() {
        TipoEvento parTipoEvento = addTipoEvento();
        eventoId = addEvento(parTipoEvento);
    }

    @Test
    public void getSesiones() {
        ClientResponse response = resource.path("evento").path(eventoId).path("sesiones").get(ClientResponse.class);
        RestResponse serviceResponse = response.getEntity(RestResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        Assert.assertTrue(serviceResponse.getSuccess());
        Assert.assertNotNull(serviceResponse.getData());
    }

    @Test
    public void addSesionWithoutFechaCelebracion() {
        Plantilla plantilla = addPlantilla();
        Sesion sesion = getSesionTest(plantilla);
        sesion.setFechaCelebracion(null);

        ClientResponse response = resource.path("evento").path(eventoId).path("sesiones").type("application/json")
            .post(ClientResponse.class, sesion);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Fecha de celebración",
            resultatOperacio.getDescripcio());
    }


    @Test
    public void addSesionWithoutHoraCelebracion() {
        Plantilla plantilla = addPlantilla();
        Sesion sesion = getSesionTest(plantilla);
        sesion.setHoraCelebracion(null);

        ClientResponse response = resource.path("evento").path(eventoId).path("sesiones").type("application/json")
            .post(ClientResponse.class, sesion);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Hora de celebración",
            resultatOperacio.getDescripcio());
    }

    @Test
    public void addSesion() {
        Plantilla plantilla = addPlantilla();
        Sesion sesion = getSesionTest(plantilla);

        ClientResponse response = resource.path("evento").path(eventoId).path("sesiones").type("application/json")
            .post(ClientResponse.class, sesion);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());

        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>() {
        });
        Assert.assertTrue(restResponse.getSuccess());
    }

    @Test
    public void addSesionWithFechaEndVentaAnteriorFechaStartVenta() {
        Plantilla plantilla = addPlantilla();
        Sesion sesion = getSesionTest(plantilla);
        sesion.setFechaInicioVentaOnline("02/12/2012");
        sesion.setFechaFinVentaOnline("01/12/2012");
        sesion.setCanalInternet("1");

        ClientResponse response = resource.path("evento").path(eventoId).path("sesiones").type("application/json")
            .post(ClientResponse.class, sesion);
        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertTrue(resultatOperacio.getDescripcio()
            .contains(FechasInvalidasException.FECHA_INICIO_VENTA_POSTERIOR_FECHA_FIN_VENTA));
    }

    @Test
    public void addSesionWithoutFechaInicioVentaOnline() {
        Plantilla plantilla = addPlantilla();
        Sesion sesion = getSesionTest(plantilla);
        sesion.setFechaInicioVentaOnline(null);
        sesion.setCanalInternet("1");

        ClientResponse response = resource.path("evento").path(eventoId).path("sesiones").type("application/json")
            .post(ClientResponse.class, sesion);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Fecha de inicio de la venta online",
            resultatOperacio.getDescripcio());
    }

    @Test
    public void addSesionWithoutFechaFinVentaOnline() {
        Plantilla plantilla = addPlantilla();
        Sesion sesion = getSesionTest(plantilla);
        sesion.setFechaFinVentaOnline(null);
        sesion.setCanalInternet("1");

        ClientResponse response = resource.path("evento").path(eventoId).path("sesiones").type("application/json")
            .post(ClientResponse.class, sesion);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Fecha de fin de la venta online",
            resultatOperacio.getDescripcio());
    }

    @Test
    public void addSesionWithoutHoraInicioVentaOnline() {
        Plantilla plantilla = addPlantilla();
        Sesion sesion = getSesionTest(plantilla);
        sesion.setHoraInicioVentaOnline(null);
        sesion.setCanalInternet("1");

        ClientResponse response = resource.path("evento").path(eventoId).path("sesiones").type("application/json")
            .post(ClientResponse.class, sesion);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Hora de inicio de la venta online",
            resultatOperacio.getDescripcio());
    }

    @Test
    public void addSesionWithoutHoraFinVentaOnline() {
        Plantilla plantilla = addPlantilla();
        Sesion sesion = getSesionTest(plantilla);
        sesion.setHoraFinVentaOnline(null);
        sesion.setCanalInternet("1");

        ClientResponse response = resource.path("evento").path(eventoId).path("sesiones").type("application/json")
            .post(ClientResponse.class, sesion);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Hora de fin de la venta online",
            resultatOperacio.getDescripcio());
    }

    @Test
    public void addSesionWithFechaEndVentaPosteriorFechaCelebracion() {
        Plantilla plantilla = addPlantilla();
        Sesion sesion = getSesionTest(plantilla);
        sesion.setFechaCelebracion("01/12/2012");
        sesion.setFechaInicioVentaOnline("01/11/2012");
        sesion.setFechaFinVentaOnline("03/12/2012");
        sesion.setCanalInternet("1");

        ClientResponse response = resource.path("evento").path(eventoId).path("sesiones").type("application/json")
            .post(ClientResponse.class, sesion);
        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertTrue(resultatOperacio.getDescripcio()
            .contains(FechasInvalidasException.FECHA_FIN_VENTA_POSTERIOR_FECHA_CELEBRACION));
    }
}
