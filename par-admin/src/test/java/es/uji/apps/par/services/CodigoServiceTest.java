package es.uji.apps.par.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CodigoServiceTest
{
    @Test
    public void getCodigoConAcentosYApostrofo()
    {
        String codigoFromNombre = CodigoService
            .getCodigoFromNombre("General_cón l'apóstrofo y varios acentos", "Sala con l'apóstrofo", false);

        Assert.assertFalse(codigoFromNombre.contains("'"));
        Assert.assertFalse(codigoFromNombre.contains("ó"));
        Assert.assertEquals("Generalconlapostrofoyvariosacentos_Salaconlapostrofo", codigoFromNombre);
    }

    @Test
    public void getCodigoConDoblesComillas()
    {
        String codigoFromNombre = CodigoService
            .getCodigoFromNombre("\"La localización\"", "Sala Test", false);

        Assert.assertFalse(codigoFromNombre.contains("\""));
        Assert.assertFalse(codigoFromNombre.contains("ó"));
        Assert.assertEquals("Lalocalizacion_SalaTest", codigoFromNombre);
    }

    @Test
    public void getCodigoConAcentosYApostrofoDiscapacitados()
    {
        String codigoFromNombre = CodigoService
            .getCodigoFromNombre("General_cón l'apóstrofo y varios acentos", "Sala con l'apóstrofo", true);

        Assert.assertFalse(codigoFromNombre.contains("'"));
        Assert.assertFalse(codigoFromNombre.contains("ó"));
        Assert.assertEquals("Generalconlapostrofoyvariosacentos_discapacitados_Salaconlapostrofo", codigoFromNombre);
    }

    @Test
    public void getCodigoConDoblesComillasDiscapacitados()
    {
        String codigoFromNombre = CodigoService
            .getCodigoFromNombre("\"La localización\"", "Sala Test", true);

        Assert.assertFalse(codigoFromNombre.contains("\""));
        Assert.assertFalse(codigoFromNombre.contains("ó"));
        Assert.assertEquals("Lalocalizacion_discapacitados_SalaTest", codigoFromNombre);
    }
}