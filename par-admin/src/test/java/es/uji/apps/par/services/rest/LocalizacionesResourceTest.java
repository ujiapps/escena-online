package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

import javax.ws.rs.core.Response.Status;

import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.model.ResultatOperacio;
import es.uji.apps.par.model.Sala;

import static org.hamcrest.core.Is.is;

public class LocalizacionesResourceTest extends BaseResourceTest
{
    public LocalizacionesResourceTest()
    {
        super();
        this.resource = resource().path("localizacion");
    }

    @Test
    public void getLocalizacionesSinEspecificarSala()
    {
        ClientResponse response = resource.get(ClientResponse.class);
        RestResponse serviceResponse = response.getEntity(RestResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        Assert.assertTrue(serviceResponse.getSuccess());
        Assert.assertNotNull(serviceResponse.getData());
        Assert.assertTrue(serviceResponse.getTotal() == 0);
    }

    @Test
    public void addLocalizacionWithoutNombre()
    {
        Localizacion localizacion = getLocalizacionTest();
        localizacion.setNombreEs(null);

        ClientResponse response = resource.type("application/json").post(ClientResponse.class, localizacion);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Nombre",
                resultatOperacio.getDescripcio());
    }


    @Test
    public void addLocalizacion()
    {
        Localizacion localizacion = getLocalizacionTest();
        ClientResponse response = resource.queryParam("sala", "1").type("application/json").post(ClientResponse.class, localizacion);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        String localizacionId = getFieldFromRestResponse(restResponse, "id");

        Assert.assertTrue(restResponse.getSuccess());
        Assert.assertNotNull(localizacionId);
        Assert.assertEquals(localizacion.getNombreEs(),
                getFieldFromRestResponse(restResponse, "nombreEs"));

        response = resource.queryParam("sala", localizacionId).get(ClientResponse.class);
        RestResponse serviceResponse = response.getEntity(RestResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        Assert.assertTrue(serviceResponse.getSuccess());
        Assert.assertNotNull(serviceResponse.getData());
    }

    @Test
    public void updateLocalizacion()
    {
        Localizacion localizacion = getLocalizacionTest();
        ClientResponse response = resource.queryParam("sala", "1").type("application/json").post(ClientResponse.class, localizacion);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        String id = getFieldFromRestResponse(restResponse, "id");
        Assert.assertNotNull(id);

        localizacion.setNombreEs("Prueba2");

        response = resource.path(id).type("application/json").put(ClientResponse.class, localizacion);
        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        Assert.assertEquals(localizacion.getNombreEs(),
                getFieldFromRestResponse(restResponse, "nombreEs"));
    }

    @Test
    public void updateLocalizacionAndRemoveNombre()
    {
        Localizacion localizacion = getLocalizacionTest();
        ClientResponse response = resource.queryParam("sala", "1").type("application/json").post(ClientResponse.class, localizacion);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        String id = getFieldFromRestResponse(restResponse, "id");
        Assert.assertNotNull(id);

        localizacion.setNombreEs("");
        response = resource.path(id).type("application/json").put(ClientResponse.class, localizacion);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        ResultatOperacio parResponseMessage = response.getEntity(ResultatOperacio.class);

        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Nombre",
                parResponseMessage.getDescripcio());
    }

    @Test
    public void createLocalizacionIntoSala(){
        Sala sala = getSalaTest();
        this.resource = resource().path("sala");
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, sala);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        response = resource.type("application/json").get(ClientResponse.class);
        Assert.assertThat(response.getStatus(), is(Status.OK.getStatusCode()));
        RestResponse entity = response.getEntity(new GenericType<RestResponse>() {
        });
        Assert.assertThat(entity.getData().size(), is(2));
        Integer idSalaGenerada = (Integer)((HashMap)entity.getData().get(1)).get("id");
        this.resource = resource().path("localizacion");
        Localizacion localizacion = getLocalizacionTest();
        response = resource.queryParam("sala", idSalaGenerada.toString()).type("application/json")
            .post(ClientResponse.class, localizacion);
        Assert.assertThat(response.getStatus(), is(Status.CREATED.getStatusCode()));
    }
}