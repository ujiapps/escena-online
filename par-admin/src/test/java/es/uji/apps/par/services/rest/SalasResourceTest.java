package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

import javax.ws.rs.core.Response;

import es.uji.apps.par.model.Sala;

import static org.hamcrest.core.Is.is;

public class SalasResourceTest extends BaseResourceTest{
    public SalasResourceTest() {
        super();
        this.resource = resource().path("sala");
    }

    @Test
    public void addSala(){
        crearSala();
    }

    @Test
    public void updateSala(){
        Integer idSalaGenerada = crearSala();
        actualizarSala(idSalaGenerada);
    }

    @Test
    public void deleteSala(){
        Integer idSalaGenerada = crearSala();
        borrarSala(idSalaGenerada);
    }

    private Integer crearSala(){
        Sala sala = getSalaTest();
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, sala);
        Assert.assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        response = resource.type("application/json").get(ClientResponse.class);
        Assert.assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
        RestResponse entity = response.getEntity(new GenericType<RestResponse>() {
        });
        Assert.assertThat(entity.getData().size(), is(2));

        HashMap salaRespuesta = null;
        for (Object data : entity.getData()) {
            if (((HashMap) data).get("nombre").equals("nombre de la sala 1"))
                salaRespuesta = (HashMap) data;
        }

        Assert.assertNotNull(salaRespuesta);
        return (Integer) salaRespuesta.get("id");
    }

    private void actualizarSala(Integer idSala){
        Sala sala = new Sala("nombre actualizado");
        sala.setId(idSala);
        ClientResponse response =
            resource.path(idSala.toString()).type("application/json").put(ClientResponse.class, sala);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        response = resource.type("application/json").get(ClientResponse.class);
        RestResponse entity = response.getEntity(new GenericType<RestResponse>() {
        });

        boolean found = false;
        for (Object data : entity.getData()) {
            String nombre = (String) ((HashMap)data).get("nombre");
            if (nombre.equals("nombre actualizado")) {
                found = true;
            }
        }

        Assert.assertTrue(found);
    }

    private void borrarSala(Integer idSala) {
        ClientResponse response =
            resource.path(idSala.toString()).type("application/json").delete(ClientResponse.class);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        response = resource.type("application/json").get(ClientResponse.class);
        RestResponse entity = response.getEntity(new GenericType<RestResponse>() {
        });
        Assert.assertThat(entity.getData().size(), is(1));
    }
}
