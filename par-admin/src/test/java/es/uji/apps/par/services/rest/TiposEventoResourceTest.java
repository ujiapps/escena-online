package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response.Status;

import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.model.ResultatOperacio;
import es.uji.apps.par.model.TipoEvento;

public class TiposEventoResourceTest extends BaseResourceTest
{
    public TiposEventoResourceTest()
    {
        super();
        this.resource = resource().path("tipoevento");
    }

    @Test
    public void getTiposEventos()
    {
        ClientResponse response = resource.get(ClientResponse.class);
        RestResponse serviceResponse = response.getEntity(RestResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        Assert.assertTrue(serviceResponse.getSuccess());
        Assert.assertNotNull(serviceResponse.getData());
    }

    @Test
    public void addTipoEventoWithoutNombre()
    {
        TipoEvento parTipoEvento = getTipoEventoTest();
        parTipoEvento.setNombreEs(null);
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, parTipoEvento);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Nombre",
                resultatOperacio.getDescripcio());
    }

    @Test
    public void addTipoEvento()
    {
        TipoEvento parTipoEvento = getTipoEventoTest();
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, parTipoEvento);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });
        Assert.assertTrue(restResponse.getSuccess());
        Assert.assertNotNull(getFieldFromRestResponse(restResponse, "id"));
        Assert.assertEquals(parTipoEvento.getNombreEs(),
                getFieldFromRestResponse(restResponse, "nombreEs"));
    }

    @Test
    public void updateTipoEvento()
    {
        TipoEvento parTipoEvento = getTipoEventoTest();
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, parTipoEvento);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        String id = getFieldFromRestResponse(restResponse, "id");
        Assert.assertNotNull(id);

        parTipoEvento.setNombreEs("Prueba2");
        response = resource.path(id).type("application/json").put(ClientResponse.class, parTipoEvento);
        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        Assert.assertEquals(parTipoEvento.getNombreEs(),
                getFieldFromRestResponse(restResponse, "nombreEs"));
    }

    @Test
    public void updateTipoEventoAndRemoveNombre()
    {
        TipoEvento parTipoEvento = getTipoEventoTest();
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, parTipoEvento);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        String id = getFieldFromRestResponse(restResponse, "id");
        Assert.assertNotNull(id);

        parTipoEvento.setNombreEs("");
        response = resource.path(id).type("application/json").put(ClientResponse.class, parTipoEvento);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio parResponseMessage = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Nombre",
                parResponseMessage.getDescripcio());
    }
}