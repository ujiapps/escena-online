package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response.Status;

import es.uji.apps.par.model.DatosIntegracion;
import es.uji.apps.par.model.DatosRegistro;

public class DatosRegistroResourceTest extends BaseResourceTest{
    @Test
    public void getDatosRegistroTest()
    {
        ClientResponse response = resource.path("registro").get(ClientResponse.class);
        DatosRegistro datosRegistro = response.getEntity(DatosRegistro.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        Assert.assertEquals(datosRegistro.getEmail(), "sergio@4tic.com");
        Assert.assertNull(datosRegistro.getPassword());
    }

    @Test
    public void updateDatosRegistroTest()
    {
        ClientResponse response = resource.path("registro").get(ClientResponse.class);
        DatosRegistro datosRegistro = response.getEntity(DatosRegistro.class);
        datosRegistro.setEmail("test@test.com");
        datosRegistro.setEmpresa("empresa");
        datosRegistro.setTelefono("telefono");
        datosRegistro.setCif("cif");
        datosRegistro.setCodMunicipio("codMunicipio");
        datosRegistro.setCp("cp");
        datosRegistro.setDireccion("direccion");
        datosRegistro.setEmailVerificado(true);
        datosRegistro.setMunicipio("municipio");
        datosRegistro.setUrl("test.com");

        response = resource.path("registro").type("application/json").put(ClientResponse.class, datosRegistro);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());

        response = resource.path("registro").get(ClientResponse.class);
        DatosRegistro datosRegistroActualizados = response.getEntity(DatosRegistro.class);
        Assert.assertNull(datosRegistroActualizados.getPassword());
        Assert.assertEquals(datosRegistroActualizados.getEmail(), "sergio@4tic.com");
        Assert.assertEquals(datosRegistroActualizados.getUrl(), "localhost");
        Assert.assertEquals(datosRegistroActualizados.getEmpresa(), "empresa");
        Assert.assertEquals(datosRegistroActualizados.getTelefono(), "telefono");
        Assert.assertEquals(datosRegistroActualizados.getCif(), "cif");
        Assert.assertEquals(datosRegistroActualizados.getCodMunicipio(), "codMunicipio");
        Assert.assertEquals(datosRegistroActualizados.getCp(), "cp");
        Assert.assertEquals(datosRegistroActualizados.getDireccion(), "direccion");
        Assert.assertEquals(datosRegistroActualizados.getEmailVerificado(), false);
        Assert.assertEquals(datosRegistroActualizados.getMunicipio(), "municipio");
    }

    @Test
    public void resendTest()
    {
        ClientResponse response = resource.path("registro/resend").put(ClientResponse.class);
        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void getIntegracionTest()
    {
        ClientResponse response = resource.path("registro/integracion").get(ClientResponse.class);
        DatosIntegracion datosIntegracion = response.getEntity(DatosIntegracion.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        Assert.assertEquals(datosIntegracion.getApiKey(), "123456789");
        Assert.assertEquals(datosIntegracion.getUrlConexion(), "https://dev.palaciodecongresoshuesca.4tic.com");
        Assert.assertEquals(datosIntegracion.getPort(), 443);
    }
}
