package es.uji.apps.par.services.rest;

import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.MultiPart;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.MediaType;

import es.uji.apps.par.builders.AbonoBuilder;
import es.uji.apps.par.model.Abono;

public class AbonosResourceTest extends BaseResourceTest
{
    @Test
    public void addAbono()
    {
        Abono abono = new AbonoBuilder("Abono 2").get();

        MultiPart multipartEntity = new FormDataMultiPart()
            .field("nombre", abono.getNombre(), MediaType.APPLICATION_FORM_URLENCODED_TYPE);


        String response = resource.path("abono").type("multipart/form-data").post(String.class, multipartEntity);

        Assert.assertEquals("{success: 'true'}", response);
    }
}
