package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response.Status;

import es.uji.apps.par.exceptions.CampoRequeridoException;
import es.uji.apps.par.model.Plantilla;
import es.uji.apps.par.model.ResultatOperacio;

public class PlantillasPreciosResourceTest extends BaseResourceTest
{
    public PlantillasPreciosResourceTest() {
        super();
        this.resource = resource().path("plantillaprecios");
    }

    @Test
    public void getPlantillaPrecios()
    {
        ClientResponse response = resource.get(ClientResponse.class);
        RestResponse serviceResponse = response.getEntity(RestResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        Assert.assertTrue(serviceResponse.getSuccess());
        Assert.assertNotNull(serviceResponse.getData());
    }

    @Test
    public void addPlantillaWithoutNombre()
    {
        Plantilla plantilla = getPlantillaTest();
        plantilla.setNombre(null);

        ClientResponse response = resource.type("application/json").post(ClientResponse.class, plantilla);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio resultatOperacio = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Nombre",
                resultatOperacio.getDescripcio());
    }

    @Test
    public void addPlantilla()
    {
        Plantilla plantillaPrecios = getPlantillaTest();

        ClientResponse response = resource.type("application/json").post(ClientResponse.class, plantillaPrecios);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });
        Assert.assertTrue(restResponse.getSuccess());
        Assert.assertNotNull(getFieldFromRestResponse(restResponse, "id"));
        Assert.assertEquals(plantillaPrecios.getNombre(),
                getFieldFromRestResponse(restResponse, "nombre"));
    }

    @Test
    public void updatePlantilla()
    {
        Plantilla plantilla = getPlantillaTest();
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, plantilla);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        String id = getFieldFromRestResponse(restResponse, "id");
        Assert.assertNotNull(id);

        plantilla.setNombre("Prueba2");

        response = resource.path(String.valueOf(plantilla.getId())).type("application/json").put(ClientResponse.class, plantilla);
        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        Assert.assertEquals(plantilla.getNombre(),
                getFieldFromRestResponse(restResponse, "nombre"));
    }

    @Test
    public void updatePlantillaAndRemoveNombre()
    {
        Plantilla plantilla = getPlantillaTest();
        ClientResponse response = resource.type("application/json").post(ClientResponse.class, plantilla);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        String id = getFieldFromRestResponse(restResponse, "id");
        Assert.assertNotNull(id);

        plantilla.setNombre("");
        response = resource.path(id).type("application/json").put(ClientResponse.class, plantilla);
        Assert.assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

        ResultatOperacio parResponseMessage = response.getEntity(ResultatOperacio.class);
        Assert.assertEquals(CampoRequeridoException.REQUIRED_FIELD + "Nombre",
                parResponseMessage.getDescripcio());
    }
}
