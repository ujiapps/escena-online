package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response.Status;

import es.uji.apps.par.dao.CinesDAO;
import es.uji.apps.par.dao.EventosDAO;
import es.uji.apps.par.dao.SalasDAO;
import es.uji.apps.par.dao.TpvsDAO;
import es.uji.apps.par.model.TipoEvento;

public class EventosResourceTest extends BaseResourceTest
{
    @Autowired
    EventosDAO eventosDAO;

    @Autowired
    TpvsDAO tpvsDAO;

    @Autowired
    CinesDAO cinesDAO;

    @Autowired
    SalasDAO salasDAO;

    @Test
    public void getEventos()
    {
        ClientResponse response = resource.path("evento").get(ClientResponse.class);
        RestResponse serviceResponse = response.getEntity(RestResponse.class);

        Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
        Assert.assertTrue(serviceResponse.getSuccess());
        Assert.assertNotNull(serviceResponse.getData());
    }

    @Test
    public void addTipoEvento()
    {
        TipoEvento tipoEvento = getTipoEventoTest();

        ClientResponse response = resource.path("tipoevento").type("application/json").post(ClientResponse.class, tipoEvento);
        Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        RestResponse serviceResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        Assert.assertTrue(serviceResponse.getSuccess());
        Assert.assertNotNull(serviceResponse.getData());
    }
}
