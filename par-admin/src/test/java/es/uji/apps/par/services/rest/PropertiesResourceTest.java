package es.uji.apps.par.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;

import javax.ws.rs.core.Response;

import static es.uji.apps.par.services.rest.BaseResourceTest.clientConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-db-test.xml", "/applicationContext-configproperties-test.xml"})
@Transactional
public class PropertiesResourceTest extends JerseyTest
{
    WebResource resource;

    String WEB = "localhost:9003/par-public";
    String HTML_TITLE = "HTML TITLE";
    String CONDICIONES_URL = "http://example.com/condiciones.html";
    String CANCELACION_URL = "http://example.com/cancelacion.html";
    String COMO_LLEGAR_URL = "http://example.com/documento.pdf";
    String PIE_ENTRADA_URL = "http://example.com/example.jpg";
    String MAIL_FROM = "mailFrom";
    String LOGO_REPORT = "logo-vertical-color.svg";
    String PUBLIC_URL = String.format("https://%s", WEB);
    String PUBLIC_URL_SIN_HTTPS = String.format("http://%s", WEB);
    String PUBLIC_URL_LIMPIO = PUBLIC_URL_SIN_HTTPS;

    public PropertiesResourceTest()
    {
        super(
            new WebAppDescriptor.Builder(
                "es.uji.apps.par.services.rest;com.fasterxml.jackson.jaxrs.json;es.uji.apps.par")
                .contextParam("contextConfigLocation",
                    "classpath:applicationContext-db-test.xml classpath:applicationContext-configproperties-test.xml")
                .contextParam("webAppRootKey", "paranimf-fw-uji.root")
                .contextListenerClass(ContextLoaderListener.class)
                .clientConfig(clientConfiguration())
                .requestListenerClass(RequestContextListener.class)
                .servletClass(SpringServlet.class).build());

        this.client().addFilter(new LoggingFilter());
        this.resource = resource();
    }

    @Test
    public void getPublicUrlFromProperties()
    {
        ClientResponse response = resource.path("index/public/properties").get(ClientResponse.class);

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        RestResponse restResponse = response.getEntity(new GenericType<RestResponse>()
        {
        });

        Assert.assertNotNull(restResponse.getData());
        Assert.assertTrue(restResponse.getTotal() == 9);
        Assert.assertEquals(restResponse.getData().get(0), PUBLIC_URL);
        Assert.assertEquals(restResponse.getData().get(1), PUBLIC_URL_LIMPIO);
        Assert.assertEquals(restResponse.getData().get(2), HTML_TITLE);
        Assert.assertEquals(restResponse.getData().get(3), CONDICIONES_URL);
        Assert.assertEquals(restResponse.getData().get(4), CANCELACION_URL);
        Assert.assertEquals(restResponse.getData().get(5), COMO_LLEGAR_URL);
        Assert.assertEquals(restResponse.getData().get(6), PIE_ENTRADA_URL);
        Assert.assertEquals(restResponse.getData().get(7), MAIL_FROM);
        Assert.assertEquals(restResponse.getData().get(8), LOGO_REPORT);
    }
}
