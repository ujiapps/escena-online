package es.uji.apps.par.services.rest;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import com.sun.jersey.test.framework.spi.container.TestContainerFactory;
import com.sun.jersey.test.framework.spi.container.grizzly.web.GrizzlyWebTestContainerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import es.uji.apps.par.builders.LocalizacionBuilder;
import es.uji.apps.par.builders.PlantillaBuilder;
import es.uji.apps.par.builders.SalaBuilder;
import es.uji.apps.par.builders.SesionBuilder;
import es.uji.apps.par.builders.TipoEventoBuilder;
import es.uji.apps.par.builders.UsuarioBuilder;
import es.uji.apps.par.config.Configuration;
import es.uji.apps.par.db.SalaDTO;
import es.uji.apps.par.model.Localizacion;
import es.uji.apps.par.model.Plantilla;
import es.uji.apps.par.model.Sala;
import es.uji.apps.par.model.Sesion;
import es.uji.apps.par.model.TipoEvento;
import es.uji.apps.par.model.Usuario;

@ContextConfiguration(locations = {"/applicationContext-db-test.xml", "/applicationContext-configdatabase-test.xml"})
public class BaseResourceTest extends JerseyTest {
    protected WebResource resource;

    @Autowired
    Configuration configuration;

    public BaseResourceTest(WebAppDescriptor build) {
        super(build);
    }

    public BaseResourceTest() {
        super(new WebAppDescriptor.Builder(
            "es.uji.apps.par.services.rest;com.fasterxml.jackson.jaxrs.json;es.uji.apps.par")
            .contextParam("contextConfigLocation",
                "classpath:applicationContext-db-test.xml classpath:applicationContext-configdatabase-test.xml")
            .contextParam("webAppRootKey", "paranimf-fw-uji.root").contextListenerClass(ContextLoaderListener.class)
            .clientConfig(clientConfiguration()).requestListenerClass(RequestContextListener.class)
            .servletClass(SpringServlet.class).build());

        this.client().addFilter(new LoggingFilter());

        Logger rootLogger = LogManager.getLogManager().getLogger("");
        rootLogger.setLevel(Level.SEVERE);
        for (Handler h : rootLogger.getHandlers()) {
            h.setLevel(Level.SEVERE);
        }

        this.resource = resource();
    }

    protected static ClientConfig clientConfiguration() {
        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(JacksonJaxbJsonProvider.class);
        return config;
    }

    @Override
    protected TestContainerFactory getTestContainerFactory() {
        return new GrizzlyWebTestContainerFactory();
    }

    @Override
    protected int getPort(int defaultPort) {
        return 19998;
    }

    protected Localizacion getLocalizacionTest() {
        return new LocalizacionBuilder("Prueba", "TEST", new BigDecimal(100)).get();
    }

    protected Sala getSalaTest() {
        return new SalaBuilder("nombre de la sala 1", null).get();
    }

    protected TipoEvento getTipoEventoTest() {
        return new TipoEventoBuilder("Prueba", "Prova", false, null).get();
    }

    protected Plantilla getPlantillaTest() {
        SalaDTO salaDTO = Sala.salaToSalaDTO(getSalaTest());
        salaDTO.setId(1);
        return new PlantillaBuilder("Prueba", salaDTO).get();
    }

    protected Sesion getSesionTest(Plantilla plantilla) {
        return new SesionBuilder(null, plantilla != null ? Sala.salaToSalaDTO(plantilla.getSala()) : null, null,
            "12:30").withVentaOnline().withPlantilla(Plantilla.plantillaPreciosToPlantillaPreciosDTO(plantilla)).get();
    }

    protected Usuario getUsuarioTest(String nombre) {
        return new UsuarioBuilder(nombre, "mail", nombre).get();
    }

    protected String getFieldFromRestResponse(
        RestResponse restResponse,
        String field
    ) {
        return ((HashMap) restResponse.getData().get(0)).get(field).toString();
    }
}
