INSERT INTO PAR_USUARIOS (ID, NOMBRE, USUARIO, MAIL, URL)
VALUES (1, 'Sergio', 'sergio', 'sergio@4tic.com', 'localhost');

INSERT INTO PAR_CINES (id, codigo, nombre, cif, direccion, cod_municipio, nom_municipio, cp, empresa, cod_registro,
                       tfno, iva, url_public, url_privacidad, url_cancelacion, url_como_llegar, mail_from,
                       url_pie_entrada, logo_report,
                       api_key, butacasentradoendistintocolor, langs, default_lang, show_iva, passbook_activado,
                       limite_entradas_gratis, comision, email, registro_mercantil, logo, logo_content_type, logo_src,
                       logo_uuid, banner, banner_content_type, banner_src, banner_uuid, clase_postventa, informes,
                       informes_generales, menu_tpv, menu_clientes, menu_integraciones, menu_salas, menu_perfil,
                       menu_ayuda, menu_taquilla, check_contratacion, formas_pago, menu_venta_anticipada, menu_abonos,
                       show_como_nos_conociste, show_logo_entrada, no_numeradas_secuencial, is_anulable_desde_enlace,
                       tiempo_anulable_desde_enlace, codigo_icaa, menu_icaa)
VALUES (1, 'huesca', 'Palacio de Congresos', 'A-22338495', 'Avda. de los Danzantes, s/n', '1', 'Huesca (España)',
        '22005', 'Ayuntamiento Huesca', '1', '974292191', 0, 'https://dev.palaciodecongresoshuesca.4tic.com/par-public',
        'https://dev.palaciocongresoshuesca.es/aviso-legal', 'https://dev.palaciocongresoshuesca.es/cancelacion.html',
        'http://www.palaciocongresoshuesca.es/auditorio',
        'no_reply@4tic.com', 'http://example.com/example.jpg', 'pchu_logo.jpg', '123456789', false, null,
        null, true, true, 10, 0, null, null, null, 'image/png', 'palacio_logo.png', 'B1RY1RF8U5V9F4QQUE3MSBQZ545N62L0',
        null, 'image/jpeg', 'palacio_banner.jpg', '1OQYMK82BLDR737W3XFQGXG268MZBBWT', null, null, null, true, true,
        true, true, true, true, true, false, 'metalico,tarjetaOffline,transferencia,credito', true, true, false, true,
        false, true, 60, '123', false);

INSERT INTO PAR_SALAS (ID, CODIGO, NOMBRE, TIPO, FORMATO, SUBTITULO, CINE_ID,
                       HTML_TEMPLATE_NAME, ASIENTOS_NUMERADOS, IS_AFORO_POR_COMPRAS)
VALUES (1, '', '', '', '', '', 1, '', 1, false);
INSERT INTO PAR_SALAS_USUARIOS (id, USUARIO_ID, SALA_ID)
VALUES (1, 1, 1);
INSERT INTO PAR_TPVS (id, nombre, code, currency, terminal, transaction_code, order_prefix, lang_ca_code, lang_es_code,
                      url, wsdl_url, secret, cif, signature_method, visible)
VALUES (1, 'Palacio de Congresos', '43242', '978', '00000001', '2', '0000554027', '2', '1',
        'https://pgw.ceca.es/cgi-bin/tpv', '-', '321312', 'A-3213', 'CECA_SHA1', true);
INSERT INTO PAR_TPVS_SALAS (id, tpv_id, sala_id)
VALUES (1, 1, 1);
INSERT INTO PAR_TIPOS_EVENTO (id, nombre_es, nombre_va, exportar_icaa, cine_id)
VALUES (1, 'Teatro', 'Teatre', FALSE, 1);